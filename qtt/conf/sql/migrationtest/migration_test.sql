
--------------------------------------------------------------------------------------------------
--
-- Process Instace Table Test
--
--------------------------------------------------------------------------------------------------

-- Number of process instances in jbpm
select decode (c1 - c2,0,'OK','not every query was transform to JBPM') as test_result from 
(select count(*) c1 from jbpm_processinstance),
(select count(*) c2 from queries q where q.status_code in ('CLOS','PROC') and query_number not in ('QTT012102', 'QTT023768', 'QTT017096', 'QTT017188', 'QTT017200', 'QTT017201', 'QTT017202', 'QTT017203', 'QTT017204', 'QTT017205', 'QTT025905', 'QTT027470'));

--PI with no query attached
select decode(count(*),0,'OK',count(*) || ' process instances not attached to queries') as  test_result from jbpm_processinstance pi where pi.key_ is null;
--PI with no start date
select decode(count(*),0,'OK','ERROR ' ||count(*) || ' process instances have no start date')as  test_result from jbpm_processinstance pi where pi.start_ is  null;
--PI with no end date
select decode(count(*),0,'OK','ERROR ' ||count(*) || ' process instances have no end date')as  test_result 
from jbpm_processinstance pi join queries q on pi.key_ =q.query_seq
where pi.end_ is null and q.status_code = 'CLOS' ;
--PI with no token
select decode(count(*),0,'OK','ERROR ' ||count(*) || ' process instances have no end date')as  test_result from jbpm_processinstance pi where pi.roottoken_ is null;

--Uniqueness of tokens in PI
select decode(count(*),0,'OK',count(*) || ' process instances have not unique token') as  test_result from (
select count(*), pi.roottoken_
from jbpm_processinstance pi 
where pi.roottoken_ is not null
group by pi.roottoken_
having count(*)<>1
);

-- Are there any queries which have no processinstance attached
select decode(count(*),0,'OK','ERROR ' ||count(*) || ' queries have no processinstance attached')as  test_result 
from queries  where process_instance is null
and query_number not in ('QTT012102', 'QTT023768', 'QTT017096', 'QTT017188', 'QTT017200', 'QTT017201', 'QTT017202', 'QTT017203', 'QTT017204', 'QTT017205', 'QTT025905', 'QTT027470')
 and status_code in ( 'PROC','CLOS');



--------------------------------------------------------------------------------------------------
--
-- Tokens Table Test
--
--------------------------------------------------------------------------------------------------

-- Dates in tokens and PI
select decode(count(*),0,'OK',count(*) || ' process instances and tokens dates are not ok.')  as test_result
from jbpm_processinstance pi join JBPM_TOKEN tok on pi.roottoken_ = tok.id_
where tok.start_ <> pi.start_ or tok.end_ <> pi.end_ or tok.nodeenter_ <> tok.end_;


--If child tokens are connected to the same PI then the parent token
select t2.id_ as child_token_id,decode(t1.processinstance_-t2.processinstance_,0,'OK','Child token attached to different processinstance then parent token') as test_result from jbpm_token t1 join jbpm_token t2 on t1.id_ = t2.parent_;


--Number of child tokens
select decode(c1 - c2,0,'OK','Not every child token was created') testresult from 
(select count(*) c1 from jbpm_token where parent_ is  not null),
(SELECT count(*) c2 FROM wp_work_item WHERE  complete_date is null
and (name ='act-QTT-MI-Medical Interpretation II' or name like '%DMG%')
and WORK_STATE_ID <> 6
and proci_id in 
(
select proci_id from wp_proci where proci_ref in (
select query_number from queries where  status_code = 'PROC' and workflow_route_type= 'B' and
query_number not in ('QTT012102', 'QTT023768', 'QTT017096', 'QTT017188', 'QTT017200', 'QTT017201', 'QTT017202', 'QTT017203', 'QTT017204', 'QTT017205', 'QTT025905', 'QTT027470')
and
query_seq  in (
   select key_ from jbpm_processinstance where id_ in  
     (select processinstance_ from jbpm_token where parent_ is  not null )
   )
)));

-- Are there any null node_s
select decode(count(*),0,'OK','ERROR ' ||count(*) || ' tokens has no node_ set') test_result from jbpm_token where node_ is null;

--Closed tokens' node_ column should contain 'end' node
select decode(name_,'end','OK','Parent token node_ column should contain fork1 node') as test_result  from jbpm_node where id_ in (
select distinct node_ from jbpm_token where processinstance_ in
(
select id_ from jbpm_processinstance where key_ in (
select query_seq from queries where  status_code = 'CLOS' and
query_number not in ('QTT012102', 'QTT023768', 'QTT017096', 'QTT017188', 'QTT017200', 'QTT017201', 'QTT017202', 'QTT017203', 'QTT017204', 'QTT017205', 'QTT025905', 'QTT027470')
)));

--parent token node_ column should contain 'fork1' node
select decode(name_,'fork1','OK','Parent token node_ column should contain fork1 node') as test_result  from jbpm_node where id_ in (
select node_ from jbpm_token where id_ in (
select distinct parent_ from jbpm_token where parent_ is  not null));

--Closed tokens' node_ column should contain 'end' node
select decode(name_,'fork1','OK','Parent token node_ column should contain fork1 node') as test_result  from jbpm_node where id_ in (
select node_ from jbpm_token where id_ in (
select distinct parent_ from jbpm_token where parent_ is  not null));




--------------------------------------------------------------------------------------------------
--
-- MODULEINSTANCE Table Test
--
--------------------------------------------------------------------------------------------------

--Moduleinstances in JBPM

select decode(c1 - c2,0,'OK','Not every process instance has moduleinstance with class _C_') as testresult from 
(select count(*) c1 from  jbpm_moduleinstance mi join jbpm_processinstance pi   on pi.id_ = mi.processinstance_
where mi.class_ = 'C' and mi.name_ = 'org.jbpm.context.exe.ContextInstance'),
(select count(*) c2 from  jbpm_processinstance pi2 );

select decode (c1 - c2,0,'OK','Not every process instance has moduleinstance with class _T_') as testresult from 
(select count(*) c1 from  jbpm_moduleinstance mi join jbpm_processinstance pi   on pi.id_ = mi.processinstance_
where mi.class_ = 'T' and mi.name_ = 'org.jbpm.taskmgmt.exe.TaskMgmtInstance'
and mi.taskmgmtdefinition_ = (SELECT id_ FROM JBPM_MODULEDEFINITION WHERE name_ = 'org.jbpm.taskmgmt.def.TaskMgmtDefinition')),
(select count(*) c2 from  jbpm_processinstance pi2 );


--------------------------------------------------------------------------------------------------
--
-- JBPM_TOKENVARIABLEMAP Table Test
--
--------------------------------------------------------------------------------------------------

select decode(c1 - c2,0,'OK','Not every token has tokenvariablemap row') as test_result from 
(select count(*) c1 from jbpm_token where parent_ is null),
(select count(*) c2 from jbpm_token ti join JBPM_TOKENVARIABLEMAP vm on vm.token_ = ti.id_);

--------------------------------------------------------------------------------------------------
--
-- JBPM_VARIABLEINSTANCE  Table Test
--
--------------------------------------------------------------------------------------------------

-- If all columns are set
select decode(count(*),0,'OK','Not all required columns set') as test_result from JBPM_VARIABLEINSTANCE 
where name_ is null or class_ is null or token_ is null or tokenvariablemap_ is null  or processinstance_ is null
or (stringvalue_ is null and longvalue_ is null);

--Variable PROCESS_INSTANCE_START_ACTOR_ID check
select decode(T1.c1-T2.c2,0,'OK','Not the same amount of processes initialized by specific user') as test_result from
(select count(INITIATOR_ID) c1,INITIATOR_ID from wp_proci
where 
proci_ref in (
   select query_number from queries where  (status_code = 'PROC' or status_code = 'CLOS')  and
   query_number not in ('QTT012102', 'QTT023768', 'QTT017096', 'QTT017188', 'QTT017200', 'QTT017201', 'QTT017202', 'QTT017203', 'QTT017204', 'QTT017205', 'QTT025905', 'QTT027470')
)
group by INITIATOR_ID
) T1 join 
(select count(stringvalue_) c2,stringvalue_  from JBPM_VARIABLEINSTANCE 
where name_ = 'PROCESS_INSTANCE_START_ACTOR_ID'
group by stringvalue_) T2 on T1.INITIATOR_ID = T2.stringvalue_;

--Variable PROCESS_INSTANCE_OWNER_ID check
select decode(count(*),0,'OK','Variable value different as query_seq') as test_result from JBPM_VARIABLEINSTANCE vi join jbpm_processinstance pi on vi.processinstance_ = pi.id_ join 
queries q on pi.key_ = q.query_seq 
where (q.status_code = 'PROC' or q.status_code = 'CLOS')  and
q.query_number not in ('QTT012102', 'QTT023768', 'QTT017096', 'QTT017188', 'QTT017200', 'QTT017201', 'QTT017202', 'QTT017203', 'QTT017204', 'QTT017205', 'QTT025905', 'QTT027470')
and vi.name_ = 'PROCESS_INSTANCE_OWNER_ID'
and vi.longvalue_ <> q.query_seq;

--Variables PS_ASSIGNED_ACTOR_ID and PS_ASSIGNED_ACTOR_EMAIL
select distinct T1.stringvalue_ , 'WARNING , no email in security DS for this actorID'from
(select name_,processinstance_,token_,stringvalue_  from JBPM_VARIABLEINSTANCE 
where name_ ='PS_ASSIGNED_ACTOR_ID') T1  left join 
(
select name_,processinstance_,token_,stringvalue_  from JBPM_VARIABLEINSTANCE 
where name_ ='PS_ASSIGNED_ACTOR_EMAIL'
) T2 on T1.token_ = T2.token_
where T2.stringvalue_ is null;

--------------------------------------------------------------------------------------------------
--
-- JBPM_TASKINSTANCE  Table Test
--
--------------------------------------------------------------------------------------------------


select decode(count(*),0,'OK','class_ not set') as test_result from jbpm_taskinstance where class_ is null;
select decode(count(*),0,'OK','name_ not set') as test_result from jbpm_taskinstance where name_ is null;
select decode(count(*),0,'OK','create_ not set') as test_result from jbpm_taskinstance where create_ is null;
select decode(count(*),0,'OK','priority_ not set') as test_result from jbpm_taskinstance where priority_ <> 3;

select decode(count(*),0,'OK','iscancelled_ not set')as test_result from jbpm_taskinstance where  iscancelled_ is null;
select decode(count(*),0,'OK','issuspended_ not set')as test_result from jbpm_taskinstance where issuspended_ is null;
select decode(count(*),0,'OK','isopen_ not set')as test_result from jbpm_taskinstance where isopen_ is null;
select decode(count(*),0,'OK','issignalling_ not set')as test_result from jbpm_taskinstance where issignalling_ is null;
select decode(count(*),0,'OK','token_ not set')as test_result from jbpm_taskinstance where token_ is null;
select decode(count(*),0,'OK','taskmgmtinstance_ not set')as test_result from jbpm_taskinstance where taskmgmtinstance_ is null;
select decode(count(*),0,'OK','procinst_ not set')as test_result from jbpm_taskinstance where procinst_ is null;

select decode(c1-c2,0,'OK','start_ not set')as test_result  from 
(select count(*) as c1 from jbpm_taskinstance where start_ is null) T1 ,
(SELECT count(*) as c2 FROM wp_work_item WHERE  
proci_id in 
(
select proci_id from wp_proci where   proci_ref in (
   select query_number from queries where  status_code in ('PROC','CLOS')  and
   query_number not in ('QTT012102', 'QTT023768', 'QTT017096', 'QTT017188', 'QTT017200', 'QTT017201', 'QTT017202', 'QTT017203', 'QTT017204', 'QTT017205', 'QTT025905', 'QTT027470')
)
) and  OPEN_DATE  is null
) T2;

select decode(T1.c1 - T2.c2,0,'OK','actorid_ not set')as test_result from 
(select count(*) c1 from jbpm_taskinstance ti where actorid_ is null) T1,
(
select count(distinct ti.id_) c2 from
 jbpm_processinstance pi 
join queries q on q.query_seq = pi.key_
join wp_proci wpp on wpp.proci_ref = q.query_number
join wp_work_item wpi on wpi.proci_id = wpp.proci_id
join jbpm_taskinstance ti on pi.id_ = ti.PROCINST_
where 
ti.actorid_ is null  and wpi.assigned_to is null and wpi.acti_id is not null
) T2;

--------------------------------------------------------------------------------------------------
--
-- JBPM_JOB  Table Test
--
--------------------------------------------------------------------------------------------------
select decode(count(*),0,'OK','duedate_ not set') as test_result from JBPM_JOB where duedate_ is null;
select decode(count(*),0,'OK','name_ not set') as test_result from JBPM_JOB where name_ is null;
select decode(count(*),0,'OK','action_ not set')as test_result from JBPM_JOB where action_ is null;
select decode(count(*),0,'OK','graphelement_ not set')as test_result from JBPM_JOB where graphelement_ is null;

select decode(c4- c3,0,'OK','repeat_ and transitionname_ not set')as test_result from 
(select count(*) as c4 from JBPM_JOB ),
(select T1.c1+T2.c2 as c3 from 
(select count(*) c1 from JBPM_JOB where repeat_ is null) T1 ,
(select count(*) c2 from JBPM_JOB where transitionname_ is null) T2);






