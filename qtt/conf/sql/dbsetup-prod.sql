alter table queries add (PROCESS_INSTANCE number(10,0) null);
create index rqtr_nk_i3 ON requesters (email);

---

--disable all foreign key constraints in the audit_history* tables referencing queries  --BEGIN
set def on ver off
spool enable_disable_fk.log

declare
l_str_sel_cur varchar2(4000);
l_str_sql_ddl varchar2(4000);
TabList varchar2(4000);
l_owner user_constraints.owner%TYPE;
l_constraint_name user_constraints.constraint_name%TYPE;
l_table_name user_constraints.table_name%TYPE;
type t_cur_fk is ref cursor;
cur_fk t_cur_fk;

begin
TabList:='''AUDIT_HISTORY_T'', ''AUDIT_HISTORY'', ''TEMP_AUDIT_HISTORY'', ''TEMP_AUDIT_HISTORY_I'', ''TEMP_WP_AUDIT_HISTORY'', ''TEMP_WP_AUDIT_HISTORY_I''';
l_str_sel_cur:= 'select fk.owner, fk.constraint_name , fk.table_name ' ||
' from user_constraints fk ' ||
' where fk.CONSTRAINT_TYPE = ''R'' and ' ||
' fk.owner = ''QTT_JBPM'' and ' ||
' ' || ' fk.TABLE_NAME in ( ' || TabList || ')';

open cur_fk for l_str_sel_cur;
--dbms_output.put_line('Executing: ');
--dbms_output.put_line(l_str_sel_cur);
loop
fetch cur_fk into l_owner, l_constraint_name, l_table_name;
exit when cur_fk%notfound;
l_str_sql_ddl := 'ALTER TABLE QTT_JBPM.'||
l_table_name||' MODIFY CONSTRAINT '||l_constraint_name||' DISABLE';

--dbms_output.put_line(l_str_sql_ddl);
execute immediate l_str_sql_ddl;
end loop;

if cur_fk%rowcount = 0 then
--dbms_output.put_line('No Foreign key to modify');
end if;
close cur_fk;
end;
--disable all foreign key constraints in the audit_history* tables referencing queries --END
