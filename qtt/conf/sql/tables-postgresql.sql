CREATE TABLE "qtt_jbpm"."ae_terms"
(
   query_seq bigint PRIMARY KEY NOT NULL,
   description varchar(600)
)
;
CREATE TABLE "qtt_jbpm"."affiliate_notes"
(
   note_seq bigint PRIMARY KEY NOT NULL,
   create_ts timestamp NOT NULL,
   followup_seq bigint,
   query_seq bigint NOT NULL,
   text varchar(1000) NOT NULL,
   username varchar(30) NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."attachment_categories"
(
   attachment_category_id smallint PRIMARY KEY NOT NULL,
   affiliate_visibility_flag char(1) NOT NULL,
   attachment_category varchar(40) NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."attachments"
(
   attachment_seq bigint PRIMARY KEY NOT NULL,
   activation_flag char(1) NOT NULL,
   author varchar(80),
   create_ts timestamp NOT NULL,
   filename varchar(100) NOT NULL,
   followup_number varchar(20),
   followup_seq bigint,
   medical_opinion_flag char(1) NOT NULL,
   query_seq bigint NOT NULL,
   title varchar(30) NOT NULL,
   version smallint NOT NULL,
   attachment_category_id smallint
)
;
CREATE TABLE "qtt_jbpm"."audit_activity"
(
   activity_type_id smallint PRIMARY KEY NOT NULL,
   activity_type varchar(50) NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."audit_history"
(
   audit_seq bigint PRIMARY KEY NOT NULL,
   current_flag varchar(1),
   followup_seq bigint,
   responsible varchar(18),
   start_ts timestamp NOT NULL,
   stop_ts timestamp,
   wf_process_seq varchar(18),
   query_seq bigint NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."case_oformats"
(
   case_oformat_id bigint PRIMARY KEY NOT NULL,
   case_oformat varchar(30) NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."cases"
(
   case_seq bigint PRIMARY KEY NOT NULL,
   external_reference_number varchar(400),
   local_reference_number varchar(50),
   mcn_number varchar(120),
   query_seq bigint NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."clinical_trials"
(
   query_seq bigint PRIMARY KEY NOT NULL,
   case_selection varchar(600),
   crtn_numbers varchar(300),
   patient_numbers varchar(300),
   protocol_numbers varchar(100),
   ctrl_oformat_id bigint
)
;
CREATE TABLE "qtt_jbpm"."ctrl_oformats"
(
   ctrl_oformat_id bigint PRIMARY KEY NOT NULL,
   ctrl_oformat varchar(50) NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."drugs"
(
   drug_number_seq bigint PRIMARY KEY NOT NULL,
   dose varchar(50),
   drug_preferred_name varchar(200),
   extra_first_drugs varchar(400),
   first_flag char(1) NOT NULL,
   formulation varchar(50),
   indication varchar(50),
   inn_generic_name varchar(120),
   roche_drug_flag char(1) NOT NULL,
   route varchar(50),
   query_seq bigint NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."followup_attachments"
(
   followup_seq bigint NOT NULL,
   query_seq bigint NOT NULL,
   create_ts timestamp NOT NULL,
   end_ts timestamp,
   followup_number varchar(20) NOT NULL,
   status_code varchar(4),
   CONSTRAINT followup_attachments_pkey PRIMARY KEY (followup_seq,query_seq)
)
;
CREATE TABLE "qtt_jbpm"."fu_ae_terms"
(
   followup_seq bigint NOT NULL,
   query_seq bigint NOT NULL,
   description varchar(600),
   CONSTRAINT fu_ae_terms_pkey PRIMARY KEY (followup_seq,query_seq)
)
;
CREATE TABLE "qtt_jbpm"."fu_cases"
(
   case_seq bigint NOT NULL,
   followup_seq bigint NOT NULL,
   query_seq bigint NOT NULL,
   external_reference_number varchar(400),
   local_reference_number varchar(50),
   mcn_number varchar(120),
   CONSTRAINT fu_cases_pkey PRIMARY KEY (case_seq,followup_seq,query_seq)
)
;
CREATE TABLE "qtt_jbpm"."fu_clinical_trials"
(
   followup_seq bigint NOT NULL,
   query_seq bigint NOT NULL,
   case_selection varchar(600),
   crtn_numbers varchar(300),
   ctrl_oformat_id bigint,
   patient_numbers varchar(300),
   protocol_numbers varchar(100),
   CONSTRAINT fu_clinical_trials_pkey PRIMARY KEY (followup_seq,query_seq)
)
;
CREATE TABLE "qtt_jbpm"."fu_drugs"
(
   drug_number_seq bigint NOT NULL,
   followup_seq bigint NOT NULL,
   query_seq bigint NOT NULL,
   dose varchar(50),
   drug_preferred_name varchar(200),
   extra_first_drugs varchar(400),
   first_flag char(1) NOT NULL,
   formulation varchar(50),
   indication varchar(50),
   inn_generic_name varchar(120),
   roche_drug_flag char(1) NOT NULL,
   route varchar(50),
   CONSTRAINT fu_drugs_pkey PRIMARY KEY (drug_number_seq,followup_seq,query_seq)
)
;
CREATE TABLE "qtt_jbpm"."fu_labelling_documents"
(
   followup_seq bigint NOT NULL,
   ldoc_type_id bigint NOT NULL,
   query_seq bigint NOT NULL,
   CONSTRAINT fu_labelling_documents_pkey PRIMARY KEY (followup_seq,ldoc_type_id,query_seq)
)
;
CREATE TABLE "qtt_jbpm"."fu_organisations"
(
   followup_seq bigint NOT NULL,
   organisation_id bigint NOT NULL,
   name varchar(60) NOT NULL,
   type_id bigint NOT NULL,
   CONSTRAINT fu_organisations_pkey PRIMARY KEY (followup_seq,organisation_id)
)
;
CREATE TABLE "qtt_jbpm"."fu_performance_metrics"
(
   followup_seq bigint NOT NULL,
   query_seq bigint NOT NULL,
   request_description varchar(1000) NOT NULL,
   CONSTRAINT fu_performance_metrics_pkey PRIMARY KEY (followup_seq,query_seq)
)
;
CREATE TABLE "qtt_jbpm"."fu_pre_query_preparations"
(
   followup_seq bigint NOT NULL,
   pqp_type_id smallint NOT NULL,
   query_seq bigint NOT NULL,
   other_comment varchar(200),
   CONSTRAINT fu_pre_query_preparations_pkey PRIMARY KEY (followup_seq,pqp_type_id,query_seq)
)
;
CREATE TABLE "qtt_jbpm"."fu_pregnancies"
(
   followup_seq bigint NOT NULL,
   query_seq bigint NOT NULL,
   exposure_type_id bigint,
   outcome varchar(100),
   time_in_pregnancy varchar(100),
   CONSTRAINT fu_pregnancies_pkey PRIMARY KEY (followup_seq,query_seq)
)
;
CREATE TABLE "qtt_jbpm"."fu_queries"
(
   followup_seq bigint NOT NULL,
   query_seq bigint NOT NULL,
   access_key varchar(32),
   affiliate_access char(1),
   alert_flag char(1) NOT NULL,
   all_drugs_flag char(1) NOT NULL,
   case_comment varchar(300),
   case_oformat_id bigint,
   close_error_status char(1),
   current_due_date timestamp,
   date_requested timestamp,
   datetime_submitted timestamp,
   dmg_due_date timestamp,
   expiry_date timestamp,
   follow_up_number varchar(75),
   followup_flag char(1) NOT NULL,
   followup_id smallint,
   followup_number varchar(20),
   ind_number varchar(50),
   mnft_batch_number varchar(70),
   oformat_special_request varchar(300),
   original_due_date timestamp,
   pre_query_prep_comment varchar(200),
   psst_state char(1),
   query_label varchar(300),
   query_number varchar(20),
   query_summary varchar(1300),
   query_summary_upper varchar(1300),
   requester_id bigint NOT NULL,
   requester_query_label varchar(250),
   status_code varchar(4) NOT NULL,
   urgency_flag char(1) NOT NULL,
   urgency_reason varchar(300),
   workflow_route_type varchar(2),
   source_country_code varchar(10) NOT NULL,
   query_type_id smallint NOT NULL,
   reporter_type_id smallint NOT NULL,
   CONSTRAINT fu_queries_pkey PRIMARY KEY (followup_seq,query_seq)
)
;
CREATE TABLE "qtt_jbpm"."fu_query_descriptions"
(
   followup_seq bigint NOT NULL,
   query_seq bigint NOT NULL,
   conmeds varchar(1000),
   diagnoses varchar(1000),
   index_case varchar(1000),
   investigations varchar(1000),
   nature_of_case varchar(1000) NOT NULL,
   other_info varchar(1000),
   prev_medical_history varchar(1000),
   signs_and_symptoms varchar(1000),
   CONSTRAINT fu_query_descriptions_pkey PRIMARY KEY (followup_seq,query_seq)
)
;
CREATE TABLE "qtt_jbpm"."fu_requesters"
(
   followup_seq bigint NOT NULL,
   requester_id bigint NOT NULL,
   email varchar(50) NOT NULL,
   fax_number varchar(25),
   first_name varchar(25) NOT NULL,
   modified_ts timestamp NOT NULL,
   organisation_id bigint NOT NULL,
   surname varchar(50) NOT NULL,
   telephone varchar(25) NOT NULL,
   username varchar(30),
   country_code varchar(10) NOT NULL,
   CONSTRAINT fu_requesters_pkey PRIMARY KEY (followup_seq,requester_id)
)
;
CREATE TABLE "qtt_jbpm"."fu_retrieval_periods"
(
   followup_seq bigint NOT NULL,
   query_seq bigint NOT NULL,
   cumulative_flag varchar(1) NOT NULL,
   end_ts timestamp,
   interval varchar(60),
   start_ts timestamp,
   CONSTRAINT fu_retrieval_periods_pkey PRIMARY KEY (followup_seq,query_seq)
)
;
CREATE TABLE "qtt_jbpm"."labelling_documents"
(
   query_seq bigint NOT NULL,
   ldoc_type_id bigint NOT NULL,
   CONSTRAINT labelling_documents_pkey PRIMARY KEY (query_seq,ldoc_type_id)
)
;
CREATE TABLE "qtt_jbpm"."ldoc_types"
(
   ldoc_type_id bigint PRIMARY KEY NOT NULL,
   ldoc_type varchar(50) NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."non_affiliate_notes"
(
   note_seq bigint PRIMARY KEY NOT NULL,
   create_ts timestamp NOT NULL,
   followup_seq bigint,
   query_seq bigint NOT NULL,
   text varchar(1000) NOT NULL,
   username varchar(30) NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."non_roche_users"
(
   email varchar(50) PRIMARY KEY NOT NULL,
   active varchar(1) NOT NULL,
   password varchar(50) NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."notifications"
(
   expiry_date bytea NOT NULL,
   query_seq bigint NOT NULL,
   CONSTRAINT notifications_pkey PRIMARY KEY (expiry_date,query_seq)
)
;
CREATE TABLE "qtt_jbpm"."organisation_types"
(
   type_id bigint PRIMARY KEY NOT NULL,
   type varchar(20) NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."organisations"
(
   organisation_id bigint PRIMARY KEY NOT NULL,
   name varchar(60) NOT NULL,
   type_id bigint NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."performance_metrics"
(
   query_seq bigint PRIMARY KEY NOT NULL,
   request_description varchar(1000) NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."pqp_types"
(
   pqp_type_id smallint PRIMARY KEY NOT NULL,
   pqp_type varchar(50) NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."pre_query_preparations"
(
   pqp_type_id smallint NOT NULL,
   query_seq bigint NOT NULL,
   other_comment varchar(200),
   CONSTRAINT pre_query_preparations_pkey PRIMARY KEY (pqp_type_id,query_seq)
)
;
CREATE TABLE "qtt_jbpm"."pregnancies"
(
   query_seq bigint PRIMARY KEY NOT NULL,
   outcome varchar(100),
   time_in_pregnancy varchar(100),
   exposure_type_id bigint
)
;
CREATE TABLE "qtt_jbpm"."prgy_exptypes"
(
   exposure_type_id bigint PRIMARY KEY NOT NULL,
   exposure_type varchar(8) NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."qtt_workflow_processes"
(
   wf_process_seq varchar(18) PRIMARY KEY NOT NULL,
   wf_process_name varchar(50) NOT NULL,
   activity_type_id smallint NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."queries"
(
   query_seq bigint PRIMARY KEY NOT NULL,
   access_key varchar(32),
   affiliate_access char(1),
   alert_flag char(1) NOT NULL,
   all_drugs_flag char(1) NOT NULL,
   case_comment varchar(300),
   close_error_status char(1),
   current_due_date timestamp,
   date_requested timestamp,
   datetime_submitted timestamp,
   dmg_due_date timestamp,
   expiry_date timestamp,
   follow_up_number varchar(75),
   followup_flag char(1) DEFAULT 'N'::bpchar NOT NULL,
   followup_id smallint,
   followup_number varchar(20),
   followup_seq bigint,
   ind_number varchar(50),
   mnft_batch_number varchar(70),
   oformat_special_request varchar(300),
   original_due_date timestamp,
   pre_query_prep_comment varchar(200),
   process_instance bigint,
   psst_state char(1),
   query_label varchar(300),
   query_number varchar(20),
   query_summary varchar(1300),
   query_summary_upper varchar(1300),
   reopen char(1) DEFAULT 'N'::bpchar,
   requester_query_label varchar(250),
   source_country_code varchar(10) NOT NULL,
   urgency_flag char(1) NOT NULL,
   urgency_reason varchar(300),
   workflow_route_type varchar(2),
   case_oformat_id bigint,
   status_code varchar(4) NOT NULL,
   query_type_id smallint NOT NULL,
   reporter_type_id smallint NOT NULL,
   requester_id bigint NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."query_descriptions"
(
   query_seq bigint PRIMARY KEY NOT NULL,
   conmeds varchar(1000),
   diagnoses varchar(1000),
   index_case varchar(1000),
   investigations varchar(1000),
   nature_of_case varchar(1000) NOT NULL,
   other_info varchar(1000),
   prev_medical_history varchar(1000),
   signs_and_symptoms varchar(1000)
)
;
CREATE TABLE "qtt_jbpm"."query_status_codes"
(
   status_code varchar(4) PRIMARY KEY NOT NULL,
   status varchar(20)
)
;
CREATE TABLE "qtt_jbpm"."query_types"
(
   query_type_id smallint PRIMARY KEY NOT NULL,
   query_type varchar(60) NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."reporter_types"
(
   reporter_type_id smallint PRIMARY KEY NOT NULL,
   reporter_type varchar(50) NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."requesters"
(
   requester_id bigint PRIMARY KEY NOT NULL,
   email varchar(50) NOT NULL,
   fax_number varchar(25),
   first_name varchar(25) NOT NULL,
   modified_ts timestamp NOT NULL,
   surname varchar(50) NOT NULL,
   telephone varchar(25) NOT NULL,
   username varchar(30),
   country_code varchar(10) NOT NULL,
   organisation_id bigint NOT NULL
)
;
CREATE TABLE "qtt_jbpm"."retrieval_periods"
(
   query_seq bigint PRIMARY KEY NOT NULL,
   cumulative_flag varchar(1) NOT NULL,
   end_ts timestamp,
   interval varchar(60),
   start_ts timestamp
)
;
CREATE TABLE "qtt_jbpm"."temp_audit_history"
(
   activity_date varchar(10) NOT NULL,
   activity_type varchar(50) NOT NULL,
   country varchar(50) NOT NULL,
   name varchar(76) NOT NULL,
   query_iteration numeric(19) NOT NULL,
   query_no varchar(40) NOT NULL,
   query_type varchar(60) NOT NULL,
   reporter_type varchar(50) NOT NULL,
   site varchar(50) NOT NULL,
   workflow_route_type varchar(12) NOT NULL,
   CONSTRAINT temp_audit_history_pkey PRIMARY KEY (activity_date,activity_type,country,name,query_iteration,query_no,query_type,reporter_type,site,workflow_route_type)
)
;
CREATE TABLE "qtt_jbpm"."temp_audit_history_i"
(
   activity_date varchar(10) NOT NULL,
   activity_type varchar(50) NOT NULL,
   country varchar(50) NOT NULL,
   name varchar(76) NOT NULL,
   query_iteration numeric(19) NOT NULL,
   query_no varchar(40) NOT NULL,
   query_type varchar(60) NOT NULL,
   reporter_type varchar(50) NOT NULL,
   site varchar(50) NOT NULL,
   workflow_route_type varchar(12) NOT NULL,
   CONSTRAINT temp_audit_history_i_pkey PRIMARY KEY (activity_date,activity_type,country,name,query_iteration,query_no,query_type,reporter_type,site,workflow_route_type)
)
;
CREATE TABLE "qtt_jbpm"."temp_wp_audit_history"
(
   lu_date bytea NOT NULL,
   lu_id varchar(240) NOT NULL,
   query_iteration numeric(19) NOT NULL,
   query_seq numeric(19) NOT NULL,
   wf_process_seq numeric(19) NOT NULL,
   CONSTRAINT temp_wp_audit_history_pkey PRIMARY KEY (lu_date,lu_id,query_iteration,query_seq,wf_process_seq)
)
;
CREATE TABLE "qtt_jbpm"."temp_wp_audit_history_i"
(
   lu_date bytea NOT NULL,
   lu_id varchar(240) NOT NULL,
   query_iteration numeric(19) NOT NULL,
   query_seq numeric(19) NOT NULL,
   wf_process_seq numeric(19) NOT NULL,
   CONSTRAINT temp_wp_audit_history_i_pkey PRIMARY KEY (lu_date,lu_id,query_iteration,query_seq,wf_process_seq)
)
;
CREATE TABLE "qtt_jbpm"."vw_arisg_qtt_countries"
(
   country_code varchar(10) PRIMARY KEY NOT NULL,
   description varchar(50)
)
;
CREATE TABLE "qtt_jbpm"."vw_arisg_qtt_drugs"
(
   inn_generic_name varchar(120) NOT NULL,
   drug_preferred_name varchar(200) NOT NULL,
   CONSTRAINT vw_arisg_qtt_drugs_pkey PRIMARY KEY (inn_generic_name,drug_preferred_name)
)
;
CREATE TABLE "qtt_jbpm"."workflow_route_types"
(
   workflow_route_type_id varchar(2) PRIMARY KEY NOT NULL,
   workflow_route_type varchar(50) NOT NULL
)
;
ALTER TABLE "qtt_jbpm"."ae_terms"
ADD CONSTRAINT fk4917118c82df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
CREATE UNIQUE INDEX ae_terms_pkey ON "qtt_jbpm"."ae_terms"(query_seq)
;
ALTER TABLE "qtt_jbpm"."affiliate_notes"
ADD CONSTRAINT fk7d8af90f82df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
CREATE UNIQUE INDEX affiliate_notes_pkey ON "qtt_jbpm"."affiliate_notes"(note_seq)
;
CREATE UNIQUE INDEX attachment_categories_pkey ON "qtt_jbpm"."attachment_categories"(attachment_category_id)
;
ALTER TABLE "qtt_jbpm"."attachments"
ADD CONSTRAINT fk54475f9082df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
ALTER TABLE "qtt_jbpm"."attachments"
ADD CONSTRAINT fk54475f908ec41a05
FOREIGN KEY (attachment_category_id)
REFERENCES "qtt_jbpm"."attachment_categories"(attachment_category_id)
;
CREATE UNIQUE INDEX attachments_pkey ON "qtt_jbpm"."attachments"(attachment_seq)
;
CREATE UNIQUE INDEX audit_activity_pkey ON "qtt_jbpm"."audit_activity"(activity_type_id)
;
ALTER TABLE "qtt_jbpm"."audit_history"
ADD CONSTRAINT fke800cff04518b7c2
FOREIGN KEY (wf_process_seq)
REFERENCES "qtt_jbpm"."qtt_workflow_processes"(wf_process_seq)
;
ALTER TABLE "qtt_jbpm"."audit_history"
ADD CONSTRAINT fke800cff082df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
CREATE UNIQUE INDEX audit_history_pkey ON "qtt_jbpm"."audit_history"(audit_seq)
;
CREATE UNIQUE INDEX case_oformats_pkey ON "qtt_jbpm"."case_oformats"(case_oformat_id)
;
ALTER TABLE "qtt_jbpm"."cases"
ADD CONSTRAINT fk3cef32382df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
CREATE UNIQUE INDEX cases_pkey ON "qtt_jbpm"."cases"(case_seq)
;
ALTER TABLE "qtt_jbpm"."clinical_trials"
ADD CONSTRAINT fk3d7c2c982df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
ALTER TABLE "qtt_jbpm"."clinical_trials"
ADD CONSTRAINT fk3d7c2c91dfb0dff
FOREIGN KEY (ctrl_oformat_id)
REFERENCES "qtt_jbpm"."ctrl_oformats"(ctrl_oformat_id)
;
CREATE UNIQUE INDEX clinical_trials_pkey ON "qtt_jbpm"."clinical_trials"(query_seq)
;
CREATE UNIQUE INDEX ctrl_oformats_pkey ON "qtt_jbpm"."ctrl_oformats"(ctrl_oformat_id)
;
ALTER TABLE "qtt_jbpm"."drugs"
ADD CONSTRAINT fk3e4ccb382df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
CREATE UNIQUE INDEX drugs_pkey ON "qtt_jbpm"."drugs"(drug_number_seq)
;
ALTER TABLE "qtt_jbpm"."followup_attachments"
ADD CONSTRAINT fkf7f01ddd82df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
CREATE UNIQUE INDEX followup_attachments_pkey ON "qtt_jbpm"."followup_attachments"
(
  followup_seq,
  query_seq
)
;
CREATE UNIQUE INDEX fu_ae_terms_pkey ON "qtt_jbpm"."fu_ae_terms"
(
  followup_seq,
  query_seq
)
;
CREATE UNIQUE INDEX fu_cases_pkey ON "qtt_jbpm"."fu_cases"
(
  case_seq,
  followup_seq,
  query_seq
)
;
CREATE UNIQUE INDEX fu_clinical_trials_pkey ON "qtt_jbpm"."fu_clinical_trials"
(
  followup_seq,
  query_seq
)
;
CREATE UNIQUE INDEX fu_drugs_pkey ON "qtt_jbpm"."fu_drugs"
(
  drug_number_seq,
  followup_seq,
  query_seq
)
;
CREATE UNIQUE INDEX fu_labelling_documents_pkey ON "qtt_jbpm"."fu_labelling_documents"
(
  followup_seq,
  ldoc_type_id,
  query_seq
)
;
ALTER TABLE "qtt_jbpm"."fu_organisations"
ADD CONSTRAINT fk1ed5f7493e283c58
FOREIGN KEY (type_id)
REFERENCES "qtt_jbpm"."organisation_types"(type_id)
;
CREATE UNIQUE INDEX fu_organisations_pkey ON "qtt_jbpm"."fu_organisations"
(
  followup_seq,
  organisation_id
)
;
CREATE UNIQUE INDEX fu_performance_metrics_pkey ON "qtt_jbpm"."fu_performance_metrics"
(
  followup_seq,
  query_seq
)
;
CREATE UNIQUE INDEX fu_pre_query_preparations_pkey ON "qtt_jbpm"."fu_pre_query_preparations"
(
  followup_seq,
  pqp_type_id,
  query_seq
)
;
CREATE UNIQUE INDEX fu_pregnancies_pkey ON "qtt_jbpm"."fu_pregnancies"
(
  followup_seq,
  query_seq
)
;
ALTER TABLE "qtt_jbpm"."fu_queries"
ADD CONSTRAINT fke534d3d65ae4f181
FOREIGN KEY (reporter_type_id)
REFERENCES "qtt_jbpm"."reporter_types"(reporter_type_id)
;
ALTER TABLE "qtt_jbpm"."fu_queries"
ADD CONSTRAINT fke534d3d682df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
ALTER TABLE "qtt_jbpm"."fu_queries"
ADD CONSTRAINT fke534d3d670b57387
FOREIGN KEY (query_type_id)
REFERENCES "qtt_jbpm"."query_types"(query_type_id)
;
ALTER TABLE "qtt_jbpm"."fu_queries"
ADD CONSTRAINT fke534d3d6a979f223
FOREIGN KEY (source_country_code)
REFERENCES "qtt_jbpm"."vw_arisg_qtt_countries"(source_country_code)
;
CREATE UNIQUE INDEX fu_queries_pkey ON "qtt_jbpm"."fu_queries"
(
  followup_seq,
  query_seq
)
;
CREATE UNIQUE INDEX fu_query_descriptions_pkey ON "qtt_jbpm"."fu_query_descriptions"
(
  followup_seq,
  query_seq
)
;
ALTER TABLE "qtt_jbpm"."fu_requesters"
ADD CONSTRAINT fk25ce49e75b1dac7f
FOREIGN KEY (country_code)
REFERENCES "qtt_jbpm"."vw_arisg_qtt_countries"(country_code)
;
CREATE UNIQUE INDEX fu_requesters_pkey ON "qtt_jbpm"."fu_requesters"
(
  followup_seq,
  requester_id
)
;
CREATE UNIQUE INDEX fu_retrieval_periods_pkey ON "qtt_jbpm"."fu_retrieval_periods"
(
  followup_seq,
  query_seq
)
;
ALTER TABLE "qtt_jbpm"."labelling_documents"
ADD CONSTRAINT fk77380d6382df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
ALTER TABLE "qtt_jbpm"."labelling_documents"
ADD CONSTRAINT fk77380d63ea4babd7
FOREIGN KEY (ldoc_type_id)
REFERENCES "qtt_jbpm"."ldoc_types"(ldoc_type_id)
;
CREATE UNIQUE INDEX labelling_documents_pkey ON "qtt_jbpm"."labelling_documents"
(
  query_seq,
  ldoc_type_id
)
;
CREATE UNIQUE INDEX ldoc_types_pkey ON "qtt_jbpm"."ldoc_types"(ldoc_type_id)
;
ALTER TABLE "qtt_jbpm"."non_affiliate_notes"
ADD CONSTRAINT fk195c837d82df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
CREATE UNIQUE INDEX non_affiliate_notes_pkey ON "qtt_jbpm"."non_affiliate_notes"(note_seq)
;
CREATE UNIQUE INDEX non_roche_users_pkey ON "qtt_jbpm"."non_roche_users"(email)
;
CREATE UNIQUE INDEX notifications_pkey ON "qtt_jbpm"."notifications"
(
  expiry_date,
  query_seq
)
;
CREATE UNIQUE INDEX organisation_types_pkey ON "qtt_jbpm"."organisation_types"(type_id)
;
ALTER TABLE "qtt_jbpm"."organisations"
ADD CONSTRAINT fkc9cb32b93e283c58
FOREIGN KEY (type_id)
REFERENCES "qtt_jbpm"."organisation_types"(type_id)
;
CREATE UNIQUE INDEX organisations_pkey ON "qtt_jbpm"."organisations"(organisation_id)
;
ALTER TABLE "qtt_jbpm"."performance_metrics"
ADD CONSTRAINT fkbdcd877482df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
CREATE UNIQUE INDEX performance_metrics_pkey ON "qtt_jbpm"."performance_metrics"(query_seq)
;
CREATE UNIQUE INDEX pqp_types_pkey ON "qtt_jbpm"."pqp_types"(pqp_type_id)
;
ALTER TABLE "qtt_jbpm"."pre_query_preparations"
ADD CONSTRAINT fk7710322f82df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
ALTER TABLE "qtt_jbpm"."pre_query_preparations"
ADD CONSTRAINT fk7710322f73efeb95
FOREIGN KEY (pqp_type_id)
REFERENCES "qtt_jbpm"."pqp_types"(pqp_type_id)
;
CREATE UNIQUE INDEX pre_query_preparations_pkey ON "qtt_jbpm"."pre_query_preparations"
(
  pqp_type_id,
  query_seq
)
;
ALTER TABLE "qtt_jbpm"."pregnancies"
ADD CONSTRAINT fk1e5f3c6bff30767
FOREIGN KEY (exposure_type_id)
REFERENCES "qtt_jbpm"."prgy_exptypes"(exposure_type_id)
;
ALTER TABLE "qtt_jbpm"."pregnancies"
ADD CONSTRAINT fk1e5f3c6b82df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
CREATE UNIQUE INDEX pregnancies_pkey ON "qtt_jbpm"."pregnancies"(query_seq)
;
CREATE UNIQUE INDEX prgy_exptypes_pkey ON "qtt_jbpm"."prgy_exptypes"(exposure_type_id)
;
ALTER TABLE "qtt_jbpm"."qtt_workflow_processes"
ADD CONSTRAINT fke303f32be44658b6
FOREIGN KEY (activity_type_id)
REFERENCES "qtt_jbpm"."audit_activity"(activity_type_id)
;
CREATE UNIQUE INDEX qtt_workflow_processes_pkey ON "qtt_jbpm"."qtt_workflow_processes"(wf_process_seq)
;
ALTER TABLE "qtt_jbpm"."queries"
ADD CONSTRAINT fk51d7634670b57387
FOREIGN KEY (query_type_id)
REFERENCES "qtt_jbpm"."query_types"(query_type_id)
;
ALTER TABLE "qtt_jbpm"."queries"
ADD CONSTRAINT fk51d76346b4b2809d
FOREIGN KEY (status_code)
REFERENCES "qtt_jbpm"."query_status_codes"(status_code)
;
ALTER TABLE "qtt_jbpm"."queries"
ADD CONSTRAINT fk51d76346a979f223
FOREIGN KEY (source_country_code)
REFERENCES "qtt_jbpm"."vw_arisg_qtt_countries"(source_country_code)
;
ALTER TABLE "qtt_jbpm"."queries"
ADD CONSTRAINT fk51d7634692c213d6
FOREIGN KEY (requester_id)
REFERENCES "qtt_jbpm"."requesters"(requester_id)
;
ALTER TABLE "qtt_jbpm"."queries"
ADD CONSTRAINT fk51d76346aed567b5
FOREIGN KEY (case_oformat_id)
REFERENCES "qtt_jbpm"."case_oformats"(case_oformat_id)
;
ALTER TABLE "qtt_jbpm"."queries"
ADD CONSTRAINT fk51d763465ae4f181
FOREIGN KEY (reporter_type_id)
REFERENCES "qtt_jbpm"."reporter_types"(reporter_type_id)
;
ALTER TABLE "qtt_jbpm"."queries"
ADD CONSTRAINT fk51d763461ffbcbf8
FOREIGN KEY (process_instance)
REFERENCES "qtt_jbpm"."jbpm_processinstance"(process_instance)
;
CREATE UNIQUE INDEX queries_query_number_key ON "qtt_jbpm"."queries"(query_number)
;
CREATE UNIQUE INDEX queries_pkey ON "qtt_jbpm"."queries"(query_seq)
;
ALTER TABLE "qtt_jbpm"."query_descriptions"
ADD CONSTRAINT fkaf7c7e2e82df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
CREATE UNIQUE INDEX query_descriptions_pkey ON "qtt_jbpm"."query_descriptions"(query_seq)
;
CREATE UNIQUE INDEX query_status_codes_pkey ON "qtt_jbpm"."query_status_codes"(status_code)
;
CREATE UNIQUE INDEX query_types_pkey ON "qtt_jbpm"."query_types"(query_type_id)
;
CREATE UNIQUE INDEX reporter_types_pkey ON "qtt_jbpm"."reporter_types"(reporter_type_id)
;
ALTER TABLE "qtt_jbpm"."requesters"
ADD CONSTRAINT fk1f2044775b1dac7f
FOREIGN KEY (country_code)
REFERENCES "qtt_jbpm"."vw_arisg_qtt_countries"(country_code)
;
ALTER TABLE "qtt_jbpm"."requesters"
ADD CONSTRAINT fk1f204477cba0f25e
FOREIGN KEY (organisation_id)
REFERENCES "qtt_jbpm"."organisations"(organisation_id)
;
CREATE UNIQUE INDEX requesters_pkey ON "qtt_jbpm"."requesters"(requester_id)
;
ALTER TABLE "qtt_jbpm"."retrieval_periods"
ADD CONSTRAINT fk2cb2f88782df4acc
FOREIGN KEY (query_seq)
REFERENCES "qtt_jbpm"."queries"(query_seq)
;
CREATE UNIQUE INDEX retrieval_periods_pkey ON "qtt_jbpm"."retrieval_periods"(query_seq)
;
CREATE UNIQUE INDEX temp_audit_history_pkey ON "qtt_jbpm"."temp_audit_history"
(
  activity_date,
  activity_type,
  country,
  name,
  query_iteration,
  query_no,
  query_type,
  reporter_type,
  site,
  workflow_route_type
)
;
CREATE UNIQUE INDEX temp_audit_history_i_pkey ON "qtt_jbpm"."temp_audit_history_i"
(
  activity_date,
  activity_type,
  country,
  name,
  query_iteration,
  query_no,
  query_type,
  reporter_type,
  site,
  workflow_route_type
)
;
CREATE UNIQUE INDEX temp_wp_audit_history_pkey ON "qtt_jbpm"."temp_wp_audit_history"
(
  lu_date,
  lu_id,
  query_iteration,
  query_seq,
  wf_process_seq
)
;
CREATE UNIQUE INDEX temp_wp_audit_history_i_pkey ON "qtt_jbpm"."temp_wp_audit_history_i"
(
  lu_date,
  lu_id,
  query_iteration,
  query_seq,
  wf_process_seq
)
;
CREATE UNIQUE INDEX vw_arisg_qtt_countries_pkey ON "qtt_jbpm"."vw_arisg_qtt_countries"(country_code)
;
CREATE UNIQUE INDEX vw_arisg_qtt_drugs_pkey ON "qtt_jbpm"."vw_arisg_qtt_drugs"
(
  inn_generic_name,
  drug_preferred_name
)
;
CREATE UNIQUE INDEX workflow_route_types_pkey ON "qtt_jbpm"."workflow_route_types"(workflow_route_type_id)
;

