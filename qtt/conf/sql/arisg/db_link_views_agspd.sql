-- DEVELOPMENT

-- create database link

create  database link "ARISD1.BAS.ROCHE.COM"
connect to DATAMART_QTT
identified by DATAMART_QTT
using '
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = arisd1.bas.roche.com)(PORT = 14014))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = arisd1.bas.roche.com)
    )
  )
'
; 
-- create views

create or replace view vw_arisg_qtt_countries as
select distinct aris_code, country_code, description
    from vw_arisg_qtt_countries@ARISD1.BAS.ROCHE.COM order by 3;
		
create or replace view vw_arisg_qtt_drugs as
select distinct drug_preferred_name, inn_generic_name
	from vw_arisg_qtt_drugs@ARISD1.BAS.ROCHE.COM;		

------ check

--select * from dual@ARISD1.BAS.ROCHE.COM; 
--select * from vw_arisg_qtt_countries;
--select * from  vw_arisg_qtt_drugs;
	
	
	
	
	
