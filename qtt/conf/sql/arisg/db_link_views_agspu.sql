-- UAT

-- create database link

create  database link "ARISTST9.KAU.ROCHE.COM"
connect to DATAMART_QTT
identified by DATAMART_QTT
using 
  '
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = veen3.kau.roche.com)(PORT = 15023))
    (CONNECT_DATA =
      (SERVICE_NAME = ARISTST9.kau.roche.com)
    )
  )
' ;

-- create views

create or replace view vw_arisg_qtt_countries as
select distinct aris_code, country_code, description
    from vw_arisg_qtt_countries@ARISTST9.KAU.ROCHE.COM order by 3;
    
create or replace view vw_arisg_qtt_drugs as
select distinct drug_preferred_name, inn_generic_name
  from vw_arisg_qtt_drugs@ARISTST9.KAU.ROCHE.COM;    

------ check

-- select * from dual@ARISTST9.KAU.ROCHE.COM
-- select * from vw_arisg_qtt_countries
-- select * from  vw_arisg_qtt_drugs
  
