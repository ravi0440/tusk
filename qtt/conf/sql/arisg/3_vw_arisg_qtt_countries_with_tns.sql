create database link "ARISD.BAS.ROCHE.COM"
connect to SHARP_READ
identified by SHARP_READ
using '(DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = arisd.bas.roche.com)(PORT = 15001))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = arisd.bas.roche.com)))'