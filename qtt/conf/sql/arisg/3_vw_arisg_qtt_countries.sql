CREATE OR REPLACE VIEW VW_ARISG_QTT_COUNTRIES AS
SELECT DISTINCT cd.code aris_code, ecd.e2b_code country_code,
                   cd.DECODE description
              FROM arisg7.codelist_decode@&db_name cd,
                   arisg7.e2b_code_decode@&db_name ecd
             WHERE cd.language_code = '001'
               AND cd.codelist_id = '1'
               AND ecd.e2b_codelist_id = '1'
               AND cd.codelist_id = ecd.e2b_codelist_id
               AND cd.decode = ecd.e2b_decode
	UNION
SELECT aris_code, country_code, description FROM qtt_countries_lu
          ORDER BY 3
