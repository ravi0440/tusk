update queries set source_country_code = 'EG' where source_country_code = 'ET';
update requesters set country_code = 'EG' where country_code = 'ET';

update queries set source_country_code = 'BZ' where source_country_code = 'BH';
update requesters set country_code = 'BZ' where country_code = 'BH';

update queries set source_country_code = 'CM' where source_country_code = 'TC';
update requesters set country_code = 'CM' where country_code = 'TC';

update queries set source_country_code = 'SV' where source_country_code = 'ES';
update requesters set country_code = 'SV' where country_code = 'ES';

update queries set source_country_code = 'LR' where source_country_code = 'LB';
update requesters set country_code = 'LR' where country_code = 'LB';

update queries set source_country_code = 'LK' where source_country_code = 'CL';
update requesters set country_code = 'LK' where country_code = 'CL';

update queries set source_country_code = 'MU' where source_country_code = 'MS';
update requesters set country_code = 'MU' where country_code = 'MS';

update queries set source_country_code = 'SZ' where source_country_code = 'SD';
update requesters set country_code = 'SZ' where country_code = 'SD';

-- Added by TSM 27 April 2011

update queries set source_country_code = 'US' where source_country_code = 'USA';
update requesters set country_code = 'US' where country_code = 'USA';

update queries set source_country_code = 'VE' where source_country_code = 'YV';
update requesters set country_code = 'VE' where country_code = 'YV';

update queries set source_country_code = 'UM' where source_country_code = 'UMI';
update requesters set country_code = 'UM' where country_code = 'UMI';

update queries set source_country_code = 'TH' where source_country_code = 'T';
update requesters set country_code = 'TH' where country_code = 'T';

update queries set source_country_code = 'TW' where source_country_code = 'RCT';
update requesters set country_code = 'TW' where country_code = 'RCT';

update queries set source_country_code = 'SY' where source_country_code = 'SYR';
update requesters set country_code = 'SY' where country_code = 'SYR';

update queries set source_country_code = 'SE' where source_country_code = 'S';
update requesters set country_code = 'SE' where country_code = 'S';

update queries set source_country_code = 'ES' where source_country_code = 'E';
update requesters set country_code = 'ES' where country_code = 'E';

update queries set source_country_code = 'GS' where source_country_code = 'SGS';
update requesters set country_code = 'GS' where country_code = 'SGS';

update queries set source_country_code = 'SB' where source_country_code = 'SLB';
update requesters set country_code = 'SB' where country_code = 'SLB';

update queries set source_country_code = 'SG' where source_country_code = 'SGP';
update requesters set country_code = 'SG' where country_code = 'SGP';

update queries set source_country_code = 'SA' where source_country_code = 'KSA';
update requesters set country_code = 'SA' where country_code = 'KSA';

update queries set source_country_code = 'KN' where source_country_code = 'KNA';
update requesters set country_code = 'KN' where country_code = 'KNA';

update queries set source_country_code = 'RO' where source_country_code = 'R';
update requesters set country_code = 'RO' where country_code = 'R';

update queries set source_country_code = 'QA' where source_country_code = 'Q';
update requesters set country_code = 'QA' where country_code = 'Q';

update queries set source_country_code = 'PT' where source_country_code = 'P';
update requesters set country_code = 'PT' where country_code = 'P';

update queries set source_country_code = 'PH' where source_country_code = 'PI';
update requesters set country_code = 'PH' where country_code = 'PI';

update queries set source_country_code = 'PK' where source_country_code = 'PAK';
update requesters set country_code = 'PK' where country_code = 'PAK';

update queries set source_country_code = 'NO' where source_country_code = 'N';
update requesters set country_code = 'NO' where country_code = 'N';

update queries set source_country_code = 'NG' where source_country_code = 'WAN';
update requesters set country_code = 'NG' where country_code = 'WAN';

update queries set source_country_code = 'NI' where source_country_code = 'NIC';
update requesters set country_code = 'NI' where country_code = 'NIC';

update queries set source_country_code = 'NP' where source_country_code = 'NEP';
update requesters set country_code = 'NP' where country_code = 'NEP';

update queries set source_country_code = 'MM' where source_country_code = 'BUR';
update requesters set country_code = 'MM' where country_code = 'BUR';

update queries set source_country_code = 'MX' where source_country_code = 'MEX';
update requesters set country_code = 'MX' where country_code = 'MEX';

update queries set source_country_code = 'MY' where source_country_code = 'MAL';
update requesters set country_code = 'MY' where country_code = 'MAL';

update queries set source_country_code = 'MO' where source_country_code = 'MAC';
update requesters set country_code = 'MO' where country_code = 'MAC';

update queries set source_country_code = 'LU' where source_country_code = 'L';
update requesters set country_code = 'LU' where country_code = 'L';

update queries set source_country_code = 'LB' where source_country_code = 'RL';
update requesters set country_code = 'LB' where country_code = 'RL';

update queries set source_country_code = 'KW' where source_country_code = 'KWT';
update requesters set country_code = 'KW' where country_code = 'KWT';

update queries set source_country_code = 'KE' where source_country_code = 'EAK';
update requesters set country_code = 'KE' where country_code = 'EAK';

update queries set source_country_code = 'JO' where source_country_code = 'HKJ';
update requesters set country_code = 'JO' where country_code = 'HKJ';

update queries set source_country_code = 'JP' where source_country_code = 'J';
update requesters set country_code = 'JP' where country_code = 'J';

update queries set source_country_code = 'JM' where source_country_code = 'JA';
update requesters set country_code = 'JM' where country_code = 'JA';

update queries set source_country_code = 'IT' where source_country_code = 'I';
update requesters set country_code = 'IT' where country_code = 'I';

update queries set source_country_code = 'IE' where source_country_code = 'IRL';
update requesters set country_code = 'IE' where country_code = 'IRL';

update queries set source_country_code = 'IN' where source_country_code = 'IND';
update requesters set country_code = 'IN' where country_code = 'IND';

update queries set source_country_code = 'HU' where source_country_code = 'H';
update requesters set country_code = 'HU' where country_code = 'H';

update queries set source_country_code = 'HN' where source_country_code = 'HON';
update requesters set country_code = 'HN' where country_code = 'HON';

update queries set source_country_code = 'DE' where source_country_code = 'D';
update requesters set country_code = 'DE' where country_code = 'D';

update queries set source_country_code = 'GE' where source_country_code = 'GZ';
update requesters set country_code = 'GE' where country_code = 'GZ';

update queries set source_country_code = 'GM' where source_country_code = 'WAG';
update requesters set country_code = 'GM' where country_code = 'WAG';

update queries set source_country_code = 'FI' where source_country_code = 'SF';
update requesters set country_code = 'FI' where country_code = 'SF';

update queries set source_country_code = 'DO' where source_country_code = 'DOM';
update requesters set country_code = 'DO' where country_code = 'DOM';

update queries set source_country_code = 'CZ' where source_country_code = 'CS_OLD';
update requesters set country_code = 'CZ' where country_code = 'CS_OLD';

update queries set source_country_code = 'CU' where source_country_code = 'C';
update requesters set country_code = 'CU' where country_code = 'C';

update queries set source_country_code = 'CL' where source_country_code = 'RCH';
update requesters set country_code = 'CL' where country_code = 'RCH';

update queries set source_country_code = 'CA' where source_country_code = 'CDN';
update requesters set country_code = 'CA' where country_code = 'CDN';

update queries set source_country_code = 'BJ' where source_country_code = 'DY';
update requesters set country_code = 'BJ' where country_code = 'DY';

update queries set source_country_code = 'BE' where source_country_code = 'B';
update requesters set country_code = 'BE' where country_code = 'B';

update queries set source_country_code = 'BB' where source_country_code = 'BDS';
update requesters set country_code = 'BB' where country_code = 'BDS';

update queries set source_country_code = 'AT' where source_country_code = 'A';
update requesters set country_code = 'AT' where country_code = 'A';

update queries set source_country_code = 'AU' where source_country_code = 'AUS';
update requesters set country_code = 'AU' where country_code = 'AUS';

update queries set source_country_code = 'AR' where source_country_code = 'RA';
update requesters set country_code = 'AR' where country_code = 'RA';

update queries set source_country_code = 'AF' where source_country_code = 'AFG';
update requesters set country_code = 'AF' where country_code = 'AFG';

update queries set source_country_code = 'TC' where source_country_code = 'TCA';
update requesters set country_code = 'TC' where country_code = 'TCA';

update queries set source_country_code = 'IQ' where source_country_code = 'IRQ';
update requesters set country_code = 'IQ' where country_code = 'IRQ';

update queries set source_country_code = 'FR' where source_country_code = 'F';
update requesters set country_code = 'FR' where country_code = 'F';

update queries set source_country_code = 'ID' where source_country_code = 'RI';
update requesters set country_code = 'ID' where country_code = 'RI';

update queries set source_country_code = 'UG' where source_country_code = 'EAU';
update requesters set country_code = 'UG' where country_code = 'EAU';

-- Based on Advent migration mapping

update queries set source_country_code = 'KR' where source_country_code = 'KOR';
update requesters set country_code = 'KR' where country_code = 'KOR';

update queries set source_country_code = 'CN' where source_country_code = 'PRC';
update requesters set country_code = 'CN' where country_code = 'PRC';


-- the same for archive data - fu~ tables

update fu_queries set source_country_code = 'EG' where source_country_code = 'ET';
update fu_requesters set country_code = 'EG' where country_code = 'ET';

update fu_queries set source_country_code = 'BZ' where source_country_code = 'BH';
update fu_requesters set country_code = 'BZ' where country_code = 'BH';

update fu_queries set source_country_code = 'CM' where source_country_code = 'TC';
update fu_requesters set country_code = 'CM' where country_code = 'TC';

update fu_queries set source_country_code = 'SV' where source_country_code = 'ES';
update fu_requesters set country_code = 'SV' where country_code = 'ES';

update fu_queries set source_country_code = 'LR' where source_country_code = 'LB';
update fu_requesters set country_code = 'LR' where country_code = 'LB';

update fu_queries set source_country_code = 'LK' where source_country_code = 'CL';
update fu_requesters set country_code = 'LK' where country_code = 'CL';

update fu_queries set source_country_code = 'MU' where source_country_code = 'MS';
update fu_requesters set country_code = 'MU' where country_code = 'MS';

update fu_queries set source_country_code = 'SZ' where source_country_code = 'SD';
update fu_requesters set country_code = 'SZ' where country_code = 'SD';

-- Added by TSM 27 April 2011

update fu_queries set source_country_code = 'US' where source_country_code = 'USA';
update fu_requesters set country_code = 'US' where country_code = 'USA';

update fu_queries set source_country_code = 'VE' where source_country_code = 'YV';
update fu_requesters set country_code = 'VE' where country_code = 'YV';

update fu_queries set source_country_code = 'UM' where source_country_code = 'UMI';
update fu_requesters set country_code = 'UM' where country_code = 'UMI';

update fu_queries set source_country_code = 'TH' where source_country_code = 'T';
update fu_requesters set country_code = 'TH' where country_code = 'T';

update fu_queries set source_country_code = 'TW' where source_country_code = 'RCT';
update fu_requesters set country_code = 'TW' where country_code = 'RCT';

update fu_queries set source_country_code = 'SY' where source_country_code = 'SYR';
update fu_requesters set country_code = 'SY' where country_code = 'SYR';

update fu_queries set source_country_code = 'SE' where source_country_code = 'S';
update fu_requesters set country_code = 'SE' where country_code = 'S';

update fu_queries set source_country_code = 'ES' where source_country_code = 'E';
update fu_requesters set country_code = 'ES' where country_code = 'E';

update fu_queries set source_country_code = 'GS' where source_country_code = 'SGS';
update fu_requesters set country_code = 'GS' where country_code = 'SGS';

update fu_queries set source_country_code = 'SB' where source_country_code = 'SLB';
update fu_requesters set country_code = 'SB' where country_code = 'SLB';

update fu_queries set source_country_code = 'SG' where source_country_code = 'SGP';
update fu_requesters set country_code = 'SG' where country_code = 'SGP';

update fu_queries set source_country_code = 'SA' where source_country_code = 'KSA';
update fu_requesters set country_code = 'SA' where country_code = 'KSA';

update fu_queries set source_country_code = 'KN' where source_country_code = 'KNA';
update fu_requesters set country_code = 'KN' where country_code = 'KNA';

update fu_queries set source_country_code = 'RO' where source_country_code = 'R';
update fu_requesters set country_code = 'RO' where country_code = 'R';

update fu_queries set source_country_code = 'QA' where source_country_code = 'Q';
update fu_requesters set country_code = 'QA' where country_code = 'Q';

update fu_queries set source_country_code = 'PT' where source_country_code = 'P';
update fu_requesters set country_code = 'PT' where country_code = 'P';

update fu_queries set source_country_code = 'PH' where source_country_code = 'PI';
update fu_requesters set country_code = 'PH' where country_code = 'PI';

update fu_queries set source_country_code = 'PK' where source_country_code = 'PAK';
update fu_requesters set country_code = 'PK' where country_code = 'PAK';

update fu_queries set source_country_code = 'NO' where source_country_code = 'N';
update fu_requesters set country_code = 'NO' where country_code = 'N';

update fu_queries set source_country_code = 'NG' where source_country_code = 'WAN';
update fu_requesters set country_code = 'NG' where country_code = 'WAN';

update fu_queries set source_country_code = 'NI' where source_country_code = 'NIC';
update fu_requesters set country_code = 'NI' where country_code = 'NIC';

update fu_queries set source_country_code = 'NP' where source_country_code = 'NEP';
update fu_requesters set country_code = 'NP' where country_code = 'NEP';

update fu_queries set source_country_code = 'MM' where source_country_code = 'BUR';
update fu_requesters set country_code = 'MM' where country_code = 'BUR';

update fu_queries set source_country_code = 'MX' where source_country_code = 'MEX';
update fu_requesters set country_code = 'MX' where country_code = 'MEX';

update fu_queries set source_country_code = 'MY' where source_country_code = 'MAL';
update fu_requesters set country_code = 'MY' where country_code = 'MAL';

update fu_queries set source_country_code = 'MO' where source_country_code = 'MAC';
update fu_requesters set country_code = 'MO' where country_code = 'MAC';

update fu_queries set source_country_code = 'LU' where source_country_code = 'L';
update fu_requesters set country_code = 'LU' where country_code = 'L';

update fu_queries set source_country_code = 'LB' where source_country_code = 'RL';
update fu_requesters set country_code = 'LB' where country_code = 'RL';

update fu_queries set source_country_code = 'KW' where source_country_code = 'KWT';
update fu_requesters set country_code = 'KW' where country_code = 'KWT';

update fu_queries set source_country_code = 'KE' where source_country_code = 'EAK';
update fu_requesters set country_code = 'KE' where country_code = 'EAK';

update fu_queries set source_country_code = 'JO' where source_country_code = 'HKJ';
update fu_requesters set country_code = 'JO' where country_code = 'HKJ';

update fu_queries set source_country_code = 'JP' where source_country_code = 'J';
update fu_requesters set country_code = 'JP' where country_code = 'J';

update fu_queries set source_country_code = 'JM' where source_country_code = 'JA';
update fu_requesters set country_code = 'JM' where country_code = 'JA';

update fu_queries set source_country_code = 'IT' where source_country_code = 'I';
update fu_requesters set country_code = 'IT' where country_code = 'I';

update fu_queries set source_country_code = 'IE' where source_country_code = 'IRL';
update fu_requesters set country_code = 'IE' where country_code = 'IRL';

update fu_queries set source_country_code = 'IN' where source_country_code = 'IND';
update fu_requesters set country_code = 'IN' where country_code = 'IND';

update fu_queries set source_country_code = 'HU' where source_country_code = 'H';
update fu_requesters set country_code = 'HU' where country_code = 'H';

update fu_queries set source_country_code = 'HN' where source_country_code = 'HON';
update fu_requesters set country_code = 'HN' where country_code = 'HON';

update fu_queries set source_country_code = 'DE' where source_country_code = 'D';
update fu_requesters set country_code = 'DE' where country_code = 'D';

update fu_queries set source_country_code = 'GE' where source_country_code = 'GZ';
update fu_requesters set country_code = 'GE' where country_code = 'GZ';

update fu_queries set source_country_code = 'GM' where source_country_code = 'WAG';
update fu_requesters set country_code = 'GM' where country_code = 'WAG';

update fu_queries set source_country_code = 'FI' where source_country_code = 'SF';
update fu_requesters set country_code = 'FI' where country_code = 'SF';

update fu_queries set source_country_code = 'DO' where source_country_code = 'DOM';
update fu_requesters set country_code = 'DO' where country_code = 'DOM';

update fu_queries set source_country_code = 'CZ' where source_country_code = 'CS_OLD';
update fu_requesters set country_code = 'CZ' where country_code = 'CS_OLD';

update fu_queries set source_country_code = 'CU' where source_country_code = 'C';
update fu_requesters set country_code = 'CU' where country_code = 'C';

update fu_queries set source_country_code = 'CL' where source_country_code = 'RCH';
update fu_requesters set country_code = 'CL' where country_code = 'RCH';

update fu_queries set source_country_code = 'CA' where source_country_code = 'CDN';
update fu_requesters set country_code = 'CA' where country_code = 'CDN';

update fu_queries set source_country_code = 'BJ' where source_country_code = 'DY';
update fu_requesters set country_code = 'BJ' where country_code = 'DY';

update fu_queries set source_country_code = 'BE' where source_country_code = 'B';
update fu_requesters set country_code = 'BE' where country_code = 'B';

update fu_queries set source_country_code = 'BB' where source_country_code = 'BDS';
update fu_requesters set country_code = 'BB' where country_code = 'BDS';

update fu_queries set source_country_code = 'AT' where source_country_code = 'A';
update fu_requesters set country_code = 'AT' where country_code = 'A';

update fu_queries set source_country_code = 'AU' where source_country_code = 'AUS';
update fu_requesters set country_code = 'AU' where country_code = 'AUS';

update fu_queries set source_country_code = 'AR' where source_country_code = 'RA';
update fu_requesters set country_code = 'AR' where country_code = 'RA';

update fu_queries set source_country_code = 'AF' where source_country_code = 'AFG';
update fu_requesters set country_code = 'AF' where country_code = 'AFG';

update fu_queries set source_country_code = 'TC' where source_country_code = 'TCA';
update fu_requesters set country_code = 'TC' where country_code = 'TCA';

update fu_queries set source_country_code = 'IQ' where source_country_code = 'IRQ';
update fu_requesters set country_code = 'IQ' where country_code = 'IRQ';

update fu_queries set source_country_code = 'FR' where source_country_code = 'F';
update fu_requesters set country_code = 'FR' where country_code = 'F';

update fu_queries set source_country_code = 'ID' where source_country_code = 'RI';
update fu_requesters set country_code = 'ID' where country_code = 'RI';

update fu_queries set source_country_code = 'UG' where source_country_code = 'EAU';
update fu_requesters set country_code = 'UG' where country_code = 'EAU';

-- Based on Advent migration mapping

update fu_queries set source_country_code = 'KR' where source_country_code = 'KOR';
update fu_requesters set country_code = 'KR' where country_code = 'KOR';

update fu_queries set source_country_code = 'CN' where source_country_code = 'PRC';
update fu_requesters set country_code = 'CN' where country_code = 'PRC';
commit;


CREATE OR REPLACE VIEW QTT_COUNTRIES AS
	SELECT DISTINCT aris_code, country_code, description
    FROM vw_arisg_qtt_countries@aristst4.kau.roche.com order by 3;

    
create or replace view qtt_drugs as
	select distinct drug_preferred_name, inn_generic_name
	from vw_arisg_qtt_drugs@aristst4.kau.roche.com; 


          
