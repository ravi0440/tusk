---- QTT Missing country codes fix

-- 1 ------------ Create table qtt_countries_lu

create table qtt_countries_lu
(
  aris_code VARCHAR2(3) not null,
  country_code            VARCHAR2(6),
  description        VARCHAR2(60) not null
)
tablespace APP_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 200K
    minextents 1
    maxextents unlimited
  );

		
-- 2 load data

-- Get data from  advp1 ()
/*
select 'Insert into qtt_countries_lu (aris_code, country_code, description) values (''000'', ''' ||
							country_code || ''',''' || description || ''');'
		from countries_lu
	where country_code in
							('A', 'AUS', 'B', 'CDN', 'DDR', 'E', 'GUS', 'GZ', 'H', 'HKJ', 'HR', 'I',
								'IND', 'IRL', 'J', 'JA', 'KOR', 'KSA', 'MAL', 'MEX', 'N', 'P', 'PI',
								'PRC', 'R', 'RA', 'RCH', 'RCL', 'RL', 'S', 'SF', 'SGP', 'SK', 'SU',
								'SYR', 'T', 'UMI', 'UNK', 'USA', 'WAG', 'YV', 'AFG', 'BA', 'BDS',
								'BUR', 'C', 'CS_OLD', 'DOM', 'DY', 'EAK', 'HON', 'KNA', 'KWT', 'L',
								'LIT', 'MAC', 'N', 'NEP', 'NIC', 'OTH', 'PAK', 'Q', 'RCT', 'SGS', 'SK',
								'SLB', 'F', 'D', 'WAN');
*/								
--- Load	qtt_countries_lu	tableInsert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'AT','AUSTRIA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'A','AUSTRIA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'AFG','AFGHANISTAN');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'AUS','AUSTRALIA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'B','BELGIUM');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'BA','BOSNIA-HERZEGOVINA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'BDS','BARBADOS');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'BUR','MYANMAR (BURMA)');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'C','CUBA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'CDN','CANADA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'CS_OLD','CZECHOSLOVAKIA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'D','GERMANY');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'DDR','GERMANY EAST');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'DOM','DOMINICAN REPUBLIC');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'DY','BENIN');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'E','SPAIN');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'EAK','KENYA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'F','FRANCE');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'GUS','UNION OF INDEPENDENT STATES');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'GZ','GEORGIA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'H','HUNGARY');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'HKJ','JORDAN');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'HON','HONDURAS');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'HR','CROATIA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'I','ITALY');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'IND','INDIA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'IRL','IRELAND');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'J','JAPAN');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'JA','JAMAICA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'KNA','SAINT KITTS AND NEVIS');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'KOR','SOUTH KOREA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'KSA','SAUDI ARABIA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'KWT','KUWAIT');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'L','LUXEMBOURG');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'LIT','LITERATURE');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'MAC','MACAU');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'MAL','MALAYSIA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'MEX','MEXICO');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'N','NORWAY');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'NEP','NEPAL');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'NIC','NICARAGUA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'OTH','OTHER');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'P','PORTUGAL');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'PAK','PAKISTAN');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'PI','PHILIPPINES');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'PRC','PEOPLES REPUBLIC OF CHINA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'Q','QATAR');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'R','ROMANIA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'RA','ARGENTINA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'RCH','CHILE');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'RCT','TAIWAN');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'RL','LEBANON');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'S','SWEDEN');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'SF','FINLAND');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'SGP','SINGAPORE');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'SGS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'SK','SLOVAK REPUBLIC');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'SLB','SOLOMON ISLANDS');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'SU','U.S.S.R');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'SYR','SYRIA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'T','THAILAND');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'UMI','UNITED STATES MINOR OUTLYING ISLANDS');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'UNK','UNKNOWN');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'USA','UNITED STATES OF AMERICA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'WAG','GAMBIA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'YU','YUGOSLAVIA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'YV','VENEZUELA');
Insert into qtt_countries_lu (aris_code, country_code, description) values ('000', 'WAN','NIGERIA');
commit;


-- 3 - update queries

update queries set source_country_code = 'EG' where source_country_code = 'ET';
update requesters set country_code = 'EG' where country_code = 'ET';
update queries set source_country_code = 'BZ' where source_country_code = 'BH';
update requesters set country_code = 'BZ' where country_code = 'BH';
update queries set source_country_code = 'CM' where source_country_code = 'TC';
update requesters set country_code = 'CM' where country_code = 'TC';
update queries set source_country_code = 'SV' where source_country_code = 'ES';
update requesters set country_code = 'SV' where country_code = 'ES';
update queries set source_country_code = 'LR' where source_country_code = 'LB';
update requesters set country_code = 'LR' where country_code = 'LB';
update queries set source_country_code = 'LK' where source_country_code = 'CL';
update requesters set country_code = 'LK' where country_code = 'CL';
update queries set source_country_code = 'MU' where source_country_code = 'MS';
update requesters set country_code = 'MU' where country_code = 'MS';
update queries set source_country_code = 'SZ' where source_country_code = 'SD';
update requesters set country_code = 'SZ' where country_code = 'SD';
commit;

