create or replace view vw_arisg_qtt_drugs as
select distinct DRUG_RETRIEVAL_NAME   drug_preferred_name,

                ACTIVE_INGREDIENTS    inn_generic_name

  from DMG_MART.XT_RETRIEVAL_NAME_VW@&db_name

order by 1, 2 
