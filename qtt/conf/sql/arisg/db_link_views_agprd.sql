CREATE DATABASE LINK "ARISPRD2.KAU.ROCHE.COM" 
CONNECT TO DATAMART_QTT IDENTIFIED BY 
USING '(DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = arisprd2.kau.roche.com)(PORT = 17500))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = arisprd2.kau.roche.com)))';
      
      
create or replace view vw_arisg_qtt_countries as      
select distinct aris_code, country_code, description
    from vw_arisg_qtt_countries@arisprd2.kau.roche.com order by 3
 
create or replace view vw_arisg_qtt_drugs as    
select distinct drug_preferred_name, inn_generic_name
	from vw_arisg_qtt_drugs@arisprd2.kau.roche.com    
