set define off
spool repair_DSCL_6390.txt
SET SERVEROUTPUT ON

DECLARE
  match_count INTEGER := 0;
  i INTEGER := 0;
  v_data_type VARCHAR2(255) :='VARCHAR2';
  type array_t is varray(349) of varchar2(10);
  rows_updated number := 0;
  rows_updated_total number := 0;
  rows_scn_min number := 0;
  rows_scn_min_loc number := 0;
  rows_scn_max number := 0;
  rows_scn_max_loc number := 0;
  out_message VARCHAR2(1024);
  start_time number := 0;
  total_time number := 0;
  array array_t := array_t(
            '&nbsp;','&#160;',
            '&iexcl;','&#161;',
            '&cent;','&#162;',
            '&pound;','&#163;',
            '&curren;','&#164;',
            '&yen;','&#165;',
            '&brvbar;','&#166;',
            '&sect;','&#167;',
            '&uml;','&#168;',
            '&copy;','&#169;',
            '&ordf;','&#170;',
            '&laquo;','&#171;',
            '&not;','&#172;',
            '&shy;','&#173;',
            '&reg;','&#174;',
            '&macr;','&#175;',
            '&deg;','&#176;',
            '&plusmn;','&#177;',
            '&sup2;','&#178;',
            '&sup3;','&#179;',
            '&acute;','&#180;',
            '&micro;','&#181;',
            '&para;','&#182;',
            '&cedil;','&#184;',
            '&sup1;','&#185;',
            '&ordm;','&#186;',
            '&raquo;','&#187;',
            '&frac14;','&#188;',
            '&frac12;','&#189;',
            '&frac34;','&#190;',
            '&iquest;','&#191;',
            '&times;','&#215;',
            '&divide;','&#247;',
            '&forall;','&#8704;',
            '&part;','&#8706;',
            '&exist;','&#8707;',
            '&empty;','&#8709;',
            '&nabla;','&#8711;',
            '&isin;','&#8712;',
            '&notin;','&#8713;',
            '&ni;','&#8715;',
            '&prod;','&#8719;',
            '&sum;','&#8721;',
            '&minus;','&#8722;',
            '&lowast;','&#8727;',
            '&radic;','&#8730;',
            '&prop;','&#8733;',
            '&infin;','&#8734;',
            '&ang;','&#8736;',
            '&and;','&#8743;',
            '&or;','&#8744;',
            '&cap;','&#8745;',
            '&cup;','&#8746;',
            '&int;','&#8747;',
            '&there4;','&#8756;',
            '&sim;','&#8764;',
            '&cong;','&#8773;',
            '&asymp;','&#8776;',
            '&ne;','&#8800;',
            '&equiv;','&#8801;',
            '&le;','&#8804;',
            '&ge;','&#8805;',
            '&sub;','&#8834;',
            '&sup;','&#8835;',
            '&nsub;','&#8836;',
            '&sube;','&#8838;',
            '&supe;','&#8839;',
            '&oplus;','&#8853;',
            '&otimes;','&#8855;',
            '&perp;','&#8869;',
            '&sdot;','&#8901;',
            '&Alpha;','&#913;',
            '&Beta;','&#914;',
            '&Gamma;','&#915;',
            '&Delta;','&#916;',
            '&Epsilon;','&#917;',
            '&Zeta;','&#918;',
            '&Eta;','&#919;',
            '&Theta;','&#920;',
            '&Iota;','&#921;',
            '&Kappa;','&#922;',
            '&Lambda;','&#923;',
            '&Mu;','&#924;',
            '&Nu;','&#925;',
            '&Xi;','&#926;',
            '&Omicron;','&#927;',
            '&Pi;','&#928;',
            '&Rho;','&#929;',
            '&Sigma;','&#931;',
            '&Tau;','&#932;',
            '&Upsilon;','&#933;',
            '&Phi;','&#934;',
            '&Chi;','&#935;',
            '&Psi;','&#936;',
            '&Omega;','&#937;',
            '&alpha;','&#945;',
            '&beta;','&#946;',
            '&gamma;','&#947;',
            '&delta;','&#948;',
            '&epsilon;','&#949;',
            '&zeta;','&#950;',
            '&eta;','&#951;',
            '&theta;','&#952;',
            '&iota;','&#953;',
            '&kappa;','&#954;',
            '&lambda;','&#955;',
            '&mu;','&#956;',
            '&nu;','&#957;',
            '&xi;','&#958;',
            '&omicron;','&#959;',
            '&pi;','&#960;',
            '&rho;','&#961;',
            '&sigmaf;','&#962;',
            '&sigma;','&#963;',
            '&tau;','&#964;',
            '&upsilon;','&#965;',
            '&phi;','&#966;',
            '&chi;','&#967;',
            '&psi;','&#968;',
            '&omega;','&#969;',
            '&thetasym;','&#977;',
            '&upsih;','&#978;',
            '&piv;','&#982;',
            '&OElig;','&#338;',
            '&oelig;','&#339;',
            '&Scaron;','&#352;',
            '&scaron;','&#353;',
            '&Yuml;','&#376;',
            '&fnof;','&#402;',
            '&circ;','&#710;',
            '&tilde;','&#732;',
            '&ensp;','&#8194;',
            '&emsp;','&#8195;',
            '&thinsp;','&#8201;',
            '&zwnj;','&#8204;',
            '&zwj;','&#8205;',
            '&lrm;','&#8206;',
            '&rlm;','&#8207;',
            '&ndash;','&#8211;',
            '&mdash;','&#8212;',
            '&lsquo;','&#8216;',
            '&rsquo;','&#8217;',
            '&sbquo;','&#8218;',
            '&ldquo;','&#8220;',
            '&rdquo;','&#8221;',
            '&bdquo;','&#8222;',
            '&dagger;','&#8224;',
            '&Dagger;','&#8225;',
            '&bull;','&#8226;',
            '&hellip;','&#8230;',
            '&permil;','&#8240;',
            '&prime;','&#8242;',
            '&Prime;','&#8243;',
            '&lsaquo;','&#8249;',
            '&rsaquo;','&#8250;',
            '&oline;','&#8254;',
            '&euro;','&#8364;',
            '&trade;','&#8482;',
            '&larr;','&#8592;',
            '&uarr;','&#8593;',
            '&rarr;','&#8594;',
            '&darr;','&#8595;',
            '&harr;','&#8596;',
            '&crarr;','&#8629;',
            '&lceil;','&#8968;',
            '&rceil;','&#8969;',
            '&lfloor;','&#8970;',
            '&rfloor;','&#8971;',
            '&loz;','&#9674;',
            '&spades;','&#9824;',
            '&clubs;','&#9827;',
            '&hearts;','&#9829;',
            '&diams;','&#9830;');
BEGIN
  EXECUTE IMMEDIATE 'SELECT dbms_utility.get_time FROM dual' INTO start_time;
  EXECUTE IMMEDIATE 'SELECT TO_CHAR(current_timestamp, ''DD-MM-YYYY HH24:MI:SS'') || ''(DD-MM-YYYY HH24:MI:SS)'' from dual' INTO out_message;
  dbms_output.put_line('Rows repair begins at:' || out_message);
  FOR q IN 1 .. array.count loop
      if mod(q, 2) = 1 then
        dbms_output.put_line(array(q) || '/' || array(q + 1));
        FOR t IN (SELECT c.table_name, c.column_name FROM all_tab_cols c, user_all_tables u
        where  c.table_name = u.table_name and data_type = v_data_type) LOOP
          EXECUTE IMMEDIATE
          'SELECT COUNT(*) FROM '||t.table_name||' WHERE LOWER('|| t.column_name ||') LIKE ''%' || array(q) ||'%'''
          INTO match_count;
          IF match_count > 0 THEN
            rows_updated := 0;
            EXECUTE IMMEDIATE 'SELECT MAX(ORA_ROWSCN) FROM ' || t.table_name INTO rows_scn_max_loc;
            EXECUTE IMMEDIATE ('UPDATE ' || t.table_name ||
            ' SET ' || t.column_name || ' = ' || 'REPLACE(' || t.column_name || ',''' || array(q) || ''',''' || array(q + 1) || ''')' ||
            ' WHERE ' ||  t.column_name || ' LIKE ''%' || array(q) || '%''');
            rows_updated:= rows_updated + sql%rowcount;
             EXECUTE IMMEDIATE ('UPDATE ' || t.table_name ||
            ' SET ' || t.column_name || ' = ' || 'REPLACE(' || t.column_name || ',UPPER(''' || array(q) || '''),''' || array(q + 1) || ''')' ||
            ' WHERE ' ||  t.column_name || ' LIKE UPPER(''%' || array(q) || '%'')');
            rows_updated:= rows_updated + sql%rowcount;
            rows_updated_total := rows_updated_total + rows_updated;
            dbms_output.put_line(t.table_name || '.' || t.column_name || ':' || rows_updated);
            commit;

            --ORA_ROWSCN min value after the change
            EXECUTE IMMEDIATE 'SELECT MIN(ORA_ROWSCN) FROM ' || t.table_name ||
            ' WHERE ORA_ROWSCN > ' || rows_scn_max_loc
            INTO rows_scn_min_loc;

            EXECUTE IMMEDIATE 'SELECT MAX(ORA_ROWSCN) FROM ' || t.table_name
            INTO rows_scn_max_loc;


            --least value of SCN
            IF rows_scn_min = 0 THEN
              rows_scn_min := rows_scn_min_loc;
            ELSE
              EXECUTE IMMEDIATE 'SELECT LEAST(' || rows_scn_min || ',' || rows_scn_min_loc || ') FROM DUAL' INTO rows_scn_min;
            END IF;

            --ORA_ROWSCN max value after the change
            EXECUTE IMMEDIATE 'SELECT GREATEST(' || rows_scn_max_loc || ',' || rows_scn_max || ') FROM DUAL'
            INTO rows_scn_max;

          END IF;
        END LOOP;
      end if;
  end loop;

  EXECUTE IMMEDIATE 'SELECT (dbms_utility.get_time - ' || start_time || ')/100 FROM dual' INTO total_time;
  EXECUTE IMMEDIATE 'SELECT TO_CHAR(current_timestamp, ''DD-MM-YYYY HH24:MI:SS'') || ''(DD-MM-YYYY HH24:MI:SS)'' from dual' INTO out_message;

  dbms_output.put_line('Total time elapsed:' || total_time || 's');
  dbms_output.put_line('Finished at:' || out_message);
  dbms_output.put_line('total rows updated:' || rows_updated_total);
  dbms_output.put_line('finished');

  dbms_output.put_line('Input for ROLLBACK');
  dbms_output.put_line('Least SCN:' || rows_scn_min);
END;
/

spool off