-- DSCL-5827 - repairing concurrency behavior - rollback
ALTER TABLE QUERIES DROP COLUMN "OPT_LOCK";
commit;
spool off;