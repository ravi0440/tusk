-- DSCL-5827 - repairing concurrency behavior
ALTER TABLE QUERIES ADD "OPT_LOCK" NUMBER(10,0) DEFAULT 0 NOT NULL ENABLE;
commit;
spool off;