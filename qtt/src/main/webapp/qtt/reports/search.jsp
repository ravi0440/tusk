<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<body>
<s:form action="TrackingReport" name="trackingReportForm" id="repSrchCriteria" theme="simple" method="get">
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
    <tbody>
    <tr>
        <td width="100%">
            <table height="100%" cellSpacing=0 cellPadding=0 width="100%"
                   border=0>
                <tbody>
                <tr>
                    <td class=TitleExpanded
                        style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid"
                        vAlign=center align=left width="100%">
                        <div style="WIDTH: 100%; HEIGHT: 100%"><span>
							<p>Tracking Reports parameters</p>
							</span></div>
                    </td>
                </tr>
                <s:if test="hasActionErrors() || hasFieldErrors()">
                   <%@ include file="../global/validation_error.jsp" %>
                </s:if>
                <tr>
                    <td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
                        <div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><span>
							<p>
							<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                <tr>
                                    <td rowspan="2" valign="top">Initiate date:</td>
                                    <td colspan="4">
                                        <table cellspacing="4" cellpadding="0" border="0">
                                            <tr>
                                                <td width="8%"><b>From</b></td>
                                                <td><img src="img/spacer.gif" width="20"></td>
                                                <td><s:textfield name="reportCriteria.dateFrom" size="13" maxlength="11"/></td>
                                                <td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="70%" colspan="4">
                                        <table cellspacing="4" cellpadding="0" border="0">
                                            <tr>
                                                <td width="8%"><b>To</b></td>
                                                <td><img src="img/spacer.gif" width="20"></td>
                                                <td><s:textfield name="reportCriteria.dateTo" size="13" maxlength="11"/></td>
                                                <td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Workflow route:</td>
                                    <td>
                                        <s:select list="routes" name="reportCriteria.route" listKey="name" listValue="label" emptyOption="true"/>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>

                            </p>
							</span></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td width="20"><img src="img/spacer.gif" width="20"></td>
    </tr>
    <tr>
        <td width="100%" height=9></td>
    </tr>
    </tbody>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
        <td><br>
            <br>
            <i>[<b>n.b.</b> some reports may take several seconds to load
                - please be patient and do not close the browser..]</i></td>
    </tr>
    <tr>
        <td width="100%"><img src="img/spacer.gif" width="20" border="0"></td>
    </tr>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
        <td align="left" width="100%"><s:submit value="%{getText('button.show')}"></s:submit></td>
        <td width="20"><img src="img/spacer.gif" width="20"
                            border="0"></td>
    </tr>
    <tr>
        <td width="100%"><img src="img/spacer.gif" width="20"
                              border="0"></td>
    </tr>
</table>

</s:form>

</body>