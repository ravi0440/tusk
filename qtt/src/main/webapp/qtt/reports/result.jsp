<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<body>
<script language="javascript" src="js/window.js"></script>
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
    <tbody>
    <tr>
        <td width="100%">
            <table height="100%" width="100%" border="1"
                   style="BORDER-COLOR: #6699cc; BORDER-COLLAPSE: collapse; PADDING: 1px;">
                <tbody>
                <tr class="reportRow">
                    <th><s:text name="query.num"/></th>
                    <th><s:text name="query.lab"/></th>
                    <th><s:text name="init.day.date"/></th>
                    <th><s:text name="init.month.date"/></th>
                    <th><s:text name="init.year.date"/></th>
                    <th><s:text name="drug.name.retrieval"/></th>
                    <th><s:text name="drug.name.gener"/></th>
                    <th><s:text name="drug.indic"/></th>
                    <th><s:text name="org.type"/></th>
                    <th><s:text name="req.country"/></th>
                    <th><s:text name="req.surname"/></th>
                    <th><s:text name="turn.days"/></th>
                    <th><s:text name="route.flow"/></th>
                    <th><s:text name="query.type"/></th>
                    <th><s:text name="repo.type"/></th>
                    <th><s:text name="query.euqppv"/></th>
                    <th><s:text name="repo.count"/></th>
                    <th><s:text name="query.own"/></th>
                    <th><s:text name="status"/></th>
                    <th><s:text name="orig.due.date"/></th>
                    <th><s:text name="final.deliv.date"/></th>

                </tr>
                <s:iterator value="reportList">
                    <tr class="reportRow">
                        <td><s:property value="queryNumber"/></td>
                        <td>
                            <script language="javascript">
                                document.write(showLabel('<s:property value="queryLabel"/>', '<s:property value="querySeq"/>'));
                            </script>
                        </td>
                        <td><s:property value="longInitiateDate"/></td>
                        <td><s:property value="mediumInitiateDate"/></td>
                        <td><s:property value="shortInitiateDate"/></td>
                        <td><s:property value="drugRetrievalName"/></td>
                        <td><s:property value="drugGenericName"/></td>
                        <td><s:property value="drugIndication"/></td>
                        <td><s:property value="organisationType"/></td>
                        <td><s:property value="requesterCountry"/></td>
                        <td><s:property value="requesterLastName"/></td>
                        <td><s:property value="turnAround"/></td>
                        <td><s:property value="workflowRoute"/></td>
                        <td><s:property value="queryType"/></td>
                        <td><s:property value="reporterType"/></td>
                        <td><s:property value="euQPPVLong"/></td>
                        <td><s:property value="reporterCountry"/></td>
                        <td><s:property value="queryOwner"/></td>
                        <td><s:property value="currentStatus"/></td>
                        <td><s:property value="daysOriginalToFinal"/></td>
                        <td><s:property value="daysFinalToDelivery"/></td>

                    </tr>
                </s:iterator>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <b><a href="<s:url action="ExportReport" escapeAmp="false" includeParams="get"/>" class="Anchor">Export</a>
                to excel...</b>
        </td>
    </tr>
    </tbody>
</table>
</body>