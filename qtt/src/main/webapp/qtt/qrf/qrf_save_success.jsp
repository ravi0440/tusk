<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="html" uri="/struts-tags" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE><request:attribute name="title"/></TITLE>
<LINK href="css/roche.css" type=text/css rel=stylesheet>
<SCRIPT language="javascript" src="js/image_script.js"></script>
<script language="javascript" src="js/global_script.js"></script>
       <script language="javascript">

        function onload() {
            preload();
        }
        window.onload = onload;
    </script>
</HEAD>
<BODY onLoad="preload()">


<!-- START MAIN BODY SPACE ALL PAGE CONTENTS GO HERE -->

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<TBODY>
  		<TR>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Query Saved</P></SPAN></DIV></TD>
						</TR>
						
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>
								<br>
									Thank you. Your query has been saved successfully.
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
									<br>
										
									</table>

								</SPAN></DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
  		<TR>
			<TD width="100%" height=20></TD>
		</TR>
  	</TBODY>
</TABLE>


<!-- END MAIN BODY SPACE -->


<!--[if IE]>
<script defer="defer">onload();</script>
<![endif]-->
</BODY>
</HTML>
