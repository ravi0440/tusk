<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="html" uri="/struts-tags" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE><request:attribute name="title"/></TITLE>
<LINK href="css/roche.css" type=text/css rel=stylesheet>
<SCRIPT language="javascript" src="js/image_script.js"></script>
<script language="javascript" src="js/global_script.js"></script>
<script language="javascript">
	function initialise() {
		//if coming from psst pre-populate the nature of case		
		var natureOfCase = '<%= request.getParameter("personalQueryId") %>';
		if ( natureOfCase != 'null' ) 
			document.forms[0].natureofcase.value=natureOfCase;
	}
    function onload() {
        preload();
        initialise();
    }
    window.onload = onload;
    
    function hideSections() {
    	<s:iterator value="searchTypes">
    		document.getElementById('<s:property value="key"/>').style.display = "none";
    	</s:iterator>
    }
    
    function showSection(section) {
    	hideSections();
		document.getElementById(section).style.display = '';
    }
    
    function enableOtherOformat(name,limit) {
    	document.forms[0]["otherOformat['" + name + "']"].disabled = false;
    	setupTinyMC("#otherOformat" + name ,limit);
    }
    
    function disableOtherOformat(name) {
   		document.forms[0]["otherOformat['" + name + "']"].value = '';
   		document.forms[0]["otherOformat['" + name + "']"].disabled = true;
   		destroyTinyMC("#otherOformat" + name);
    }

    function enableDisableFrequencyOther() {
    	var select = document.forms[0]['dataSearch.frequency'];
    	if (select.options[select.selectedIndex].value == 'OTHER') {
    		document.forms[0]['dataSearch.frequencyOther'].disabled = false;
    		setupTinyMC('#frequencyOther',300);
    	} else {
    		document.forms[0]['dataSearch.frequencyOther'].value = '';
    		document.forms[0]['dataSearch.frequencyOther'].disabled = true;
    	}
    }
    
</script>
</HEAD>
<BODY onLoad="onload();">
<html:form theme="simple" action="DataSearchAction" method="POST" acceptcharset="utf-8" onsubmit="disableFormSubmit(this);">
<html:hidden name="activity" />

<input type="hidden" name="iehack" value="&#9760;"/>


<!-- START MAIN BODY SPACE ALL PAGE CONTENTS GO HERE -->

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<TBODY>
       <tr><td>
       <s:if test="hasActionErrors() || hasFieldErrors()">
          <table style="border: 1px solid rgb(40, 87, 139); padding:3px; width:100%">
              <tr>
                  <td>
                      <span class="ErrorHeader"> Validation errors detected!  </span>
                      <s:fielderror cssStyle="margin-bottom:0"/>
                      <s:actionerror cssStyle="margin-top:0"/>
                      <span class="ErrorHeader"> Please correct these errors and try again.</span>
                  </td>
              </tr>
          </table>
      </s:if>
      </td></tr>
  		<TR>
			<TD class=BodyText vAlign=top colSpan=3>Fields marked with a # are mandatory </TD>
		</TR>
  		<TR>
			<TD vAlign=top colSpan=3><IMG height=16 src="img/spacer.gif" width=1></TD>
		</TR>
	</TBODY>

	<TBODY>
  		<TR>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Type of Search</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>


									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
																	<td width="30%" valign="top"># Request:</td>
																	<td width="70%"><html:radio name="dataSearch.requestType" list="requestTypes"/></td>
																</tr>
																<TR>
										                			<TD colSpan=2 height="5"><IMG src="img/spacer.gif" height="5"></TD>
																</TR>
																<tr>
																	<td width="30%" valign="top"># For:</td>
																	<td width="70%">
																		<s:iterator value="searchTypes">
																			<input type="radio" name="dataSearch.searchType" value="<s:property value="key"/>" 
																				onclick="showSection('<s:property value="key"/>')"
																				<s:if test="dataSearch.searchType != null && key == dataSearch.searchType.toString()">checked</s:if>
																			/><s:property value="value"/><br/>
																		</s:iterator>
																	</td>
																</tr>
																<TR>
										                			<TD colSpan=2 height="5"><IMG src="img/spacer.gif" height="5"></TD>
																</TR>
									</table>


								</SPAN></DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
  		<TR id="AUTOMATED_QUERY" <s:if test="dataSearch==null || dataSearch.searchType == null || dataSearch.searchType.toString() != 'AUTOMATED_QUERY'">style="display:none"</s:if>>
			<TD width="100%">
				 <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Automated Query</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV class=clsBodyExpanded id=csnTopLeft_pnlBody style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>
								<P>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top"># Specification:</td>
											<td width="70%"><html:textarea id="specification" name="dataSearch.specification" cols="75" rows="4" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);"></html:textarea>
 											 </td>
 											  <script>setupTinyMC('#specification',300)</script>
										</tr>
										<tr>
											<td width="30%" valign="top">Output Format</td>
											
											<td width="70%">
												<s:iterator value="outputFormats">
													<input type="radio" 
														name="oformat['AUTOMATED_QUERY'].caseOformatId" 
														value="<s:property value="caseOformatId"/>"
														<s:if test="caseOformatId == oformat['AUTOMATED_QUERY'].caseOformatId">checked</s:if>
														<s:if test="caseOformatId == @com.roche.dss.qtt.model.CaseOformat@OTHER">onclick="enableOtherOformat('AUTOMATED_QUERY',300);"</s:if>
														<s:else>onclick="disableOtherOformat('AUTOMATED_QUERY',300);"</s:else>
													/>
													<s:property value="caseOformat"/>&nbsp;
											    </s:iterator>
											</td>
										<tr>
											<td width="30%" valign="top">If 'Other' please specify:</td>
											<td width="70%">
												<s:if test="oformat['AUTOMATED_QUERY'].caseOformatId == @com.roche.dss.qtt.model.CaseOformat@OTHER">
													<html:textarea id="otherOformatAUTOMATED_QUERY" name="otherOformat['AUTOMATED_QUERY']" cols="75" rows="2" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);"/>
													<script>
														enableOtherOformat('AUTOMATED_QUERY',300);
													</script>
												</s:if>
												<s:else>
													<html:textarea id="otherOformatAUTOMATED_QUERY" name="otherOformat['AUTOMATED_QUERY']" cols="75" rows="2" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);" disabled="true"/>
												</s:else>
											</td>
																
										</tr>
										<tr>
											<td width="30%" valign="top">BO Report:</td>
											<td width="70%"><s:textfield name="dataSearch.boReport" size="99"   onkeypress="textCounter(this,100);"maxlength="100"/></td>
										</tr>
										<tr>
											<td width="30%" valign="top">Report Parameters:</td>
											<td width="70%"><html:textarea id="reportParameters" name="dataSearch.reportParameters" cols="75" rows="2" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);"></html:textarea>
											
											
											<script>setupTinyMC('#reportParameters',300)</script>
											</td>
										</tr>
										
										<tr>
											<td width="30%" valign="top">Frequency:</td>
											<td width="70%">
													<html:select name="dataSearch.frequency" list="frequencies" headerKey="" headerValue="" onchange="enableDisableFrequencyOther();"/> 
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">If 'Other', please specify:</td>
											<td width="70%">
												<s:if test="dataSearch.frequency == @com.roche.dss.qtt.model.DataSearch$Frequency@OTHER">
													<html:textarea id="frequencyOther" name="dataSearch.frequencyOther" cols="75" rows="2" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);"></html:textarea>
													<script>enableDisableFrequencyOther()</script>
												</s:if>
												<s:else>
													<html:textarea id="frequencyOther"  name="dataSearch.frequencyOther" cols="75" rows="2" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);" disabled="true"></html:textarea>
												</s:else>
											</td>
										</tr>
										<TR>
					            			<TD colSpan=2 height="5"><IMG src="images/spacer.gif" height="5"></TD>
										</TR>
									</table>

								</P></SPAN></DIV>
							</TD>
						</TR>
				</TABLE>
			</TD>
  		</TR>
  		<TR id="SIGNAL_DETECTION" <s:if test="dataSearch==null || dataSearch.searchType == null || dataSearch.searchType.toString() != 'SIGNAL_DETECTION'">style="display:none"</s:if>>
			<TD width="100%">
				 <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Data Search - Signal Detection / Drug Safety Reports</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV class=clsBodyExpanded id=csnTopLeft_pnlBody style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>
								<P>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top"># MedDRA Preferred Term(s):</td>
											<td width="70%"><html:textarea id="meddraPrefferedTerm" name="meddraPreferredTerm['SIGNAL_DETECTION']" cols="75" rows="2" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);"></html:textarea></td>
											<script>setupTinyMC('#meddraPrefferedTerm',300)</script>
										</tr>
										<tr>
											<td width="30%" valign="top">Additional Comments:</td>
											<td width="70%"><html:textarea id="comments" name="comments['SIGNAL_DETECTION']" cols="75" rows="4" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);"></html:textarea>
 											 </td>
 											 <script>setupTinyMC('#comments',300)</script>
										</tr>
										<tr>
											<td width="30%" valign="top">Output Format</td>
											<td width="70%">
												<s:iterator value="outputFormats">
													<input type="radio" 
														name="oformat['SIGNAL_DETECTION'].caseOformatId" 
														value="<s:property value="caseOformatId"/>"
														<s:if test="caseOformatId == oformat['SIGNAL_DETECTION'].caseOformatId">checked</s:if>
														<s:if test="caseOformatId == @com.roche.dss.qtt.model.CaseOformat@OTHER">onclick="enableOtherOformat('SIGNAL_DETECTION',300);"</s:if>
														<s:else>onclick="disableOtherOformat('SIGNAL_DETECTION');"</s:else>
													/>
													<s:property value="caseOformat"/>&nbsp;
											    </s:iterator>
											</td>
										<tr>
											<td width="30%" valign="top">If 'Other', please specify:</td>
											<td width="70%">
												<s:if test="oformat['SIGNAL_DETECTION'].caseOformatId == @com.roche.dss.qtt.model.CaseOformat@OTHER">
													<html:textarea   id="otherOformatSIGNAL_DETECTION" name="otherOformat['SIGNAL_DETECTION']" cols="75" rows="2" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);"/>
													<script>
														enableOtherOformat('SIGNAL_DETECTION',300);
													</script>
												</s:if>
												<s:else>
													<html:textarea  id="otherOformatSIGNAL_DETECTION" name="otherOformat['SIGNAL_DETECTION']" cols="75" rows="2" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);" disabled="true"/>
												</s:else>
											</td>
										</tr>
										<TR>
					            			<TD colSpan=2 height="5"><IMG src="images/spacer.gif" height="5"></TD>
										</TR>
									</table>
								</P></SPAN></DIV>
							</TD>
						</TR>
				</TABLE>
			</TD>
  		</TR>
  		<TR id="ASIME" <s:if test="dataSearch==null || dataSearch.searchType == null || dataSearch.searchType.toString() != 'ASIME'">style="display:none"</s:if>>
			<TD width="100%">
				 <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Data Search - ASIMEs </P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV class=clsBodyExpanded id=csnTopLeft_pnlBody style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>
								<P>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top"># MedDRA Preferred Term(s):</td>
											<td width="70%"><html:textarea id="meddraPreferredTermASIME" name="meddraPreferredTerm['ASIME']" cols="75" rows="2" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);"></html:textarea></td>
											<script>setupTinyMC('#meddraPreferredTermASIME',600)</script>
										</tr>
										<tr>
											<td width="30%" valign="top">Index Case (AER No.):</td>
											<td width="70%"><s:textfield name="indexcase" size="99" maxlength="1000" onkeypress="textCounter(this,1000);"/></td>
										</tr>
										<tr>
											<td width="30%" valign="top">Additional Comments:</td>
											<td width="70%"><html:textarea name="comments['ASIME']" cols="75" rows="4" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);"></html:textarea>
 											 </td>
										</tr>
										<TR>
					            			<TD colSpan=2 height="5"><IMG src="images/spacer.gif" height="5"></TD>
										</TR>
									</table>
								</P></SPAN></DIV>
							</TD>
						</TR>
				</TABLE>	
			</TD>
  		</TR>
  		<TR id="PERIODIC_REPORTS" <s:if test="dataSearch==null || dataSearch.searchType == null || dataSearch.searchType.toString() != 'PERIODIC_REPORTS'">style="display:none"</s:if>>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Data Search - Periodic Reports</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify>
									<SPAN>

										<table cellspacing="0" cellpadding="1" border="0" width="100%">
											<TR>
						            			<TD colSpan=2 height="5"><IMG src="img/spacer.gif" height="5"></TD>
											</TR>
											<tr>
												<td colspan="2">Please ensure you attached your specifications and ad-hoc requests on the first page of the QRF request.</td>
											</tr>
											<TR>
						            			<TD colSpan=2 height="5"><IMG src="img/spacer.gif" height="5"></TD>
											</TR>
										</table>
									
									</SPAN>
								</DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
  		</TR>
  		<TR id="SAE_RECONCILIATIONS" <s:if test="dataSearch==null || dataSearch.searchType == null || dataSearch.searchType.toString() != 'SAE_RECONCILIATIONS'">style="display:none"</s:if>>
			<TD width="100%">
				 <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Data Search - SAE Reconciliations or Internal Audit</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV class=clsBodyExpanded id=csnTopLeft_pnlBody style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>
								<P>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top"># Protocol/Study No.:</td>
											<td width="70%"><s:textfield name="clinicalTrial.protocolNumber"  onkeypress="textCounter(this,100);" size="99" maxlength="100"/></td>
										</tr>
										<tr>
											<td width="30%" valign="top">CRTN/Country/Investigator(s):</td>
											<td width="70%"><s:textfield name="clinicalTrial.crtnNumber"  onkeypress="textCounter(this,300);" size="99" maxlength="300"/></td>
										</tr>
										<tr>
											<td width="30%" valign="top">Patient No.(s):</td>
											<td width="70%"><s:textfield id="clinicalTrialpatientNumber" name="clinicalTrial.patientNumber" size="99" maxlength="300"/></td>
											<script>setupTinyMC('#clinicalTrialpatientNumber',300)</script>
										</tr>
										<tr>
											<td width="30%" valign="top">Case Selection:</td>
											<td width="70%"><s:textfield name="clinicalTrial.caseSelection"  onkeypress="textCounter(this,1000);" size="99" maxlength="1000"/></td>
										</tr>
										<tr>
											<td width="30%" valign="top">Output Format</td>
											
											<td width="70%">
												<s:iterator value="outputFormats">
													<input type="radio" 
														name="oformat['SAE_RECONCILIATIONS'].caseOformatId" 
														value="<s:property value="caseOformatId"/>"
														<s:if test="caseOformatId == oformat['SAE_RECONCILIATIONS'].caseOformatId">checked</s:if>
														<s:if test="caseOformatId == @com.roche.dss.qtt.model.CaseOformat@OTHER">onclick="enableOtherOformat('SAE_RECONCILIATIONS',300);"</s:if>
														<s:else>onclick="disableOtherOformat('SAE_RECONCILIATIONS');"</s:else>
													/>
													<s:property value="caseOformat"/>&nbsp;
											    </s:iterator>
											</td>
										<tr>
											<td width="30%" valign="top">If 'Other', please specify:</td>
											<td width="70%">
												<s:if test="oformat['SAE_RECONCILIATIONS'].caseOformatId == @com.roche.dss.qtt.model.CaseOformat@OTHER">
													<html:textarea id="otherOformatSAE_RECONCILIATIONS"  name="otherOformat['SAE_RECONCILIATIONS']" cols="75" rows="2" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);"/>
													<script>
														enableOtherOformat('SAE_RECONCILIATIONS',300);
													</script>
												</s:if>
												<s:else>
													<html:textarea id="otherOformatSAE_RECONCILIATIONS"  name="otherOformat['SAE_RECONCILIATIONS']" cols="75" rows="2" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);" disabled="true"/>
												</s:else>
											</td>
										</tr>
										<TR>
					            			<TD colSpan=2 height="5"><IMG src="images/spacer.gif" height="5"></TD>
										</TR>
									</table>
								</P></SPAN></DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
  		</TR>
  		<TR id="OTHER" <s:if test="dataSearch==null || dataSearch.searchType == null || dataSearch.searchType.toString() != 'OTHER'">style="display:none"</s:if>>
			<TD width="100%">
				 <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Data Search - Other</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify>
								<SPAN>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top"># Your Question:</td>
											<td width="70%"><html:textarea id="natureOfCaseOTHER" name="natureOfCase['OTHER']" cols="75" rows="4" onkeypress="textCounter(this,1000);" onblur="textCounter(this,1000);"></html:textarea></td>
											<script>setupTinyMC('#natureOfCaseOTHER',4000)</script>
										</tr>
										<tr>
											<td width="30%" valign="top">Output Format</td>
											
											<td width="70%">												
												<s:iterator value="outputFormats">
													<input type="radio" 
														name="oformat['OTHER'].caseOformatId" 
														value="<s:property value="caseOformatId"/>"
														<s:if test="caseOformatId == oformat['OTHER'].caseOformatId">checked</s:if>
														<s:if test="caseOformatId == @com.roche.dss.qtt.model.CaseOformat@OTHER">onclick="enableOtherOformat('OTHER',300);"</s:if>
														<s:else>onclick="disableOtherOformat('OTHER');"</s:else>
													/>
													<s:property value="caseOformat"/>&nbsp;
											    </s:iterator>
											</td>
										<tr>
											<td width="30%" valign="top">If 'Other', please specify:</td>
											<td width="70%">
												<s:if test="oformat['OTHER'].caseOformatId == @com.roche.dss.qtt.model.CaseOformat@OTHER">
													<html:textarea  id="otherOformatOTHER" name="otherOformat['OTHER']" cols="75" rows="2" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);"/>
													<script>
														enableOtherOformat('OTHER',300);
													</script>
												</s:if>
												<s:else>
													<html:textarea  id="otherOformatOTHER" name="otherOformat['OTHER']" cols="75" rows="2" onkeypress="textCounter(this,300);" onblur="textCounter(this,300);" disabled="true"/>
												</s:else>
											</td>
										</tr>
										<TR>
					            			<TD colSpan=2 height="5"><IMG src="images/spacer.gif" height="5"></TD>
										</TR>
									</table>
								</SPAN></DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
  		</TR>
  		<TR id="LITERATURE_SEARCH" <s:if test="dataSearch==null || dataSearch.searchType == null || dataSearch.searchType.toString() != 'LITERATURE_SEARCH'">style="display:none"</s:if>>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Literature Search</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify>
									<SPAN>

										<table cellspacing="0" cellpadding="1" border="0" width="100%">
											<tr>
												<td width="30%" valign="top"># Your question:</br> <a href="http://rochewiki.roche.com/confluence/download/attachments/169804597/Guidance+on+SMQ+Literature+search+requests+Final.docx">Guidance on SMQ Literature Search Requests</a></td>
												<td width="70%"><html:textarea id="natureOfCaseLITERATURE_SEARCH" name="natureOfCase['LITERATURE_SEARCH']" rows="4" cols="75" onblur="textCounter(this,1000);" onkeypress="textCounter(this,1000);"/></td>
												<script>setupTinyMC('#natureOfCaseLITERATURE_SEARCH',4000)</script>
											</tr>
											<TR>
						            			<TD colSpan=2 height="5"><IMG src="img/spacer.gif" height="5"></TD>
											</TR>
										</table>
									
									</SPAN>
								</DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
  		<TR>
			<TD width="100%" height=20></TD>
		</TR>
  	</TBODY>
</TABLE>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td width="100%" align="center"><html:submit value="Next" title="Finish" /></td>
		<TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
	</tr>
</table>

</html:form>
<!--[if IE]>
<script defer="defer">onload();</script>
<![endif]-->
</BODY>
</HTML>
