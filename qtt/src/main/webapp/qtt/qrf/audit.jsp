<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="html" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE><request:attribute name="title"/></TITLE>
<LINK href="css/roche.css" type=text/css rel=stylesheet>
<SCRIPT language="javascript" src="js/image_script.js"></script>
<script language="javascript" src="js/qrf_script.js"></script>
<script language="javascript" src="js/global_script.js"></script>
<script src="js/jquery-2.1.4.min.js"></script>

<script type="text/javascript">

function outputFormatChanged(){
	var outputFormat = $('#optputFormat').val();
	// setup rich editor only for other cases
	if(outputFormat==4){
		setupTinyMC('#specialRequest',300);
		}else{
			destroyTinyMC('#specialRequest');
			}
	enableSpecial();
}

$( document ).ready(function() {
	outputFormatChanged();
});

 </script>

</HEAD>
<BODY onload="enableStudyNo();">

<html:form action="AuditAction" theme="simple" method="POST" acceptcharset="utf-8" onsubmit="disableFormSubmit(this);">
<input type="hidden" name="iehack" value="&#9760;"/>


<!-- START MAIN BODY SPACE ALL PAGE CONTENTS GO HERE -->

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
	<TBODY>
      <tr><td>
       <s:if test="hasActionErrors() || hasFieldErrors()">
          <table style="border: 1px solid rgb(40, 87, 139); padding:3px; width:100%">
              <tr>
                  <td>
                      <span class="ErrorHeader"> Validation errors detected!  </span>
                      <s:fielderror cssStyle="margin-bottom:0"/>
                      <s:actionerror cssStyle="margin-top:0"/>
                      <span class="ErrorHeader"> Please correct these errors and try again.</span>
                  </td>
              </tr>
          </table>
      </s:if>
      </td></tr>
        <TR>
			<TD vAlign=top colSpan=3><IMG height=16 src="img/spacer.gif" width=1></TD>
		</TR>
		<TR>
			<TD class=clsBodyText vAlign=top colSpan=3>Fields marked with a # are mandatory</TD>
		</TR>
		<TR>
			<TD vAlign=top colSpan=3><IMG height=16 src="img/spacer.gif" width=1></TD>
		</TR>
  		<TR>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Description Of Query</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV class=clsBodyExpanded id=csnTopLeft_pnlBody style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>


									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top"># Your question:</td>
											<td width="70%"><html:textarea id="natureofcase" name="natureofcase" rows="4" cols="75" onblur="textCounter(this,1000);" onkeypress="textCounter(this,1000);"/></td>
											<script>setupTinyMC('#natureofcase',4000)</script>
										</tr>
										<tr>
											<td width="30%" valign="top">Other Pertinent Info:</td>
											<td width="70%"><html:textarea id="otherinfo" name="otherinfo" rows="4" cols="75" onblur="textCounter(this,1000);" onkeypress="textCounter(this,1000);"/></td>
											<script>setupTinyMC('#otherinfo',3000)</script>
										</tr>
										<TR>
					            			<TD colSpan=2 height="5"><IMG src="img/spacer.gif" height="5"></TD>
										</TR>
									</table>

								</SPAN></DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
  		<TR>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Data Metrics</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>


									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top">All Cases:</td>
											<td width="70%">                                                
												<s:radio name="audit.allCases" list="#{true:'Yes',false:'No'}" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">All Studies:</td>
											<td width="70%">                                                
												<s:radio name="audit.allStudies" list="#{true:'Yes',false:'No, only for Study/Protocol No.'}" onchange="enableStudyNo();" />
												<s:textfield name="audit.studyNo" onkeypress="textCounter(this,3000);" maxlength="3000" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">All Spontaneous Cases:</td>
											<td width="70%">                                                
												<s:radio name="audit.allSpontaneousCases" list="#{true:'Yes',false:'No'}" />
											</td>
										</tr>
									</table>

								</SPAN></DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
  		<TR>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Data Selection</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>


									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top">All Submission Timelines:</td>
											<td width="70%">                                                
												<s:radio name="audit.allSubm" list="#{true:'Yes',false:'No'}" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">SUSAR Submission Timelines:</td>
											<td width="70%">                                                
												<s:radio name="audit.susarSubm" list="#{true:'Yes',false:'No'}" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Serious Cases Timelines:</td>
											<td width="70%">                                                
												<s:radio name="audit.seriousCases" list="#{true:'Yes',false:'No'}" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Non-serious Cases Timelines:</td>
											<td width="70%">                                                
												<s:radio name="audit.nonSeriousCases" list="#{true:'Yes',false:'No'}" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Submission Titles:</td>
											<td width="70%"><html:textarea id="auditsubmissionTitles" name="audit.submissionTitles" rows="4" cols="75" onblur="textCounter(this,1000);" onkeypress="textCounter(this,1000);"/></td>
											<script>setupTinyMC('#auditsubmissionTitles',1000)</script>
										</tr>
									</table>

								</SPAN></DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
  		<TR>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Data Output</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>


									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
																	<td width="30%" valign="top"># Output Format:</td>
																	<td width="70%"><html:select id="optputFormat" name="clinical.ctrlOformat.ctrlOformatId"  list="clotypes"   listKey="ctrlOformatId" listValue="ctrlOformat" headerKey="0" headerValue="" onchange="outputFormatChanged()"/></td>
																</tr>
																<tr>
																	<td width="30%" valign="top">Special Requests:</td>
																	<td width="70%"><html:textarea id="specialRequest" name="specialRequest" rows="4" cols="75" onblur="textCounter(this,300);" onkeypress="textCounter(this,300);" /></td>
						</tr>
									</table>

								</SPAN></DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
  	</TBODY>
</TABLE>


<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td width="100%" align="center"><html:submit value="Next" title="Finish" /></td>
		<TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
	</tr>
	<tr>
		<TD height="20" width="20" colspan="6"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
	</tr>
</table>


</html:form>
<!--[if IE]>
<script defer="defer">onload();</script>
<![endif]-->
</BODY>
</HTML>
