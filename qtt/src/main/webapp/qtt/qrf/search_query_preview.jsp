<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="html" uri="/struts-tags" %>

<html>
<head>
    <TITLE><request:attribute name="title"/></TITLE>
    <LINK href="css/roche.css" type=text/css rel=stylesheet>
    <script language="javascript" src="js/initiate_script.js"></script>
    <SCRIPT language="javascript" src="js/image_script.js"></script>
    <script language="javascript" src="js/qrf_script.js"></script>
    <script language="javascript" src="js/global_script.js"></script>
    <script language="javascript">
        function doIgnore() {
            document.forms[0].ignoreEmail.value = 'yes';
            document.forms[0].action = 'SendInitiationEmail.do';
            document.forms[0].target = '_self';
            document.forms[0].submit();
        }
        function confirmationDialog(dialogType) {
            var path = "ConfirmationDialog.action?option=" + dialogType+"&queryId=<s:property value="%{mid}"/>";
            var args;
            var sFeatures = getModalValues(230, 630);
            var sReturn = window.showModalDialog(path, args, sFeatures);
            if (sReturn != null) {
                window.parent.location = sReturn;
            }
        }//end

    </script>

    <script language="javascript">

        function onload() {
            preload();
        }
        window.onload = onload;
    </script>
</head>
<body onLoad="preload()">
<html:form theme="simple" action="/InitiateList" acceptcharset="utf-8" method="POST">
<input type="hidden" name="iehack" value="&#9760;"/>


<!-- START MAIN BODY SPACE ALL PAGE CONTENTS GO HERE -->


<TABLE cellSpacing=0 cellPadding=1 width="100%" border=0>
<TBODY>
<TR>
<TD width="100%">
<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR>
    <TD class=TitleExpanded
        style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid"
        vAlign=center align=left width="100%">
        <DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P><s:property value ="pageTitle"/></P></SPAN></DIV>
    </TD>
</TR>
<TR>
<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2
    height="100%">
<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%"
     align=justify><SPAN>
								<P>

									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                    <s:if test="!queryResponse">
                                        <tr>
                                            <td valign="top" colspan="3"><font size="+1">Provisional Query Number:
                                                <s:property escapeHtml="false" value="mid"/></font></td>
                                        </tr>
                                    </s:if>
                                        <tr>
                                            <TD height="10" colspan="3"><img alt="" src="img/spacer.gif" height="10">
                                            </TD>
                                        </tr>
                                        <tr>
                                            <td valign="top" colspan="3"><font size="+1">Requester Details</font></td>
                                        </tr>
                                        <tr>
                                            <TD height="10" colspan="3"><img alt="" src="img/spacer.gif" height="10">
                                            </TD>
                                        </tr>
                                        <tr>
                                            <td width="30%">
                                                First Name:
                                            </td>
                                            <td width="70%">
                                                <s:property escapeHtml="false" value="firstname"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%">
                                                Last Name:
                                            </td>
                                            <td>
                                                <s:property escapeHtml="false" value="lastname"/>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%">
                                                Tel No:
                                            </td>
                                            <td>
                                                <s:property escapeHtml="false" value="telephone"/>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%">
                                                Email Address:
                                            </td>
                                            <td>
                                                <s:property escapeHtml="false" value="email"/>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%">
                                                Country:
                                            </td>
                                            <td>
                                                <s:property escapeHtml="false" value="reqCountry"/>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%">
                                                Organisation Name:
                                            </td>
                                            <td>
                                                <s:property escapeHtml="false" value="orgName"/>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%">
                                                Organisation Type:
                                            </td>
                                            <td>
                                                <s:property escapeHtml="false" value="orgType"/>

                                            </td>
                                        </tr>
                                        <TR>
                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                              border="0"></TD>
                                        </TR>
                                        <TR>
                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                        </TR>
                                        <TR>
                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                              border="0"></TD>
                                        </TR>
                                    </table>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <td valign="top" colspan="3"><font size="+1">Query Origin </font></td>
                                        </tr>
                                        <tr>
                                            <TD height="10" colspan="3"><img alt="" src="img/spacer.gif" height="10">
                                            </TD>
                                        </tr>
                                        <tr>
                                            <td width="30%">Source Country of Request:
                                            </td>
                                            <td width="70%">
                                                <s:property escapeHtml="false" value="sourceCountry"/>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Reporter Type:
                                            </td>
                                            <td>
                                                <s:property escapeHtml="false" value="reporterType"/>

                                            </td>
                                        </tr>
                                        <TR>
                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                              border="0"></TD>
                                        </TR>
                                        <TR>
                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                        </TR>
                                        <TR>
                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                              border="0"></TD>
                                        </TR>
                                    </table>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <td valign="top" colspan="3"><font size="+1">Drug Details </font></td>
                                        </tr>
                                        <tr>
                                            <TD height="10" colspan="3"><img alt="" src="img/spacer.gif" height="10">
                                            </TD>
                                        </tr>
                                        <tr>
                                            <td width="30%">
                                                All Drugs:
                                            </td>
                                            <td>
                                                <s:property escapeHtml="false" value="allDrugs"/>

                                            </td>
                                        </tr>

                                          <s:if test="%{allDrugs==@com.roche.dss.qtt.query.web.action.ActionConstants@NO}">
                                            <tr>
                                                <td width="25%">
                                                    Drug Retrieval Name:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="retrDrug"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%">
                                                    Route:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="route"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%">
                                                    Formulation:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="formulation"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%">
                                                    Indication:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="indication"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%">
                                                    Dose:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="dose"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top">
                                                    Other Roche Drugs:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="othersDrug"/>

                                                </td>
                                            </tr>
                                           </s:if>
                                        <TR>
                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                              border="0"></TD>
                                        </TR>
                                        <TR>
                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                        </TR>
                                        <TR>
                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                              border="0"></TD>
                                        </TR>
                                    </table>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <td valign="top" colspan="3"><font size="+1">Response Timelines</font></td>
                                        </tr>
                                        <tr>
                                            <TD height="10" colspan="3"><img alt="" src="img/spacer.gif" height="10">
                                            </TD>
                                        </tr>
                                        <tr>
                                            <td width="30%">Original Due Date:
                                            </td>
                                            <td width="70%">
                                                <s:property escapeHtml="false" value="dateRequested"/>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="30%" valign="top">Urgency:
                                            </td>
                                            <td>
                                                <s:property escapeHtml="false" value="urgency"/>
                                            </td>
                                        </tr>
                                        <TR>
                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                              border="0"></TD>
                                        </TR>
                                        <TR>
                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                        </TR>
                                        <TR>
                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                              border="0"></TD>
                                        </TR>
                                    </table>
								                                     <s:if test="%{orgType==@com.roche.dss.qtt.query.web.action.ActionConstants@QRF_ORG_TYPE_AFFILIATE}">
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Preparatory Work Already Done</font></td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">
                                                    Repository:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="repository"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top">
                                                    PSUR:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="psur"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top">
                                                    CDS:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="cds"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top">
                                                    Issue Workups:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="issue"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top">
                                                    Literature:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="literature"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top">
                                                    Others:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="others"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top">
                                                    Other Comments:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="othersComment"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top">
                                                    Comments:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="comments"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <td valign="top" colspan="3"><font size="+1">Query Details</font></td>
                                        </tr>
                                        <tr>
                                            <TD height="10" colspan="3"><img alt="" src="img/spacer.gif" height="10">
                                            </TD>
                                        </tr>
                                        <tr>
                                            <td width="30%">Query Type:
                                            </td>
                                            <td width="70%">
                                                <s:property escapeHtml="false" value="queryType"/>

                                            </td>
                                        </tr>
                                        <TR>
                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                              border="0"></TD>
                                        </TR>
                                        <TR>
                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                        </TR>
                                        <TR>
                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                              border="0"></TD>
                                        </TR>
                                    </table>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <td valign="top" colspan="3"><font size="+1">Attachments </font></td>
                                        </tr>
                                        <tr>
                                            <TD height="10" colspan="3"><img alt="" src="img/spacer.gif" height="10">
                                            </TD>
                                        </tr>

                                        <s:iterator id="doc" value="attachments" var="i" status="rowstatus">
                                            <tr>
                                                <td width="35%">&nbsp;<s:property escapeHtml="false" value="documentName"/></TD>
                                                <td align="left"><a target="attachments" href="InitiateAttachment.action?index=<s:property escapeHtml="false" value="#rowstatus.index"/>&id=<s:property escapeHtml="false" value="mid"/>
" onclick="toggle('img<s:property escapeHtml="false" value="#rowstatus.index"/>2')" onmouseover="hilight('img<s:property escapeHtml="false" value="#rowstatus.index"/>2')" onmouseout="lolight('img<s:property escapeHtml="false" value="#rowstatus.index"/>2')"><img
                                                        name="img<s:property escapeHtml="false" value="#rowstatus.index"/>2" src="img/find_off.gif"
                                                        title="View/Open Document" border="0"></a></TD>
                                                <td>&nbsp;</td>
                                            </tr>

                                        </s:iterator>


                                        <TR>
                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                              border="0"></TD>
                                        </TR>
                                        <TR>
                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                        </TR>
                                        <TR>
                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                              border="0"></TD>
                                        </TR>
                                    </table>

									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <td valign="top" colspan="3"><font size="+1">Cumulative Details</font></td>
                                        </tr>
                                        <tr>
                                            <TD height="10" colspan="3"><img alt="" src="img/spacer.gif" height="10">
                                            </TD>
                                        </tr>
                                        <tr>
                                            <td width="30%">Cumulative Period:
                                            </td>
                                            <td width="70%">
                                                <s:property escapeHtml="false" value="cumulative"/>

                                            </td>
                                        </tr>
                                                              <s:if test="%{cumulative==@com.roche.dss.qtt.query.web.action.ActionConstants@NO}">
                                            <tr>
                                                <td width="30%">Date from:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="dateFrom"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Date to:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="dateTo"/>

                                                </td>
                                            </tr>
                                        </s:if>
                                        <tr>
                                            <td width="30%">Interval:
                                            </td>
                                            <td width="70%">
                                                <s:property escapeHtml="false" value="interval"/>

                                            </td>
                                        </tr>
                                        <TR>
                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                              border="0"></TD>
                                        </TR>
                                        <TR>
                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                        </TR>
                                        <TR>
                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                              border="0"></TD>
                                        </TR>
                                    </table>
								                                     <s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@DRUG_EVENT.toString()}">
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Query Description</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Your question:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="natureOfCase"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Index Case:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="indexCase"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Signs & Symp:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="signsSymp"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Investigations:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="investigations"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Past Medical History:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="pmh"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Comedications:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="conMeds"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Other pertinent info:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="otherInfo"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Diagnosis/Confounders:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="diagnosis"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <!--<tr>
                                                       <td valign="top" colspan="3"><font size="+1">Adverse Event(s)</span></td>
                                                   </tr>
                                                   <tr>
                                                       <TD height="10" colspan="3"><img alt=""  src="img/spacer.gif" height="10"></TD>
                                                   </tr>-->
                                            <tr>
                                                <td width="30%" valign="top">Adverse Event(s):
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="aeTerm"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Case Details</font></td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <TD width="30%">AER</TD>
                                                <TD width="30%">Local Ref Number</TD>
                                                <TD width="30%">External Ref Number</TD>
                                            </tr>

                                             <s:iterator value="cases" var="casesList">
                                                <tr>
                                                    <td width="30%">&nbsp;<s:property escapeHtml="false" value="#casesList.aerNumber"/></TD>
                                                    <td width="30%">&nbsp;<s:property escapeHtml="false" value="#casesList.localRefNumber"/></TD>
                                                    <td width="30%"><s:property escapeHtml="false" value="#casesList.extRefNumber"/></TD>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </s:iterator>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                        </table>
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td width="30%" valign="top">Additional comments:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="additionalComments"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@DRUG_INTERACTION.toString()}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Interacting Drug(s)</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <% int i=0;%>
                                            <s:iterator id="drugsList" value="drugs" status="" var="drugsList">
                                                <s:if test="#drugsList.genericDrug!=null&&#drugsList.genericDrug!='' || #drugsList.retrDrug!=null&&#drugsList.retrDrug!=''" >
                                                <tr>
                                                    <td valign="top" colspan="3">Additional Drug Details
                                                        - <%=++i%>:
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Drug Type:
                                                    </td>
                                                    <td width="70%">
                                                        <s:property escapeHtml="false" value="#drugsList.drugType"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Generic name:
                                                    </td>
                                                    <td>
                                                        <s:property escapeHtml="false" value="#drugsList.genericDrug"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Retrieval name:
                                                    </td>
                                                    <td>
                                                        <s:property escapeHtml="false" value="#drugsList.retrDrug"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Route:
                                                    </td>
                                                    <td>
                                                        <s:property escapeHtml="false" value="#drugsList.route"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Dose:
                                                    </td>
                                                    <td>
                                                        <s:property escapeHtml="false" value="#drugsList.dose"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Formulation:
                                                    </td>
                                                    <td>
                                                        <s:property escapeHtml="false" value="#drugsList.formulation"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                     height="10"></TD>
                                                </tr>
                                                </s:if>
                                            </s:iterator>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Query Description</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Your question:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="natureOfCase"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Index Case:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="indexCase"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Signs & Symp:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="signsSymp"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Investigations:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="investigations"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Past Medical History:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="pmh"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Comedications:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="conMeds"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Other pertinent info:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="otherInfo"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Diagnosis/Confounders:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="diagnosis"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Case Details</font></td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <TD width="30%">AER</TD>
                                                <TD width="30%">Local Ref Number</TD>
                                                <TD width="30%">External Ref Number</TD>
                                            </tr>
                                            <s:iterator value="cases" var="casesList">
                                                <tr>
                                                    <td width="30%">&nbsp;<s:property escapeHtml="false" value="#casesList.aerNumber"/></TD>
                                                    <td width="30%">&nbsp;<s:property escapeHtml="false" value="#casesList.localRefNumber"/></TD>
                                                    <td width="30%"><s:property escapeHtml="false" value="#casesList.extRefNumber"/></TD>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </s:iterator>
                                            <tr>
                                                <td width="30%" valign="top">Additional comments:</td>
                                                <td colspan="2">
                                                    <s:property escapeHtml="false" value="additionalComments"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="3"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="3" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="3"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@MANUFACTURING.toString()}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Query Description</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Your question:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="natureOfCase"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Index Case:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="indexCase"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Signs & Symp:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="signsSymp"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Investigations:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="investigations"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Past Medical History:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="pmh"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Comedications:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="conMeds"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Other pertinent info:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="otherInfo"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Diagnosis/Confounders:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="diagnosis"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td width="30%">Batch No:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="batch"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Case Details</font></td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <TD width="30%">AER</TD>
                                                <TD width="30%">Local Ref Number</TD>
                                                <TD width="30%">External Ref Number</TD>
                                            </tr>
                                            <s:iterator value="cases" var="casesList">
                                                <tr>
                                                    <td width="30%">&nbsp;<s:property escapeHtml="false" value="#casesList.aerNumber"/></TD>
                                                    <td width="30%">&nbsp;<s:property escapeHtml="false" value="#casesList.localRefNumber"/></TD>
                                                    <td width="30%"><s:property escapeHtml="false" value="#casesList.extRefNumber"/></TD>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </s:iterator>
                                            <tr>
                                                <td width="30%" valign="top">Additional comments:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="additionalComments"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Output Format:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="outputFormat"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">'Others' specification:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="outputOther"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="3"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="3" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="3"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@PREGNANCY.toString()}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Query Description</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Your question:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="natureOfCase"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Index Case:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="indexCase"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Signs & Symp:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="signsSymp"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Investigations:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="investigations"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Past Medical History:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="pmh"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Comedications:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="conMeds"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Other pertinent info:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="otherInfo"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Diagnosis/Confounders:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="diagnosis"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Pregnancy Information</font></td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="false">Type of exposure:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="exposureType"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="false">Timing of exposure in pregnancy:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="timing"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="false">Pregnancy/Foetal outcome:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="outcome"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Case Details</font></td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <TD width="30%">AER</TD>
                                                <TD width="30%">Local Ref Number</TD>
                                                <TD width="30%">External Ref Number</TD>
                                            </tr>
                                            <s:iterator value="cases" var="casesList">
                                                <tr>
                                                    <td width="30%">&nbsp;<s:property escapeHtml="false" value="#casesList.aerNumber"/></TD>
                                                    <td width="30%">&nbsp;<s:property escapeHtml="false" value="#casesList.localRefNumber"/></TD>
                                                    <td width="30%"><s:property escapeHtml="false" value="#casesList.extRefNumber"/></TD>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </s:iterator>
                                            <tr>
                                                <td width="30%" valign="false">Additional comments:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="additionalComments"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="false">Output Format:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="outputFormat"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="false">'Others' specification:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="outputOther"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="3"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="3" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="3"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@EXTERNAL_AUDIT.toString()}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Query Description</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Nature of request:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="natureOfCase"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Other pertinent info:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="otherInfo"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">

                                            <tr>
                                                <td width="30%" valign="top">Adverse Event(s):
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="aeTerm"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Clinical Trial Info</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%">Protocol No(s):
                                                </td>
                                                <td width="70%" valign="top">
                                                    <s:property escapeHtml="false" value="protocolNumber"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">CRTN No(s):
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="crtnNumber"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Patient No(s):
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="patientNumber"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Case Selection:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="caseSelection"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Output Format:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="outputFormatDesc"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Special requests:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="outputOther"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@CASE_CLARIFICATION.toString()}">
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Case Details</font></td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <TD width="30%">AER</TD>
                                                <TD width="30%">Local Ref Number</TD>
                                                <TD width="30%">External Ref Number</TD>
                                            </tr>
                                            <s:iterator value="cases" var="casesList">
                                                <tr>
                                                    <td width="30%">&nbsp;<s:property escapeHtml="false" value="#casesList.aerNumber"/></TD>
                                                    <td width="30%">&nbsp;<s:property escapeHtml="false" value="#casesList.localRefNumber"/></TD>
                                                    <td width="30%"><s:property escapeHtml="false" value="#casesList.extRefNumber"/></TD>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </s:iterator>
                                            <tr>
                                                <td width="30%" valign="top">Additional comments:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="additionalComments"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Output Format:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="outputFormat"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">'Others' specification:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="outputOther"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="3"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="3" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="3"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@CASE_DATA_REQUEST.toString()}">
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Case Details</font></td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <TD width="30%">AER</TD>
                                                <TD width="30%">Local Ref Number</TD>
                                                <TD width="30%">External Ref Number</TD>
                                            </tr>
                                            <s:iterator value="cases" var="casesList">
                                                <tr>
                                                    <td width="30%">&nbsp;<s:property escapeHtml="false" value="#casesList.aerNumber"/></TD>
                                                    <td width="30%">&nbsp;<s:property escapeHtml="false" value="#casesList.localRefNumber"/></TD>
                                                    <td width="30%"><s:property escapeHtml="false" value="#casesList.extRefNumber"/></TD>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </s:iterator>
                                            <tr>
                                                <td width="30%" valign="top">Additional comments:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="additionalComments"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Output Format:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="outputFormat"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">'Others' specification:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="outputOther"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="3"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="3" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="3"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@SAE_RECONCILIATION.toString()}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Clinical Trial Info</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Protocol No(s):
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="protocolNumber"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">CRTN No(s):
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="crtnNumber"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Patient No(s):
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="patientNumber"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Case Selection:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="caseSelection"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Output Format:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="outputFormatDesc"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Special requests:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="outputOther"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@IND_UPDATES.toString()}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td width="30%">IND Number:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="indNumber"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@PERFORMANCE_METRICS.toString()}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Query Description</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Metrics required:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="natureOfCase"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Other pertinent info:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="otherInfo"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@SIGNAL_DETECTION.toString()}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Query Description</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Nature of question:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="natureOfCase"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">

                                            <tr>
                                                <td width="30%" valign="top">Adverse Event(s):
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="aeTerm"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@LITERATURE_SEARCH.toString()}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Query Description</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Nature of question:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="natureOfCase"/>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Other pertinent info:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="otherInfo"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">

                                            <tr>
                                                <td width="30%" valign="top">Adverse Event(s):
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="aeTerm"/>

                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@PERFORMANCE_METRICS2.toString()}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Query Description</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Metrics required:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="performanceMetrics.requestDescription"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Data Output</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Output Format:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="performanceMetrics.outputFormat.ctrlOformat"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Special Requests:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="performanceMetrics.specialRequests"/>
                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@EXTERNAL_AUDIT_INSPECTION.toString()}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="2"><font size="+1">Description of Query</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="2"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Your question:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="natureOfCase"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Other Pertinent Info:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="otherInfo"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <tr>
                                                <td valign="top" colspan="2"><font size="+1">Data Metrics</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="2"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">All Cases:
                                                </td>
                                                <td>
                                                	<s:if test="audit.allCases">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@YES"/>
                                                   	</s:if>
                                                   	<s:elseif test="audit.allCases == false">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@NO"/>
                                                   	</s:elseif>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">All Studies:
                                                </td>
                                                <td>
                                                	<s:if test="audit.allStudies">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@YES"/>
                                                   	</s:if>
                                                   	<s:elseif test="audit.allStudies == false">
                                                    	No, only for Study/Protocol No. <s:property value="audit.studyNo"/>
                                                   	</s:elseif>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">All Spontaneous Cases:
                                                </td>
                                                <td>
                                                	<s:if test="audit.allSpontaneousCases">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@YES"/>
                                                   	</s:if>
                                                   	<s:elseif test="audit.allSpontaneousCases == false">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@NO"/>
                                                   	</s:elseif>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <tr>
                                                <td valign="top" colspan="2"><font size="+1">Data Selection</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="2"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">All Submission Timelines:
                                                </td>
                                                <td>
                                                	<s:if test="audit.allSubm">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@YES"/>
                                                   	</s:if>
                                                   	<s:elseif test="audit.allSubm == false">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@NO"/>
                                                   	</s:elseif>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">SUSAR Submission Timelines:
                                                </td>
                                                <td>
                                                	<s:if test="audit.susarSubm">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@YES"/>
                                                   	</s:if>
                                                   	<s:elseif test="audit.susarSubm == false">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@NO"/>
                                                   	</s:elseif>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Serious Cases Timelines:
                                                </td>
                                                <td>
                                                	<s:if test="audit.seriousCases">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@YES"/>
                                                   	</s:if>
                                                   	<s:elseif test="audit.seriousCases == false">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@NO"/>
                                                   	</s:elseif>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Non-serious Cases Timelines:
                                                </td>
                                                <td>
                                                	<s:if test="audit.nonSeriousCases">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@YES"/>
                                                   	</s:if>
                                                   	<s:elseif test="audit.nonSeriousCases == false">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@NO"/>
                                                   	</s:elseif>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Submission Titles:
                                                </td>
                                                <td width="70%">
                                                   	<s:property escapeHtml="false" value="audit.submissionTitles"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <tr>
                                                <td valign="top" colspan="2"><font size="+1">Data Output</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="2"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Output Format:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="outputFormatDesc"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Special Requests:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="specialRequest"/>
                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@MANUFACTURING_RECALL.toString()}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Description of Query</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Your question:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="natureOfCase"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
                                               <tr>
                                                <td valign="top" colspan="3"><font size="+1">Manufacturing Details</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">&nbsp;</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="manufacturing.type.label"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Batch No(s).:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="manufacturing.batchNo"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Trackwise No(s).:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="manufacturing.trackwiseNo"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Associated Adverse Event(s)</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Reported Adverse Event(s):
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="aeTerm"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Data Output</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Output Format:
                                                </td>
	                                                <td>
	                                                	<s:if test="%{outputFormat=='Other'}">
	                                                    	<s:property escapeHtml="false" value="specialRequest"/>
	                                                    </s:if>
	                                                    <s:else>
	                                                    	<s:property escapeHtml="false" value="outputFormat"/>
	                                                    </s:else>
	                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Additional Info:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="otherInfo"/>
                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@MEDICAL.toString()}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Query Description</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Your question:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="natureOfCase"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
                                               <tr>
                                                <td valign="top" colspan="3"><font size="+1">Adverse Event</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Index Case:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="indexCase"/>
                                                </td>
                                            </tr>
                                            <s:iterator value="cases" status="status">
                                            	<s:if test="%{#status.first}">
		                                            <tr>
		                                                <td width="30%" valign="top">AER No.:</td>
		                                                <td width="70%">
		                                                    <s:property escapeHtml="false" value="aerNumber"/>
		                                                </td>
		                                            </tr>
		                                            <tr>
		                                                <td width="30%" valign="top">LRN:</td>
		                                                <td width="70%">
		                                                    <s:property escapeHtml="false" value="localRefNumber"/>
		                                                </td>
		                                            </tr>
	                                            </s:if>
                                            </s:iterator>
                                            <tr>
                                                <td width="30%" valign="top">Signs & Symptoms:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="signsSymp"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Investigations:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="investigations"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Past Medical History:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="pmh"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Comedications:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="conMeds"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Other Pertinent Info:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="otherInfo"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Diagnosis/Confounders:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="diagnosis"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Pregnancy</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Type of Exposure:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="exposureType"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Timing of Exposure in Pregnancy:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="timing"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Pregnancy / Foetal Outcome:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="outcome"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Comments:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="comments"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Drug Interaction</font>
                                                </td>
                                            </tr>
                                            <s:iterator value="drugs" status="status">
	                                            <tr>
	                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
	                                                                                 height="10"></TD>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Drug Type:
	                                                </td>
	                                                <td>
                                                    	<s:property escapeHtml="false" value="drugType"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Generic Name:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="genericDrug"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Drug Retrieval Name:
	                                                </td>
	                                                <td>
                                                    	<s:property escapeHtml="false" value="retrDrug"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Route:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="route"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Dose:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="dose"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Formulation:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="formulation"/>
	                                                </td>
	                                            </tr>
	                                            <TR>
	                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                                  border="0"></TD>
	                                            </TR>
                                            </s:iterator>
                                            <tr>
                                                <td width="30%" valign="top">Comment:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="additionalComments"/>
                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@DATA_SEARCH.toString()}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Type of Search</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Request:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="dataSearch.requestType.label"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">For:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="dataSearch.searchType.label"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <s:if test="%{dataSearch.searchType==@com.roche.dss.qtt.model.DataSearch$SearchType@AUTOMATED_QUERY}">
	                                        
	                                            <tr>
	                                                <td valign="top" colspan="3"><font size="+1">Automated Query</font>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
	                                                                                 height="10"></TD>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Specification:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="dataSearch.specification"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Output Format:
	                                                </td>
	                                                <td>
	                                                	<s:if test="%{outputFormat=='Other'}">
	                                                    	<s:property escapeHtml="false" value="outputOther"/>
	                                                    </s:if>
	                                                    <s:else>
	                                                    	<s:property escapeHtml="false" value="outputFormat"/>
	                                                    </s:else>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">BO Report:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="dataSearch.boReport"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Report Parameters:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="dataSearch.reportParameters"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Frequency:
	                                                </td>
	                                                <td>
	                                                	<s:if test="dataSearch.frequency == @com.roche.dss.qtt.model.DataSearch$Frequency@OTHER">
	                                                    	<s:property escapeHtml="false" value="dataSearch.frequencyOther"/>
	                                                	</s:if>
	                                                	<s:else>
	                                                    	<s:property escapeHtml="false" value="dataSearch.frequency.label"/>
	                                                    </s:else>
	                                                </td>
	                                            </tr>
	                                            
	                                        </s:if>
	                                        <s:if test="%{dataSearch.searchType==@com.roche.dss.qtt.model.DataSearch$SearchType@SIGNAL_DETECTION}">
	                                        
	                                            <tr>
	                                                <td valign="top" colspan="3"><font size="+1">Data Search - Signal Detection / Drug Safety Reports</font>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
	                                                                                 height="10"></TD>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">MedDRA Preferred Term(s):
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="dataSearch.meddraPreferredTerm"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Additional Comments:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="additionalComments"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Output Format:
	                                                </td>
	                                                <td>
	                                                	<s:if test="%{outputFormat=='Other'}">
	                                                    	<s:property escapeHtml="false" value="outputOther"/>
	                                                    </s:if>
	                                                    <s:else>
	                                                    	<s:property escapeHtml="false" value="outputFormat"/>
	                                                    </s:else>
	                                                </td>
	                                            </tr>
	                                            
	                                        </s:if>
	                                        <s:if test="%{dataSearch.searchType==@com.roche.dss.qtt.model.DataSearch$SearchType@ASIME}">
	                                        
	                                            <tr>
	                                                <td valign="top" colspan="3"><font size="+1">Data Search - ASIMEs </font>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
	                                                                                 height="10"></TD>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">MedDRA Preferred Term(s):
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="dataSearch.meddraPreferredTerm"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Index Case (AER No.):
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="indexCase"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Additional Comments:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="additionalComments"/>
	                                                </td>
	                                            </tr>
	                                            
	                                        </s:if>
	                                        <s:if test="%{dataSearch.searchType==@com.roche.dss.qtt.model.DataSearch$SearchType@SAE_RECONCILIATIONS}">
	                                        
	                                            <tr>
	                                                <td valign="top" colspan="3"><font size="+1">Data Search - SAE Reconciliations or Internal Audit</font>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
	                                                                                 height="10"></TD>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Protocol/Study No.:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="protocolNumber"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">CRTN/Country/Investigator(s):
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="crtnNumber"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Patient No.(s):
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="patientNumber"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Case Selection:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="caseSelection"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Output Format:
	                                                </td>
	                                                <td>
	                                                	<s:if test="%{outputFormat=='Other'}">
	                                                    	<s:property escapeHtml="false" value="outputOther"/>
	                                                    </s:if>
	                                                    <s:else>
	                                                    	<s:property escapeHtml="false" value="outputFormat"/>
	                                                    </s:else>
	                                                </td>
	                                            </tr>
	                                            
	                                        </s:if>
	                                        <s:if test="%{dataSearch.searchType==@com.roche.dss.qtt.model.DataSearch$SearchType@OTHER}">
	                                        
	                                            <tr>
	                                                <td valign="top" colspan="3"><font size="+1">Data Search - Other</font>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
	                                                                                 height="10"></TD>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Your Question:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="natureOfCase"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Output Format:
	                                                </td>
	                                                <td>
	                                                	<s:if test="%{outputFormat=='Other'}">
	                                                    	<s:property escapeHtml="false" value="outputOther"/>
	                                                    </s:if>
	                                                    <s:else>
	                                                    	<s:property escapeHtml="false" value="outputFormat"/>
	                                                    </s:else>
	                                                </td>
	                                            </tr>
	                                            
	                                        </s:if>
	                                        <s:if test="%{dataSearch.searchType==@com.roche.dss.qtt.model.DataSearch$SearchType@LITERATURE_SEARCH}">
	                                        
	                                            <tr>
	                                                <td valign="top" colspan="3"><font size="+1">Literature Search</font>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
	                                                                                 height="10"></TD>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Your Question:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="natureOfCase"/>
	                                                </td>
	                                            </tr>
	                                            
	                                        </s:if>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
							</SPAN></DIV>
</TD>
</TR>
</TBODY>
</TABLE>
</TD>
<TD width="20"><img alt="" src="img/spacer.gif" width="20"></TD>
</TR>
<TR>
    <TD width="100%" height=20></TD>
</TR>
</TBODY>
</TABLE>


<!-- END MAIN BODY SPACE -->
<s:hidden name="id" value="%{mid}" />
<s:hidden name="activity" />

</html:form>
<!--[if IE]>
<script defer="defer">onload();</script>
<![endif]-->

</body>
</html>
