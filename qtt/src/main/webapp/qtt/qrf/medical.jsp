<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="html" uri="/struts-tags" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE><request:attribute name="title"/></TITLE>
<LINK href="css/roche.css" type=text/css rel=stylesheet>
<SCRIPT language="javascript" src="js/image_script.js"></script>
<script language="javascript" src="js/qrf_script.js"></script>
<script language="javascript" src="js/global_script.js"></script>
<script language="javascript">
	function initialise() {
		
		<% 
			//initialise only if coming here for the first time and not by navigation 'back' button from step 2
			if ( request.getAttribute("back") == null) { %>
			
				//if coming from psst pre-populate the nature of case		
				var natureOfCase = '<%= request.getParameter("personalQueryId") %>';
				if ( natureOfCase != 'null' ) 
					document.forms[0].natureofcase.value=natureOfCase;
		<%
		}
		%>
			
	}

    function onload() {
        preload();
        initialise();
    }
    window.onload = onload;
    
    var drugCount = <s:if test="drugs.isEmpty">1</s:if><s:else><s:property value="drugs.size" /></s:else>;
    
	function addDrug() {
    	var genericNames = "" +
		<s:iterator value="drugRetrievalNames" var="drn" status="status">
			"<option><s:property value="drn"/></option>"
			<s:if test="%{!#status.last}">+</s:if>
		</s:iterator>;
    	addRow("<IMG src=\"img/spacer.gif\" height=\"5\">", "<IMG src=\"img/spacer.gif\" height=\"5\">");
    	addRow("Drug Type:", "<input type=\"radio\" name=\"drugs[" + drugCount + "].rocheDrugFlag\" value=\"Y\" onclick=\"enableDisableGeneric('" + drugCount + "' , 'Y')\"></input>Roche<input type=\"radio\" name=\"drugs[" + drugCount + "].rocheDrugFlag\" value=\"N\" onclick=\"enableDisableGeneric('" + drugCount + "' , 'N')\"></input>Non-Roche");
    	addRow("Generic Name (Free Text):", "<input type=\"text\" value=\"\" name=\"drugs[" + drugCount + "].innGenericName\" disabled=\"true\">");
    	addRow("Drug Retrieval Name:", "<select styleid=\"retrDrug1\" name=\"drugs[" + drugCount + "].drugRetrievalName\" disabled=\"true\"><option value=\"\"></option>" + genericNames + "</select><span class=\"rightarrowwhite\">»</span>");
    	addRow("Route:", "<input type=\"text\" value=\"\" name=\"drugs[" + drugCount + "].route\">");
    	addRow("Dose:", "<input type=\"text\" value=\"\" name=\"drugs[" + drugCount + "].dose\">");
    	addRow("Formulation:", "<input type=\"text\" value=\"\" name=\"drugs[" + drugCount + "].formulation\">");
    	drugCount++;
    }
    
    function addRow(label, input) {
    	var table = document.getElementById("drugTable");
    	var linkRow = document.getElementById("linkRow");
    	var newRow = table.insertRow(linkRow.rowIndex);
    	var labelCell = newRow.insertCell(0);
    	var inputCell = newRow.insertCell(1);
    	labelCell.innerHTML = label;
    	inputCell.innerHTML = input;
    }    

    function enableDisableGeneric(drugNumber, value) {
        if (value == 'N') {
        	document.forms[0]['drugs[' + drugNumber + '].drugRetrievalName'].disabled = true;
        	document.forms[0]['drugs[' + drugNumber + '].drugRetrievalName'].selectedIndex = 0;
        	document.forms[0]['drugs[' + drugNumber + '].innGenericName'].disabled = false;
        } else {
        	document.forms[0]['drugs[' + drugNumber + '].drugRetrievalName'].disabled = false;
        	document.forms[0]['drugs[' + drugNumber + '].innGenericName'].value = '';
        	document.forms[0]['drugs[' + drugNumber + '].innGenericName'].disabled = true;
        }
    }

</script>
</HEAD>
<BODY onLoad="preload();initialise()">
<html:form action="MedicalAction" theme="simple" method="POST" acceptcharset="utf-8" onsubmit="disableFormSubmit(this);">
<input type="hidden" name="iehack" value="&#9760;"/>


<!-- START MAIN BODY SPACE ALL PAGE CONTENTS GO HERE -->

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
	<TBODY>
      <tr><td>
       <s:if test="hasActionErrors() || hasFieldErrors()">
          <table style="border: 1px solid rgb(40, 87, 139); padding:3px; width:100%">
              <tr>
                  <td>
                      <span class="ErrorHeader"> Validation errors detected!  </span>
                      <s:fielderror cssStyle="margin-bottom:0"/>
                      <s:actionerror cssStyle="margin-top:0"/>
                      <span class="ErrorHeader"> Please correct these errors and try again.</span>
                  </td>
              </tr>
          </table>
      </s:if>
      </td></tr>
		<TR>
			<TD class=BodyText vAlign=top colSpan=3>Fields marked with a # are mandatory </TD>
		</TR>
		<TR>
			<TD vAlign=top colSpan=3><IMG height=16 src="img/spacer.gif" width=1></TD>
		</TR>
  		<TR>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Description Of Query</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify>
									<SPAN>

										<table cellspacing="0" cellpadding="1" border="0" width="100%">
											<tr>
												<td width="30%" valign="top"># Your question:</td>
												<td width="70%"><html:textarea id="natureofcase" name="natureofcase" rows="4" cols="75" onblur="textCounter(this,1000);" onkeypress="textCounter(this,1000);"/></td>
												<script>setupTinyMC('#natureofcase',4000)</script>
											</tr>
											<TR>
						            			<TD colSpan=2 height="5"><IMG src="img/spacer.gif" height="5"></TD>
											</TR>
										</table>
									
									</SPAN>
								</DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
  		<TR>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Adverse Event</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>


									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top">Index Case:</td>
											<td width="70%">                                                
												<html:textarea name="indexcase" rows="4" cols="75" onblur="textCounter(this,600);" onkeypress="textCounter(this,600);"/>
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">AER No.:</td>
											<td width="70%">                                                
												<s:textfield name="caze.aerNumber" onkeypress="textCounter(this,3000);" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">LRN:</td>
											<td width="70%">                                                
												<s:textfield name="caze.localReferenceNumber" onkeypress="textCounter(this,3000);"/>
											</td>
										</tr>
										<TR>
					            			<TD colSpan=2 height="5"><IMG src="img/spacer.gif" height="5"></TD>
										</TR>
                                        <TR>
                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                        </TR>
										<TR>
					            			<TD colSpan=2 height="5"><IMG src="img/spacer.gif" height="5"></TD>
										</TR>
										<tr>
											<td width="30%" valign="top">Signs & Symptoms:</td>
											<td width="70%">                                                
												<s:textfield id="signsymp" name="signsymp" />
												<script>setupTinyMC('#signsymp',3000)</script>
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Investigations:</td>
											<td width="70%">                                                
												<s:textfield id="investigation" name="investigation" />
												<script>setupTinyMC('#investigation',3000)</script>
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Past Medical History:</td>
											<td width="70%">                                                
												<s:textfield id="pmh" name="pmh" />
												<script>setupTinyMC('#pmh',3000)</script>
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Comedications:</td>
											<td width="70%">                                                
												<s:textfield id="conmeds"  name="conmeds" />
												<script>setupTinyMC('#conmeds',3000)</script>
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Other Pertinent Info:</td>
											<td width="70%">                                                
												<s:textfield  id="otherinfo" name="otherinfo" />
												<script>setupTinyMC('#otherinfo',3000)</script>
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Diagnosis / Confounders:</td>
											<td width="70%">                                                
												<s:textfield  id="diagnosis" name="diagnosis" />
												<script>setupTinyMC('#diagnosis',3000)</script>
											</td>
										</tr>
                                        
									</table>

								</SPAN></DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
  		<TR>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Pregnancy</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify>
									<SPAN>

										<table cellspacing="0" cellpadding="1" border="0" width="100%">
											<tr>
												<td width="30%" valign="top">Type of Exposure:</td>
												<td width="70%">                                                
													<s:radio name="preg.prgyExptype.exposureTypeId" list="prgyExptypes" listKey="exposureTypeId" listValue="exposureType" />
												</td>
											</tr>
											<tr>
												<td width="30%" valign="top">Timing of Exposure in Pregnancy:</td>
												<td width="70%">                                                
													<s:textfield name="preg.timeInPregnancy" onkeypress="textCounter(this,3000);" />
												</td>
											</tr>
											<tr>
												<td width="30%" valign="top">Pregnancy / Foetal Outcome:</td>
												<td width="70%">                                                
													<s:textfield name="preg.outcome" onkeypress="textCounter(this,3000);" />
												</td>
											</tr>
											<tr>
												<td width="30%" valign="top">Comments:</td>
												<td width="70%"><html:textarea name="preg.comments" rows="4" cols="75" onblur="textCounter(this,600);" onkeypress="textCounter(this,600);"/></td>
											</tr>
											<TR>
						            			<TD colSpan=2 height="5"><IMG src="img/spacer.gif" height="5"></TD>
											</TR>
										</table>
									
									</SPAN>
								</DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
  		<TR>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Drug Interaction</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify>
									<SPAN>

										<table id="drugTable" cellspacing="0" cellpadding="1" border="0" width="100%">
											<s:if test="drugs.empty">
												<tr>
													<td width="30%" valign="top">Drug Type:</td>
													<td width="70%">                                                
														<s:radio name="drugs[0].rocheDrugFlag" list="#{'Y':'Roche', 'N':'Non-Roche'}" 
															onclick="enableDisableGeneric('0' , this.value)"/>
													</td>
												</tr>
												<tr>
													<td width="30%" valign="top">Generic Name (Free Text):</td>
													<td width="70%">                                                
														<s:textfield name="drugs[0].innGenericName" disabled="true" />
													</td>
												</tr>
												<tr>
													<td width="30%" valign="top">Drug Retrieval Name:</td>
													<td width="70%">                                                
														<html:select name="drugs[0].drugRetrievalName" styleId="retrDrug1"
	                                                            list="drugRetrievalNames"
                                                             	headerKey=""
                                                             	headerValue=""
                                                             	disabled="true"
                                                    	/>
	                                                	<span class="rightarrowwhite">&raquo;</span>
													</td>
												</tr>
												<tr>
													<td width="30%" valign="top">Route:</td>
													<td width="70%">                                                
														<s:textfield name="drugs[0].route" onkeypress="textCounter(this,3000);"/>
													</td>
												</tr>
												<tr>
													<td width="30%" valign="top">Dose:</td>
													<td width="70%">                                                
														<s:textfield name="drugs[0].dose" onkeypress="textCounter(this,3000);"/>
													</td>
												</tr>
												<tr>
													<td width="30%" valign="top">Formulation:</td>
													<td width="70%">                                                
														<s:textfield name="drugs[0].formulation" onkeypress="textCounter(this,3000);" />
													</td>
												</tr>
											</s:if>
											<s:iterator value="drugs" var="drug" status="status">
												<tr>
													<td width="30%" valign="top">Drug Type:</td>
													<td width="70%">                                                
														<s:radio name="drugs[%{#status.index}].rocheDrugFlag" list="#{'Y':'Roche', 'N':'Non-Roche'}" 
															onclick="enableDisableGeneric('%{#status.index}' , this.value)"/>
													</td>
												</tr>
												<tr>
													<td width="30%" valign="top">Generic Name (Free Text):</td>
													<td width="70%">                                                
														<s:if test="rocheDrugFlag != 'N'">
															<s:textfield name="drugs[%{#status.index}].innGenericName" disabled="true" />
														</s:if> 
														<s:else>
															<s:textfield name="drugs[%{#status.index}].innGenericName" />
														</s:else>
													</td>
												</tr>
												<tr>
													<td width="30%" valign="top">Drug Retrieval Name:</td>
													<td width="70%">
														<s:if test="rocheDrugFlag != 'Y'"> 
															<html:select name="drugs[%{#status.index}].drugRetrievalName" styleId="retrDrug1"
		                                                            list="drugRetrievalNames"
	                                                             	headerKey=""
	                                                             	headerValue=""
	                                                             	disabled="true"
	                                                    	/>
	                                                    </s:if>
	                                                    <s:else>
															<html:select name="drugs[%{#status.index}].drugRetrievalName" styleId="retrDrug1"
		                                                            list="drugRetrievalNames"
	                                                             	headerKey=""
	                                                             	headerValue=""
	                                                    	/>
	                                                    </s:else>
	                                                	<span class="rightarrowwhite">&raquo;</span>
													</td>
												</tr>
												<tr>
													<td width="30%" valign="top">Route:</td>
													<td width="70%">                                                
														<s:textfield name="drugs[%{#status.index}].route" onkeypress="textCounter(this,600);" />
													</td>
												</tr>
												<tr>
													<td width="30%" valign="top">Dose:</td>
													<td width="70%">                                                
														<s:textfield name="drugs[%{#status.index}].dose" onkeypress="textCounter(this,600);"/>
													</td>
												</tr>
												<tr>
													<td width="30%" valign="top">Formulation:</td>
													<td width="70%">                                                
														<s:textfield name="drugs[%{#status.index}].formulation" onkeypress="textCounter(this,600);" />
													</td>
												</tr>
											</s:iterator>
											<tr id="linkRow">
												<td width="30%" valign="top"><a href="javascript:addDrug();">Click here to add an additional interacting drug</a></td>
												<td width="70%">&nbsp;</td>
											</tr>
											<tr>
												<td width="30%" valign="top">Comment:</td>
												<td width="70%"><html:textarea name="comments" rows="4" cols="75" onblur="textCounter(this,600);" onkeypress="textCounter(this,600);"/></td>
											</tr>
											<TR>
						            			<TD colSpan=2 height="5"><IMG src="img/spacer.gif" height="5"></TD>
											</TR>
										</table>
									
									</SPAN>
								</DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
  	</TBODY>
</TABLE>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td width="100%" align="center"><html:submit value="Next" title="Finish" /></td>
		<TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
	</tr>
	<tr>
		<TD height="20" width="20" colspan="6"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
	</tr>
</table>

</html:form>
<!--[if IE]>
<script defer="defer">onload();</script>
<![endif]-->
</BODY>
</HTML>
