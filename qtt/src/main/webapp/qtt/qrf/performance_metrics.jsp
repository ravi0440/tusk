<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="html" uri="/struts-tags" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE><request:attribute name="title"/></TITLE>
<LINK href="css/roche.css" type=text/css rel=stylesheet>
<SCRIPT language="javascript" src="js/image_script.js"></script>
<script language="javascript" src="js/qrf_script.js"></script>
<script language="javascript" src="js/global_script.js"></script>
<script src="js/jquery-2.1.4.min.js"></script>
<script language="javascript">
	function initialise() {
		//if coming from psst pre-populate the nature of case		
		var natureOfCase = '<%= request.getParameter("personalQueryId") %>';
		if ( natureOfCase != 'null' ) 
			document.forms[0].natureOfCase.value=natureOfCase;
	}
    function onload() {
        preload();
        initialise();
    }
    window.onload = onload;

    function outputFormatChanged(){
    	var outputFormat = $('#outputFormat').val();
    	// setup rich editor only for other cases
    	if(outputFormat==4){
    		setupTinyMC('#specialRequests',300);
    	}else{
    		destroyTinyMC('#specialRequests');
    	}
    	enableSpecial();
    }
    $( document ).ready(function() {
    	outputFormatChanged();
    });
    
</script>
</HEAD>
<BODY onLoad="preload();initialise()">
<html:form action="PerformanceAction" method="POST" acceptcharset="utf-8" theme="simple" onsubmit="disableFormSubmit(this);">
<input type="hidden" name="iehack" value="&#9760;"/>


<!-- START MAIN BODY SPACE ALL PAGE CONTENTS GO HERE -->

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<TBODY>
      <tr><td>
       <s:if test="hasActionErrors() || hasFieldErrors()">
          <table style="border: 1px solid rgb(40, 87, 139); padding:3px; width:100%">
              <tr>
                  <td>
                      <span class="ErrorHeader"> Validation errors detected!  </span>
                      <s:fielderror cssStyle="margin-bottom:0"/>
                      <s:actionerror cssStyle="margin-top:0"/>
                      <span class="ErrorHeader"> Please correct these errors and try again.</span>
                  </td>
              </tr>
          </table>
      </s:if>
      </td></tr>
  		<TR>
			<TD class=BodyText vAlign=top colSpan=3>Fields marked with a # are mandatory </TD>
		</TR>
  		<TR>
			<TD vAlign=top colSpan=3><IMG height=16 src="img/spacer.gif" width=1></TD>
		</TR>
	</TBODY>

	<TBODY>
  		<TR>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Description Of Query</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>


									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
																	<td width="30%" valign="top"># Metrics required:</td>
																	<td width="70%"><html:textarea id="requestDescription" name="requestDescription" rows="4" cols="75" onblur="textCounter(this,1000);" onkeypress="textCounter(this,1000);"/></td>
																	<script>setupTinyMC('#requestDescription',1000)</script>
																</tr>
									</table>
								</SPAN></DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
  		<TR>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Data Output</P></SPAN></DIV></TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>


									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
																	<td width="30%" valign="top"># Output Format:</td>
																	<td width="70%"><html:select id="outputFormat" name="outputFormat.ctrlOformatId"  list="clotypes"   listKey="ctrlOformatId" listValue="ctrlOformat" headerKey="0" headerValue="" onchange="outputFormatChanged()"/></td>
																</tr>
																<tr>
																	<td width="30%" valign="top">Special Requests:</td>
																	<td width="70%"><html:textarea id="specialRequests" name="specialRequests" rows="4" cols="75" onblur="textCounter(this,300);" onkeypress="textCounter(this,300);"/></td>
																	
						</tr>
									</table>

								</SPAN></DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
		<TR>
			<TD width="100%" height=20></TD>
		</TR>
  	</TBODY>
</TABLE>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td width="100%" align="center"><html:submit value="Next" title="Finish" /></td>
		<TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
	</tr>
</table>

</html:form>
<!--[if IE]>
<script defer="defer">onload();</script>
<![endif]-->
</BODY>
</HTML>
