<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="html" uri="/struts-tags" %>

<HTML>
<HEAD><TITLE>Partially Saved Queries</TITLE>
    <LINK href="css/roche.css" type=text/css rel=stylesheet>
    <SCRIPT language="javascript" src="js/image_script.js"></script>
    <SCRIPT language="javascript" src="js/global_script.js"></script>
    <script language="javascript" src="js/qrf_script.js"></script>

    <script language="javascript">

        function onload() {
            preload();
        }
        window.onload = onload;
    </script>
</HEAD>
<BODY onLoad="preload()">
<form action="PopulateQueryAction.action" method="POST" onsubmit="disableFormSubmit(this);">

      <tr><td>
       <s:if test="hasActionErrors() || hasFieldErrors()">
          <table style="border: 1px solid rgb(40, 87, 139); padding:3px; width:100%">
              <tr>
                  <td>
                      <span class="ErrorHeader"> Validation errors detected!  </span>
                      <s:fielderror cssStyle="margin-bottom:0"/>
                      <s:actionerror cssStyle="margin-top:0"/>
                      <span class="ErrorHeader"> Please correct these errors and try again.</span>
                  </td>
              </tr>
          </table>
      </s:if>
      </td></tr>

       <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>

        <TR>
            <TD width="100%">
                <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TBODY>
                    <TR>
                        <TD class=TitleExpanded
                            style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid"
                            vAlign=center align=left width="100%">
                            <DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Partially Saved Queries</P></SPAN></DIV>
                        </TD>
                    </TR>

                    <TR>
                        <TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid"
                            colSpan=2 height="100%">
                            <DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%"
                                 align=justify><SPAN>


									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <TD height="9" colspan="3"><img src="img/spacer.gif" height="9"></TD>
                                        </tr>
                                    </table>

									
                            <s:if test="!hasActionErrors()">
                                        <s:if test="%{partialQueriesList==null||partialQueriesList.isEmpty}">

                                        <TR>
                                            <TD class=TitleExpanded
                                                style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid"
                                                vAlign=center align=left width="100%">
                                                <DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
														<P>No Partially Saved Queries found for the given email
                                                            address</P></SPAN></DIV>
                                            </TD>
                                        </TR>
                                        </s:if>
                                        <s:else>
                                        <TABLE class=DataGrid
                                               style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc"
                                               borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1 width="100%">
                                            <TBODY>
                                            <TR class=DataGridHeader>
                                                <TD class=DataGridHeader align="center" width="8%">Select</TD>
                                                <TD class=DataGridHeader align="center" width="18%">Date Submitted</TD>
                                                <TD class=DataGridHeader align="center" width="28%">Email</TD>
                                                <TD class=DataGridHeader align="center" width="23%">Reporter Type</TD>
                                                <TD class=DataGridHeader align="center" width="23%">Query Type</TD>
                                            </TR>
                                            </tbody>
                                        </table>
                                        <table style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc"
                                               border="1" width="100%">
                                            <s:iterator value="partialQueriesList" status="groupStatus">
                                                <tr>
                                                    <td><input type=radio name="queryNumberRadio" value="<s:property value="queryNumber" />"></td>
                                                    <td><s:property value="formattedDateOnly"/></td>
                                                    <td><s:property value="emailAddress"/></td>
                                                    <td><s:property value="reporterType"/></td>
                                                    <td><s:property value="queryType"/></td>
                                                </tr>
                                            </s:iterator>
                                        </table>

                                       </s:else>
                                   </s:if>

								</SPAN></DIV>
                        </TD>
                    </TR>
                    </TBODY>
                </TABLE>
            </TD>
            <TD width="20"><img src="img/spacer.gif" width="20"></TD>
        </TR>
        <TR>
            <TD width="100%" height=20></TD>
        </TR>
        </TBODY>
    </TABLE>

    <table cellspacing="0" cellpadding="0" align="center" border="0" width="100%">
        <tr>
           <s:if test="!hasActionErrors()">
                <s:if test="(partialQueriesList!=null)&&(!partialQueriesList.isEmpty())">
                <td align="center" width="100%"><input type="button" name="open" value="Open"
                                                       onclick="doPartialSubmit();"></td>
                </s:if>
            </s:if>
            <TD height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
            <td align="center"><input type="button" name="close" value="Close" onclick="window.close();"></td>
            <TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
        </tr>
        <tr>
            <TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
        </tr>
    </table>
    <!-- END MAIN BODY SPACE -->
</form>
<!--[if IE]>
<script defer="defer">onload();</script>
<![endif]-->
</BODY>
</HTML>
