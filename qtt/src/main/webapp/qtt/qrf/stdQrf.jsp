<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="html" uri="/struts-tags" %>
<html>
<head>
    <LINK href="css/roche.css" type="text/css" rel="stylesheet">
    <SCRIPT language="javascript" src="js/image_script.js"></script>
    <script language="javascript" src="js/qrf_script.js"></script>
    <script language="javascript" src="js/global_script.js"></script>
    <script language="javascript" src="js/quick_list_script.js"></script>
    <script src="js/jquery.1.10.2.min.js"></script>
    <script language="javascript">

        function removeAttachment(descInput, idInput, descSpan, uploadSpan, number) {
            document.forms[0].idsToDelete.value += ":" + idInput.value;

            descInput.value = "";
            idInput.value = "";

            descSpan.style.display = "none";
            uploadSpan.style.display = "block";

            input1Name = "StdQrfAction_tempFile" + number;
            input1 = document.getElementById(input1Name);
            input1.value = "";

            input2Name = "StdQrfAction_tempFileName" + number;
            input2 = document.getElementById(input2Name)
            input2.value = "";
        }

        function removeAttachmentStandard(idInput, descSpan, uploadSpan, number) {
            document.forms[0].idsToDelete.value += ":" + idInput.value;

            idInput.value = "";

            descSpan.style.display = "none";
            uploadSpan.style.display = "block";


            input1Name = "StdQrfAction_tempFile" + number;
            input1 = document.getElementById(input1Name);
            input1.value = "";

            input2Name = "StdQrfAction_tempFileName" + number;
            input2 = document.getElementById(input2Name);
            input2.value = "";

        }

        var fetchUserFromADAvailable = true;
        function fetchUserFromAD() {
            if (fetchUserFromADAvailable) {
                fetchUserFromADAvailable = false;
                var username = $('#username').val();
                $.ajax({
                    url: "FindUserDetails.action",
                    type: 'POST',
                    data: {username: username},
                    success: function (data) {
                        if (data.errorMsg) {
                            alert(data.errorMsg);
                        } else {
                            $('#firstname').val(data.firstName);
                            $('#lastname').val(data.lastName);
                            $('#email').val(data.email);
                            validateEmail();
                        }
                    },
                    error: function (data) {
                        alert("Error occurred during fetching user details");
                    }
                });
                setTimeout(function() { fetchUserFromADAvailable = true; }, 100);
            }
        }

        function validateEmail() {
            var email = $('#email').val();
            if (email) {
                var domain = email.replace(/.*@/, "").toUpperCase();
                var possibleDomains = $('#possibleEmailDomains').val().toUpperCase().split(", ");
                if (jQuery.inArray(domain, possibleDomains) < 0) {
                    $('#emailErrorMsg').show();
                } else {
                    $('#emailErrorMsg').hide();
                }
            } else {
                $('#emailErrorMsg').hide();
            }
        }

        $( window ).on( "load", validateEmail)

    </script>
    <script language="javascript">

        function initialise() {

            <s:if test="%{!hasActionErrors() && !hasFieldErrors()}">
            //means no errors
            <s:if test="%{(myAction ==null || myAction=='loggedin') && activity!='amd'}">
            //this is std qrf so proceed as normal
            document.forms[0].allDrugs[1].checked = true;
            </s:if><s:else>
            //came here from 'find partial'/'find provisional'/'logged in' so set the fields accordingly
            //document.forms[0].email.disabled = true;
            //set the queryType to disabled if came from 'find provisional'
            <s:if test="%{myAction=='provisional'}">
            document.forms[0].queryType.disabled = true;
            </s:if>

            </s:else>
            //set its value based on the form bean
            drugCheck();
            cumulativeCheck();
            </s:if>
            <s:else>

            //there are errors on this form so set it according to what was submitted

            drugCheck();
            cumulativeCheck();

            //set the queryType to disabled if came from 'find provisional'
            <s:if test="%{myAction=='provisional'}">
            document.forms[0].queryType.disabled = true;
            </s:if>


            </s:else>

            //quick search initialization
            supercombo_1 = new TypeAheadCombo("reqCountry");
            supercombo_2 = new TypeAheadCombo("sourceCountry");
            supercombo_3 = new TypeAheadCombo("retrDrug");
        }
        function onloadFunc() {
            preload();
            initialise();
        }
        window.onload = onloadFunc;
    </script>


    <style type="text/css">


        td.bodyCell {
            BORDER-RIGHT: #28578b 1px solid;
            BORDER-LEFT: #28578b 1px solid;
            BORDER-BOTTOM: #28578b 1px solid
        }

        div.divBox {
            HEIGHT: 100%;
            PADDING-BOTTOM: 10px;
            PADDING-LEFT: 10px;
            PADDING-RIGHT: 10px;
            PADDING-TOP: 7px;
            WIDTH: 95%;
            text-align: justify
        }

        td.titleCell {
            BORDER-TOP: #28578b 1px solid;
            BORDER-LEFT: #28578b 1px solid;
            BORDER-RIGHT: #28578b 1px solid;
            BORDER-BOTTOM: #28578b 3px solid
        }

        .radioSpace {
            margin-left: 2em;
        }

        td.tdLabel {
            width: 30% !important;
        }
    </style>

</head>
<html:form action="/StdQrfAction" target="_self" method="POST" enctype="multipart/form-data"
           acceptcharset="utf-8">
<!-- START MAIN BODY SPACE ALL PAGE CONTENTS GO HERE -->
<input type="hidden" name="iehack" value="&#9760;"/>
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
    <TBODY>
    <tr>
        <td>
            <s:if test="hasActionErrors() || hasFieldErrors()">
                <table style="border: 1px solid rgb(40, 87, 139); padding:3px; width:100%">
                    <tr>
                        <td>
                            <span class="ErrorHeader"> Validation errors detected!  </span>
                            <s:fielderror cssStyle="margin-bottom:0"/>
                            <s:actionerror cssStyle="margin-top:0"/>
                            <span class="ErrorHeader"> Please correct these errors and try again.</span>
                        </td>
                    </tr>
                </table>
            </s:if>
        </td>
    </tr>
    <TR>
        <TD class=clsBodyText vAlign=top colSpan=3>Fields marked with a # are mandatory</TD>
    </TR>
    <TR>
        <TD vAlign=top colSpan=3><IMG height=16 src="img/spacer.gif" width=1></TD>
    </TR>
    <TR>
        <TD width="100%">
            <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
                <TBODY>
                <TR>
                    <TD class="TitleExpanded titleCell" vAlign=center align=left width="100%">
                        <DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Roche/Licensing partner requester details</P></SPAN></DIV>
                    </TD>
                </TR>
                <TR>
                    <TD class="bodyCell" colSpan=2 height="100%" width="80%">
                        <DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 95%; PADDING-TOP: 7px; HEIGHT: 100%"
                             align=justify>
                                    <SPAN>

									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <s:hidden name="userLogged" id="userLogged"/>
                                        <s:if test="!userLogged">
                                            <tr style="padding-bottom: 10em;">
                                                <td width="20%">Username:</td>
                                                <td width="80%">
                                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                        <tr>
                                                            <td width="10%">
                                                                <html:textfield theme="simple" label="" name="username" id="username" size="20" maxlength="25" onfocusout="fetchUserFromAD()"/>
                                                            </td>
                                                            <td width="90%">
                                                                <input type="button" style="margin-left: 5px;" name="findUserButton" value="Fetch User Details" onclick="fetchUserFromAD()">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" colspan="3"><img src="img/spacer.gif" height="10"></td>
                                            </tr>
                                        </s:if>
                                        <tr>
                                            <td>
                                                # First Name:
                                            </td>
                                            <td>
                                                <html:textfield theme="simple" label="" name="firstname" size="20" id="firstname" maxlength="25"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                # Last Name:
                                            </td>
                                            <td>
                                                <html:textfield theme="simple" label="" name="lastname" size="20" id="lastname" maxlength="50"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                # Telephone:
                                            </td>
                                            <td>
                                                <html:textfield theme="simple" label="" name="telephone" size="20" maxlength="25"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"># Email Address:</td>
                                            <td width="80%">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td width="20%">
                                                            <html:textfield theme="simple" label="" name="email"
                                                                            id="email"
                                                                            size="40"
                                                                            maxlength="50"
                                                                            onfocusout="validateEmail()"/>
                                                        </td>
                                                        <td width="80%">
                                                            <span id="emailErrorMsg" name="emailErrorMsg" class="errorMessage" style="display: none">
                                                                Email does not belong to commonly used domains in QTT tool, please check for typo and do not use private email addresses
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <s:hidden name="possibleEmailDomains" id="possibleEmailDomains"/>
                                        </tr>
                                        <tr>
                                            <td>
                                                # Country:
                                            </td>
                                            <td>
                                                <html:select theme="simple" list="orderedCountries"
                                                             listKey="countryCode"
                                                             listValue="description" name="reqCountry"
                                                             headerKey="0" headerValue="" id="reqCountry">
                                                </html:select><span class="rightarrowwhite">&raquo;</span>
                                            </td>
                                        </tr>


                                        <html:textfield label="# Organisation Name" name="orgName" size="50"
                                                        maxlength="60"/>

                                        <html:select label="# Organisation Type" list="organisationTypes"
                                                     listKey="typeId" listValue="type" name="orgType" headerKey="0"
                                                     headerValue=""
                                                     onchange="affiliateAlert();"/>

                                    </table>
                                      </SPAN></DIV>
                    </TD>
                </TR>
                </TBODY>
            </TABLE>
        </TD>
        <TD width="20"><img title="" src="img/spacer.gif" width="20"></TD>
    </TR>
    <TR>
        <TD width="100%" height=9></TD>
    </TR>
    </TBODY>
</TABLE>

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
    <TBODY>
    <TR>
        <TD width="100%">
            <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
                <TBODY>
                <TR>
                    <TD class="TitleExpanded titleCell" vAlign=center align=left width="100%">
                        <DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Query Origin</P></SPAN></DIV>
                    </TD>
                </TR>
                <TR>
                    <TD class="bodyCell" colSpan=2 height="100%">
                        <DIV class="divBox" align=justify><SPAN>


									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <td width="30%">
                                                # Source country of request:
                                            </td>
                                            <td width="70%">
                                                <html:select theme="simple" list="orderedCountries"
                                                             listKey="countryCode"
                                                             listValue="description" name="sourceCountry"
                                                             id="sourceCountry" headerKey="0"
                                                             headerValue=""/>
                                                <span class="rightarrowwhite">&raquo;</span>
                                            </td>
                                        </tr>
                                        <td width="30%" colspan="2"><img title="" src="img/spacer.gif" height="2"></td>
                                        <tr>
                                            <td width="30%"># Reporter Type:</td>
                                            <td width="70%">
                                                <html:select theme="simple" list="reporterTypes"
                                                             listKey="reporterTypeId" listValue="reporterType"
                                                             name="reporterType" headerKey="0" headerValue=""/>
                                            </td>
                                        </tr>
                                    </table>
								</SPAN></DIV>
                    </TD>
                </TR>
                </TBODY>
            </TABLE>
        </TD>
        <TD width="20"><img title="" src="img/spacer.gif" width="20"></TD>
    </TR>
    <TR>
        <TD width="100%" height=9></TD>
    </TR>
    </TBODY>
</TABLE>

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
    <TBODY>
    <TR>
        <TD width="100%">
            <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
                <TBODY>
                <TR>
                    <TD class="TitleExpanded titleCell" vAlign=center align=left width="100%">
                        <DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Drug Details</P></SPAN></DIV>
                    </TD>
                </TR>
                <TR>
                    <TD class="bodyCell" colSpan=2 height="100%">
                        <DIV class="divBox"
                             align=justify><SPAN>
											<table cellspacing="0" cellpadding="1" border="0"
                                                   width="100%">

                                                <!-- to test only -->


                                                <s:radio cssClass="radioSpace" label="All Drugs"
                                                         name="allDrugs" listKey="key" listValue="value"
                                                         list="#{'1':'Yes','0':'No'}" onclick="drugCheck()"/>

                                                <tr>
                                                    <td># Drug Retrieval Name:</td>
                                                    <td width="80%" colspan="2"><html:select
                                                            theme="simple" list="drugRetrievalNames" name="retrDrug"
                                                            id="retrDrug" headerKey="0" id="retrDrug"
                                                            headerValue=""/> <span
                                                            class="rightarrowwhite">&raquo;</span></td>
                                                </tr>

                                                <tr>
                                                    <td>Route:</td>
                                                    <td width="80%" colspan="2"><s:textfield
                                                            onkeypress="textCounter(this,50);"
                                                            theme="simple" name="route" size="50" maxlength="50"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Formulation:</td>
                                                    <td width="80%" colspan="2"><s:textfield
                                                            theme="simple" name="formulation"
                                                            onkeypress="textCounter(this,50);" size="50"
                                                            maxlength="50"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Indication:</td>
                                                    <td width="80%" colspan="2"><s:textfield
                                                            theme="simple" name="indication" size="50"
                                                            onkeypress="textCounter(this,50);" maxlength="50"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Dose:</td>
                                                    <td width="80%" colspan="2"><s:textfield
                                                            theme="simple" name="dose" size="50"
                                                            onkeypress="textCounter(this,50);" maxlength="50"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Other Roche Drugs:</td>
                                                    <td width="80%" colspan="2"><s:textarea id="othersDrug"
                                                                                            theme="simple"
                                                                                            name="othersDrug" rows="4"
                                                                                            cols="25"
                                                                                            onblur="textCounter(this,200);"
                                                                                            onkeypress="textCounter(this,200);"/></td>

                                                </tr>
                                            </table>
										</SPAN></DIV>
                    </TD>
                </TR>
                </TBODY>
            </TABLE>
        </TD>
        <TD width="20"><img title="" src="img/spacer.gif" width="20"></TD>
    </TR>
    <TR>
        <TD width="100%" height=9></TD>
    </TR>
    </TBODY>
</TABLE>

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
    <TBODY>
    <TR>
        <TD width="100%">
            <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
                <TBODY>
                <TR>
                    <TD class="TitleExpanded titleCell" vAlign=center align=left width="100%">
                        <DIV style="WIDTH: 95%; HEIGHT: 100%"><SPAN>
								<P>Response timelines</P></SPAN></DIV>
                    </TD>
                </TR>
                <TR>
                    <TD class="bodyCell" colSpan=2 height="100%">
                        <DIV class="divBox"
                             align=justify><SPAN>


									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <td width="30%"># Date response requested by:</td>
                                            <td width="70%" colspan="4">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <s:textfield name="dateRequested" size="13" maxlength="11"
                                                                         theme="simple"/>
                                                        </td>
                                                        <td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="30%">Urgency:</td>
                                            <td width="70%">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <html:textarea theme="simple" name="urgency" maxlength="300"
                                                                           rows="4"
                                                                           cols="25"
                                                                           onkeypress="textCounter(this,300);"/>
                                                        </td>
                                                        <td width="10" colspan="2"><img title="" src="img/spacer.gif"
                                                                                        height="2"
                                                                                        width="10"></td>
                                                        <td>(required only if 'Date Response Requested By' is &lt; 14
                                                            calendar days from date of request)
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

								</SPAN></DIV>
                    </TD>
                </TR>
                </TBODY>
            </TABLE>
        </TD>
        <TD width="20"><img title="" src="img/spacer.gif" width="20"></TD>
    </TR>
    <TR>
        <TD width="100%" height=9></TD>
    </TR>
    </TBODY>
</TABLE>

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR>
<TD width="100%">
<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR>
    <TD class="TitleExpanded titleCell" vAlign=center align=left width="100%">
        <DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Attachments </P></SPAN></DIV>
    </TD>
</TR>
<TR>
<TD class="bodyCell" colSpan=2 height="100%">
<DIV class="divBox"
     align=justify><SPAN>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                    <html:hidden name="idsToDelete" id="idsToDelete"/>
                                    <s:hidden name="freeDiskSpace" id="freeDiskSpace"/>

                                    <!-- for debug
                                        id=${id} descOne=${descOne} activity=${activity} idOne=${idOne} myAction=${myAction} parameters.myAction[0]=${parameters.myAction[0]} attachmentDTOArray[0]=${attachmentDTOArray[0]}
                                    -->

                                    <!-- section One -->
                                    <s:if test="%{(id!=0 && descOne!=null && #parameters.myAction[0]!=null && #parameters.myAction[0]=='partial' && idOne!= null && idOne!='')}">

                                        <tr id="attachmentOneDesc" style="display:block">
                                            <td width="30%">1) Description:</td>
                                            <td width="20%">
                                                <s:text name="descOne" />
                                                <html:hidden name="idOne"/>
                                            </td>
                                            <td width="50%">
                                                <a target="attachments"
                                                   href='InitiateAttachment.action?index=0&id=<s:property value="id"/>'
                                                   onclick="toggle('img01x')" onmouseover="hilight('img01x')"
                                                   onmouseout="lolight('img01x')"><img name="img01x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a><a
                                                    href="javascript:removeAttachment(document.forms[0].descOne, document.forms[0].idOne, document.all.attachmentOneDesc, document.all.attachmentOneUpload, 1);"><img
                                                    name="img02x" src="img/delete_off.gif"
                                                    onmouseover="hilight('img02x')"
                                                    onmouseout="lolight('img02x')" title="Delete Attachment"
                                                    border="0"/></a>
                                            </td>
                                        </tr>
                                        <tr id="attachmentOneUpload" style="display:none">
                                            <td width="30%">1) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descOne" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileOne" size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:if>
                                    <s:elseif
                                            test="%{(id!=0 && descOne!=null && activity != null && activity == 'amd' && idOne!= null && idOne!='')}">

                                        <tr id="attachmentOneDesc" style="display: block">
                                            <td width="30%">1) Description:</td>
                                            <td width="20%"><s:text name="descOne"/> <html:hidden
                                                    name="idOne"/></td>
                                            <td width="50%">
                                                <a target="attachments"
                                                   href='Attachment.action?attachId=<s:property value="idOne"/>&queryId=<s:property value="id"/>'
                                                   onclick="toggle('img01x')"
                                                   onmouseover="hilight('img01x')"
                                                   onmouseout="lolight('img01x')"><img name="img01x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a>
                                                <a href="javascript:removeAttachment(document.forms[0].descOne, document.forms[0].idOne, document.all.attachmentOneDesc, document.all.attachmentOneUpload, 1);"><img
                                                        name="img02x" src="img/delete_off.gif"
                                                        onmouseover="hilight('img02x')"
                                                        onmouseout="lolight('img02x')" title="Delete Attachment"
                                                        border="0"/></a>
                                            </td>
                                        </tr>
                                        <tr id="attachmentOneUpload" style="display: none">
                                            <td width="30%">1) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descOne" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileOne" size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:elseif>
                                    <s:elseif
                                            test="tempFileName1!=null && tempFileName1!= '' || fileOneFileName!= null && fileOneFileName!=''">

                                        <s:if test="fileOneFileName!= null && fileOneFileName!=''">
                                            <s:if test="tempFile1=fileOne.name"/>
                                            <s:if test="tempFileName1=fileOneFileName"/>
                                        </s:if>
                                        <tr>
                                            <td width="30%">1) Description:</td>
                                            <td width="20%">
                                                <s:textfield theme="simple" name="descOne" size="20"
                                                             maxlength="100"/>
                                                <html:hidden name="idOne"/>
                                            </td>
                                            <td width="50%" id="attachmentOneDesc" style="display:block">
                                                <a target="attachments"
                                                   href='ShowUnsavedAttachment.action?tempfile=<s:property value="tempFile1"/>&&fileName=<s:property value="tempFileName1"/>'
                                                   onclick="toggle('img01x')" onmouseover="hilight('img01x')"
                                                   onmouseout="lolight('img01x')"><img name="img01x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a><a
                                                    href="javascript:removeAttachmentStandard(document.forms[0].idOne, document.all.attachmentOneDesc, document.all.attachmentOneUpload, 1);"><img
                                                    name="img02x" src="img/delete_off.gif"
                                                    onmouseover="hilight('img02x')"
                                                    onmouseout="lolight('img02x')" title="Delete Attachment"
                                                    border="0"/></a>


                                                    <%--<s:property  value="fileOne.name" /><s:property  value="fileOneFileName"/>--%>
                                                <s:hidden name="tempFile1" value="%{tempFile1}"/><s:hidden
                                                    name="tempFileName1" value="%{tempFileName1}"/>

                                            </td>
                                            <td id="attachmentOneUpload" style="display:none"><s:file theme="simple"
                                                                                                      name="fileOne"
                                                                                                      size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:elseif>
                                    <s:else>
                                        <tr>
                                            <td width="30%">1) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descOne" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileOne" value="fileOneFileName"
                                                        size="80" onchange="validateFileSize(this)"/>
                                            </td>
                                        </tr>
                                    </s:else>

                                    <!-- section two -->
                                    <s:if test="%{(id!=0 && descTwo!=null && #parameters.myAction[0]!=null && #parameters.myAction[0]=='partial' && idTwo!= null && idTwo!='')}">
                                        <tr id="attachmentTwoDesc" style="display:block">
                                            <td width="30%">2) Description:</td>
                                            <td width="20%">
                                                <s:text name="descTwo"/>
                                                <html:hidden name="idTwo"/>
                                            </td>
                                            <td width="50%">
                                                <a target="attachments"
                                                   href='InitiateAttachment.action?index=1&id=<s:property value="id"/>'
                                                   onclick="toggle('img11x')" onmouseover="hilight('img11x')"
                                                   onmouseout="lolight('img11x')"><img name="img11x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a><a
                                                    href="javascript:removeAttachment(document.forms[0].descTwo, document.forms[0].idTwo, document.all.attachmentTwoDesc, document.all.attachmentTwoUpload, 2);"><img
                                                    name="img12x" src="img/delete_off.gif"
                                                    onmouseover="hilight('img12x')"
                                                    onmouseout="lolight('img12x')" title="Delete Attachment"
                                                    border="0"/></a>
                                            </td>
                                        </tr>
                                        <tr id="attachmentTwoUpload" style="display:none">
                                            <td width="30%">2) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descTwo" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileTwo" size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:if>
                                    <s:elseif
                                            test="%{(id!=0 && descOne!=null && activity != null && activity == 'amd' && idTwo!= null && idTwo!='')}">
                                        <tr id="attachmentTwoDesc" style="display: block">
                                            <td width="30%">2) Description:</td>
                                            <td width="20%"><s:text name="descTwo"/> <html:hidden name="idTwo"/></td>
                                            <td width="50%">
                                                <a target="attachments"
                                                   href='Attachment.action?attachId=<s:property value="idTwo"/>&queryId=<s:property value="id"/>'
                                                   onclick="toggle('img11x')"
                                                   onmouseover="hilight('img11x')"
                                                   onmouseout="lolight('img11x')"><img name="img11x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a>
                                                <a href="javascript:removeAttachment(document.forms[0].descTwo, document.forms[0].idTwo, document.all.attachmentTwoDesc, document.all.attachmentTwoUpload, 2);"><img
                                                        name="img12x" src="img/delete_off.gif"
                                                        onmouseover="hilight('img12x')"
                                                        onmouseout="lolight('img12x')" title="Delete Attachment"
                                                        border="0"/></a>
                                            </td>
                                        </tr>
                                        <tr id="attachmentTwoUpload" style="display: none">
                                            <td width="30%">2) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descTwo" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileTwo" size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:elseif>
                                    <s:elseif
                                            test="tempFileName2!=null && tempFileName2!= '' || fileTwoFileName!= null && fileTwoFileName!=''">
                                        <s:if test="fileTwoFileName!= null && fileTwoFileName!=''">
                                            <%--set does not work if dest variable not null, so using the below--%>
                                            <s:if test="tempFile2=fileTwo.name"/>
                                            <s:if test="tempFileName2=fileTwoFileName"/>
                                        </s:if>
                                        <tr>
                                            <td width="30%">2) Description:</td>
                                            <td width="20%">
                                                <s:textfield theme="simple" name="descTwo" size="20"
                                                             maxlength="100"/>
                                                <html:hidden name="idTwo"/>
                                            </td>
                                            <td width="50%" id="attachmentTwoDesc" style="display:block">
                                                <a target="attachments"
                                                   href='ShowUnsavedAttachment.action?tempfile=<s:property value="tempFile2"/>&&fileName=<s:property value="tempFileName2"/>'
                                                   onclick="toggle('img11x')" onmouseover="hilight('img11x')"
                                                   onmouseout="lolight('img11x')"><img name="img11x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a><a
                                                    href="javascript:removeAttachmentStandard(document.forms[0].idTwo, document.all.attachmentTwoDesc, document.all.attachmentTwoUpload, 2);"><img
                                                    name="img12x" src="img/delete_off.gif"
                                                    onmouseover="hilight('img12x')"
                                                    onmouseout="lolight('img12x')" title="Delete Attachment"
                                                    border="0"/></a>
                                                <s:hidden name="tempFile2" value="%{tempFile2}"/><s:hidden
                                                    name="tempFileName2" value="%{tempFileName2}"/>
                                            </td>
                                            <td id="attachmentTwoUpload" style="display:none"><s:file theme="simple"
                                                                                                      name="fileTwo"
                                                                                                      size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:elseif>
                                    <s:else>
                                        <tr>
                                            <td width="30%">2) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descTwo" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileTwo" size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:else>

                                    <!-- section three-->
                                    <s:if test="%{(id!=0 && descThree!=null && #parameters.myAction[0]!=null && #parameters.myAction[0]=='partial' && idThree!= null && idThree!='')}">
                                        <tr id="attachmentThreeDesc" style="display:block">
                                            <td width="30%">3) Description:</td>
                                            <td width="20%">
                                                <s:text name="descThree"/>
                                                <html:hidden name="idThree"/>
                                            </td>
                                            <td width="50%">
                                                <a target="attachments"
                                                   href='InitiateAttachment.action?index=2&id=<s:property value="id"/>'
                                                   onclick="toggle('img21x')" onmouseover="hilight('img21x')"
                                                   onmouseout="lolight('img21x')"><img name="img21x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a><a
                                                    href="javascript:removeAttachment(document.forms[0].descThree, document.forms[0].idThree, document.all.attachmentThreeDesc, document.all.attachmentThreeUpload, 3);"><img
                                                    name="img22x" src="img/delete_off.gif"
                                                    onmouseover="hilight('img22x')"
                                                    onmouseout="lolight('img22x')" title="Delete Attachment"
                                                    border="0"/></a>
                                            </td>
                                        </tr>
                                        <tr id="attachmentThreeUpload" style="display:none">
                                            <td width="30%">3) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descThree" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileThree" size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:if>
                                    <s:elseif
                                            test="%{(id!=0 && descThree!=null && activity != null && activity == 'amd' && idThree!= null && idThree!='')}">
                                        <tr id="attachmentThreeDesc" style="display: block">
                                            <td width="30%">3) Description:</td>
                                            <td width="20%"><s:text name="descThree"/> <html:hidden
                                                    name="idThree"/></td>
                                            <td width="50%">
                                                <a target="attachments"
                                                   href='Attachment.action?attachId=<s:property value="idThree"/>&queryId=<s:property value="id"/>'
                                                   onclick="toggle('img21x')"
                                                   onmouseover="hilight('img21x')"
                                                   onmouseout="lolight('img21x')"><img name="img21x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a>
                                                <a href="javascript:removeAttachment(document.forms[0].descThree, document.forms[0].idThree, document.all.attachmentThreeDesc, document.all.attachmentThreeUpload, 3);"><img
                                                        name="img22x" src="img/delete_off.gif"
                                                        onmouseover="hilight('img22x')"
                                                        onmouseout="lolight('img22x')" title="Delete Attachment"
                                                        border="0"/></a>
                                            </td>
                                        </tr>
                                        <tr id="attachmentThreeUpload" style="display: none">
                                            <td width="30%">3) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descThree" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileThree" size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:elseif>
                                    <s:elseif
                                            test="tempFileName3!=null && tempFileName3!= '' || fileThreeFileName!= null && fileThreeFileName!=''">
                                        <s:if test="fileThreeFileName!= null && fileThreeFileName!=''">
                                            <%--set does not work if dest variable not null, so using the below--%>
                                            <s:if test="tempFile3=fileThree.name"/>
                                            <s:if test="tempFileName3=fileThreeFileName"/>
                                        </s:if>


                                        <tr>
                                            <td width="30%">3) Description:</td>
                                            <td width="20%">
                                                <s:textfield theme="simple" name="descThree" size="20"
                                                             maxlength="100"/>
                                                <html:hidden name="idThree"/>
                                            </td>
                                            <td id="attachmentThreeDesc" style="display:block" width="50%">
                                                <a target="attachments"
                                                   href='ShowUnsavedAttachment.action?tempfile=<s:property value="tempFile3"/>&&fileName=<s:property value="tempFileName3"/>'
                                                   onclick="toggle('img21x')" onmouseover="hilight('img21x')"
                                                   onmouseout="lolight('img21x')"><img name="img21x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a><a
                                                    href="javascript:removeAttachmentStandard(document.forms[0].idThree, document.all.attachmentThreeDesc, document.all.attachmentThreeUpload, 3);"><img
                                                    name="img22x" src="img/delete_off.gif"
                                                    onmouseover="hilight('img22x')"
                                                    onmouseout="lolight('img22x')" title="Delete Attachment"
                                                    border="0"/></a>
                                                <s:hidden name="tempFile3" value="%{tempFile3}"/><s:hidden
                                                    name="tempFileName3" value="%{tempFileName3}"/>
                                            </td>
                                            <td id="attachmentThreeUpload" style="display:none"><s:file theme="simple"
                                                                                                        name="fileThree"
                                                                                                        size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:elseif>
                                    <s:else>
                                        <tr>
                                            <td width="30%">3) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descThree" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileThree" size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:else>

                                    <!--section four-->
                                    <s:if test="%{(id!=0 && descFour!=null && #parameters.myAction[0]!=null && #parameters.myAction[0]=='partial' && idFour!= null && idFour!='')}">
                                        <tr id="attachmentFourDesc" style="display:block">
                                            <td width="30%">4) Description:</td>
                                            <td width="20%">
                                                <s:text name="descFour"/>
                                                <html:hidden name="idFour"/>
                                            </td>
                                            <td width="50%">
                                                <a target="attachments"
                                                   href='InitiateAttachment.action?index=3&id=<s:property value="id"/>'
                                                   onclick="toggle('img31x')" onmouseover="hilight('img31x')"
                                                   onmouseout="lolight('img31x')"><img name="img31x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a><a
                                                    href="javascript:removeAttachment(document.forms[0].descFour, document.forms[0].idFour, document.all.attachmentFourDesc, document.all.attachmentFourUpload, 4);"><img
                                                    name="img32x" src="img/delete_off.gif"
                                                    onmouseover="hilight('img32x')"
                                                    onmouseout="lolight('img32x')" title="Delete Attachment"
                                                    border="0"/></a>
                                            </td>
                                        </tr>
                                        <tr id="attachmentFourUpload" style="display:none">
                                            <td width="30%">4) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descFour" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileFour" size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:if>
                                    <s:elseif
                                            test="%{(id!=0 && descFour!=null && activity != null && activity == 'amd' && idFour!= null && idFour!='')}">
                                        <tr id="attachmentFourDesc" style="display: block">
                                            <td width="30%">4) Description:</td>
                                            <td width="20%"><s:text name="descFour"/> <html:hidden
                                                    name="idFour"/></td>
                                            <td width="50%">
                                                <a target="attachments"
                                                   href='Attachment.action?attachId=<s:property value="idFour"/>&queryId=<s:property value="id"/>'
                                                   onclick="toggle('img31x')"
                                                   onmouseover="hilight('img31x')"
                                                   onmouseout="lolight('img31x')"><img name="img31x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a>
                                                <a href="javascript:removeAttachment(document.forms[0].descFour, document.forms[0].idFour, document.all.attachmentFourDesc, document.all.attachmentFourUpload, 4);"><img
                                                        name="img32x" src="img/delete_off.gif"
                                                        onmouseover="hilight('img32x')"
                                                        onmouseout="lolight('img32x')" title="Delete Attachment"
                                                        border="0"/></a>
                                            </td>
                                        </tr>
                                        <tr id="attachmentFourUpload" style="display: none">
                                            <td width="30%">4) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descFour" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileFour" size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:elseif>
                                    <s:elseif
                                            test="tempFileName4!=null && tempFileName4!= '' || fileFourFileName!= null && fileFourFileName!=''">
                                        <s:if test="fileFourFileName!= null && fileFourFileName!=''">
                                            <%--set does not work if dest variable not null, so using the below--%>
                                            <s:if test="tempFile4=fileFour.name"/>
                                            <s:if test="tempFileName4=fileFourFileName"/>
                                        </s:if>
                                        <tr>
                                            <td width="30%">4) Description:</td>
                                            <td width="20%">
                                                <s:textfield theme="simple" name="descFour" size="20" maxlength="100"/>
                                                <html:hidden name="idFour"/>
                                            </td>
                                            <td id="attachmentFourDesc" style="display:block" width="50%">
                                                <a target="attachments"
                                                   href='ShowUnsavedAttachment.action?tempfile=<s:property value="tempFile4"/>&&fileName=<s:property value="tempFileName4"/>'
                                                   onclick="toggle('img31x')" onmouseover="hilight('img31x')"
                                                   onmouseout="lolight('img31x')"><img name="img31x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a><a
                                                    href="javascript:removeAttachmentStandard(document.forms[0].idFour, document.all.attachmentFourDesc, document.all.attachmentFourUpload, 4);"><img
                                                    name="img32x" src="img/delete_off.gif"
                                                    onmouseover="hilight('img32x')"
                                                    onmouseout="lolight('img32x')" title="Delete Attachment"
                                                    border="0"/></a>
                                                <s:hidden name="tempFile4" value="%{tempFile4}"/><s:hidden
                                                    name="tempFileName4" value="%{tempFileName4}"/>
                                            </td>
                                            <td id="attachmentFourUpload" style="display:none"><s:file theme="simple"
                                                                                                       name="fileFour"
                                                                                                       size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:elseif>
                                    <s:else>
                                        <tr>
                                            <td width="30%">4) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descFour" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileFour" size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:else>

                                    <!-- section five-->
                                    <s:if test="%{(id!=0 && descFive!=null && #parameters.myAction[0]!=null && #parameters.myAction[0]=='partial' && idFive!= null && idFive!='')}">
                                        <tr id="attachmentFiveDesc" style="display:block">
                                            <td width="30%">5) Description:</td>
                                            <td width="20%">
                                                <s:text name="descFive"/>
                                                <html:hidden name="idFive"/>
                                            </td>
                                            <td width="50%">
                                                <a target="attachments"
                                                   href='InitiateAttachment.action?index=4&id=<s:property value="id"/>'
                                                   onclick="toggle('img41x')" onmouseover="hilight('img41x')"
                                                   onmouseout="lolight('img41x')"><img name="img41x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a><a
                                                    href="javascript:removeAttachment(document.forms[0].descFive, document.forms[0].idFive, document.all.attachmentFiveDesc, document.all.attachmentFiveUpload, 5);"><img
                                                    name="img42x" src="img/delete_off.gif"
                                                    onmouseover="hilight('img42x')"
                                                    onmouseout="lolight('img42x')" title="Delete Attachment"
                                                    border="0"/></a>
                                            </td>
                                        </tr>
                                        <tr id="attachmentFiveUpload" style="display:none">
                                            <td width="30%">5) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descFive" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileFive" size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:if>
                                    <s:elseif
                                            test="%{(id!=0 && descFive!=null && activity != null && activity == 'amd' && idFive!= null && idFive!='')}">
                                        <tr id="attachmentFiveDesc" style="display: block">
                                            <td width="30%">5) Description:</td>
                                            <td width="20%"><s:text name="descFive"/> <html:hidden
                                                    name="idFive"/></td>
                                            <td width="50%">
                                                <a target="attachments"
                                                   href='Attachment.action?attachId=<s:property value="idFive"/>&queryId=<s:property value="id"/>'
                                                   onclick="toggle('img41x')"
                                                   onmouseover="hilight('img41x')"
                                                   onmouseout="lolight('img41x')"><img name="img41x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a>
                                                <a href="javascript:removeAttachment(document.forms[0].descFive, document.forms[0].idFive, document.all.attachmentFiveDesc, document.all.attachmentFiveUpload, 5);"><img
                                                        name="img42x" src="img/delete_off.gif"
                                                        onmouseover="hilight('img42x')"
                                                        onmouseout="lolight('img42x')" title="Delete Attachment"
                                                        border="0"/></a>
                                            </td>
                                        </tr>
                                        <tr id="attachmentFiveUpload" style="display: none">
                                            <td width="30%">5) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descFive" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileFive" size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:elseif>
                                    <s:elseif
                                            test="tempFileName5!=null && tempFileName5!= '' || fileFiveFileName!= null && fileFiveFileName!=''">
                                        <s:if test="fileFiveFileName!= null && fileFiveFileName!=''">
                                            <%--set does not work if dest variable not null, so using the below--%>
                                            <s:if test="tempFile5=fileFive.name"/>
                                            <s:if test="tempFileName5=fileFiveFileName"/>
                                        </s:if>
                                        <tr>
                                            <td width="30%">5) Description:</td>
                                            <td width="20%">
                                                <s:textfield theme="simple" name="descFive" size="20"
                                                             maxlength="100"/>
                                                <html:hidden name="idFive"/>
                                            </td>
                                            <td width="50%" id="attachmentFiveDesc" style="display:block">
                                                <a target="attachments"
                                                   href='ShowUnsavedAttachment.action?tempfile=<s:property value="tempFile5"/>&&fileName=<s:property value="tempFileName5"/>'
                                                   onclick="toggle('img41x')" onmouseover="hilight('img41x')"
                                                   onmouseout="lolight('img41x')"><img name="img41x"
                                                                                       src="img/find_off.gif"
                                                                                       title="View/Open Document"
                                                                                       border="0"/></a><a
                                                    href="javascript:removeAttachmentStandard(document.forms[0].idFive, document.all.attachmentFiveDesc, document.all.attachmentFiveUpload, 5);"><img
                                                    name="img42x" src="img/delete_off.gif"
                                                    onmouseover="hilight('img42x')"
                                                    onmouseout="lolight('img42x')" title="Delete Attachment"
                                                    border="0"/></a>
                                                <s:hidden name="tempFile5" value="%{tempFile5}"/><s:hidden
                                                    name="tempFileName5" value="%{tempFileName5}"/>
                                            </td>
                                            <td id="attachmentFiveUpload" style="display:none"><s:file theme="simple"
                                                                                                       name="fileFive"
                                                                                                       size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:elseif>
                                    <s:else>
                                        <tr>
                                            <td width="30%">5) Description:</td>
                                            <td width="20%"><s:textfield theme="simple" name="descFive" size="20"
                                                                         maxlength="100"/></td>
                                            <td><s:file theme="simple" name="fileFive" size="80" onchange="validateFileSize(this)"/></td>
                                        </tr>
                                    </s:else>


                                    </table>

								</SPAN></DIV>
</TD>
</TR>
</TBODY>
</TABLE>
</TD>
<TD width="20"><img title="" src="img/spacer.gif" width="20"></TD>
</TR>
<TR>
    <TD width="100%" height=9></TD>
</TR>
</TBODY>
</TABLE>

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
    <TBODY>
    <TR>
        <TD width="100%">
            <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
                <TBODY>
                <TR>
                    <TD class="TitleExpanded titleCell" vAlign=center align=left width="100%">
                        <DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Cumulative details</P></SPAN></DIV>
                    </TD>
                </TR>
                <TR>
                    <TD class="bodyCell" colSpan=2 height="100%">
                        <DIV class="divBox"
                             align=justify><SPAN>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <s:radio label="Cumulative period" name="cumulative"
                                                 list="#{'1':'Yes', '0':'No'}" onclick="cumulativeCheck()"
                                                 cssClass="radioSpace"/>
                                        <tr>
                                            <td>
                                                <i>If no please provide the dates:</i>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Date From:
                                            </td>
                                            <td width="70%" colspan="4">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <s:textfield name="from" size="13" maxlength="11"
                                                                         theme="simple"/>
                                                        </td>
                                                        <td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>


                                        <tr>

                                            <td>
                                                Date To:
                                            </td>
                                            <td width="70%" colspan="4">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <s:textfield name="to" size="13" maxlength="11"
                                                                         theme="simple"/>
                                                        </td>
                                                        <td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <s:textfield label="Interval" name="interval" size="52"
                                                     onkeypress="textCounter(this,60);" maxlength="60"/>

                                    </table>

								</SPAN></DIV>
                    </TD>
                </TR>
                </TBODY>
            </TABLE>
        </TD>
        <TD width="20"><img title="" src="img/spacer.gif" width="20"></TD>
    </TR>
    <TR>
        <TD width="100%" height=9></TD>
    </TR>
    </TBODY>
</TABLE>

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
    <TBODY>
    <TR>
        <TD width="100%">
            <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
                <TBODY>
                <TR>
                    <TD class="TitleExpanded titleCell" vAlign=center align=left width="100%">
                        <DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Query details</P></SPAN></DIV>
                    </TD>
                </TR>
                <TR>
                    <TD class="bodyCell" colSpan=2 height="100%">
                        <DIV class="divBox"
                             align=justify><SPAN>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <td width="30%"># Query Type:</td>
                                            <td width="70%" colspan="2">
                                                <s:select theme="simple" list="dictionary.queryTypes"
                                                          listKey="queryTypeCode"
                                                          listValue="queryType" name="queryType" headerKey=""
                                                          headerValue=""/>
                                            </td>
                                        </tr>
                                    </table>
								</SPAN></DIV>
                    </TD>
                </TR>
                </TBODY>
            </TABLE>
        </TD>
        <TD width="20"><img title="" src="img/spacer.gif" width="20"></TD>
    </TR>
    <TR>
        <TD width="100%" height=20></TD>
    </TR>
    </TBODY>
</TABLE>

<table cellspacing="0" cellpadding="0" border="0" align="center">
    <tr>
        <td align="center">
            <input type="button" name="save" value="Next" onclick="enable(); ">
        </td>
    </tr>
    <tr>
        <TD height="20" colspan="5"><img title="" src="img/spacer.gif" height="20"></TD>
    </tr>
</table>


<s:hidden name="id"/>
<s:hidden name="activity" value="%{activity}"/>
<s:hidden name="myAction" value="%{myAction}"/>
</html:form>
<!--[if IE]>
<script defer="defer">onload();</script>
<![endif]-->
</html>
