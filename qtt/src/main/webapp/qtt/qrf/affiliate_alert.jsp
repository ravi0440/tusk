<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Affiliate Alert</title>
<link href="css/dialog.css" type=text/css rel=stylesheet>
<script language="javascript" src="js/global_script.js"></script>
</head>
<body>
<div class="DialogMessage">Any queries that do not follow SOP-0111914 will be rejected by the Drug Safety Contact Line.<br />
 Only requests from a Regulatory Authority, Legal, Media, Drug Safety Monitoring Board or Ethics Committee, or queries for database searches for local data and reconciliation purposes, may be submitted.</div>
<table>
	<tr>
		<td width="28%">&nbsp;</td>
		<td width="22%"><input type="button" name="ok" value="OK" class="button" onclick="window.close();"
			onmouseover="hover(this,'buttonhover')"
			onmouseout="hover(this,'button')"></td>
		<td width="28%">&nbsp;</td>
	</tr>
</table>
</body>
</html>
