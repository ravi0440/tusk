<%@ taglib prefix="s" uri="/struts-tags" %>

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
    <TBODY>
    <TR>
        <TD width="100%">
            <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
                <TBODY>
                <TR>
                    <TD class=TitleExpanded
                        style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid"
                        vAlign=center align=left width="100%">
                        <DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Query Answer Information</P></SPAN></DIV>
                    </TD>
                </TR>
                <TR>
                    <TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid"
                        colSpan=2 height="100%">
                        <DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%"
                             align=justify><SPAN>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <td valign="top" colspan="3"><font size="+1">Query No. <s:property
                                                    value="queryNumber"/></font></td>
                                        </tr>
                                        <tr>
                                            <TD height="20" colspan="3"><img src="img/spacer.gif" height="20"></TD>
                                        </tr>
                                        <tr>
                                            <td><b>Drug Name</b></td>
                                            <td colspan="2"><s:property value="retrDrug"/></td>
                                        </tr>
                                        <tr>
                                            <td><b>Query Type</b></td>
                                            <td colspan="2"><s:property value="queryType"/></td>
                                        </tr>
                                        <tr>
                                            <TD height="20" colspan="3"><img src="img/spacer.gif" height="20"></TD>
                                        </tr>
                                    </table>
									<TABLE class=DataGrid
                                           style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc"
                                           borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
                                        <TBODY>
                                        <TR class=DataGridHeader>
                                            <TD class=DataGridHeader style="WIDTH: 65%">Response Document Name</TD>
                                            <TD class=DataGridHeader style="WIDTH: 20%">Date Deposited</TD>
                                            <TD class=DataGridHeader style="WIDTH: 15%">Deposited By</TD>
                                            <TD class=DataGridHeader style="WIDTH: 30"></TD>
                                        </TR>


                                        <s:iterator id="att" value="finalAttachments" var="i" status="rowstatus">
                                            <TR>
                                                <TD class=DataGridItem style="VERTICAL-ALIGN: middle">
                                                    &nbsp;<s:property value="title"/></TD>
                                                <TD class=DataGridItem style="VERTICAL-ALIGN: middle">
                                                    &nbsp;<s:date name="createTs.toDateTime().toDate()" format="dd-MMM-yyyy"/>
                                                </TD>
                                                <TD class=DataGridItem style="VERTICAL-ALIGN: middle">
                                                    &nbsp;<s:property value="author"/></TD>
                                                <TD class=DataGridItem valign="middle" align="center"><a
                                                        target="attachments"
                                                        href="ShowFinalAttachment.action?id=<s:property value="mid"/>&key=<s:property value="key"/>&attachId=<s:property value="attachmentSeq"/>"
                                                        onmouseover="hilight('imgget<s:property value="#rowstatus.index" />')"
                                                        onmouseout="lolight('imgget<s:property value="#rowstatus.index" />')"><img
                                                        name="imgget<s:property value="#rowstatus.index" />"
                                                        src="img/find_off.gif" title="View/Open Document" border="0"></a>
                                                </TD>
                                            </TR>
                                        </s:iterator>
                                        </TBODY>
                                    </TABLE>
								</SPAN></DIV>
                    </TD>
                </TR>
                </TBODY>
            </TABLE>
        </TD>
        <TD width="20"><img src="img/spacer.gif" width="20"></TD>
    </TR>
    <TR>
        <TD width="100%" height=20></TD>
    </TR>
    </TBODY>
</TABLE>