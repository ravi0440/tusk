<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<body onload="preload(); window.setInterval('location.reload()', 120000);" onunload="window.clearInterval();">
<script language="javascript" src="js/window.js"></script>
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
	<tbody>
		<tr>
			<td width="100%">
				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td>
								<table class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=1 border=1>
									<tbody>
										<tr class=DataGridHeader>
											<td class=DataGridHeader style="WIDTH: 20"></td>
											<td class=DataGridHeadersmall style="WIDTH: 8%" valign="top"><a class=BlindHeaderLink href="SortQueryList.action?sort=0&last=<s:property value="last"/>&email=<s:property value="email"/>" ><s:text name="query.query#" /></a></td>
											<td class=DataGridHeadersmall style="WIDTH: 20%" valign="top"><a class=BlindHeaderLink href="SortQueryList.action?sort=1&&last=<s:property value="last"/>&email=<s:property value="email"/>" ><s:text name="drug.name" /></a>
												<div class=DataGridHeadersmaller>
												Query Label
												</div>
											</td>
											<td class=DataGridHeadersmall style="WIDTH: 20%" valign="top"><a class=BlindHeaderLink href="SortQueryList.action?sort=2&last=<s:property value="last"/>&email=<s:property value="email"/>" ><s:text name="query.type" /></a></td>
											<td class=DataGridHeadersmall style="WIDTH: 10%" valign="top"><a class=BlindHeaderLink href="SortQueryList.action?sort=3&last=<s:property value="last"/>&email=<s:property value="email"/>" ><s:text name="date.submit" /></a></td>
											<td class=DataGridHeadersmall style="WIDTH: 10%" valign="top"><a class=BlindHeaderLink href="SortQueryList.action?sort=4&last=<s:property value="last"/>&email=<s:property value="email"/>" ><s:text name="date.resp" /></a></td>
											<td class=DataGridHeadersmall style="WIDTH: 10%" valign="top"><a class=BlindHeaderLink href="SortQueryList.action?sort=5&last=<s:property value="last"/>&email=<s:property value="email"/>" ><s:text name="activity" /></a></td>
											<td class=DataGridHeader style="WIDTH: 20"></td>
											<td class=DataGridHeader style="WIDTH: 20"></td>
											<td class=DataGridHeader style="WIDTH: 20"></td>
										</tr>

								<!-- start loop -->
										<s:iterator value="queryList" status="stat">
										<tr>
											<td class="Smalltext" borderColor=#6699cc valign="middle" align="center">
												<s:if test="urgency">
													<img name="img01" src="img/urgent_2_off.gif" alt="This query is flagged as URGENT" border="0" title="This query is flagged as URGENT">
												</s:if>
											</td>

											<td class="Smalltext" borderColor=#6699cc valign="middle"><s:property value="querySeq"/></td>
									 		<td class="Smalltext" borderColor=#6699cc>
                                                 <s:if test="%{drugName != 'null'}">
									 			    <s:property value="drugName" />
                                                 </s:if>
                                                <div class="Smallertext">
                                                    <s:if test="%{queryLabel != 'null'}">
														<script language="javascript">
															document.write(showLabel('<s:property value="queryLabel"/>', '<s:property value="querySeq"/>'));
														</script>
                                                    </s:if>
                                                </div>
									 		</td>
											<td class="Smalltext" borderColor=#6699cc>
												<s:property value="queryType"/>
											</td>

											<td class="Smalltext" borderColor=#6699cc><s:property value="initiationDateFormatted"/></td>
											<td class="Smalltext" borderColor=#6699cc><s:property value="responseDateFormatted"/></td>
											<td class="Smalltext" borderColor=#6699cc><s:property value="statusCode"/></td>

											<td class="Smalltext" borderColor=#6699cc valign="middle" align="center">
												<s:if test="displayContact">
												    <a href="#" onclick="document.getElementById('mailframe').contentWindow.location='mailto:<s:property value="requesterEmail"/>?subject=<s:property value="queryNumber"/>&cc=<s:property value="dsclEmail"/>';"><img src="img/mail_off.gif" alt="Send Question to DSCL" title="Send Question to DSCL" border="0"></a>
												</s:if>
											</td>

											<td class="Smalltext" borderColor=#6699cc valign="middle" align="center">
												<s:if test="displayClosed">
												    <a href="ClosedQueryDetails.action?queryId=<s:property value="querySeq" />" onmouseover="hilight('imgd<s:property value="#stat.index" />')" onmouseout="lolight('imgd<s:property value="#stat.index" />')">
                                                        <img name="imgd<s:property value="#stat.index" />" src="img/find_off.gif" alt="View Closed Query Details" title="Closed Query Preview" border="0"></a>
												</s:if>
                                                <s:else>
                                                    <a href="QueryReview.action?queryId=<s:property value="querySeq" />" onmouseover="hilight('imgd<s:property value="#stat.index" />')" onmouseout="lolight('imgd<s:property value="#stat.index" />')">
                                                        <img name="imgd<s:property value="#stat.index" />" src="img/find_off.gif" alt="View Query Details" title="Query Preview" border="0">
                                                    </a>
                                                </s:else>
											</td>

											<td class="Smalltext" borderColor=#6699cc valign="middle" align="center">
												<a href="AddAdditionalAttachment.action?queryId=<s:property value="querySeq"/>" onmouseover="hilight('imga<s:property value="#stat.index" />')" onmouseout="lolight('imga<s:property value="#stat.index" />')"><img name="imga<s:property value="#stat.index" />" src="img/add_off.gif" title="Add additional attachment" border="0"></a>
												<%--<a href="#" onclick="document.getElementById('mailframe').contentWindow.location='mailto:<s:property value="requesterEmail"/>?subject=<s:property value="queryNumber"/>&cc=<s:property value="dsclEmail"/>';"><img src="img/mail_off.gif" alt="Send Question to DSCL" title="Send Question to DSCL" border="0"></a>--%>
											</td>

										</tr>

										</s:iterator>
									<!-- end loop -->
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=20></td>
		</tr>
  	</tbody>
</table>
<iframe width="0" height="0" name="mailframe" id="mailframe"></iframe>
</body>