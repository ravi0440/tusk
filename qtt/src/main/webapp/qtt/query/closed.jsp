<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<body onLoad="preload()">
<s:set name="sourceSearchString" value="%{''}"/>												
<s:if test='#parameters.sourceSearch[0] == "repositorySearch"'>
	<s:set name="sourceSearchString" value="%{'&sourceSearch=repositorySearch'}"/>													
</s:if>
<s:elseif test='#parameters.sourceSearch[0] == "quickSearch"' >
	<s:set name="sourceSearchString" value="%{'&sourceSearch=quickSearch'}"/>
</s:elseif>	
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<tbody>
  	
  		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%"><span>
								<p>Information</p></span></div>
							</td>
						</tr>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><span>
								<p>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Query No. <s:property value="query.queryNumber" />  <s:property value="query.followupNumber" /></font></td>
										</tr>

										<tr>
											<td valign="top" colspan="3">
											<s:if test="query.followupQueries.size() > 0">
												<table cellspacing="0" cellpadding="1" border="0" width="100%">
												<TBODY>
													<s:iterator value="query.followupQueries">
													<TR>
														<TD style="WIDTH: 25%">&nbsp;</TD>
														<TD style="WIDTH: 10%">
														<a href="javascript:onclick=openQueryFollowup(<s:property value="query.querySeq" />, <s:property value="id.followupSeq" />);"><s:property value="followupNumber" /></a>
														</TD>
												 		<TD style="WIDTH: 65%">&nbsp;</TD>
													</TR>
													</s:iterator>
												</TBODY>
											</TABLE>
											</s:if>
											</td>
										</tr>

										<tr>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="15%"><b>Drug Name</b></td>
											<td width="45%"><s:property value="query.drugName" /></td>
											<td width="40%">&nbsp;</td>
										</tr>
										<tr>
											<td><b>Query Label</b></td>
											<td><s:property value="query.queryLabel" /></td>
											<td>
												<s:if test="dscl">
													<b><a href="EnterQueryLabel.action?queryId=<s:property value="query.querySeq" /><s:property value="#sourceSearchString"/>" class="Anchor" >Change</a> query label...</b>
												</s:if>
											</td>
										</tr>

										<tr>
											<td><b>Response Date</b></td>
											<td><s:property value="query.currentDueDateFormatted"/></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><b>Query Type</b></td>
											<td><s:property value="query.queryType.queryType"/></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><b>Requester</b></td>
											<td><s:property value="query.requester.name"/></td>
											<td>&nbsp;</td>
										</tr>

										<tr>
											<td><b>View QRF</b></td>
											<td>
												<a href="javascript:onclick=openQrfNoAttachments('<s:property value="query.querySeq"/>');" onmouseover="hilight('img00')" onmouseout="lolight('img00')"><img name="img00" src="img/find_off.gif" title="" border="0"></a>
											</td>
											<td>
												<s:if test="dscl">
													<b><a href="EnterEmailResponse.action?queryId=<s:property value="query.querySeq"/>&re=y&sourceSearch=<s:property value="sourceSearch"/>" class="Anchor" >Re-send</a> electronic query response to requester...</b>
												</s:if>
											</td>
										</tr>

										<s:if test="query.queryStatusCode.statusCode == 'CLOS' && dscl">
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>																										
												<s:if test="query.openFollowupAttachExist">
                                                    <b><a href="CloseFollowupAttachment.action?queryId=<s:property value="query.querySeq"/><s:property value="#sourceSearchString"/>" class="AnchorAlert" >Close Follow-up Attachment</a> to opened query...</b>
												</s:if>
												<s:else>
                                                    <b><a href="OpenFollowupAttachment.action?queryId=<s:property value="query.querySeq"/><s:property value="#sourceSearchString"/>" class="Anchor" >Open Follow-up</a> to attach documents to close query...</b>
												</s:else>
											</td>
										</tr>
										</s:if>

										<tr>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<s:if test="query.queryStatusCode.statusCode == 'CLOS' && !affiliate">
										<tr>
											<td colspan="2">&nbsp;</td>
											<td>
													<b><a href="ReopenQueryConfirm.action?queryId=<s:property value="query.querySeq" />" class="Anchor" >Re-open</a> the closed query for follow-up...</b>
											</td>
										</tr>
										</s:if>

										<tr>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="15%"><b>Expiry date</b></td>
											<td width="45%"><s:property value="query.expiryDateFormatted"/></td>
											<td width="40%">&nbsp;</td>
										</tr>
										<tr>
											<td width="15%"><b>Final Response visible to Affiliate</b></td>
                                            <td width="45%">
                                                <s:if test="query.affiliateAccess=='N'">false</s:if>
												<s:else>true</s:else>
											</td>
											<td width="40%">
												<s:if test="dscl">
													<b><a href="EnterExpiryDate.action?queryId=<s:property value="query.querySeq" />&sourceSearch=<s:property value="sourceSearch"/>" class="Anchor" >Change</a> expiry date, affiliate access...</b>
												</s:if>
											</td>
										</tr>

										<tr>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>

									</table>


									<s:if test="!query.followupEmpty">
										<table class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
											<tbody>
												<TR class=DataGridHeaderTitle>
													<TD  colspan="6">Attach Documents</TD>
												</TR>
												<TR class=DataGridHeader>
													<TD class=DataGridHeader style="WIDTH: 10%">Follow Up</TD>
													<td class=DataGridHeader style="WIDTH: 10%">Date Deposited</td>
													<td class=DataGridHeader style="WIDTH: 10%">Category</td>
													<td class=DataGridHeader style="WIDTH: 50%">Document Name</td>
													<td class=DataGridHeader style="WIDTH: 10%">Deposited By</td>
													<td class=DataGridHeader style="WIDTH: 30"></td>
												</tr>
												<s:iterator value="query.attachmentsForDisplay" status="stat">
													<tr>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="fuQueryNumber"/></td>
												 		<td class=DataGridItem style="VERTICAL-ALIGN: middle"><joda:format value="${createTs}" pattern="dd-MMM-yyyy" locale="en"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="attachmentCategory.attachmentCategory"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="filenameUpper"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="author"/></td>
														<td class=DataGridItem valign="middle" align="center">
                                                        <a target="attachments" href="Attachment.action?attachId=<s:property value="attachmentSeq"/>&queryId=<s:property value="querySeq"/>" onmouseover="hilight('imgd<s:property value="#stat.index" />')" onmouseout="lolight('imgd<s:property value="#stat.index" />')">
                                                            <img name="imgd<s:property value="#stat.index" />" src="img/find_off.gif" title="View/Open Document" border="0">
                                                        </a>
                                                        </td>
													</tr>
												</s:iterator>
											</tbody>
										</table>
									</s:if>
                                	<s:else>
										<table class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
											<tbody>
												<TR class=DataGridHeaderTitle>
													<TD  colspan="6">Attach Documents</TD>
												</TR>
												<TR class=DataGridHeader>
													<td class=DataGridHeader style="WIDTH: 10%">Date Deposited</td>
													<td class=DataGridHeader style="WIDTH: 15%">Category</td>
													<td class=DataGridHeader style="WIDTH: 50%">Document Name</td>
													<td class=DataGridHeader style="WIDTH: 15%">Deposited By</td>
													<td class=DataGridHeader style="WIDTH: 30"></td>
												</tr>
												<s:iterator value="query.attachmentsForDisplay" status="stat">
													<tr>
												 		<td class=DataGridItem style="VERTICAL-ALIGN: middle"><joda:format value="${createTs}" pattern="dd-MMM-yyyy" locale="en"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="attachmentCategory.attachmentCategory"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="filenameUpper"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="author"/></td>
														<td class=DataGridItem valign="middle" align="center"><a target="attachments" href="Attachment.action?attachId=<s:property value="attachmentSeq"/>&queryId=<s:property value="querySeq"/>" onmouseover="hilight('imgd<s:property value="#stat.index" />')" onmouseout="lolight('imgd<s:property value="#stat.index" />')"><img name="imgd<s:property value="#stat.index" />" src="img/find_off.gif" title="View/Open Document" border="0"></a></td>
													</tr>
												</s:iterator>
											</tbody>
										</table>
									</s:else>

									<br>
                                        <s:if test="!query.followupEmpty">
											<s:iterator value="query.followupAttachments" status="stat">
											<table class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
												<tbody>
												<TR class=DataGridHeaderTitle>
													<TD colspan="6">Follow-up Attach Documents</TD>
												</TR>
													<TR class=DataGridHeader>
														<td class=DataGridHeader style="WIDTH: 10%">Follow Up</td>
														<td class=DataGridHeader style="WIDTH: 10%">Date Deposited</td>
														<td class=DataGridHeader style="WIDTH: 10%">Category</td>
														<td class=DataGridHeader style="WIDTH: 50%"><s:property value="followupNumber" /> Document Name</td>
														<td class=DataGridHeader style="WIDTH: 10%">Deposited By</td>
														<td class=DataGridHeader style="WIDTH: 30"></td>
													</tr>
													<s:iterator value="attachments">
													<tr>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="query.followupNumber"/></td>
												 		<td class=DataGridItem style="VERTICAL-ALIGN: middle"><joda:format value="${createTs}" pattern="dd-MMM-yyyy" locale="en"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="attachmentCategory.attachmentCategory"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="filenameUpper"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="author"/></td>
														<td class=DataGridItem valign="middle" align="center">
                                                            <a target="attachments" href="Attachment.action?attachId=<s:property value="attachmentSeq"/>&queryId=<s:property value="querySeq"/>" onmouseover="hilight('img<s:property value="#stat.index" />')" onmouseout="lolight('img<s:property value="#stat.index" />')">
                                                                <img name="img<s:property value="#stat.index" />" src="img/find_off.gif" title="View/Open Document" border="0">
                                                            </a>
                                                        </td>
													</tr>
													</s:iterator>
												</tbody>
											</table>
											</s:iterator>
										</s:if>
                                        <s:else>
											<s:iterator value="query.followupAttachments">
											<table class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
												<tbody>
												<TR class=DataGridHeaderTitle>
													<TD  colspan="5">Follow-up Attach Documents</TD>
												</TR>
													<TR class=DataGridHeader>
														<td class=DataGridHeader style="WIDTH: 50%">Follow-up <s:property value="followupNumber" /> Document Name</td>
														<td class=DataGridHeader style="WIDTH: 10%">Date Deposited</td>
														<td class=DataGridHeader style="WIDTH: 15%">Category</td>
														<td class=DataGridHeader style="WIDTH: 15%">Deposited By</td>
														<td class=DataGridHeader style="WIDTH: 30"></td>
													</tr>
													<s:iterator value="attachments" status="stat">
													<tr>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="filenameUpper"/></td>
												 		<td class=DataGridItem style="VERTICAL-ALIGN: middle"><joda:format value="${createTs}" pattern="dd-MMM-yyyy" locale="en"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="attachmentCategory.attachmentCategory"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="author"/></td>
														<td class=DataGridItem valign="middle" align="center"><a target="attachments" href="Attachment.action?attachId=<s:property value="attachmentSeq"/>&queryId=<s:property value="querySeq"/>" onmouseover="hilight('img<s:property value="#stat.index" />')" onmouseout="lolight('img<s:property value="#stat.index" />')"><img name="img<s:property value="#stat.index" />" src="img/find_off.gif" title="View/Open Document" border="0"></a></td>
													</tr>
													</s:iterator>
												</tbody>
											</table>
											</s:iterator>
										</s:else>


                                    <s:if test="dscl">
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
											<tr>
												<td width="70%" valign="middle">
													<b>Click <a href="AttachmentActivationList.action?queryId=<s:property value="query.querySeq"/>" class="Anchor" >here</a> to deactivate/reactivate attachment...</b>
												</td>
												<td width="30%" valign="middle">
                                                    <s:if test="query.openFollowupAttachExist">
													<table cellspacing="0" cellpadding="0" border="0">
														<tr>
															<td><a href="AddNewAttachment.action?queryId=<s:property value="query.querySeq"/>" onmouseover="hilight('img04')" onmouseout="lolight('img04')"><img name="img04" src="img/add_off.gif" title="" border="0"></a></td>
															<td width="100%"><logic:equal name="query" property="displayAttachmentFollowupLink" value="true">&nbsp;Add New Document</logic:equal></td>
														</tr>
													</table>
                                                    </s:if>
												</td>
											</tr>
									</table>
                                    </s:if>


									<br>
									<s:if test="!query.followupEmpty">
										<TABLE class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
											<TBODY>
												<TR class=DataGridHeaderTitle2>
													<TD  colspan="4">Query Notes</TD>
												</TR>

												<TR class=DataGridHeader2>
													<TD class=DataGridHeader2 style="WIDTH: 10%">Follow Up</TD>
													<TD class=DataGridHeader2 style="WIDTH: 10%">Date</TD>
													<TD class=DataGridHeader2 style="WIDTH: 10%">Author</TD>
													<TD class=DataGridHeader2 style="WIDTH: 65%">Note</TD>
												</TR>

												<s:iterator value="query.nonAffiliateNotes">
												<TR>
													<TD class=DataGridItem>&nbsp;<s:property value="query.followupNumber"/></TD>
													<TD class=DataGridItem>&nbsp;<joda:format value="${createTs}" pattern="dd-MMM-yyyy" locale="en"/></TD>
											 		<TD class=DataGridItem>&nbsp;<s:property value="username" /></TD>
													<TD class=DataGridItem><s:property value="text" /></TD>
												</TR>
												</s:iterator>

											</TBODY>
										</TABLE>
									</s:if>
									<s:else>
										<TABLE class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
											<TBODY>
												<TR class=DataGridHeaderTitle2>
													<TD  colspan="3">Query Notes</TD>
												</TR>

												<TR class=DataGridHeader2>
													<TD class=DataGridHeader2 style="WIDTH: 10%">Date</TD>
													<TD class=DataGridHeader2 style="WIDTH: 10%">Author</TD>
													<TD class=DataGridHeader2 style="WIDTH: 75%">Note</TD>
												</TR>

												<s:iterator value="query.nonAffiliateNotes">
												<TR>
													<TD class=DataGridItem>&nbsp;<joda:format value="${createTs}" pattern="dd-MMM-yyyy" locale="en"/></TD>
											 		<TD class=DataGridItem>&nbsp;<s:property value="username" /></TD>
													<TD class=DataGridItem><s:property value="text" /></TD>
												</TR>
												</s:iterator>

											</TBODY>
										</TABLE>
									</s:else>
									<br>


									<s:if test="!query.followupEmpty">
										<TABLE class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
											<TBODY>
												<TR class=DataGridHeaderTitle1>
													<TD colspan="4">Activity History</TD>
												</TR>
												<TR class=DataGridHeader1>
													<TD class=DataGridHeader1 style="WIDTH: 10%">Follow Up</TD>
													<TD class=DataGridHeader1 style="WIDTH: 10%">Date</TD>
													<TD class=DataGridHeader1 style="WIDTH: 60%">Activity</TD>
													<TD class=DataGridHeader1 style="WIDTH: 15%">User</TD>
												</TR>
												<s:iterator value="query.processInstance.taskInstances">
												<TR>
													<TD class=DataGridItem>&nbsp;<s:property value="query.followupNumber"/></TD>
													<TD class=DataGridItem>&nbsp;<s:date name="end" format="dd-MMM-yyyy"/></TD>
											 		<TD class=DataGridItem>&nbsp;<s:property value="name"/></TD>
													<TD class=DataGridItem><s:property value="actorId"/></TD>
												</TR>
												</s:iterator>
											</TBODY>
										</TABLE>
									</s:if>
									<s:else>
										<TABLE class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
											<TBODY>
												<TR class=DataGridHeaderTitle1>
													<TD colspan="3">Activity History</TD>
												</TR>
												<TR class=DataGridHeader1>
													<TD class=DataGridHeader1 style="WIDTH: 10%">Date</TD>
													<TD class=DataGridHeader1 style="WIDTH: 70%">Activity</TD>
													<TD class=DataGridHeader1 style="WIDTH: 15%">User</TD>
												</TR>
												<s:iterator value="query.processInstance.taskInstances">
												<TR>
													<TD class=DataGridItem>&nbsp;<s:date name="end" format="dd-MMM-yyyy"/></TD>
											 		<TD class=DataGridItem>&nbsp;<s:property value="name"/></TD>
													<TD class=DataGridItem><s:property value="actorId"/></TD>
												</TR>
												</s:iterator>
											</TBODY>
										</TABLE>
									</s:else>

									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td height="20" colspan="2"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="50%"><b>Click <u>
											<s:if test="sourceSearch == 'repositorySearch'">
												<a href="PerformRepositorySearch.action" class="Anchor">
											</s:if>
											<s:else>
												<a href="QuickSearchResults.action" class="Anchor">
											</s:else>
											here</a></u> to return to search results...</b></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td height="20" colspan="2"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="50%"><b>Click <u><a href="RepositorySearch.action" class="Anchor">here</a></u> to specify new search criteria...</b></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td height="20" colspan="2"><img src="img/spacer.gif" height="20"></td>
										</tr>
									</table>
								</p>
                                </span></div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
		<tr>
			<td width="100%" height=20></td>
		</tr>
	</tbody>
</table>
</td>
</tr>
</tbody>
</table>

</body>