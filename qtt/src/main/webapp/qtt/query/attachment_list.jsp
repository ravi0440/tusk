<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<html>
<head>
    <script language="javascript">
	function attachmentDeactivation(attachId, queryId, taskId) {
		if (confirm("Deactivating will take out this attachment from attachment list of this query.  Please click 'OK' if you wish to proceed." ))	{
			disableButtonsSubmit(document.forms[0]);
            document.forms[0].action="DeactivateAttachment.action?id="+attachId+"&queryId="+queryId+"&taskId="+taskId;
			document.forms[0].target='_self';
			document.forms[0].submit();
		}
	}
	function attachmentReactivation(attachId, queryId, taskId) {
		if (confirm("Reactivating will put in this attachment in attachment list of this query. Please click 'OK' if you wish to proceed."))	{
            disableButtonsSubmit(document.forms[0]);
			document.forms[0].action="ReactivateAttachment.action?id="+attachId+"&queryId="+queryId+"&taskId="+taskId;
			document.forms[0].target='_self';
			document.forms[0].submit();
		}
	}
</script>
</head>
<body>
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<tbody>
  		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%"><span>
								<p>Information</p></span></div></td>
						</tr>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><span>
								<p>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="2"><font size="+1">Query No. <s:property value="query.queryNumber" /></font></td>
										</tr>
										<tr>
											<td height="20" colspan="2"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="30%"><b>Drug Name</b></td>
											<td width="70%"><s:property value="query.drugName" /></td>
										</tr>
										<tr>
											<td width="30%"><b>Query Type</b></td>
											<td width="70%"><s:property value="query.queryType.queryType"/></td>
										</tr>
										<tr>
											<td width="30%"><b>Initiated</b></td>
                                            <td width="70%">
                                            <s:if test="%{(#parameters.taskId != null) && (#parameters.taskId != '')}">
                                                <s:property value="#parameters.initDate" />
                                            </s:if>
                                            <s:else>
											    <joda:format value="${query.initiateDate}" pattern="dd-MMM-yyyy" locale="en" />
                                            </s:else>
                                            </td>
										</tr>
										<tr>
											<td width="30%"><b>Response Date</b></td>
											<td width="70%"><s:property value="query.currentDueDateFormatted"/></td>
										</tr>
										<tr>
											<td width="30%"><b>Requester</b></td>
											<td width="70%"><s:property value="query.requester.name"/></td>
										</tr>
										<tr>
											<td width="30%"><b>View QRF</b></td>
											<td width="70%">
												<a href="javascript:onclick=openQrfNoAttachments('<s:property value="query.querySeq"/>');" onmouseover="hilight('img00')" onmouseout="lolight('img00')"><img name="img00" src="img/find_off.gif" title="" border="0"></a>
											</td>
										</tr>
										<tr>
											<td height="10" colspan="2"><img src="img/spacer.gif" height="10"></td>
										</tr>
									</table>

                                    <s:if test="!query.followupEmpty">
										<table class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
											<tbody>
												<TR class=DataGridHeader>
													<td class=DataGridHeader style="WIDTH: 10%">Follow Up</td>
													<td class=DataGridHeader style="WIDTH: 12%">Date Deposited</td>
													<td class=DataGridHeader style="WIDTH: 12%">Category</td>
													<td class=DataGridHeader style="WIDTH: 50%">Document Name</td>
													<td class=DataGridHeader style="WIDTH: 10%">Deposited By</td>
                                                    <td class=DataGridHeader style="WIDTH: 10%">Toggle</td>
													<td class=DataGridHeader style="WIDTH: 30"></td>
												</tr>
												<s:iterator value="query.attachmentsForDisplay">
													<tr <s:if test="!activated"> style="color: #c0c0c0;" </s:if> >
														<td class=DataGridItemBold style="VERTICAL-ALIGN: middle"><s:property value="query.followupNumber"/></td>
												 		<td class=DataGridItemBold style="VERTICAL-ALIGN: middle"><joda:format value="${createTs}" pattern="dd-MMM-yyyy" locale="en"/></td>
														<td class=DataGridItemBold style="VERTICAL-ALIGN: middle"><s:property value="attachmentCategory.attachmentCategory"/></td>
														<td class=DataGridItemBold style="VERTICAL-ALIGN: middle"><s:property value="filenameUpper"/></td>
														<td class=DataGridItemBold style="VERTICAL-ALIGN: middle"><s:property value="author"/></td>
                                                        <s:if test="activated">
                                                        <td>
                                                            <input type="button" name="toggle" value="Deactivate" onclick="attachmentDeactivation('<s:property value="attachmentSeq"/>' ,'<s:property value="querySeq"/>', '<s:property value="%{#parameters.taskId}" />');"
                                                             class="buttonSmallRed" onmouseover="hover(this,'buttonSmallRedHover')" onmouseout="hover(this,'buttonSmallRed')">
														</td>
                                                        </s:if>
                                                        <s:else>
                                                        <td>
                                                            <input type="button" name="toggle" value="Reactivate" onclick="attachmentReactivation('<s:property value="attachmentSeq"/>' ,'<s:property value="querySeq"/>', '<s:property value="%{#parameters.taskId}" />');"
                                                             class="buttonSmallGreen" onmouseover="hover(this,'buttonSmallGreenHover')" onmouseout="hover(this,'buttonSmallGreen')">
														</td>
                                                        </s:else>
                                                        <td class=DataGridItem valign="middle" align="center"><a target="attachments" href="Attachment.action?attachId=<s:property value="attachmentSeq"/>&queryId=<s:property value="querySeq"/>" onmouseover="hilight('img2')" onmouseout="lolight('img2')"><img name="img2" src="img/find_off.gif" title="View/Open Document" border="0"></a></td>
													</tr>
												</s:iterator>
											</tbody>
										</table>
									</s:if>
                                	<s:else>
										<table class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
											<tbody>
												<TR class=DataGridHeader>
													<td class=DataGridHeader style="WIDTH: 12%">Date Deposited</td>
													<td class=DataGridHeader style="WIDTH: 12%">Category</td>
													<td class=DataGridHeader style="WIDTH: 50%">Document Name</td>
													<td class=DataGridHeader style="WIDTH: 10%">Deposited By</td>
                                                    <td class=DataGridHeader style="WIDTH: 10%">Toggle</td>
													<td class=DataGridHeader style="WIDTH: 30"></td>
												</tr>
												<s:iterator value="query.attachmentsForDisplay">
													<tr <s:if test="!activated"> style="color: #c0c0c0;" </s:if>>
												 		<td class=DataGridItemBold style="VERTICAL-ALIGN: middle"><joda:format value="${createTs}" pattern="dd-MMM-yyyy" locale="en"/></td>
														<td class=DataGridItemBold style="VERTICAL-ALIGN: middle"><s:property value="attachmentCategory.attachmentCategory"/></td>
														<td class=DataGridItemBold style="VERTICAL-ALIGN: middle"><s:property value="filenameUpper"/></td>
														<td class=DataGridItemBold style="VERTICAL-ALIGN: middle"><s:property value="author"/></td>
                                                        <s:if test="activated">
                                                        <td>
                                                           <input type="button" name="toggle" value="Deactivate" onclick="attachmentDeactivation('<s:property value="attachmentSeq"/>' ,'<s:property value="querySeq"/>', '<s:property value="%{#parameters.taskId}" />');"
														   class="buttonSmallRed" onmouseover="hover(this,'buttonSmallRedHover')" onmouseout="hover(this,'buttonSmallRed')">
														</td>
                                                        </s:if>
                                                        <s:else>
                                                        <td>
                                                            <input type="button" name="toggle" value="Reactivate" onclick="attachmentReactivation('<s:property value="attachmentSeq"/>' ,'<s:property value="querySeq"/>', '<s:property value="%{#parameters.taskId}" />');"
														    class="buttonSmallGreen" onmouseover="hover(this,'buttonSmallGreenHover')" onmouseout="hover(this,'buttonSmallGreen')">
														</td>
                                                        </s:else>
                                                        <td class=DataGridItem valign="middle" align="center"><a target="attachments" href="Attachment.action?attachId=<s:property value="attachmentSeq"/>&queryId=<s:property value="querySeq"/>" onmouseover="hilight('img2')" onmouseout="lolight('img2')"><img name="img2" src="img/find_off.gif" title="View/Open Document" border="0"></a></td>
													</tr>
												</s:iterator>
											</tbody>
										</table>
									</s:else>

									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td height="30"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td width="100%">
                                            <s:if test="query.queryStatusCode.statusClosed">
												<b>Click <a href="ClosedQueryDetails.action?queryId=<s:property value="query.querySeq"/>" class="Anchor">here</a> to return to Query Details...</b>
											</s:if>
											<s:else>
												<b>Click <a href="RetrieveQueryDetails.action?queryId=<s:property value="query.querySeq"/>&taskId=<s:property value="%{#parameters.taskId}" />" class="Anchor">here</a> to return to Query Details...</b>
											</s:else>
											</td>
										</tr>
									</table>

								</p>
                                </span></div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=20></td>
		</tr>
  	</tbody>
</table>

</td>
</tr>
</tbody>
</table>
<s:form theme="simple">
</s:form>
</body>
</html>