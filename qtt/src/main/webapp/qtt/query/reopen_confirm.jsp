<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<body>

<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<tbody>
  		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%"><span>
								<p>Query Information</p></span></div></td>
						</tr>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><span>
								<p>

									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Query No. <s:property value="query.queryNumber" /></font></td>
										</tr>
										<tr>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="30%">
												<b>Drug Name</b>
											</td>
											<td width="70%" colspan="3">
												<s:property value="query.drugName" />
											</td>
										</tr>
							
										<tr>
											<td>
												<b>Query Type</b>
											</td>
											<td colspan="3">
												<s:property value="query.queryType.queryType"/>
											</td>
										</tr>
										<tr>
											<td>
												<b>Requester</b>
											</td>
											<td colspan="3">
												<s:property value="query.requester.name"/>
											</td>
										</tr>
										<tr>
											<td height="20" colspan="4"><img src="img/spacer.gif" height="20"></td>
										</tr>
							
										<tr>
											<td colspan="4"><img src="img/spacer.gif" height="10"></td>
										</tr>

                                    <!-- DSCL-6757 revoked
                                        <style type="text/css">
                                            #comment {
                                                border: 2px solid #ff0000;
                                            }
                                        </style>
                                        <td id="comment" align="center">
                                            <br/>
                                            <font size="+1">You are about to make changes in Query data.</font>
                                            <br/>
                                            <br/>
                                            Are you sure you want to submit the changes?<br/>
                                            This action will permanently override present data which will not be recoverable anymore (even by administrator)<br/>
                                            <br/>
                                        </td>
                                    -->

										<tr>
											<td colspan="4"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td align="left" colspan="3"><b>Click <a href="ProcessQRFAction.action?activity=reo&id=<s:property value="query.querySeq"/>" class="Anchor" >here</a> to confirm the Re-Opening of the query for follow-up...</b></td>
											
											<td height="20"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td align="left" colspan="4">Re-Opening of query will start the process from the beginning with the copy of original QRF. 
												All worked performed related to this query will appear as original query.</td>
										</tr>

										<tr>
											<td colspan="4"><IMG src="img/spacer.gif" border="0" width="1" height="10"></td>
										</tr>
										<tr>
											<td colspan="4"><IMG src="img/spacer.gif" border="0" width="1" height="10"></td>
										</tr>
										<tr>
											<td colspan="4"><IMG src="img/spacer.gif" border="0" width="1" height="10"></td>
										</tr>

										<tr>
											<td width="50%"><b>Click <u><a href="PerformRepositorySearch.action" class="Anchor">here</a></u> to return to search results...</b></td>
											<td height="20"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td colspan="4"><IMG src="img/spacer.gif" border="0" width="1" height="10"></td>
										</tr>
										
									</table>

								    </p>
                                </span></div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=20></td>
		</tr>
  	</tbody>
</table>

</td>
</tr>
</tbody>
</table>

</body>