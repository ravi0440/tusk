<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<body>

<s:form action="QueryList" name="querySearchForm" id="querySrch" theme="simple">

    <table cellSpacing=0 cellPadding=0 width="100%" border=0>
        <tbody>
        <tr>
            <td class=BodyText vAlign=top colSpan=3>Fields marked with a # are mandatory</td>
        </tr>
        <tr>
            <td vAlign=top colSpan=3><IMG height=16 src="img/spacer.gif" width=1></td>
        </tr>
        <s:if test="hasActionErrors() || hasFieldErrors()">
            <%@ include file="../global/validation_error.jsp" %>
        </s:if>
        <tr>
            <td width="95%">

                <table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <tbody>
                    <tr>
                        <td class=TitleExpanded
                            style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid"
                            vAlign=center align=left width="100%">
                            <div style="WIDTH: 100%; HEIGHT: 100%"><span><p>Email Address</p></span></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid"
                            colSpan=2 height="100%">
                            <div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%"
                                 align=justify><span>
									<p>
										<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td height="20" colspan="2"><img src="img/spacer.gif" height="20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%">
                                                    #Email Address:
                                                </td>
                                                <td align="left" width="80%">
                                                    <s:textfield name="email" size="50"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" colspan="2"><img src="img/spacer.gif" height="20">
                                                </td>
                                            </tr>
                                        </table>

                                </p>
                                    </span></div>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </td>
            <td width="20"><img src="img/spacer.gif" width="20"></td>
        </tr>
        <tr>
            <td width="95%" height=20></td>
            <td width="20"><img src="img/spacer.gif" width="20"></td>
        </tr>
        </tbody>
    </table>

    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
            <td align="left" width="20%">
                <s:submit value="%{getText('button.open')}"></s:submit>
            </td>
            <td height="20" width="80%"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
        </tr>
        <tr>
            <td height="20" width="100%" colspan="2"><img src="img/spacer.gif" height="20" width="20" border="0">
            </td>
        </tr>
    </table>
</s:form>

</body>