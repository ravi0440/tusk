<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<body onLoad="preload()">
<script language="javascript" src="js/window.js"></script>

<s:form action="FilterQueryTaskList" method="get" theme="simple" id="searchForm">

<table cellSpacing=0 cellPadding=0 width="100%" border=0>
	<tbody>
		<tr>
			<td width="100%">
				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<s:if test="hasActionErrors() || hasFieldErrors()">
			              	<%@ include file="../global/validation_error.jsp" %>
						</s:if>

	                    <tr>
	                        <td class="TitleExpanded"
	                            style="BORDER-RIGHT: #28578b 1px solid; BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid"
	                            valign="center" align="left" width="100%">
	                            <div style="WIDTH: 100%; HEIGHT: 100%">
	                                <p><bean:write name="data" property="subject"/></p>
	                            </div>
	                        </td>
	                    </tr>
						<tr>
		                    <td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
		                        <div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><span>
									<p>
											<table cellspacing="0" cellpadding="1" border="0" width="100%">
				                                <tr>
				                                    <td>Owner:</td>
				                                    <td>
				                                        <s:textfield name="params.owner"/>
				                                    </td>
				                                    <td>&nbsp;</td>
				                                </tr>
				                                <tr>
				                                    <td rowspan="2" valign="top">Response date:</td>
				                                    <td colspan="4">
				                                        <table cellspacing="4" cellpadding="0" border="0">
				                                            <tr>
				                                                <td width="8%"><b>From</b></td>
				                                                <td><img src="img/spacer.gif" width="20"></td>
				                                                <td><s:textfield name="params.from" size="13" maxlength="11"/></td>
				                                                <td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
				                                            </tr>
				                                        </table>
				                                    </td>
				                                </tr>
				                                <tr>
				                                    <td width="70%" colspan="4">
				                                        <table cellspacing="4" cellpadding="0" border="0">
				                                            <tr>
				                                                <td width="8%"><b>To</b></td>
				                                                <td><img src="img/spacer.gif" width="20"></td>
				                                                <td><s:textfield name="params.to" size="13" maxlength="11"/></td>
				                                                <td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
				                                            </tr>
				                                        </table>
				                                    </td>
				                                </tr>
				                                <tr>
				                                	<td colspan="4">
				                                		<input type="submit" value="Apply Filter"/>
				                                		<input type="submit" value="Clear Filter" onclick="clearForm(this.form);"/>
				                                	</td>
				                                </tr>
				                            </table>
			                        </p>
									</span></div>
		                    </td>
						</tr>
						<tr>
							<td>
								<table class=DataGrid style="WIDTH: 100%; BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=1 border=1>
									<tbody>
										<tr class=DataGridHeader>
											<s:url action="RetrieveQueryTaskList" escapeAmp="false" var="actionUrl">
												<s:param name="page" value="%{'task_list'}"/>
											</s:url>
											<td class=DataGridHeader style="WIDTH: 30"></td>
											<td class=DataGridHeader style="WIDTH: 30"></td>
											<td class=DataGridHeadersmall style="WIDTH: 80" valign="top"><a class=BlindHeaderLink href="<s:property value="actionUrl"/>&sort=0&asc=<s:if test="sort == 0 && asc">false</s:if><s:else>true</s:else>&filterKey=<s:property value="filterKey"/>">Query #</a></td>
											<td class=DataGridHeadersmall style="WIDTH: 8%" valign="top"><a class=BlindHeaderLink href="<s:property value="actionUrl"/>&sort=1&asc=<s:if test="sort == 1 && asc">false</s:if><s:else>true</s:else>&filterKey=<s:property value="filterKey"/>">Drug Name</a>
												<div class=DataGridHeadersmaller>
													<s:if test="!showLabelLink">
														<br><a class=BlindHeaderLink href="<s:property value="actionUrl"/>&filterKey=<%=com.roche.dss.qtt.query.web.action.TaskBaseAction.FILTER_QUERY_LABEL_ON%>&sort=<s:property value="sort"/>&asc=<s:property value="asc"/>">Show Label</a>
													</s:if>
													<s:if test="showLabelLink">
														<br><a class=BlindHeaderLink href="<s:property value="actionUrl"/>&filterKey=<%=com.roche.dss.qtt.query.web.action.TaskBaseAction.FILTER_QUERY_LABEL_OFF%>&sort=<s:property value="sort"/>&asc=<s:property value="asc"/>">Hide Label</a>
													</s:if>
												</div>
											</td>
											<td class=DataGridHeadersmall style="WIDTH: 50" valign="top"><a class=BlindHeaderLink href="<s:property value="actionUrl"/>&sort=2&asc=<s:if test="sort == 2 && asc">false</s:if><s:else>true</s:else>&filterKey=<s:property value="filterKey"/>">Query Type</a></td>
											<td class=DataGridHeadersmall style="WIDTH: 50" valign="top"><a class=BlindHeaderLink href="<s:property value="actionUrl"/>&sort=3&asc=<s:if test="sort == 3 && asc">false</s:if><s:else>true</s:else>&filterKey=<s:property value="filterKey"/>">Req. Ctry</a></td>
											<td class=DataGridHeadersmall style="WIDTH: 8%" valign="top"><a class=BlindHeaderLink href="<s:property value="actionUrl"/>&sort=4&asc=<s:if test="sort == 4 && asc">false</s:if><s:else>true</s:else>&filterKey=<s:property value="filterKey"/>">Date Initiated</a></td>
											<td class=DataGridHeadersmall style="WIDTH: 8%" valign="top"><a class=BlindHeaderLink href="<s:property value="actionUrl"/>&sort=5&asc=<s:if test="sort == 5 && asc">false</s:if><s:else>true</s:else>&filterKey=<s:property value="filterKey"/>">Response Date</a></td>
											<td class=DataGridHeadersmall style="WIDTH: 8%" valign="top"><a class=BlindHeaderLink href="<s:property value="actionUrl"/>&sort=6&asc=<s:if test="sort == 6 && asc">false</s:if><s:else>true</s:else>&filterKey=<s:property value="filterKey"/>">Response Date (DMG)</a></td>
											<td class=DataGridHeadersmall style="WIDTH: 70" valign="top"><a class=BlindHeaderLink href="<s:property value="actionUrl"/>&sort=7&asc=<s:if test="sort == 7 && asc">false</s:if><s:else>true</s:else>&filterKey=<s:property value="filterKey"/>">Activity</a></td>
											<td class=DataGridHeadersmall style="WIDTH: 70" valign="top"><a class=BlindHeaderLink href="<s:property value="actionUrl"/>&sort=8&asc=<s:if test="sort == 8 && asc">false</s:if><s:else>true</s:else>&filterKey=<s:property value="filterKey"/>">Owner</a></td>
											<td class=DataGridHeadersmall style="WIDTH: 70" valign="top"><a class=BlindHeaderLink href="<s:property value="actionUrl"/>&sort=8&asc=<s:if test="sort == 8 && asc">false</s:if><s:else>true</s:else>&filterKey=<s:property value="filterKey"/>">Verifier</a></td>
											<td class=DataGridHeadersmall style="WIDTH: 70" valign="top"><a class=BlindHeaderLink href="<s:property value="actionUrl"/>&sort=9&asc=<s:if test="sort == 9 && asc">false</s:if><s:else>true</s:else>&filterKey=<s:property value="filterKey"/>">Workflow Type</a></td>
											<td class=DataGridHeader style="WIDTH: 30"></td>
											<td class=DataGridHeader style="WIDTH: 30"></td>
											<td class=DataGridHeader style="WIDTH: 30"></td>
											<td class=DataGridHeader style="WIDTH: 30"></td>
											<td class=DataGridHeader style="WIDTH: 30"></td>
											<td class=DataGridHeader style="WIDTH: 30"></td>
											<td class=DataGridHeader style="WIDTH: 30"></td>
										</tr>
										<s:iterator value="tasks" status="stat">
										<tr>
											<td style="Smalltext" borderColor=#6699cc valign="middle" align="center">
												<s:if test="query.urgencyFlag=='Y'">
													<img name="img01" src="img/urgent_2_off.gif" alt="This query is flagged as URGENT" border="0" title="This query is flagged as URGENT" border="0">
												</s:if>
											</td>
											<td style="Smalltext" borderColor=#6699cc valign="middle" align="center">
												<s:if test="query.alertFlag=='Y'">
													<img name="img02" src="img/important_off.gif" alt="New information added to this query" border="0" title="New information added to this query" border="0">
												</s:if>
											</td>
											<td class="<s:property value="cssStyle"/>" borderColor=#6699cc valign="middle">
												<s:property value="query.queryNumber"/><s:if test="%{query.followupNumber != null && query.followupNumber != ''}"><br>|__<s:property value="query.followupNumber"/></s:if>
											</td>
									 		<td class="<s:property value="cssStyle"/>" borderColor=#6699cc>
									 			<s:if test="query.drugName != null">
										 			<acronym title='<s:property value="query.drugName"/>'>
														<s:property value="truncDrugName"/>
										 			</acronym>
									 			</s:if>
                                                <s:if test="displayQueryLabel">
											 		<acronym title='<s:property value="query.queryLabel"/>'>
										 				<s:div cssClass="Smallertext">
															<script language="javascript">
																document.write(showLabel('<s:property value="query.queryLabel"/>', '<s:property value="query.querySeq"/>'));
															</script>
											 			</s:div>
											 		</acronym>
										 		</s:if>
									 		</td>									 		
									 		<td class="<s:property value="cssStyle"/>" borderColor=#6699cc>
									 			<acronym title="<s:property value="query.queryType.queryType"/>">
													<s:property value="truncType"/>
												</acronym>
											</td>
											<td class="<s:property value="cssStyle"/>" borderColor=#6699cc>
												<s:if test="!affiliate">
													<s:property value="query.country.description"/>
											    </s:if>
											</td>
											<td class="<s:property value="cssStyle"/>" borderColor=#6699cc>
												<s:property value="query.initiateDateFormatted"/>
											</td>
											<td class="<s:property value="cssStyle"/>" borderColor=#6699cc>
												<s:property value="query.currentDueDateFormatted"/>
											</td>
											<td class="<s:property value="cssStyle"/>" borderColor=#6699cc>
												<s:property value="query.dmgDueDateFormatted"/>
											</td>
											<td class="<s:property value="cssStyle"/>" borderColor=#6699cc>
												<s:property value="task.name"/>
											</td>
											<td class="<s:property value="cssStyle"/>" borderColor=#6699cc>
												<s:property value="task.actorId"/>
											</td>
											<td class="<s:property value="cssStyle"/>" borderColor=#6699cc>
												<s:property value="query.verifyPerson"/>
												
												<s:if test="verified(query, task)">
												 Verified
												</s:if>
												<s:else>
												<s:if test="readyForVerification(query, task)">
												ready for verification
												</s:if>
												</s:else>
											</td>
											<td class="<s:property value="cssStyle"/>" borderColor=#6699cc>
												<s:property value="query.workflowRouteType"/>
											</td>
											<td class="Smalltext" borderColor=#6699cc valign="middle" align="center">
												<a href="RetrieveQueryNotes.action?queryId=<s:property value="query.querySeq"/>&taskId=<s:property value="task.id"/>" onmouseover="hilight('imgv<s:property value="#stat.count"/>')" onmouseout="lolight('imgv<s:property value="#stat.count"/>')">
													<img name="imgv<s:property value="#stat.count"/>" src="img/notes_off.gif" alt="View Query Notes" title="View Query Notes" border="0">
												</a>
											</td>
											<td class="Smalltext" borderColor=#6699cc valign="middle" align="center">
												<s:if test="dscl && !finalDocExists && !followup">
													<a href="PreviewAction.action?queryId=<s:property value="query.querySeq" />&viewtype=assignDelete" onmouseover="hilight('imgdel<s:property value="#stat.count"/>')" onmouseout="lolight('imgdel<s:property value="#stat.count"/>')">
														<img name="imgdel<s:property value="#stat.count"/>" src="img/delete_off.gif" alt="Delete Query" title="Delete Query" border="0">
													</a>
												</s:if>
											</td>
											<td class="Smalltext" borderColor=#6699cc valign="middle" align="center">
												<s:if test="DMGAllowed && displayDMG">
													<a href="DmgDataSearch.action?taskName=<s:property value="task.name"/>&queryId=<s:property value="query.querySeq"/>&taskId=<s:property value="task.id"/>" onmouseover="hilight('imgdmg<s:property value="#stat.count"/>')" onmouseout="lolight('imgdmg<s:property value="#stat.count"/>')">
														<img name="imgdmg<s:property value="#stat.count"/>" src="img/dmg_off.gif" alt="DMG Data Search" title="DMG Data Search" border="0">
													</a>
												</s:if>
											</td>
											<td class="Smalltext" borderColor=#6699cc valign="middle" align="center">
												<s:if test="MIAllowed && displayMI">
													<s:url action="MedicalInterpretation" var="partialMiUrl">
														<s:param name="taskName"><s:property value="task.name"/></s:param>
													</s:url>
													<a href="<s:property value="#partialMiUrl"/>&queryId=<s:property value="query.querySeq"/>&taskId=<s:property value="task.id"/>" onmouseover="hilight('imgmi<s:property value="#stat.count"/>')" onmouseout="lolight('imgmi<s:property value="#stat.count"/>')">
														<img name="imgmi<s:property value="#stat.count"/>" src="img/mi_off.gif" alt="Medical Interpretation" title="Medical Interpretation" border="0">
													</a>
												</s:if>
											</td>
											<td class="Smalltext" borderColor=#6699cc valign="middle" align="center">
												<s:if test="query.closeErrorStatus == null || query.closeErrorStatus == 0">
													<s:if test="dscl && displayAwaken">
														<a href="ProcessFollowUp.action?queryId=<s:property value="query.querySeq"/>&taskId=<s:property value="task.id"/>" onmouseover="hilight('imgaw<s:property value="#stat.count"/>')" onmouseout="lolight('imgaw<s:property value="#stat.count"/>')">
															<img name="imgaw<s:property value="#stat.count"/>" src="img/sleep_off.gif" alt="Reopen Query for Follow-Up" title="Reopen Query for Follow-Up" border="0">
														</a>
													</s:if>
												</s:if>
											</td>
											<td class="Smalltext" borderColor=#6699cc valign="middle" align="center">
												<s:if test="query.closeErrorStatus == null || query.closeErrorStatus == 0">
													<s:if test="displayContactRequester">
														<a  href="mailto:<s:property value="query.requester.email"/>?subject=<s:property value="query.queryNumber"/>&cc=<s:property value="dsclEmail"/>" onmouseover="hilight('imge<s:property value="#stat.count"/>')" onmouseout="lolight('imge<s:property value="#stat.count"/>')">
															<img name="imge<s:property value="#stat.count"/>" src="img/mail_off.gif" alt="Send Question to Requester" title="Send Question to Requester" border="0">
														</a>
													</s:if>
												</s:if>
											</td>
											<td class="Smalltext" borderColor=#6699cc valign="middle" align="center">
												<s:if test="query.closeErrorStatus == null || query.closeErrorStatus == 0">
													<a href="RetrieveQueryDetails.action?queryId=<s:property value="query.querySeq"/>&taskId=<s:property value="task.id"/>" onmouseover="hilight('imgd<s:property value="#stat.count"/>')" onmouseout="lolight('imgd<s:property value="#stat.count"/>')">
														<img name="imgd<s:property value="#stat.count"/>" src="img/find_off.gif" alt="View Query Details" title="View Query Details" border="0">
													</a>
												</s:if>
											</td>
										</tr>
										</s:iterator>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=20></td>
		</tr>
  	</tbody>
</table>

<s:if test="showSummaryLink">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<td colspan="16">
				<b>Click <u><a href="RetrieveSummaryList.action" class="Anchor">here</a></u> to view query summary list...</b>
			</td>
		</tr>
	</table>
</s:if>

</s:form>
</body>
		                            

