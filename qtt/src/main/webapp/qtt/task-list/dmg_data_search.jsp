<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" import="com.roche.dss.qtt.service.workflow.WorkflowGlobalConstants" %>

<script language="javascript">
	function confirmationDialog() {
		var path="ConfirmationDialog.action?option=dTm" + "&taskId=<s:property value="taskId"/>" + "&queryId=<s:property value="query.querySeq"/>";
		var args;
		var sFeatures = getModalValues(230, 830);
	    var sReturn = window.showModalDialog(path, args, sFeatures);
		if(	sReturn != null) {	
			window.parent.location=sReturn;     	
		}
	}	
</script>

<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<tbody>
  		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=middle align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%">
									<p>Query Information</p>
								</div>
							</td>
						</tr>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Query No. <s:property value="query.queryNumber"/></font></td>
										</tr>
										<tr>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="30%">
												<b>Drug Name</b>
											</td>
											<td width="70%" colspan="3">
												<s:property value="query.drugName"/>
											</td>
										</tr>
										<tr>
											<td>
												<b>Query Type</b>
											</td>
											<td colspan="3">
												<s:property value="query.queryType.queryType"/>
											</td>
										</tr>
										<tr>
											<td>
												<b>Requester</b>
											</td>
											<td colspan="3">
												<s:property value="query.requester.name"/>
											</td>
										</tr>
										<tr>
											<td>
												<b>Current Activity</b>
											</td>
											<td colspan="3">
												<s:property value="taskName"/>
											</td>
										</tr>
										<tr>
											<td height="10" colspan="4"><img src="img/spacer.gif" height="10"></td>												
										</tr>										
										<tr>
											<td colspan="4">
												<s:if test="followupNumber != null">
													<table class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
														<tbody>
															<tr class=DataGridHeaderTitle1>
																<td colspan="4" style="WIDTH: 100%">Activity History</td>
															</tr>
															<tr class=DataGridHeader1>
																<td class=DataGridHeader1 style="WIDTH: 10%">Follow Up</td>														
																<td class=DataGridHeader1 style="WIDTH: 15%">Date</td>
																<td class=DataGridHeader1 style="WIDTH: 60%">Activity</td>
																<td class=DataGridHeader1 style="WIDTH: 15%">User</td>
															</tr>
															<s:iterator value="tasks">
																<tr>
																	<td class=DataGridItem>&nbsp;<s:property value="query.followupNumber"/></td>
																		<td class=DataGridItem>&nbsp;<s:date name="end" format="dd-MMM-yyyy"/></td>
															 			<td class=DataGridItem>&nbsp;<s:property value="name"/></td>
																		<td class=DataGridItem><s:property value="actorId"/></td>
																</tr>
															</s:iterator>
														</tbody>
													</table>
												</s:if>										
												<s:if test="followupNumber == null">
													<table class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
														<tbody>
															<tr class=DataGridHeaderTitle1>
																<td colspan="3" style="WIDTH: 100%">Activity History</td>
															</tr>
															<tr class=DataGridHeader1>
																<td class=DataGridHeader1 style="WIDTH: 15%">Date</td>
																<td class=DataGridHeader1 style="WIDTH: 70%">Activity</td>
																<td class=DataGridHeader1 style="WIDTH: 15%">User</td>
															</tr>
															<s:iterator value="tasks">
																<tr>
																	<td class=DataGridItem>&nbsp;<s:date name="end" format="dd-MMM-yyyy"/></td>
														 			<td class=DataGridItem>&nbsp;<s:property value="name"/></td>
																	<td class=DataGridItem><s:property value="actorId"/></td>
																</tr>
															</s:iterator>
														</tbody>
													</table>
												</s:if>
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=20></td>
		</tr>
  	</tbody>
</table>

<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td colspan="2"><img src="img/spacer.gif" border="0" width="1" height="10"></td>
	</tr> <s:if test="Owner">
	<s:if test="DMGDataSearch">
	
  		<tr>
			<td colspan="2" background="img/line.gif"></td>
		</tr>
		<tr>
			<td colspan="2"><img src="img/spacer.gif" border="0" width="1" height="10"></td>
		</tr>
		<tr>
			<td width="100%"><b>Click here to seek <a href="TaskComplete.action?taskId=<s:property value="taskId"/>&nextActivity=<%=WorkflowGlobalConstants.DMG_CLARIFICATION_ACTIVITY%>" class="Anchor">clarification</a>.</b></td>
		</tr>
		<tr>
		
		 <s:if test="waitingForVerification(query)">
			<td width="100%"><b>Query needs to be verified before compose query summary</b></td>
         </s:if>
         <s:else>
         <td width="100%"><b>Click here to compose a <a href="EnterQuerySummary.action?taskName=<s:property value="taskName"/>&taskId=<s:property value="taskId"/>&queryId=<s:property value="query.querySeq"/>" class="Anchor">query summary</a>.</b></td>
         </s:else>		
		</tr>
		<tr>
			<td colspan="2"><img src="img/spacer.gif" border="0" width="1" height="10"></td>
		</tr>
	</s:if>
	
	<s:if test="MIDMGDataSearch">
	  	<tr>
			<TD colspan="2" background="img/line.gif"></TD>
		</tr>
		<tr>
			<TD colspan="2"><img src="img/spacer.gif" border="0" width="1" height="10"></TD>
		</tr>
		<tr>
			<td width="100%"><b>Click here to seek <a href="TaskComplete.action?taskId=<s:property value="taskId"/>&nextActivity=<%=WorkflowGlobalConstants.DMG_CLARIFICATION_ACTIVITY%>" class="Anchor">clarification</a>.</b></td>
		</tr>
		<tr>
			<td width="100%"><b>Click here to complete <a href="javascript:confirmationDialog();" class="Anchor" >DMG Data Search</a>.</b></td>
		</tr>
		<tr>
			<TD colspan="2"><img src="images/spacer.gif" border="0" width="1" height="10"></TD>
		</tr>
	</s:if>
	
	<s:if test="DMGClarification">
	  	<tr>
			<td colspan="2" background="img/line.gif"></td>
		</tr>
		<tr>
			<td colspan="2"><img src="img/spacer.gif" border="0" width="1" height="10"></td>
		</tr>
		<tr>
			<td width="100%"><b>Click here to continue the <a href="TaskComplete.action?taskId=<s:property value="taskId"/>" class="Anchor">data search</a>.</b></td>
		</tr>
		<tr>
			<td colspan="2"><img src="img/spacer.gif" border="0" width="1" height="10"></td>
		</tr>
  	</s:if>
	<s:if test="DMGReview">
		<tr>
			<td colspan="2" background="img/line.gif"></td>
		</tr>
		<tr>
			<td colspan="2"><img src="img/spacer.gif" border="0" width="1" height="10"></td>
		</tr>
		<tr>
			<td width="100%"><b>Click here to continue the <a href="TaskComplete.action?taskId=<s:property value="taskId"/>" class="Anchor">data search</a>.</b></td>
		</tr>
		<tr>
			<td colspan="2"><img src="img/spacer.gif" border="0" width="1" height="10"></td>
		</tr>
	</s:if>
	</s:if>
  	<tr>
		<td colspan="2" background="img/line.gif"></td>
	</tr>
	<tr>
		<td colspan="2"><img src="img/spacer.gif" border="0" width="1" height="10"></td>
	</tr>
	<tr>
		<td colspan="2"><IMG src="img/spacer.gif" border="0" width="1" height="30"></td>
	</tr>
</table>
