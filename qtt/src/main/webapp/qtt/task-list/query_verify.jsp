<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<body onLoad="preload()">
    <script language="javascript" src="js/global_script.js"></script>
	<script src="js/jquery-2.1.4.min.js"></script>
	<script>
		function buildInputChanged() {
			var buildType = $('input[name="buildType"]:checked').val();
			if (buildType === "HIGH") {
				jQuery("#correctParametersPassed").show();

				jQuery("#codeReviewPassed").hide();
				jQuery("#checkPassed").hide();
				jQuery("#resultsVerified").hide();


				jQuery("[name='correctParametersPassed']").attr("disabled", false);

				jQuery("[name='codeReviewPassed']").attr("disabled", true);
				jQuery("[name='checkPassed']").attr("disabled", true);
				jQuery("[name='resultsVerified']").attr("disabled", true);
				
			} else {
				jQuery("#correctParametersPassed").hide();

				jQuery("#codeReviewPassed").show();
				jQuery("#checkPassed").show();
				jQuery("#resultsVerified").show();


				jQuery("[name='correctParametersPassed'").attr("disabled", true);

				jQuery("[name='codeReviewPassed']").attr("disabled", false);
				jQuery("[name='checkPassed']").attr("disabled", false);
				jQuery("[name='resultsVerified']").attr("disabled", false);
			}

		}

		$( document ).ready(function() {
			buildInputChanged();
		});
				
	</script>


	<s:form action="SubmitVerificationForm" theme="simple">
		<input type="hidden" name="queryId"
			value="<s:property value="queryId" />" />
		<input type="hidden" name="taskId"
			value="<s:property value="taskId" />" />
		<table cellSpacing=0 cellPadding=0 width="100%" border=0>
			<tbody>
				<tr>
					<td width="100%">
						<table height="100%" cellSpacing=0 cellPadding=0 width="100%"
							border=0>
							<tbody>
								<tr>
									<td class=TitleExpanded
										style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid"
										vAlign=middle align=left width="100%">
										<div style="WIDTH: 100%; HEIGHT: 100%">Query Information</div>
									</td>
								</tr>
								<s:if test="hasActionErrors() || hasFieldErrors()">
									<%@ include file="../global/validation_error.jsp"%>
								</s:if>
								<tr>
									<td
										style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid"
										colSpan=2 height="100%">
										<div
											style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%"
											align=justify>
											<table cellspacing="0" cellpadding="1" border="0"
												width="100%">
												<tr>
													<td valign="top" colspan="3"><font size="+1">Query
															No. <s:property value="query.queryNumber" /> <s:property
																value="query.followupNumber" />
													</font></td>
												</tr>
												<tr>
													<td height="20" colspan="3"><img src="img/spacer.gif"
														height="20"></td>
												</tr>
												<tr>
													<td width="15%"><b>Drug Name</b></td>
													<td width="45%"><s:property value="query.drugName" /></td>
													<td width="40%">&nbsp;</td>
												</tr>
												<tr>
													<td><b>Query Type</b></td>
													<td><s:property value="query.queryType.queryType" /></td>
													<td>&nbsp;</td>
												</tr>

												<tr>
													<td><b>Requester</b></td>
													<td><s:property value="query.requester.name" /></td>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td><b>Current Activity</b></td>
													<td><s:property value="taskInstance.name" /></td>
													<td>&nbsp;</td>
												</tr>

												<tr>
													<td height="20" colspan="3"><img src="img/spacer.gif"
														height="20"></td>
												</tr>
											</table>

											<style>
.secondCell {
	width: 70%;
}

.areaStyle {
	margin: 0px;
	width: 561px;
	height: 94px;
}
</style>
											<table height="100%" cellspacing="0" cellpadding="0"
												width="100%" border="0">
												<tbody>
													<tr>
														<td class=" titleCell" valign="center" align="left"
															width="100%" colspan="2">
															<div style="WIDTH: 95%; HEIGHT: 100%">
																<span>
																	<p><b>Requirements analysis:</b></p>
																	<hr>
																</span>
															</div>
														</td>
													</tr>
													<tr>
														<td class="bodyCell" height="100%"><span>
																Requirements are understood </span></td>
														<td class="secondCell"><s:checkbox
																name="requirementsUnderstood" theme="simple" /></td>
													</tr>
													<tr>

														<td class="bodyCell" height="100%"><span>Clarification
																was requested, received and documented </span></td>
														<td class="secondCell"><s:checkbox
																name="clarificationRequested" theme="simple" /></td>
													</tr>
													<tr>

														<td class="bodyCell" height="100%"><span>
																Assumptions had to be made and have been documented </span></td>
														<td class="secondCell"><s:checkbox
																name="assumptionsDocumented" theme="simple" /></td>
													</tr>
													<tr>

														<td class="bodyCell" height="100%"><span>
																Provide further reference(s) if applicable </span></td>
														<td class="secondCell"><s:textarea 
																cssClass="areaStyle" name="furtherRequirements" onblur="textCounter(this,500);"
															onkeypress="textCounter(this,500);"
																theme="simple" /></td>
													</tr>
													<tr>
														<td class=" titleCell" colspan="2" valign="center"
															align="left" width="100%">
															<div style="WIDTH: 95%; HEIGHT: 100%">
																<span> <br>
																	<p><b>Build:</b></p>
																	<hr>
																</span>
															</div>
														</td>
													</tr>
													<tr>
														<td class="bodyCell" colspan="2" height="100%"><s:radio
																name="buildType" listKey="key" listValue="value"
																list="#{'HIGH':'Parameters passed to an unmodified program','MEDIUM':'New program developed or existing program modified'}"
																theme="radioVertical" onclick="buildInputChanged()" /></td>
													</tr>
													<tr>
														<td class=" titleCell" colspan="2" valign="center"
															align="left" width="100%">
															<div style="WIDTH: 95%; HEIGHT: 100%">
																<span> <br>
																	<p><b>Test:<b></b></p>
																	<hr>
																</span>
															</div>
														</td>
													</tr>
													<tr id="correctParametersPassed">
														<td class="bodyCell" height="100%"><span>
																Correct parameters passed to unmodified program </span></td>
														<td class="secondCell"><s:checkbox
																name="correctParametersPassed" theme="simple" /></td>
													</tr>
													<tr id="codeReviewPassed">
														<td class="bodyCell" height="100%"><span> Code
																review performed with result 'Pass' </span></td>
														<td class="secondCell"><s:checkbox
																name="codeReviewPassed" theme="simple" /></td>
													</tr>
													<tr id="checkPassed">
														<td class="bodyCell" height="100%"><span>
																Plausibility check performed with result 'Pass' </span></td>
														<td class="secondCell"><s:checkbox name="checkPassed"
																theme="simple" /></td>
													</tr>
													<tr id="resultsVerified">
														<td class="bodyCell" height="100%"><span>
																Results verified using a check program </span></td>
														<td class="secondCell"><s:checkbox
																name="resultsVerified" theme="simple" /></td>
													</tr>
													<tr>
														<td class="bodyCell" height="100%"><span>
																Provide future reference if applicable </span></td>
														<td class="secondCell"><s:textarea
																cssClass="areaStyle" name="furtherTestReference" onblur="textCounter(this,500);"
															onkeypress="textCounter(this,500);"
																theme="simple" /></td>
													</tr>
													<tr>
														<td class=" titleCell" valign="center" colspan="2"
															align="left" width="100%">
															<div style="WIDTH: 95%; HEIGHT: 100%">
																<span> <br>
																	<p><b>Release and use:</b></p>
																	<hr>
																</span>
															</div>
														</td>
													</tr>
													<tr>
														<td class="bodyCell" height="100%"><span> The
																result files have been appropriately labeled </span></td>
														<td class="secondCell"><s:checkbox
																name="resultsAppropriatelyLabeled" theme="simple" /></td>
													</tr>
													<tr>
														<td class="bodyCell" height="100%"><span>
																Program files and result files have been stored in Query
																Directory </span></td>
														<td class="secondCell"><s:checkbox
																name="resultsStored" theme="simple" /></td>
													</tr>
													<tr>
														<td class="bodyCell" height="100%"><span>Search
																criteria and assumptions stated when results delivered </span>
														</td>
														<td class="secondCell"><s:checkbox
																name="searchCriteriaStated" theme="simple" /></td>
													</tr>
												</tbody>
											</table>

										</div>
									</td>
								</tr>
							</tbody>
						</table> <br /> Date completed <s:textfield name="date" size="13"
							maxlength="11" />&nbsp;&nbsp;<s:text name="struts.date.format" />
							<br />
							For digital Signature please enter password <s:password name="password" size="11"
							 />&nbsp;&nbsp;
							<br />
						<table cellspacing="0" cellpadding="1" border="0" width="100%">
							<tr>
								<td height="10" colspan="3"><img src="img/spacer.gif"
									height="10"></td>
							</tr>
							<tr>
								<td width="30%" valign="middle"><s:submit value="Submit" /></td>
								<td width="30%" valign="middle">
								
								  <s:if test="query.verifyPerson&&query.verifyPerson.equals(getLoggedUserName())">
								  	<s:submit value="Reject"
										action="RejectVerification" />
                                  </s:if>
							</td>
								<td width="30%" valign="middle"><s:submit value="Cancel"  action="CancelVerification"/></td>
							</tr>
						</table>



					</td>
					<td width="20"><img src="img/spacer.gif" width="20"></td>
				</tr>
				<tr>
					<td width="100%" height=20></td>
				</tr>
			</tbody>
		</table>
	</s:form>
</body>