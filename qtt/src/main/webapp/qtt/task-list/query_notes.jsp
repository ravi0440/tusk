<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<body onLoad="preload()">

<table cellSpacing=0 cellPadding=0 width="100%" border=0>
    <tbody>
    <tr>
        <td width="100%">
            <table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
                <tbody>
                <tr>
                    <td class=TitleExpanded
                        style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid"
                        vAlign=center align=left width="100%">
                        <div style="WIDTH: 100%; HEIGHT: 100%"><span>
								<p>All Notes Associated With Query</p></span></div>
                    </td>
                </tr>
                <tr>
                    <td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid"
                        colSpan=2 height="100%">
                        <DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%"
                             align=justify><SPAN>
								<P>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <td valign="top" colspan="3"><font size="+1">Query No. <s:property
                                                    value="query.queryNumber"/></font></td>
                                        </tr>
                                        <tr>
                                            <td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">
                                                <b>Drug Name</b>
                                            </td>
                                            <td width="70%" colspan="3">
                                                <s:property value="query.drugName"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Response Date</b>
                                            </td>
                                            <td colspan="3">
                                                <s:property value="query.currentDueDateFormatted"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Query Type</b>
                                            </td>
                                            <td colspan="3">
                                                <s:property value="query.queryType.queryType"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Requester</b>
                                            </td>
                                            <td colspan="3">
                                                <s:property value="query.requester.name"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" colspan="4"><img src="img/spacer.gif" height="20"></td>
                                        </tr>
                                    </table>

                                        <TABLE class=DataGrid
                                               style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc"
                                               borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
                                            <TBODY>
                                            <TR class=DataGridHeaderTitle2>
                                                <TD colspan="4" style="WIDTH: 100%">Query Notes</TD>
                                            </TR>

                                            <TR class=DataGridHeader2>
                                                <s:if test="!query.followupEmpty">
                                                <TD class=DataGridHeader2 style="WIDTH: 10%">Follow Up</TD>
                                                </s:if>
                                                <TD class=DataGridHeader2 style="WIDTH: 15%">Date</TD>
                                                <TD class=DataGridHeader2 style="WIDTH: 10%">Author</TD>
                                                <TD class=DataGridHeader2 style="WIDTH: 62%">Note</TD>
                                                <TD class=DataGridHeader2 style="WIDTH: 30"></TD>
                                            </TR>

                                            <s:if test="affiliate">
                                                <s:iterator value="query.affiliateNotes" status="stat">
                                                <TR>
                                                    <s:if test="!query.followupEmpty">
                                                    <TD class=DataGridItem>&nbsp;<s:property value="query.followupNumber"/></TD>
                                                    </s:if>
                                                    <TD class=DataGridItem>&nbsp;<joda:format value="${createTs}" pattern="dd-MMM-yyyy" locale="en"/></TD>
                                                    <TD class=DataGridItem>&nbsp;<s:property value="username"/></TD>
                                                    <TD class=DataGridItem><s:property value="text"/></TD>
                                                    <TD class=DataGridItem valign="middle" align="center"><a href="ManageQueryNote.action?id=<s:property value="noteSeq" />&queryId=<s:property value="queryId" /><s:if test="%{(#parameters.taskId != null) && (#parameters.taskId != '')}">&fromTask=true</s:if>" onmouseover="hilight('imgget<s:property value="#stat.index"/>')" onmouseout="lolight('imgget<s:property value="#stat.index"/>')" class="Anchor"><img name="imgget<s:property value="#stat.index"/>" src="img/find_off.gif" title="Edit/Delete Notes" border="0"></a></TD>
                                                </TR>
                                                </s:iterator>
                                             </s:if>
                                             <s:else>
                                                <s:iterator value="query.nonAffiliateNotes" status="stat">
                                                <TR>
                                                    <s:if test="!query.followupEmpty">
                                                    <TD class=DataGridItem>&nbsp;<s:property value="query.followupNumber"/></TD>
                                                    </s:if>
                                                    <TD class=DataGridItem>&nbsp;<joda:format value="${createTs}" pattern="dd-MMM-yyyy" locale="en"/></TD>
                                                    <TD class=DataGridItem>&nbsp;<s:property value="username"/></TD>
                                                    <TD class=DataGridItem><s:property value="text"/></TD>
                                                    <TD class=DataGridItem valign="middle" align="center"><a href="ManageQueryNote.action?id=<s:property value="noteSeq" />&queryId=<s:property value="queryId" /><s:if test="%{(#parameters.taskId != null) && (#parameters.taskId != '')}">&fromTask=true</s:if>" onmouseover="hilight('imgget<s:property value="#stat.index"/>')" onmouseout="lolight('imgget<s:property value="#stat.index"/>')" class="Anchor"><img name="imgget<s:property value="#stat.index"/>" src="img/find_off.gif" title="Edit/Delete Notes" border="0"></a></TD>
                                                </TR>
                                                </s:iterator>
                                             </s:else>

                                            </TBODY>
                                        </TABLE>
                                    <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <TD height="10" colspan="2"><img src="img/spacer.gif" height="10"></TD>
                                        </tr>
                                        <tr>
                                            <td><a href="NewQueryNote.action?queryId=<s:property value="queryId" /><s:if test="%{(#parameters.taskId != null) && (#parameters.taskId != '')}">&fromTask=true</s:if> " class="Anchor" onmouseover="hilight('img01')" onmouseout="lolight('img01')">
                                                <img name="img01" src="img/add_off.gif" alt="" border="0"></a></td>
                                            <td width="100%%">&nbsp;Add New Note</td>
                                        </tr>
                                    </table>

                                    <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <TD height="10" colspan="3"><img src="img/spacer.gif" height="10"></TD>
                                        </tr>
                                        <tr>
                                            <td width="100%"><b>Click <u><a href="RetrieveQueryTaskList.action" class="Anchor">here</a></u> to return to
                                                task-list...</b></td>
                                        </tr>
                                    </table>

                            </p>
                            </span></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td width="20"><img src="img/spacer.gif" width="20"></td>
    </tr>
    <tr>
        <td width="100%" height=20></td>
    </tr>
    </tbody>
</table>


</td>
</tr>
</tbody>
</table>

</body>