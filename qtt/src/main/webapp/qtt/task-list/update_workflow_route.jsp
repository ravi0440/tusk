<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:form action="UpdateProcessRoute" theme="simple">
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
	<tbody>
		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=middle align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%">
									<p>Query Information</p>
								</div>
							</td>
						</tr>
                        <s:if test="hasActionErrors() || hasFieldErrors()">
							<%@ include file="../global/validation_error.jsp" %>
						</s:if>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Query No. <s:property value="query.queryNumber" /></font></td>
										</tr>
										<tr>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="30%">
												<b>Drug Name</b>
											</td>
											<td width="70%" colspan="3">
												<s:property value="query.drugName" />
											</td>
										</tr>
										<tr>
											<td>
												<b>Query Type</b>
											</td>
											<td colspan="3">
												<s:property value="query.queryType.queryType"/>
											</td>
										</tr>
										<tr>
											<td>
												<b>Requester</b>
											</td>
											<td colspan="3">
												<s:property value="query.requester.name"/>
											</td>
										</tr>
										<tr>
											<td>
												<b>Query Label</b>
											</td>
											<td colspan="3">
												<s:textfield name="queryLabel" size="70" maxlength="300" value="%{query.queryLabel}"/>
											</td>
										</tr>
										<tr>
											<td height="20" colspan="4"><img src="img/spacer.gif" height="20"></td>
										</tr>							
										<tr>
											<td colspan="4"><img src="img/spacer.gif" height="10"></td>
										</tr>
                                        <s:if test="%{query.workflowRouteType != null}">
	                                    	<tr>
												<td colspan="4">Workflow route previously selected as: <s:property value="query.workflowRouteType"/></td>
											</tr>
											<tr>
												<td colspan="4"><img src="img/spacer.gif" height="10"></td>
											</tr>
											<tr>
												<td colspan="4"><img src="img/spacer.gif" height="10"></td>
											</tr>
                                        </s:if>
										<tr>
											<td colspan="1">EU Regulatory Authority Query:</td>
											<td colspan="3"><s:checkbox name="euqppv" value="euqppv"></s:checkbox></td>
										</tr>
										<tr>
											<td colspan="4">Please select a workflow route appropriate to the query type:</td>
										</tr>
										<tr>
											<td colspan="4"><img src="img/spacer.gif" height="10"></TD>
										</tr>
									    <tr>
											<td width="20%">&nbsp;</td>
											<td width="80%" colspan="3">
												<s:radio name="selectedRoute" list="routes" listKey="name" listValue="viewLabel" theme="radioVertical" value="query.workflowRouteType" />
											</td>
										</tr>
										
										<tr>
											<td colspan="4"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td align="left">
												<s:submit value="Submit" />
											</td>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td colspan="4">
												<img src="img/spacer.gif" border="0" width="1" height="10">
												<s:hidden name="queryId" value="%{query.querySeq}"/>
											</td>
										</tr>									
									</table>
                              	</div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=20></td>
		</tr>
  	</tbody>
</table>

</s:form>
