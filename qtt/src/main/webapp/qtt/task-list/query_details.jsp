<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<body onLoad="preload()">
<script language="javascript" src="js/window.js"></script>
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<tbody>
  		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=middle align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%">
									Information
								</div>
							</td>
						</tr>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Query No. <s:property value="query.queryNumber" />  <s:property value="query.followupNumber" /></font></td>
										</tr>
										<tr>
											<td valign="top" colspan="3">
												<s:if test="query.followupQueries.size() > 0">
													<table cellspacing="0" cellpadding="1" border="0" width="100%">
														<tbody>
															<s:iterator value="query.followupQueries">
																<tr>
																	<td style="WIDTH: 25%">&nbsp;</td>
																	<td style="WIDTH: 10%">
																	<a href="javascript:onclick=openQueryFollowup('<s:property value="query.querySeq" />', '<s:property value="id.followupSeq" />');"><s:property value="followupNumber" /></a>
																	</td>
															 		<td style="WIDTH: 65%">&nbsp;</td>
																</tr>
															</s:iterator>
														</tbody>
													</table>
												</s:if>
											</td>
										</tr>
										<tr>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="15%"><b>Drug Name</b></td>
											<td width="45%"><s:property value="query.drugName" /></td>
											<td width="40%">&nbsp;</td>
										</tr>
										<tr>
											<td><b>Query Label</b></td>
											<td>
												<script language="javascript">
													document.write(showLabel('<s:property value="query.queryLabel"/>', '<s:property value="query.querySeq"/>'));
												</script>
												<%--<s:property value="query.queryLabel" />--%>
											</td>
											<td>
												<s:if test="dscl">
													<b><a href="EnterQueryLabel.action?queryId=<s:property value="query.querySeq" />&from=task&taskId=<s:property value="taskInstance.id" />" class="Anchor" >Change</a> query label...</b>
												</s:if>
											</td>
										</tr>
                                        <tr>
											<td><b>Query Type</b></td>
											<td><s:property value="query.queryType.queryType"/></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><b>Initiated</b></td>
											<td><s:property value="query.initiateDateFormatted" /></td>
											<td>&nbsp;</td>
										</tr>
                                    	<tr>
											<td><b>Response Date</b></td>
											<td><s:property value="query.currentDueDateFormatted"/></td>
											<td>
												<s:if test="displayUpdateResponseDate">
													<b><a href="EnterResponseDate.action?queryId=<s:property value="query.querySeq" />&taskId=<s:property value="taskInstance.id" />" class="Anchor" >Change</a> this date...</b>
												</s:if>
											</td>
										</tr>
                                    	<tr>
											<td><b>DMG Response Date</b></td>
											<td><s:property value="query.dmgDueDateFormatted"/></td>
											<td>
												<s:if test="displayUpdateDMGDate">
													<b><a href="EnterDMGResponseDate.action?queryId=<s:property value="query.querySeq" />&taskId=<s:property value="taskInstance.id" />" class="Anchor" >Change</a> this date...</b>
												</s:if>
											</td>
										</tr>
										<tr>
											<td><b>Requester</b></td>
											<td><s:property value="query.requester.name"/></td>
											<td>&nbsp;</td>
										</tr>
                                        <tr>
											<td><b>Current Activity</b></td>
											<td><s:property value="taskInstance.name"/></td>
											<td>&nbsp;</td>
										</tr>
                                    	<tr>
											<td><b>Current Owner</b></td>
											<td><s:property value="currentOwner"/></td>
											<td>&nbsp;</td>
										</tr>
                                        <tr>
											<td><b>Urgent</b></td>
											<td>
                                                <s:if test="query.urgencyFlag=='Y'">Yes</s:if>
                                                <s:else>No</s:else>
                                            </td>
											<td>
												<s:if test="dscl">
													<b><a href="UpdateUrgency.action?queryId=<s:property value="query.querySeq" />&taskId=<s:property value="taskInstance.id" />" class="Anchor">Change</a> the urgency flag...</b>
												</s:if>
											</td>
										</tr>
                                        <tr>
											<td><b>Process Type</b></td>
											<td><s:property value="query.workflowRouteType"/></td>
											<td>
												<s:if test="displayDispatch">
													<b><a href="SelectWorkflowProcess.action?queryId=<s:property value="query.querySeq"/>&taskId=<s:property value="taskInstance.id"/>&taskOwner=<s:property value="taskInstance.actorId"/>" class="Anchor" onClick="disableAllLinks()">Dispatch</a> query for further processing...</b>
												</s:if>
											</td>
										</tr>
										<tr>
											<td><b>View QRF</b></td>
											<td>
												<a href="javascript:onclick=openQrfNoAttachments('<s:property value="query.querySeq"/>');" onmouseover="hilight('img00')" onmouseout="lolight('img00')"><img name="img00" src="img/find_off.gif" title="" border="0"></a>
											</td>
                                            <td>&nbsp;</td>
										</tr>
										<s:if test="displayRestartWorkflow">
											<tr>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>
	                                                <b><a href="RestartWorkflowProcess.action?queryId=<s:property value="query.querySeq"/>&taskId=<s:property value="taskInstance.id"/>&taskOwner=<s:property value="taskInstance.actorId"/>" class="Anchor" >Re-Select</a> workflow for further processing...</b>
												</td>
											</tr>
										</s:if>
										<tr>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<s:if test="displayAmendQuery">
											<tr>
												<td colspan="2">&nbsp;</td>
												<td>
													<b><a href="PreviewAction.action?queryId=<s:property value="query.querySeq" />&viewtype=assignAmend" class="Anchor" >Amend Query</a></b>
												</td>
											</tr>
										</s:if>
										<s:if test="displayDeleteQuery">
											<tr>
											<tr>
												<td colspan="2">&nbsp;</td>
												<td>
													<b><a href="PreviewAction.action?queryId=<s:property value="query.querySeq" />&viewtype=assignDelete" class="Anchor" >Delete Query</a></b>
												</td>
											</tr>
											</tr>
										</s:if>
										<tr>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>
									</table>
									<s:if test="!query.followupEmpty">
										<table class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
											<tbody>
												<tr class=DataGridHeaderTitle>
													<td colspan="6" style="WIDTH: 100%">Attach Documents</td>
												</tr>
												<tr class=DataGridHeader>
													<td class=DataGridHeader style="WIDTH: 10%">Follow Up</td>
													<td class=DataGridHeader style="WIDTH: 12%">Date Deposited</td>
													<td class=DataGridHeader style="WIDTH: 12%">Category</td>
													<td class=DataGridHeader style="WIDTH: 50%">Document Name</td>
													<td class=DataGridHeader style="WIDTH: 10%">Deposited By</td>
													<td class=DataGridHeader style="WIDTH: 30"></td>
												</tr>
												<s:iterator value="query.attachmentsForDisplay" status="stat">
													<tr>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="fuQueryNumber"/></td>
												 		<td class=DataGridItem style="VERTICAL-ALIGN: middle"><joda:format value="${createTs}" pattern="dd-MMM-yyyy" locale="en"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="attachmentCategory.attachmentCategory"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="filenameUpper"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="author"/></td>
														<td class=DataGridItem valign="middle" align="center">
                                                        <a target="attachments" href="Attachment.action?attachId=<s:property value="attachmentSeq"/>&queryId=<s:property value="querySeq"/>" onmouseover="hilight('imgd<s:property value="#stat.index" />')" onmouseout="lolight('imgd<s:property value="#stat.index" />')">
                                                            <img name="imgd<s:property value="#stat.index" />" src="img/find_off.gif" title="View/Open Document" border="0">
                                                        </a>
                                                        </td>
													</tr>
												</s:iterator>
											</tbody>
										</table>
									</s:if>
                                	<s:else>
										<table class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
											<tbody>
												<tr class=DataGridHeaderTitle>
													<td colspan="6" style="WIDTH: 100%">Attach Documents</td>
												</tr>
												<tr class=DataGridHeader>
													<td class=DataGridHeader style="WIDTH: 15%">Date Deposited</td>
													<td class=DataGridHeader style="WIDTH: 15%">Category</td>
													<td class=DataGridHeader style="WIDTH: 55%">Document Name</td>
													<td class=DataGridHeader style="WIDTH: 15%">Deposited By</td>
													<td class=DataGridHeader style="WIDTH: 30"></td>
												</tr>
												<s:iterator value="query.attachmentsForDisplay" status="stat">
													<tr>
												 		<td class=DataGridItem style="VERTICAL-ALIGN: middle"><joda:format value="${createTs}" pattern="dd-MMM-yyyy" locale="en"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="attachmentCategory.attachmentCategory"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="filenameUpper"/></td>
														<td class=DataGridItem style="VERTICAL-ALIGN: middle"><s:property value="author"/></td>
														<td class=DataGridItem valign="middle" align="center"><a target="attachments" href="Attachment.action?attachId=<s:property value="attachmentSeq"/>&queryId=<s:property value="querySeq"/>" onmouseover="hilight('imgd<s:property value="#stat.index" />')" onmouseout="lolight('imgd<s:property value="#stat.index" />')"><img name="imgd<s:property value="#stat.index" />" src="img/find_off.gif" title="View/Open Document" border="0"></a></td>
													</tr>
												</s:iterator>
											</tbody>
										</table>
									</s:else>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
											<tr>
												<td width="70%" valign="middle">
													<b>Click <a href="AttachmentActivationList.action?queryId=<s:property value="query.querySeq"/>&taskId=<s:property value="taskInstance.id"/>&initDate=<s:date format="dd-MMM-yyyy" name="taskInstance.create" />" class="Anchor" >here</a> to deactivate/reactivate attachment...</b>
												</td>
											</tr>
									</table>
									<br>
									<s:if test="!query.followupEmpty">
										<table class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
											<tbody>
												<tr class=DataGridHeaderTitle1>
													<td colspan="4" style="WIDTH: 100%">Activity History</td>
												</tr>
												<tr class=DataGridHeader1>
													<td class=DataGridHeader1 style="WIDTH: 10%">Follow Up</td>
													<td class=DataGridHeader1 style="WIDTH: 15%">Date</td>
													<td class=DataGridHeader1 style="WIDTH: 60%">Activity</td>
													<td class=DataGridHeader1 style="WIDTH: 15%">User</td>
												</tr>
												<s:if test="tasks.size() > 0">
													<s:iterator value="tasks">
														<tr>
															<td class=DataGridItem>&nbsp;
															<s:if test="query.processInstanceId == processInstance.id">
																<s:property value="query.followupNumber" />
															</s:if>
															</td>
															<td class=DataGridItem>&nbsp;<s:date name="end" format="dd-MMM-yyyy"/></td>
															<td class=DataGridItem>&nbsp;<s:property value="name"/></td>
															<td class=DataGridItem><s:property value="actorId"/></td>
														</tr>
													</s:iterator>												
												</s:if>
											</tbody>
										</table>
									</s:if>
									<s:else>
										<table class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
											<tbody>
												<tr class=DataGridHeaderTitle1>
													<td colspan="3" style="WIDTH: 100%">Activity History</td>
												</tr>
												<tr class=DataGridHeader1>
													<td class=DataGridHeader1 style="WIDTH: 15%">Date</td>
													<td class=DataGridHeader1 style="WIDTH: 70%">Activity</td>
													<td class=DataGridHeader1 style="WIDTH: 15%">User</td>
												</tr>
												<s:if test="tasks.size() > 0">
													<s:iterator value="tasks">
														<tr>
															<td class=DataGridItem>&nbsp;<s:date name="end" format="dd-MMM-yyyy"/></td>
															<td class=DataGridItem>&nbsp;<s:property value="name"/></td>
															<td class=DataGridItem><s:property value="actorId"/></td>
														</tr>
													</s:iterator>
												</s:if>
											</tbody>
										</table>
									</s:else>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td width="30%" valign="middle">
												<table cellspacing="0" cellpadding="0" border="0">
													<tr>
                                                        <s:if test="!affiliate">
														<td><a href="mailto:<s:property value="query.requester.email"/>?subject=<s:property value="query.queryNumber"/>&cc=<s:property value="dsclEmail"/>" onmouseover="hilight('img03')" onmouseout="lolight('img03')"><img name="img03" src="img/find_off.gif" title="" border="0"></a></td>
														<td width="100%">&nbsp;Contact Requester</td>
                                                        </s:if>
													</tr>
												</table>
											</td>
											<td width="30%" valign="middle">
												<table cellspacing="0" cellpadding="0" border="0">
													<tr>
                                                        <s:if test="!affiliate">
                                                        <s:if test="query.verifyPerson&&query.verifyPerson.equals(getLoggedUserName())&&!(taskInstance.getActorId().equals(getLoggedUserName()) )">
                                                        <!-- Verifier cannot attach files -->
                                                        </s:if>
                                                        <s:else>
														<td><a href="AddNewAttachment.action?queryId=<s:property value="query.querySeq"/>&taskId=<s:property value="taskInstance.id" />" onmouseover="hilight('img04')" onmouseout="lolight('img04')"><img name="img04" src="img/add_off.gif" title="" border="0"></a></td>
														<td width="100%">&nbsp;Add New Document</td>
														 </s:else>
                                                        </s:if>
													</tr>
												</table>
											</td>
										</tr>
										<s:if test="displayContactOwner">
											<tr>
												<td width="30%" valign="middle">
													<table cellspacing="0" cellpadding="0" border="0">
														<tr>
															<td><a href="mailto:<s:property value="ownerEmail"/>?subject=<s:property value="query.queryNumber"/>" onmouseover="hilight('img05')" onmouseout="lolight('img05')"><img name="img05" src="img/find_off.gif" title="" border="0"></a></td>
															<td width="100%">&nbsp;Contact Owner</td>
														</tr>
													</table>
												</td>
											</tr>
										</s:if>
										<tr>
											<td width="30%" valign="middle">
												<table cellspacing="0" cellpadding="0" border="0">
													<tr>
														<td><a href="mailto:<s:property value="dsclEmail"/>?subject=<s:property value="query.queryNumber"/>" onmouseover="hilight('img06')" onmouseout="lolight('img06')"><img name="img06" src="img/find_off.gif" title="" border="0"></a></td>
														<td width="100%">&nbsp;Contact DSCL</td>
													</tr>
												</table>
											</td>
											<td width="30%" valign="middle">
												<table cellspacing="0" cellpadding="0" border="0">
													<tr>
													    <s:if test="displayVerification()">
													    <td><a href="VerifyQuery.action?queryId=<s:property value="query.querySeq"/>&taskId=<s:property value="taskInstance.id" />" onmouseover="hilight('img04')" onmouseout="lolight('img04')"><img name="img04" src="img/find_off.gif" title="" border="0"></a></td>
														<td width="100%">&nbsp;Verify Query</td>
														</s:if>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<s:if test="displayFinalResponseSent">
										
											<s:if test="displaywaitingForVerification">										
											<tr>
												<td width="100%">Waiting for query approval by Verifier</td>
											</tr>
											</s:if>
											<s:if test="!displaywaitingForVerification">
											<tr>
												<td width="100%"><b>Click <a href='ProcessResponseEmail.action?queryId=<s:property value="query.querySeq"/>' class="Anchor">here</a>
												to indicate final response has been sent back to requester...</b></td>
											</tr>
											</s:if>
											<tr>
												<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
											</tr>
										</s:if>
										<tr>
											<td width="100%"><b>Click <a href="RetrieveQueryTaskList.action" class="Anchor" onClick="disableAllLinks()">here</a> to return to task-list...</b></td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
		<tr>
			<td width="100%" height=20></td>
		</tr>
	</tbody>
</table>

</body>