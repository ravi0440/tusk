<%@ page language="java" import="com.roche.dss.qtt.service.workflow.WorkflowGlobalConstants" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<title>DMG to Safety Science/Medical Information Confirmation</title>
<link href="css/dialog.css" type=text/css rel=stylesheet>
<script language="javascript" src="js/global_script.js"></script>
<script language="javascript">
	function doAttachment() {		
			var args = "RetrieveQueryDetails.action?queryId=" + "<%=(Long)request.getAttribute("queryId")%>" + "&taskId=" + "<%=(Long)request.getAttribute("taskId")%>";
		    window.returnValue = args;
		    window.close();
	}
	function doNote() {		
			var args = "RetrieveQueryNotes.action?queryId=" + "<%=(Long)request.getAttribute("queryId")%>" + "&taskId=" + "<%=(Long)request.getAttribute("taskId")%>";
		    window.returnValue = args;
		    window.close();
	}
	function sendToMeg() {		
			var args = "TaskComplete.action?nextActivity=" + "<%=WorkflowGlobalConstants.DMG_MI_COMPLETE_ACTIVITY%>" + "&taskId=" + "<%=(Long)request.getAttribute("taskId")%>"; 
		    window.returnValue = args;
		    window.close();
	}
</script>
</head>
<body>
<div class="DialogMessage">Have you attached search specifications and data/literature outputs?</div>
<table>
	<tr>
		<td width="6%">&nbsp;</td>
		<td width="27%"><input type="button" name="dmg" value="Yes, Send to Safety Science"
			class="button_bigger" onclick="sendToMeg();"
			onmouseover="hover(this,'button_bigger_hover')"
			onmouseout="hover(this,'button_bigger')"></td>
		<td>&nbsp;</td>
		<td width="22%"><input type="button" name="note" value="No, Add Note"
			class="button" onclick="doNote();"
			onmouseover="hover(this,'buttonhover')"
			onmouseout="hover(this,'button')"></td>
		<td>&nbsp;</td>
		<td width="22%"><input type="button" name="attach"
			value="No, Attach Document" class="button" onclick="doAttachment();"
			onmouseover="hover(this,'buttonhover')"
			onmouseout="hover(this,'button')"></td>
		<td>&nbsp;</td>
		<td width="17%"><input type="button" name="cancel" value="Cancel"
			class="button" onclick="window.close();"
			onmouseover="hover(this,'buttonhover')"
			onmouseout="hover(this,'button')"></td>
		<td width="6%">&nbsp;</td>
	</tr>
</table>
</body>
</html>
