<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<body>

<s:form action="UploadAttachment" theme="simple" enctype="multipart/form-data" method="POST">
    <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR>
            <TD width="100%">
                <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TBODY>
                    <TR>
                        <TD class=TitleExpanded
                            style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid"
                            vAlign=center align=left width="100%">
                            <DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Specify Document to be Added</P></SPAN></DIV>
                        </TD>
                    </TR>
                    <s:if test="hasActionErrors() || hasFieldErrors()">
                       <%@ include file="../global/validation_error.jsp" %>
                    </s:if>
                    <TR>
                        <TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid"
                            colSpan=2 height="100%">
                            <DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%"
                                 align=justify><SPAN>
								<P>

									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <tr>
                                            <td valign="top" colspan="3"><font size="+1">Query No. <s:property value="query.queryNumber" /></font></td>
                                        </tr>
                                        <tr>
                                            <TD height="20" colspan="3"><img src="img/spacer.gif" height="20"></TD>
                                        </tr>
                                        <tr>
                                            <td width="30%">
                                                <b>Drug Name</b>
                                            </td>
                                            <td width="70%" colspan="3">
                                                <s:property value="query.drugName"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Response Date</b>
                                            </td>
                                            <td colspan="3">
                                                <joda:format value="${query.currentDueDate}" pattern="dd-MMM-yyyy" locale="en"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Query Type</b>
                                            </td>
                                            <td colspan="3">
                                                <s:property value="query.queryType.queryType"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Requester</b>
                                            </td>
                                            <td colspan="3">
                                                <s:property value="query.requester.name"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <TD height="20" colspan="4"><img src="img/spacer.gif" height="20"></TD>
                                        </tr>

                                        <tr>
                                            <td width="30%" valign="center">
                                                Document Name
                                            </td>
                                            <td>
                                                <s:textfield name="attachment.givenFileName" size="60" maxlength="100"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%" valign="center">
                                                Document Type
                                            </td>
                                            <td>
                                                <s:select name="attachment.type" list="attachmentCategoryList" listKey="id"
                                                          listValue="name" emptyOption="true"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%" valign="center">
                                                Select File
                                            </td>
                                            <td>
                                                <s:file name="attachment.upload" size="60" onchange="validateFileSize(this)"/>
                                                <s:hidden name="freeDiskSpace" id="freeDiskSpace"/>
                                            </td>
                                        </tr>
                                    </table>


                                </P></SPAN></DIV>
                        </TD>
                    </TR>
                    </TBODY>
                </TABLE>
            </TD>
            <TD width="20"><img src="img/spacer.gif" width="20"></TD>
        </TR>
        <TR>
            <TD width="100%" height=20></TD>
        </TR>
        </TBODY>
    </TABLE>


    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
            <td align="left" width="100%">
                <s:submit value="Submit"/>
            </td>
            <TD height="20" colspan="5">
                <img src="img/spacer.gif" height="20">
                <s:hidden name="queryId" value="%{query.querySeq}"/>
                <s:hidden name="closed" value="%{query.queryStatusCode.statusClosed}"/>
                <s:if test="%{(#parameters.taskId != null) && (#parameters.taskId != '')}">
                       <s:hidden name="taskId" value="%{#parameters.taskId}"/>
                </s:if>
            </TD>
        </tr>
    </table>

    </TD>
    </TR>
    </TBODY>
    </TABLE>
</s:form>

</body>