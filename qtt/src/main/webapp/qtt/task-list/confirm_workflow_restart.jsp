<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<body>

<table cellSpacing=0 cellPadding=0 width="100%" border=0>
	<tbody>
		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%"><span>
								<p>Query Information</p></span></div></td>
						</tr>
                        <s:if test="hasActionErrors() || hasFieldErrors()">
                             <%@ include file="../global/validation_error.jsp" %>
                        </s:if>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>
								<P>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Query No. <s:property value="query.queryNumber" /></font></td>
										</tr>
										<tr>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="30%">
												<b>Drug Name</b>
											</td>
											<td width="70%" colspan="3">
												<s:property value="query.drugName" />
											</td>
										</tr>

										<tr>
											<td>
												<b>Query Type</b>
											</td>
											<td colspan="3">
												<s:property value="query.queryType.queryType"/>
											</td>
										</tr>
										<tr>
											<td>
												<b>Requester</b>
											</td>
											<td colspan="3">
												<s:property value="query.requester.name"/>
											</td>
										</tr>
										<tr>
											<td height="20" colspan="4"><img src="img/spacer.gif" height="20"></td>
										</tr>


										<tr>
											<td colspan="4"><img src="img/spacer.gif" height="10"></TD>
										</tr>
										<tr>
											<td colspan="4">Workflow route currently selected as: <s:property value="query.workflowRouteType"/></td>
										</tr>
										<tr>
											<td colspan="4"><img src="img/spacer.gif" height="10"></TD>
										</tr>

										<tr>
											<td colspan="4"><img src="img/spacer.gif" height="10"></TD>
										</tr>
										<tr>
											<td align="left" colspan="3"><b>Click <a href="ConfirmProcessRestart.action?queryId=<s:property value="query.querySeq"/>&processId=<s:property value="query.processInstanceId"/>&taskId=<s:property value="taskId"/>&taskOwner=<s:property value="taskOwner"/>&restart=true " onClick="disableAllLinks()">here</a> to confirm re-select workflow route. </b></td>
											<TD height="20"><img src="img/spacer.gif" height="20"></TD>
										</tr>
										<tr>
											<td align="left" colspan="4">Re-selecting of workflow will start the process from the beginning.
												All outstanding tasks related to this query will disappear from user's task-list and you need to re-select workflow for further processing.</td>
										</tr>

										<tr>
											<TD colspan="4"><IMG src="img/spacer.gif" border="0" width="1" height="10"></TD>
										</tr>
										<tr>
											<TD colspan="4"><IMG src="img/spacer.gif" border="0" width="1" height="10"></TD>
										</tr>
										<tr>
											<TD colspan="4"><IMG src="img/spacer.gif" border="0" width="1" height="10"></TD>
										</tr>

                                    	<tr>
											<td align="left" colspan="3"><b>Click <a href="RetrieveQueryTaskList.action" class="Anchor" onClick="disableAllLinks()">here</a> to return to task-list...</b></td>
											<TD height="20"><img src="img/spacer.gif" height="20"></TD>
										</tr>
										<tr>
											<TD colspan="4"><IMG src="img/spacer.gif" border="0" width="1" height="10"></TD>
										</tr>
									</table>
								</p>
                                </span></div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=20></td>
		</tr>
  	</tbody>
</table>

</td>
</tr>
</tbody>
</table>

</body>
</html>