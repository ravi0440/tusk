<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script src="js/jquery-2.1.4.min.js"></script>
<script>
 function peerReviewRequired(){
	 var companyRiskLevel = $('input[name="companyRiskLevel"]:checked').val();
	 var programCodeCategory = $('input[name="programCodeCategory"]:checked').val();
	 var complexQuery = $('input[name="complexQuery"]:checked').val();
	 var highUserExperience = $('input[name="highUserExperience"]:checked').val();

	 if(<s:property value="query.querySeq"/> % 20 == 0){
		 return true; 
     }
	
	if (companyRiskLevel==='HIGH') {
		return true;
	} else if (companyRiskLevel==='MEDIUM') {
			
			if(programCodeCategory==='NEW'){
				if( highUserExperience==='NO'){
					return true;
				}else{
					return false;
				}
			}else if(programCodeCategory==='CHANGED'){
					if(complexQuery==='YES'&& highUserExperience==='NO'){
						return true;
					}else{
						return false;
					}
			}else if (programCodeCategory==='UNCHANGED'){
					return false;
			}
			
	} else if (companyRiskLevel==='LOW'&&programCodeCategory==='NEW'&&complexQuery==='YES'&& highUserExperience==='NO') {
         return true;
	}

	  return false;

	}

 function queryAssessmentChanged(){
	 if(peerReviewRequired()){
		 //show verifiers list
		 $("#verifierCheck").show();
	 }else{
		 //hide verifiers list
		 $("#verifierCheck").hide();
	 }
}


 $( document ).ready(function() {
	 queryAssessmentChanged();
	});
</script>
<s:form action="DMGResourceAssignment" method="POST" onsubmit="disableFormSubmit(this);">
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<tbody>
  		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=middle align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%">
									<p>Query Information</p>
								</div>
							</td>
						</tr>
						<s:if test="hasActionErrors() || hasFieldErrors()">
							<%@ include file="../global/validation_error.jsp" %>
						</s:if>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colspan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Query No. <s:property value="query.queryNumber"/></font></td>
										</tr>
										<tr>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="25%">
												<b>Drug Name</b>
											</td>
											<td width="65%" colspan="3">
												<s:property value="query.drugName"/>
											</td>
										</tr>
										<tr>
											<td>
												<b>Query Type</b>
											</td>
											<td colspan="3">
												<s:property value="query.queryType.queryType"/>
											</td>
										</tr>
										<tr>
											<td>
												<b>Requester</b>
											</td>
											<td colspan="3">
												<s:property value="query.requester.name"/>
									 		</td>
										</tr>
										<tr>
											<td>
												<b>Current Activity</b>
											</td>
											<td colspan="3">
												<s:property value="taskName"/>
											</td>
										</tr>
										<tr>
											<td height="20" colspan="4"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="30%"colspan="2" valign="middle">Please make the Query assessment:</td>
										</tr>
										<tr>
											<td width="20%">
												<b>Company Risk Level</b><br>
												<s:radio  name="companyRiskLevel" value="companyRiskLevel" listKey="key" listValue="value" list="#{'HIGH':'High','MEDIUM':'Medium','LOW':'Low'}" theme="radioVertical"  onclick="queryAssessmentChanged()"/>
											</td>
											<td width="30%">
 												<b>Program Code Category</b><br>
											 	<s:radio name="programCodeCategory" value="programCodeCategory" listKey="key" listValue="value" list="#{'NEW':'New Program','CHANGED':'Validated program changed','UNCHANGED':'Validated program unchanged'}" onclick="queryAssessmentChanged()" theme="radioVertical"/>
											</td>
											<td width="20%">
												<b>Complex Query</b><br>
											 	<s:radio name="complexQuery" value="complexQuery" listKey="key" listValue="value" list="#{'YES':'Yes','NO':'No'}" onclick="queryAssessmentChanged()" theme="radioVertical"/>
											</td>
											<td width="30%">
												<b>High User Experience and Knowledge</b><br>
											 	<s:radio name="highUserExperience" value="highUserExperience" listKey="key" listValue="value" list="#{'YES':'Yes','NO':'No'}" onclick="queryAssessmentChanged()" theme="radioVertical"/>
											 </td>
										</tr>
										<tr>
											<td height="20" colspan="4"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td colspan="4">
													Assessment comment: <s:textfield name="comment" size="80" maxlength="80"  theme="simple"/>
												</td>
										</tr>
										<tr>
											<td height="20" colspan="4"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="30%"colspan="2" valign="middle">Please assign a DMG Scientist to handle the data search for this query:</td>
										</tr>
										<tr>
											
											<td width="30%" colspan="2">
											    <b>Responsible for Outputs ("Owner")</b><br>
												<s:radio name="userForDMG" listKey="userName" listValue="userFullName" list="resourceList" theme="radioVertical"/>
											</td>
											<td width="20%" >
											    <span id="verifierCheck" style="display:none;">
											    <b>Responsible for Check ("Verifier")</b><br>
												<s:radio name="verifierForDMG"  value="verifierForDMG" listKey="userName" listValue="userFullName" list="resourceList" theme="radioVertical"/>
												</span>
											</td>
											<td width="20%">&nbsp;</td>
										</tr>
										<tr>
											<td colspan="4"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td>
												<b>Response Date</b>
											</td>
											<td colspan="3">
												<s:property value="query.currentDueDateFormatted"/>
											</td>
										</tr>
										<tr>
											<td colspan="4"><img src="img/spacer.gif" height="3"></td>
										</tr>				
										<tr>
											<td>
												DMG Response Due Date
											</td>
												<td>
													<s:textfield name="dmgResponseDueDate" size="13" maxlength="11" disabled="%{DMGResponseDueDateNotRequired}" theme="simple"/>&nbsp;&nbsp;<s:text name="struts.date.format"/>
												</td>
										</tr>
										<tr>
											<td colspan="2"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td>
												<s:submit name="ok" value="Submit" align="left"/>
											</td>
											<td height="20" colspan="5"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td colspan="2"><img src="img/spacer.gif" border="0" width="1" height="20"></td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=20></td>
		</tr>
  	</tbody>
</table>
</s:form>
