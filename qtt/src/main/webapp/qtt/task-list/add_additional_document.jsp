<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<body>

<s:form action="UploadAdditionalAttachment" theme="simple" enctype="multipart/form-data" method="POST">
    <table cellSpacing=0 cellPadding=0 width="100%" border=0>
        <tbody>
        <tr>
            <td width="100%">
                <table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <tbody>
                    <tr>
                        <td class="TitleExpanded tableBorderHeader">
                            Specify Document to be Added
                        </td>
                    </tr>
                    <s:if test="hasActionErrors() || hasFieldErrors()">
                        <%@ include file="../global/validation_error.jsp" %>
                    </s:if>
                    <tr>
                        <td class="tableBorderBody" colSpan=2 height="100%">
                            <div class="tablePadding" align=justify>
                                <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                    <tr>
                                        <td width="30%" valign="center">
                                            Document Name
                                        </td>
                                        <td>
                                            <s:textfield name="attachment.givenFileName" size="60" maxlength="100"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="center">
                                            Select File
                                        </td>
                                        <td>
                                            <s:file name="attachment.upload" size="60" onchange="validateFileSize(this)"/>
                                            <s:hidden name="freeDiskSpace" id="freeDiskSpace"/>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td width="20"><img src="img/spacer.gif" width="20"></td>
        </tr>
        <tr>
            <td width="100%" height=20></td>
        </tr>
        </tbody>
    </table>


    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
            <td align="left" width="100%">
                <s:hidden name="queryId" value="%{query.querySeq}"/>
                <s:submit value="Submit"/>
            </td>
        </tr>
    </table>
</s:form>

</body>