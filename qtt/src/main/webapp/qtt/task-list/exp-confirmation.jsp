<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head><title>Response Due Date expired</title>
<link href="css/dialog.css" type=text/css rel=stylesheet>
<script language="javascript" src="js/global_script.js"></script>
</head>
<body>
<div class="DialogMessage">The response Due Date has expired, please negotiate a new Due Date with the query originator</div>
<table>
	<tr>
		<td width="39%">&nbsp;</td>
		<td width="22%"><input type="button" name="cancel" value="Cancel"
			class="button" onclick="window.close();"
			onmouseover="hover(this,'buttonhover')"
			onmouseout="hover(this,'button')"></td>
		<td width="39%">&nbsp;</td>
	</tr>
</table>
</body>
</html>
