<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<body onload="preload()">
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
	<tbody>
		<tr>
			<td width="100%">
				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td>
								<table class=DataGridEmphasis style="BORDER-LEFT-COLOR: #009900; BORDER-BOTTOM-COLOR: #009900; BORDER-TOP-COLOR: #009900; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #009900" borderColor=#009900 cellSpacing=0 cellPadding=1 border=1>
									<tbody>
										<tr class=DataGridEmphasisHeader>
												<td class=DataGridEmphasisHeader style="WIDTH: 30"></td>
												<td class=DataGridEmphasisHeader style="WIDTH: 30"></td>
												<td class=DataGridEmphasisHeadersmall style="WIDTH: 80" valign="top"><a class=BlindHeaderLink href="RetrieveUserTaskList.action?sort=0&asc=<s:if test="sort == 0 && asc">false</s:if><s:else>true</s:else>&user=<s:property value="user"/>">Query #</a></td>
												<td class=DataGridEmphasisHeadersmall style="WIDTH: 8%" valign="top"><a class=BlindHeaderLink href="RetrieveUserTaskList.action?sort=1&asc=<s:if test="sort == 1 && asc">false</s:if><s:else>true</s:else>&user=<s:property value="user"/>">Drug Name</a></td>
												<td class=DataGridEmphasisHeadersmall style="WIDTH: 50" valign="top">Query Type</td>
												<td class=DataGridEmphasisHeadersmall style="WIDTH: 50" valign="top">Req. Ctry</td>
												<td class=DataGridEmphasisHeadersmall style="WIDTH: 8%" valign="top">Date Initiated</td>
												<td class=DataGridEmphasisHeadersmall style="WIDTH: 8%" valign="top"><a class=BlindHeaderLink href="RetrieveUserTaskList.action?sort=5&asc=<s:if test="sort == 5 && asc">false</s:if><s:else>true</s:else>&user=<s:property value="user"/>">Response Date</a></td>
												<td class=DataGridEmphasisHeadersmall style="WIDTH: 8%" valign="top">Response Date (DMG)</td>
												<td class=DataGridEmphasisHeadersmall style="WIDTH: 70" valign="top">Activity</td>
												<td class=DataGridEmphasisHeadersmall style="WIDTH: 70" valign="top">Workflow Type</td>
												<td class=DataGridEmphasisHeader style="WIDTH: 30"></td>
												<td class=DataGridEmphasisHeader style="WIDTH: 30"></td>
												<td class=DataGridEmphasisHeader style="WIDTH: 30"></td>
												<td class=DataGridEmphasisHeader style="WIDTH: 30"></td>
											</tr>
											
                                    <s:iterator value="tasks" status="stat">
                                        <tr>
                                            <td style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; BORDER-BOTTOM: 1px solid"
                                                borderColor=#6699cc valign="middle" align="center">
                                                <s:if test="query.urgencyFlag=='Y'">
                                                    <img name="img01" src="img/urgent_2_off.gif"
                                                         alt="This query is flagged as URGENT" border="0"
                                                         title="This query is flagged as URGENT">
                                                </s:if>
                                            </td>
                                            <td style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; BORDER-BOTTOM: 1px solid" borderColor=#6699cc valign="middle" align="center">
                                                <s:if test="query.alertFlag=='Y'">
                                                    <img name="img02" src="img/important_off.gif" alt="New information added to this query" border="0" title="New information added to this query">
                                                </s:if>
                                            </td>
                                            <td class="Smalltext" borderColor=#6699cc valign="middle">
                                                <s:property value="query.queryNumber"/>
                                            </td>
                                            <td class="Smalltext" borderColor=#6699cc valign="middle">
                                                <s:property value="query.drugName"/>
                                            </td>
                                            <td style="Smalltext" borderColor=#6699cc>
                                                <acronym title="<s:property value="query.queryType.queryType"/>">
                                                    <s:property value="truncType"/>
                                                </acronym>
                                            </td>
                                            <td style="Smalltext" borderColor=#6699cc>
                                                <s:if test="!affiliate">
                                                    <s:property value="query.country.description"/>
                                                </s:if>
                                            </td>

                                            <td class="Smalltext" style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; BORDER-BOTTOM: 1px solid" borderColor=#6699cc>
                                                <s:date name="task.create" format="dd-MMM-yyyy"/>
                                            </td>

                                            <td class="Smalltext" style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; BORDER-BOTTOM: 1px solid"  borderColor=#6699cc>
                                                <s:property value="query.currentDueDateFormatted"/>
                                            </td>
                                            <td style="Smalltext" borderColor=#6699cc>
                                                <s:property value="query.dmgDueDateFormatted"/>
                                            </td>
                                            <td class="Smalltext" style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; BORDER-BOTTOM: 1px solid" borderColor=#6699cc>
                                                <s:property value="task.name"/>
                                            </td>
                                            <td class="Smalltext" style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; BORDER-BOTTOM: 1px solid" borderColor=#6699cc>
                                                <s:property value="query.workflowRouteType"/>
                                            </td>

                                            <td class=DataGridItem style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; BORDER-BOTTOM: 1px solid" borderColor=#6699cc valign="middle" align="center">
                                                <a href="RetrieveQueryNotes.action?queryId=<s:property value="query.querySeq" />" onmouseover="hilight('imgv<s:property value="#stat.count"/>')" onmouseout="lolight('imgv<s:property value="#stat.count"/>')">
                                                    <img name="imgv<s:property value="#stat.count"/>" src="img/notes_off.gif" alt="View Query Notes" title="View Query Notes" border="0"></a>
                                            </td>

                                            <td class=DataGridItem
                                                style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; BORDER-BOTTOM: 1px solid"
                                                borderColor=#6699cc valign="middle" align="center">
                                                <s:if test="displayManualReassign">
                                                    <a href="RetrieveAvailableResources.action?queryId=<s:property value="query.querySeq" />&taskId=<s:property value="task.id"/>&taskName=<s:property value="task.name"/>" onmouseover="hilight('imgm<s:property value="#stat.count"/>')" onmouseout="lolight('imgm<s:property value="#stat.count"/>')"><img name="imgm<s:property value="#stat.count"/>" src="img/sleep_off.gif" alt="Manual Reassignment" title="Manual Reassignment" border="0"></a>
                                                </s:if>
                                            </td>

                                            <TD class="Smalltext" borderColor=#009900 valign="middle" align="center">
												<s:if test="query.closeErrorStatus == null || query.closeErrorStatus == 0">
													<s:if test="displayContactRequester">
														<a href="mailto:<s:property value="query.requester.email"/>?subject=<s:property value="query.queryNumber"/>&cc=<s:property value="dsclEmail"/>" onmouseover="hilight('imge<s:property value="#stat.count"/>')" onmouseout="lolight('imge<s:property value="#stat.count"/>')">
															<img name="imge<s:property value="#stat.count"/>" src="img/mail_off.gif" alt="Send Question to Requester" title="Send Question to Requester" border="0">
														</a>
													</s:if>
												</s:if>
											</TD>

                                            <td class=DataGridItem
                                                style="BORDER-RIGHT: 1px solid; BORDER-TOP: 1px solid; BORDER-LEFT: 1px solid; BORDER-BOTTOM: 1px solid"
                                                borderColor=#6699cc valign="middle" align="center">
                                                <s:if test="displayDetails">
                                                    <a href="RetrieveQueryDetails.action?queryId=<s:property value="query.querySeq" />&taskId=<s:property value="task.id"/>" onmouseover="hilight('imgd<s:property value="#stat.count"/>')" onmouseout="lolight('imgd<s:property value="#stat.count"/>')"><img name="imgd<s:property value="#stat.count"/>" src="img/find_off.gif" alt="View Query Details" title="View Query Details" border="0"></a>
                                                </s:if>
                                            </td>
                                        </tr>
                                    </s:iterator>

									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=20></td>
		</tr>
  	</tbody>
</table>

<s:if test="showSummaryLink">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<td colspan="16">
				<b>Click <u><a href="RetrieveSummaryList.action" class="Anchor">here</a></u> to view query summary list...</b>
			</td>
		</tr>
	</table>
</s:if>

</body>