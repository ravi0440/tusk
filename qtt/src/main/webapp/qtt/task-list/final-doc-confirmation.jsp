<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>'Final Response' document Confirmation</title>
<link href="css/dialog.css" type=text/css rel=stylesheet>
<script language="javascript" src="js/global_script.js"></script>
<script language="javascript">
	function doAttachment() {		
		var args = "draftDoc";
	    window.returnValue = args;
	    window.close();
	}
</script>
</head>
<body>
<div class="DialogMessage">Please attach 'Final Response' type document</div>
<table>
	<tr>
		<td width="28%">&nbsp;</td>
		<td width="22%"><input type="button" name="attach"
			value="Attach Document" class="button" onclick="doAttachment();"
			onmouseover="hover(this,'buttonhover')"
			onmouseout="hover(this,'button')"></td>
		<td>&nbsp;</td>
		<td width="22%"><input type="button" name="cancel" value="Cancel"
			class="button" onclick="window.close();"
			onmouseover="hover(this,'buttonhover')"
			onmouseout="hover(this,'button')"></td>
		<td width="28%">&nbsp;</td>
	</tr>
</table>
</body>
</html>
