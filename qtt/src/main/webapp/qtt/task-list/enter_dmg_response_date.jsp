<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<body>

<s:form action="UpdateDMGResponseDate" theme="simple">
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
	<tbody>
		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%"><span>
								<p>Query Information</p></span></div></td>
						</tr>
                            <s:if test="hasActionErrors() || hasFieldErrors()">
                                <%@ include file="../global/validation_error.jsp" %>
                            </s:if>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><span>
								<p>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Query No. <s:property value="query.queryNumber" /></font></td>
										</tr>
										<tr>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="30%">
												<b>Drug Name</b>
											</td>
											<td width="70%" colspan="3">
												<s:property value="query.drugName" />
											</td>
										</tr>

										<tr>
											<td>
												<b>Query Type</b>
											</td>
											<td colspan="3">
												<s:property value="query.queryType.queryType"/>
											</td>
										</tr>
										<tr>
											<td>
												<b>Requester</b>
											</td>
											<td colspan="3">
												<s:property value="query.requester.name"/>
											</td>
										</tr>
										<tr>
											<td height="20" colspan="4"><img src="img/spacer.gif" height="20"></td>
										</tr>

                                    	<tr>
											<td width="30%">
                                                Current DMG Response Date
											</td>
											<td colspan="3">
												<s:property value="query.dmgDueDateFormatted"/>
											</td>
										</tr>

										<tr>
											<td>
												<b>New DMG Response Date</b>
											</td>
											<td colspan="3">
												<s:textfield name="date" size="13" maxlength="11"/>&nbsp;&nbsp;<s:text name="struts.date.format"/>
											</td>
										</tr>

										<tr>
											<td>
												Explanation for the due date change
											</td>
											<td colspan="3">
												<s:textarea name="explanation" cols="75" rows="4" onblur="textCounter(this,1000);" onkeypress="textCounter(this,1000);"/>
											</td>
										</tr>
									</table>
								</p>
                                </span></div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=20></td>
		</tr>
  	</tbody>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td align="left" width="100%">
			<s:submit value="Submit" />
		</td>
		    <td height="20" colspan="5">
                <img src="img/spacer.gif" height="20">
                <s:hidden name="queryId" value="%{query.querySeq}"/>
                <s:hidden name="taskId" value="%{#parameters.taskId}"/>
            </td>
		</tr>
</table>

</td>
</tr>
</tbody>
</table>
</s:form>

</body>