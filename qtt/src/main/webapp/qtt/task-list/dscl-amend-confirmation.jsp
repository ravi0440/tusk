<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE>Query Amend Confirmation</TITLE>
<LINK href="/qtt/css/dialog.css" type=text/css rel=stylesheet>
<SCRIPT language="javascript" src="/qtt/js/global_script.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">

	function doDsclAmendToQRF() {		
			var args = "ProcessQRFAction.action?activity=amd&next=qrf&id=" + <s:property value="queryId"/>;
		    window.returnValue = args;
		    window.close();
	}
	function doDsclAmendToEmail() {		
			var args = "ProcessQRFAction.action?activity=amd&next=email&id=" + <s:property value="queryId"/>;
		    window.returnValue = args;
		    window.close();
	}
</script>

</HEAD>
<BODY>

<div class="DialogMessage">This query is currently being processed. This option will take the query out from the task list and the query has to be accepted and assigned again. Do you want to continue?</div>
<table>
	<tr>
		<td width="12%">&nbsp;</td>
		<td width="25%"><input type="button" name="qrf" value="Yes, and go to QRF"
			class="button" onclick="doDsclAmendToQRF();"
			onmouseover="hover(this,'buttonhover')"
			onmouseout="hover(this,'button')"></td>
		<td>&nbsp;</td>
		<td width="25%"><input type="button" name="email" value="Yes, and send Email"
			class="button" onclick="doDsclAmendToEmail();"
			onmouseover="hover(this,'buttonhover')"
			onmouseout="hover(this,'button')"></td>
		<td>&nbsp;</td>
		<td width="25%"><input type="button" name="cancel" value="Cancel"
			class="button" onclick="window.close();"
			onmouseover="hover(this,'buttonhover')"
			onmouseout="hover(this,'button')"></td>
		<td width="12%">&nbsp;</td>
	</tr>
</table>
</BODY>
</HTML>
