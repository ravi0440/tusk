<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE>Query Delete Confirmation</TITLE>
<LINK href="css/dialog.css" type=text/css rel=stylesheet>
<SCRIPT language="javascript" src="js/global_script.js"></script>
<script language="javascript">

	function doDsclDelete() {		
			var args = "ProcessQRFAction.action?activity=del&id=" + <s:property value="queryId"/>;
		    window.returnValue = args;
		    window.close();
	}
	function doDsclDeleteToEmail() {		
			var args = "ProcessQRFAction.action?activity=del&next=email&id=" + <s:property value="queryId"/>;
		    window.returnValue = args;
		    window.close();
	}
</script>

</HEAD>
<BODY>

<div class="DialogMessage">This query is currently being processed. This option will take the query out from the task list and delete without saving it. Do you want to continue?</div>
<table>
	<tr>
		<td width="25%">&nbsp;</td>
		<td width="25%"><input type="button" name="qrf" value="Yes"
			class="button" onclick="doDsclDelete();"
			onmouseover="hover(this,'buttonhover')"
			onmouseout="hover(this,'button')"></td>
		<td>&nbsp;</td>
		<td width="25%"><input type="button" name="cancel" value="Cancel"
			class="button" onclick="window.close();"
			onmouseover="hover(this,'buttonhover')"
			onmouseout="hover(this,'button')"></td>
		<td width="25%">&nbsp;</td>
	</tr>
</table>
</BODY>
</HTML>
