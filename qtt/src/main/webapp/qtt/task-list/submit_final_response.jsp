<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<script language="javascript">
	function confirmationDialog(dialogType) {
		var path="ConfirmationDialog.action?option=" + dialogType + "&taskId=<s:property value="taskId"/>" + "&queryId=<s:property value="query.querySeq"/>";
		var args;
		var sFeatures = getModalValues(230, 630);
	    var sReturn = window.showModalDialog(path, args, sFeatures);
	    if(sReturn == 'submit') {
			//disableButtonsSubmit(document.forms[0]);					
			document.forms[0].target='_self';			
			document.forms[0].submit();
	    } else if(sReturn == 'draftDoc') {	
			document.forms[0].draft.value = sReturn;							    
			document.forms[0].target='_self';			
			document.forms[0].submit();
	    } else if(sReturn != null) {	
			window.parent.location=sReturn;     	
		}
	}
	function saveDraft() {
			document.forms[0].draft.value = 'draft';						
			document.forms[0].target='_self';			
			document.forms[0].submit();
	}
	function doSummarySubmit() {
		if (confirm("You are Submitting Query Summary to DSCL. Do you want to continue?")) {			
			disableButtonsSubmit(document.forms[0]);	
			document.forms[0].target='_self';			
			document.forms[0].submit();
		}
	}
</script>

<s:form action="UpdateQuerySummary" method="POST" onsubmit="disableFormSubmit(this);" theme="simple">
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
	<tbody>
		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=middle align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%">
									<p>Query Information</p>
								</div>
							</td>
						</tr>
						<s:if test="hasActionErrors() || hasFieldErrors()">
							<%@ include file="../global/validation_error.jsp" %>
						</s:if>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Query No. <s:property value="query.queryNumber"/></font></td>
										</tr>
										<tr>
											<td height="20" colspan="3"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td width="30%">
												<b>Drug Name</b>
											</td>
											<td width="70%" colspan="3">
												<s:property value="query.drugName"/>
											</td>
										</tr>
										<tr>
											<td>
												<b>Query Type</b>
											</td>
											<td colspan="3">
												<s:property value="query.queryType.queryType"/>
											</td>
										</tr>
										<tr>
											<td>
												<b>Requester</b>
											</td>
											<td colspan="3">
												<s:property value="query.requester.name"/>
											</td>
										</tr>
										<tr>
											<td>
												<b>Current Activity</b>
											</td>
											<td colspan="3">
												<s:property value="taskName"/>
											</td>
										</tr>
										<tr>
											<td height="20" colspan="4"><img src="img/spacer.gif" height="20"></td>
										</tr>
										<tr>
											<td colspan="4" width="100%">
												At the point of forwarding the response back to the DSCL, the following information must be entered to allow a definitive response to be formulated for this query:
											</td>
										</tr>
							
										<tr>
											<td width="30%">
												Labelling Document Used
											</td>
											<td colspan="3">
												<s:select list="labellingDocs" name="labellingDoc" listKey="ldocTypeId" listValue="ldocType" value="%{labellingDoc}" emptyOption="true" theme="simple"/>
												&nbsp;(if 'Local Label', please state country in summary text)
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">
												Future search terms																										
											</td>
											<td colspan="3">
												<s:textarea name="summary" rows="11" cols="75" onblur="textCounter(this,1000);" onkeypress="textCounter(this,1000);" theme="simple"/>
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=20></td>
		</tr>
  	</tbody>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td align="left" width="10%">
		
<%--			<s:if test="attachmentNoteWarning"> --%>
<%--				<input type="button" name="send" value="Submit" onclick="confirmationDialog('dTm');"> --%>
<%--			</s:if> --%>
<%--			<s:if test="!attachmentNoteWarning"> --%>

			<s:if test="finalDocWarning">
				<input type="button" name="send" value="Submit" onclick="confirmationDialog('fDoc');">
			</s:if>
			<s:if test="!finalDocWarning">
				<input type="button" name="send" value="Submit" onclick="doSummarySubmit();">	
			</s:if>
			
<%--			</s:if> --%>

		</td>
		<td align="left" width="90%">
			<input type="button" name="save" value="Save Draft" onclick="saveDraft();">
		</td>		
	</tr>
	<tr>
		<td height="20" colspan="5"><img src="img/spacer.gif" height="20"></td>
	</tr>
</table>

<s:hidden name="draft"/>

</s:form>

