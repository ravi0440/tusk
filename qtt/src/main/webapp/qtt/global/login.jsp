 <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<title>User Login</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel=stylesheet type="text/css" href="css/login.css">
    <script language="javascript" src="js/validateForm.js"></script>
	<script language="javascript" src="js/formUtils.js"></script>	
	<script language="JavaScript">
		function checkForm(form){
			var formRequiredFields = new Array('j_username','j_password');	
			var requiredFieldMessages = new Array('User name must be entered','Password must be entered');
			return areRequiredFieldsPresent(form,formRequiredFields,requiredFieldMessages);			
		}

		function formClear(form){
			form.j_password.value="";
		}
	</script>
</head>

<body onload="formClear(document.forms[0]);setFormFocus();javascript:top.window.resizeTo(1024,768);" class="bg-login">

<form id="login" name="login" action="${pageContext.request.contextPath}/SecurityCheck.action" method="post" onsubmit="return checkForm(document.forms[0]);">
	<table class="loginTable" >
		<tr> 
			<td width="1" rowspan="9"><img src="img/spacer.gif" width="5" height="5"></td>
			<td colspan="2">			
				<s:if test="%{authenticationFailed}">
					User not authenticated 
				</s:if>
			</td>
			<td width="1" rowspan="9"><img src="img/spacer.gif" width="5" height="5"></td>
		</tr>
		<tr> 
			<td colspan="2"><img src="img/spacer.gif" width="1" height="3"></td>
		</tr>
		<tr valign="bottom"> 
			<td width="90" class="tr4"><strong>User name:</strong>
		  	<td width="190" class="tr4">
		  		<s:textfield name="j_username" cssClass="logintextfield" theme="simple" value="%{#session.get('SPRING_SECURITY_LAST_USERNAME')}" />
		  	</td>
		</tr>
		<tr> 
			<td colspan="2"><img src="img/spacer.gif" width="1" height="3"></td>
		</tr>
		<tr> 
			<td width="90" class="tr4"><strong>Password:</strong></td>
			<td width="190" class="tr4">
				<s:password name="j_password" theme="simple" cssClass="logintextfield"/>
			</td>
	  	</tr>
	  	<tr> 
			<td colspan="2" valign="bottom"><img src="img/spacer.gif" width="1" height="3"></td>
	  	</tr>
	  	<tr> 
			<td class="tr4"><strong>Domain:</strong></td>
			<td class="tr4">
				<s:select name="j_domain" list="{'EMEA','ASIA','NALA','EXBP'}" theme="simple" cssClass="loginselecttfield"/>
		  	</td>
	  	</tr>
	  	<tr> 
			<td colspan="2"><img src="img/spacer.gif" width="1" height="3"></td>
	  	</tr>
	  	<tr valign="bottom"> 
			<td height="22" colspan="2" align="center"><input type="submit" value="Login" class="button" /></td>
	  	</tr>
	</table>
</form>

</body>

</html>