<%@ page contentType="text/html; charset=UTF-8" %>
<html>

<body onLoad="preload()">

<!-- START MAIN BODY SPACE ALL PAGE CONTENTS GO HERE -->
<small>

	<b>Version 4.11.0.0   </b><br/>
	<ul>
		<li>DSCL-8146 - QTT: 'Verify Query Results by Owner' task must be mandatory - gorskip1</li>
		<li>DSCL-8147 - QTT: 'Verify Query Results by Owner' task must be added as mandatory in business process flow - gorskip1</li>
		<li>DSCL-7285 - Query label sensitive to regex special characters - gorskip1</li>
	</ul>

	<b>Version 4.10.0.0   </b><br/>
	<ul>
		<li>DSCL-7931 - QTT: Raise attachment size limit in app - gorskip1</li>
		<li>DSCL-8148 - QTT: Improve interface for query creation - gorskip1</li>
		<li>DSCL-8149 - QTT: Improve interface for query deletion - gorskip1</li>
		<li>DSCL-8150 - QTT: Remove 'Save Draft' button if unneeded - gorskip1</li>
		<li>DSCL-8158 - QTT: Matching queries to users by email - gorskip1</li>
		<li>DSCL-8299 - QTT: Request for removal of Review Functionality [PM00017760 IM15101633] - gorskip1</li>
	</ul>

	<b>Version 4.9.1.0   </b><br/>
	<ul>
		<li>DSCL-7989-QTT: Fix logging mechanism to QRF for LDAP users (with no QTT roles)</li>
	</ul>

    <b>Version 4.9.0.0   </b><br/>
    <ul>
        <li>DSCL-7311-Adding Safety Security Module into QTT - torenck</li>
        <li>DSCL-7374-QTT: Flashback retraction - torenck</li>
        <li>DSCL-7533-QTT : Databases migration to Oracle12C - torenck</li>
    </ul>

    <b>Version 4.8.0.0   </b><br/>
<ul>
    <li>DSCL-5584-QTT Report in xls (Excel) format should display all data in column related to the headers - torenck</li>
    <!-- DSCL-6757 revoked
    <li>DSCL-6757-FU queries that are rejected should not delete the initial query - torenck</li>
    -->
    <li>DSCL-6826-User cannot see attachments in preview screen - torenck</li>
	<li>DSCL-6390 &NBSP tags should not appear in QRF view when changing text to uppercase - farynam</li>
	<li>DSCL-6025 Query state when searching should be the same as on the home page (now displays random data) - farynam</li>
	<li>DSCL-6024 QTT: Minor styling problems - farynam</li>
</ul>

<b>Version 4.7.1.0   </b><br/>
<ul>
    <li>DSCL-6032-The attachment is not found when it is Re-Opened - torenck</li>
    <li>DSCL-5827-All attachments in query disappeared - torenck</li>
    <li>DSCL-5848-List with attachments should be consistent - torenck</li>
    <li>DSCL-6546 SOP reference to be updated in QRF - farynam</li>
    <li>DSCL-6304 longer attachments names - farynam</li>
</ul>

<b>Version 4.7.0.0   </b><br/>
<ul>
	<li>DSCL-5831-QTT Audit Trail - cimerl, palaczk</li>

</ul>

<b>Version 4.6.0.0   </b><br/>
<ul>
<li>DSCL-3905-Add formatting capabilities and field length validation to the Query Request Form- cimerl</li>
<li>DSCL-3925-Enhance attachments capability - zawadzl1</li>
<li>DSCL-4095-Add linkage between workflows and follow-up queries - palaczk</li>
<li>DSCL-4603-Values are inconsistent for non-entered fields - palaczk</li>
<li>DSCL-4604-Wrong error message when changing response date - zawadzl1</li>
<li>DSCL-4622-Email alert is not received by the alert owner - zawadzl1</li>
<li>DSCL-4959-User profile values are not getting pre-populated in Query Request Form - cimerl</li>
<li>DSCL-5065-Enhance copy&paste functionality - cimerl</li>
<li>DSCL-5066-Remove fields 'Expiry date' and 'Final response visible to affiliate' - cimerl</li>
<li>DSCL-5067-Enhance query searching capabilities - cimerl</li>
<li>DSCL-5077-Attachments with the same name - cimerl</li>
<li>DSCL-5078-Enhance query handling - cimerl</li>
<li>DSCL-5278-Change query notes - cimerl</li>
<li>DSCL-2198-Query Request Form showing wrong country - cimerl</li>
<li>DSCL-3461-Enhance permissions to access the About option in QRF - cimerl</li>
<li>DSCL-4827-Update users on Manual Reassignment list in DMGCO role - palaczk</li>
<li>DSCL-4828-Remove dates on About page - cimerl</li>
<li>DSCL-4988-Remove fax number from Query Review screen - cimerl</li>
<li>DSCL-5075-Standardize date fields - zawadzl1</li>
<li>DSCL-5076-Resize button/text regarding Safety Science - zawadzl1</li>

</ul>

<b>Version 4.5.2.4   </b><br/>
<ul>
<li>DSCL-5360-Implement emails to EU QPPV in query workflow - cimerl</li>
<li>DSCL-5371-Add EU QPPV column to reports - cimerl</li>

</ul>

<b>Version 4.5.0.0   </b><br/>
<ul>
<li>DSCL-4094-Original query gets deleted when removing re-opened draft - cimerl</li>
<li>DSCL-4346-Attachments from follow up query not visible at closed query details page - cimerl</li>
<li>DSCL-3141-Wrong list of deployed DSCL on About screen in QTT - cimerl</li>
<li>DSCL-3967-Wrong version numbers in About page - cimerl</li>
<li>DSCL-4002-Add guidance document to QRF - cimerl</li>
<li>DSCL-4070-Remove option from "Type of Search" in QRF- cimerl</li>
</ul>

<b>Version 4.4.0.0  </b><br/>
<ul>
<li>DSCL-3350-QTT IE10 compatibility fixes - cimerl</li>
</ul>

<b>Version 4.3.4.0  - deployed in dev, test</b><br/>
<ul>
<li>IM06106621 - DSCL-2913-Need to swith from Security Module in WL8.1 to instance in Jboss 5.1 - cimerl</li>
<li>IM06106638 - DSCL-2097-'Session Expired' error message when user is trying to submit a Data/Literature Search Query - cimerl</li>
<li>IM06106648 - DSCL-2715-Include link to the newly created QTT User Guide - cimerl</li>
<li>IM06106716 - DSCL-2553-QTT: Provisional Query QTTXXXX cannot be rejected - savovp</li>
</ul>

<b>Version 4.3.3.0  - deployed in dev, test</b><br/>
<ul>
<li>IM05969026-DSCL-2818-QTT: No notification is sent when a query is re-opened<i> - cimerl</i></li>
<li>IM05969080-DSCL-2196-QTT: Report headers need to be exported to Excel as well<i> - savovp</i></li>
<li>IM05969090-DSCL-2836-QTT: Queries showed multiple times in the QTT Task list<i> - cimerl</i></li>
</ul>

<b>Version 4.3.2.0  - deployed in dev, test</b><br/>
<ul>
<li>C00124477 Defect fixes<i> - savovp</i></li>
</ul>

<b>Version 4.3.1.0  - deployed in production</b><br/>
<ul>
<li>00092677 Defect fixes<i> - savovp</i></li>
</ul>

<b>Version 4.3.0.0  - deployed in dev, test</b><br/>
<ul>
<li>00082699 Defect fixes, changes<i> - savovp</i></li>
</ul>

<b>Version 4.1.0  - deployed on dev</b><br/>
<ul>
<li>4052192 Defect fixes, changes<i> - borowieb, savovp</i></li>
</ul>

<b>Version 4.0.17  - deployed on dev, test, uat</b><br/>
<ul>
<li>4052192 Replacement of WebLogic and Workpoint (http://pdapps.gene.com/jira/browse/DSCL-1052)<i> - borowieb, savovp</i></li>
</ul>

<b>Version 3.6.0  - deployed on production</b><br/>
<ul>
<li>498273 Remove ADVENT dependency on QTT<i> - borowieb</i></li>
</ul>

<b>Version 3.5.1  - deployed on production</b><br/>
<ul>
<li>334457 Change Access to Query List<i> - hudowskr</i></li>
</ul>


<b>Version 3.5.0  - deployed on production</b><br/>
<ul>
<li>328098 Package to produce special reports<i> - hudowskr</i></li>
</ul>


<b>Version 3.4.1  - deployed on production</b><br/>
<ul>
<li>302987 An email is sent to DSCL user when query is reopend by non DSCL user<i> - hudowskr</i></li>
<li>302984 Split qtt and adst(psst)<i> - hudowskr</i></li>
</ul>

<b>Version 3.2.4  - deployed on production</b><br/>
<ul>
	<li>279641 Fix to the problem with rejecting queries<i> - gorkam</i></li>
	<li>270008 Fix the delete button<i> - borowieb</i></li>
	<li>276875 Query closing mechanism improvements<i> - borowieb</i></li>
	<li>276875 Query closing mechanism improvements<i> - borowieb</i></li>
</ul>


<b>Version 3.2.3 </b><br/>
<ul>
	<li>273944 Expiry Mail Limit for B Queries<i> - borowieb</i></li>
	<li>273966 Fix to the bug when attaching the documents<i> - borowieb</i></li>
</ul>

<b>Version 3.2.2 </b><br/>
<ul>
	<li>Bugfix "About" link does not work<i> - borowieb</i></li>
</ul>

<b>Version 3.2.1 </b><br/>
<ul>
	<li>Add missing changes to changelog<i> - borowieb</i></li>
</ul>

<b>Version 3.2.0  - deployed on production</b><br/>
<ul>
	<li>177917  PERFORMANCE METRICS FOR QTT (QM ID 34 74)<i> - gorkam</i></li>
	<li>177942  CHANGE EXPIRY DATA AND AFFILIATE CHECK BOX IN QUERY REPOSITORY<i> - gorkam</i></li>
	<li>177955  Fix in the Amend Query Function<i> - gorkam</i></li>
	<li>269973  Read access for affiliates<i> - gorkam</i></li>
	<li>269982  CRF QTT Query List<i> - borowieb</i></li>
	<li>Fix to delete button<i> - gorkam</i></li>
</ul>

<b>Version 3.1.4 </b><br/>
<ul>
	<li>Add column Query Label<i> - borowieb</i></li>
	<li>Add changelog <i> - borowieb</i></li>
</ul>
</small>
<!-- END MAIN BODY SPACE -->

</body>
</html>