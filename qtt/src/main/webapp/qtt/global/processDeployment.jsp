<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<s:actionerror />

<s:form action="DeployProcessDefinition" method="post" enctype="multipart/form-data">
	<s:file name="processDefinitionParFile" label="PAR file"/>
    <s:submit value="Deploy"/>
</s:form>

<s:form action="MigrateToJbpm" method="post">
	<s:submit value="Migrate"/>
</s:form>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
        <td colspan="16">
            <b>Click <u><a href="RetrieveQueryTaskList.action" class="Anchor">here</a></u> to return to task list...</b>
        </td>
    </tr>
</table>