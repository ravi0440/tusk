<%@ page language="java" import="com.roche.dss.qtt.query.web.menu.MenuItem" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<tiles:importAttribute name="menuItems" />

<table cellSpacing=0 cellPadding=0 width="100%" border=0>
	<tr>
		<td width="100%">
			<table cellSpacing=0 cellPadding=0 width="100%" border=0>
				<tr>
					<td class=Impersonation colSpan=5><img src="img/gradient.jpg"></td>
				</tr>
				<tr>
					<td valign="top" bgcolor="#003366">
						<table cellSpacing=0 cellPadding=0 border="0">
							<tr>
								<td height="1" colspan="3"><img src="img/spacer.gif" border="0" width="580" height="1"></td>
							</tr>
							<tr>
								<td class=Impersonation><img src="img/spacer.gif" border="0"></td>
								<td align="left" class=TopNavLink bgcolor="#003366" width="130">
									<s:date name="todaysDate" format="dd-MMM-yyyy" />
								</td>
								<td class=TopNavigationBar align="left" width="640">
									<c:set var="homeLabelValue">
    									<%=MenuItem.HOME.getValue()%>
									</c:set>
									<c:forEach var="item" items="${menuItems}" varStatus="status">
										<c:choose>
  											<c:when test="${status.first && item.value eq homeLabelValue}">
    											<b><a class=TopNavLink href="${item.link}">${item.value}</a></b>
  											</c:when>
											<c:otherwise>
    											<a class=TopNavLink href="${item.link}">${item.value}</a>
    										</c:otherwise>
 										</c:choose>
										${not status.last ? ' |' : ''}
									</c:forEach>
								</td>
							</tr>
							<tr>
								<td height="1" colspan="3"><img src="img/spacer.gif" border="0" height="1"></td>
							</tr>
						</table>
					</td>	
					<td align="right" class=TopNavLink bgcolor="#003366" height="36">
						<s:property	value="loggedUserDisplayName" />
					</td>
					<td width="10" class=Impersonation>
						<img src="img/spacer.gif" border="0" width="10">
					</td>
				</tr>
				<tr>
					<td class=Impersonation colSpan=5><img src="img/gradient.jpg"></td>
				</tr>
			</table>
		</td>
		<td bgcolor="#003366" valign="middle">
			<table cellSpacing=0 cellPadding=0 border=0>
				<tr>
					<td><img src="img/user.gif" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
