<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<tr>
	<td class=ErrorHeader style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid" vAlign=middle align=left width="100%">
		 <s:property value="getText('errors.header')"/>
	</td>
</tr>
<tr>
	<td class=ErrorText style="BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid" vAlign=middle align=left width="100%">
		<ul>
     		<s:iterator id="actionError" value="actionErrors">
     	   		<li>
        	   		<s:property value="#actionError" />
        	   	</li>
			</s:iterator>
            <s:iterator id="fieldErrorName" value="fieldErrors.keySet()">
           		<s:iterator id="fieldErrorValue" value="fieldErrors[#fieldErrorName]">
                	<li>
                		<s:property value="#fieldErrorValue" />
                	</li>
                </s:iterator>
            </s:iterator>
		</ul>
	</td>
</tr>
<tr>
	<td class=ErrorHeader style="BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" vAlign=middle align=left width="100%">
		<s:property value="getText('errors.footer')"/>
	</td>
</tr>
