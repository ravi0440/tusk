<%@ page language="java" %>
<html>

<head>
<title>Access denied</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/login.css" rel="stylesheet" type="text/css">
</head>

<body class="bg" leftmargin="0" topmargin="0">
<table width="100%" height="100%" border="0">
	<tr> 
    	<td width="100%" height="100%" align="center" valign="middle" class="tsubhead"><font size="+2">Access denied</font><br>If you think you should have access to this area, please contact your administrator</td>
  	</tr>
</table>
</body>

</html>
