<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><tiles:insertAttribute name="title"/></title>
    <link rel=stylesheet type="text/css" href="css/roche.css">
    <link rel=stylesheet type="text/css" href="css/button.css">
    <script language="javascript" src="js/image_script.js"></script>
    <script language="javascript" src="js/global_script.js"></script>
    <script language="javascript" src="js/quick_list_script.js"></script>
    <script language="javascript" src="js/search_script.js"></script>
    <script language="javascript" src="js/tinymce/tinymce.min.js"></script>
	<script src="js/jquery.1.10.2.min.js"></script>
    <script type="text/javascript">

    function validateFileSize(component){
        if(component.files[0]!=undefined) {
            var fileSize = component.files[0].size;
            var freeDiskSpace = $('#freeDiskSpace').val();
            if(freeDiskSpace - fileSize < 100000000) {
                alert("Currently there is not enough server disk space to upload this file. Please try again later.");
                component.value = null;
			}
        }
    }

    function setupTinyMC(selector, maxlength){
    	tinymce.init({ selector:selector,    		
			 setup: function (ed) {
				 	ed.lock=false;
	                ed.on('keyup', function (evt) { 
		                if(ed.lock){
	                    var count = tinymce.activeEditor.getContent().length;
	                   if(count > maxlength ){
	                	   ed.setContent(ed.latest_content);
	                       ed.selection.moveToBookmark(ed.bookmark);
	                       ed.execCommand('mceCleanup');
		                    alert("Maximum number of characters ("+maxlength+") exceeded");
		                   }
	                   ed.lock=false;
		                }
	                });

	                ed.on('keydown', function (evt) { 
		                if(!ed.lock){
			                ed.lock=true;
			                ed.bookmark = ed.selection.getBookmark(2,true);
		                     ed.latest_content = ed.getContent({format:'raw'});
			            }
	                	 
	                });
	                
	            },
	            width: 850,
    	element_format : "html",
    	fix_list_elements : false,
    	mode : "textareas",
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : '',
        invalid_elements: "em",
        plugins: [
                  'advlist autolink lists link image charmap print preview anchor',
                  'searchreplace visualblocks code fullscreen',
                  'insertdatetime media table contextmenu paste code'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
      
        
    	    
			  });
        }

    function destroyTinyMC(selector){
    	tinymce.remove(selector);
        }
    
    </script>
</head>

<body background=<tiles:insertAttribute name="bodyBackground"/>>

<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
    <tbody>
	    <tr>
		    <td vAlign=top width="100%" height=100 background="img/header_gradient.jpg">
		    	<tiles:insertAttribute name="header"/>
		        <tiles:insertAttribute name="menu"/>
		  	</td>
	    </tr>
	    <tr>
	    	<td>
				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
	        		<tbody>
		        		<tr>
		          			<td class=LeftPane vAlign=top align=left>
		          				<span>
		          					<span>
		          						<img class="imgZeroPadding" src="img/left_query_tracking_tool.gif">
		          					</span>
		          				</span>
		          			</td>
		               		<td class=RightPaneHeader vAlign=top align=left>
		                    	<tiles:insertAttribute name="siteTitle"/>
		                        <table cellSpacing=0 cellPadding=0 width="100%" border=0>
									<tbody>
			              				<tr>
			                				<td width="100%" background="img/line.gif"><img height=1 src="img/line.gif" border="0"></td>
										</tr>
			              				<tr>
			                				<td><img height="16" src="img/spacer.gif" border="0"></td>
										</tr>
									</tbody>
								</table>
                                   <tiles:insertAttribute name="pre"/>
                                   <tiles:insertAttribute name="body"/>
                                   <tiles:insertAttribute name="post"/>
		                    </td>
		                </tr>
	                </tbody>
	            </table>
	            <br>
	        </td>
	    </tr>
	</tbody>
</table>

</body>

</html>