
<%
com.roche.dss.qtt.utility.NavigationHelper.setupNavigation(request);
%>

<logic:notPresent name="help.link">
	<request:setAttribute name="help.link">help.page.default</request:setAttribute>
</logic:notPresent>

<bean:define id="helpURL" name="help.link" type="java.lang.String" />
<bean:define id="navBar" name="qtt.navbar"
	type="com.roche.dss.qtt.query.web.displaybean.NavBarDisplayBean" />

<jsp:useBean id="today" class="java.util.Date" scope="session" />

<table cellSpacing=0 cellPadding=0 border="0">
	<tr>
		<td height="1" colspan="3"><img src="img/spacer.gif"
			border="0" width="580" height="1"></TD>
	</tr>
	<tr>
		<td class=Impersonation><img src="img/spacer.gif" border="0"></td>
		<td align="left" class=TopNavLink bgcolor="#003366" width="130">
		<bean:write name="today" format="dd-MMM-yyyy" /></td>
		<td class=TopNavigationBar align="left" width="640">
			<logic:equal name="navBar" property="displayTaskList" value="true">
				<b><a class=TopNavLink href="RetrieveQueryTaskList.do" onClick="disableAllLinks()">Home</a></b> |
			</logic:equal> 

			<logic:equal name="navBar" property="displayQRF" value="true">
				<a class=TopNavLink href="QrfAction.do" onClick="disableAllLinks()">QRF</a> |
			</logic:equal>
 
			<logic:equal name="navBar" property="displayQueryList" value="true">
				<a class=TopNavLink href="QueryListAction.do" onClick="disableAllLinks()">Query List</a> |
			</logic:equal> 

			<logic:equal name="navBar" property="displayQueryListUsers" value="true">
				<a class=TopNavLink href="QueryListUsersAction.do" onClick="disableAllLinks()">Query List Users</a> |
			</logic:equal>
			
			<logic:equal name="navBar" property="displayInitiate" value="true">
				<a class=TopNavLink href="InitiateList.do" onClick="disableAllLinks()">Initiate</a> |
			</logic:equal> 

			<logic:equal name="navBar" property="displaySearch" value="true">
				<a class=TopNavLink href="RepositorySearch.do" onClick="disableAllLinks()">Search</a> |
			</logic:equal>
 
			<logic:equal name="navBar" property="displayQuickSearch" value="true">
				<a class=TopNavLink href="QuickSearch.do" onClick="disableAllLinks()">Quick	Search</a> |
			</logic:equal> 

			<logic:equal name="navBar" property="displayPerfMetrics" value="true">
				<a class=TopNavLink href="TrackingReportsMenu.do" onClick="disableAllLinks()">Reports</a> |
			</logic:equal> 

			<a class=TopNavLink 
			   href="<%="http://" + request.getServerName() + ":" + request.getServerPort() + "/qmguide/" %>"
			   target="Help">Help
		    </a>| 
            <a class=TopNavLink href="AboutInformation.do" onClick="disableAllLinks()">About</a>

		</td>
	</tr>
	<tr>
		<td height="1" colspan="3">
			<img src="img/spacer.gif" border="0" height="1">
        </td>
	</tr>
</table>
