<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>

<body>
<s:form action="QuickSearchResults" method="get">
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<tbody>
  		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<s:if test="hasActionErrors() || hasFieldErrors()">
			              	<%@ include file="../global/validation_error.jsp" %>
						</s:if>

						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%"><span>
								<p>Quick Search</p></span></div></td>
						</tr>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><span>
								<p>

									<table cellspacing="0" cellpadding="1" border="0" width="100%">

										<tr>
											<td>Search Term:</td>
											<td>
												<s:textfield  name="searchParams.term" maxlength="100" cssClass="SearchWidget" theme="simple"/>
											</td>
										</tr>
										
                          				<tr>
                            				<td>Search scope:</td>
                            				<td>
                            					<s:select name="searchParams.searchScope" list="scopes" listKey="key" listValue="value" cssClass="SearchWidget" theme="simple"/>
                            				</td>
                            			</tr>	
										
										<tr>
											<td rowspan="2" valign="top">Time Period Addressed within the Response:</td>
											<td colspan="4">
												<table cellspacing="4" cellpadding="0" border="0">
													<tr>
														<td width="8%"><b>From</b></td>
														<td><img src="img/spacer.gif" width="20"></td>
														<td>
															<s:textfield  name="searchParams.periodDateFrom" size="13" maxlength="11" theme="simple"/>
														</td>
														<td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="70%" colspan="4">
												<table cellspacing="4" cellpadding="0" border="0">
													<tr>
														<td width="8%"><b>To</b></td>
														<td><img src="img/spacer.gif" width="20"></td>
														<td>
															<s:textfield name="searchParams.periodDateTo" size="13" maxlength="11" theme="simple"/>
														</td>
														<td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td rowspan="2" valign="top">Date of Query Request:</td>
											<td colspan="4">
												<table cellspacing="4" cellpadding="0" border="0">
													<tr>
														<td width="8%"><b>From</b></td>
														<td><img src="img/spacer.gif" width="20"></td>
														<td>
															<s:textfield name="searchParams.qryResponseDateFrom" size="13" maxlength="11" theme="simple"/>
														</td>
														<td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="70%" colspan="4">
												<table cellspacing="4" cellpadding="0" border="0">
													<tr>
														<td width="8%"><b>To</b></td>
														<td><img src="img/spacer.gif" width="20"></td>
														<td>
															<s:textfield  name="searchParams.qryResponseDateTo" size="13" maxlength="11" theme="simple"/>
														</td>
														<td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
													</tr>
												</table>
											</td>
										</tr>
										
									</table>



								</p></span></div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
		<tr>
			<td width="100%" height=9></td>
		</tr>
	</tbody>
</table>



<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td align="left" width="100%">
			<s:submit value="Search" align="left"/>
		</td>
		<td width="20"><img src="img/spacer.gif" width="20" border="0"></td>
	</tr>
	<tr>
		<td width="100%"><img src="img/spacer.gif" width="20" border="0"></td>
	</tr>
</table>



<table border="0" cellpadding="10" cellspacing="0" style="border-collapse: collapse"  width="100%">
  <tr>
    <td width="100%"><b>Search Tips:</b></td>
  </tr>
  <tr>
    <td width="100%"><b>Terms and Phrases</b>: A search sentence is broken up into terms and operators. 
  There are two types of terms: Single Terms and Phrases. A Single Term is a single word such as &quot;causality&quot; or 
  &quot;assessments&quot;.<br>
  A Phrase is a group of words surrounded by double quotes such as &quot;causality in 
  connection&quot;.<br>
  Multiple terms can be combined together with Boolean operators to form a more 
  complex query (see below).</td>
  </tr>
  <tr>
    <td width="100%"><b>Database Fields: </b>Search database field by typing the field name followed by a 
  colon &quot;:&quot; and then the term you are looking for. Database fields are listed at 
  the end of the page. As an example, If you want to find queries 
  where aer number is &quot; 376556 &quot; &nbsp;and indication is &quot; COLORECTAL CANCER &quot;, you 
  can enter:<b> <font color="#000080">aerNumber:</font></b><font color="#000080">376556<b> AND</b> <b>indication:</b> &quot;COLORECTAL CANCER&quot;</font></td>
  </tr>
  <tr>
    <td width="100%">
  <b>Wildcard Searches</b>: To perform a single character wildcard 
  search use the &quot;?&quot; symbol. To perform a multiple character wildcard 
  search use the &quot;*&quot; symbol.
    </td>
  </tr>
  <tr>
    <td width="100%"><b>Fuzzy Searches:</b> To search for a term similar in spelling, 
    type &quot;~' (tilde) symbol at the end of a single word term. <br>
    <font color="#000080">roam~&nbsp;</font>&nbsp;&nbsp; 
    (this search will find terms like foam and roams)</td>
  </tr>
  <tr>
    <td width="100%"><b>Proximity Searches: </b>To find words that are within a specific 
  distance away, type the tilde, &quot;~&quot;, symbol at the end of a Phrase. For example 
  to search for a &quot;Roaccutane&quot; and &quot; nausea&quot; within 10 words of each other in a 
  document type:<br> 
    <font color="#000080">&quot;Roaccutane nausea&quot;~10 </font>
    </td>
  </tr>
  <tr>
    <td width="100%"><b>Range Searches: </b>To find a date whose values are between 
  the lower and upper bound specified by the range type:&nbsp; <br>
    <font color="#000080">queryExpiryDate:[20060801 TO 20061001]&nbsp; </font>
  <br>This will find queries whose queryExpiryDate fields have values between 20060801 and 20061001. The date 
  format is <b><font color="#000080">YYYYMMDD</font></b>.
    </td>
  </tr>
  <tr>
    <td width="100%"><b>Boolean operators:</b><br><b>OR</b> - To search for queries that contains either &quot;death cases&quot; or just &quot;death&quot; 
    type:<br><font color="#000080">death cases&quot; OR death&nbsp; </font> <br>
<b>AND - </b>The AND operator matches queries where both terms exist anywhere of a single query.
To search for queries that contain death cases and Pegasys type:<br>&quot;death cases&quot; AND Pegasys <br>
<b>NOT - </b>The NOT operator excludes queries that contain the term after NOT.
To search for query that contain &quot;Death cases&quot; but not &quot;Pregnancy cases&quot; type:<br>
    <font color="#000080">&quot;Death cases&quot; NOT &quot;Pregnancy cases&quot;&nbsp;&nbsp;
    </font>
</td>
  </tr>
  <tr>
    <td width="100%"><b>Database field 
    list:</b></td>
  </tr>
</table>






































<table class="MsoTableContemporary" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
  <thead>
    <tr>
      <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
      <p class="MsoNormal"><b>Database fields</b></td>
      <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
      <p class="MsoNormal"><b>Description</b></td>
    </tr>
  </thead>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">queryId </td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Query Id, just number</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">queryNumber</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Query Id with full qualified number ex:QTT001234</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">followupNumber</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Followup number included in QRF</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">sourceCountry</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Source country of the query</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">queryType</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Query type</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">reporterType</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Reporter or requester type</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">statusCode </td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Query status code:</p>
    <p class="MsoNormal" style="text-autospace: none">
    <span style="font-size: 10.0pt; color: black">NSUB - Not Submitted</span></p>
    <p class="MsoNormal" style="text-autospace: none">
    <span style="font-size: 10.0pt; color: black">SUBM - Submitted</span></p>
    <p class="MsoNormal" style="text-autospace: none">
    <span style="font-size: 10.0pt; color: black">RJCT - Rejected</span></p>
    <p class="MsoNormal" style="text-autospace: none">
    <span style="font-size: 10.0pt; color: black">PROC - Being Processed</span></p>
    <p class="MsoNormal" style="text-autospace: none">
    <span style="font-size: 10.0pt; color: black">CLOS - Closed</span></p>
    <p class="MsoNormal" style="text-autospace: none">
    <span style="font-size: 10.0pt; color: black">AMND - Amended</span></p>
    <p class="MsoNormal" style="text-autospace: none">
    <span style="font-size: 10.0pt; color: black">Use as </span>statusCode:<span style="font-size: 10.0pt; color: black"> 
    NSUB</span></td>
  </tr>  
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">currentDueDate</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Changed due date of the query</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">originalDueDate</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Original due date of the query</td>
  </tr>

  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">urgencyReason</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Urgency reason for requesting query to be answered with 
    in less than 14 days</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">workflowRouteType</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Workflow route type:</p>
    <p class="MsoNormal">B - Assign to PS for Medical Interpretation</p>
    <p class="MsoNormal">A2 - Assign to Self</p>
    <p class="MsoNormal">A1 - Assign to DMG for Data Search</p>
    <p class="MsoNormal">Use as workflowRouteType:B</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">requestedDate</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Date requested by requester to be answered</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">dMGResponseDate</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">DMG response date</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">queryExpiryDate</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Query expiry date</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">querySummary</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Query summary</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">preQueryPrepComment</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Pre query preparation comment</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">caseComment</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Case comment</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">oformatSpecialRequest</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">OFormat special request</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">mnftBatchNumber</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Manufacture batch number</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">indNumber</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">IND number</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">submitDate</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Query submit date</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">queryLabel</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Query label given by DSCL</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">requesterQueryLabel</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Query title given by requester</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">requesterFirstName</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Requester first name</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">requesterLastName</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Requester last name</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">requesterPhone</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Requester phone</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">requesterFax</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Requester fax</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">requesterEmail</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Requester email</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">requesterCountry</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Requester country</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">natureOfCase</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Nature of case, your question</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">indexCase</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Index case</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">prevMedicalHistory</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Previous medical history</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">signsAndSymptoms</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Signs and symptoms</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Conmeds</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Comedications</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Investigations</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">investigations</td>
  </tr>

  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Diagnoses</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">diagnoses</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">otherInfo</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Other info</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">pqpType</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Pre query preparation type</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">pqpOtherComment</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Pre query preparation other comment</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">pregnancyExposureType</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Pregnancy exposure type</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">pregnancyOutcome</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Pregnancy outcome</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">pmxRequestDescription</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Performance metrics request description</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">nonAffiliateNotes</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Non affiliate notes</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">affiliateNotes</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Affiliate notes</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">drugRetrievalName</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Drug retrieval name</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">innGenericName</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Generic drug name</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Route</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Route</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Formulation</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Formulation</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Indication</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">indication</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Does</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">does</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">caseSelection</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Case selection for clinical trial</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">protocolNumbers</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Protocol number</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">patientNumbers</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Patient number</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">ctrlOfFormat</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Control of format</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">crtnNumbers</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Control numbers</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">aerNumber</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Aer number</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">localReferenceNumber</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Local reference number</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">externalReferenceNumber</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">External reference number</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">aeTerms</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">AE terms</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">labelingDocType</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Labeling document type</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">dmgMember</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">DMG member involved in a query</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">pSMember</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">PS member involved in a query</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">dSCLMember</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">DSCL member involved in a query</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Interval</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Interval</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">timePeriodAddressedFrom</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Time period addressed from, in QRF page, Cumulative 
    period - Date from</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">timePeriodAddressedTo</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Time period addressed to, in QRF page, Cumulative 
    period - Date to</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">queryResponseCoordinator</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Query Response coordinator who composed the final 
    document</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Filenames</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Actual file names attached to a query</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">fileTitles</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">File title given to an attachment for a query</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">fileAuthors</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Who attached an attachment to a query</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">initiateDate;</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">Date when query is accepted and initiated by DSCL</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">finalDeliveryDate</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">Date when a final response delivered to a requester.</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">affiliateAccess</td>
    <td width="366" valign="top" style="width: 274.5pt; border-left: medium none; border-right: medium none; border-top: medium none; border-bottom: 2.25pt solid white; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #F2F2F2">
    <p class="MsoNormal">If the query has affiliate access given. Values are either 'Y' or 'N'</p>
    <p class="MsoNormal">Uses:</p>
    <p class="MsoNormal">affiliateAccess:Y</td>
  </tr>
  <tr>
    <td width="223" valign="top" style="width: 167.4pt; border-left: medium none; border-right: 2.25pt solid white; border-top: medium none; border-bottom: medium none; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">expiredResponse</td>
    <td width="366" valign="top" style="width: 274.5pt; border: medium none; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in; background: #CCCCCC">
    <p class="MsoNormal">If the query response is expired. Values are either 'Y' 
    or 'N'</p>
    <p class="MsoNormal">Uses:</p>
    <p class="MsoNormal">expiredResponse:N</td>
  </tr>

</table>






			</td>
		</tr>
	</tbody>
</table>
</s:form>
</body>
</html>