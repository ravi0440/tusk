<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<html>

<body onLoad="preload()">
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
	<TBODY>
		<TR>
			<TD width="100%">
				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>The following queries matched your search criteria</P></SPAN></DIV>
							</TD>
						</TR>
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>
								<P>

									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<TD height="20" colspan="2"><img src="img/spacer.gif" height="20"></TD>
										</tr>
										<tr>
											<td width="40%">
												<b>Total number of matching results: <s:property value="results.resultSize"/></b>
											</td>
										</tr>
										<tr>
											<TD height="20" colspan="2"><img src="img/spacer.gif" height="20"></TD>
										</tr>
										<tr>
											<td width="40%">
												<s:if test="resultPage != 1">
													<a href="
														<s:url action="PerformRepositorySearch">
															<s:param name="orderBy" value="%{orderBy}" />
															<s:param name="asc" value="%{asc}" />
															<s:param name="resultPage" value="%{resultPage - 1}" />
														</s:url>">
														&lt;
													</a>
												</s:if>
												Page <s:property value="resultPage" /> of <s:property value="numPages" />
												<s:if test="resultPage < numPages">
													<a href="
														<s:url action="PerformRepositorySearch">
															<s:param name="orderBy" value="%{orderBy}" />
															<s:param name="asc" value="%{asc}" />
															<s:param name="resultPage" value="%{resultPage + 1}" />
														</s:url>">
														&gt;
													</a>
												</s:if>
											</td>
										</tr>

										<tr>
											<TD height="20" colspan="2"><img src="img/spacer.gif" height="20"></TD>
										</tr>


									</table>

									<TABLE class=DataGrid style="BORDER-LEFT-COLOR: #6699cc; BORDER-BOTTOM-COLOR: #6699cc; BORDER-TOP-COLOR: #6699cc; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #6699cc" borderColor=#6699cc cellSpacing=0 cellPadding=3 border=1>
										<TBODY>
											<TR class=DataGridHeader>
												<s:if test="results.isQuick">
													<TD class=DataGridHeader style="WIDTH: 10%"><a class=BlindHeaderLink href="QuickSearchResults.action?orderBy=queryNumber">Query#</a></TD>
													<TD class=DataGridHeader style="WIDTH: 10%"><a class=BlindHeaderLink href="#">Status</a></TD>
													<TD class=DataGridHeader style="WIDTH: 12%"><a class=BlindHeaderLink href="QuickSearchResults.action?orderBy=drugNameAbbrev">Drug Name</a></TD>
													<TD class=DataGridHeader style="WIDTH: 16%"><a class=BlindHeaderLink href="QuickSearchResults.action?orderBy=queryType.queryType">Query Type</a></TD>
													<TD class=DataGridHeader style="WIDTH: 10%"><a class=BlindHeaderLink href="QuickSearchResults.action?orderBy=requester.country.description">Requester Country</a></TD>
													<TD class=DataGridHeader style="WIDTH: 15%"><a class=BlindHeaderLink href="QuickSearchResults.action?orderBy=currentDueDate">Response Date</a></TD>
													<TD class=DataGridHeader style="WIDTH: 15%"><a class=BlindHeaderLink href="QuickSearchResults.action?orderBy=expiryDate">Expiry Date</a></TD>
												</s:if>
												<s:else>
													<TD class=DataGridHeader style="WIDTH: 10%"><a class=BlindHeaderLink href="PerformRepositorySearch.action?orderBy=queryNumber&asc=<s:if test="orderBy == 'queryNumber' && asc">false</s:if><s:else>true</s:else>">Query#</a></TD>
													<TD class=DataGridHeader style="WIDTH: 10%"><a class=BlindHeaderLink href="#">Status</a></TD>
													<TD class=DataGridHeader style="WIDTH: 12%"><a class=BlindHeaderLink href="PerformRepositorySearch.action?orderBy=drugs.drugRetrievalName&asc=<s:if test="orderBy == 'drugs.drugRetrievalName' && asc">false</s:if><s:else>true</s:else>">Drug Name</a></TD>
													<TD class=DataGridHeader style="WIDTH: 16%"><a class=BlindHeaderLink href="PerformRepositorySearch.action?orderBy=queryType.queryType&asc=<s:if test="orderBy == 'queryType.queryType' && asc">false</s:if><s:else>true</s:else>">Query Type</a></TD>
													<TD class=DataGridHeader style="WIDTH: 10%"><a class=BlindHeaderLink href="PerformRepositorySearch.action?orderBy=requester.country.description&asc=<s:if test="orderBy == 'requester.country.description' && asc">false</s:if><s:else>true</s:else>">Requester Country</a></TD>
													<TD class=DataGridHeader style="WIDTH: 15%"><a class=BlindHeaderLink href="PerformRepositorySearch.action?orderBy=currentDueDate&asc=<s:if test="orderBy == 'currentDueDate' && asc">false</s:if><s:else>true</s:else>">Response Date</a></TD>
													<TD class=DataGridHeader style="WIDTH: 15%"><a class=BlindHeaderLink href="PerformRepositorySearch.action?orderBy=expiryDate&asc=<s:if test="orderBy == 'expiryDate' && asc">false</s:if><s:else>true</s:else>">Expiry Date</a></TD>
												</s:else>
												<TD class=DataGridHeader style="WIDTH: 25%">Query Summary</TD>
												<TD class=DataGridHeader style="WIDTH: 2%"></TD>
											</TR>
											<s:iterator value="results.results" status="stat">
											<tr>
												<td class=DataGridItem><s:property value="query.queryNumber"/></td>
												<td class=DataGridItem><s:property value="query.queryStatusCode.status"/></td>
												<td class=DataGridItem><s:property value="query.drugNameAbbrev"/></td>
												<td class=DataGridItem><s:property value="query.queryType.queryType"/></td>
												<td class=DataGridItem><s:property value="query.requester.country.description"/></td>
												<td class=DataGridItem>
													<joda:format value="${query.currentDueDate}" pattern="dd-MMM-yyyy" locale="en"/>
												</td>
												<td class=DataGridItem>
													<joda:format value="${query.expiryDate}" pattern="dd-MMM-yyyy" locale="en"/>
												</td>
												<td class=DataGridItem><s:property value="query.querySummaryAsHtml" escapeHtml="false" /></td>
												<td class=DataGridItem>
												<s:if test="searchParams instanceof com.roche.dss.qtt.query.web.model.RepositorySearchParams">
													<s:if test="query.queryStatusCode.statusCode == 'CLOS'">											
													<a href="ClosedQueryDetails.action?queryId=<s:property value="query.querySeq"/>&sourceSearch=repositorySearch"
														onclick="toggle('img<s:property value="#stat.index"/>a')"
														onmouseover="hilight('img<s:property value="#stat.index"/>a')"
														onmouseout="lolight('img<s:property value="#stat.index"/>a')">
														<img name="img<s:property value="#stat.index"/>a"
															src="img/find_off.gif"
															alt="View Closed Query Details"
															title="View Closed Query Details"
															border="0">
													</a>
													</s:if>
													<s:else>
													<s:if test="query.queryStatusCode.statusCode == 'PROC'">
														<a href="RetrieveQueryDetails.action?queryId=<s:property value="query.querySeq"/>&sourceSearch=repositorySearch"
															onclick="toggle('img<s:property value="#stat.index"/>a')"
															onmouseover="hilight('img<s:property value="#stat.index"/>a')"
															onmouseout="lolight('img<s:property value="#stat.index"/>a')">
															<img name="img<s:property value="#stat.index"/>a"
																src="img/find_off.gif"
																alt="View  Query Details"
																title="View  Query Details"
																border="0">
														</a>
													</s:if>
													<s:else>
													<a href="SearchPreviewAction.action?queryId=<s:property value="query.querySeq"/>&sourceSearch=repositorySearch"
														onclick="toggle('img<s:property value="#stat.index"/>a')"
														onmouseover="hilight('img<s:property value="#stat.index"/>a')"
														onmouseout="lolight('img<s:property value="#stat.index"/>a')">
														<img name="img<s:property value="#stat.index"/>a"
															src="img/find_off.gif"
															alt="View  Query Preview"
															title="View  Query Preview"
															border="0">
													</a>
													</s:else>
													</s:else>
												</s:if>
												<s:else>
													<s:if test="query.queryStatusCode.statusCode == 'CLOS'">	
													<a href="ClosedQueryDetails.action?queryId=<s:property value="query.querySeq"/>&sourceSearch=quickSearch"
														onclick="toggle('img<s:property value="#stat.index"/>a')"
														onmouseover="hilight('img<s:property value="#stat.index"/>a')"
														onmouseout="lolight('img<s:property value="#stat.index"/>a')">
														<img name="img<s:property value="#stat.index"/>a"
															src="img/find_off.gif"
															alt="View Closed Query Details"
															title="View Closed Query Details"
															border="0">
													</a>
													</s:if>
													<s:else>
													<s:if test="query.queryStatusCode.statusCode == 'PROC'">
														<a href="RetrieveQueryDetails.action?queryId=<s:property value="query.querySeq"/>&sourceSearch=quickSearch"
															onclick="toggle('img<s:property value="#stat.index"/>a')"
															onmouseover="hilight('img<s:property value="#stat.index"/>a')"
															onmouseout="lolight('img<s:property value="#stat.index"/>a')">
															<img name="img<s:property value="#stat.index"/>a"
																src="img/find_off.gif"
																alt="View  Query Details"
																title="View  Query Details"
																border="0">
														</a>
													</s:if>
													<s:else>
													<a href="SearchPreviewAction.action?queryId=<s:property value="query.querySeq"/>&sourceSearch=quickSearch"
														onclick="toggle('img<s:property value="#stat.index"/>a')"
														onmouseover="hilight('img<s:property value="#stat.index"/>a')"
														onmouseout="lolight('img<s:property value="#stat.index"/>a')">
														<img name="img<s:property value="#stat.index"/>a"
															src="img/find_off.gif"
															alt="View  Query Preview"
															title="View  Query Preview"
															border="0">
													</a>
													</s:else>
													</s:else>
												</s:else>
												</td>
											</tr>
												<s:if test="attsMatchFound">
												<tr>
												<td colspan="8">
												<s:if test="attMatches > 1">
													<b><s:property value="attMatches"/> Match found.</b><br>
												</s:if>
												<s:iterator value="attsWithInfo">	
													[<s:property value="highlightedText" escape="false"/>]
													file: <span class="attachment"><s:property value="attachment.title"/></span>
													<br/>
												</s:iterator>
												</td>
												</tr>
												</s:if>
											</s:iterator>
										</TBODY>
									</TABLE>

									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<TD height="20" colspan="2"><img src="img/spacer.gif" height="20"></TD>
										</tr>
										<tr>
											<td width="100%">
												<s:form action="RepositorySearch" method="post">
													<s:iterator var="val" value="searchParams.drugRetrievalName">
														<input type="hidden" name="searchParams.drugRetrievalName" value="<s:property value="val"/>"/>
													</s:iterator>
													<s:iterator var="val" value="searchParams.innGenericName">
														<input type="hidden" name="searchParams.innGenericName" value="<s:property value="val"/>"/>
													</s:iterator>
													<s:iterator var="val" value="searchParams.indication">
														<input type="hidden" name="searchParams.indication" value="<s:property value="val"/>"/>
													</s:iterator>
													<s:hidden name="searchParams.queryType"/>
													<s:hidden name="searchParams.aeTermDescription"/>
													<s:hidden name="searchParams.queryDescriptionPrevMedicalHistory"/>
													<s:hidden name="searchParams.interactingDrugName"/>
													<s:iterator var="val" value="searchParams.author">
														<input type="hidden" name="searchParams.author" value="<s:property value="val"/>"/>
													</s:iterator>
													<s:hidden name="searchParams.periodDateFrom"/>
													<s:hidden name="searchParams.periodDateTo"/>
													<s:iterator var="val" value="searchParams.labelingDocTypeIds">
														<input type="hidden" name="searchParams.labelingDocTypeIds" value="<s:property value="%{#val}"/>"/>
													</s:iterator>
													<s:hidden name="searchParams.querySummary"/>
													<s:iterator var="val" value="searchParams.workflowRouteTypeIds">
														<input type="hidden" name="searchParams.workflowRouteTypeIds" value="<s:property value="val"/>"/>
													</s:iterator>
													<s:iterator var="val" value="searchParams.dmgMembers">
														<input type="hidden" name="searchParams.dmgMembers" value="<s:property value="val"/>"/>
													</s:iterator>
													<s:iterator var="val" value="searchParams.psMembers">
														<input type="hidden" name="searchParams.psMembers" value="<s:property value="val"/>"/>
													</s:iterator>
													<s:iterator var="val" value="searchParams.dsclMembers">
														<input type="hidden" name="searchParams.dsclMembers" value="<s:property value="val"/>"/>
													</s:iterator>
													<s:hidden name="searchParams.nonExpiredResponsesOnly"/>
													<s:hidden name="searchParams.requesterSurname"/>
													<s:iterator var="val" value="searchParams.requesterCountryCode">
														<input type="hidden" name="searchParams.requesterCountryCode" value="<s:property value="val"/>"/>
													</s:iterator>
													<s:iterator var="val" value="searchParams.reporterTypeId">
														<input type="hidden" name="searchParams.reporterTypeId" value="<s:property value="%{#val}" />"/>
													</s:iterator>
													<s:iterator var="val" value="searchParams.sourceCountryCode">
														<input type="hidden" name="searchParams.sourceCountryCode" value="<s:property value="val"/>"/>
													</s:iterator>
													<s:hidden name="searchParams.qryResponseDateFrom"/>
													<s:hidden name="searchParams.qryResponseDateTo"/>
													<s:hidden name="searchParams.requesterQueryLabel"/>
													<s:hidden name="searchParams.queryNumber"/>
													<s:hidden name="searchParams.qryInitiateDateFrom"/>
													<s:hidden name="searchParams.qryInitiateDateTo"/>
													<s:hidden name="searchParams.qryFinalDeliveryDateFrom"/>
													<s:hidden name="searchParams.qryFinalDeliveryDateTo"/>
													<b><u><a href="javascript:document.forms[0].submit();" class="Anchor">Refine</a></u> search criteria... </b>
												</s:form>
											</td>
										</tr>
									</table>
								</P></SPAN></DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
  		<TR>
			<TD width="100%" height=20></TD>
		</TR>
  	</TBODY>
</TABLE>
</TD>
</TR>
</TBODY>
</TABLE>
</body>
</html>