<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>

<body onload="initialise();queryTypeChange();">
<s:form action="RepositorySearchApplyParameters" method="post">
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<tbody>
  		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>

						<s:if test="hasActionErrors() || hasFieldErrors()">
			              	<%@ include file="../global/validation_error.jsp" %>
						</s:if>

						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%"><span>
								<p>Drug Details</p></span></div></td>
						</tr>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><span>
								<p>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top">Retrieval Name / Preferred Name:</td>
											<td width="40%">
												<s:select id="drugPNameList" name="searchParams.drugRetrievalName" size="5" multiple="true" list="dictionary.usedRetrievalNames" cssClass="SearchWidget" theme="simple"/>
												<span class="rightarrowwhite">&raquo;</span>
											</td>
											<td width="30%" valign="top">
												<input type="checkbox" value="on" id="drugPNameCB" name="allDrugRetrievalNames" onclick="enableDisable('drugPNameList','drugPNameCB')"/>All
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Generic Name:</td>
											<td width="40%">
												<s:select id="genrDrugList" name="searchParams.innGenericName" size="5" multiple="true" list="dictionary.usedGenericNames" cssClass="SearchWidget" theme="simple"/>
												<span class="rightarrowwhite">&raquo;</span>											
											</td>
											<td width="30%" valign="top">
												<input type="checkbox" value="on" id="genrDrugCB" name="allGenericDrugName" onclick="enableDisable('genrDrugList','genrDrugCB')"/> All
											</td>
										</tr>
										<tr>
											<td valign="top">Indication: </td>
											<td width="40%">
												<s:select id="indicsList" name="searchParams.indication" size="5" multiple="true" list="dictionary.usedIndications" cssClass="SearchWidget" theme="simple"/>
											</td>
											<td width="30%" valign="top">
												<input type="checkbox" value="on" id="indicsCB" name="allIndications" onclick="enableDisable('indicsList','indicsCB')"/> All
											</td>
										</tr>
									</table>
								</p></span></div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
		<tr>
			<td width="100%" height=9></td>
		</tr>
	</tbody>
</table>

<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<tbody>
  		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%"><span>
								<p>Query Type</p></span></div>
							</td>
						</tr>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><span>
								<p>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top">
												Query Type:
											</td>
											<td width="40%">
												<s:select id="queryTypesList" onchange="queryTypeChange()" name="searchParams.queryType" list="dictionary.allQueryTypeNames"
												cssClass="SearchWidgetLong" theme="simple"/>
											</td>
											<td width="30%" valign="top">
												<input type="checkbox" value="on" id="queryTypesCB" name="allQueryTypes" onclick="enableDisable('queryTypesList','queryTypesCB');queryTypeChange()"/> All
											</td>
										</tr>
										</tr>
											<td colspan="3"><img src="img/spacer.gif" height="2"></td>
										<tr>
											<td>Incoming Event Terms:</td>
											<td>
												<s:textfield id="eventTerms" name="searchParams.aeTermDescription" cssClass="SearchWidget" maxlength="600" theme="simple"/>
											</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>Previous Medical History Terms: </td>
											<td>
												<s:textfield id="prevMedicalHistory" name="searchParams.queryDescriptionPrevMedicalHistory" class="SearchWidget" maxlength="1000" theme="simple"/>
											</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>Interacting Drug Name (Generic): </td>
											<td>
												<s:textfield id="interactingDrugName" name="searchParams.interactingDrugName" class="SearchWidget" maxlength="200" theme="simple"/>
											</td>
											<td>&nbsp;</td>
										</tr>
									</table>
								</p></span></div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=9></td>
		</tr>
  	</tbody>
</table>

<table cellSpacing=0 cellPadding=0 width="100%" border=0>
	<tbody>
		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%"><span>
								<p>Query Response Details</p></span></div></td>
						</tr>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><span>
								<p>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top">Query Response Coordinator:</td>
											<td width="40%">
												<s:select id="authorList" name="searchParams.author" size="5" multiple="true" list="dictionary.usedResponseAuthors" cssClass="SearchWidget" theme="simple"/>
											</td>
											<td width="30%" valign="top">
												<input type="checkbox" value="on" id="authorCB" name="allAuthors" onclick="enableDisable('authorList','authorCB')"/> All
											</td>
										</tr>
										<tr>
											<td rowspan="2" valign="top">Time Period Addressed within the Response:</td>
											<td colspan="4">
												<table cellspacing="4" cellpadding="0" border="0">
													<tr>
														<td width="8%"><b>From</b></td>
														<td><img src="img/spacer.gif" width="20"></td>
														<td>
															<s:textfield name="searchParams.periodDateFrom" size="13" maxlength="11" theme="simple"/>
														</td>
														<td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="70%" colspan="4">
												<table cellspacing="4" cellpadding="0" border="0">
													<tr>
														<td width="8%"><b>To</b></td>
														<td><img src="img/spacer.gif" width="20"></td>
														<td>
															<s:textfield name="searchParams.periodDateTo" size="13" maxlength="11" theme="simple"/>
														</td>
														<td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td valign="top">Labelling document used:</td>
											<td>
												<s:select id="lDocsList" name="searchParams.labelingDocTypeIds" size="5" multiple="true" list="dictionary.usedLabellingDocs"
												cssClass="SearchWidget" theme="simple" listKey="ldocTypeId" listValue="ldocType"/>
											</td>
											<td valign="top">
												<input type="checkbox" value="on" id="lDocsCB" name="allLabellingDocs" onclick="enableDisable('lDocsList','lDocsCB')"/> All
											</td>
										</tr>
										<tr>
											<td valign="top">Query Summary:</td>
											<td colspan="3">
												<table cellspacing="0" cellpadding="1" border="0">
													<tr>
														<td width="10%">
															<s:textarea name="searchParams.querySummary" cols="65" rows="8" onkeypress="textCounter(this,400);" onblur="textCounter(this,400);" theme="simple"/>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td valign="top">Workflow Route type (Process Type):</td>
											<td>
												<s:select id="lProcessList" name="searchParams.workflowRouteTypeIds" size="5" multiple="true" list="dictionary.processTypes"
												cssClass="SearchWidget" theme="simple" listKey="workflowRouteTypeId" listValue="workflowRouteType"/>
											</td>
											<td valign="top">
												<input type="checkbox" value="on" id="lProcessCB" name="allProcessType" onclick="enableDisable('lProcessList','lProcessCB')"/> All
											</td>
										</tr>

										<tr>
											<td width="30%" valign="top">DMG Members involved in the Query:</td>
											<td width="40%">
												<s:select id="dmgList" name="searchParams.dmgMembers" size="5" multiple="true" list="dictionary.DMGUserInvolvedNames" cssClass="SearchWidget" theme="simple"/>
											</td>
											<td width="30%" valign="top">
												<input type="checkbox" value="on" id="dmgCB" name="allDmgMembersInvolved" onclick="enableDisable('dmgList','dmgCB')"/> All
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">PS Members involved in the Query:</td>
											<td width="40%">
												<s:select id="psList" name="searchParams.psMembers" size="5" multiple="true" list="dictionary.PSUserInvolvedNames" cssClass="SearchWidget" theme="simple"/>
											</td>
											<td width="30%" valign="top">
												<input type="checkbox" value="on" id="psCB" name="allPsMembersInvolved" onclick="enableDisable('psList','psCB')"/> All
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">DSCL Members involved in the Query:</td>
											<td width="40%">
												<s:select id="dsclList" name="searchParams.dsclMembers" size="5" multiple="true" list="dictionary.DSCLUserInvolvedNames" cssClass="SearchWidget" theme="simple"/>
											</td>
											<td width="30%" valign="top">
												<input type="checkbox" value="on" id="dsclCB" name="allDsclMembersInvolved" onclick="enableDisable('dsclList','dsclCB')"/> All
											</td>
										</tr>

										
										<tr>
											<td valign="top">
                                                Search non expired responses only
                                            </td>
											<td valign="top" colspan="3">
												<s:checkbox id="nonExpCB" name="searchParams.nonExpiredResponsesOnly"/>
                                            </td>
										</tr>
									</table>

								</p></span></div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=9></td>
		</tr>
  	</tbody>
</table>

<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<tbody>
  		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%"><span>
								<p>Query Request Details</p></span></div></td>
						</tr>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><span>
								<p>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top">Requester Last Name:</td>
											<td width="40%">
												<s:select id="reqLNameList" name="searchParams.requesterSurname" list="dictionary.usedRequestorLastNames"
												cssClass="SearchWidget" theme="simple"/>
												<span class="rightarrowwhite">&raquo;</span>
											</td>
											<td width="30%" valign="top">
												<input type="checkbox" value="on" id="reqLNameCB" name="allRequestorLastNames" onclick="enableDisable('reqLNameList','reqLNameCB')"/> All
											</td>
										</tr>
										<tr>
											<td valign="top">Requester Country:</td>
											<td>
												<s:select id="reqCtryList" name="searchParams.requesterCountryCode" size="5" multiple="true" list="dictionary.usedRequestorCountries"
												cssClass="SearchWidget" theme="simple" listKey="countryCode" listValue="description"/>
												<span class="rightarrowwhite">&raquo;</span>
											</td>
											<td valign="top">
												<input type="checkbox" value="on" id="reqCtryCB" name="allRequestorCountries" onclick="enableDisable('reqCtryList','reqCtryCB')"/> All
											</td>
										</tr>
										<tr>
											<td valign="top">Reporter Type: </td>
											<td>
												<s:select id="repTypeList" name="searchParams.reporterTypeId" size="5" multiple="true" list="dictionary.usedReporterTypes"
												cssClass="SearchWidget" theme="simple" listKey="reporterTypeId" listValue="reporterType"/>
											</td>
											<td valign="top">
												<input type="checkbox" value="on" id="repTypeCB" name="allReporterTypes" onclick="enableDisable('repTypeList','repTypeCB')"/> All
											</td>
										</tr>
										<tr>
											<td valign="top">Source Country: </td>
											<td>
												<s:select id="srcCtryList" name="searchParams.sourceCountryCode" size="5" multiple="true" list="dictionary.usedSourceCountries"
												cssClass="SearchWidget" theme="simple" listKey="countryCode" listValue="description"/>
												<span class="rightarrowwhite">&raquo;</span>
											</td>
											<td width="90%" valign="top">
												<input type="checkbox" value="on" id="srcCtryCB" name="allSourceCountries" onclick="enableDisable('srcCtryList','srcCtryCB')"/>All
											</td>
										</tr>
										<tr>
											<td rowspan="2" valign="top">Date of Query Request</td>
											<td colspan="4">
												<table cellspacing="4" cellpadding="0" border="0">
													<tr>
														<td width="8%"><b>From</b></td>
														<td><img src="img/spacer.gif" width="20"></td>
														<td>
															<s:textfield name="searchParams.qryResponseDateFrom" size="13" maxlength="11" theme="simple"/>
														</td>
														<td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="70%" colspan="4">
												<table cellspacing="4" cellpadding="0" border="0">
													<tr>
														<td width="8%"><b>To</b></td>
														<td><img src="img/spacer.gif" width="20"></td>
														<td>
															<s:textfield name="searchParams.qryResponseDateTo" size="13" maxlength="11" theme="simple"/>
														</td>
														<td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
													</tr>
												</table>
											</td>
										</tr>
										
										<tr>
											<td>Requester Query Title:</td>
											<td>
												<s:textfield name="searchParams.requesterQueryLabel" maxlength="200" size="20" cssClass="SearchWidget" theme="simple"/>
											</td>
										</tr>
										
									</table>

								</p></span></div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=9></td>
		</tr>
  	</tbody>
</table>






<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<tbody>
  		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%"><span>
								<p>DSCL Details</p></span></div></td>
						</tr>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><span>
								<p>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td>DSCL Query Number</td>
											<td>
												<s:textfield name="searchParams.queryNumber" maxlength="20" cssClass="SearchWidget" theme="simple"/>
											</td>
										</tr>
										<tr>
											<td rowspan="2" valign="top">Query Initiate Date</td>
											<td colspan="4">
												<table cellspacing="4" cellpadding="0" border="0">
													<tr>
														<td width="8%"><b>From</b></td>
														<td><img src="img/spacer.gif" width="20"></td>
														<td>
															<s:textfield name="searchParams.qryInitiateDateFrom" size="13" maxlength="11" theme="simple"/>
														</td>
														<td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="70%" colspan="4">
												<table cellspacing="4" cellpadding="0" border="0">
													<tr>
														<td width="8%"><b>To</b></td>
														<td><img src="img/spacer.gif" width="20"></td>
														<td>
															<s:textfield name="searchParams.qryInitiateDateTo" size="13" maxlength="11" theme="simple"/>
														</td>
														<td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
													</tr>
												</table>
											</td>
										</tr>


										<tr>
											<td rowspan="2" valign="top">Final Delivery Date</td>
											<td colspan="4">
												<table cellspacing="4" cellpadding="0" border="0">
													<tr>
														<td width="8%"><b>From</b></td>
														<td><img src="img/spacer.gif" width="20"></td>
														<td>
															<s:textfield name="searchParams.qryFinalDeliveryDateFrom" size="13" maxlength="11" theme="simple"/>
														</td>
														<td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="70%" colspan="4">
												<table cellspacing="4" cellpadding="0" border="0">
													<tr>
														<td width="8%"><b>To</b></td>
														<td><img src="img/spacer.gif" width="20"></td>
														<td>
															<s:textfield name="searchParams.qryFinalDeliveryDateTo" size="13" maxlength="11" theme="simple"/>
														</td>
														<td width="70%">&nbsp;<s:text name="struts.date.format"/></td>
													</tr>
												</table>
											</td>
										</tr>

									</table>

								</p></span></div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=9></td>
		</tr>
  	</tbody>
</table>


<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td align="left" width="100%">
			<s:submit value="Search" align="left"/>
		</td>
		<td width="20"><img src="img/spacer.gif" width="20" border="0"></td>
	</tr>
	<tr>
		<td width="100%"><img src="img/spacer.gif" width="20" border="0"></td>
	</tr>
</table>



			</td>
		</tr>
	</tbody>
</table>
</s:form>
</body>