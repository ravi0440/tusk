<%@ taglib prefix="html" uri="/struts-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<HTML>
<HEAD>
    <TITLE><request:attribute name="title"/></TITLE>
    <LINK href="css/roche.css" type=text/css rel=stylesheet>
    <SCRIPT language="javascript" src="js/image_script.js"></script>
    <SCRIPT language="javascript" src="js/global_script.js"></script>
    <script language="javascript" src="js/initiate_script.js"></script>
    <script language="javascript">
        function doIgnore() {
            document.forms[0].ignoreEmail.value = 'yes';
            document.forms[0].action = 'SendInitiationEmail.action';
            document.forms[0].target = '_self';
            document.forms[0].submit();
        }
    </script>

</HEAD>
<BODY onLoad="preload()">
<html:form action="SendInitiationEmail.action" theme="simple" enctype="multipart/form-data" method="POST" acceptcharset="utf-8">
    <!-- START MAIN BODY SPACE ALL PAGE CONTENTS GO HERE -->
	<input type="hidden" name="iehack" value="&#9760;"/>
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <TBODY>
        <s:actionerror/>
        <tr>
            <td width="100%">
                <table height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <TBODY>
                    <tr>
                        <td class="TitleExpanded"
                            style="BORDER-RIGHT: #28578b 1px solid; BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid"
                            valign="center" align="left" width="100%">
                            <div style="WIDTH: 100%; HEIGHT: 100%">
                                <p><bean:write name="data" property="subject"/></p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid"
                            colspan="2" height="100%">
                            <div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%"
                                 align="left">
                                <table cellspacing="0" cellpadding="1" width="100%" border="0">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                                <table cellspacing="0" cellpadding="1" width="100%" border="0">
                                    <tr>
                                        <td># To:</td>
                                        <td width="90%" colspan="2"><b><html:textfield maxlength="50" size="110"
                                                                                       property="to"
                                                                                       name="to"/></b></td>
                                    </tr>
                                    <tr>
                                        <td>CC:</td>
                                        <td width="90%" colspan="2"><html:textfield maxlength="50" size="110"
                                                                                    property="cc"
                                                                                    name="cc"/></td>
                                    </tr>
                                    <tr>
                                        <td># Subject:</td>
                                        <td width="90%" colspan="2"><html:textfield maxlength="500" size="110"
                                                                                    property="subject"
                                                                                    name="subject"/></td>
                                    </tr>

                                    <tr>
                                        <td>Attach Files:</td>
                                        <td width="90%" colspan="2"></td>
                                    </tr>

                                    <tr>
                                        <td>File 1:</td>
                                        <td width="90%" colspan="2"><a class="Anchor"
                                                                       href="QRFPdfAction?id=<s:property value="queryId"/>&type=<s:property value="emailType"/>"
                                                                       target="attachment"><b><s:property
                                                value="fileOneFileName"/></b></a></td>
                                    </tr>

                                    <tr>
                                        <td>File 2:</td>
                                        <td width="90%" colspan="2"><html:file name="fileTwo" maxlength="100"
                                                                               size="110"/></td>
                                    </tr>

                                    <tr>
                                        <td>File 3:</td>
                                        <td width="90%" colspan="2"><html:file name="fileThree" maxlength="100"
                                                                               size="110"/></td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td width="90%" colspan="2">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td width="90%" colspan="2"><html:textarea rows="12" cols="86"
                                                                                   name="body"/></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    </TBODY>
                </table>
                <table cellspacing="0" cellpadding="0" border="0" align="center">
                    <tr>
                        <td align="center">&nbsp;<input type=hidden name=ignoreEmail value=""></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <html:submit property="send" value="  Send Email  "/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="submit" value="  Ignore Email  " onclick="doIgnore()"/>
                        </td>
                    </tr>
                </table>
            </td>
            <TD width="20"><img src="img/spacer.gif" width="20"></TD>
        </tr>
        </TBODY>
    </table>
    <s:hidden name="emailType"/>
    <s:hidden name="queryId" value="%{queryId}"/>
    <s:hidden name="msg" value="%{msg}"/>
</html:form>
</BODY>
</HTML>
