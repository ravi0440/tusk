<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
 <title>Review QRF</title>
 <script language="javascript" src="js/initiate_script.js"></script>
 <script language="javascript" src="js/qrf_script.js"></script>
 <script language="javascript" src="js/image_script.js"></script>
 <LINK href="css/roche.css" type=text/css rel=stylesheet>
</head>

<body onLoad="preload()">

<table cellSpacing=0 cellPadding=1 width="100%" border=0>
	<tbody>
  		<tr>
			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%"><span>
								<p>Query Review</p></span></div></td>
						</tr>
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><span>
								<p>

									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3">
												<font size="+1">
													<s:if test="%{query.processInstanceId != null && query.processInstanceId != ''}">Query Number: <s:property value="query.queryNumber" /></s:if>
                                                    <s:else>Provisional Query Number: <s:property value="query.queryNumber" /> (<s:property value="query.followupNumber" />)</s:else>
												</font>
											</td>
										</tr>
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td valign="top" colspan="3"><font size="+1">Requester Details</font></td>
										</tr>
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td width="30%">
												First Name:
											</td>
											<td width="70%">
												<s:property value="query.requester.firstName" />
											</td>
										</tr>
										<tr>
											<td width="30%">
												Last Name:
											</td>
											<td>
                                                <s:property value="query.requester.surname" />
											</td>
										</tr>
										<tr>
											<td width="30%">
												Tel No:
											</td>
											<td>
												<s:property value="query.requester.telephone" />
											</td>
										</tr>										
										<tr>
											<td width="30%">
												Email Address:
											</td>
											<td>
												<s:property value="query.requester.email" />
											</td>
										</tr>
										<tr>
											<td width="30%">
												Country:
											</td>
											<td>
                                               <s:property escapeHtml="false" value="query.requester.country.description" />
											</td>
										</tr>
										<tr>
											<td width="30%">
												Organisation Name:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.requester.organisation.name" />
											</td>
										</tr>
										<tr>
											<td width="30%">
												Organisation Type:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.requester.organisation.organisationType.type" />
											</td>
										</tr>
										<tr>
											<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
										</tr>
										<tr>
											<td width="100%" colspan="2" background="img/line.gif"></td>
										</tr>
										<tr>
											<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Query Origin </font></td>
										</tr>
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td width="30%">Source Country of Request:
											</td>
											<td width="70%">
												<s:property escapeHtml="false" value="query.country.description" />
											</td>
										</tr>
										<tr>
											<td width="30%">Reporter Type:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.reporterType.reporterType" />
											</td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2" background="img/line.gif"></td>
										</tr>
				              			<tr>
				                			<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Drug Details </font></td>
										</tr>
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td width="30%">
												All Drugs:
											</td>
											<td>
                                                <s:if test="!query.allDrugs">No</s:if>
												<s:else>Yes</s:else>
											</td>
										</tr>

										<s:if test="!query.allDrugs">
											<tr>
												<td width="25%">
													Drug Retrieval Name:
												</td>
												<td>
													<s:property escapeHtml="false" value="query.drugForPreview.drugRetrievalName" />
												</td>
											</tr>
											<tr>
												<td width="25%">
													Generic Drug Name:
												</td>
												<td>
													<s:property escapeHtml="false" value="query.drugForPreview.innGenericName" />
												</td>
											</tr>
											<tr>
												<td width="25%">
													Route:
												</td>
												<td>
													<s:property escapeHtml="false" value="query.drugForPreview.route" />
												</td>
											</tr>
											<tr>
												<td width="25%">
													Formulation:
												</td>
												<td>
													<s:property escapeHtml="false" value="query.drugForPreview.formulation" />
												</td>
											</tr>
											<tr>
												<td width="25%">
													Indication:
												</td>
												<td>
													<s:property escapeHtml="false" value="query.drugForPreview.indication" />
												</td>
											</tr>
											<tr>
												<td width="25%">
													Dose:
												</td>
												<td>
													<s:property escapeHtml="false" value="query.drugForPreview.dose" />
												</td>
											</tr>
											<tr>
												<td width="25%" valign="top">
													Other Roche Drugs:
												</td>
												<td>
													<s:property escapeHtml="false" value="query.drugForPreview.extraFirstDrug" />
												</td>
											</tr>
										</s:if>
										<tr>
				                			<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2" background="img/line.gif"></td>
										</tr>
				              			<tr>
				                			<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Response Timelines</font></td>
										</tr>
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td width="30%">Original Due Date:
											</td>
											<td width="70%">
                                                <joda:format value="${query.dateRequested}" pattern="dd-MMM-yyyy" locale="en"/>
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Urgency:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.urgencyReason" />
											</td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2" background="img/line.gif"></td>
										</tr>
				              			<tr>
				                			<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
										</tr>
									</table>


									<s:if test="query.requester.organisation.organisationType.rocheAffiliate">
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Preparatory Work Already Done</font></td>
										</tr>
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td width="30%" valign="top">
												Repository:
											</td>
											<td>
												<s:property escapeHtml="false"  value="query.preparation.repository" />
											</td>
										</tr>
										<tr>
											<td width="25%" valign="top">
												PSUR:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.preparation.psur" />
											</td>
										</tr>
										<tr>
											<td width="25%" valign="top">
												CDS:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.preparation.cds" />
											</td>
										</tr>
										<tr>
											<td width="25%" valign="top">
												Issue Workups:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.preparation.issue" />
											</td>
										</tr>
										<tr>
											<td width="25%" valign="top">
												Literature:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.preparation.literature" />
											</td>
										</tr>
										<tr>
											<td width="25%" valign="top">
												Others:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.preparation.others" />
											</td>
										</tr>
										<tr>
											<td width="25%" valign="top">
												Other Comments:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.preparation.othersComment" />
											</td>
										</tr>
										<tr>
											<td width="25%" valign="top">
												Comments:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.preQueryPrepComment" />
											</td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2" background="img/line.gif"></td>
										</tr>
				              			<tr>
				                			<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
										</tr>
									</table>
									</s:if>

									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Query Details</font></td>
										</tr>
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td width="30%">Query Type:
											</td>
											<td width="70%">
												<s:property escapeHtml="false" value="query.queryType.queryType" />
											</td>
										</tr>
										<tr>
											<td width="30%">Followup Number:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.followUpNumber" />
											</td>
										</tr>
										<tr>
											<td width="30%">Query Title:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.requesterQueryLabel" />
											</td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2" background="img/line.gif"></td>
										</tr>
				              			<tr>
				                			<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
										</tr>
									</table>

                                    <s:if test="%{#parameters.withAttachs==null || #parameters.withAttachs}">
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Attachments </font></td>
										</tr>
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>

                                        <!-- attachments -->
                                        <s:set var="attCounter" value="0"/>
                                        <s:iterator id="att" value="query.attachments" status="rowstatus">
                                            <!-- all active final type elements-->
                                            <s:if test="%{#att.activationFlag=='Y' && #attCounter<5}">
                                                <s:if test="%{attachmentCategory.attachmentCategoryId==3}">
                                                    <tr>
                                                        <td width="35%">&nbsp;<s:property value="filenameUpper" /></td>
                                                        <td align="left"><a target="attachments" href="Attachment.action?attachId=<s:property value="attachmentSeq" />&queryId=<s:property value="querySeq" />" onclick="toggle('img2')" onmouseover="hilight('img2')" onmouseout="lolight('img2')"><img name="img2" src="img/find_off.gif" title="View/Open Document" border="0"></a></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <s:set var="attCounter" value="%{#attCounter+1}"/>
                                                </s:if>
                                            </s:if>
                                        </s:iterator>
                                        <s:iterator id="att" value="query.attachments" status="rowstatus">
                                            <!-- all active other type elements-->
                                            <s:if test="%{#att.activationFlag=='Y' && #attCounter<5}">
                                                <s:if test="%{attachmentCategory.attachmentCategoryId!=3 && query.followupNumber.equals(#att.followupNumber)}">
                                                    <tr>
                                                        <td width="35%">&nbsp;<s:property value="filenameUpper" /></td>
                                                        <td align="left"><a target="attachments" href="Attachment.action?attachId=<s:property value="attachmentSeq" />&queryId=<s:property value="querySeq" />" onclick="toggle('img2')" onmouseover="hilight('img2')" onmouseout="lolight('img2')"><img name="img2" src="img/find_off.gif" title="View/Open Document" border="0"></a></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <s:set var="attCounter" value="%{#attCounter+1}" />
                                                </s:if>
                                            </s:if>
                                        </s:iterator>

										<tr>
											<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
										</tr>
										<tr>
											<td width="100%" colspan="2" background="img/line.gif"></td>
										</tr>
										<tr>
											<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
										</tr>
									</table>
                                    </s:if>


									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Cumulative Details</font></td>
										</tr>
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td width="30%">Cumulative Period:
											</td>
											<td width="70%">
                                                <s:if test='query.retrievalPeriod.cumulativeFlag=="N"'>No</s:if>
												<s:else>Yes</s:else>
											</td>
										</tr>
										<s:if test='query.retrievalPeriod.cumulativeFlag=="N"'>
										<tr>
											<td width="30%">Date from:
											</td>
											<td>
                                               <joda:format value="${query.retrievalPeriod.startTs}" pattern="dd-MMM-yyyy" locale="en"/>
											</td>
										</tr>
										<tr>
											<td width="30%">Date to:
											</td>
											<td>
												<joda:format value="${query.retrievalPeriod.endTs}" pattern="dd-MMM-yyyy" locale="en"/>
											</td>
										</tr>
										</s:if>
										<tr>
											<td width="30%">Interval:
											</td>
											<td width="70%">
												<s:property value="query.retrievalPeriod.interval" />
											</td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2" background="img/line.gif"></td>
										</tr>
				              			<tr>
				                			<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
										</tr>
									</table>


                                    <s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@DRUG_INTERACTION}">
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Interacting Drug(s)</font></td>
                                            </tr>
                                            <tr>
                                                <td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
                                            </tr>
                                            <s:iterator value="query.drugs" status="stat">
                                            <s:if test="firstFlag=='N'">
                                            <tr>
                                                <td valign="top" colspan="3">Additional Drug Details - <s:property value="#stat.index+1" /></td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Drug Type:
                                                </td>
                                                <td width="70%">
                                                    <s:if test="rocheDrugFlag=='Y'">
                                                       Roche
                                                    </s:if>
                                                    <s:elseif test="rocheDrugFlag=='N'">
                                                       Non-Roche
                                                    </s:elseif>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Generic name:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="innGenericName"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Retrieval name:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="drugRetrievalName"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Route:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="route"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Dose:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="dose"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Formulation:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="formulation"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
                                            </tr>
                                            </s:if>
                                            </s:iterator>
                                            <tr>
                                                <td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
                                            </tr>
                                            <tr>
                                                <td width="100%" colspan="2" background="img/line.gif"></td>
                                            </tr>
                                            <tr>
                                                <td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
                                            </tr>
                                        </table>
                                   </s:if>

                                    <s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@DRUG_EVENT
                                    				|| query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@MANUFACTURING
                                    				|| query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@PREGNANCY}">
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Query Description</font></td>
										</tr>
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td width="30%" valign="top">Your question:
											</td>
											<td width="70%">
                                                <s:property escapeHtml="false" value="query.queryDescription.natureOfCase" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Index Case:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.queryDescription.indexCase" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Signs & Symp:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.queryDescription.signAndSymptom" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Investigations:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.queryDescription.investigation" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Past Medical History:
											</td>
											<td>
												 <s:property escapeHtml="false" value="query.queryDescription.prevMedicalHistory" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Comedications:
											</td>
											<td>
											    <s:property escapeHtml="false" value="query.queryDescription.conmed" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Other pertinent info:
											</td>
											<td>
												<s:property  escapeHtml="false" value="query.queryDescription.otherInfo" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Diagnosis/Confounders:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.queryDescription.diagnosis" />
											</td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2" background="img/line.gif"></td>
										</tr>
				              			<tr>
				                			<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
										</tr>
									</table>
                                    </s:if>


                                    <s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@PERFORMANCE_METRICS}">
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Query Description</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif" height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Metrics required:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.queryDescription.natureOfCase"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Other pertinent info:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="query.queryDescription.otherInfo"/>
                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif" border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif" border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>

                                    <s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@SIGNAL_DETECTION
                                    				|| query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@LITERATURE_SEARCH
                                    				|| query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@EXTERNAL_AUDIT}">
                                	<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Query Description</font></td>
										</tr>
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td width="30%" valign="top">Nature of question:
											</td>
											<td width="70%">
                                                <s:property escapeHtml="false" value="query.queryDescription.natureOfCase" />
											</td>
										</tr>
                                        <s:if test="query.literSearch || query.externalAudit">
                                        <tr>
                                            <td width="30%" valign="top">Other pertinent info:
                                            </td>
                                            <td>
                                                <s:property escapeHtml="false" value="query.queryDescription.otherInfo"/>
                                            </td>
                                        </tr>
                                        </s:if>
                                    	<tr>
				                			<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2" background="img/line.gif"></td>
										</tr>
				              			<tr>
				                			<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
										</tr>
									</table>
                                    </s:if>

                                    <s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@SIGNAL_DETECTION
                                    				|| query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@DRUG_EVENT
                                    				|| query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@LITERATURE_SEARCH
                                    				|| query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@EXTERNAL_AUDIT}">
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top">Adverse Event(s):
											</td>
											<td width="70%">
												<s:property escapeHtml="false" value="query.aeTerm.description" />
											</td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2" background="img/line.gif"></td>
										</tr>
				              			<tr>
				                			<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
										</tr>
									</table>
                                    </s:if>

                                    <s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@SAE_RECONCILIATION
                                    				|| query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@EXTERNAL_AUDIT}">
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
											<tr>
												<td valign="top" colspan="3"><font size="+1">Clinical Trial Info</font></td>
											</tr>
											<tr>
												<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
											</tr>
											<tr>
												<td width="30%">Protocol No(s):
												</td>
												<td width="70%" valign="top">
                                                    <s:property escapeHtml="false" value="query.trial.protocolNumber" />
												</td>
											</tr>
											<tr>
												<td width="30%" valign="top">CRTN No(s):
												</td>
												<td>
													<s:property escapeHtml="false" value="query.trial.crtnNumber" />
												</td>
											</tr>
											<tr>
												<td width="30%" valign="top">Patient No(s):
												</td>
												<td>
													<s:property escapeHtml="false" value="query.trial.patientNumber" />
												</td>
											</tr>
											<tr>
												<td width="30%" valign="top">Case Selection:
												</td>
												<td>
													<s:property escapeHtml="false" value="query.trial.caseSelection" />
												</td>
											</tr>
											<tr>
											<td width="30%" valign="top">Output Format:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.trial.ctrlOformat.ctrlOformat" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="top">Special requests:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.oformatSpecialRequest" />
											</td>
										</tr>
											<tr>
					                			<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
											</tr>
											<tr>
					                			<td width="100%" colspan="2" background="img/line.gif"></td>
											</tr>
					              			<tr>
					                			<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
											</tr>
										</table>
                                    </s:if>

                                    <s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@MANUFACTURING}">
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td width="30%">Batch No:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.mnftBatchNumber" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
                                            </tr>
                                            <tr>
                                                <td width="100%" colspan="2" background="img/line.gif"></td>
                                            </tr>
                                            <tr>
                                                <td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
                                            </tr>
                                        </table>
                                    </s:if>

                                    <s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@PREGNANCY}">
                                	<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Pregnancy Information</font></td>
										</tr>
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td width="30%" valign="false">Type of exposure:
											</td>
											<td width="70%">
												<s:property escapeHtml="false" value="query.pregnancy.prgyExptype.exposureType" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="false">Timing of exposure in pregnancy:
											</td>
											<td width="70%">
												<s:property escapeHtml="false" value="query.pregnancy.timeInPregnancy" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="false">Pregnancy/Foetal outcome:
											</td>
											<td width="70%">
												<s:property escapeHtml="false" value="query.pregnancy.outcome" />
											</td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2" background="img/line.gif"></td>
										</tr>
				              			<tr>
				                			<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
										</tr>
									</table>
                                    </s:if>

        							<s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@DRUG_INTERACTION
                                    				|| query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@DRUG_EVENT
                                    				|| query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@PREGNANCY
                                    				|| query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@CASE_CLARIFICATION
                                    				|| query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@CASE_DATA_REQUEST
                                    				|| query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@MANUFACTURING}">
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td valign="top" colspan="3"><font size="+1">Case Details</font></td>
										</tr>
										<tr>
											<td height="10" colspan="3"><img src="img/spacer.gif" height="10"></td>
										</tr>
										<tr>
											<td width="30%">AER</td>
											<td width="30%">Local Ref Number</td>
											<td width="30%">External Ref Number</td>
										</tr>
										<s:iterator value="query.cases">
											<tr>
												<td width="30%">&nbsp;<s:property escapeHtml="false" value="aerNumber" /><bean:write name="casesList" property="aerNumber"/></td>
												<td width="30%">&nbsp;<s:property escapeHtml="false" value="localReferenceNumber" /></td>
												<td width="30%"><s:property escapeHtml="false" value="externalReferenceNumber" /></td>
												<td>&nbsp;</td>
											</tr>
										</s:iterator>
									</table>



									<table cellspacing="0" cellpadding="1" border="0" width="100%">
										<tr>
											<td width="30%" valign="top">Additional comments:</td>
											<td width="70%">
												<s:property escapeHtml="false"  value="query.caseComment" />
											</td>
										</tr>
                                        <s:if test="query.pregnancyEvent || query.manufacturing || query.case">
                                        <tr>
											<td width="30%" valign="false">Output Format:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.caseOformat.caseOformat" />
											</td>
										</tr>
										<tr>
											<td width="30%" valign="false">'Others' specification:
											</td>
											<td>
												<s:property escapeHtml="false" value="query.oformatSpecialRequest" />
											</td>
										</tr>
                                        </s:if>
										<tr>
				                			<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
										</tr>
										<tr>
				                			<td width="100%" colspan="2" background="img/line.gif"></td>
										</tr>
				              			<tr>
				                			<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
										</tr>
									</table>
                                    </s:if>

									<s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@IND_UPDATES}">

										<table cellspacing="0" cellpadding="1" border="0" width="100%">
											<tr>
												<td width="30%">IND Number:
												</td>
												<td width="70%">
													<s:property value="query.indNumber"/>
												</td>
											</tr>
											<tr>
					                			<td width="100%" colspan="2"><img height=10 src="img/spacer.gif" border="0"></td>
											</tr>
											<tr>
					                			<td width="100%" colspan="2" background="img/line.gif"></td>
											</tr>
					              			<tr>
					                			<td width="100%" colspan="2"><img height=2 src="img/spacer.gif" border="0"></td>
											</tr>
										</table>
									</s:if>
									<s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@PERFORMANCE_METRICS2}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Query Description</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Metrics required:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.performanceMetric.requestDescription"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Data Output</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Output Format:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="query.performanceMetric.outputFormat.ctrlOformat"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Special Requests:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="query.performanceMetric.specialRequests"/>
                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@EXTERNAL_AUDIT_INSPECTION}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="2"><font size="+1">Description of Query</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="2"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Your question:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.queryDescription.natureOfCase"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Other Pertinent Info:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.queryDescription.otherInfo"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <tr>
                                                <td valign="top" colspan="2"><font size="+1">Data Metrics</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="2"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">All Cases:
                                                </td>
                                                <td>
                                                	<s:if test="query.audit.allCases">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@YES"/>
                                                   	</s:if>
                                                   	<s:elseif test="query.audit.allCases == false">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@NO"/>
                                                   	</s:elseif>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">All Studies:
                                                </td>
                                                <td>
                                                	<s:if test="query.audit.allStudies">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@YES"/>
                                                   	</s:if>
                                                   	<s:elseif test="query.audit.allStudies == false">
                                                    	No, only for Study/Protocol No. <s:property  escapeHtml="false" value="query.audit.studyNo"/>
                                                   	</s:elseif>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">All Spontaneous Cases:
                                                </td>
                                                <td>
                                                	<s:if test="query.audit.allSpontaneousCases">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@YES"/>
                                                   	</s:if>
                                                   	<s:elseif test="query.audit.allSpontaneousCases == false">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@NO"/>
                                                   	</s:elseif>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <tr>
                                                <td valign="top" colspan="2"><font size="+1">Data Selection</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="2"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">All Submission Timelines:
                                                </td>
                                                <td>
                                                	<s:if test="query.audit.allSubm">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@YES"/>
                                                   	</s:if>
                                                   	<s:if test="query.audit.allSubm == false">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@NO"/>
                                                   	</s:if>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">SUSAR Submission Timelines:
                                                </td>
                                                <td>
                                                	<s:if test="query.audit.susarSubm">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@YES"/>
                                                   	</s:if>
                                                   	<s:elseif test="query.audit.susarSubm == false">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@NO"/>
                                                   	</s:elseif>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Serious Cases Timelines:
                                                </td>
                                                <td>
                                                	<s:if test="query.audit.seriousCases">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@YES"/>
                                                   	</s:if>
                                                   	<s:elseif test="query.audit.seriousCases == false">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@NO"/>
                                                   	</s:elseif>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Non-serious Cases Timelines:
                                                </td>
                                                <td>
                                                	<s:if test="query.audit.nonSeriousCases">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@YES"/>
                                                   	</s:if>
                                                   	<s:elseif test="query.audit.nonSeriousCases == false">
                                                    	<s:property escapeHtml="false" value="@com.roche.dss.qtt.query.web.action.ActionConstants@NO"/>
                                                   	</s:elseif>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Submission Titles:
                                                </td>
                                                <td width="70%">
                                                   	<s:property escapeHtml="false" value="query.audit.submissionTitles"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <tr>
                                                <td valign="top" colspan="2"><font size="+1">Data Output</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="2"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Output Format:
                                                </td>
                                                <td>
                                                   	<s:property escapeHtml="false" value="query.trial.ctrlOformat.ctrlOformat"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Special Requests:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="query.oformatSpecialRequest"/>
                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@MANUFACTURING_RECALL}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Description of Query</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Your question:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.queryDescription.natureOfCase"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
                                               <tr>
                                                <td valign="top" colspan="3"><font size="+1">Manufacturing Details</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">&nbsp;</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.manufacturing.type.label"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Batch No(s).:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.manufacturing.batchNo"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Trackwise No(s).:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.manufacturing.trackwiseNo"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Associated Adverse Event(s)</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Reported Adverse Event(s):
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.aeTerm.description"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Data Output</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Output Format:
                                                </td>
	                                                <td>
	                                                	<s:if test="%{query.trial.ctrlOformat.ctrlOformatId==@com.roche.dss.qtt.model.CtrlOformat@OUTPUT_FORMAT_OTHER}">
	                                                    	<s:property escapeHtml="false" value="query.oformatSpecialRequest"/>
	                                                    </s:if>
	                                                    <s:else>
	                                                    	<s:property escapeHtml="false" value="query.trial.ctrlOformat.ctrlOformat"/>
	                                                    </s:else>
	                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Additional Info:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="query.queryDescription.otherInfo"/>
                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@MEDICAL}">
                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Query Description</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Your question:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.queryDescription.natureOfCase"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
                                               <tr>
                                                <td valign="top" colspan="3"><font size="+1">Adverse Event</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Index Case:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.queryDescription.indexCase"/>
                                                </td>
                                            </tr>
                                            <s:iterator value="query.cases" status="status">
                                            	<s:if test="%{#status.first}">
		                                            <tr>
		                                                <td width="30%" valign="top">AER No.:</td>
		                                                <td width="70%">
		                                                    <s:property escapeHtml="false" value="aerNumber"/>
		                                                </td>
		                                            </tr>
		                                            <tr>
		                                                <td width="30%" valign="top">LRN:</td>
		                                                <td width="70%">
		                                                    <s:property escapeHtml="false" value="localReferenceNumber"/>
		                                                </td>
		                                            </tr>
	                                            </s:if>
                                            </s:iterator>
                                            <tr>
                                                <td width="30%" valign="top">Signs & Symptoms:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.queryDescription.signAndSymptom"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Investigations:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.queryDescription.investigation"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Past Medical History:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.queryDescription.prevMedicalHistory"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Comedications:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.queryDescription.conmed"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Other Pertinent Info:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.queryDescription.otherInfo"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Diagnosis/Confounders:</td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.queryDescription.diagnosis"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <s:if test="query.pregnancy != null">
		                                        <TR>
		                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
		                                        </TR>
		                                        <TR>
		                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
		                                                                              border="0"></TD>
		                                        </TR>
	                                            <tr>
	                                                <td valign="top" colspan="3"><font size="+1">Pregnancy</font>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
	                                                                                 height="10"></TD>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Type of Exposure:
	                                                </td>
	                                                <td width="70%">
	                                                    <s:property escapeHtml="false" value="query.pregnancy.prgyExptype.exposureType"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Timing of Exposure in Pregnancy:
	                                                </td>
	                                                <td width="70%">
	                                                    <s:property escapeHtml="false" value="query.pregnancy.timeInPregnancy"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Pregnancy / Foetal Outcome:
	                                                </td>
	                                                <td width="70%">
	                                                    <s:property escapeHtml="false" value="query.pregnancy.outcome"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Comments:
	                                                </td>
	                                                <td width="70%">
	                                                    <s:property escapeHtml="false" value="query.pregnancy.comments"/>
	                                                </td>
	                                            </tr>
		                                        <TR>
		                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
		                                                                              border="0"></TD>
		                                        </TR>
		                                    </s:if>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Drug Interaction</font>
                                                </td>
                                            </tr>
		                                    <s:if test="query.drugs.size > 1 || query.drugs.size == 1 && query.drugs[0].firstFlag == 'N'">
	                                            <s:iterator value="query.drugs" status="status">
	                                            	<s:if test="firstFlag == 'N'">
			                                            <tr>
			                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
			                                                                                 height="10"></TD>
			                                            </tr>
			                                            <tr>
			                                                <td width="30%" valign="top">Drug Type:
			                                                </td>
			                                                <td>
			                                                	<s:if test="rocheDrugFlag == 'Y'">Roche</s:if>
			                                                	<s:else>Non-Roche</s:else>
			                                                </td>
			                                            </tr>
			                                            <tr>
			                                                <td width="30%" valign="top">Generic Name:
			                                                </td>
			                                                <td>
			                                                    <s:property escapeHtml="false" value="innGenericName"/>
			                                                </td>
			                                            </tr>
			                                            <tr>
			                                                <td width="30%" valign="top">Drug Retrieval Name:
			                                                </td>
			                                                <td>
		                                                    	<s:property escapeHtml="false" value="drugRetrievalName"/>
			                                                </td>
			                                            </tr>
			                                            <tr>
			                                                <td width="30%" valign="top">Route:
			                                                </td>
			                                                <td>
			                                                    <s:property escapeHtml="false" value="route"/>
			                                                </td>
			                                            </tr>
			                                            <tr>
			                                                <td width="30%" valign="top">Dose:
			                                                </td>
			                                                <td>
			                                                    <s:property escapeHtml="false" value="dose"/>
			                                                </td>
			                                            </tr>
			                                            <tr>
			                                                <td width="30%" valign="top">Formulation:
			                                                </td>
			                                                <td>
			                                                    <s:property escapeHtml="false" value="formulation"/>
			                                                </td>
			                                            </tr>
			                                            <TR>
			                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
			                                                                                  border="0"></TD>
			                                            </TR>
			                                        </s:if>
	                                            </s:iterator>
	                                        </s:if>
                                            <tr>
                                                <td width="30%" valign="top">Comment:
                                                </td>
                                                <td>
                                                    <s:property escapeHtml="false" value="query.caseComment"/>
                                                </td>
                                            </tr>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>
									<s:if test="%{query.queryType.queryTypeCode==@com.roche.dss.qtt.model.QueryType$Code@DATA_SEARCH}">

                                        <table cellspacing="0" cellpadding="1" border="0" width="100%">
                                            <tr>
                                                <td valign="top" colspan="3"><font size="+1">Type of Search</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
                                                                                 height="10"></TD>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">Request:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.dataSearch.requestType.label"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" valign="top">For:
                                                </td>
                                                <td width="70%">
                                                    <s:property escapeHtml="false" value="query.dataSearch.searchType.label"/>
                                                </td>
                                            </tr>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2" background="img/line.gif"></TD>
	                                        </TR>
	                                        <TR>
	                                            <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
	                                                                              border="0"></TD>
	                                        </TR>
	                                        <s:if test="%{query.dataSearch.searchType==@com.roche.dss.qtt.model.DataSearch$SearchType@AUTOMATED_QUERY}">
	                                        
	                                            <tr>
	                                                <td valign="top" colspan="3"><font size="+1">Automated Query</font>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
	                                                                                 height="10"></TD>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Specification:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="query.dataSearch.specification"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Output Format:
	                                                </td>
	                                                <td>
	                                                	<s:if test="%{query.caseOformat.caseOformatId==@com.roche.dss.qtt.model.CaseOformat@OTHER}">
	                                                    	<s:property escapeHtml="false" value="query.oformatSpecialRequest"/>
	                                                    </s:if>
	                                                    <s:else>
	                                                    	<s:property escapeHtml="false" value="query.caseOformat.caseOformat"/>
	                                                    </s:else>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">BO Report:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="query.dataSearch.boReport"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Report Parameters:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="query.dataSearch.reportParameters"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Frequency:
	                                                </td>
	                                                <td>
	                                                	<s:if test="query.dataSearch.frequency == @com.roche.dss.qtt.model.DataSearch$Frequency@OTHER">
	                                                    	<s:property escapeHtml="false" value="query.dataSearch.frequencyOther"/>
	                                                	</s:if>
	                                                	<s:else>
	                                                    	<s:property escapeHtml="false" value="query.dataSearch.frequency.label"/>
	                                                    </s:else>
	                                                </td>
	                                            </tr>
	                                            
	                                        </s:if>
	                                        <s:if test="%{query.dataSearch.searchType==@com.roche.dss.qtt.model.DataSearch$SearchType@SIGNAL_DETECTION}">
	                                        
	                                            <tr>
	                                                <td valign="top" colspan="3"><font size="+1">Data Search - Signal Detection / Drug Safety Reports</font>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
	                                                                                 height="10"></TD>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">MedDRA Preferred Term(s):
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="query.dataSearch.meddraPreferredTerm"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Additional Comments:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="query.caseComment"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Output Format:
	                                                </td>
	                                                <td>
	                                                	<s:if test="%{query.caseOformat.caseOformatId==@com.roche.dss.qtt.model.CaseOformat@OTHER}">
	                                                    	<s:property escapeHtml="false" value="query.oformatSpecialRequest"/>
	                                                    </s:if>
	                                                    <s:else>
	                                                    	<s:property escapeHtml="false" value="query.caseOformat.caseOformat"/>
	                                                    </s:else>
	                                                </td>
	                                            </tr>
	                                            
	                                        </s:if>
	                                        <s:if test="%{query.dataSearch.searchType==@com.roche.dss.qtt.model.DataSearch$SearchType@ASIME}">
	                                        
	                                            <tr>
	                                                <td valign="top" colspan="3"><font size="+1">Data Search - ASIMEs </font>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
	                                                                                 height="10"></TD>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">MedDRA Preferred Term(s):
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="query.dataSearch.meddraPreferredTerm"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Index Case (AER No.):
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="query.queryDescription.indexCase"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Additional Comments:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="query.caseComment"/>
	                                                </td>
	                                            </tr>
	                                            
	                                        </s:if>
	                                        <s:if test="%{query.dataSearch.searchType==@com.roche.dss.qtt.model.DataSearch$SearchType@SAE_RECONCILIATIONS}">
	                                        
	                                            <tr>
	                                                <td valign="top" colspan="3"><font size="+1">Data Search - SAE Reconciliations or Internal Audit</font>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
	                                                                                 height="10"></TD>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Protocol/Study No.:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="query.trial.protocolNumber"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">CRTN/Country/Investigator(s):
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="query.trial.crtnNumber"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Patient No.(s):
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="query.trial.patientNumber"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Case Selection:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="query.trial.caseSelection"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Output Format:
	                                                </td>
	                                                <td>
	                                                	<s:if test="%{query.caseOformat.caseOformatId==@com.roche.dss.qtt.model.CaseOformat@OTHER}">
	                                                    	<s:property escapeHtml="false" value="query.oformatSpecialRequest"/>
	                                                    </s:if>
	                                                    <s:else>
	                                                    	<s:property escapeHtml="false" value="query.caseOformat.caseOformat"/>
	                                                    </s:else>
	                                                </td>
	                                            </tr>
	                                            
	                                        </s:if>
	                                        <s:if test="%{query.dataSearch.searchType==@com.roche.dss.qtt.model.DataSearch$SearchType@OTHER}">
	                                        
	                                            <tr>
	                                                <td valign="top" colspan="3"><font size="+1">Data Search - Other</font>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
	                                                                                 height="10"></TD>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Your Question:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="query.queryDescription.natureOfCase"/>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Output Format:
	                                                </td>
	                                                <td>
	                                                	<s:if test="%{query.caseOformat.caseOformatId==@com.roche.dss.qtt.model.CaseOformat@OTHER}">
	                                                    	<s:property escapeHtml="false" value="query.oformatSpecialRequest"/>
	                                                    </s:if>
	                                                    <s:else>
	                                                    	<s:property escapeHtml="false" value="query.caseOformat.caseOformat"/>
	                                                    </s:else>
	                                                </td>
	                                            </tr>
	                                            
	                                        </s:if>
	                                        <s:if test="%{query.dataSearch.searchType==@com.roche.dss.qtt.model.DataSearch$SearchType@LITERATURE_SEARCH}">
	                                        
	                                            <tr>
	                                                <td valign="top" colspan="3"><font size="+1">Literature Search</font>
	                                                </td>
	                                            </tr>
	                                            <tr>
	                                                <TD height="10" colspan="3"><img alt="" src="img/spacer.gif"
	                                                                                 height="10"></TD>
	                                            </tr>
	                                            <tr>
	                                                <td width="30%" valign="top">Your Question:
	                                                </td>
	                                                <td>
	                                                    <s:property escapeHtml="false" value="query.queryDescription.natureOfCase"/>
	                                                </td>
	                                            </tr>
	                                            
	                                        </s:if>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=10 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2" background="img/line.gif"></TD>
                                            </TR>
                                            <TR>
                                                <TD width="100%" colspan="2"><img alt="" height=2 src="img/spacer.gif"
                                                                                  border="0"></TD>
                                            </TR>
                                        </table>
                                    </s:if>


								</p></span></div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></td>
		</tr>
  		<tr>
			<td width="100%" height=20></td>
		</tr>
	</tbody>
</table>

<s:form theme="simple">
<!-- TODO make sure all actions below are assigned and executable-->
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<s:if test="previewMode=='ALL'">
	<tr>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
			<logic:equal name="query" property="haLegalNoDoc" value="true">
				<td align="center"><input type="button" value="     Submit     " onclick="doQRFSubmitNoDoc();"/></td>
			</logic:equal>
			<logic:equal name="query" property="haLegalNoDoc" value="false">
				<td align="center"><input type="button"  value="     Submit     " onclick="doQRFSubmit();"/></td>
			</logic:equal>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td align="center"><input type="button"  value="Amend Query" onclick="doQRFAmend();"/></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<%--<td align="center"><input type="button"  value=" Save Draft " onclick="doQRFSave();"/></td>--%>
		<td height="20" width="20" ><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<s:if test="query.deleteAllowed">
			<td align="center"><input type="button"  value="Delete Query" onclick="doQRFDelete();"/></td>
			<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		</s:if>
		<td align="center"><input type="button"  property="qrfPrint" value="       Print       " onclick="window.print();"/></td>
	</tr>
</s:if>
<s:if test="previewMode=='ADP'">
	<tr>
		<td align="center"><input type="button" value="Amend Query" onclick="doQRFAmend();"/></td>
		<td height="20" width="20" ><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<s:if test="query.deleteAllowed" >
			<td align="center"><input type="button" value="Delete Query" onclick="doQRFDelete();"/></td>
			<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		</s:if>
		<td align="center"><input type="button" value="Print" onclick="window.print();"/></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
	</tr>
</s:if>
<s:if test="previewMode=='AADP'">
	<tr>
		<td align="center"><input type="button" value="Amend Query" onclick="confirmationDialog('dsclAmnd');"/></td>
		<td height="20" width="20" ><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<s:if test="query.deleteAllowed" >
			<td align="center"><input type="button" value="Delete Query" onclick="confirmationDialog('dsclDelete');"/></td>
			<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		</s:if>
		<td align="center"><input type="button" value="       Print       " onclick="window.print();"/></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
	</tr>
</s:if>
<s:if test="previewMode=='PRNT'">
	<tr>
		<td height="20" width="20" ><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20" ><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20" ><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20" ><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td align="center"><input type="button" value="       Print       " onclick="window.print();"/></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20" ><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		<td height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
	</tr>
</s:if>
<s:if test="%{previewMode==@com.roche.dss.qtt.query.web.action.ActionConstants@PREVIEW_MODE_DUPLCHK_ACC_REJECT_AMNDTYP}">
	<tr>
		<TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
		<TD height="20" width="20" ><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
		<td align="center">
        <input type="button" name="accept" value="Accept Query" onclick="doAccept()">
		</td>
		<TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
		<s:if test="query.rejectAllowed" >
			<td align="center"><input type="button" name="reject" value="Reject Query" onclick="doReject()"></td>
			<TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
		</s:if>
		<TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
		<TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
	</tr>
</s:if>

<s:if test="%{previewMode==@com.roche.dss.qtt.query.web.action.ActionConstants@PREVIEW_MODE_PRNT_CLOSE}">
	<tr>
	<script language="JavaScript">
	<!--
	if (window.print)
    	document.write('<form><td align="center" width="100%"><input type="button" value="Print" onClick="window.print()"></td></form>');
	//-->
	</script>

		<td align="center" width="100%"><input type="button" name="close" value="Close" onclick="window.close();"></td>
		<TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
	</tr>
	<tr>
		<TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
	</tr>
</s:if>



	<tr>
		<td height="20" width="20" colspan="10"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
	</tr>
</table>

<s:hidden name="id" value="%{query.querySeq}" />

<!-- END MAIN BODY SPACE -->
<s:if test="differentRequester">
    <input type="hidden" name="activity" value="keepDoc">
</s:if>
<s:else>
    <!-- <input type="hidden" name="activity" value="%{activity}"> -->
    <s:hidden name="activity"/> 
</s:else>

</s:form>

</body>
</html>