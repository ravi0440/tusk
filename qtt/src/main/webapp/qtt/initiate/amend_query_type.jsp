<%@ taglib prefix="s" uri="/struts-tags" %>
<HTML>
<HEAD><TITLE>Amend Query Type</TITLE>
    <LINK href="css/roche.css" type=text/css rel=stylesheet>
    <SCRIPT language="javascript" src="js/image_script.js"></script>
    <script language="javascript">
        function doSubmit() {
            if (confirm("Details associated with the original query type are about to be deleted.\n Please click 'OK' if you wish to proceed.")) {
                document.forms[0].action = "AmendQueryType.action";
                document.forms[0].target = '_self';
                document.forms[0].submit();
            } else
                return;
        }

    </script>
</HEAD>
<BODY onLoad="preload()">

<s:form action="AmendQueryType" theme="simple" method="POST">
    <!-- START MAIN BODY SPACE ALL PAGE CONTENTS GO HERE -->
   <s:hidden value="id" name="id" />
    <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TBODY>
        <TR>
            <TD width="100%">
                <TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TBODY>
                    <TR>
                        <TD class=TitleExpanded
                            style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid"
                            vAlign=center align=left width="100%">
                            <DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>Amend Query Type</P></SPAN></DIV>
                        </TD>
                    </TR>
                    <tr>
                        <td>
                            <s:if test="hasActionErrors()">
                                <table style="border: 1px solid rgb(40, 87, 139); padding:3px; width:100%">
                                    <tr>
                                        <td>
                                            <span class="ErrorHeader"> Validation errors detected!  </span>
                                            <br/>
                                            <s:fielderror cssStyle="margin-bottom:0"/>
                                            <s:actionerror cssStyle="margin-top:0"/>
                                            <span class="ErrorHeader"> Please correct these errors and try again.</span>
                                        </td>
                                    </tr>
                                </table>
                            </s:if>
                        </td>
                    </tr>
                    <TR>
                        <TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid"
                            colSpan=2 height="100%">
                            <DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%"
                                 align=justify><SPAN>
								<br>
									Please note amending the query type will delete all the details associated with the original query type.
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
                                        <br>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <br>
                                        <tr>
                                            <td width="30%">
                                                # Query Type:
                                            </td>
                                            <td width="70%">
                                                <s:select name="queryType" list="qTypes" headerKey="0"
                                                             listKey="queryTypeCode" listValue="queryType"
                                                             headerValue="">
                                                </s:select>
                                            </td>
                                        </tr>
                                    </table>
								</SPAN></DIV>
                        </TD>
                    </TR>
                    </TBODY>
                </TABLE>
            </TD>
            <TD width="20"><img src="img/spacer.gif" width="20"></TD>
        </TR>
        <TR>
            <TD width="100%" height=20></TD>
        </TR>
        </TBODY>
    </TABLE>

    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
            <td align="center" width="100%"><input type="button" name="save" value="Save" onclick="doSubmit()"></td>
            <TD height="20" width="20"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
            <td align="center"><input type="button" name="close" value="Close" onclick="window.close();"></td>
            <TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
        </tr>
    </table>

    <!-- END MAIN BODY SPACE -->
</s:form>
</BODY>
</HTML>
