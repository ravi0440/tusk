<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>

<script language="javascript" src="js/image_script.js"></script>
<script language="javascript" src="js/global_script.js"></script>
<script language="javascript" src="js/initiate_script.js"></script>

<form action="QueryReview.action" method="POST">

<table cellSpacing=0 cellPadding=0 width="100%" border=0>
	<tbody>
  		<tr>
			<td width="30%" valign="top">Name: <s:property value="loggedUserName"/></td>
			<td width="30%" valign="top">Date: <s:date name="todaysDate" format="dd-MMM-yyyy" /></td>
			<td width="40%" valign="top">Number of Queries: <s:property value="initiateListSize" /></td>
		</tr>
  		<tr>
			<td vAlign=top colSpan=3><img height=16 src="img/spacer.gif" width=1></td>
		</tr>
	</tbody>
</table>

<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<tbody>
  		<tr>
  			<td width="100%">
  				<table height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<tbody>
						<tr>
							<td class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" align=left width="100%">
								<div style="WIDTH: 100%; HEIGHT: 100%">
									<span>New Queries</span>
								</div>
							</td>
						</tr>				
						<tr>
							<td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<table>
									<tr>
										<td height="9" colspan="3"><img src="img/spacer.gif" height="9"></td>
									</tr>
								</table>
								<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify>
									<span>
										<s:if test="initiateList == null || initiateListSize == 0">	
											<table width="100%">
												<tr>
													<td style="BORDER-TOP: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
														<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify>
															<span>
																<br>No Queries found for Initiation
															</span>
														</div>
													</td>
												</tr>
											</table>
										</s:if>	
										<s:else>			
											<display:table name="initiateList" requestURI="InitiateList.action" class="DataGridHeaderPlus" cellspacing="1" cellpadding="5" >																				
												<display:column headerClass="DataGridItemHeaderPlus" property="querySeq" title="Select" decorator="com.roche.dss.qtt.query.web.displaybean.RadioButtonDecorator" class="DataGridItemPlus" />
												<display:column headerClass="DataGridItemHeaderPlus" property="requester.name" title="Requester Name" class="DataGridItemPlus" />
												<display:column headerClass="DataGridItemHeaderPlus" property="requester.organisation.name" title="Requester Location" class="DataGridItemPlus" />
												<display:column headerClass="DataGridItemHeaderPlus" property="datetimeSubmittedFormatted" title="Date/Time Received" class="DataGridItemPlus" />													
											</display:table>
										</s:else>
									</span>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="20"><img src="img/spacer.gif" width="20"></TD>
		</tr>
  		<tr>
			<td width="100%" height=20></td>
		</tr>
  	</tbody>
</table>

<s:hidden name="viewtype" value="initiatePreview" />
<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<s:if test="initiateList != null && initiateListSize != 0">	
			<td align="center" width="50%"><input type="button" name="open" value="Open query" onclick="doInitiateSubmit();"></td>
			<td height="20" width="20" ><img src="img/spacer.gif" height="20" width="20" border="0"></td>
		</s:if>
		<td align="center"><input type="button" name="refresh" value="Refresh" onclick="doRefresh();"></td>
		<td height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
	</tr>
	<tr>
		<td height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></td>
	</tr>
</table>

<input type="hidden" name="queryId">

</form>

