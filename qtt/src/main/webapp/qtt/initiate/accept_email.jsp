<%@ taglib prefix="html" uri="/struts-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<HTML>
<HEAD>
    <TITLE><request:attribute name="title"/></TITLE>
    <LINK href="css/roche.css" type=text/css rel=stylesheet>
    <SCRIPT language="javascript" src="js/image_script.js"></script>
    <SCRIPT language="javascript" src="js/global_script.js"></script>
    <script language="javascript">
    function addRecipient() {
    	var table = document.getElementById("formTable");
    	var newRow = table.insertRow(table.rows.length);
    	var cell1 = newRow.insertCell(0);
    	var cell2 = newRow.insertCell(1);
    	var cell3 = newRow.insertCell(2);
    	cell2.innerHTML = "<b><input type=\"text\" maxlength=\"50\" size=\"110\" name=\"emailRecipients\"/></b>";
    }
    </script>
</HEAD>
<BODY onLoad="preload()">
<html:form action="AcceptQuery.action" theme="simple" method="POST" acceptcharset="utf-8">
    <!-- START MAIN BODY SPACE ALL PAGE CONTENTS GO HERE -->
	<input type="hidden" name="iehack" value="&#9760;"/>
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <TBODY>
        <s:actionerror/>
        <tr>
            <td width="100%">
                <table height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <TBODY>
                    <tr>
                        <td class="TitleExpanded"
                            style="BORDER-RIGHT: #28578b 1px solid; BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid"
                            valign="center" align="left" width="100%">
                            <div style="WIDTH: 100%; HEIGHT: 100%">
                                <p><bean:write name="data" property="subject"/></p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid"
                            colspan="2" height="100%">
                            <div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%"
                                 align="left">
                                <table cellspacing="0" cellpadding="1" width="100%" border="0">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                                <table id="formTable" cellspacing="0" cellpadding="1" width="100%" border="0">
                                	<s:if test="emailRecipients.empty">
	                                    <tr>
	                                        <td>To:</td>
	                                        <td>
	                                        	<b>
	                                        		<input type="text" maxlength="50" size="110" name="emailRecipients" value="<s:property value="to"/>"/>
	                                        	</b>
	                                        </td>
	                                        <td width="100%" align="left">
		                                    	<a href="javascript:addRecipient();">
		                                    		<img name="img01" src="img/add_off.gif" alt="Add recipient" border="0" onmouseover="hilight('img01')" onmouseout="lolight('img01')"/>
		                                    	</a>
	                                        </td>
	                                    </tr>
	                                </s:if>
	                               	<s:iterator value="emailRecipients" var="to" status="status">
	                                    <tr>
	                                        <td><s:if test="%{#status.first}">To:</s:if></td>
    	                                    <td>
        	                                	<b>
            	                            		<input type="text" maxlength="50" size="110" name="emailRecipients" value="<s:property value="to"/>"/>
                	                        	</b>
                    	                    </td>
                    	                    <td width="100%" align="left">
			                                    <s:if test="%{#status.first}">
			                                    	<a href="javascript:addRecipient();">
			                                    		<img name="img01" src="img/add_off.gif" alt="Add recipient" border="0" onmouseover="hilight('img01')" onmouseout="lolight('img01')"/>
			                                    	</a>
			                                    </s:if>
                    	                    </td>
                        	            </tr>
                                    </s:iterator>
                                </table>
                            </div>
                        </td>
                    </tr>
                    </TBODY>
                </table>
                <table cellspacing="0" cellpadding="0" border="0" align="center">
                    <tr>
                        <td align="center">
                            <html:submit onclick="disableButtonsSubmit(document.forms[0]);document.forms[0].submit();"/>
                        </td>
                    </tr>
                </table>
            </td>
            <TD width="20"><img src="img/spacer.gif" width="20"></TD>
        </tr>
        </TBODY>
    </table>
    <s:hidden name="id" />
</html:form>
</BODY>
</HTML>
