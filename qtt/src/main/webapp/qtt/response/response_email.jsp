<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
  <script language="javascript">
	function doIgnore() {
		document.forms[0].ignoreEmail.value='yes';
		document.forms[0].action='ProcessResponse.action';
		document.forms[0].target='_self';
		document.forms[0].submit();
	}

	function doSubmit() {
		document.forms[0].ignoreEmail.value='';
		document.forms[0].action='ProcessResponse.action';
		document.forms[0].target='_self';
		document.forms[0].submit();
	}
	
	function goHome() {
		document.forms[0].ignoreEmail.value='yes';
		document.forms[0].action='RetrieveQueryTaskList.action';
		document.forms[0].target='_self';
		document.forms[0].submit();
	}
</script>
</head>
<body>                                                      

<s:form action="ProcessResponse" theme="simple" onsubmit="return false;">
	<s:hidden name="queryId"/>
	<s:hidden name="re"/>
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tbody>
        <tr>
            <td width="100%">
                <table height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tbody>
						<s:if test="hasActionErrors() || hasFieldErrors()">
			              	<%@ include file="../global/validation_error.jsp" %>
						</s:if>
                    <tr>
                        <td class="TitleExpanded"
                            style="BORDER-RIGHT: #28578b 1px solid; BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid"
                            valign="center" align="left" width="100%">
                            <div style="WIDTH: 100%; HEIGHT: 100%">
                                <p><s:property value="email.subject" /></p>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid"
                            colspan="2" height="100%">
                            <div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%"
                                 align="left">
                                <table cellspacing="0" cellpadding="1" width="100%" border="0">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>

                                <table cellspacing="0" cellpadding="1" width="100%" border="0">
                                    <tr>
                                        <td># To:</td>
                                        <td width="90%" colspan="2"><b><s:textfield name="email.to" maxlength="50" size="110"/></b></td>
                                    </tr>

                                    <tr>
                                        <td>CC:</td>
                                        <td width="90%" colspan="2"><s:textfield name="email.cc" maxlength="50" size="110"/></td>
                                    </tr>

                                    <tr>
                                        <td># Subject:</td>
                                        <td width="90%" colspan="2"><s:textfield name="email.subject"  maxlength="500" size="110"/></td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td width="90%" colspan="2">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td width="90%" colspan="2"><s:textarea name="email.body" rows="12" cols="86"/></td>
                                    </tr>
                                </table>


                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <table cellspacing="0" cellpadding="0" border="0" align="center">
                    <tr>
                        <td align="center">&nbsp;<input type=hidden name=ignoreEmail value=""></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <s:submit value="Send Email" onclick="doSubmit()"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <s:if test="! (\"y\".equals(re))">
                            	<s:submit type="button" value="Ignore Email" onclick="doIgnore()"/>
                            </s:if>
                            <s:else>
                            	<s:if test="hasActionErrors() || hasFieldErrors()">
                            		<s:submit type="button" value="Ignore Email" onclick="goHome()"/>
                            	</s:if>
                            </s:else>
                        </td>
                    </tr>
                </table>


                <table cellspacing="0" cellpadding="1" border="0" width="100%">
                    <tr>
                        <td height="20" colspan="2"><img src="img/spacer.gif" height="20"></td>
                    </tr>
                    <tr>
                        <td width="50%"><b>Click <u>
                        <s:if test="#parameters.sourceSearch[0] == 'repositorySearch'">
                            <a href="PerformRepositorySearch.action" class="Anchor">
                        </s:if>
                        <s:else>
                            <a href="QuickSearchResults.action" class="Anchor">
                        </s:else>
                        here</a></u> to return to search results...</b></td>
                    </tr>
                    <tr>
                        <td height="20" colspan="2"><img src="img/spacer.gif" height="20"></td>
                    </tr>
                    <tr>
                        <td width="100%"><b>Click <a href="RepositorySearch.action" class="Anchor">here</a> to specify new search criteria...</b></td>
                    </tr>
                    <tr>
                        <td height="20" colspan="2"><img src="img/spacer.gif" height="20"></td>
                    </tr>
                </table>


            </td>
            <TD width="20"><img src="img/spacer.gif" width="20"></TD>
        </tr>
        </tbody>
    </table>
</s:form>

</body>
</html>