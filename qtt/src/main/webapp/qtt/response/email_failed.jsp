<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="html" uri="/struts-tags" %>
<html>

<body>

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
  	<TBODY>
  		<TR>
			<TD width="100%">
  				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<TBODY>
						<TR>
							<TD class=TitleExpanded style="BORDER-TOP: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-RIGHT: #28578b 1px solid; BORDER-BOTTOM: #28578b 3px solid" vAlign=center align=left width="100%">
								<DIV style="WIDTH: 100%; HEIGHT: 100%"><SPAN>
								<P>The Query Response could not be completed</P></SPAN></DIV></TD>
						</TR>
						
						<TR>
							<TD style="BORDER-RIGHT: #28578b 1px solid; BORDER-LEFT: #28578b 1px solid; BORDER-BOTTOM: #28578b 1px solid" colSpan=2 height="100%">
								<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 100%; PADDING-TOP: 7px; HEIGHT: 100%" align=justify><SPAN>
								<P><br>
									The Query Response has been failed due to invalid Requester's email address. Please contact Support immediately.
									<table cellspacing="0" cellpadding="1" border="0" width="100%">
									<br>
										
									</table>

								</P></SPAN></DIV>
							</TD>
						</TR>
					</TBODY>
				</TABLE>
			</TD>
			<TD width="20"><img src="img/spacer.gif" width="20"></TD>
		</TR>
  		<TR>
			<TD width="100%" height=20></TD>
		</TR>
  	</TBODY>
</TABLE>
<!--
<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		
		<!--<td align="center"><input type="button" name="close" value="Close" onclick="doRefresh()"></td>-->
		<TD height="20" width="20" colspan="5"><img src="img/spacer.gif" height="20" width="20" border="0"></TD>
	</tr>
</table>
-->
</TD>
</TR>
</TBODY>
</TABLE>
</body>
</html>