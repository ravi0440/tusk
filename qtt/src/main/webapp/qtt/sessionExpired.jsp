<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE>Your session has expired...</TITLE>
<LINK href="css/roche.css" type=text/css rel=stylesheet>
<SCRIPT language="javascript" src="js/image_script.js"></script>
<script language="javascript" src="js/global_script.js"></script>
</HEAD>
<BODY >

<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
	<TBODY>
		<TR>
			<TD width="100%">
				<TABLE height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0 >
					<TBODY>
						<tr>
							<td>	
								<b>This may be because your browser was inactive for a long period. Please close your browser and login again.</b>								
							</td>
						</tr>
						<tr>
							<td>	
								<br><br><b>If you continue to have problems, please contact support for further assistance.</b>
							</td>
						</tr>
					</TBODY>
				</TABLE>
			</TD>
		</TR>
	</TBODY>
</TABLE>

</BODY>
</HTML>

