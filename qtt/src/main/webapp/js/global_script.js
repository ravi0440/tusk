function getModalValues(iHeight, iWidth){
	var iTop;
	var iLeft;
    var sEdge;
	var bCenter='Yes';
	var bHelp='No';
	var bResize='No';
	var bStatus='No';
	
	var sFeatures="dialogHeight: " + iHeight + "px; dialogWidth: " + iWidth + "px; dialogTop: " + iTop + "px; dialogLeft: " + iLeft + "px; edge: " + sEdge + "; center: " + bCenter + "; help: " + bHelp + "; resizable: " + bResize + "; status: " + bStatus + ";";
	return sFeatures;
}

function getSelectedRadio(buttonGroup) {
		   // returns the array number of the selected radio button or -1 if no button is selected
		   if (buttonGroup[0]) { // if the button group is an array (one button is not an array)
		      for (var i=0; i<buttonGroup.length; i++) {
		         if (buttonGroup[i].checked) {
		            return i
		         }
		      }
		   } else {
		      if (buttonGroup.checked) { return 0; } // if the one button is checked, return zero
		   }
		   // if we get to this point, no radio button is selected
		   return -1;
	} // Ends the "getSelectedRadio" function
	
function getSelectedRadioValue(buttonGroup) {
	   // returns the value of the selected radio button or "" if no button is selected
	   var i = getSelectedRadio(buttonGroup);
	   if (i == -1) {
	      return "";
	   } else {
	      if (buttonGroup[i]) { // Make sure the button group is an array (not just one button)
	         return buttonGroup[i].value;
	      } else { // The button group is just the one button, and it is checked
	         return buttonGroup.value;
	      }
	   }
	} // Ends the "getSelectedRadioValue" function
	
function sendEmail(to, subject, body) {
	     var emailBody;
	     var emailTo;
	     var emailSubject;	    
	 	
	     var emailURL; // The url that will open the user's email client     
	 
	     // Set some default values
	     emailBody = "\n\n" + body;
	     emailTo = to;
	     emailSubject = subject;	     
	 
	     emailURL = "mailto:" + escape(emailTo) + 
	 	"?subject=" + escape(emailSubject) +	 	
	 	"&body=" + escape(emailBody);
	 
	     window.location = emailURL;
	     return true;
}	

//function to restrict the max length of a textarea box
function textCounter( field, maxlimit ) {
 		
	if ( field.value.length >= maxlimit )	{
		field.value = field.value.substring( 0, maxlimit );
		alert("Maximum number of characters ("+maxlimit+") exceeded");
		return false;
	}
	return;  
}

//function to navigate to the location passed in
function go(location) {
		
	document.forms[0].action=location;
	document.forms[0].submit();
}

//function to open a qrf based on its id
function openQrf(id) {				
					
	//submit the form 
	document.forms[0].action="QueryReview.action?duplicate=true&isInitiated=true&queryId="+id;						
	window.open('', 'qrf', "height=600,width=750,resizable=yes,status=yes,scrollbars=yes,toolbar=no,menubar=no,location=no");		
	document.forms[0].target='qrf';
	document.forms[0].submit();		
}

//function to open a qrf based on its id
function openQrfNoAttachments(id) {				

		var path="QueryReview.action?viewtype=nolayout&formQueryDetail=true&withAttachs=false&queryId="+id;
		var sFeatures = getModalValues(720, 740);
		var args;
     	window.open(path, '_blank','width=720,height=740,scrollbars=yes');
}

//function to open a qrf based on its id
function openQueryFollowup(queryId, followupId) {				

		var path="FollowupQueryPreview.action?withAttachs=false&queryId="+queryId+"&&followupId="+followupId;
		var sFeatures = getModalValues(720, 740);
		var args;
     	var sReturn = window.showModalDialog(path, args, sFeatures);
}
// makes all submit and reset fields disabled to avoid double submit
function disableFormSubmit(aForm) {
    for (i=0; i< aForm.elements.length; i++) {
		t = aForm[i].type.toLowerCase();
		if (t == "submit" || t == "reset" || t == "button")
			aForm[i].disabled = true;
    }
}

// makes all submit and reset fields disabled to avoid double submit
function buttonDisableFormSubmit(aForm) {
    for (i=0; i< aForm.elements.length; i++) {
		t = aForm[i].type.toLowerCase();
		if (t == "submit" || t == "reset" || t == "button") {
			aForm[i].disabled = true;
		}
		else {
			aForm[i].disabled = false;
    	}
	}
}

//makes all form fields, and buttons readonly or disabled
function disableForm(aForm) {
    for (i=0; i< aForm.elements.length; i++) {
	    try {
			t = aForm[i].type.toLowerCase();
		    if (t=="text" || t=="textbox")
    			aForm[i].readonly = true;
	    	else
				aForm[i].disabled = true;
	    } catch (e) {}
	}
}

// makes all submit, reset, and buttons fields enabled
function enableFormSubmit(aForm) {
    for (i=0; i< aForm.elements.length; i++) {
		t = aForm[i].type.toLowerCase();
		if (t == "submit" || t == "reset" || t == "button")
			aForm[i].disabled = false;
    }
}

//helper function for disabled link
function cancelLink () {
  return false;
}

// make all links disabled
function disableAllLinks () {

    for (i=0; i< document.links.length; i++) {
	l = document.links[i];
	disableLink (l);
	}
}

// make a link disabled
function disableLink (link) {
  if (link.onclick)
    link.oldOnClick = link.onclick;
  link.onclick = cancelLink;
  if (link.style)
    link.style.cursor = 'default';
}

// make all link enabled
function enableLink (link) {
  link.onclick = link.oldOnClick ? link.oldOnClick : null;
  if (link.style)
    link.style.cursor = 
      document.all ? 'hand' : 'pointer';
}

// makes all submit and reset fields disabled to avoid double submit
function disableButtonsSubmit(aForm) {
    for (i=0; i< aForm.elements.length; i++) {
		t = aForm[i].type.toLowerCase();
		if (t == "submit" || t == "reset" || t == "button")
			aForm[i].disabled = true;
    }
}
// Controls mouseover effect on buttons
function hover(loc,cls){
			if (loc.className)
				loc.className=cls;
}

//clears all form fields
function clearForm(aForm) {
    for (i=0; i< aForm.elements.length; i++) {
	    try {
			t = aForm[i].type.toLowerCase();
			if (t == "text" || t == "textarea") {
   				aForm[i].value="";
			} else if (t == "select" || t == "select-multiple") {
				for (var j=0; j < aForm[i].options.length; j++) {
					aForm[i].options[j].selected = false;
				}
			}
	    } catch (e) {}
	}
}
