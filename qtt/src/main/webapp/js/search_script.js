
// toggles a list item (arg #1) according to value of checkbox (arg #2)
//
function enableDisable( listName, checkboxName )
{
	// obtain reference to the input elements
	list = document.getElementById( listName );
	checkbox = document.getElementById( checkboxName );
	
	// enable or disable the list as appropriate
	list.disabled = checkbox.checked;
	
	// for good measure, unselect all items
	if ( list.multiple )
	{
		list.selectedIndex = -1;
	}
	else
	{
		list.selectedIndex = 0;
	}
}


// sets all checkboxes on the page to 'checked'
function setAllCheckboxesTrue()
{
	elements = document.getElementById("repSrchCriteria").elements;
	for (i=0; i<elements.length; i++)
	{
		if (elements[i].type == "checkbox")
		{
			elements[i].checked = true;
		}
	}
}


// called on page body load, and onchange of queryType input control
// to enable/disable fields as appropriate for query type
function queryTypeChange()
{
	
	// get currently selected value
	typeList = document.getElementById( "queryTypesList" );
	currentType = typeList.options[typeList.selectedIndex].value;

	// get references to affected elements
	iet = document.getElementById( "eventTerms" );
	pmh = document.getElementById( "prevMedicalHistory" );
	idn = document.getElementById( "interactingDrugName" );	
	
	// if 'all' checkbox is selected, enable all affected elements
	if ( document.getElementById( "queryTypesCB" ).checked == true )
	{		
		iet.disabled = true;
		pmh.disabled = true;
		idn.disabled = true;
	}
	// if type ID is 1
	else if ( currentType == "1" )
	{
		iet.disabled = false;
		pmh.disabled = false;
		clearAndDisable( idn );
	}
	// if type ID is 2
	else if ( currentType == "2" )
	{
		clearAndDisable( iet ); 
		pmh.disabled = false;
		idn.disabled = false;
	}
	// if type ID is 3 or 4
	else if ( currentType == "3" || currentType == "4" )
	{
		clearAndDisable( iet );
		pmh.disabled = false;
		clearAndDisable( idn );
	}
	// if type ID is 5, 13, 14
	else if ( currentType == "5" || currentType == "13" || currentType == "14" )
	{
		iet.disabled = false;
		clearAndDisable( pmh );
		clearAndDisable( idn );
	}
	// otherwise, default catch-all
	else
	{
		clearAndDisable( iet );
		clearAndDisable( pmh );
		clearAndDisable( idn );
	}
}


// clears the value of, and disables, an input element
function clearAndDisable( element )
{
	element.value = "";
	element.disabled = true;
}


// called on page load to initialise the checkbox-list enablement
function initialise()
{
	// set up initial conditions for drugRetrievalName
	initialisePair( document.getElementById( "drugPNameList" ), document.getElementById( "drugPNameCB" ) );
	initialisePair( document.getElementById( "genrDrugList" ), document.getElementById( "genrDrugCB" ) );	
	initialisePair( document.getElementById( "indicsList" ), document.getElementById( "indicsCB" ) );
	initialisePair( document.getElementById( "queryTypesList" ), document.getElementById( "queryTypesCB" ) );
	initialisePair( document.getElementById( "authorList" ), document.getElementById( "authorCB" ) );
	initialisePair( document.getElementById( "lDocsList" ), document.getElementById( "lDocsCB" ) );
	initialisePair( document.getElementById( "reqLNameList" ), document.getElementById( "reqLNameCB" ) );
	initialisePair( document.getElementById( "reqCtryList" ), document.getElementById( "reqCtryCB" ) );
	initialisePair( document.getElementById( "repTypeList" ), document.getElementById( "repTypeCB" ) );
	initialisePair( document.getElementById( "srcCtryList" ), document.getElementById( "srcCtryCB" ) );
	
	initialisePair( document.getElementById( "lProcessList" ), document.getElementById( "lProcessCB" ) );
	initialisePair( document.getElementById( "dmgList" ), document.getElementById( "dmgCB" ) );
	initialisePair( document.getElementById( "psList" ), document.getElementById( "psCB" ) );
	initialisePair( document.getElementById( "dsclList" ), document.getElementById( "dsclCB" ) );
	
	//quick search initialization, quick_list_script.js is needed for this
	supercombo_1 = new TypeAheadCombo("drugPNameList");
	supercombo_2 = new TypeAheadCombo("reqLNameList");
	supercombo_3 = new TypeAheadCombo("reqCtryList");
	supercombo_4 = new TypeAheadCombo("srcCtryList");
	supercombo_5 = new TypeAheadCombo("genrDrugList");	
}


function initialisePair( list, chkbox )
{
	if (   ( list.multiple && list.selectedIndex == -1 ) 
	    || ( !list.multiple && list.selectedIndex == 0 ) )
	{
		chkbox.checked = true;
		list.disabled = true;
	}
	else
	{
		chkbox.checked = false;
		list.disabled = false;
		//scroll the list to the first selected item
		list.options[list.selectedIndex].selected = true;
	}
}
