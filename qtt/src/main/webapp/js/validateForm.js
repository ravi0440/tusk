function addConditions(form)
{
	var liste=new Array();
	if (typeof formRequiredFields != "undefined") 
	{
		for (var j = 0; j < formRequiredFields.length; j++){liste[j]=formRequiredFields[j];}
	}//create a list with required Fields without conditions		
	if (typeof conditionel != "undefined") 
	{
		for (var i = 0; i < conditionel.length; i=i+2)
		{
			var field=conditionel[i];
			var nbCondition=conditionel[i+1]
			addToCheck(form,field,nbCondition,liste);						
		}//add conditional fields to the list of fields to check
	}
	return liste;
}

function addToCheck(form,field,nbCondition,liste)
{
    eval("var test = form." +field);
    //ckecks if the conditional field has been validated.
    switch(test.type)
    {
      case "text" :
      case "textarea" :
      case "hidden" :
         if (!trim(test.value)=="" )
         {
         	for (var i = 1; i <= nbCondition; i++)
		{
        	 	var messageOK=new Array();
			var message=new Array();
			messageOK=eval('Link_'+field+i+'_OK');
			message=eval('Link_'+field+i);
			if(messageOK.length>0)
			{
				var k = trim(test.value);
				for (var j = 0; j<messageOK.length; j++)
				{
		  			if(k.toUpperCase() == messageOK[j].toUpperCase())
			        	{
						for (var l = 0; l<message.length; l++)
						{liste[liste.length]=message[l];}
					}
				}
			}
			else
			{
				for (var j = 0; j<message.length; j++)
				{liste[liste.length]=message[j];}
			}
		}//end of for nbcondition
	  } return "";
      case "checkbox" :
		if (test.checked==true)
		{
			var messageOK=new Array();
			messageOK=eval('Link_'+field+'_OK');
			for (var j = 0; j<messageOK.length; j++)
			{liste[liste.length]=messageOK[j];}
		} 
		if ((test.checked!=true) && (nbCondition==2) )
		{
			var messageKO=new Array();
			messageKO=eval('Link_'+field+'_KO');
			for (var j = 0; j<messageKO.length; j++)
			{liste[liste.length]=messageKO[j];}
		}return "";
      default :
		if(test[0].type == "radio")
		{
			for (var i = 1; i <= nbCondition; i++)
			{
				var messageOK=new Array();
				var message=new Array();
				var selectedRadio;
				messageOK=eval('Link_'+field+i+'_OK');
				message=eval('Link_'+field+i);
				for (var n = 0; n< test.length; n++)
				{
					if (test[n].checked){selectedRadio =test[n].value}
                 		}
				if(selectedRadio && messageOK.length>0)
				{
					for (var j = 0; j<messageOK.length; j++)
					{
						if(selectedRadio.toUpperCase() == messageOK[j].toUpperCase())
				        	{
							for (var m = 0; m<message.length; m++)
							{liste[liste.length]=message[m];}
					       	}
					}
				}
				else if (selectedRadio)
				{
					for (var j = 0; j<message.length; j++)
					{liste[liste.length]=message[j];}
				}
			}		
		}		
		else if (test.selectedIndex!=0)
		{
			
			for (var i = 1; i <= nbCondition; i++)
			{
				var messageOK=new Array();
				var message=new Array();				
				messageOK=eval('Link_'+field+i+'_OK');
				message=eval('Link_'+field+i);					
				if(messageOK.length>0)
				{
					var k = test.selectedIndex;
         				for (var j = 0; j<messageOK.length; j++)
         				{
         					if(test.options[k].text.toUpperCase() == messageOK[j].toUpperCase())
         					{
							for (var m = 0; m<message.length; m++)
							{liste[liste.length]=message[m];}
						}
					}
				}
				else
				{
					for (var j = 0; j<message.length; j++)
					{liste[liste.length]=message[j];}
				}
			}
		} return "";
		break;
	}//Ends all type checks
}//Ends addToCheck function

function getFieldValue(field)
{
   switch(field.type)
   {
      case "text" :
      case "textarea" :
      case "password" :
      case "hidden" :
         return field.value;
      case "button" :
      case "reset" :
      case "submit" :
         return "";
      case "checkbox" :
         if (field.checked) { return field.value; } else { return ""; }
      default :
      	if(field[0].type == "radio")
	{
		for (i = 0; i < field.length; i++)
		{
	        	if (field[i].checked){return field[i].value;}
                }	        	
	        return "";
	 }
         else
         {
         	var i = field.selectedIndex;
	        if (i == 0)   return "";
	        else   return (field.options[i].value == "") ? field.options[i].text : field.options[i].value;
         	break;
         }      
   }   
   return "";
}

function evenOutArrays(form,requiredFields) 
{   
   var fieldMessages=new Array();
   for (var i = 0; i < requiredFields.length; i++) 
   {
      eval("var message = form." +requiredFields[i]);
      if(message.id)
      	{
		
     		fieldMessages[i] = "Field " + message.id + " is a required field.";     	
     	}
     	else
     	{
	     	fieldMessages[i] = "Field " + message[0].id + " is a required field.";     	
     	}
   }
   return fieldMessages;
} // Ends the "evenOutArrays" function


function areRequiredFieldsPresent(form,requiredFields,requiredFieldMessages) 
{
   for (var i=0; i < requiredFields.length; i++) 
   {
     eval("var tocheck = form." + requiredFields[i]);
     if (tocheck) 
      {
      	if (trim(getFieldValue(tocheck)) == "") 
        {
            if(tocheck.type)
            {
            	alert(requiredFieldMessages[i]);
            	tocheck.focus();
            }
            else
            {
            	alert(requiredFieldMessages[i]);
            	tocheck[0].focus();
            }
            return false; // Return as soon as a failure is detected
         } // Ends the check to see if the field value is blank
      } // Ends the check to make sure the field exists
   } // Move on to the next field
   return true;
} // Ends the "areRequiredFieldsPresent" function

function trim(inputString) {
   if (typeof inputString != "string") { return inputString; }
   var retValue = inputString;
   var ch = retValue.substring(0, 1);
   while (ch == " ") // Check for spaces at the beginning of the string
   { 
      retValue = retValue.substring(1, retValue.length);
      ch = retValue.substring(0, 1);
   }
   ch = retValue.substring(retValue.length-1, retValue.length);
   while (ch == " ") // Check for spaces at the end of the string
   { 
      retValue = retValue.substring(0, retValue.length-1);
      ch = retValue.substring(retValue.length-1, retValue.length);
   }
   while (retValue.indexOf("  ") != -1)  // Note that there are two spaces in the string - look for multiple spaces within the string
   {
      retValue = retValue.substring(0, retValue.indexOf("  ")) + retValue.substring(retValue.indexOf("  ")+1, retValue.length); // Again, there are two spaces in each of the strings
   }
   return retValue; // Return the trimmed string back to the user
} 

function isFormOK(form) {

   var requiredFields = new Array();
   var requiredFieldMessages = new Array();
   requiredFields=addConditions(form);
   requiredFieldMessages=evenOutArrays(form,requiredFields);
   if ( !(areRequiredFieldsPresent(form,requiredFields,requiredFieldMessages)) ) { return false; }
   return true;
   
} // Ends the "isFormOK" function
