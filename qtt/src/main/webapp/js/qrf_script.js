	
	//toggle enable/disable form fields
	function enableDisableIt(obj) {
			obj.disabled = !(obj.disabled);
			var z = (obj.disabled) ? 'disabled' : 'enabled';
	}//end
	
	//if all drug is set to 'no' then enable the drugs else disable it
	function drugCheck() {			

		if (document.forms[0].allDrugs[1].checked) 
			enableDrugs();					
		else 
			disableDrugs();		
	}//end

	
	//enable the drug form fields	
	function enableDrugs() {
			
	    document.forms[0].retrDrug.disabled = false;		    		
		document.forms[0].route.disabled = false;	
		document.forms[0].formulation.disabled = false;	
		document.forms[0].indication.disabled = false;	
		document.forms[0].dose.disabled = false;	
		document.forms[0].othersDrug.disabled = false;	
		 setupTinyMC('#othersDrug',200);
	}//end
	
	//disable the drug form fields
	function disableDrugs() {				
	
		document.forms[0].retrDrug.disabled = true;		    		
		document.forms[0].route.disabled = true;	
		document.forms[0].formulation.disabled = true;	
		document.forms[0].indication.disabled = true;	
		document.forms[0].dose.disabled = true;	
		document.forms[0].othersDrug.disabled = true;	
		destroyTinyMC('#othersDrug');
	}//end
	
	
	//if cumulative period is set to 'no' then enable the cumulative fields else disable it
	function cumulativeCheck() {
				
		if (document.forms[0].cumulative[1].checked) 
			enableCumulative();					
		else 
			disableCumulative();		
	}//end

	//enable the cumulative form fields	
	function enableCumulative() {
		
		document.forms[0].from.disabled = false;
		document.forms[0].to.disabled = false;
	}//end
	
	//disable the cumulative form fields
	function disableCumulative() {
		document.forms[0].from.value = "";
		document.forms[0].from.disabled = true;
		document.forms[0].to.value = "";
		document.forms[0].to.disabled = true;
	}//end	
	 
	//open popup for partial queries 
	function openPartial() {
		
		//var top=(screen.height-350)/2;
		//var left=(screen.width-400)/2;		
		var path="FindPartialAction.action?email="+document.forms[0]["email"].value;
		var args;
		var sFeatures = getModalValues(637, 618);//changed from 637, 518
     	var sReturn = window.showModalDialog(path, args, sFeatures);
     	

		if(	sReturn != null) {	
			window.parent.location=sReturn;     	
		}
	}//end
	
	//enable and submit the form
	function enable() {	

      /*  document.forms[0].fileOnePath = document.forms[0].fileOne.value;
        alert(document.forms[0].fileOne.value);*/
		//we need to enable the email and query type in case its disabled
		document.forms[0].email.disabled = false;
		document.forms[0].queryType.disabled = false;	
		
		//submit the form		
		document.forms[0].action="StdQrfAction.action";
		document.forms[0].target="_self";
		document.forms[0].method="Post";
		//disable all buttons
	    for (i=0; i< document.forms[0].elements.length; i++) {
			t = document.forms[0][i].type.toLowerCase();
			if (t == "submit" || t == "reset" || t == "button")
				document.forms[0][i].disabled = true;
	    }
		
		document.forms[0].submit();
	}//end
	
	function doPartialSubmit() {
		if ( (getSelectedRadioValue(document.forms[0].queryNumberRadio)) == '' ) {
			alert("Please select a query to Open");
			return;
		}
		else 	{
			var optValue = getSelectedRadioValue(document.forms[0].queryNumberRadio);
			var args = null;
			if(optValue != '') {
				args = "PopulateQueryAction.action?myAction=partial&e-7172-g=4&ar-0=wer&a="+Math.random()+"jsession=000edrft0002120000wsed0012-45&jd1=314&d-48=098-s&d-48098-o=2&n=213456&sess=ooooooowe32000000&a=2dbf18ddaecc9108856c3013ba5776e4&fti=yes&on=21345&no="+Math.round(2*optValue)+"&d-48098-s&d-48098-o=2";
			}
		    window.returnValue = args;
		    window.close();
		} 
	}
		
	//methods used by drug interaction form

    function enableDisableRoche(drugNumber, value) {
      if (value==2) {
          disableDrugsInt(drugNumber);
      }   else
      if(value==1) {
          enableRoche(drugNumber);
      } else {
          enableNonRoche(drugNumber);
      }
    }


	function enableRoche(drugNumber) {
		//enable fields for roche drugs
		eval("document.forms[0].genericDrug"+drugNumber+".disabled=false");
		eval("document.forms[0].nongeneric"+drugNumber+".disabled=true");
		eval("document.forms[0].retrDrug"+drugNumber+".disabled=false");
		eval("document.forms[0].route"+drugNumber+".disabled=false");	
		eval("document.forms[0].formulation"+drugNumber+".disabled=false");	
		eval("document.forms[0].dose"+drugNumber+".disabled=false");
	} 
	
	function enableNonRoche(drugNumber) {
		//enable fields for non-roche drugs
		eval("document.forms[0].genericDrug"+drugNumber+".disabled=true");
		eval("document.forms[0].nongeneric"+drugNumber+".disabled=false");
		eval("document.forms[0].retrDrug"+drugNumber+".disabled=true");
		eval("document.forms[0].route"+drugNumber+".disabled=false");	
		eval("document.forms[0].formulation"+drugNumber+".disabled=false");	
		eval("document.forms[0].dose"+drugNumber+".disabled=false");
	} 
	
	function disableDrugsInt(drugNumber) {
		//disable all fields
		eval("document.forms[0].genericDrug"+drugNumber+".disabled=true");
		eval("document.forms[0].nongeneric"+drugNumber+".disabled=true");
		eval("document.forms[0].retrDrug"+drugNumber+".disabled=true");	
		eval("document.forms[0].route"+drugNumber+".disabled=true");	
		eval("document.forms[0].formulation"+drugNumber+".disabled=true");	
		eval("document.forms[0].dose"+drugNumber+".disabled=true");	
	}
	
	function doDrugs() {
			
			//set the values to what the user selected initially
			if (document.forms[0].drugtype1[0].checked)
				enableRoche('1');
			if (document.forms[0].drugtype2[0].checked)
				enableRoche('2');
			if (document.forms[0].drugtype3[0].checked)
				enableRoche('3');
			if (document.forms[0].drugtype4[0].checked)
				enableRoche('4');
			if (document.forms[0].drugtype5[0].checked)
				enableRoche('5');
				
			if (document.forms[0].drugtype1[1].checked)
				enableNonRoche('1');
			if (document.forms[0].drugtype2[1].checked)
				enableNonRoche('2');
			if (document.forms[0].drugtype3[1].checked)
				enableNonRoche('3');
			if (document.forms[0].drugtype4[1].checked)
				enableNonRoche('4');
			if (document.forms[0].drugtype5[1].checked)
				enableNonRoche('5');
				
			if (document.forms[0].drugtype2[2].checked)
				disableDrugsInt('2');
			if (document.forms[0].drugtype3[2].checked)
				disableDrugsInt('3');
			if (document.forms[0].drugtype4[2].checked)
				disableDrugsInt('4');
			if (document.forms[0].drugtype5[2].checked)
				disableDrugsInt('5');
				
		}
	
	function enableOther()	{
			
		//this function will enable the 'Other' field only if the output format is selected to 'other'
		if (document.forms[0].outputformat.selectedIndex == 4) {
				document.forms[0].othercomments.disabled=false;	
		}
		else {
				document.forms[0].othercomments.value="";
				document.forms[0].othercomments.disabled=true;
		}
	}

	function enableSpecial()	{
		
		//this function will enable the 'Other' field only if the output format is selected to 'other'
		
		if ('outputFormat.ctrlOformatId' in document.forms[0] 
				&& document.forms[0]['outputFormat.ctrlOformatId'].selectedIndex == 4) {
			document.forms[0].specialRequests.disabled=false;
		} else if ('clinical.ctrlOformat.ctrlOformatId' in document.forms[0] 
				&& document.forms[0]['clinical.ctrlOformat.ctrlOformatId'].selectedIndex == 4) {
			document.forms[0].specialRequest.disabled=false;	
		} else {
			if ('specialRequests' in document.forms[0]) {
				document.forms[0].specialRequests.value="";			
				document.forms[0].specialRequests.disabled=true;		
			} else {
				document.forms[0].specialRequest.value="";			
				document.forms[0].specialRequest.disabled=true;		
			}
		}
	}	
	
	function enableStudyNo() {
		if (allStudies = document.forms[0]['audit.allStudies'][1].checked) {
			document.forms[0]['audit.studyNo'].disabled = false;
		} else {
			document.forms[0]['audit.studyNo'].value = "";
			document.forms[0]['audit.studyNo'].disabled = true;
		}
	}
	
	function doQRFAmend() {

		if (confirm("This option will return you to the QRF entry screen. Do you want to continue?"))	{			

			var optValue = document.forms[0].id.value;
			var args = null;								
			if(optValue != '') {
				args = "PopulateQueryAction.action?myAction=partial&no="+Math.round(2*optValue);
			document.forms[0].action=args;	
			disableButtonsSubmit(document.forms[0]);					
			document.forms[0].target='_self';			
			document.forms[0].submit();
			}
		}
	}
	
	function doAssignedQueryAmend() {

		if (confirm("This query is currently being processed. This option will take out the query from the task list and the query has to be accepted and assigned again. Do you want to continue?"))	{			
			document.forms[0].activity.value = 'amd';	
			document.forms[0].action="ProcessQRFAction.action";
			disableButtonsSubmit(document.forms[0]);					
			document.forms[0].target='_self';			
			document.forms[0].submit();
			}
	}

	function doQRFDelete() {
		if (confirm("This option will delete the QRF without saving it. Do you want to continue?"))	{			
			document.forms[0].activity.value = 'del';	
			document.forms[0].action="ProcessQRFAction.action";
			disableButtonsSubmit(document.forms[0]);					
			document.forms[0].target='_self';			
			document.forms[0].submit();
			}
	}

	function doQRFSave() {
		
		if (confirm("This option will save the QRF as draft. You can retrieve it by entering your e-mail address and clicking the  'Find partially completed QRF's button. Do you want to continue?"))	{			
			disableButtonsSubmit(document.forms[0]);	
			document.forms[0].action="ProcessQRFAction.action";
			document.forms[0].activity.value = 'sav';						
			document.forms[0].target='_self';			
			document.forms[0].submit();
		} else return false;
	}

	function doQRFPrint() {
		w = window.open("/qtt/pdf/QRF?i=" + document.forms[0].id.value + "&t=", "Print", "width=950,height=650,scrollbars,menubar=no,resizable=yes");		}

	function doQRFSubmit() {
		
		if (confirm("This option will submit the QRF to the Drug Safety Contact Line. Do you want to continue?"))	{			
			document.forms[0].action="ProcessQRFAction.action";
			document.forms[0].activity.value = 'sub';						
			disableButtonsSubmit(document.forms[0]);					
			document.forms[0].target='_self';			
			document.forms[0].submit();
							
		}
	}
	function doQRFSubmitNoDoc() {

		if (confirm("This is HA or Legal Query. Please attach the Original Document."))	{			
				var optValue = document.forms[0].id.value;
				var args = null;								
				if(optValue != '') {
					args = "PopulateQueryAction.action?myAction=partial&no="+Math.round(2*optValue);
				document.forms[0].action=args;	
				disableButtonsSubmit(document.forms[0]);					
				document.forms[0].target='_self';			
				document.forms[0].submit();
				}
			}
	}

	function affiliateAlert() {
		if (document.forms[0].orgType[document.forms[0].orgType.selectedIndex].value == 2) {
			var path="ConfirmationDialog.action?option=affiliateAlert";
			var args;
			var sFeatures = getModalValues(230, 630);
		    window.showModalDialog(path, args, sFeatures);
		}
	}

