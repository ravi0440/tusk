function buildUrl(append) {
    var currentLocation = window.location;
    var protocol = currentLocation.protocol;
    var hostName = currentLocation.hostname;
    var port = currentLocation.port;
    var pathName = currentLocation.pathname.substring(1, currentLocation.pathname.length);
    var root = pathName.substring(0, pathName.indexOf("/"));

    var portPart = "";
    if (port) {
        portPart = ":" + port;
    }

    return protocol + "//" + hostName + portPart + "/" + root + "/" + append;
}

function showLabel(text, querySeq) {
    var TEXT_MAX_LEN = 10;
    if (text.length > TEXT_MAX_LEN) {
        var textTrimmed = text.substring(0, TEXT_MAX_LEN) + "...";
        var url = buildUrl("queryDetails?QUERY_SEQ=" + querySeq);
        var template = "<div style='cursor: pointer' onclick='showUrlWindow(\"" + url + "\")'>%sometext%</div>";
        return template.replace("%sometext%", textTrimmed);
    }
    return text;
}

function showUrlWindow(url) {
    var winHeight = 200;
    var winWidth = 200;
    var w = window.innerWidth;
    var h = window.innerHeight;
    var left =  w / 2 - winWidth / 2;
    var top = h / 2 - winHeight / 2;
    window.open(url, "",
        "width=" + winWidth + ",height=" + winHeight +
        ",menubar=no,resizable=no,top=" + top + ",left=" + left + ",location=no,status=no,titlebar=no,toolbar=no");
}