function openInitiateList() {		
		var args = "InitiateList.action";											
	    window.returnValue = args;
	    window.close();
}

function modalWin(path, width, height) {
    if (window.showModalDialog) {
       return window.showModalDialog(path, null, getModalValues(width, height))
    } else {
        window.open(path, '', 'height=' + height + ',width=' + width + ',toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no ,modal=yes');
    }
}


function doReject() {
		
		if (confirm("Rejecting a query will remove it completely. Please click 'OK' if you wish to proceed."))	{			
			document.forms[0].action="InitiationEmail.action?type=rej";
			disableButtonsSubmit(document.forms[0]);					
			document.forms[0].target='_self';			
			document.forms[0].submit();
		}
		
}

function doRefresh()
{
		window.parent.location="InitiateList.action";
}

function doAccept() {
		document.forms[0].action="AcceptQueryListRecipients.action";
		disableButtonsSubmit(document.forms[0]);		
		document.forms[0].target='_self';			
		document.forms[0].submit();
}

function doInitiateSubmit() {		
		
		if ( (getSelectedRadioValue(document.forms[0].qttradio)) == '' ) {
			alert("Please select a query to Open");
			return;
		}
		else 	{	
			document.forms[0].queryId.value=getSelectedRadioValue(document.forms[0].qttradio);		
			document.forms[0].action="PreviewAction.action";
			document.forms[0].method="POST";						
			disableButtonsSubmit(document.forms[0]);	
			document.forms[0].target='_self';								
			document.forms[0].submit();
		}
}
		
function doDuplicateSubmit() {				
		
		if ( (getSelectedRadioValue(document.forms[0].qttradio)) == '' ) {
			alert("Please select a query to View");
			return;
		}
		else 	{		
			//submit the form 
			document.forms[0].action="QueryReview.action?viewtype=duplicate&queryId="+getSelectedRadioValue(document.forms[0].qttradio);
			document.forms[0].target='_self';
			document.forms[0].submit();
		}
}	