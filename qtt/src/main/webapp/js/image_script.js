
// Changes image of link below mouse cursor
var imgNames = new Array();//store of img names
var newImg = new Array();//store of replacement img objects
var origSrc = new Array();//store of original img paths
var on = null,loaded = false;//'on' and 'loaded' state flags

// Iterates through document's images array and stores/creates images for later use
//	takes an optional parameter 'ref' to enable an image to be toggled() 'on' at page load
function preload(ref){

	if(!ref){
		ref = null;
	}
	for(i = 0;i < document.images.length;i++){
		img = document.images[i];	//Assigns current image to variable img
		if(img.src.substring(img.src.lastIndexOf('.')-4,img.src.lastIndexOf('.')) == '_off'){
			imgNames[i] = img.name;
			origSrc[i] = img.src;	//Stores image location in origSrc array
			newImg[i] = new Image();	//Creates new image in newImg array
			newImg[i].src = img.src.substring(0,img.src.lastIndexOf('.')-3) + 'on' + img.src.substring(img.src.lastIndexOf('.'));	//Constructs the path for the newImg image preserving img src extension
		}
	}
	loaded = true;
	if(ref != null){
		toggle(ref)
	}
}

//Toggles image for clicked link and sets flag variable on to link's image
function toggle(ref){
	num = refToNum(ref);
	img = document.images[ref];
	if(on != null){
		document.images[on].src = origSrc[on];
		img.src = newImg[num].src;	//Set clicked image within clicked link to new image source
	}else{
		img.src = newImg[num].src;	//Set clicked image within clicked link to new image source
	}
	on = num;						//Set flag to reflect index/id of last clicked link
}

//Changes image of link below mouse cursor
function hilight(ref){
	num = refToNum(ref);
	if(loaded && num != on){
		img = document.images[ref];
		img.src = newImg[num].src				//Set link's image to new source
	}
}

//Reverts link's image when link loses focus
function lolight(ref){
	num = refToNum(ref);
	if(loaded && num != on){
		img = document.images[ref];
		img.src = origSrc[num]	//Set link's image to original source
	}
}


//Checks if ref is numeric and retrieves the numeric index of image with Id = ref if it is not
function refToNum(ref){
	if(loaded){
		if(isNaN(ref)){//If 'ref' is not numeric
			for(i=0;i<imgNames.length;i++){//For each id in imgNames array
				if(ref == imgNames[i]){
					num = i;
					break
				}
			}
		}else{
			num = parseInt(ref, 10)
		}
		return num
	}
}

//this function gets rid of the frames if any.
function escapeFrame() {
	//check for any frames
 	if(top!=self) {
		top.location.href=self.location.href;
	}
	return;
}
