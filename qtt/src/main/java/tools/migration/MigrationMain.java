package tools.migration;

public class MigrationMain {
	
	public static void main(String[] args) {
//    	if (args.length != 3) {
//    		usageExit();
//    		return;
//    	}
//
//        String dbUrl = args[0];
//        String dbUser = args[1];
//        String dbPassword = args[2];
//        Migration migration = new Migration(dbUrl, dbUser, dbPassword);
        //Migration migration = new Migration("jdbc:postgresql://localhost:5432/qtt_jbpm", "qtt_jbpm", "qtt_jbpm");//local
       // Migration migration = new Migration("jdbc:oracle:thin:@MONET.KAU.ROCHE.COM:1530:AGSPD", "qtt_jbpm", "qtt_jbpm");//dev
        Migration migration = new Migration("jdbc:oracle:thin:@MONET.KAU.ROCHE.COM:1530:AGSPD", "qtt_jbpm_backup", "qtt_jbpm_backup");//st
        
        //Migration migration = new Migration("jdbc:oracle:thin:@monet.kau.roche.com:1532:agspu", "qtt_jbpm", "qtt_jbpm");//uat
		
		//Migration migration = new Migration("jdbc:oracle:thin:@rwfmspglora02.emea.roche.com:1521:unit4", "qtt_jbpm", "qtt_jbpm");//unit4
        
        migration.start();
	}
	
	private static void usageExit() {
		System.out.println("Usage:");
		System.out.println("args[0] The database url. ");
		System.out.println("args[1] The user name to connect to the database. ");
		System.out.println("args[2] The user password to connect to the database. ");
		System.exit(-1);
	}

}