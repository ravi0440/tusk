package tools.migration;

import java.sql.Timestamp;

public class WorkpointTask {
	
	private Long taskId; 
	private String taskName;
	private int taskStatus;
	private String actorId;
	private Timestamp taskCreate;
	private Timestamp taskStart;
	private Timestamp taskEnd;
	private Timestamp taskLastUpdate;
	private String timer;
	

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public int getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(int taskStatus) {
		this.taskStatus = taskStatus;
	}

	public String getActorId() {
		return actorId;
	}

	public void setActorId(String actorId) {
		this.actorId = actorId;
	}	

	public Timestamp getTaskCreate() {
		return taskCreate;
	}

	public void setTaskCreate(Timestamp taskCreate) {
		this.taskCreate = taskCreate;
	}

	public Timestamp getTaskStart() {
		return taskStart;
	}

	public void setTaskStart(Timestamp taskStart) {
		this.taskStart = taskStart;
	}

	public Timestamp getTaskEnd() {
		return taskEnd;
	}

	public void setTaskEnd(Timestamp taskEnd) {
		this.taskEnd = taskEnd;
	}

	public Timestamp getTaskLastUpdate() {
		return taskLastUpdate;
	}

	public void setTaskLastUpdate(Timestamp taskLastUpdate) {
		this.taskLastUpdate = taskLastUpdate;
	}

	public String getTimer() {
		return timer;
	}

	public void setTimer(String timer) {
		this.timer = timer;
	}

}