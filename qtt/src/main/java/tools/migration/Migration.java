package tools.migration;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.roche.dss.qtt.utility.Activities;
import com.roche.dss.qtt.utility.Nodes;


public class Migration {
	
	private static final Logger logger = Logger.getLogger(Migration.class);
	
	public static final String WF_ROUTE_B = "B";
	
	private SimpleJdbcTemplate jdbc;
	private MigrationHelper mHelper;
	private WorkpointHelper wHelper;
	
	private Long processDefinitionId;
	private Map<String, Object> taskIds;
	private Map<String, Object> nodeIds;
	

	public Migration(String dbUrl, String dbUser, String dbPassword) {
		SingleConnectionDataSource ds = MigrationHelper.createDs(dbUrl, dbUser, dbPassword);
		ds.setAutoCommit(false);
		jdbc = new SimpleJdbcTemplate(ds);	
		mHelper = new MigrationHelper(jdbc);
		
		//TODO: can be also one data source
		//SingleConnectionDataSource wkptDs = WorkpointHelper.createDs("jdbc:oracle:thin:@monet.kau.roche.com:1532:agspu", "qtt_wkpt_tmp", "qtt_wkpt_tmp");//uat
		//SingleConnectionDataSource wkptDs = WorkpointHelper.createDs("jdbc:oracle:thin:@MONET.KAU.ROCHE.COM:1530:AGSPD", "qtt_wkpt", "qtt_wkpt_p1");//dev
		SingleConnectionDataSource wkptDs = WorkpointHelper.createDs("jdbc:oracle:thin:@MONET.KAU.ROCHE.COM:1531:AGSPS", "qtt_wkpt_backup_20120514", "backup");//st
		                                                                                                                
		
		
		//SingleConnectionDataSource securityDs = WorkpointHelper.createDs("jdbc:oracle:thin:@MONET.KAU.ROCHE.COM:1530:AGSPD", "security", "security");//security dev
		SingleConnectionDataSource securityDs = WorkpointHelper.createDs("jdbc:oracle:thin:@MONET.KAU.ROCHE.COM:1531:AGSPS", "security", "security");//security dev

		SimpleJdbcTemplate wkptJdbc = new SimpleJdbcTemplate(wkptDs);
		//SimpleJdbcTemplate securityJdbc = new SimpleJdbcTemplate(securityDs);
		wHelper = new WorkpointHelper(wkptJdbc, securityDs);
	}
	
	@SuppressWarnings({ "rawtypes" })
	public void start() {
		
		try {
			logger.info("Migration process started... ");
					
			prepare();
	
			logger.info("Getting closed queries from Workpoint... ");
			List<Map<String, Object>> closedQueries = mHelper.getAllClosedQueries();

			logger.info(closedQueries.size() + " closed queries ready for migration from Workpoint to Jbpm. ");
	
			Map[] processInstanceFullArgumentsList = prepareProcessInstanceArgumentsList(closedQueries);
			
			prepareTaskInstanceArgumentList(processInstanceFullArgumentsList, false);//false!!!
			
			logger.info(closedQueries.size() + " closed queries prepared for commit to Jbpm. ");
			
			logger.info("Committing closed queries to Jbpm... ");
			
			mHelper.commit();
			
			logger.info(closedQueries.size() + " closed queries successfully migrated from Workpoint to Jbpm. ");
			
			logger.info("Getting open queries from Workpoint... ");
			List<Map<String, Object>> openQueries = mHelper.getAllOpenQueries();
			
			logger.info(openQueries.size() + " open queries ready for migration from Workpoint to Jbpm. ");
			
			Map[] processInstanceFullArgumentsListOpenQ = prepareProcessInstanceArgumentsList(openQueries);
			
			prepareTaskInstanceArgumentList(processInstanceFullArgumentsListOpenQ, true);
			
			logger.info(openQueries.size() + " open queries prepared for commit to Jbpm. ");
			
			logger.info("Committing open queries... ");
	
			mHelper.commit();
			
			logger.info(openQueries.size() + " open queries successfully migrated from Workpoint to Jbpm. ");
			
			logger.info("Migration process successfully finished! ");
		
		} catch (DataAccessException e) {
			mHelper.rollback();
			logger.error("RolledBack - Database error while migration process... ", e);
		}

	}
	
	private void prepare() {
		processDefinitionId = mHelper.getProcessDefinitionId();
		taskIds = mHelper.getTaskIds();
		nodeIds = mHelper.getNodeIds();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Map[] prepareProcessInstanceArgumentsList(List<Map<String, Object>> queryList) {
		Long currentProcessInsatnceId = mHelper.getNextProcessInstanceId();
		Long currentTokenId = mHelper.getNextTokenId();
		Long currentModuleInstanceId = mHelper.getNextModuleInstanceId();
		Long currentTokenVariableMapId = mHelper.getNextTokenVariableMapId();
		Long currentVariableId = mHelper.getNextVariableId();
		Long taskMgmtDefinitionId = mHelper.getTaskMgmtDefinitionId();
		Map<String, Object> query = null;
		Map[] queryListWithProcessInstancess = new Map[queryList.size()];
		Map[] processInstancesWithToken = new Map[queryList.size()];
		Map[] argumentsList = new Map[queryList.size()];
		Map[] tokenArgumentsList = new Map[queryList.size()];
		Map[] moduleInstanceArgumentsList = new Map[queryList.size()];
		Map[] additionalModuleInstanceArgumentsList = new Map[queryList.size()];
		Map[] tokenVariableArgumentsList = new Map[queryList.size()];
		Map[] variableArgumentsList = new Map[queryList.size()];
		Map[] additionalVariableArgumentsList = new Map[queryList.size()];
		int i = 0;
		
		for(Iterator it = queryList.iterator(); it.hasNext(); ++i) {
			query = (HashMap<String, Object>) it.next();
			argumentsList[i] = makeProcessInstanceArguments(currentProcessInsatnceId, query);
			query.put("processInstanceId", argumentsList[i].get("id"));
			queryListWithProcessInstancess[i] = query;
			tokenArgumentsList[i] = makeTokenArguments(currentTokenId, (Long) argumentsList[i].get("id"));
			processInstancesWithToken[i] = argumentsList[i];
			processInstancesWithToken[i].put("rootTokenId", tokenArgumentsList[i].get("id"));
			moduleInstanceArgumentsList[i] = makeModuleInstanceArguments(currentModuleInstanceId, (Long) argumentsList[i].get("id"));
			tokenVariableArgumentsList[i] = makeVariableMapArguments(currentTokenVariableMapId, currentTokenId, currentModuleInstanceId);
			variableArgumentsList[i] = makeVariableArguments(currentVariableId, currentTokenId, currentTokenVariableMapId, currentProcessInsatnceId);
			++currentVariableId;
			++currentModuleInstanceId;
			additionalModuleInstanceArgumentsList[i] = makeAdditionalModuleInstanceArguments(currentModuleInstanceId, (Long) argumentsList[i].get("id"), taskMgmtDefinitionId);
			additionalVariableArgumentsList[i] = makeAdditionalVariableArguments(currentVariableId, currentTokenId, currentTokenVariableMapId, currentProcessInsatnceId, query.get("query_seq"));
			processInstancesWithToken[i].put("taskMgmtInstance", additionalModuleInstanceArgumentsList[i].get("id"));
			processInstancesWithToken[i].put("queryNumber", query.get("query_number"));
			processInstancesWithToken[i].put("workflowRouteType", query.get("workflow_route_type"));
			processInstancesWithToken[i].put("currentDueDate", query.get("current_due_date"));
			processInstancesWithToken[i].put("tokenVariableMapId", currentTokenVariableMapId);
			++currentProcessInsatnceId;
			++currentTokenId;
			++currentModuleInstanceId;
			++currentTokenVariableMapId;
			++currentVariableId;
			logger.debug(i + 1 + ". Preparing query data for query number: " + query.get("query_number") + " - part 1. ");
		}
		
		mHelper.batchInsertProcessInstance(argumentsList);
		mHelper.batchUpdateQuery(queryListWithProcessInstancess);
		mHelper.batchInsertToken(tokenArgumentsList);
		mHelper.batchUpdateProcessInstance(processInstancesWithToken);
		mHelper.batchInsertModuleInstance(moduleInstanceArgumentsList);
		mHelper.batchInsertModuleInstance(additionalModuleInstanceArgumentsList);
		mHelper.batchInsertTokenVariableMap(tokenVariableArgumentsList);
		mHelper.batchInsertVariableInstance(variableArgumentsList);
		mHelper.batchInsertVariableInstance(additionalVariableArgumentsList);
				
		return processInstancesWithToken;
	}
		
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void prepareTaskInstanceArgumentList(Map[] processInstanceFullArgumentsList, boolean openQuery) {
		Map<String, Object> closeTimerJobInfo = mHelper.getJobData("com.roche.dss.qtt.service.workflow.handler.QTTCloseTimerHandler");
		Map<String, Object> dueDateTimerJobInfo = mHelper.getJobData("com.roche.dss.qtt.service.workflow.handler.QTTMIDueSoonHandler");
		Long currentTaskInsatnceId = mHelper.getNextTaskInstanceId();
		Object query_seq = null;
		String query_number = null;
		DateTime currentDueDate = null;
		WorkpointModel workpointModelHelper = null;
		Long currentTokenId = mHelper.getNextTokenId();
		Long currentVariableId = mHelper.getNextVariableId();
		Long currentJobId = mHelper.getNextJobId();
		for(int i = 0; i < processInstanceFullArgumentsList.length; ++i) {
			
			logger.debug(i + 1 + ". Preparing query data for query number: " + processInstanceFullArgumentsList[i].get("queryNumber") + " - part 2. ");
			
			List childTokenArgumentsList = new ArrayList();
			query_seq = processInstanceFullArgumentsList[i].get("key");
			query_number = (String) processInstanceFullArgumentsList[i].get("queryNumber");
			currentDueDate = new DateTime (((Timestamp) processInstanceFullArgumentsList[i].get("currentDueDate")).getTime());
			
			processInstanceFullArgumentsList[i].put("lastNodeId", mHelper.getLastNodeId());
			
			workpointModelHelper = wHelper.getWorkpointData(query_number, currentDueDate, openQuery);
			int childToken = 0;
			if(workpointModelHelper != null) {
				int j = 0;
				Map[] taskInstancess = new Map[workpointModelHelper.getTasks().size()];
				Map[] jobArguments = new Map[1];
				for(WorkpointTask workpointTask : workpointModelHelper.getTasks()){
					boolean childTokenAdded = false;
					
					if(workpointTask.getTaskEnd() == null && openQuery) {
						processInstanceFullArgumentsList[i].put("lastNodeId", nodeIds.get(MigrationHelper.nodeNameMapper.get(workpointTask.getTaskName())));
						if(workpointTask.getTaskStatus() != 6 && mIChildTokenNeeded(processInstanceFullArgumentsList[i], workpointTask)) {
							processInstanceFullArgumentsList[i].put("name", "for mi and dmg");
							processInstanceFullArgumentsList[i].put("start", workpointTask.getTaskStart());
							Map childTokenArguments = new HashMap(processInstanceFullArgumentsList[i]);
							childTokenArguments.put("currentChildTokenId", currentTokenId);
							childTokenArgumentsList.add(childTokenArguments);
							childTokenAdded = true;
							childToken++;
							processInstanceFullArgumentsList[i].put("lastNodeId", nodeIds.get(Nodes.FORK.getNodeName()));
						} else if(workpointTask.getTaskStatus() != 6 && dMGChildTokenNeeded(processInstanceFullArgumentsList[i], workpointTask)) {
							processInstanceFullArgumentsList[i].put("name", "assign someone for dmg");
							processInstanceFullArgumentsList[i].put("start", workpointTask.getTaskStart());
							Map childTokenArguments = new HashMap(processInstanceFullArgumentsList[i]);
							childTokenArguments.put("currentChildTokenId", currentTokenId);
							childTokenArgumentsList.add(childTokenArguments);
							childTokenAdded = true;
							childToken++;
							processInstanceFullArgumentsList[i].put("lastNodeId", nodeIds.get(Nodes.FORK.getNodeName()));
						}
						
						if(workpointTask.getTimer() != null) {
							jobArguments[0] = makeJobInstanceArguments(currentJobId, currentTaskInsatnceId, processInstanceFullArgumentsList[i], workpointTask, dueDateTimerJobInfo, closeTimerJobInfo);
							++currentJobId;
						}						
					}					
					
					taskInstancess[j] = makeTaskInsanceArguments(currentTaskInsatnceId, processInstanceFullArgumentsList[i], workpointModelHelper, workpointTask, childTokenAdded, currentTokenId);
					++currentTaskInsatnceId;
					++j;
					if(childTokenAdded) {
						++currentTokenId;
					}
				}
				if(!childTokenArgumentsList.isEmpty()) {
					insertChildToken(childTokenArgumentsList);
				}
				mHelper.batchInsertTaskInstance(taskInstancess);
				if(jobArguments[0] != null) {
					mHelper.batchInsertJob(jobArguments);
				}
				
				if(childToken > 0) {
					processInstanceFullArgumentsList[i].put("isAbleToReactivateParent", Boolean.TRUE);
				}
				
				processInstanceFullArgumentsList[i].put("start", workpointModelHelper.getTokenStart());
				processInstanceFullArgumentsList[i].put("end", workpointModelHelper.getTokenEnd());
				processInstanceFullArgumentsList[i].put("isAbleToReactivateParent", workpointModelHelper.getTokenEnd() == null ? Boolean.TRUE : Boolean.FALSE);
				processInstanceFullArgumentsList[i].put("initiatorId", workpointModelHelper.getInitiatorId());
				processInstanceFullArgumentsList[i].put("nodeEnter", workpointModelHelper.getTokenLastUpdate());
				
				Map<String, String> processVariable = workpointModelHelper.getVariables();
				if(!processVariable.isEmpty()) {
					Map[] additionalvariableArgumentsList = new Map[processVariable.size()];
					int k = 0;
					for(Map.Entry<String, String> entry : processVariable.entrySet()) {
						additionalvariableArgumentsList[k] = new HashMap(processInstanceFullArgumentsList[i]);
						additionalvariableArgumentsList[k].put("currentVariableId", currentVariableId);
						additionalvariableArgumentsList[k].put("name", entry.getKey());
						additionalvariableArgumentsList[k].put("stringValue", entry.getValue());
						++k;
						++currentVariableId;
					}
					
					mHelper.batchInsertVariableInstanceSimple(additionalvariableArgumentsList);
			
				}
			}
			workpointModelHelper = null;
			logger.info(i + 1 + ". Query data for query number: " + processInstanceFullArgumentsList[i].get("queryNumber") + " prepared. ");
		}
		
		mHelper.batchUpdateProcessInstanceDates(processInstanceFullArgumentsList);
		mHelper.batchUpdateTokenData(processInstanceFullArgumentsList);
		mHelper.batchUpdateVariable(processInstanceFullArgumentsList);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void insertChildToken(List childTokenArgumentsList) {
		Map[] childTokens = new Map[childTokenArgumentsList.size()];
		int i = 0;
		for(Iterator iter = childTokenArgumentsList.iterator(); iter.hasNext();++i) {
			Map helper = (Map) iter.next();
			childTokens[i] = helper;
			childTokens[i].put("parent", helper.get("rootTokenId"));
			childTokens[i].put("processInstanceId", helper.get("id"));
			childTokens[i].put("id", helper.get("currentChildTokenId"));
			childTokens[i].put("end", null);
			childTokens[i].put("nodeEnter", null);
			childTokens[i].put("isAbleToReactivateParent", Boolean.TRUE);
			
		}
		mHelper.batchInsertToken(childTokens);
	}
	
	@SuppressWarnings("rawtypes")
	private boolean mIChildTokenNeeded(Map processInstanceFullArgumentsList, WorkpointTask workpointTask) {

		if (processInstanceFullArgumentsList.get("workflowRouteType") != null && processInstanceFullArgumentsList.get("workflowRouteType").equals(
				WF_ROUTE_B)
				&& MigrationHelper.taskNameMapper.get(workpointTask.getTaskName())
						.equals(Activities.MEDICAL_INTERPRETATION_II
								.getTaskName())) {
			return true;

		}
		return false;
	}
	
	@SuppressWarnings("rawtypes")
	private boolean dMGChildTokenNeeded(Map processInstanceFullArgumentsList, WorkpointTask workpointTask) {

		if (processInstanceFullArgumentsList.get("workflowRouteType") != null && processInstanceFullArgumentsList.get("workflowRouteType").equals(
				WF_ROUTE_B)
				&& workpointTask.getTaskName().matches(".*DMG.*")) {
			return true;

		}
		return false;
	}
	
	@SuppressWarnings("rawtypes")
	private Map<String, Object> makeJobInstanceArguments(Long currentId, Long currentTaskInsatnceId, Map processInstanceFullArgumentsList, WorkpointTask workpointTask, Map<String, Object> dueDateTimerJobInfo, Map<String, Object> closeTimerJobInfo) {
		Map<String, Object> jobArguments = new HashMap<String, Object>();
		jobArguments.put("id", currentId);
		
		jobArguments.put("processInstanceId", processInstanceFullArgumentsList.get("id"));
		jobArguments.put("rootTokenId", processInstanceFullArgumentsList.get("rootTokenId"));
		jobArguments.put("taskInstanceId", currentTaskInsatnceId);
		if(workpointTask.getTimer().equals("com.roche.dss.qtt.service.workflow.handler.QTTMIDueSoonHandler")) {
	       	DateTime now = new DateTime(workpointTask.getTaskCreate().getTime());
        	DateTime dueDate = now.plusHours(6);
			jobArguments.put("dueDate", (Date) dueDate.toDate());
			jobArguments.put("name", dueDateTimerJobInfo.get("name_"));
			jobArguments.put("repeat", dueDateTimerJobInfo.get("repeat_"));
			jobArguments.put("transitionName", null);
			jobArguments.put("actionId", dueDateTimerJobInfo.get("timeraction_"));
			jobArguments.put("graphElementId", taskIds.get(MigrationHelper.taskNameMapper.get(workpointTask.getTaskName())));
		} else {
	       	DateTime now = new DateTime(workpointTask.getTaskCreate().getTime());
        	DateTime dueDate = now.plusDays(5);
			jobArguments.put("dueDate", (Date) dueDate.toDate());
			jobArguments.put("name", closeTimerJobInfo.get("name_"));
			jobArguments.put("repeat", null);
			jobArguments.put("transitionName", "close");
			jobArguments.put("actionId", closeTimerJobInfo.get("timeraction_"));
			jobArguments.put("graphElementId", taskIds.get(MigrationHelper.taskNameMapper.get(workpointTask.getTaskName())));
		}		
		return jobArguments;
	}
	
	private Map<String, Object> makeProcessInstanceArguments(Long currentId, Map<String, Object> query ) {
		Map<String, Object> processInstanceArguments = new HashMap<String, Object>(); 
		processInstanceArguments.put("id", currentId);
		processInstanceArguments.put("key", query.get("query_seq"));
		processInstanceArguments.put("processdefinition", processDefinitionId);	
		return processInstanceArguments;
	}
	
	private Map<String, Object> makeTokenArguments(Long currentId, Long processInstanceId) {
		Map<String, Object> tokenArguments = new HashMap<String, Object>(); 
		tokenArguments.put("id", currentId);
		tokenArguments.put("lastNodeId", mHelper.getLastNodeId());
		tokenArguments.put("processInstanceId", processInstanceId);
		tokenArguments.put("parent", null);
		tokenArguments.put("isAbleToReactivateParent", Boolean.FALSE);
		tokenArguments.put("name", null);
		tokenArguments.put("start", null);
		tokenArguments.put("end", null);
		tokenArguments.put("nodeEnter", null);
		return tokenArguments;
	}

	private Map<String, Object> makeModuleInstanceArguments(Long currentId, Long processInstanceId) {
		Map<String, Object> moduleInstanceArguments = new HashMap<String, Object>();
		moduleInstanceArguments.put("id", currentId);
		moduleInstanceArguments.put("class", "C");
		moduleInstanceArguments.put("processInstanceId", processInstanceId);
		moduleInstanceArguments.put("taskMgmtDefinition", null);
		moduleInstanceArguments.put("name", "org.jbpm.context.exe.ContextInstance");
		return moduleInstanceArguments;
	}
	
	private Map<String, Object> makeAdditionalModuleInstanceArguments(Long currentId, Long processInstanceId, Long taskMgmtDefinition) {
		Map<String, Object> moduleInstanceArguments = new HashMap<String, Object>();
		moduleInstanceArguments.put("id", currentId);
		moduleInstanceArguments.put("class", "T");
		moduleInstanceArguments.put("processInstanceId", processInstanceId);
		moduleInstanceArguments.put("taskMgmtDefinition", taskMgmtDefinition);
		moduleInstanceArguments.put("name", "org.jbpm.taskmgmt.exe.TaskMgmtInstance");
		return moduleInstanceArguments;
	}
	
	@SuppressWarnings("rawtypes")
	private Map<String, Object> makeTaskInsanceArguments(Long currentId, Map processInstanceFullArgumentsList, WorkpointModel workpointModelHelper, WorkpointTask workpointTask, boolean childTokenAdded, Long childTokenId) {
		Map<String, Object> taskInstanceArguments = new HashMap<String, Object>();
		taskInstanceArguments.put("id", currentId);
		taskInstanceArguments.put("name", MigrationHelper.taskNameMapper.get(workpointTask.getTaskName()));
		taskInstanceArguments.put("actorId", workpointTask.getActorId());
		taskInstanceArguments.put("taskId", taskIds.get(MigrationHelper.taskNameMapper.get(workpointTask.getTaskName())));
		taskInstanceArguments.put("tokenId", processInstanceFullArgumentsList.get("rootTokenId"));
		if(childTokenAdded) {
			taskInstanceArguments.put("tokenId", childTokenId);
		}
		taskInstanceArguments.put("processInstanceId", processInstanceFullArgumentsList.get("id"));
		taskInstanceArguments.put("taskMgmtInstance", processInstanceFullArgumentsList.get("taskMgmtInstance"));
		taskInstanceArguments.put("create", workpointTask.getTaskCreate());
		taskInstanceArguments.put("start", workpointTask.getTaskStart());
		taskInstanceArguments.put("end", workpointTask.getTaskEnd());
		if(workpointTask.getTaskEnd() == null && workpointTask.getTaskStatus() != 6) {
			taskInstanceArguments.put("isOpen", Boolean.TRUE);
			taskInstanceArguments.put("issignalling", Boolean.TRUE);
			taskInstanceArguments.put("isCancelled", Boolean.FALSE);
		} else if (workpointTask.getTaskStatus() == 6) {
			taskInstanceArguments.put("isOpen", Boolean.FALSE);
			taskInstanceArguments.put("issignalling", Boolean.FALSE);
			taskInstanceArguments.put("isCancelled", Boolean.TRUE);
			taskInstanceArguments.put("end", workpointTask.getTaskLastUpdate());
		} else {
			taskInstanceArguments.put("isOpen", Boolean.FALSE);
			taskInstanceArguments.put("issignalling", Boolean.FALSE);
			taskInstanceArguments.put("isCancelled", Boolean.FALSE);
		}
		
		return taskInstanceArguments;
	}
	
	private Map<String, Object> makeVariableMapArguments(Long currentId, Long currentTokenId, Long currentModuleInstanceId) {
		Map<String, Object> variableMapArguments = new HashMap<String, Object>();
		variableMapArguments.put("id", currentId);
		variableMapArguments.put("tokenId", currentTokenId);
		variableMapArguments.put("contextInstance", currentModuleInstanceId);
		return variableMapArguments;
	}
	
	private Map<String, Object> makeVariableArguments(Long currentId, Long currentTokenId, Long currentTokenVariableMapId, Long currentProcessInsatnceId) {
		Map<String, Object> variableArguments = new HashMap<String, Object>();
		variableArguments.put("id", currentId);
		variableArguments.put("class", "S");
		variableArguments.put("name", "PROCESS_INSTANCE_START_ACTOR_ID");
		variableArguments.put("tokenId", currentTokenId);
		variableArguments.put("tokenVariableMapId", currentTokenVariableMapId);
		variableArguments.put("processInstanceId", currentProcessInsatnceId);
		variableArguments.put("longValue", null);
		variableArguments.put("stringValue", null);
		return variableArguments;
	}
	
	private Map<String, Object> makeAdditionalVariableArguments(Long currentId, Long currentTokenId, Long currentTokenVariableMapId, Long currentProcessInsatnceId, Object queryId) {
		Map<String, Object> variableArguments = new HashMap<String, Object>();
		variableArguments.put("id", currentId);
		variableArguments.put("class", "L");
		variableArguments.put("name", "PROCESS_INSTANCE_OWNER_ID");
		variableArguments.put("tokenId", currentTokenId);
		variableArguments.put("tokenVariableMapId", currentTokenVariableMapId);
		variableArguments.put("processInstanceId", currentProcessInsatnceId);
		variableArguments.put("longValue", queryId);
		variableArguments.put("stringValue", null);
		return variableArguments;
	}

}