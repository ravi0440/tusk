package tools.migration;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MigrationHelper {
	
    public static final Map<String, String> taskNameMapper = new HashMap<String, String>();

    static {
    	taskNameMapper.put("Job Start", "Job Start");
    	taskNameMapper.put("act-QTT-Query-Assignment", "Unassigned");
    	taskNameMapper.put("act-QTT-PS-ManualAssignment", "Assign PS");
    	taskNameMapper.put("act-QTT-MI-Medical Interpretation I", "Medical Interpretation");
    	taskNameMapper.put("act-QTT-MI-Clarification", "MI Clarification");
    	taskNameMapper.put("act-QTT-MI-Review", "MI Review");
    	taskNameMapper.put("act-QTT-MI-Medical Interpretation II", "Medical Interpretation+DMG");
    	taskNameMapper.put("act-QTT-DMG-ManualAssignment", "Assign DMG");
    	taskNameMapper.put("act-QTT-DMG-DataSearch", "DMG Data Search");
    	taskNameMapper.put("act-QTT-DMG-Clarification", "DMG Clarification");
    	taskNameMapper.put("act-QTT-DMG-Review", "DMG Review");
    	taskNameMapper.put("act-QTT-SendToRequester", "Ready to Send");
    	taskNameMapper.put("act-QTT-AwaitingFollow-up", "Sent to Requester");
    }
    
    public static final Map<String, String> nodeNameMapper = new HashMap<String, String>();

    static {
    	nodeNameMapper.put("Job Start", "Job Start");
    	nodeNameMapper.put("act-QTT-Query-Assignment", "Query Assignment");
    	nodeNameMapper.put("act-QTT-PS-ManualAssignment", "MI-PS Manual Assignment");
    	nodeNameMapper.put("act-QTT-MI-Medical Interpretation I", "MI-Medical Interpretation I");
    	nodeNameMapper.put("act-QTT-MI-Clarification", "MI-Clarification");
    	nodeNameMapper.put("act-QTT-MI-Review", "MI-Review");
    	nodeNameMapper.put("act-QTT-MI-Medical Interpretation II", "MI-Medical Interpretation II");
    	nodeNameMapper.put("act-QTT-DMG-ManualAssignment", "DMG-Manual Assignment");
    	nodeNameMapper.put("act-QTT-DMG-DataSearch", "DMG-Data Search");
    	nodeNameMapper.put("act-QTT-DMG-Clarification", "DMG-Clarification");
    	nodeNameMapper.put("act-QTT-DMG-Review", "DMG-Review");
    	nodeNameMapper.put("act-QTT-SendToRequester", "Send To Requester");
    	nodeNameMapper.put("act-QTT-AwaitingFollow-up", "Awaiting Follow Up");
    }
	
	private static final String LAST_NODE_NAME = "end";

	private static final String GET_ALL_CLOSED_QUERIES = "SELECT q.query_seq, q.query_number, q.process_instance, q.workflow_route_type, q.current_due_date FROM QTT_JBPM_BACKUP.queries q"
		+ " WHERE q.status_code in ('CLOS','NSUB','REOP') AND q.process_instance is null and query_number not in ('QTT012102', 'QTT023768', 'QTT017096', 'QTT017188', 'QTT017200', 'QTT017201', 'QTT017202', 'QTT017203', 'QTT017204', 'QTT017205', 'QTT025905', 'QTT027470', 'QTT028021')";

	private static final String GET_ALL_OPEN_QUERIES = "SELECT q.query_seq, q.query_number, q.process_instance, q.workflow_route_type, q.current_due_date FROM QTT_JBPM_BACKUP.queries q"
		+ " WHERE q.status_code = 'PROC' AND q.process_instance is null";

	private static final String GET_ALL_TASK_IDS = "SELECT id_, name_ FROM QTT_JBPM_BACKUP.jbpm_task";
	
	private static final String GET_ALL_NODE_IDS = "SELECT id_, name_ FROM QTT_JBPM_BACKUP.jbpm_node";
	
	private static final String GET_JOB_DATA = "select name_, duedate_, repeat_, timeraction_ from QTT_JBPM_BACKUP.jbpm_action where timeraction_ ="
		+ " (select id_ from jbpm_action where actiondelegation_ ="
		+ " (select id_ from jbpm_delegation where classname_ = ?))";
	
	private static final String INSERT_MODULE_DEFINITION = "INSERT INTO QTT_JBPM_BACKUP.jbpm_moduledefinition (id_, class_, name_, processdefinition_, starttask_)"
		+ " VALUES (:id, :class, :name, :processdefinition, NULL)";
	
	private static final String INSERT_PROCESS_INSTANCE = "INSERT INTO QTT_JBPM_BACKUP.jbpm_processinstance (id_, version_, key_, start_, end_, issuspended_, processdefinition_, roottoken_, superprocesstoken_)"
		+ " VALUES (:id, 0, :key, NULL, NULL, 0, :processdefinition, NULL, NULL)";
		//+ " VALUES (:id, 0, :key, :start, :end, false, :processdefinition, NULL, NULL)"; dates added while update
	
	private static final String INSERT_TOKEN = "INSERT INTO QTT_JBPM_BACKUP.jbpm_token (id_, version_, name_, start_, end_, nodeenter_, nextlogindex_, isabletoreactivateparent_,"
        + " isterminationimplicit_, issuspended_, lock_, node_, processinstance_, parent_, subprocessinstance_)"
        + " VALUES (:id, 0, :name, :start, :end, :nodeEnter, 0, :isAbleToReactivateParent, 0, 0, NULL, :lastNodeId, :processInstanceId, :parent, NULL)";
        //+ " VALUES (:id, 0, NULL, :start, :end, :nodeenter, 0, 'false', 'false', 'false', NULL, :lastNodeId, :processInstanceId, NULL, NULL)"; dates added while update
	
	private static final String INSERT_MODULE_INSTANCE = "INSERT INTO QTT_JBPM_BACKUP.jbpm_moduleinstance (id_, class_, version_, processinstance_, taskmgmtdefinition_, name_)"
		+ " VALUES (:id, :class, 0, :processInstanceId, :taskMgmtDefinition, :name)";
	
	private static final String INSERT_TASK_INSTANCE = "INSERT INTO QTT_JBPM_BACKUP.jbpm_taskinstance (id_, class_, version_, name_, description_, actorid_,"
        + " create_, start_, end_, duedate_, priority_, iscancelled_, issuspended_, isopen_, issignalling_, isblocking_, task_, token_,"
        + " procinst_, swimlaninstance_, taskmgmtinstance_)"
        + " VALUES (:id, 'T', 0, :name, NULL, :actorId, :create, :start, :end, NULL,"
        //+ " VALUES (:id, 'T', 0, :name, NULL, :actorId, '2011-04-11 10:40:00', '2011-04-11 10:40:00', '2011-04-11 10:40:01', NULL,"
        //+ " VALUES (:id, 'T', 0, :name, NULL, :actorId, :create, :start, :end, NULL," 
        + " 3, :isCancelled, 0, :isOpen, :issignalling, 0, :taskId, :tokenId, :processInstanceId, NULL, :taskMgmtInstance)";
	
	private static final String INSERT_TOKEN_VARIABLE_MAP = "INSERT INTO QTT_JBPM_BACKUP.jbpm_tokenvariablemap (id_, version_, token_, contextinstance_)"
		+ " VALUES (:id, 0, :tokenId, :contextInstance)";		
		
	private static final String INSERT_VARIABLE_INSTANCE = "INSERT INTO QTT_JBPM_BACKUP.jbpm_variableinstance (id_, class_, version_, name_, converter_,"
		+ " token_, tokenvariablemap_, processinstance_, bytearrayvalue_, datevalue_, doublevalue_, longidclass_, longvalue_, stringidclass_, stringvalue_, taskinstance_)"
		+ " VALUES (:id, :class, 0, :name, NULL, :tokenId, :tokenVariableMapId, :processInstanceId, NULL, NULL, NULL, NULL, :longValue, NULL, :stringValue, NULL)";

	private static final String INSERT_VARIABLE_INSTANCE_SIMPLE = "INSERT INTO QTT_JBPM_BACKUP.jbpm_variableinstance (id_, class_, version_, name_, converter_,"
		+ " token_, tokenvariablemap_, processinstance_, bytearrayvalue_, datevalue_, doublevalue_, longidclass_, longvalue_, stringidclass_, stringvalue_, taskinstance_)"
		+ " VALUES (:currentVariableId, 'S', 0, :name, NULL, :rootTokenId, :tokenVariableMapId, :id, NULL, NULL, NULL, NULL, NULL, NULL, :stringValue, NULL)";
		
	private static final String INSERT_JOB_INSTANCE = "INSERT INTO QTT_JBPM_BACKUP.jbpm_job (id_, class_, version_, duedate_, processinstance_, token_, taskinstance_,"
		+ " issuspended_, isexclusive_, lockowner_, locktime_, exception_, retries_, name_, repeat_, transitionname_, action_, graphelementtype_,"
		+ " graphelement_, node_)"
		+ " VALUES (:id, 'T', 0, :dueDate, :processInstanceId, :rootTokenId, :taskInstanceId, 0, 0, NULL, NULL, NULL, 1, :name, :repeat, :transitionName, :actionId, 'org.jbpm.taskmgmt.def.Task',"
		+ " :graphElementId, NULL)";
	
	private static final String UPADTE_QUERY_PROCESS_INSTANCE_ID = "UPDATE QTT_JBPM_BACKUP.queries SET process_instance = :processInstanceId"
		+ " WHERE QUERY_SEQ = :QUERY_SEQ";
								
	private static final String UPADTE_PROCESS_INSTANCE_TOKEN = "UPDATE QTT_JBPM_BACKUP.jbpm_processinstance SET roottoken_ = :rootTokenId"
		+ " WHERE id_ = :id";
	
	private static final String UPDATE_PROCESS_INSTANCE_DATES = "UPDATE QTT_JBPM_BACKUP.jbpm_processinstance SET start_ = :start, end_ = :end"
		+ " WHERE id_ = :id";
	
	private static final String UPDATE_TOKEN_DATA = "UPDATE QTT_JBPM_BACKUP.jbpm_token SET start_ = :start, end_ = :end, nodeenter_ = :nodeEnter, isabletoreactivateparent_ = :isAbleToReactivateParent,"
		+ " node_ = :lastNodeId WHERE id_ = :id";
	
	private static final String UPDATE_VARIABLE = "UPDATE QTT_JBPM_BACKUP.jbpm_variableinstance SET stringvalue_ = :initiatorId"
		+ " WHERE processinstance_ = :id AND name_ = 'PROCESS_INSTANCE_START_ACTOR_ID'";	

	private static final String GET_LAST_MODULE_DEFINIOTION_ID = "SELECT MAX(id_) FROM QTT_JBPM_BACKUP.jbpm_moduledefinition";
	
	private static final String GET_LAST_PROCESS_INSTANCE_ID = "SELECT MAX(id_) FROM QTT_JBPM_BACKUP.jbpm_processinstance";
	
	private static final String GET_LAST_TOKEN_ID = "SELECT MAX(id_) FROM QTT_JBPM_BACKUP.jbpm_token";
	
	private static final String GET_LAST_MODULE_INSTANCE_ID = "SELECT MAX(id_) FROM QTT_JBPM_BACKUP.jbpm_moduleinstance";
	
	private static final String GET_LAST_NODE_ID = "SELECT id_ FROM QTT_JBPM_BACKUP.jbpm_node WHERE name_ = '" + LAST_NODE_NAME + "'";
	
	private static final String GET_LAST_TASK_INSTANCE_ID = "SELECT MAX(id_) FROM QTT_JBPM_BACKUP.jbpm_taskinstance";
	
	private static final String GET_LAST_TOKEN_VARIABLE_MAP_ID = "SELECT MAX(id_) FROM QTT_JBPM_BACKUP.jbpm_tokenvariablemap";
	
	private static final String GET_LAST_VARIABLE_ID = "SELECT MAX(id_) FROM QTT_JBPM_BACKUP.jbpm_variableinstance";
	
	private static final String GET_LAST_JOB_ID = "SELECT MAX(id_) FROM QTT_JBPM_BACKUP.jbpm_job";
	
	private static final String GET_TASK_MGMT_DEFINITION_ID = "SELECT id_ FROM QTT_JBPM_BACKUP.jbpm_moduledefinition WHERE name_ = 'org.jbpm.taskmgmt.def.TaskMgmtDefinition'";
		
	private static final String GET_PROCESS_DEFINITION_ID = "SELECT id_ FROM QTT_JBPM_BACKUP.jbpm_processdefinition";
	
	private SimpleJdbcTemplate jdbc;

	public MigrationHelper(SimpleJdbcTemplate jdbc) {
		super();
		this.jdbc = jdbc;
	}
	
	public static SingleConnectionDataSource createDs(String dbUrl, String dbUserName, String dbPassword) {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Unable to load JDBC driver", e);
		}
		return new SingleConnectionDataSource(dbUrl, dbUserName, dbPassword, true);
	}
	
	public void commit() {
		jdbc.update("COMMIT");
	}
	
	public void rollback() {
		jdbc.update("ROLLBACK");
	}	
	
	public Long getNextModuleDefinitionId() {
		return jdbc.queryForLong(GET_LAST_MODULE_DEFINIOTION_ID) + 1;
	}
	
	public Long getNextProcessInstanceId() {
		return jdbc.queryForLong(GET_LAST_PROCESS_INSTANCE_ID) + 1;
	}
	
	public Long getNextTokenId() {
		return jdbc.queryForLong(GET_LAST_TOKEN_ID) + 1;
	}
	public Long getNextModuleInstanceId() {
		return jdbc.queryForLong(GET_LAST_MODULE_INSTANCE_ID) + 1;
	}
	
	public Long getNextTaskInstanceId() {
		return jdbc.queryForLong(GET_LAST_TASK_INSTANCE_ID) + 1;
	}
	
	public Long getNextTokenVariableMapId() {
		return jdbc.queryForLong(GET_LAST_TOKEN_VARIABLE_MAP_ID) + 1;
	}
	
	public Long getNextVariableId() {
		return jdbc.queryForLong(GET_LAST_VARIABLE_ID) + 1;
	}
	
	public Long getNextJobId() {
		return jdbc.queryForLong(GET_LAST_JOB_ID) + 1;
	}
	
	public Long getProcessDefinitionId() {
		return jdbc.queryForLong(GET_PROCESS_DEFINITION_ID);
	}
	
	public Map<String, Object> getTaskIds() {
		Map<String, Object> taskInfo = new HashMap<String, Object>();
		List<Map<String, Object>> tasksIds = jdbc.queryForList(GET_ALL_TASK_IDS);
		Map<String, Object> task = null;
		for(Iterator<Map<String, Object>> iter = tasksIds.iterator(); iter.hasNext();) {
			task = iter.next();
			taskInfo.put((String) task.get("name_"), task.get("id_"));
		}
		return taskInfo;
	}
	
	public Map<String, Object> getNodeIds() {
		Map<String, Object> nodeInfo = new HashMap<String, Object>();
		List<Map<String, Object>> nodeIds = jdbc.queryForList(GET_ALL_NODE_IDS);
		Map<String, Object> node = null;
		for(Iterator<Map<String, Object>> iter = nodeIds.iterator(); iter.hasNext();) {
			node = iter.next();
			nodeInfo.put((String) node.get("name_"), node.get("id_"));
		}
		return nodeInfo;
	}
	
	public Map<String, Object> getJobData(String className) {
		Map<String, Object> jobInfo = jdbc.queryForMap(GET_JOB_DATA, new Object[] {className});
		return jobInfo;
	}
	
	public Long getTaskMgmtDefinitionId() {
		return jdbc.queryForLong(GET_TASK_MGMT_DEFINITION_ID);
	}
	
	public Long getLastNodeId() {
		return jdbc.queryForLong(GET_LAST_NODE_ID);
	}	
	
	public List<Map<String, Object>> getAllClosedQueries() {
		List<Map<String, Object>> closedQueries = jdbc.queryForList(GET_ALL_CLOSED_QUERIES);
		return closedQueries;
	}
	
	public List<Map<String, Object>> getAllOpenQueries() {
		List<Map<String, Object>> openQueries = jdbc.queryForList(GET_ALL_OPEN_QUERIES);
		return openQueries;
	}
	
	public void batchInsertModuleDefinition(Map[] moduleDefinitionArgumentsList) {
		jdbc.batchUpdate(INSERT_MODULE_DEFINITION, moduleDefinitionArgumentsList);
	}
	
	public void batchInsertProcessInstance(Map[] processInstanceArgumentsList) {
		jdbc.batchUpdate(INSERT_PROCESS_INSTANCE, processInstanceArgumentsList);
	}
	
	public void batchInsertToken(Map[] tokenArgumentsList) {
		jdbc.batchUpdate(INSERT_TOKEN, tokenArgumentsList);
	}
	
	public void batchInsertModuleInstance(Map[] moduleInstanceArgumentsList) {
		jdbc.batchUpdate(INSERT_MODULE_INSTANCE, moduleInstanceArgumentsList);
	}
	
	public void batchInsertTaskInstance(Map[] taskInstanceArgumentsList) {
		jdbc.batchUpdate(INSERT_TASK_INSTANCE, taskInstanceArgumentsList);
	}
	
	public void batchInsertTokenVariableMap(Map[] tokenVariableArgumentsList) {
		jdbc.batchUpdate(INSERT_TOKEN_VARIABLE_MAP, tokenVariableArgumentsList);
	}
	
	public void batchInsertVariableInstance(Map[] variableArgumentsList) {
		jdbc.batchUpdate(INSERT_VARIABLE_INSTANCE, variableArgumentsList);
	}
	
	public void batchInsertVariableInstanceSimple(Map[] variableArgumentsList) {
		jdbc.batchUpdate(INSERT_VARIABLE_INSTANCE_SIMPLE, variableArgumentsList);
	}
	
	public void batchInsertJob(Map[] jobArgumentsList) {
		jdbc.batchUpdate(INSERT_JOB_INSTANCE, jobArgumentsList);
	}
	public void batchUpdateQuery(Map[] queryArgumentsList) {
		jdbc.batchUpdate(UPADTE_QUERY_PROCESS_INSTANCE_ID, queryArgumentsList);
	}
	
	public void batchUpdateProcessInstance(Map[] processInstanceArgumentsList) {
		jdbc.batchUpdate(UPADTE_PROCESS_INSTANCE_TOKEN, processInstanceArgumentsList);
	}
	
	public void batchUpdateProcessInstanceDates(Map[] processInstanceArgumentsList) {
		jdbc.batchUpdate(UPDATE_PROCESS_INSTANCE_DATES, processInstanceArgumentsList);
	}
	
	public void batchUpdateTokenData(Map[] tokenArgumentsList) {
		jdbc.batchUpdate(UPDATE_TOKEN_DATA, tokenArgumentsList);
	}	
	
	public void batchUpdateVariable(Map[] variableArgumentsList) {
		jdbc.batchUpdate(UPDATE_VARIABLE, variableArgumentsList);
	}
	
}