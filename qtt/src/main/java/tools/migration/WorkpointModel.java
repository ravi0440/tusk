package tools.migration;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class WorkpointModel {
	
	private String queryId;
	private String jobId;
	private Timestamp tokenStart;
	private Timestamp tokenEnd;
	private Timestamp tokenLastUpdate;
	private String initiatorId;
	private Map<String, String> variables;
	private List<WorkpointTask> tasks = new ArrayList<WorkpointTask>();

	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public Timestamp getTokenStart() {
		return tokenStart;
	}

	public void setTokenStart(Timestamp tokenStart) {
		this.tokenStart = tokenStart;
	}

	public Timestamp getTokenEnd() {
		return tokenEnd;
	}

	public void setTokenEnd(Timestamp tokenEnd) {
		this.tokenEnd = tokenEnd;
	}
		
	public Timestamp getTokenLastUpdate() {
		return tokenLastUpdate;
	}

	public void setTokenLastUpdate(Timestamp tokenLastUpdate) {
		this.tokenLastUpdate = tokenLastUpdate;
	}

	public String getInitiatorId() {
		return initiatorId;
	}

	public void setInitiatorId(String initiatorId) {
		this.initiatorId = initiatorId;
	}

	public Map<String, String> getVariables() {
		return variables;
	}

	public void setVariables(Map<String, String> variables) {
		this.variables = variables;
	}
	
	public List<WorkpointTask> getTasks() {
		return tasks;
	}

	public void setTasks(List<WorkpointTask> tasks) {
		this.tasks = tasks;
	}
		
}