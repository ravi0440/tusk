package tools.migration;

public class WorkpointActor {
	
	String actorId;
	String lastActorId;
	
	public String getActorId() {
		return actorId;
	}
	public void setActorId(String actorId) {
		this.actorId = actorId;
	}
	public String getLastActorId() {
		return lastActorId;
	}
	public void setLastActorId(String lastActorId) {
		this.lastActorId = lastActorId;
	}	
	
}