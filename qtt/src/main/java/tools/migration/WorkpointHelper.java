package tools.migration;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import com.roche.dss.qtt.security.model.DSSUser;
import com.roche.dss.qtt.security.utils.storeprocedures.DSSStoreProceduresConstants;
import com.roche.dss.qtt.security.utils.storeprocedures.user.DSSUserStoredProcedure;
import com.roche.dss.qtt.security.utils.storeprocedures.user.DSSUsersStoredProcedure;
import com.roche.dss.qtt.service.workflow.WorkflowGlobalConstants;


public class WorkpointHelper implements WorkflowGlobalConstants {
	
	private static final Logger logger = Logger.getLogger(WorkpointHelper.class);
	
	private static final String WORKPOINT_SCHEMA = "qtt_wkpt_backup_20120514";
	
	private static final String GET_JOB_FOR_QUERY_NUMBER = "SELECT * FROM " + WORKPOINT_SCHEMA + ".wp_proci WHERE proci_ref = ?";
	
	private static final String GET_WORK_ITEMS_FOR_JOB_ID = "SELECT * FROM " + WORKPOINT_SCHEMA + ".wp_work_item WHERE proci_id = ? ORDER BY acti_id";
	
	private static final String GET_WORK_ITEMS_ACTOR_FOR_JOB_ID = "SELECT * FROM " + WORKPOINT_SCHEMA + ".wp_work_item_part WHERE proci_id = ? and acti_id = ?";
	
//	private static final String GET_JOB_FOR_QUERY_NUMBER = "SELECT * FROM qtt_wkpt_tmp.wp_proci WHERE proci_ref = ?";
//	
//	private static final String GET_WORK_ITEMS_FOR_JOB_ID = "SELECT * FROM qtt_wkpt_tmp.wp_work_item WHERE proci_id = ? ORDER BY acti_id";
//	
//	private static final String GET_WORK_ITEMS_ACTOR_FOR_JOB_ID = "SELECT * FROM qtt_wkpt_tmp.wp_work_item_part WHERE proci_id = ? and acti_id = ?";


	private static final int JOB_STATUS_ = 4;//co to za status
	
	private static final int JOB_STATUS_ACTIVE = 3;
	
	private static final int JOB_STATUS_CLOSED = 5;
	
	private static final int JOB_STATUS_CANCELLED = 6;
	
	private static final String WORKPOINT_ACTOR_ID = "WorkPoint";
		
	private SimpleJdbcTemplate jdbc;
	
	private DataSource securityDs;
	
	private Map<String, String> pSUsersMail = new HashMap<String, String>();
	
	
	@SuppressWarnings("unchecked")
	public WorkpointHelper(SimpleJdbcTemplate jdbc, DataSource securityDs) {
		super();
		this.jdbc = jdbc;
		this.securityDs = securityDs;
		List<DSSUser> resourceList = null;
		Map<String, Object> users = getDSSUsersWithPerm("PS", "QTT");
		resourceList = (ArrayList<DSSUser>) users.get(USER_SERVICE_GET_USERS_OUT_PARAM);
		for(DSSUser user : resourceList) {
			pSUsersMail.put(user.getUserName().toUpperCase(), user.getEmail());
		}
		users = getDSSUsersWithPerm("QTTUser", "PORTAL");
		resourceList = (ArrayList<DSSUser>) users.get(USER_SERVICE_GET_USERS_OUT_PARAM);
		for(DSSUser user : resourceList) {
			pSUsersMail.put(user.getUserName().toUpperCase(), user.getEmail());
		}
	}
	
	public static SingleConnectionDataSource createDs(String dbUrl, String dbUserName, String dbPassword) {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Unable to load JDBC driver", e);
		}
		return new SingleConnectionDataSource(dbUrl, dbUserName, dbPassword, true);
	}

	public WorkpointModel getWorkpointData(String queryNumber, DateTime currentDueDate, boolean openQuery) {
		WorkpointModel workpointModel = jdbc.queryForObject(GET_JOB_FOR_QUERY_NUMBER, new WorkpointModelMapper(), new Object[] {queryNumber});
		
		List<WorkpointTask> workpointTasks = jdbc.query(GET_WORK_ITEMS_FOR_JOB_ID, new WorkpointTaskMapper(), new Object[] {workpointModel.getJobId()});
		
		Collections.sort(workpointTasks, new WorkPointTaskComparator());
		
		Map<String, String> variables = new HashMap<String, String>();
		
		for(WorkpointTask workpointTask : workpointTasks) {
			if(workpointTask.getTaskEnd() == null) {
				if(openQuery && (workpointTask.getTaskStatus() == JOB_STATUS_ACTIVE || workpointTask.getTaskStatus() == JOB_STATUS_)) {
					//logger.debug("token for query: " + queryNumber + " having task with name: " +workpointTask.getTaskName() + " will be marked as open");
					workpointModel.setTokenEnd(null);
				}
				if(workpointTask.getActorId() == null) {
					List<WorkpointActor> actorIds = jdbc.query(GET_WORK_ITEMS_ACTOR_FOR_JOB_ID, new ActorMapper(), new Object[] {workpointModel.getJobId(), workpointTask.getTaskId()});
					if (actorIds.size() == 1) {
						workpointTask.setActorId(actorIds.get(0).getActorId());
					} else if(WorkpointActivities.DMG_ASSESS.getActivityName().equals(workpointTask.getTaskName())) {
						workpointTask.setActorId("DMGCO");
					} else if (WorkpointActivities.AWAITING_FOLLOWUP.getActivityName().equals(workpointTask.getTaskName()) || WorkpointActivities.SEND_TO_REQUESTER.getActivityName().equals(workpointTask.getTaskName()) || WorkpointActivities.ASSIGN.getActivityName().equals(workpointTask.getTaskName())) {
						workpointTask.setActorId("DSCL");
					} else if(actorIds.size() > 1) {
						workpointTask.setActorId(actorIds.get(0).getLastActorId());
					} else if (workpointTask.getTaskStatus() == JOB_STATUS_ACTIVE || workpointTask.getTaskStatus() == JOB_STATUS_) {
						workpointTask.setActorId("DSCL");
					}
				}
				if(WorkpointActivities.MEDICAL_INTERPRETATION_I.getActivityName().equals(workpointTask.getTaskName()) && workpointTask.getTaskStatus() != JOB_STATUS_CANCELLED) {
					DateTime now = new DateTime();
		        	DateTime now3 = now.plusDays(3);		        	
		        	if(now3.isAfter(currentDueDate)) {
		        		variables.put(DUE_SOON_ALERT, currentDueDate.minusDays(3).toString());
		        		workpointTask.setTimer("com.roche.dss.qtt.service.workflow.handler.QTTMIDueSoonHandler");
		        	} else if(now3.isBefore(currentDueDate)) {
		        		workpointTask.setTimer("com.roche.dss.qtt.service.workflow.handler.QTTMIDueSoonHandler");
		        	}
				} else if(WorkpointActivities.AWAITING_FOLLOWUP.getActivityName().equals(workpointTask.getTaskName()) && workpointTask.getTaskStatus() != JOB_STATUS_CANCELLED) {
					workpointTask.setTimer("com.roche.dss.qtt.service.workflow.handler.QTTCloseTimerHandler");
				}
			}
			if (workpointTask.getTaskStatus() != JOB_STATUS_CANCELLED) {
				if(WorkpointActivities.MEDICAL_INTERPRETATION_I.getActivityName().equals(workpointTask.getTaskName())) {
					String userMail = pSUsersMail.get(workpointTask.getActorId().toUpperCase());
					if(userMail != null) {
						variables.put(PS_ASSIGNED_ACTOR_EMAIL, userMail);
					} else {
						logger.debug("NULL email for user: " + workpointTask.getActorId().toUpperCase());
						DSSUser dssUser = getDSSUser(workpointTask.getActorId());
						if(dssUser != null) {
							logger.info("Got email for user: " + workpointTask.getActorId().toUpperCase());
							pSUsersMail.put(workpointTask.getActorId().toUpperCase(), dssUser.getEmail());
							variables.put(PS_ASSIGNED_ACTOR_EMAIL, dssUser.getEmail());
						}
					}
					workpointModel.setVariables(variables);					
				}
				if(WorkpointActivities.MEDICAL_INTERPRETATION_I.getActivityName().equals(workpointTask.getTaskName())) {
					variables.put(PS_ASSIGNED_ACTOR_ID, workpointTask.getActorId());
				} else if(WorkpointActivities.DMG_DATA_SEARCH.getActivityName().equals(workpointTask.getTaskName())) {
					variables.put(DMG_ASSIGNED_ACTOR_ID, workpointTask.getActorId());
				}
			}
			if(workpointTask.getTaskEnd() == null) {
				if(workpointTask.getTaskStatus() != JOB_STATUS_CANCELLED) {
					if(workpointTask.getTaskName().matches(".*MI.*")) {
						variables.put(PS_ASSIGNED_ACTOR_ID, workpointTask.getActorId());
					} else if(!WorkpointActivities.DMG_ASSESS.getActivityName().equals(workpointTask.getTaskName()) && workpointTask.getTaskName().matches(".*DMG.*")) {
						variables.put(DMG_ASSIGNED_ACTOR_ID, workpointTask.getActorId());
					}
					
				}
			}
		}
		
		List<WorkpointTask> allWorkpointTasks = prepareJobStartActivities(workpointTasks, workpointModel.getInitiatorId());
		
		Collections.sort(allWorkpointTasks, new WorkPointTaskComparator());
		
		workpointModel.setVariables(variables);
	
		workpointModel.setTasks(allWorkpointTasks);

		return workpointModel;
	}
	
	private List<WorkpointTask> prepareJobStartActivities(List<WorkpointTask> workpointTasks, String initiatorId) {
		
		List<WorkpointTask> allWorkpointTasks = new ArrayList<WorkpointTask>(workpointTasks);
		boolean initiatorIdAdded = false;
		
		for(WorkpointTask workpointTask : workpointTasks) {
			if(WorkpointActivities.ASSIGN.getActivityName().equals(workpointTask.getTaskName()) && workpointTask.getTaskStatus() != JOB_STATUS_CANCELLED) {
				WorkpointTask jobStart = new WorkpointTask();
				jobStart.setTaskName(WorkpointActivities.JOB_START.getActivityName());
				jobStart.setActorId(workpointTask.getActorId());
				if(!initiatorIdAdded) {
					jobStart.setActorId(initiatorId);
					initiatorIdAdded = true;
				}
				DateTime dateHelper = new DateTime(workpointTask.getTaskCreate().getTime());
		     	DateTime create = dateHelper.minusMillis(1);
				jobStart.setTaskCreate(new Timestamp(((Date) create.toDate()).getTime()));
				jobStart.setTaskStart(new Timestamp(((Date) create.toDate()).getTime()));
				jobStart.setTaskEnd(new Timestamp(((Date) create.toDate()).getTime()));
				jobStart.setTaskLastUpdate(new Timestamp(((Date) create.toDate()).getTime()));
				jobStart.setTaskStatus(JOB_STATUS_CLOSED);
				allWorkpointTasks.add(jobStart);
			}
		}
		return allWorkpointTasks;
	}
	
    public Map<String, Object> getDSSUsersWithPerm(String permission, String app) {
        DSSUsersStoredProcedure sproc = new DSSUsersStoredProcedure(securityDs, false);
        Map<String, Object> result = sproc.execute(app, new String[]{permission});
        return result;
    }
	
    @SuppressWarnings("unchecked")
	private DSSUser getDSSUser(String actorId) {
        DSSUserStoredProcedure sproc = new DSSUserStoredProcedure(securityDs);
        List<DSSUser> result = (List<DSSUser>) sproc.execute(actorId).get(DSSStoreProceduresConstants.USER_SERVICE_GET_USER_OUT_PARAM);
        if(CollectionUtils.isNotEmpty(result)){
            return result.get(0);
        }else{
            return null;
        }
    }	
	
	private class WorkPointTaskComparator implements Comparator<WorkpointTask>, Serializable {

		private static final long serialVersionUID = 5794808832172423110L;

		@Override
		public int compare(WorkpointTask t1, WorkpointTask t2) {
	        
			Timestamp create1 = t1.getTaskCreate();
			Timestamp create2 = t2.getTaskCreate();

			if ( create1 == null )
				return -1;
			if ( create2 == null )
				return 1;
			
	        return create1.compareTo(create2);
	    }
	}
	
	private static final class WorkpointModelMapper implements RowMapper<WorkpointModel> {

		@Override
	    public WorkpointModel mapRow(ResultSet rs, int rowNum) throws SQLException {
	    	WorkpointModel workpointModel = new WorkpointModel();
	    	workpointModel.setQueryId(rs.getString("PROCI_REF"));
	    	workpointModel.setJobId(rs.getString("PROCI_ID"));
	    	workpointModel.setInitiatorId(rs.getString("INITIATOR_ID"));
	    	workpointModel.setTokenStart(rs.getTimestamp("START_DATE"));
	    	workpointModel.setTokenLastUpdate(rs.getTimestamp("LU_DATE"));
	    	workpointModel.setTokenEnd(rs.getTimestamp("LU_DATE"));
	        return workpointModel;
	    }
	}
	
	private static final class WorkpointTaskMapper implements RowMapper<WorkpointTask> {

		@Override
	    public WorkpointTask mapRow(ResultSet rs, int rowNum) throws SQLException {
	    	WorkpointTask workpointTask = new WorkpointTask();
	    	workpointTask.setTaskId(rs.getLong("ACTI_ID"));
			workpointTask.setTaskCreate(rs.getTimestamp("AVAILABLE_DATE"));
			workpointTask.setTaskStart(rs.getTimestamp("OPEN_DATE"));
			workpointTask.setActorId(rs.getString("ASSIGNED_TO"));
			if(WORKPOINT_ACTOR_ID.equals(rs.getString("ASSIGNED_TO"))) {
				workpointTask.setActorId("DSCL");
			} 
			workpointTask.setTaskName(rs.getString("NAME"));
			workpointTask.setTaskStatus(rs.getInt("WORK_STATE_ID"));
			workpointTask.setTaskEnd(rs.getTimestamp("COMPLETE_DATE"));
			workpointTask.setTaskLastUpdate(rs.getTimestamp("LU_DATE"));
			return workpointTask;
	    }
	}
	
	private static final class ActorMapper implements RowMapper<WorkpointActor> {

		@Override
	    public WorkpointActor mapRow(ResultSet rs, int rowNum) throws SQLException {
			WorkpointActor workpointActor = new WorkpointActor();
			workpointActor.setActorId(rs.getString("RESC_ID"));
			workpointActor.setLastActorId(rs.getString("LU_ID"));
			return workpointActor;
	    }
	}
	
	public enum WorkpointActivities {
		
		JOB_START(0, "Job Start"),
		ASSIGN(1, "act-QTT-Query-Assignment"),
	    PS_ASSIGN(2, "act-QTT-PS-ManualAssignment"),
	    MEDICAL_INTERPRETATION_I(3, "act-QTT-MI-Medical Interpretation I"),
	    DMG_ASSESS(7, "act-QTT-DMG-ManualAssignment"),
	    DMG_DATA_SEARCH(8, "act-QTT-DMG-DataSearch"),
	    SEND_TO_REQUESTER(11, "act-QTT-SendToRequester"),
	    AWAITING_FOLLOWUP(12, "act-QTT-AwaitingFollow-up");
	    
	    public int activityTypeId;
	    public String activityName;
	    
	    WorkpointActivities(int activityTypeId, String activityName) {
	    	this.activityTypeId = activityTypeId;
	    	this.activityName = activityName;
	    }

		public int getActivityTypeId() {
			return activityTypeId;
		}

		public String getActivityName() {
			return activityName;
		}
		
	}

}
