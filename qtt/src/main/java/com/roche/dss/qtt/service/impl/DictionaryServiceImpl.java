package com.roche.dss.qtt.service.impl;

import com.roche.dss.qtt.model.AttachmentCategory;
import com.roche.dss.qtt.model.LdocType;
import com.roche.dss.qtt.model.QttCountry;
import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.model.ReporterType;
import com.roche.dss.qtt.model.WorkflowRouteType;
import com.roche.dss.qtt.query.comms.dao.DictionaryDAO;
import com.roche.dss.qtt.service.DictionaryService;
import javax.annotation.Resource;
import java.util.List;

/**
 * User: pruchnil
 */
@Service("dictionaryService")
public class DictionaryServiceImpl implements DictionaryService {
    private DictionaryDAO dictionaryDAO;

    @Resource
    public void setDictionaryDAO(DictionaryDAO dictionaryDAO) {
        this.dictionaryDAO = dictionaryDAO;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<AttachmentCategory> getAllAttachmentCategories() {
        return dictionaryDAO.getAllAttachmentCategories();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getUsedRetrievalNames() {
    	return dictionaryDAO.getUsedRetrievalNames();
    }

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<String> getUsedGenericNames() {
		return dictionaryDAO.getUsedGenericNames();
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<String> getUsedIndications() {
		return dictionaryDAO.getUsedIndications();
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<QueryType> getQueryTypes() {
		List<QueryType> list = dictionaryDAO.getQueryTypes();  
    	return list;
    }
	
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<QueryType> getAllQueryTypes() {
		List<QueryType> list = dictionaryDAO.getAllQueryTypes();  
    	return list;
    }
	
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getUsedResponseAuthors() {
    	return dictionaryDAO.getUsedResponseAuthors();
    }
    
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<LdocType> getUsedLabellingDocs() {
    	return dictionaryDAO.getUsedLabellingDocs();
    }
    
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<WorkflowRouteType> getProcessTypes() {
    	return dictionaryDAO.getProcessTypes();
    }
    
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getDMGUserInvolvedNames() {
    	return dictionaryDAO.getDMGUserInvolvedNames();
    }
	
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getPSUserInvolvedNames() {
    	return dictionaryDAO.getPSUserInvolvedNames();
    }

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getDSCLUserInvolvedNames() {
    	return dictionaryDAO.getDSCLUserInvolvedNames();
    }
    
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getUsedRequestorLastNames() {
    	return dictionaryDAO.getUsedRequestorLastNames();
    }
    
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<QttCountry> getUsedRequestorCountries() {
    	return dictionaryDAO.getUsedRequestorCountries();
    }
    
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ReporterType> getUsedReporterTypes() {
    	return dictionaryDAO.getUsedReporterTypes();
    }
    
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<QttCountry> getUsedSourceCountries() {
    	return dictionaryDAO.getUsedSourceCountries();
    }

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getAllTaskNames() {
    	return dictionaryDAO.getAllTaskNames();
    }

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<String> getAllQueryTypeNames() {
    	return dictionaryDAO.getAllQueryTypeNames();
    }
}