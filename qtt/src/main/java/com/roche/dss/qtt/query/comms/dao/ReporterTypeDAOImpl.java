package com.roche.dss.qtt.query.comms.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.roche.dss.qtt.query.comms.dao.impl.JDBCTemplateDAO;
import com.roche.dss.qtt.query.comms.dto.ReporterTypeDTO;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
@Repository
public class ReporterTypeDAOImpl extends JDBCTemplateDAO implements ReporterTypeDAO {

    private static final Logger logger = LoggerFactory.getLogger(ReporterTypeDAOImpl.class);

    @Override
    public List<ReporterTypeDTO> getReporterTypes()  {
        String queryStr = "select reporter_type_id, reporter_type from reporter_types";
        return jdbcTemplate.query(queryStr, new ReporterTypesDTORowMapper());
    }

     class ReporterTypesDTORowMapper implements RowMapper<ReporterTypeDTO> {
        @Override
        public ReporterTypeDTO mapRow(ResultSet resultSet, int i) throws SQLException {
            ReporterTypeDTO reportTypesDTO = new ReporterTypeDTO();
            reportTypesDTO.setReporterTypeId(resultSet.getString("reporter_type_id"));
            reportTypesDTO.setReporterType(resultSet.getString("reporter_type"));
            return reportTypesDTO;
        }
    }
}
