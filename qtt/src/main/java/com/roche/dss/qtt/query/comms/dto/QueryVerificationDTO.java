/* 
====================================================================
  $Header$
  @author $Author: savovp $
  @version $Revision: 2654 $ $Date: 2013-01-31 17:15:50 +0100 (Cz, 31 sty 2013) $

====================================================================
*/
package com.roche.dss.qtt.query.comms.dto;

import java.util.Date;

import com.roche.dss.qtt.model.QueryVerification;



public class QueryVerificationDTO {
	
	private String requirementsUnderstood;
	private String clarificationRequested;
	private String assumptionsDocumented;
	private String furtherRequirements;
	private String buildType;
	private String correctParametersPassed;
	private String codeReviewPassed;
	private String checkPassed;
	private String resultsVerified;
	private String furtherTestReference;
	private String resultsAppropriatelyLabeled;
	private String resultsStored;
	private String searchCriteriaStated;
	
	private Date dateCompleted;
	private String filledBy;
	private QueryVerification.RECORD_TYPE recordType;
	
	
	
	public String getRequirementsUnderstood() {
		return requirementsUnderstood;
	}
	public void setRequirementsUnderstood(String requirementsUnderstood) {
		this.requirementsUnderstood = requirementsUnderstood;
	}
	public String getClarificationRequested() {
		return clarificationRequested;
	}
	public void setClarificationRequested(String clarificationRequested) {
		this.clarificationRequested = clarificationRequested;
	}
	public String getAssumptionsDocumented() {
		return assumptionsDocumented;
	}
	public void setAssumptionsDocumented(String assumptionsDocumented) {
		this.assumptionsDocumented = assumptionsDocumented;
	}
	public String getFurtherRequirements() {
		return furtherRequirements;
	}
	public void setFurtherRequirements(String furtherRequirements) {
		this.furtherRequirements = furtherRequirements;
	}
	public String getBuildType() {
		return buildType;
	}
	public void setBuildType(String buildType) {
		this.buildType = buildType;
	}
	public String getCorrectParametersPassed() {
		return correctParametersPassed;
	}
	public void setCorrectParametersPassed(String correctParametersPassed) {
		this.correctParametersPassed = correctParametersPassed;
	}
	public String getCodeReviewPassed() {
		return codeReviewPassed;
	}
	public void setCodeReviewPassed(String codeReviewPassed) {
		this.codeReviewPassed = codeReviewPassed;
	}
	public String getCheckPassed() {
		return checkPassed;
	}
	public void setCheckPassed(String checkPassed) {
		this.checkPassed = checkPassed;
	}
	public String getResultsVerified() {
		return resultsVerified;
	}
	public void setResultsVerified(String resultsVerified) {
		this.resultsVerified = resultsVerified;
	}
	public String getFurtherTestReference() {
		return furtherTestReference;
	}
	public void setFurtherTestReference(String furtherTestReference) {
		this.furtherTestReference = furtherTestReference;
	}
	public String getResultsAppropriatelyLabeled() {
		return resultsAppropriatelyLabeled;
	}
	public void setResultsAppropriatelyLabeled(String resultsAppropriatelyLabeled) {
		this.resultsAppropriatelyLabeled = resultsAppropriatelyLabeled;
	}
	public String getResultsStored() {
		return resultsStored;
	}
	public void setResultsStored(String resultsStored) {
		this.resultsStored = resultsStored;
	}
	public String getSearchCriteriaStated() {
		return searchCriteriaStated;
	}
	public void setSearchCriteriaStated(String searchCriteriaStated) {
		this.searchCriteriaStated = searchCriteriaStated;
	}
	public Date getDateCompleted() {
		return dateCompleted;
	}
	public void setDateCompleted(Date dateCompleted) {
		this.dateCompleted = dateCompleted;
	}
	public String getFilledBy() {
		return filledBy;
	}
	public void setFilledBy(String filledBy) {
		this.filledBy = filledBy;
	}
	public QueryVerification.RECORD_TYPE getRecordType() {
		return recordType;
	}
	public void setRecordType(QueryVerification.RECORD_TYPE recordType) {
		this.recordType = recordType;
	}
	
	public QueryVerificationDTO(){}
	
	public QueryVerificationDTO(QueryVerification qv){
		requirementsUnderstood =qv.getRequirementsUnderstood();
		 clarificationRequested=qv.getClarificationRequested();
		assumptionsDocumented=qv.getAssumptionsDocumented();
		 furtherRequirements=qv.getFurtherRequirements();
		 buildType=qv.getBuildType();
		 correctParametersPassed=qv.getCorrectParametersPassed();
		 codeReviewPassed=qv.getCodeReviewPassed();
		 checkPassed=qv.getCheckPassed();
		 resultsVerified=qv.getResultsVerified();
		 furtherTestReference=qv.getFurtherTestReference();
		 resultsAppropriatelyLabeled=qv.getResultsAppropriatelyLabeled();
		 resultsStored=qv.getResultsStored();
		 searchCriteriaStated=qv.getSearchCriteriaStated();
		
		 dateCompleted=qv.getDateCompleted();
		 filledBy=qv.getFilledBy();
		 
		 if(qv.getRecordType().equals( QueryVerification.RECORD_TYPE.FILLED_BY_OWNER.name())){
			 recordType=QueryVerification.RECORD_TYPE.FILLED_BY_OWNER;
		 }else  if(qv.getRecordType().equals( QueryVerification.RECORD_TYPE.FILLED_BY_VERIFIER.name())){
			 recordType=QueryVerification.RECORD_TYPE.FILLED_BY_VERIFIER;
		 }
		 
	}
	
    
    
}
