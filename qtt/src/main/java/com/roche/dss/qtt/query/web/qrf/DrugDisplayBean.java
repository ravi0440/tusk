/* 
====================================================================
  $Header$
  @author $Author: rahmanh1 $
  @version $Revision: 628 $ $Date: 2004-04-16 14:24:45 +0200 (Pt, 16 kwi 2004) $

====================================================================
*/
package com.roche.dss.qtt.query.web.qrf;

import com.roche.dss.qtt.query.comms.dto.DrugDTO;
import com.roche.dss.qtt.query.web.qrf.model.AbstractDisplayBean;

/**
 * Display-Bean for transporting information at the web tier, relating to the
 * 'QRF' screens.
 * @author wisbeya
 */
public class DrugDisplayBean extends AbstractDisplayBean {

	private boolean drugType;
	private	String retrDrug;
	private	String genericDrug;	
	private	String route;
	private	String formulation;
	private	String indication;
	private	String dose; 
	
	public DrugDisplayBean(DrugDTO dto) {
		
		this.drugType = dto.isRocheDrug() ? true : false; 
		this.retrDrug = dto.getRetrName();
		this.genericDrug = dto.getGenericName();
		this.route = dto.getRoute();
		this.formulation = dto.getFormulation();
		this.indication = dto.getIndication();
		this.dose = dto.getDose();
	}
    /**
     * @return
     */
    public String getDose() {
        return dose;
    }

    /**
     * @return
     */
    public String getFormulation() {
        return formulation;
    }

    /**
     * @return
     */
    public String getGenericDrug() {
        return genericDrug;
    }

    /**
     * @return
     */
    public String getIndication() {
        return indication;
    }

    /**
     * @return
     */
    public String getRetrDrug() {
        return retrDrug;
    }

    /**
     * @return
     */
    public String getRoute() {
        return route;
    }

    /**
     * @return
     */
    public String getDrugType() {
        return  drugType ? "Roche" : "Non-Roche";
    }

    /**
     * @param b
     */
    public void setDrugType(boolean b) {
        drugType = b;
    }

}
