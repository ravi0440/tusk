package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.OrganisationEAO;
import com.roche.dss.qtt.model.Organisation;

@Repository("organisationEAO")
public class OrganisationEAOImpl extends AbstractBaseEAO<Organisation> implements OrganisationEAO {

}