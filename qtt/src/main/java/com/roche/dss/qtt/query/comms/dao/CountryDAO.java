package com.roche.dss.qtt.query.comms.dao;

import com.roche.dss.qtt.query.comms.dto.CountryDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface CountryDAO {
    public List<CountryDTO> getCountries();

}
