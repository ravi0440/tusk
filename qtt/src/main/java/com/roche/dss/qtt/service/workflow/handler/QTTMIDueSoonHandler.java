package com.roche.dss.qtt.service.workflow.handler;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.comms.dto.EmailDTO;
import com.roche.dss.qtt.service.workflow.WorkflowGlobalConstants;
import com.roche.dss.qtt.utility.EmailSender;
import com.roche.dss.qtt.utility.config.Config;       
import com.roche.dss.util.ApplicationContextProvider;
import java.util.Properties;


public class QTTMIDueSoonHandler implements ActionHandler, WorkflowGlobalConstants {

	private static final Logger logger = LoggerFactory.getLogger(QTTMIDueSoonHandler.class);
	
	
    
	
	@Override
	public void execute(ExecutionContext executionContext) throws Exception {
		logger.debug("QTTMIDueSoonHandler");
		ContextInstance ctx = executionContext.getTaskInstance().getTaskMgmtInstance().getProcessInstance().getContextInstance();
        Long queryId = (Long) ctx.getVariable(PROCESS_INSTANCE_OWNER_ID);
        Session session = (Session) executionContext.getJbpmContext().getSession();
        Query query = (Query) session.get(Query.class, queryId);
        if (query != null) {
        	EmailDTO email = new EmailDTO();
        	
        	ApplicationContext appCtx = ApplicationContextProvider.ctx;
        	EmailSender emailSender = appCtx.getBean(EmailSender.class);
        	Properties properties = (Properties) appCtx.getBean("properties");
        	        	
        	String alertSent = (String) ctx.getVariable(DUE_SOON_ALERT);
        	logger.debug("alertSent: " + alertSent);
        	if (alertSent == null) {
	        	DateTime now = new DateTime();
	        	DateTime now3 = now.plusDays(3);
	        	
	        	if (now3.isAfter(query.getCurrentDueDate())) {
	        		logger.debug("sending a notification for query: " + query.getQuerySeq());
	        		
		        	String userName = (String) ctx.getVariable(PS_ASSIGNED_ACTOR_ID);
		         	
		        	String userEmail;        	
		        	Config config = appCtx.getBean(Config.class);
		        	if(config.isTestEnvironment()){
		        		 userEmail = config.getQttTestMiDueSoonEmail();
		        	} else {
		        		userEmail = (String) ctx.getVariable(PS_ASSIGNED_ACTOR_EMAIL);
		        	}	        	
		        	
		        	logger.debug("assingedUser: " + userName + " with email: " + userEmail);
		        	
		        	email.buildDTO_DueAlert(query, userEmail, userName, properties);
	        	
		            try {
						emailSender.sendMessage(email.getFrom(), email.getTo(), email.getCc(),
							email.getSubject(), email.getBody());
			        	ctx.setVariable(DUE_SOON_ALERT, now.toString());
					} catch (AddressException e) {
						logger.warn("QTTMIDueSoonHandler AddressException", e);
					} catch (SendFailedException e) {
						logger.warn("QTTMIDueSoonHandler SendFailedException", e);
					} catch (MessagingException e) {
						logger.warn("QTTMIDueSoonHandler MessagingException", e);
					} catch (Exception e) {
						logger.warn("QTTMIDueSoonHandler Exception", e);			
					}	        	
	        	}
        	}
        } else {
        	logger.warn("problem with sending a reminder for query: " + queryId + ", the query is not found in the database");
        }
	}}
