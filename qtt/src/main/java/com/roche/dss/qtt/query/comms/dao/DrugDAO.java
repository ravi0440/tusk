package com.roche.dss.qtt.query.comms.dao;

import com.roche.dss.qtt.query.comms.dto.DrugDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface DrugDAO {
    List<DrugDTO> getDrugNames();

    List<DrugDTO> getGenDrugNames();
}

