package com.roche.dss.qtt.query.web.action;


import com.roche.dss.qtt.security.activedirectory.ActiveDirectoryService;
import java.util.Map;

public class FindUserDetailsAction extends ActionSupport {
    private static final long serialVersionUID = -3827439829486925185L;
    private static final String ERROR_MSG = "User not found in Active Directory";
    private static final String LOG_ERROR_MSG = "User {} not found in Active Directory";
    private static final Logger LOGGER = LoggerFactory.getLogger(FindUserDetailsAction.class);

    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String errorMsg;

    @Autowired
    private ActiveDirectoryService activeDirectoryService;

    @Override
    public String execute() throws Exception {

        try {
            Map<String, String> userProperties = activeDirectoryService.findUserDetails(username);
            firstName = userProperties.get(ActiveDirectoryService.FIRSTNAME_AD_PROPERTY_NAME).toUpperCase();
            lastName = userProperties.get(ActiveDirectoryService.SURNAME_AD_PROPERTY_NAME).toUpperCase();
            email = userProperties.get(ActiveDirectoryService.MAIL_AD_PROPERTY_NAME).toUpperCase();
            return "success";
        } catch (ActiveDirectoryUserNotFoundException e) {
            LOGGER.info(LOG_ERROR_MSG, username);
            errorMsg = ERROR_MSG;
            return "error";
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

}
