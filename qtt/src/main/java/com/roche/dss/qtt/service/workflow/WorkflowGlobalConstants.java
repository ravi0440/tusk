package com.roche.dss.qtt.service.workflow;

import com.roche.dss.qtt.security.utils.storeprocedures.DSSStoreProceduresConstants;

/**
 * @author zerkowsm
 *
 */
public interface WorkflowGlobalConstants extends DSSStoreProceduresConstants{
	
    public static final String WF_ROUTE_A1 = "A1";
	public static final String WF_ROUTE_A2 = "A2";
	public static final String WF_ROUTE_B = "B";
	
	/******************************   QTT Workflow process variables   ***************************/
	public static final String QTT_WORKFLOW_PROCESS_NAME = "query";//"P-QTT-QTT";
	public static final String PROCESS_INSTANCE_OWNER_ID = "PROCESS_INSTANCE_OWNER_ID";
	public static final String PROCESS_INSTANCE_START_ACTOR_ID = "PROCESS_INSTANCE_START_ACTOR_ID";
	public static final String PS_ASSIGNED_ACTOR_ID = "PS_ASSIGNED_ACTOR_ID";
	public static final String PS_ASSIGNED_ACTOR_EMAIL = "PS_ASSIGNED_ACTOR_EMAIL";
	public static final String DMG_ASSIGNED_ACTOR_ID = "DMG_ASSIGNED_ACTOR_ID";

	public static final String MI_CLARIFICATION_ACTIVITY = "MIClarify";
	public static final String MI_REVIEW_ACTIVITY = "MIReview";
	public static final String MI_ASSISTANCE_ACTIVITY = "MIAssistance";
	public static final String DMG_CLARIFICATION_ACTIVITY = "DMGClarify";
	public static final String DMG_REVIEW_ACTIVITY = "DMGReview";
	public static final String DMG_MI_COMPLETE_ACTIVITY = "DMGMIComplete";
	public static final String MI_AND_DMG_COMPLETE_ACTIVITY = "MIDMGComplete";	
	public static final String AWAITING_FOLLOW_UP_COMPLETE_ACTIVITY = "AwaitingFollowUpComplete";
	
	public static final String DUE_SOON_ALERT = "DUE_SOON_ALERT";
		
//	## Workflow activity identifiers
//	qtt.workflow.activities.Assign=act-QTT-Query-Assignment
//	qtt.workflow.activities.DmgAssess=act-QTT-DMG-ManualAssignment
//	qtt.workflow.activities.PsAssign=act-QTT-PS-ManualAssignment
//	qtt.workflow.activities.DmgDataSearch=act-QTT-DMG-DataSearch
//	qtt.workflow.activities.MedicalInterpretationI=act-QTT-MI-Medical Interpretation I
//	qtt.workflow.activities.MiClarification=act-QTT-MI-Clarification
//	qtt.workflow.activities.MiReview=act-QTT-MI-Review
//	qtt.workflow.activities.MedicalInterpretationII=act-QTT-MI-Medical Interpretation II
//	qtt.workflow.activities.DmgClarification=act-QTT-DMG-Clarification
//	qtt.workflow.activities.DmgReview=act-QTT-DMG-Review
//	qtt.workflow.activities.SendToRequester=act-QTT-SendToRequester
//	qtt.workflow.activities.AwaitingFollowup=act-QTT-AwaitingFollow-up
//
//	###############################################################################
//	## Workflow transition identifiers
//	qtt.workflow.transitions.DmgReview=sTs-QTT-DMG-ReviewNeeded
//	qtt.workflow.transitions.DmgClarify=sTs-QTT-DMG-ClarificationNeeded
//	qtt.workflow.transitions.DmgComplete=sTs-QTT-DMG-SearchCompleted
//	qtt.workflow.transitions.DmgMiComplete=sTs-QTT-MI-DMG-SearchCompleted
//	qtt.workflow.transitions.MiAssistance=sTs-QTT-MI-DMGAssistanceRequired
//	qtt.workflow.transitions.MiReview=sTs-QTT-MI-ReviewNeeded
//	qtt.workflow.transitions.MiClarify=sTs-QTT-MI-ClarificationNeeded
//	qtt.workflow.transitions.MiComplete=sTs-QTT-MI-ResponseCompleted
	
}