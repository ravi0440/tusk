package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.PreQueryPreparation;

public interface PreQueryPreparationEAO extends QueryRelatedEAO<PreQueryPreparation>{
	
}