package com.roche.dss.qtt.eao;

import java.util.List;

import com.roche.dss.qtt.model.QueryType;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface QueryTypeEAO extends BaseEAO<QueryType> {
	
	List<QueryType> getActive();
}
