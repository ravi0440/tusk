package com.roche.dss.qtt.query.comms.dto;

import java.util.Date;


/**
 * This DTO class contains sufficient information to render the task-list
 * screens in the Task-List screens.
 * 
 * @author wisbeya
 */
public class UserQueryDTO {

    protected int id;
    protected String number;
    protected boolean urgency;
    protected String drugRetrievalName;
    protected String type;
    protected Date submittedDate;
    protected Date responseDue;
    protected String statusCode;
    protected String email;
    protected String requesterQueryLabel;

    /**
     * default 0-arg ctor
     */
    public UserQueryDTO() {
        super();
    }

    /**
     * Constructor tha populates all fields.
     * 
     * @param id
     * @param number
     * @param urgency
     * @param drugRetrievalName
     * @param type
     * @param requestorEmail
     * @param submittedDate
     * @param responseDue
     * @param statusCode
     */
    public UserQueryDTO(int id, boolean urgency, String number, String drugRetrievalName, String type,
            Date submittedDate, Date responseDue, String statusCode) {
        this.id = id;
        this.number = number;
        this.urgency = urgency;
        this.drugRetrievalName = drugRetrievalName;
        this.type = type;
        this.submittedDate = submittedDate;
        this.responseDue = responseDue;
        this.statusCode = statusCode;
    }

    /**
     * @return internal identifier of the query.
     */
    public int getId() {
        return id;
    }

    /**
     * @return query number.
     */
    public String getNumber() {
        return number;
    }

    /**
     * @return urgency flag.
     */
    public boolean isUrgency() {
        return urgency;
    }

    /**
     * @return drug retrieval name.
     */
    public String getDrugRetrievalName() {
        return drugRetrievalName;
    }

    /**
     * @return query type.
     */
    public String getType() {
        return type;
    }

    /**
     * @return Returns the submittedDate.
     */
    public Date getSubmittedDate() {
        return submittedDate;
    }

    /**
     * @return response due date.
     */
    public Date getResponseDue() {
        return responseDue;
    }

    /**
     * @return status code.
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @return Returns the email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param string
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @param string
     */
    public void setNumber(String string) {
        number = string;
    }

    /**
     * @param b
     */
    public void setUrgency(boolean b) {
        urgency = b;
    }

    /**
     * @param string
     */
    public void setDrugRetrievalName(String string) {
        drugRetrievalName = string;
    }

    /**
     * @param string
     */
    public void setType(String string) {
        type = string;
    }

    /**
     * @param submittedDate
     *            The submittedDate to set.
     */
    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }

    /**
     * @param date
     */
    public void setResponseDue(Date date) {
        responseDue = date;
    }

    /**
     * @param string
     */
    public void setStatusCode(String string) {
        statusCode = string;
    }

    /**
     * @param email
     *            The email to set.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return Returns the requesterQueryLabel.
     */
    public String getRequesterQueryLabel() {
        return requesterQueryLabel;
    }

    /**
     * @param requesterQueryLabel
     *            The requesterQueryLabel to set.
     */
    public void setRequesterQueryLabel(String requesterQueryLabel) {
        this.requesterQueryLabel = requesterQueryLabel;
    }

    /**
     * provides a string representation of the QuerySummaryDTO
     */
    @Override
	public String toString() {
        return number + "[" + "id:" + id + ", " + "urgency:" + urgency + ", " + "number:" + number + ", "
                + "drugRetrievalName:" + drugRetrievalName + ", " + "type:" + type + ", " + "requesterQueryLabel:"
                + requesterQueryLabel + ", " + "responseDue:" + responseDue + ", " + "statusCode:" + statusCode + "]";
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + id;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime
				* result
				+ ((drugRetrievalName == null) ? 0 : drugRetrievalName
						.hashCode());
		result = prime
				* result
				+ ((requesterQueryLabel == null) ? 0 : requesterQueryLabel
						.hashCode());
		result = prime * result
				+ ((responseDue == null) ? 0 : responseDue.hashCode());
		result = prime * result
				+ ((statusCode == null) ? 0 : statusCode.hashCode());
		result = prime * result
				+ ((submittedDate == null) ? 0 : submittedDate.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + (urgency ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if(!(obj instanceof UserQueryDTO))
			return false;
		UserQueryDTO other = (UserQueryDTO) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id != other.id)
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (drugRetrievalName == null) {
			if (other.drugRetrievalName != null)
				return false;
		} else if (!drugRetrievalName.equals(other.drugRetrievalName))
			return false;
		if (requesterQueryLabel == null) {
			if (other.requesterQueryLabel != null)
				return false;
		} else if (!requesterQueryLabel.equals(other.requesterQueryLabel))
			return false;
		if (responseDue == null) {
			if (other.responseDue != null)
				return false;
		} else if (!responseDue.equals(other.responseDue))
			return false;
		if (statusCode == null) {
			if (other.statusCode != null)
				return false;
		} else if (!statusCode.equals(other.statusCode))
			return false;
		if (submittedDate == null) {
			if (other.submittedDate != null)
				return false;
		} else if (!submittedDate.equals(other.submittedDate))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (urgency != other.urgency)
			return false;
		return true;
	}
}
