package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.AeTerm;

/**
 * @author zerkowsm
 *
 */
public interface AeTermEAO extends QueryRelatedEAO<AeTerm>{
	
}