package com.roche.dss.qtt.model;
// Generated 2010-10-04 12:00:38 by Hibernate Tools 3.3.0.GA

import com.roche.dss.util.ApplicationUtils;


/**
 * AuditHistory generated by hbm2java
 */
//@NamedQueries({
//        @NamedQuery(name = "list.dmgInvolvedNames", query = "select distinct responsible from AuditHistory ah"
//        		+ " where ah.responsible is not null and ah.responsible <> 'WorkPoint'"
//        		+ " and ah.qttWorkflowProcess.wfProcessSeq in('7                 ','8                 ','9                 ','10                ')"
//        		+ " order by ah.responsible"
//        		, hints = {@QueryHint(name = QueryHints.HINT_CACHEABLE, value="true")}),
//        @NamedQuery(name = "list.psInvolvedNames", query = "select distinct responsible from AuditHistory ah"
//        		+ " where ah.responsible is not null and ah.responsible <> 'WorkPoint'"
//        		+ " and ah.qttWorkflowProcess.wfProcessSeq in('3                 ','4                 ','5                 ','6                 ')"
//        		+ " order by ah.responsible"
//        		, hints = {@QueryHint(name = QueryHints.HINT_CACHEABLE, value="true")}),
//        @NamedQuery(name = "list.dsclInvolvedNames", query = "select distinct responsible from AuditHistory ah"
//        		+ " where ah.responsible is not null and ah.responsible <> 'WorkPoint'"
//        		+ " and ah.qttWorkflowProcess.wfProcessSeq in('1                 ','2                 ','11                ','12                ')"
//        		+ " order by ah.responsible"
//        		, hints = {@QueryHint(name = QueryHints.HINT_CACHEABLE, value="true")})
//})
@Entity
@Table(name = "AUDIT_HISTORY")
public class AuditHistory implements java.io.Serializable {

    private static final long serialVersionUID = 7072891882078574169L;

    private long auditSeq;
    private QttWorkflowProcess qttWorkflowProcess;
    private DateTime startTs;
    private DateTime stopTs;
    private String currentFlag;
    private String responsible;
    private Long followupSeq;
    private Query query;

    public AuditHistory() {
    }

    public AuditHistory(Query query, DateTime startTs) {
    	this.query = query;
        this.startTs = startTs;
    }

    public AuditHistory(Query query, QttWorkflowProcess qttWorkflowProcess, DateTime startTs,
                        DateTime stopTs, String currentFlag, String responsible,
                        Long followupSeq) {
    	this.query = query;
        this.qttWorkflowProcess = qttWorkflowProcess;
        this.startTs = startTs;
        this.stopTs = stopTs;
        this.currentFlag = currentFlag;
        this.responsible = responsible;
        this.followupSeq = followupSeq;
    }

    @Id
    @Column(name = "AUDIT_SEQ", nullable = false, precision = 10, scale = 0)
    @SequenceGenerator(name="SQ_AUDT_GENERATOR", sequenceName="SQ_AUDT_AUDT_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_AUDT_GENERATOR")
	public long getAuditSeq() {
		return this.auditSeq;
	}

	public void setAuditSeq(long auditSeq) {
		this.auditSeq = auditSeq;
	}

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "WF_PROCESS_SEQ")
    public QttWorkflowProcess getQttWorkflowProcess() {
        return this.qttWorkflowProcess;
    }

    public void setQttWorkflowProcess(
            QttWorkflowProcess qttWorkflowProcess) {
        this.qttWorkflowProcess = qttWorkflowProcess;
    }

    @Type(type = "com.roche.dss.util.PersistentDateTime")
    @Column(name = "START_TS", nullable = false, length = 7)
    public DateTime getStartTs() {
        return this.startTs;
    }

    public void setStartTs(DateTime startTs) {
        this.startTs = startTs;
    }

    @Type(type = "com.roche.dss.util.PersistentDateTime")
    @Column(name = "STOP_TS", length = 7)
    public DateTime getStopTs() {
        return this.stopTs;
    }

    public void setStopTs(DateTime stopTs) {
        this.stopTs = stopTs;
    }

    @Column(name = "CURRENT_FLAG", length = 1)
    public String getCurrentFlag() {
        return this.currentFlag;
    }

    public void setCurrentFlag(String currentFlag) {
        this.currentFlag = currentFlag;
    }

    @Column(name = "RESPONSIBLE", length = 18)
    public String getResponsible() {
        return this.responsible;
    }

    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }

    @Column(name = "FOLLOWUP_SEQ", precision = 10, scale = 0)
    public Long getFollowupSeq() {
        return this.followupSeq;
    }

    public void setFollowupSeq(Long followupSeq) {
        this.followupSeq = followupSeq;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUERY_SEQ", nullable = false, insertable = true, updatable = false)
    public Query getQuery() {
        return this.query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    @Transient
    public String getStopTsFormatted() {
        return (stopTs != null) ? ApplicationUtils.DATE_TIME_FORMATTER.print(stopTs) : "";
    }

}
