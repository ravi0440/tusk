package com.roche.dss.qtt.service.impl;

import com.roche.dss.qtt.eao.*;
import com.roche.dss.qtt.model.*;
import com.roche.dss.qtt.query.comms.dto.PreparationDTO;
import com.roche.dss.qtt.service.FollowupQueryService;
import com.roche.dss.util.ApplicationUtils;
import java.util.List;
import java.util.Set;

import static com.roche.dss.qtt.query.web.action.ActionConstants.*;

/**
 * User: pruchnil
 */
@Service("followupQueryService")
public class FollowupQueryServiceImpl implements FollowupQueryService {
    private static final Logger logger = LoggerFactory.getLogger(FollowupQueryServiceImpl.class);

    @Autowired
    FollowupQueryEAO followupQueryEAO;

    @Autowired
    QueryEAO queryEAO;

    @Autowired
    DrugEAO drugEAO;

    @Autowired
    LdocTypeEAO ldocTypeEAO;

    @Autowired
    RetrievalPeriodEAO retrievalPeriodEAO;

    @Autowired
    PreQueryPreparationEAO preQueryPreparationEAO;

    @Autowired
    PregnancyEAO pregnancyEAO;

    @Autowired
    ClinicalTrialEAO clinicalTrialEAO;

    @Autowired
    CaseEAO caseEAO;

    @Autowired
    AeTermEAO aeTermEAO;

    @Autowired
    RequesterEAO requesterEAO;

    @Autowired
    OrganisationEAO organisationEAO;

    @Autowired
    PerformanceMetricEAO performanceMetricEAO;

    @Autowired
    QueryDescriptionEAO queryDescriptionEAO;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    @Override
    public FuQuery getFollowupQueryDetails(long queryId, long followupId) {
        FuQuery query = followupQueryEAO.getByIdObj(new FuQueryId(followupId, queryId));
        query.setRequester(followupQueryEAO.getRequesterById(new FuRequesterId(followupId, query.getRequesterId())));
        query.setRetrievalPeriod(followupQueryEAO.getRetrievalPeriodById(new FuRetrievalPeriodId(followupId, queryId)));
        query.setQueryDescription(followupQueryEAO.getQueryDescriptionById(new FuQueryDescriptionId(followupId, queryId)));
        if (!query.isAllDrugs()) {
            query.setDrugForPreview(followupQueryEAO.getDisplayDrug(followupId, queryId));
        }
        query.setCases(followupQueryEAO.getCasesById(new FuCaseId(followupId, queryId)));
        query.setPreQueryPreparations(followupQueryEAO.getPreQueryPreparationsById(new FuPreQueryPreparationId(followupId, queryId)));
        query.setPreparation(setPreQueryPreparations(query));
        return query;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void createFollowup(int queryId) {
        long querySeq = queryId;
        Long maxFollowupSeq = queryEAO.getMaxFollowupSeq(querySeq);
        long followupSeq = maxFollowupSeq == null ? 0 : maxFollowupSeq + 1;

        Query queryToReopen = queryEAO.find(querySeq);

        String followupNumber = queryToReopen.getFollowupNumber();
        if(StringUtils.isEmpty(followupNumber)){
            followupNumber = new StringBuilder("FU").append(ApplicationUtils.addZeros(followupSeq)).toString();
        }

        addFuQuery(querySeq, followupNumber, followupSeq);
        addFuDrug(querySeq, followupSeq);
        addFuLabellingDocument(querySeq, queryToReopen.getLabelingDocTypes(), followupSeq);
        addFuRetrievalPeriod(querySeq, followupSeq);
        addFuPreQueryPreparation(querySeq, queryToReopen.getPreQueryPreparations(), followupSeq);
        addFuPregnancy(querySeq, followupSeq);
        addFuClinicalTrial(querySeq, followupSeq);
        addFuCase(querySeq, followupSeq);
        addFuAeTerm(querySeq, followupSeq);
        addFuQueryDescription(querySeq, followupSeq);
        addFuPerformanceMetric(querySeq, followupSeq);
        addFuRequester(querySeq, followupSeq);

        ++followupSeq;

        queryToReopen.setFollowupSeq(followupSeq);
        queryToReopen.setDmgDueDate(null);
        queryToReopen.setFollowupNumber(new StringBuilder("FU").append(ApplicationUtils.addZeros(followupSeq)).toString());
        queryToReopen.setReopen('Y');
        queryEAO.merge(queryToReopen);
    }

    @Override
    public void addFuQuery(long queryId, String followupNumber, long followupSeq) {
        Query query = queryEAO.find(queryId);
        FuQuery fuquery = new FuQuery(query, followupSeq, followupNumber);
        followupQueryEAO.merge(fuquery);
    }

    @Override
    public void addFuDrug(long queryId, long followupSeq) {
        List<Drug> drugs = drugEAO.findAllByQueryId(queryId);
        if (CollectionUtils.isNotEmpty(drugs)) {
            for (Drug drug : drugs) {
                FuDrug fudrug = new FuDrug(drug, followupSeq);
                followupQueryEAO.mergeFuDrug(fudrug);
            }
        }
    }

    @Override
    public void addFuLabellingDocument(long queryId, Set<LdocType> labellingDocs, long followupSeq) {
        if (CollectionUtils.isNotEmpty(labellingDocs)) {
            for (LdocType ldocType : labellingDocs) {
                FuLabellingDocument doc = new FuLabellingDocument(new FuLabellingDocumentId(followupSeq, queryId, ldocType.getLdocTypeId()));
                followupQueryEAO.mergeFuLabellingDocument(doc);
            }
        }
    }

    @Override
    public void addFuRetrievalPeriod(long queryId, long followupSeq) {
        RetrievalPeriod retrievalPeriod = retrievalPeriodEAO.find(queryId);
        if (retrievalPeriod != null) {
            FuRetrievalPeriod fuRetrievalPeriod = new FuRetrievalPeriod(retrievalPeriod, followupSeq);
            followupQueryEAO.mergeFuRetrievalPeriod(fuRetrievalPeriod);
        }
    }

    @Override
    public void addFuPreQueryPreparation(long queryId, Set<PreQueryPreparation> preQueryPreparations, long followupSeq) {
        if (CollectionUtils.isNotEmpty(preQueryPreparations)) {
            for (PreQueryPreparation preparation : preQueryPreparations) {
                 FuPreQueryPreparation fuPreQueryPreparation = new FuPreQueryPreparation(preparation, followupSeq);
                followupQueryEAO.mergeFuPreQueryPreparation(fuPreQueryPreparation);
            }
        }
    }

    @Override
    public void addFuPregnancy(long queryId, long followupSeq) {
        Pregnancy pregnancy = pregnancyEAO.find(queryId);
        if (pregnancy != null) {
            FuPregnancy fuPregnancy = new FuPregnancy(pregnancy, followupSeq);
            followupQueryEAO.mergeFuPregnancy(fuPregnancy);
        }
    }

    @Override
    public void addFuClinicalTrial(long queryId, long followupSeq) {
        ClinicalTrial trial = clinicalTrialEAO.find(queryId);
        if (trial != null) {
            FuClinicalTrial fuClinicalTrial = new FuClinicalTrial(trial, followupSeq);
            followupQueryEAO.mergeFuClinicalTrial(fuClinicalTrial);
        }
    }

    @Override
    public void addFuCase(long queryId, long followupSeq) {
        List<Case> cases = caseEAO.findAllByQueryId(queryId);
        if (CollectionUtils.isNotEmpty(cases)) {
            for (Case aCase : cases) {
                FuCase fuCase = new FuCase(aCase, followupSeq);
                followupQueryEAO.mergeFuCase(fuCase);
            }
        }
    }

    @Override
    public void addFuAeTerm(long queryId, long followupSeq) {
        AeTerm aeTerm = aeTermEAO.find(queryId);
        if (aeTerm != null) {
            FuAeTerm fuAeTerm = new FuAeTerm(aeTerm, followupSeq);
            followupQueryEAO.mergeFuAeTerm(fuAeTerm);
        }
    }

    @Override
    public void addFuQueryDescription(long queryId, long followupSeq) {
        QueryDescription description = queryDescriptionEAO.find(queryId);
        if (description != null) {
            FuQueryDescription fuQueryDescription = new FuQueryDescription(description, followupSeq);
            followupQueryEAO.mergeFuQueryDescription(fuQueryDescription);
        }
    }

    @Override
    public void addFuPerformanceMetric(long queryId, long followupSeq) {
        PerformanceMetric performanceMetric = performanceMetricEAO.find(queryId);
        if (performanceMetric != null) {
            FuPerformanceMetric fuPerformanceMetric = new FuPerformanceMetric(performanceMetric, followupSeq);
            followupQueryEAO.mergeFuPerformanceMetric(fuPerformanceMetric);
        }
    }

    @Override
    public void addFuRequester(long queryId, long followupSeq) {
        Query query = queryEAO.find(queryId);
        Requester requester = query.getRequester();
        if (requester != null) {
            Organisation organisation = requester.getOrganisation();
            FuOrganisation fuOrganisation = new FuOrganisation(organisation, followupSeq);
            followupQueryEAO.mergeFuOrganisation(fuOrganisation);
            FuRequester fuRequester = new FuRequester(requester, followupSeq, fuOrganisation);
            followupQueryEAO.mergeFuRequester(fuRequester);
        }
    }

    private PreparationDTO setPreQueryPreparations(FuQuery query) {
        PreparationDTO preparation = new PreparationDTO();
        for (FuPreQueryPreparation prePreparation : query.getPreQueryPreparations()) {
            switch (prePreparation.getId().getPqpTypeId()) {
                case REPOSITORY:
                    preparation.setRepository(YES);
                    break;
                case PSURS:
                    preparation.setPsur(YES);
                    break;
                case CDS:
                    preparation.setCds(YES);
                    break;
                case ISSUE_WORKUPS:
                    preparation.setIssue(YES);
                    break;
                case LITERATURE:
                    preparation.setLiterature(YES);
                    break;
                case OTHERS:
                    preparation.setOthers(YES);
                    preparation.setOthersComment(prePreparation.getOtherComment());
                    break;
                default:
                    break;
            }
        }
        return preparation;
    }

}
