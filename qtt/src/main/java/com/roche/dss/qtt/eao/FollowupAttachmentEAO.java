package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.FollowupAttachment;

/**
 * User: pruchnil
 */
public interface FollowupAttachmentEAO extends BaseEAO<FollowupAttachment> {
    FollowupAttachment findOpenByQuerySeq(long queryId);

    long getMaxFollowupSeq(long queryId);
}
