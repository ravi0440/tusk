package com.roche.dss.qtt.security.ldap.authentication;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.sql.DataSource;

import com.roche.dss.qtt.security.model.DSSUser;
import com.roche.dss.qtt.security.utils.storeprocedures.DSSStoreProceduresConstants;
import com.roche.dss.qtt.security.utils.storeprocedures.user.DSSUserStoredProcedure;
import com.roche.dss.qtt.security.web.authentication.QTTUsernamePasswordAuthenticationToken;

/**
 * @author zerkowsm
 *
 */
public class QTTBindAuthenticator implements LdapAuthenticator, DSSStoreProceduresConstants {

	private static final Log logger = LogFactory.getLog(QTTBindAuthenticator.class);

	private static String INITIAL_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";

	private static String SECURITY_AUTHENTICATION = "simple";
	
	private String ldapUrl;
	
	private DataSource securityDataSource;

    private AuthenticationManager authenticationManager;
	
	@SuppressWarnings("unchecked")
	public DSSUser authenticate(Authentication authentication) {
		Assert.isInstanceOf(UsernamePasswordAuthenticationToken.class, authentication, "Can only process UsernamePasswordAuthenticationToken objects");

		String username = authentication.getName();
		String password = (String) authentication.getCredentials();
		String domain = ((QTTUsernamePasswordAuthenticationToken) authentication).getDomain();

		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
		env.put(Context.PROVIDER_URL, ldapUrl);
		env.put(Context.SECURITY_AUTHENTICATION, SECURITY_AUTHENTICATION);
		String principal = username + "@" + domain + "." + "roche" + ".com";
		env.put(Context.SECURITY_PRINCIPAL, principal);
		env.put(Context.SECURITY_CREDENTIALS, password);
		
		if(logger.isDebugEnabled()) {
			logger.debug("User: " + principal + " getting authenticated (authenticate method). ");
		}

		DirContext ctx = null;

        try {
			ctx = new InitialDirContext(env);
			if(logger.isDebugEnabled()) {
				logger.debug("User: " + principal + " authenticated (authenticate method). ");
			}
		} catch (NamingException e) {
			throw LdapUtils.convertLdapException(e);
		} finally {
			LdapUtils.closeContext(ctx);
		}

        Map<String, Object> users = null;
		
		try{
			users = getDSSUser(username);
			DSSUser user = ((ArrayList<DSSUser>)users.get(USER_SERVICE_GET_USER_OUT_PARAM)).get(0);

            SecurityContextHolder.getContext().setAuthentication(authentication);
            logger.info("Authentication registered at context (authenticate method): " + authentication);

			return user;
		}catch (Exception e) {
            throw new AuthenticationServiceException("Cannot retrieve user information for user: " + username, e);
		}		
	}

    @Override
    public LdapUser authenticateLdap(String login, String password, String domain) {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
        env.put(Context.PROVIDER_URL, ldapUrl);
        env.put(Context.SECURITY_AUTHENTICATION, SECURITY_AUTHENTICATION);
        String principal = login + "@" + domain + "." + "roche" + ".com";
        env.put(Context.SECURITY_PRINCIPAL, principal);
        env.put(Context.SECURITY_CREDENTIALS, password);

        if (logger.isDebugEnabled()) {
            logger.debug("User: " + principal + " getting authenticated (authenticateLdap method). ");
        }

        DirContext ctx = null;

        try {
            ctx = new InitialDirContext(env);
            if (logger.isDebugEnabled()) {
                logger.debug("User: " + principal + " authenticated (authenticateLdap method). ");
            }

            String filter = "(&(objectclass=user)(cn=" + login + "))";
            String[] attrIDs = {"sn", "givenName", "fullName", "name", "mail"};
            SearchControls controls = new SearchControls();
            controls.setReturningAttributes(attrIDs);
            controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration results;
            results = ctx.search("", filter, controls);
            if (results.hasMore()) {
            	LdapUser user = new LdapUser();
                SearchResult searchResult = (SearchResult) results.next();
                Attributes attributes = searchResult.getAttributes();
                
                user.setMail(attributes.get("mail").get().toString());
                user.setGivenName(attributes.get("givenName").get().toString());
                user.setName(attributes.get("name").get().toString());
                user.setSurname(attributes.get("sn").get().toString());

                return user;
            }
        } catch (NamingException e) {
            return null;
        } finally {
            LdapUtils.closeContext(ctx);
        }
        return null;
    }

    private Map<String, Object> getDSSUser(String username){
		DSSUserStoredProcedure sproc = new DSSUserStoredProcedure(securityDataSource);
	    Map<String, Object> result = sproc.execute(username);	
		return result;
	}

	public void setLdapUrl(String ldapUrl) {
		this.ldapUrl = ldapUrl;
	}
	
    public void setDataSource(DataSource securityDataSource) {
        this.securityDataSource = securityDataSource;
    }

    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }
}
