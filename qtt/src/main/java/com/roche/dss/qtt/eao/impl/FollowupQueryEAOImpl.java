package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.FollowupQueryEAO;
import com.roche.dss.qtt.model.*;
import java.util.List;

/**
 * User: pruchnil
 */
@Repository("followupQueryEAO")
public class FollowupQueryEAOImpl extends AbstractBaseEAO<FuQuery> implements FollowupQueryEAO {
    private static final Logger log = LoggerFactory.getLogger(FollowupQueryEAOImpl.class);

    @Override
    public FuRequester getRequesterById(FuRequesterId fuRequesterId) {
        FuRequester requester = getEntityManager().find(FuRequester.class, fuRequesterId);
        requester.setOrganisation(getEntityManager().find(FuOrganisation.class, new FuOrganisationId(requester.getId().getFollowupSeq(), requester.getOrganisationId())));
        return requester;
    }

    @Override
    public FuRetrievalPeriod getRetrievalPeriodById(FuRetrievalPeriodId fuRetrievalPeriodId) {
        return getEntityManager().find(FuRetrievalPeriod.class, fuRetrievalPeriodId);
    }

    @Override
    public FuQueryDescription getQueryDescriptionById(FuQueryDescriptionId fuQueryDescriptionId) {
        return getEntityManager().find(FuQueryDescription.class, fuQueryDescriptionId);
    }

    @Override
    public List<FuCase> getCasesById(FuCaseId fuCaseId) {
        return getEntityManager().createQuery("select fc from FuCase fc where fc.id.querySeq=:querySeq and fc.id.followupSeq=:followupSeq", FuCase.class).setParameter("querySeq", fuCaseId.getQuerySeq()).setParameter("followupSeq", fuCaseId.getFollowupSeq()).getResultList();
    }

    @Override
    public List<FuPreQueryPreparation> getPreQueryPreparationsById(FuPreQueryPreparationId fuPreQueryPreparationId) {
        return getEntityManager().createQuery("select fpq from FuPreQueryPreparation fpq where fpq.id.querySeq=:querySeq and fpq.id.followupSeq=:followupSeq", FuPreQueryPreparation.class).setParameter("querySeq", fuPreQueryPreparationId.getQuerySeq()).setParameter("followupSeq", fuPreQueryPreparationId.getFollowupSeq()).getResultList();
    }

    @Override
    public FuDrug getDisplayDrug(long followupId, long queryId) {
        return getEntityManager().createNamedQuery("FuDrug.findDisplayDrug", FuDrug.class).setParameter("querySeq", queryId).setParameter("followupSeq", followupId).getSingleResult();
    }

    @Override
    public void mergeFuDrug(FuDrug drug) {
        getEntityManager().merge(drug);
    }

    @Override
    public void mergeFuRetrievalPeriod(FuRetrievalPeriod fuRetrievalPeriod) {
        getEntityManager().merge(fuRetrievalPeriod);
    }

    @Override
    public void mergeFuPregnancy(FuPregnancy fuPregnancy) {
        getEntityManager().merge(fuPregnancy);
    }

    @Override
    public void mergeFuClinicalTrial(FuClinicalTrial fuClinicalTrial) {
        getEntityManager().merge(fuClinicalTrial);
    }

    @Override
    public void mergeFuCase(FuCase fuCase) {
        getEntityManager().merge(fuCase);
    }

    @Override
    public void mergeFuAeTerm(FuAeTerm fuAeTerm) {
        getEntityManager().merge(fuAeTerm);
    }

    @Override
    public void mergeFuQueryDescription(FuQueryDescription fuQueryDescription) {
        getEntityManager().merge(fuQueryDescription);
    }

    @Override
    public void mergeFuPerformanceMetric(FuPerformanceMetric fuPerformanceMetric) {
        getEntityManager().merge(fuPerformanceMetric);
    }

    @Override
    public void mergeFuRequester(FuRequester fuRequester) {
        getEntityManager().merge(fuRequester);
    }

    @Override
    public void mergeFuOrganisation(FuOrganisation fuOrganisation) {
        getEntityManager().merge(fuOrganisation);
    }

    @Override
    public void mergeFuLabellingDocument(FuLabellingDocument doc) {
        getEntityManager().merge(doc);
    }

    @Override
    public void mergeFuPreQueryPreparation(FuPreQueryPreparation fuPreQueryPreparation) {
        getEntityManager().merge(fuPreQueryPreparation);
    }
}
