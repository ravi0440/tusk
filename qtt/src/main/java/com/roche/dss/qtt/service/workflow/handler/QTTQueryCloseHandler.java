package com.roche.dss.qtt.service.workflow.handler;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryStatusCode;
import com.roche.dss.qtt.service.workflow.WorkflowGlobalConstants;
import com.roche.dss.qtt.utility.QueryStatusCodes;

public class QTTQueryCloseHandler implements ActionHandler, WorkflowGlobalConstants {

 	private static final long serialVersionUID = -1701940272288140395L;
 	
 	private static final Logger logger = LoggerFactory.getLogger(QTTQueryCloseHandler.class);

	@Override
	public void execute(ExecutionContext executionContext) throws Exception {
        logger.info("Closing query");
                
        Long queryId = (Long) executionContext.getVariable(PROCESS_INSTANCE_OWNER_ID);
        Session session = (Session) executionContext.getJbpmContext().getSession();
        Query query = (Query) session.get(Query.class, queryId);
        if (query != null) {
	        query.setQueryStatusCode(new QueryStatusCode(QueryStatusCodes.CLOSED.getCode()));
	        executionContext.getJbpmContext().getSession().update(query);
        }

        logger.info("Query with id: " + queryId + " closed. ");
    }
	
}
