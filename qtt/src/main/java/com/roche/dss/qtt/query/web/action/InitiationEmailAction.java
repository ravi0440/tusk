package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.web.model.InitiationEmailModel;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.utility.config.Config;
import java.util.Map;

@ParentPackage("default")
@Action(value="InitiationEmail", results={
        @Result(name="success", type="tiles", location="/initiation.email"),
        @Result(name="amendemail", location="/qtt/initiate/amend_email.jsp"),
        @Result(name="failure",location="/qtt/global/error.jsp")
})
public class InitiationEmailAction extends CommonActionSupport implements ModelDriven, SessionAware {

    private Map<String, Object> session;
    private static final Logger logger = LoggerFactory.getLogger(InitiationEmailAction.class);
    private String type;
    private String id;

    @Autowired
    protected Config config;
    @Autowired
    protected QueryService queryService;

    InitiationEmailModel emailModel = new InitiationEmailModel();


    public String execute() {

        //security check
        boolean allowed = isDscl();
        Query query = null;
        if (session.get(ActionConstants.INITIATE_QUERY_SESSION_NAME) != null) {
            Long queryId = (Long) session.get(ActionConstants.INITIATE_QUERY_SESSION_NAME);
            query = queryService.find(queryId);
        } else if (getId() != null) {
            long queryId = Long.valueOf(getId());
            query = queryService.find(queryId);
        }
        if (query == null) {
            logger.error("Query object could not be located from the session!");
            return ERROR;
        }

        // retrieve queryNumber from request param's
        String emailType = "amd";
        if (type != null) {
            emailType = type;
        }
        buildEmailModel(query, emailType, emailModel);
        return SUCCESS;
    }

    private void buildEmailModel(Query query, String emailType, InitiationEmailModel emailModel) {

        emailModel.setQueryId(query.getQuerySeq());
        emailModel.setTo(query.getRequester().getEmail());
        emailModel.setFileOneFileName("QRF-" + query.getQueryNumber() + ".pdf");
        emailModel.setEmailType(emailType);

        try {
            logger.debug("InitiationEmailAction:preparing email to:" + "TODO"
                    + " for query:" + query.getQueryNumber());
            //get the configuartion values

            if (emailType.equals("amd")) {
                emailModel.setFrom(config.getInititateDsclEmail());
                emailModel.setSubject(config.getInititateDsclEmailSubject() + " " + query.getQueryNumber());
                emailModel.setBody(config.getInititateAmendBody1Para()
                        + "\n\n" + config.getInititateAmendBody2Para()
                        + " " + query.getQueryNumber()
                        + ". "
                        + "\n\n" + config.getInititateAmendBody3Para()
                        + "\n\n" + config
                        .getInititateAmendBodyLastPara());
            } else if (emailType.equals("rej")) {
                emailModel.setFrom(config.getInititateDsclEmail());
                emailModel.setSubject(config.getInititateRejectedEmailSubject());
                emailModel.setBody(config.getInititateRejectedEmail1Para()
                        + "\n\n" + config.getInititateRejectedEmailLastPara());
            }
        } catch (Exception e) {
            logger.error("Error AcceptQueryAction:sending email" + e);
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Object getModel() {
        return emailModel;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
