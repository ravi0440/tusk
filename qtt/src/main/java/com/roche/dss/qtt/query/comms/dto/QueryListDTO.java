package com.roche.dss.qtt.query.comms.dto;

import com.roche.dss.util.ApplicationUtils;
import java.io.Serializable;

/**
 * This DTO class contains sufficient fields to transport data for QueryList
 * <p/>
 * User: pruchnil
 */
public class QueryListDTO implements Serializable{

    private long querySeq;
    private String queryNumber;
    private boolean urgency;
    private String drugName;
    private String queryLabel;
    private String statusCode;
    private String queryType;
    private DateTime initiationDate;
    private DateTime responseDate;
    private String activity;
    private String requesterEmail;

    public long getQuerySeq() {
        return querySeq;
    }

    public void setQuerySeq(long querySeq) {
        this.querySeq = querySeq;
    }

    public String getQueryNumber() {
        return queryNumber;
    }

    public void setQueryNumber(String queryNumber) {
        this.queryNumber = queryNumber;
    }

    public boolean isUrgency() {
        return urgency;
    }

    public void setUrgency(boolean urgency) {
        this.urgency = urgency;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getQueryLabel() {
        return queryLabel;
    }

    public void setQueryLabel(String queryLabel) {
        this.queryLabel = queryLabel;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public DateTime getInitiationDate() {
        return initiationDate;
    }

    public void setInitiationDate(DateTime initiationDate) {
        this.initiationDate = initiationDate;
    }

    public String getInitiationDateFormatted() {
        return ApplicationUtils.DATE_TIME_FORMATTER.print(initiationDate);
    }

    public DateTime getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(DateTime responseDate) {
        this.responseDate = responseDate;
    }

    public String getResponseDateFormatted() {
        return ApplicationUtils.DATE_TIME_FORMATTER.print(responseDate);
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isDisplayContact() {
        return statusCode.equals("Being Processed");
    }

    public boolean isDisplayClosed() {
        return statusCode.equals("Closed");
    }

    public String getRequesterEmail() {
        return requesterEmail;
    }

    public void setRequesterEmail(String requesterEmail) {
        this.requesterEmail = requesterEmail;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((activity == null) ? 0 : activity.hashCode());
		result = prime * result
				+ ((drugName == null) ? 0 : drugName.hashCode());
		result = prime * result
				+ ((initiationDate == null) ? 0 : initiationDate.hashCode());
		result = prime * result
				+ ((queryLabel == null) ? 0 : queryLabel.hashCode());
		result = prime * result
				+ ((queryNumber == null) ? 0 : queryNumber.hashCode());
		result = prime * result + (int) (querySeq ^ (querySeq >>> 32));
		result = prime * result
				+ ((queryType == null) ? 0 : queryType.hashCode());
		result = prime * result
				+ ((requesterEmail == null) ? 0 : requesterEmail.hashCode());
		result = prime * result
				+ ((responseDate == null) ? 0 : responseDate.hashCode());
		result = prime * result
				+ ((statusCode == null) ? 0 : statusCode.hashCode());
		result = prime * result + (urgency ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if(!(obj instanceof QueryListDTO))
			return false;
		QueryListDTO other = (QueryListDTO) obj;
		if (activity == null) {
			if (other.activity != null)
				return false;
		} else if (!activity.equals(other.activity))
			return false;
		if (drugName == null) {
			if (other.drugName != null)
				return false;
		} else if (!drugName.equals(other.drugName))
			return false;
		if (initiationDate == null) {
			if (other.initiationDate != null)
				return false;
		} else if (!initiationDate.equals(other.initiationDate))
			return false;
		if (queryLabel == null) {
			if (other.queryLabel != null)
				return false;
		} else if (!queryLabel.equals(other.queryLabel))
			return false;
		if (queryNumber == null) {
			if (other.queryNumber != null)
				return false;
		} else if (!queryNumber.equals(other.queryNumber))
			return false;
		if (querySeq != other.querySeq)
			return false;
		if (queryType == null) {
			if (other.queryType != null)
				return false;
		} else if (!queryType.equals(other.queryType))
			return false;
		if (requesterEmail == null) {
			if (other.requesterEmail != null)
				return false;
		} else if (!requesterEmail.equals(other.requesterEmail))
			return false;
		if (responseDate == null) {
			if (other.responseDate != null)
				return false;
		} else if (!responseDate.equals(other.responseDate))
			return false;
		if (statusCode == null) {
			if (other.statusCode != null)
				return false;
		} else if (!statusCode.equals(other.statusCode))
			return false;
		if (urgency != other.urgency)
			return false;
		return true;
	}
}
