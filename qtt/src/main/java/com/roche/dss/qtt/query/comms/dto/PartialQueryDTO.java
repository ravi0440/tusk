package com.roche.dss.qtt.query.comms.dto;

import java.util.Date;

public class PartialQueryDTO  {

	private int queryNumber;
	private Date date;
	private Date formattedDate;	
	private Date formattedDateOnly;
	private String emailAddress;
	private String reporterType;
	private String queryType;
	private String requesterName;
	private String requesterLoc;
	
    /**
     * @return
     */
    public Date getDate() {
        return date;
    }

    /**
     * @return
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @return
     */
    public int getQueryNumber() {
        return queryNumber;
    }

    /**
     * @return
     */
    public String getQueryType() {
        return queryType;
    }

    /**
     * @return
     */
    public String getReporterType() {
        return reporterType;
    }


    public void setDate(Date d) {
        date = d;
    }

    /**
     * @param string
     */
    public void setEmailAddress(String string) {
        emailAddress = string;
    }

    /**
     * @param i
     */
    public void setQueryNumber(int i) {
        queryNumber = i;
    }

    /**
     * @param string
     */
    public void setQueryType(String string) {
        queryType = string;
    }

    /**
     * @param string
     */
    public void setReporterType(String string) {
        reporterType = string;
    }

    /**
     * @return
     */
    public String getRequesterLoc() {
        return requesterLoc;
    }

    /**
     * @return
     */
    public String getRequesterName() {
        return requesterName;
    }

    /**
     * @param string
     */
    public void setRequesterLoc(String string) {
        requesterLoc = string;
    }

    /**
     * @param string
     */
    public void setRequesterName(String string) {
        requesterName = string;
    }

    /**
     * @return date and time
     */
    public String getFormattedDate() {
		return  new java.text.SimpleDateFormat("dd-MMM-yyyy hh:mm aaa").format( getDate() );
       
    }
    
    public void setFormattedDate(Date d) {
		formattedDate = getDate();
    }
   

    /**
     * @return only date as dd-mmm-yyyy
     */
    public String getFormattedDateOnly() {
		return new java.text.SimpleDateFormat("dd-MMM-yyyy").format( getDate() );
    }

    /**
     * @param date
     */
    public void setFormattedDateOnly(Date date) {
        formattedDateOnly = date;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result
				+ ((emailAddress == null) ? 0 : emailAddress.hashCode());
		result = prime * result + queryNumber;
		result = prime * result
				+ ((queryType == null) ? 0 : queryType.hashCode());
		result = prime * result
				+ ((reporterType == null) ? 0 : reporterType.hashCode());
		result = prime * result
				+ ((requesterLoc == null) ? 0 : requesterLoc.hashCode());
		result = prime * result
				+ ((requesterName == null) ? 0 : requesterName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if(!(obj instanceof PartialQueryDTO))
			return false;
		PartialQueryDTO other = (PartialQueryDTO) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		if (queryNumber != other.queryNumber)
			return false;
		if (queryType == null) {
			if (other.queryType != null)
				return false;
		} else if (!queryType.equals(other.queryType))
			return false;
		if (reporterType == null) {
			if (other.reporterType != null)
				return false;
		} else if (!reporterType.equals(other.reporterType))
			return false;
		if (requesterLoc == null) {
			if (other.requesterLoc != null)
				return false;
		} else if (!requesterLoc.equals(other.requesterLoc))
			return false;
		if (requesterName == null) {
			if (other.requesterName != null)
				return false;
		} else if (!requesterName.equals(other.requesterName))
			return false;
		return true;
	}
}
