package com.roche.dss.qtt;

import com.roche.dss.qtt.service.workflow.WorkflowGlobalConstants;

/**
 * @author zerkowsm
 *
 */
public interface QttGlobalConstants extends WorkflowGlobalConstants {
	
	public static final String QTT_DSS_APPLICATION_CODE = "QTT";

	public static final String DMG_COORD_PERMISSION = "ROLE_DMGCO";
	public static final String DMG_SCI_PERMISSION = "ROLE_DMGSC";
	public static final String DSCL_PERMISSION = "ROLE_DSCL";
	public static final String PDS_USER_PERMISSION = "ROLE_PDMS";
	public static final String PS_PERMISSION = "ROLE_PS";
	public static final String AFFILIATE_PERMISSION = "ROLE_AFFIL";
	public static final String QTT_USER_PERMISSION = "ROLE_QTTUSER";
		
	public static final String EMPTY_STRING = "";
	
    public final static String TASKLIST_LINK_TO_QUERY_LABEL = "link.to.queryLabel";

}