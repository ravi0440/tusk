/* 
====================================================================
  $Header$
  @author $Author: borowieb $
  @version $Revision: 1241 $ $Date: 2008-07-01 11:50:42 +0200 (Wt, 01 lip 2008) $

====================================================================
*/
package com.roche.dss.qtt.query.web.qrf;

import com.roche.dss.qtt.query.comms.dto.AttachmentDTO;
import com.roche.dss.qtt.query.web.qrf.model.AbstractDisplayBean;

import java.text.SimpleDateFormat;

/**
 * Display-Bean for transporting Attachment information at the web tier.
 * @author wisbeya
 */
public class AttachmentDisplayBean extends AbstractDisplayBean
{
	private int id;
	private String documentName;
	private String category;
	private String dateDeposited;
	private String author;
	private String fileName;
    private boolean activationFlag;
    private String followupNumberString;
    private String followupNumber;
	
		
    public AttachmentDisplayBean(AttachmentDTO dto)
	{
		this.id = dto.getId();
		this.documentName = dto.getTitle();
		this.category = dto.getCategory();
		this.dateDeposited = new SimpleDateFormat( "dd-MMM-yyyy" ).format( dto.getDateTime() );
		this.author = dto.getAuthor();
		this.fileName = dto.getFileName();
        this.activationFlag = dto.isActivationFlag();
        this.followupNumberString = dto.getFollowupNumberString();
        this.followupNumber = dto.getFollowupNumber(); 
        
	}

    
    @Override
	public String toString()
	{
		return id + "->["
				+ "documentName=" + documentName
				+ ", dateDeposited=" + dateDeposited
				+ ", author=" + author
				+ ", fileName=" + fileName 
                + ", followupNumber=" + followupNumber
                + ", followupNumberString=" + followupNumberString + "']";        
	}
	

    /**
     * @return
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @return
     */
    public String getDateDeposited() {
        return dateDeposited;
    }

    /**
     * @return
     */
    public String getDocumentName() {
        return documentName;
    }

    /**
     * @return
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * @return
     */
    public String getCategory() {
        return category;
    }
    
    
    /**
     * @return Returns the activationFlag.
     */
    public boolean isActivationFlag() {
        return activationFlag;
    }


    /**
     * @param activationFlag The activationFlag to set.
     */
    public void setActivationFlag(boolean activationFlag) {
        this.activationFlag = activationFlag;
    }
    /**
     * @return Returns the followupNumberString.
     */
    public String getFollowupNumberString() {
        return followupNumberString;
    }


    /**
     * @param followupNumberString The followupNumberString to set.
     */
    public void setFollowupNumberString(String followupNumberString) {
        this.followupNumberString = followupNumberString;
    }


    /**
     * @return the followupNumber
     */
    public String getFollowupNumber() {
        return followupNumber;
    }


    /**
     * @param followupNumber the followupNumber to set
     */
    public void setFollowupNumber(String followupNumber) {
        this.followupNumber = followupNumber;
    }
}
