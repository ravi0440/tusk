package com.roche.dss.qtt.query.web.action.oldqrf;

import java.util.List;

import com.roche.dss.qtt.model.AeTerm;
import com.roche.dss.qtt.model.ClinicalTrial;
import com.roche.dss.qtt.model.CtrlOformat;
import com.roche.dss.qtt.model.Manufacturing;
import com.roche.dss.qtt.query.comms.dto.ClinicalTrialDTO;
import com.roche.dss.qtt.query.comms.dto.QueryDTO;
import com.roche.dss.qtt.query.comms.dto.QueryDescDTO;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.model.QueryDescFormModel;
import com.roche.dss.qtt.service.QrfService;

import javax.annotation.Resource;

public class ManufacturingAction extends QRFAbstractAction {

    private QrfService qrfService;

    private Manufacturing manufacturing = new Manufacturing();
    
    private ClinicalTrial clinical = new ClinicalTrial();
    
    private String aeTerm;
	
    private String specialRequest;

    @Resource
    public void setQrfService(QrfService qrfService) {
		this.qrfService = qrfService;
	}
    QueryDescFormModel queryForm = new QueryDescFormModel();

    @Override
    public Object getModel() {
        return queryForm;
    }

    private QueryDescDTO createQueryDescDTO(QueryDescFormModel queryDescForm) {
    	QueryDescDTO dto = new QueryDescDTO();
    	dto.setNatureOfCase(queryDescForm.getNatureofcase());
    	dto.setOtherInfo(queryDescForm.getOtherinfo());
		return dto;
	}

    public String execute()  {
        // the query is update
        if (getSession().get(ActionConstants.QRF_EDIT_QUERY_TYPE) != null) {
            qrfService.updateManufacturing(createQueryDescDTO(queryForm), manufacturing, clinical, aeTerm, specialRequest, (Integer) getSession().get(ActionConstants.QRF_QUERY_ID));
        } else {
            qrfService.addManufacturing(createQueryDescDTO(queryForm), manufacturing, clinical, aeTerm, specialRequest, (Integer) getSession().get(ActionConstants.QRF_QUERY_ID));
        }
        return SUCCESS;
    }

	@Override
	public void validate() {
		super.validate();
		if (queryForm.getNatureofcase() == null || queryForm.getNatureofcase().isEmpty()) {
			addActionError(getText("queryDescForm.natureofcase.displayname"));
		}else 
		if (clinical.getCtrlOformat() == null || clinical.getCtrlOformat().getCtrlOformatId() == 0) {
			addActionError(getText("outputformat.displayname"));
		} else if (clinical.getCtrlOformat().getCtrlOformatId() == CtrlOformat.OUTPUT_FORMAT_OTHER
				&& StringUtils.isBlank(specialRequest)) {
			addActionError(getText("caseForm.othercomments.displayname"));
		}  
		
		
	}

	public List<CtrlOformat> getClotypes() {
        return qrfService.getCtrlOformats();
    }

	public Manufacturing getManufacturing() {
		return manufacturing;
	}

	public void setManufacturing(Manufacturing manufacturing) {
		this.manufacturing = manufacturing;
	}

	public ClinicalTrial getClinical() {
		return clinical;
	}

	public void setClinical(ClinicalTrial clinical) {
		this.clinical = clinical;
	}

	public String getSpecialRequest() {
		return specialRequest;
	}

	public void setSpecialRequest(String specialRequest) {
		this.specialRequest = specialRequest;
	}

	public String getAeTerm() {
		return aeTerm;
	}

	public void setAeTerm(String aeTerm) {
		this.aeTerm = aeTerm;
	}
}
