package com.roche.dss.qtt.utility.config;

@Configuration
//@ImportResource("classpath:application.properties")
public class Config {

    @Value("${qtt.repository.baseDirectory}")
    protected String baseDir;
    
    @Value("${qtt.pdf.executable}")
    protected String pdfExecutable;
    
    @Value("${qtt.pdf.temp}")
    protected String pdfTemp;
    
    @Value("${qtt.pdf.platform}")
    protected String pdfPlatform;

    @Value("${qtt.attachment.download.buffer.size}")
    protected Integer outputBufferSize;
    
    @Value("${qtt.email.host}")
    protected String host;

    @Value("${qtt.email.test.header}")
    protected String testEnvironmentEmailHeader;
    
	@Value("${qtt.dscl.email.address}")
    protected String to;
	
	@Value("${qtt.euqppv.email.address}")
    protected String euqppvEmail;

    @Value("${qtt.reopen.query.email.subject}")
    protected String subject;

    @Value("${qtt.reopen.query.email.body}")
    protected String body;

    @Value("${qtt.initiate.dscl.email.address}")
    protected String inititateDsclEmail;

    @Value("${qtt.initiate.query.amend.email.subject}")
    protected String inititateDsclEmailSubject;

    @Value("${qtt.initiate.query.amend.email.bodyFirstPara}")
    protected String inititateAmendBody1Para;

    @Value("${qtt.initiate.query.amend.email.bodySecondPara}")
    protected String inititateAmendBody2Para;

    @Value("${qtt.initiate.query.amend.email.bodyThirdPara}")
    protected String inititateAmendBody3Para;

    @Value("${qtt.initiate.query.amend.email.bodyLastPara}")
    protected String inititateAmendBodyLastPara;

    @Value("${qtt.initiate.query.reject.email.subject}")
    private String inititateRejectedEmailSubject;

    @Value("${qtt.initiate.query.reject.email.bodyFirstPara}")
    private String inititateRejectedEmail1Para;

    @Value("${qtt.initiate.query.reject.email.bodyLastPara}")
    private String inititateRejectedEmailLastPara;

    @Value("${qtt.initiate.query.accept.email.subject}")
    private String inititateAcceptEmailSubject;

    @Value("${qtt.initiate.query.accept.email.bodyFirstPara}")
        private String inititateAcceptBody1Para;

    @Value("${qtt.initiate.query.accept.email.bodySecondPara}")
        private String inititateAcceptBody2Para;

    @Value("${qtt.initiate.query.accept.email.bodyThirdPara}")
        private String inititateAcceptBody3Para;

    @Value("${qtt.initiate.query.accept.email.bodyFourthPara}")
        private String inititateAcceptBody4Para;

    @Value("${qtt.initiate.query.accept.email.bodyLastPara}")
        private String inititateAcceptBodyLastPara;
    
   
	@Value("${qtt.test.environmet}")
    private String qttTestEnvironment;
    
    @Value("${qtt.test.mi.due.soon.email}")
    private String qttTestMiDueSoonEmail;
    
    @Value("${qtt.test.mi.notification.email}")
    private String qttTestMiNotificationEmail;

    @Value("${qtt.attachment.email.header}")
    private String attachmentMailHeader;

    @Value("${qtt.attachment.email.bodyPartOne}")
    private String attachmentMailBodyPartOne;

    @Value("${qtt.attachment.email.bodyPartTwo}")
    private String attachmentMailBodyPartTwo;

    @Value("${qtt.attachment.email.DmgToBQueryBodyPartOne}")
    private String attachmentMailDmgToBQueryBodyPartOne;

    @Value("${qtt.attachment.email.DmgToBQueryBodyPartTwo}")
    private String attachmentMailDmgToBQueryBodyPartTwo;

    @Value("${qtt.attachment.email.bodyLastPara}")
    private String attachmentMailLastPara;

    @Value("${qtt.application.link}")
    private String applicationAddress;
       
    public String getBaseDir() {
		return baseDir;
	}

	public void setBaseDir(String baseDir) {
		this.baseDir = baseDir;
	}
	
	public String getPdfExecutable() {
		return pdfExecutable;
	}

	public void setPdfExecutable(String pdfExecutable) {
		this.pdfExecutable = pdfExecutable;
	}

	public String getPdfTemp() {
		return pdfTemp;
	}

	public void setPdfTemp(String pdfTemp) {
		this.pdfTemp = pdfTemp;
	}

	public String getPdfPlatform() {
		return pdfPlatform;
	}

	public void setPdfPlatform(String pdfPlatform) {
		this.pdfPlatform = pdfPlatform;
	}

	public Integer getOutputBufferSize() {
		return outputBufferSize;
	}

	public void setOutputBufferSize(Integer outputBufferSize) {
		this.outputBufferSize = outputBufferSize;
	}

    public String getInititateRejectedEmailSubject() {
        return inititateRejectedEmailSubject;
    }

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

    public String getInititateDsclEmail() {
        return inititateDsclEmail;
    }

    public String getInititateDsclEmailSubject() {
        return inititateDsclEmailSubject;
    }

    public void setInititateDsclEmailSubject(String inititateDsclEmailSubject) {
        this.inititateDsclEmailSubject = inititateDsclEmailSubject;
    }

    public String getInititateAmendBody1Para() {
        return inititateAmendBody1Para;
    }

    public void setInititateAmendBody1Para(String inititateAmendBody1Para) {
        this.inititateAmendBody1Para = inititateAmendBody1Para;
    }

    public String getInititateAmendBody2Para() {
        return inititateAmendBody2Para;
    }

    public void setInititateAmendBody2Para(String inititateAmendBody2Para) {
        this.inititateAmendBody2Para = inititateAmendBody2Para;
    }

    public String getInititateAmendBody3Para() {
        return inititateAmendBody3Para;
    }

    public void setInititateAmendBody3Para(String inititateAmendBody3Para) {
        this.inititateAmendBody3Para = inititateAmendBody3Para;
    }

    public String getInititateAmendBodyLastPara() {
        return inititateAmendBodyLastPara;
    }

    public void setInititateAmendBodyLastPara(String inititateAmendBodyLastPara) {
        this.inititateAmendBodyLastPara = inititateAmendBodyLastPara;
    }

    public String getInititateRejectedEmail1Para() {
        return inititateRejectedEmail1Para;
    }

    public String getInititateRejectedEmailLastPara() {
        return inititateRejectedEmailLastPara;
    }

    public void setInititateDsclEmail(String inititateDsclEmail) {
        this.inititateDsclEmail = inititateDsclEmail;
    }


    public String getInititateAcceptEmailSubject() {
        return inititateAcceptEmailSubject;
    }

    public String getInititateAcceptBody1Para() {
        return inititateAcceptBody1Para;
    }

    public String getInititateAcceptBody2Para() {
        return inititateAcceptBody2Para;
    }

    public String getInititateAcceptBody3Para() {
        return inititateAcceptBody3Para;
    }

    public String getInititateAcceptBody4Para() {
        return inititateAcceptBody4Para;
    }

    public String getInititateAcceptBodyLastPara() {
        return inititateAcceptBodyLastPara;
    }
    
    public String getQttTestEnvironment() {
		return qttTestEnvironment;
	}

	public void setQttTestEnvironment(String qttTestEnvironment) {
		this.qttTestEnvironment = qttTestEnvironment;
	}

	public String getQttTestMiDueSoonEmail() {
		return qttTestMiDueSoonEmail;
	}

	public void setQttTestMiDueSoonEmail(String qttTestMiDueSoonEmail) {
		this.qttTestMiDueSoonEmail = qttTestMiDueSoonEmail;
	}

	public String getQttTestMiNotificationEmail() {
		return qttTestMiNotificationEmail;
	}

	public void setQttTestMiNotificationEmail(String qttTestMiNotificationEmail) {
		this.qttTestMiNotificationEmail = qttTestMiNotificationEmail;
	}

    public String getTestEnvironmentEmailHeader() {
		return testEnvironmentEmailHeader;
	}

	public void setTestEnvironmentEmailHeader(String testEnvironmentEmailHeader) {
		this.testEnvironmentEmailHeader = testEnvironmentEmailHeader;
	}

	public boolean isTestEnvironment() {
		if(!qttTestEnvironment.equals("false") ){
			return true;
		}
		return false;
	}

	public String getEuqppvEmail() {
		return euqppvEmail;
	}

	public void setEuqppvEmail(String euqppvEmail) {
		this.euqppvEmail = euqppvEmail;
	}

    public String getAttachmentMailHeader() {
        return attachmentMailHeader;
    }

    public String getAttachmentMailBodyPartOne() {
        return attachmentMailBodyPartOne;
    }

    public String getAttachmentMailBodyPartTwo() {
        return attachmentMailBodyPartTwo;
    }

    public String getAttachmentMailDmgToBQueryBodyPartOne() {
        return attachmentMailDmgToBQueryBodyPartOne;
    }

    public String getAttachmentMailDmgToBQueryBodyPartTwo() {
        return attachmentMailDmgToBQueryBodyPartTwo;
    }

    public String getAttachmentMailLastPara() {
        return attachmentMailLastPara;
    }

    public String getApplicationAddress() {
        return applicationAddress;
    }
}