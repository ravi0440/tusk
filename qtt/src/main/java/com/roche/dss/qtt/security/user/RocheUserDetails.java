package com.roche.dss.qtt.security.user;

import java.util.Collection;

/**
 * @author zerkowsm
 *
 */
public interface RocheUserDetails extends UserDetails {

    public Collection<GrantedAuthority> getAuthorities();

    public String getPassword();

    public String getUsername();

    public String getDisplayName();

    public String getLocation();

    public String getDepartment();

    public String getFirstName();

    public String getSurname();

    public String getMail();

    public String getCountry();

    public String getCountryCode();

    public String getDomain();

    public String getDN();
    
    public String getPhone();

    public String getFax();

    public String getSite();
    
    public String getMsExchHomeServerName();

    Long getId();
}