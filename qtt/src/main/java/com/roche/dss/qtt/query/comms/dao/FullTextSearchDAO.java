package com.roche.dss.qtt.query.comms.dao;

import com.roche.dss.qtt.service.SearchResults;

public interface FullTextSearchDAO {
    public static final String SEARCH_FIELDS[] = {
    		"querySeq" /*OK, it was queryId*/, "queryNumber"/*OK*/, "followupNumber"/*OK*/, "sourceCountry"/*OK*/,
            "queryType.queryType" /*OK, it was queryType*/, "reporterType.reporterType" /*OK, it was reporterType*/, "queryStatusCode.statusCode" /*OK, it was statusCode*/, "currentDueDate" /*OK*/,
            "originalDueDate"/*OK*/, "urgencyReason"/*OK*/, "workflowRouteType"/*OK*/, "dateRequested" /*OK, it was requestedDate*/,
            "dmgDueDate"/*OK, it was dMGResponseDate*/, "expiryDate" /*OK, it was queryExpiryDate*/, "querySummary" /*OK*/, "preQueryPrepComment"/*OK*/,
            "caseComment"/*OK*/, "oformatSpecialRequest"/*OK*/, "mnftBatchNumber"/*OK*/, "indNumber"/*OK*/,
            "datetimeSubmitted"/*OK, it was submitDate*/, "queryLabel"/*OK*/, "requesterQueryLabel"/*OK*/, "requester.firstName" /*OK, it was requesterFirstName*/,
            "requester.surname"/*OK, it was requesterLastName*/, "requester.phone" /*OK, it was requesterPhone*/, "requester.fax" /*OK, it was requesterFax*/, "requester.email" /*OK, it was requesterEmail*/,
            "requester.country.country"/*OK, it was requesterCountry*/, "queryDescription.natureOfCase" /*OK, it was natureOfCase*/, "queryDescription.indexCase" /*OK, it was indexCase*/, "queryDescription.prevMedicalHistory"/*OK, it was prevMedicalHistory*/,
            "queryDescription.signAndSymptom"/*OK, it was signsAndSymptoms*/, "queryDescription.conmeds"/*OK, it was conmeds*/, "queryDescription.investigation" /*OK, it was investigations*/, "queryDescription.diagnosis" /*OK, it was diagnoses*/, "queryDescription.otherInfo" /*OK, it was otherInfo*/,
            "preQueryPreparations.pqpType.pqpType"/*OK, it was pqpType*/, "preQueryPreparations.otherComment" /*OK, it was pqpOtherComment*/, "pregnancy.prgyExptype.exposureType" /*OK, it was pregnancyExposureType*/, "pregnancy.outcome" /*OK, it was pregnancyOutcome*/,
            "performanceMetric.requestDescription" /*OK, it was pmxRequestDescription*/, "nonAffiliateNotes.text" /*OK, it was nonAffiliateNotes*/, "affiliateNotes.text" /*OK, it was affiliateNotes*/,
            "drugs.drugRetrievalName" /*OK, it was drugRetrievalName*/, "drugs.innGenericName" /*OK, it was innGenericName*/, "drugs.route" /*OK, it was route*/, "drugs.formulation" /*OK, it was formulation*/, "drugs.indication" /*OK, it was indication*/,
            "drugs.dose" /*OK, it was does*/, "trial.caseSelection"/*OK, it was caseSelection*/, "trial.protocolNumber"/*OK, it was protocolNumbers*/, "trial.patientNumber"/*OK, it was patientNumbers*/, "trial.ctrlOformat.ctrlOformat"/*OK, it was ctrlOfFormat*/,
            "trial.crtnNumber" /*OK, it was crtnNumbers*/, "cases.aerNumber"/*OK, was aerNumber*/, "cases.localReferenceNumber"/*OK, was localReferenceNumber*/, "cases.externalReferenceNumber"/*OK, it was externalReferenceNumber*/,
            "aeTerm.description" /*OK, it was aeTerms*/, "labelingDocTypes.ldocType"/*OK, it was labelingDocType*/, "dmgMember"/*OK*/, "pSMember"/*OK*/, "dSCLMember"/*OK*/,
            "retrievalPeriod.interval" /*OK, it was interval*/, "retrievalPeriod.startTs"/*FIXME it was timePeriodAddressedFrom*/, "retrievalPeriod.endTs"/*FIXME it was timePeriodAddressedTo*/,
            "responseCoordinator" /*OK, it was queryResponseCoordinator*/,
            "initiateDate" /*OK*/, "finalDeliveryDate" /*OK*/
            /*I will not index affiliateAccess since it is only 'Y' or 'N'*/
            /*I will not index expiredResponse since it was 'Y' or 'N' and additionally was time dependent and not only dependent on the data*/
    };
    
    public static final String SEARCH_ATTACH_FIELDS[] = {
    	"attachments.content", "attachments.filename"/*OK, it was fileNames*/, "attachments.title"/*OK, it was fileTitles*/, "attachments.author"/*OK, it was fileAuthors*/
    };

    public SearchResults searchFT(final String keywords, final String[] fields);
    
    public SearchResults searchDatabase(String keywords);
    
    public SearchResults searchAttachments(String keywords);
    
    public SearchResults searchBoth(String keywords);
}
