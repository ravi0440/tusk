package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.workflow.WorkflowService;
import com.roche.dss.qtt.utility.Activities;
import javax.annotation.Resource;

/**
 * This action presents query notes list
 * <p/>
 * User: pruchnil
 */
public class RetrieveQueryNotesAction extends CommonActionSupport {
    private Query query;
    private long queryId;
    private long taskId;

    private QueryService queryService;
    private WorkflowService workflowService;

    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Resource
    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    @Override
    public String execute() throws Exception {
        query = queryService.find(queryId);

        boolean updateAlert = true;
        TaskInstance taskInstance = workflowService.getTaskInstance(taskId);

        // if DMG Coordinator and DMG Assign activity then do not update the alert
        if (isDMGCoord()) {
            TaskInstance task = workflowService.getTaskInstance(taskId);
            if (task != null) {
                if (task.getName().equals(Activities.DMG_ASSESS.getTaskName())) {
                    updateAlert = false;
                }
            }
        }
        if (updateAlert && !isAffiliate()) {
            queryService.updateAlertFlag(queryId, false);
        }
        return SUCCESS;
    }

    public Query getQuery() {
        return query;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }
}
