package com.roche.dss.qtt.security.ldap.userdetails;

import java.util.Collection;

import com.roche.dss.qtt.security.model.DSSUser;
import com.roche.dss.qtt.security.user.RocheUserDetailsImpl;

/**
 * @author zerkowsm
 *
 */
public class QTTUserDetailsContextMapper implements UserDetailsContextMapper {
	
	private final Log logger = LogFactory.getLog(QTTUserDetailsContextMapper.class);

	@Override
    public UserDetails mapUserFromContext(DSSUser user, String username, Collection<GrantedAuthority> authorities) {
		
		if(logger.isDebugEnabled()){
			logger.debug("Mapping user details for user: " + username);
		}
        
        RocheUserDetailsImpl.Essence essence = new RocheUserDetailsImpl.Essence(user);
        essence.setAuthorities(authorities);

        return essence.createUserDetails();
	}
	
}