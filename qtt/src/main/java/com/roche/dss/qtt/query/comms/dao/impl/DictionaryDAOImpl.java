package com.roche.dss.qtt.query.comms.dao.impl;

import com.roche.dss.qtt.model.AttachmentCategory;
import com.roche.dss.qtt.model.LdocType;
import com.roche.dss.qtt.model.QttCountry;
import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.model.ReporterType;
import com.roche.dss.qtt.model.WorkflowRouteType;
import com.roche.dss.qtt.query.comms.dao.DictionaryDAO;
import java.util.Arrays;
import java.util.List;

import static com.roche.dss.qtt.utility.Activities.*;

@Repository("dictionaryDAO")
public class DictionaryDAOImpl implements DictionaryDAO{
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<AttachmentCategory> getAllAttachmentCategories() {
        return entityManager.createNamedQuery("attachmentCategory.findAll", AttachmentCategory.class).getResultList();
    }
    
    @Override
    public List<String> getUsedRetrievalNames() {
    	return entityManager.createNamedQuery("list.drugRetrNames", String.class).getResultList();
    }

	@Override
	public List<String> getUsedGenericNames() {
		return entityManager.createNamedQuery("list.genericNames", String.class).getResultList();
	}
	
	@Override
	public List<String> getUsedIndications() {
		return entityManager.createNamedQuery("list.drugIndics", String.class).getResultList();
	}
	
	@Override
	public List<QueryType> getQueryTypes() {
		List<QueryType> list = entityManager.createNamedQuery("list.activeQueryTypes", QueryType.class).getResultList();
		return list;
	}
	
	@Override
	public List<QueryType> getAllQueryTypes() {
		return entityManager.createNamedQuery("list.queryTypes", QueryType.class).getResultList();
	}
	
	@Override
	public List<String> getAllQueryTypeNames() {
		return entityManager.createNamedQuery("list.queryTypeNames", String.class).getResultList();
	}
	
	@Override
	public List<String> getUsedResponseAuthors() {
		return entityManager.createNamedQuery("list.authors", String.class).getResultList();
	}
	
	@Override
	public List<LdocType> getUsedLabellingDocs() {
		return entityManager.createNamedQuery("list.labellingDocs", LdocType.class).getResultList();
	}
	
	@Override
    public List<WorkflowRouteType> getProcessTypes() {
    	return entityManager.createNamedQuery("list.processTypes", WorkflowRouteType.class).getResultList();
    }
    
	@Override
    public List<String> getDMGUserInvolvedNames() {
        return entityManager.createNamedQuery("TaskInstance.listInvolvedUsers", String.class).setParameter("values", Arrays.asList(UNKNOWN.getDmgActivities())).getResultList();
    }
	
	@Override
    public List<String> getPSUserInvolvedNames() {
    	return entityManager.createNamedQuery("TaskInstance.listInvolvedUsers", String.class).setParameter("values", Arrays.asList(UNKNOWN.getPsActivities())).getResultList();
    }

	@Override
    public List<String> getDSCLUserInvolvedNames() {
    	return entityManager.createNamedQuery("TaskInstance.listInvolvedUsers", String.class).setParameter("values", Arrays.asList(UNKNOWN.getDsclActivities())).getResultList();
    }
	
	@Override
	public List<String> getUsedRequestorLastNames() {
		return entityManager.createNamedQuery("list.reqLastNames", String.class).getResultList();
	}
	
	@Override
	public List<QttCountry> getUsedRequestorCountries() {
		return entityManager.createNamedQuery("list.reqCountries", QttCountry.class).getResultList();
	}
	
	@Override
	public List<ReporterType> getUsedReporterTypes() {
		return entityManager.createNamedQuery("list.reporterTypes", ReporterType.class).getResultList();
	}
	
	@Override
	public List<QttCountry> getUsedSourceCountries() {
		return entityManager.createNamedQuery("list.sourceCountries", QttCountry.class).getResultList();
	}

	@Override
	public List<String> getAllTaskNames() {
		return entityManager.createNamedQuery("Task.getAllTaskNames", String.class).getResultList();
	}
}
