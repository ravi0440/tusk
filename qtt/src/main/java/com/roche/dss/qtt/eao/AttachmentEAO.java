package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.Attachment;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

public interface AttachmentEAO extends QueryRelatedEAO<Attachment>{

    List<Attachment> findForClosedQuery(long querySeq, String followupNumber);

    DateTime getLatestFinalDocCreationDate(long queryId);

    String getMaxFollowupNumber(long queryId);
}
