package com.roche.dss.qtt.security_old;

import java.io.IOException;


public class DssSecurityProcessingFilterEntryPoint implements AuthenticationEntryPoint {

	private static final String PARAM_DEST_PATH = "destPath";

	private String loginFormUrl;
	
	@Override
	public void commence(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException authException)
			throws IOException, ServletException {
		
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

		String redirectUrl = loginFormUrl + "?" + PARAM_DEST_PATH + "=" + httpRequest.getRequestURL().toString();

		httpResponse.sendRedirect(httpResponse.encodeRedirectURL(redirectUrl)); 
	}

	public void setLoginFormUrl(String loginFormUrl) {
		this.loginFormUrl = loginFormUrl;
	}

}
