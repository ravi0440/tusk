package com.roche.dss.qtt.service.workflow.handler;

import com.roche.dss.qtt.service.workflow.WorkflowGlobalConstants;

/**
 * @author zerkowsm
 *
 */
public class QTTDMGAssignmentHandler implements AssignmentHandler, WorkflowGlobalConstants {

	private static final long serialVersionUID = -6166361647193143208L;

	@Override
	public void assign(Assignable assignable, ExecutionContext executionContext)
			throws Exception {

		assignable.setActorId((String) executionContext.getContextInstance().getVariable(DMG_ASSIGNED_ACTOR_ID));
	
	}

}
