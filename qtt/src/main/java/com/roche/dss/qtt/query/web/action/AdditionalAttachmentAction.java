package com.roche.dss.qtt.query.web.action;


import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.web.model.FileUploadBean;
import com.roche.dss.qtt.security.model.DSSUser;
import com.roche.dss.qtt.service.AttachmentService;
import com.roche.dss.qtt.service.FileOperationsService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.utility.EmailSender;
import com.roche.dss.qtt.utility.config.Config;
import java.io.File;

public class AdditionalAttachmentAction extends CommonActionSupport {

    private static final int MAX_FILENAME_LENGTH = 100;

    private static final String QRF_QUERY_TYPE = "1";

    private static final Logger logger = LoggerFactory.getLogger(AdditionalAttachmentAction.class);

    private Query query;
    private long queryId;
    private FileUploadBean attachment = new FileUploadBean();
    private Long freeDiskSpace;
    private String nextActionName;

    @Autowired
    private QueryService queryService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private EmailSender emailSender;

    @Autowired
    private Config config;

    @Autowired
    private FileOperationsService fileOperationsService;

    @Override
    @SkipValidation
    public String execute() throws Exception {
        query = queryService.find(queryId);
        setFreeDiskSpace();
        return SUCCESS;
    }

    public String upload() throws Exception {
        attachment.setType(QRF_QUERY_TYPE);
        query = queryService.find(queryId);
        fileOperationsService.correctFileNameOfAttachmentIfNeeded(query, attachment);
        Attachment addedAttachment = attachmentService.addFileToRepo(queryId, attachment, getLoggedUserName(), isAffiliate());

        String actorId = queryService.findActorIdFromDmgOrMiTask(query);
        if(actorId!=null){
	        DSSUser dssUser = securityService.getDSSUser(actorId);
	        if (dssUser != null) {
	            emailSender.sendNotificationForAddedAttachment(query, addedAttachment, dssUser.getEmail(),
	                    config.getAttachmentMailBodyPartOne(), config.getAttachmentMailBodyPartTwo());
	        }
        }
        return SUCCESS;
    }

    @Override
    public void validate() {
        if (StringUtils.isEmpty(attachment.getGivenFileName())) {
            addActionError(getText("attach.name.required"));
        }
        if (attachment.getUpload() == null) {
            addActionError(getText("attach.file.required"));
        } else {
            if (attachment.getUpload().length() == 0) {
                addActionError(getText("attach.file.empty"));
            }
            if (StringUtils.length(attachment.getUploadFileName()) > MAX_FILENAME_LENGTH) {
                addActionError(getText("attach.name.length"));
            }
        }
        super.validate();
    }

    private void setFreeDiskSpace() {
        File tempDir = (File) getServletContext().getAttribute("javax.servlet.context.tempdir");
        freeDiskSpace = tempDir.getFreeSpace();
        logger.info("tempDir: {}, freeDiskSpace: {}", tempDir, freeDiskSpace);
    }

    @Override
    public void onActionErrors() {
        query = queryService.find(queryId);
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public Query getQuery() {
        return query;
    }

    public FileUploadBean getAttachment() {
        return attachment;
    }

    public void setAttachment(FileUploadBean attachment) {
        this.attachment = attachment;
    }

    public String getNextActionName() {
        return nextActionName;
    }

    public void setNextActionName(String nextActionName) {
        this.nextActionName = nextActionName;
    }

    public Long getFreeDiskSpace() {
        return freeDiskSpace;
    }

    public void setFreeDiskSpace(Long freeDiskSpace) {
        this.freeDiskSpace = freeDiskSpace;
    }

}
