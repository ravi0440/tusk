package com.roche.dss.qtt.model;
// Generated 2010-10-04 12:00:38 by Hibernate Tools 3.3.0.GA

import java.util.HashSet;
import java.util.Set;

/**
 * AttachmentCategory generated by hbm2java
 */
@NamedQueries({
    @NamedQuery(name = "attachmentCategory.findAll", query = "select ac from AttachmentCategory ac")
})
@Entity
@Table(name = "ATTACHMENT_CATEGORIES")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class AttachmentCategory implements java.io.Serializable {

	private static final long serialVersionUID = -8098474845447182655L;
	public static final byte CATEGORY_FINAL_RESPONSE = 3;
	
	private byte attachmentCategoryId;
	private String attachmentCategory;
	private char affiliateVisibilityFlag;
	private Set<Attachment> attachments = new HashSet<Attachment>(0);

	public AttachmentCategory() {
	}

	public AttachmentCategory(byte attachmentCategoryId,
			String attachmentCategory, char affiliateVisibilityFlag) {
		this.attachmentCategoryId = attachmentCategoryId;
		this.attachmentCategory = attachmentCategory;
		this.affiliateVisibilityFlag = affiliateVisibilityFlag;
	}

	public AttachmentCategory(byte attachmentCategoryId,
			String attachmentCategory, char affiliateVisibilityFlag,
			Set<Attachment> attachments) {
		this.attachmentCategoryId = attachmentCategoryId;
		this.attachmentCategory = attachmentCategory;
		this.affiliateVisibilityFlag = affiliateVisibilityFlag;
		this.attachments = attachments;
	}

    public AttachmentCategory(String id) {
        this.attachmentCategoryId = Byte.valueOf(id);
    }

    @Id
	@Column(name = "ATTACHMENT_CATEGORY_ID", nullable = false, precision = 2, scale = 0)
	public byte getAttachmentCategoryId() {
		return this.attachmentCategoryId;
	}

	public void setAttachmentCategoryId(byte attachmentCategoryId) {
		this.attachmentCategoryId = attachmentCategoryId;
	}

	@Column(name = "ATTACHMENT_CATEGORY", nullable = false, length = 40)
	public String getAttachmentCategory() {
		return this.attachmentCategory;
	}

	public void setAttachmentCategory(String attachmentCategory) {
		this.attachmentCategory = attachmentCategory;
	}

	@Column(name = "AFFILIATE_VISIBILITY_FLAG", nullable = false, length = 1)
	public char getAffiliateVisibilityFlag() {
		return this.affiliateVisibilityFlag;
	}

	public void setAffiliateVisibilityFlag(char affiliateVisibilityFlag) {
		this.affiliateVisibilityFlag = affiliateVisibilityFlag;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "attachmentCategory")
	public Set<Attachment> getAttachments() {
		return this.attachments;
	}

	public void setAttachments(Set<Attachment> attachments) {
		this.attachments = attachments;
	}

}
