package com.roche.dss.qtt.query.web.menu;

import java.util.List;

/**
 * @author zerkowsm
 * 
 */
@Service
public class MenuViewPreparer implements ViewPreparer {

	private List<Menu> menuList;

	@Autowired
	private MenuService menuService;
	

	public MenuService getMenuService() {
		return menuService;
	}

	public void setMenuService(MenuService menuService) {
		this.menuService = menuService;
	}    
	
	public List<Menu> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<Menu> menuList) {
		this.menuList = menuList;
	}
	
	@Override
	public void execute(TilesRequestContext tilesContext, AttributeContext attributeContext) throws PreparerException {
		menuList = menuService.getMenuList();
		attributeContext.putAttribute("menuItems", new Attribute(menuList));
	}

}