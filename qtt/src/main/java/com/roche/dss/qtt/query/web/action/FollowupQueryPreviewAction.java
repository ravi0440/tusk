package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.FuQuery;
import com.roche.dss.qtt.service.FollowupQueryService;
import javax.annotation.Resource;


public class FollowupQueryPreviewAction extends CommonActionSupport {
    private static final Logger logger = LoggerFactory.getLogger(FollowupQueryPreviewAction.class);
    
    private FuQuery query;
    private long queryId;
    private long followupId;

    private FollowupQueryService queryService;

    @Resource
    public void setFollowupQueryService(FollowupQueryService queryService) {
        this.queryService = queryService;
    }

    public String execute() {
        query = queryService.getFollowupQueryDetails(queryId, followupId);
        previewMode = ActionConstants.PREVIEW_MODE_PRNT_CLOSE;
        return SUCCESS;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public FuQuery getQuery() {
        return query;
    }

    public long getFollowupId() {
        return followupId;
    }

    public void setFollowupId(long followupId) {
        this.followupId = followupId;
    }
}