package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.RetrievalPeriod;

public interface RetrievalPeriodEAO extends QueryRelatedEAO<RetrievalPeriod>{
	
}