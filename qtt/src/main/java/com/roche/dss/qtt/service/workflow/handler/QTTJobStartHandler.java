package com.roche.dss.qtt.service.workflow.handler;


import com.roche.dss.qtt.service.workflow.WorkflowGlobalConstants;
import com.roche.dss.qtt.utility.Activities;

/**
 * @author zerkowsm
 *
 */
public class QTTJobStartHandler implements ActionHandler, WorkflowGlobalConstants {

	private static final long serialVersionUID = -1288506782908661533L;

	@Override
	public void execute(ExecutionContext executionContext) throws Exception {
		
		ProcessInstance processInstance = executionContext.getProcessInstance();
		
		TaskMgmtInstance tmi = processInstance.getTaskMgmtInstance();
		Task task = tmi.getTaskMgmtDefinition().getTask(Activities.JOB_START.getTaskName());
		task.getParent().setProcessDefinition(processInstance.getProcessDefinition());
		//task.setProcessDefinition(processInstance.getProcessDefinition());

		TaskInstance taskInstance = tmi.createTaskInstance(task, processInstance.getRootToken());
		taskInstance.setActorId((String) executionContext.getContextInstance().getVariable(PROCESS_INSTANCE_START_ACTOR_ID));
		//taskInstance.setEnd(new Date());
		taskInstance.end();
		taskInstance.getTaskMgmtInstance().setProcessInstance(processInstance);
		executionContext.getJbpmContext().getSession().save(taskInstance);
		
	}




}