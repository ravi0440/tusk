package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.ClinicalTrialEAO;
import com.roche.dss.qtt.model.ClinicalTrial;

@Repository("clinicalTrialEAO")
public class ClinicalTrialEAOImpl extends AbstractQueryRelatedEAO<ClinicalTrial> implements ClinicalTrialEAO {

}