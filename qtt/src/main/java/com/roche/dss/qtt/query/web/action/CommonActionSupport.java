package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.QttGlobalConstants;
import com.roche.dss.qtt.model.AttachmentCategory;
import com.roche.dss.qtt.security.service.SecurityService;
import com.roche.dss.qtt.security.user.RocheUserDetails;
import com.roche.dss.qtt.service.DictionaryService;

import javax.annotation.Resource;

import java.util.Date;
import java.util.List;
import java.util.Properties;


/**
 * This class contains common things for actions
 * <p/>
 * User: pruchnil
 */
public class CommonActionSupport extends ActionSupport implements QttGlobalConstants {
    private static final long serialVersionUID = -5931161836672421405L;

    public final static String ALREADY = "alreadyProcessed";
    public final static String DENIED = "denied";
    public final static String UPDATE = "update";
    public final static String MODIFIED = "dataAlreadyModified";
    public final static String ERROR = "error";
    public static final String ASSIGNMENT = "assignment";
    public static final String TASK_ALREADY_BEGUN = "taskAlreadyBegun";
    public static final String TASK_ALREADY_COMPLETED = "taskAlreadyCompleted";
    public static final String NO_LAYOUT = "nolayout";
    public static final String OTHER = "other";


    protected String previewMode;

    protected SecurityService securityService;

    protected DictionaryService dictionaryService;
    
    @Autowired
    protected Properties properties;

    @Resource
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Resource
    public void setDictionaryService(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    public RocheUserDetails getLoggedUser() {
        return securityService.getLoggedUser();
    }

    public String getLoggedUserDisplayName() {
        return securityService.getLoggedUserDisplayName();
    }

    public String getLoggedUserName() {
        return securityService.getLoggedUserName();
    }

    public boolean isShowSummaryLink() {
    	return (securityService.isAllowedCheck(DSCL_PERMISSION) 
    			|| securityService.isAllowedCheck(DMG_COORD_PERMISSION) 
    			|| securityService.isAllowedCheck(PS_PERMISSION))
    			&& !securityService.isAllowedCheck(AFFILIATE_PERMISSION);
    }

    public Date getTodaysDate() {
        return new Date();
    }

    public String getPreviewMode() {
        return previewMode;
    }

    public String getDsclEmail() {
        return properties.getProperty("qtt.dscl.email.address");
    }

    public boolean isAffiliate() {
        return securityService.isAllowedCheck(AFFILIATE_PERMISSION);
    }

    public boolean isDscl() {
        return securityService.isAllowedCheck(DSCL_PERMISSION);
    }

    public boolean isDMGCoord() {
        return securityService.isAllowedCheck(DMG_COORD_PERMISSION);
    }

    public boolean isDMGScientist() {
        return securityService.isAllowedCheck(DMG_SCI_PERMISSION);
    }

    public boolean isPS() {
        return securityService.isAllowedCheck(PS_PERMISSION);
    }
    
    public boolean isPDS(){
    	return securityService.isAllowedCheck(PDS_USER_PERMISSION);
    }

    @Override
    public void validate() {
        super.validate();
        if (hasErrors()) {
            onActionErrors();
        }
    }

    public void onActionErrors() {
    }

}
