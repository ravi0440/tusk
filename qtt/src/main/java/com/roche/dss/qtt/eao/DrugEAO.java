package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.Drug;
import com.roche.dss.qtt.model.Query;

import java.util.List;

/**
 * User: pruchnil
 */
public interface DrugEAO extends QueryRelatedEAO<Drug> {
    public Drug findDrugForQueryPreview(long querySeq);
    void removeFlaggedQueryDrugs(Query query, Character s);

    List<String> getGenericNames();

    List<String> getRetrievalNames();

    List<Drug> findAllByQueryId(long queryId);
}
