package com.roche.dss.qtt.model;
// Generated 2010-10-04 12:00:38 by Hibernate Tools 3.3.0.GA

import java.math.BigDecimal;

/**
 * TempAuditHistoryId generated by hbm2java
 */
@Embeddable
public class TempAuditHistoryId implements java.io.Serializable {

	private static final long serialVersionUID = -7402034777177140574L;
	
	private String country;
	private String site;
	private String activityDate;
	private String name;
	private String queryNo;
	private BigDecimal queryIteration;
	private String activityType;
	private String workflowRouteType;
	private String queryType;
	private String reporterType;

	public TempAuditHistoryId() {
	}

	public TempAuditHistoryId(String country, String site, String activityDate,
			String name, String queryNo, BigDecimal queryIteration,
			String activityType, String workflowRouteType, String queryType,
			String reporterType) {
		this.country = country;
		this.site = site;
		this.activityDate = activityDate;
		this.name = name;
		this.queryNo = queryNo;
		this.queryIteration = queryIteration;
		this.activityType = activityType;
		this.workflowRouteType = workflowRouteType;
		this.queryType = queryType;
		this.reporterType = reporterType;
	}

	@Column(name = "COUNTRY", length = 50)
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "SITE", length = 50)
	public String getSite() {
		return this.site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	@Column(name = "ACTIVITY_DATE", length = 10)
	public String getActivityDate() {
		return this.activityDate;
	}

	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}

	@Column(name = "NAME", length = 76)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "QUERY_NO", length = 40)
	public String getQueryNo() {
		return this.queryNo;
	}

	public void setQueryNo(String queryNo) {
		this.queryNo = queryNo;
	}

	@Column(name = "QUERY_ITERATION", precision = 38, scale = 0)
	public BigDecimal getQueryIteration() {
		return this.queryIteration;
	}

	public void setQueryIteration(BigDecimal queryIteration) {
		this.queryIteration = queryIteration;
	}

	@Column(name = "ACTIVITY_TYPE", length = 50)
	public String getActivityType() {
		return this.activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	@Column(name = "WORKFLOW_ROUTE_TYPE", length = 12)
	public String getWorkflowRouteType() {
		return this.workflowRouteType;
	}

	public void setWorkflowRouteType(String workflowRouteType) {
		this.workflowRouteType = workflowRouteType;
	}

	@Column(name = "QUERY_TYPE", length = 60)
	public String getQueryType() {
		return this.queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	@Column(name = "REPORTER_TYPE", length = 50)
	public String getReporterType() {
		return this.reporterType;
	}

	public void setReporterType(String reporterType) {
		this.reporterType = reporterType;
	}

	@Override
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TempAuditHistoryId))
			return false;
		TempAuditHistoryId castOther = (TempAuditHistoryId) other;

		return ((this.getCountry() == castOther.getCountry()) || (this
				.getCountry() != null
				&& castOther.getCountry() != null && this.getCountry().equals(
				castOther.getCountry())))
				&& ((this.getSite() == castOther.getSite()) || (this.getSite() != null
						&& castOther.getSite() != null && this.getSite()
						.equals(castOther.getSite())))
				&& ((this.getActivityDate() == castOther.getActivityDate()) || (this
						.getActivityDate() != null
						&& castOther.getActivityDate() != null && this
						.getActivityDate().equals(castOther.getActivityDate())))
				&& ((this.getName() == castOther.getName()) || (this.getName() != null
						&& castOther.getName() != null && this.getName()
						.equals(castOther.getName())))
				&& ((this.getQueryNo() == castOther.getQueryNo()) || (this
						.getQueryNo() != null
						&& castOther.getQueryNo() != null && this.getQueryNo()
						.equals(castOther.getQueryNo())))
				&& ((this.getQueryIteration() == castOther.getQueryIteration()) || (this
						.getQueryIteration() != null
						&& castOther.getQueryIteration() != null && this
						.getQueryIteration().compareTo(
								castOther.getQueryIteration()) == 0))
				&& ((this.getActivityType() == castOther.getActivityType()) || (this
						.getActivityType() != null
						&& castOther.getActivityType() != null && this
						.getActivityType().equals(castOther.getActivityType())))
				&& ((this.getWorkflowRouteType() == castOther
						.getWorkflowRouteType()) || (this
						.getWorkflowRouteType() != null
						&& castOther.getWorkflowRouteType() != null && this
						.getWorkflowRouteType().equals(
								castOther.getWorkflowRouteType())))
				&& ((this.getQueryType() == castOther.getQueryType()) || (this
						.getQueryType() != null
						&& castOther.getQueryType() != null && this
						.getQueryType().equals(castOther.getQueryType())))
				&& ((this.getReporterType() == castOther.getReporterType()) || (this
						.getReporterType() != null
						&& castOther.getReporterType() != null && this
						.getReporterType().equals(castOther.getReporterType())));
	}

	@Override
	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getCountry() == null ? 0 : this.getCountry().hashCode());
		result = 37 * result
				+ (getSite() == null ? 0 : this.getSite().hashCode());
		result = 37
				* result
				+ (getActivityDate() == null ? 0 : this.getActivityDate()
						.hashCode());
		result = 37 * result
				+ (getName() == null ? 0 : this.getName().hashCode());
		result = 37 * result
				+ (getQueryNo() == null ? 0 : this.getQueryNo().hashCode());
		result = 37
				* result
				+ (getQueryIteration() == null ? 0 : this.getQueryIteration()
						.hashCode());
		result = 37
				* result
				+ (getActivityType() == null ? 0 : this.getActivityType()
						.hashCode());
		result = 37
				* result
				+ (getWorkflowRouteType() == null ? 0 : this
						.getWorkflowRouteType().hashCode());
		result = 37 * result
				+ (getQueryType() == null ? 0 : this.getQueryType().hashCode());
		result = 37
				* result
				+ (getReporterType() == null ? 0 : this.getReporterType()
						.hashCode());
		return result;
	}

}
