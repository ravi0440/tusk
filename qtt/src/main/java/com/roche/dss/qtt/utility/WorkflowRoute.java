package com.roche.dss.qtt.utility;

/**
 * User: pruchnil
 */
public enum WorkflowRoute {
    A1("Assign to DMG for data search"), A2("Assign to self"), B("Assign to Safety Science/Medical Information for medical interpretation");
    private String label;

    private WorkflowRoute(String label) {
        this.label = label;
    }

    public String getLabel() {
        return new StringBuffer(this.name()).append(" : ").append(label).toString();
    }
    
    public String getViewLabel() {
        return new StringBuffer("<B>" + this.name() + "</B>").append(" - ").append(label).toString();
    }

    public String getName() {
        return this.name();
    }
}
