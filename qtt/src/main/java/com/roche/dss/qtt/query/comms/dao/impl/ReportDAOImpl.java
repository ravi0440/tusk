package com.roche.dss.qtt.query.comms.dao.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.roche.dss.qtt.query.comms.dao.ReportDAO;
import com.roche.dss.qtt.query.comms.dto.ReportDTO;
import com.roche.dss.qtt.query.web.model.ReportCriteria;
import com.roche.dss.qtt.utility.QueryStatusCodes;
import com.roche.dss.util.ApplicationUtils;

@Repository
public class ReportDAOImpl extends JDBCTemplateDAO implements ReportDAO {
    private static final Logger logger = LoggerFactory.getLogger(ReportDAOImpl.class);
    private String sql1;
    private String where1;
    private String sql2;
    private String where2;
    private String order;

	private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<ReportDTO> getReportData(ReportCriteria reportCriteria) {
        StringBuilder query = new StringBuilder("");
        StringBuilder query1 = new StringBuilder(sql1).append(" ").append(where1);
        StringBuilder query2 = new StringBuilder(sql2).append(" ").append(where2);
        List<Object> params = new ArrayList<Object>();
        if (reportCriteria.getDateFrom() != null) {
            query1.append(" and (select max(t.create_) from jbpm_taskinstance t where t.procinst_=q.process_instance and t.name_ = 'Unassigned' ) >= ?");
            params.add(reportCriteria.getDateFrom());
        }
        if (reportCriteria.getDateTo() != null) {
            query1.append(" and (select trunc(max(t.create_)) from jbpm_taskinstance t where t.procinst_=q.process_instance and t.name_ = 'Unassigned' ) <= ?");
            params.add(reportCriteria.getDateTo());
        }
        if (StringUtils.isNotEmpty(reportCriteria.getRoute())) {
            query1.append(" and Q.WORKFLOW_ROUTE_TYPE = ?");
            params.add(reportCriteria.getRoute());
        }
        if (reportCriteria.getDateFrom() != null) {
            query2.append(" and (select max(t.create_) from jbpm_taskinstance t where t.procinst_=q.process_instance and t.name_ = 'Unassigned' ) >= ? ");
            params.add(reportCriteria.getDateFrom());
        }
        if (reportCriteria.getDateTo() != null) {
            query2.append(" and (select trunc(max(t.create_)) from jbpm_taskinstance t where t.procinst_=q.process_instance and t.name_ = 'Unassigned' ) <= ?");
            params.add(reportCriteria.getDateTo());
        }
        if (StringUtils.isNotEmpty(reportCriteria.getRoute())) {
            query2.append(" and Q.WORKFLOW_ROUTE_TYPE = ?");
            params.add(reportCriteria.getRoute());
        }
        query.append(query1).append(" ").append(query2).append(" ").append(order);
        List<ReportDTO> reportList = jdbcTemplate.query(query.toString(), params.toArray(new Object[params.size()]), new ReportDTORowMapper());

        return reportList;
    }

    class ReportDTORowMapper implements RowMapper<ReportDTO> {

        @Override
        public ReportDTO mapRow(ResultSet resultSet, int i) throws SQLException {
            ReportDTO reportDTO = new ReportDTO();
            reportDTO.setQuerySeq(resultSet.getLong("QUERY_SEQ"));
            reportDTO.setQueryNumber(resultSet.getString("QUERY_NUMBER"));
            reportDTO.setDrugGenericName(resultSet.getString("INN_GENERIC_NAME"));
            reportDTO.setDrugRetrievalName(resultSet.getString("DRUG_RETRIEVAL_NAME"));
            reportDTO.setDrugIndication(resultSet.getString("INDICATION"));
            reportDTO.setOrganisationType(resultSet.getString("TYPE"));
            reportDTO.setRequesterCountry(resultSet.getString("RQC_DESCRIPTION"));
            reportDTO.setRequesterLastName(resultSet.getString("SURNAME"));
            reportDTO.setWorkflowRoute(resultSet.getString("WORKFLOW_ROUTE_TYPE"));
            reportDTO.setQueryType(resultSet.getString("QUERY_TYPE"));
            reportDTO.setReporterType(resultSet.getString("REPORTER_TYPE"));
            reportDTO.setReporterCountry(resultSet.getString("RPC_DESCRIPTION"));
            reportDTO.setCurrentStatus(resultSet.getString("STATUS"));
            reportDTO.setQueryLabel(resultSet.getString("QUERY_LABEL"));
            reportDTO.setQueryOwner(retrieveQueryOwner(reportDTO.getQuerySeq()));
            reportDTO.setEuQPPV(resultSet.getString("INFORM_EU_QPPV"));
            Date originalDueDate = resultSet.getDate("ORIGINAL_DUE_DATE");
            Date finalDueDate = resultSet.getDate("CURRENT_DUE_DATE");
            long processInstanceId = resultSet.getLong("PROCESS_INSTANCE");
            java.util.Date initiated = retrieveDateInitiate(processInstanceId);

            java.util.Date delivered = null;
            if(reportDTO.getCurrentStatus().equals(QueryStatusCodes.CLOSED.getValue())){
                delivered = retrieveDateDelivered(processInstanceId);
            }

            reportDTO.setInitiateDate(initiated);
            reportDTO.setTurnAround(ApplicationUtils.calculateDaysBetween(initiated, delivered));
            reportDTO.setDaysOriginalToFinal(ApplicationUtils.calculateDaysBetween(originalDueDate, finalDueDate));
            reportDTO.setDaysFinalToDelivery(ApplicationUtils.calculateDaysBetween(finalDueDate, delivered));

            return reportDTO;
        }

    }

    private java.util.Date retrieveDateInitiate(long processInstanceId) {
        try {
            Object obj = entityManager.createNativeQuery("select max(t.create_) from jbpm_taskinstance t where t.procinst_=:processInstance and t.name_ = 'Unassigned'").setParameter("processInstance", processInstanceId).getSingleResult();
            return (java.util.Date) obj;
        } catch (NoResultException ex) {
            return null;
        }
    }

    private java.util.Date retrieveDateDelivered(long processInstanceId) {
        try {
            Object obj = entityManager.createNativeQuery("select max(t.create_) from jbpm_taskinstance t where t.procinst_=:processInstance and t.name_ = 'Sent to Requester'").setParameter("processInstance", processInstanceId).getSingleResult();
            return (java.util.Date) obj;
        } catch (NoResultException ex) {
            return null;
        }
    }

    private String retrieveQueryOwner(long querySeq) {
        try {
            String author = null;
            List<String> authorList = entityManager.createNamedQuery("attachment.getOwner", String.class).setParameter("querySeq", querySeq).getResultList();
            if(CollectionUtils.isNotEmpty(authorList)){
                author = authorList.get(0);
            }
            return author;
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Value("${sql.report.select1}")
    public void setSql1(String sql1) {
        this.sql1 = sql1;
    }

    @Value("${sql.report.where1}")
    public void setWhere1(String where1) {
        this.where1 = where1;
    }

    @Value("${sql.report.select2}")
    public void setSql2(String sql2) {
        this.sql2 = sql2;
    }

    @Value("${sql.report.where2}")
    public void setWhere2(String where2) {
        this.where2 = where2;
    }

    @Value("${sql.report.order}")
    public void setOrder(String order) {
        this.order = order;
    }

}
