package com.roche.dss.qtt.query.web.action;

import java.io.File;

import javax.annotation.Resource;

import com.roche.dss.qtt.service.workflow.jbpm.JbpmService;

/**
 * @author zerkowsm
 *
 */
public class DeployProcessDefinitionAction extends CommonActionSupport {
	
	private static final long serialVersionUID = -8475796837198242806L;
	
	private static final Logger logger = LoggerFactory.getLogger(DeployProcessDefinitionAction.class);

	private File processDefinitionParFile;
	
    private JbpmService jbpmService;

    
    @Resource
    public void setJbpmService(JbpmService jbpmService) {
        this.jbpmService = jbpmService;
    }
	
	public String execute() {
		
		if(processDefinitionParFile == null){
			return INPUT;
		}
		try{
			jbpmService.deployProcessDefinitionParFile(processDefinitionParFile);
		} catch (JbpmException e) {
			addActionError(e.getMessage());
		}
		
		setProcessDefinitionParFile(null);
		return SUCCESS;
	}
	
	public String migrate() {		
		return SUCCESS;
	}

	public File getProcessDefinitionParFile() {
		return processDefinitionParFile;
	}

	public void setProcessDefinitionParFile(File processDefinitionParFile) {
		this.processDefinitionParFile = processDefinitionParFile;
	}
	
}