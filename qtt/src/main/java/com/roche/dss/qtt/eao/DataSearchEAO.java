package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.DataSearch;

public interface DataSearchEAO extends QueryRelatedEAO<DataSearch>{
}
