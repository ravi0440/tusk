package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.QueryDescription;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface QueryDescriptionEAO extends QueryRelatedEAO<QueryDescription> {
}
