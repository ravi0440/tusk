package com.roche.dss.qtt.query.web.action.oldqrf;

import java.util.ArrayList;
import java.util.List;

import com.roche.dss.qtt.model.Case;
import com.roche.dss.qtt.model.Drug;
import com.roche.dss.qtt.model.Pregnancy;
import com.roche.dss.qtt.model.PrgyExptype;
import com.roche.dss.qtt.query.comms.dto.CaseDTO;
import com.roche.dss.qtt.query.comms.dto.DrugDTO;
import com.roche.dss.qtt.query.comms.dto.QueryDTO;
import com.roche.dss.qtt.query.comms.dto.QueryDescDTO;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.model.QueryDescFormModel;
import com.roche.dss.qtt.service.QrfService;

import javax.annotation.Resource;

public class PopulateMedicalAction extends QRFAbstractAction {

    private QrfService qrfService;
    
    private Case caze = new Case();

    private Pregnancy preg = new Pregnancy();
    
    private List<Drug> drugs = new ArrayList<Drug>();
	
    private String comments;
    
    QueryDescFormModel queryForm = new QueryDescFormModel();

    @Resource
    public void setQrfService(QrfService qrfService) {
		this.qrfService = qrfService;
	}

    @Override
    public Object getModel() {
    	if (getSession().get(ActionConstants.QRF_EDIT_QUERY_TYPE) != null) {
            int queryId = ((Integer) getSession().get(ActionConstants.QRF_QUERY_ID)).intValue();
            QueryDTO query = qrfService.openQuery(queryId, null, true);
            queryForm.setNatureofcase(query.getQueryDesc().getNatureOfCase());
            queryForm.setOtherinfo(query.getQueryDesc().getOtherInfo());
            queryForm.setConmeds(query.getQueryDesc().getConMeds());
            queryForm.setDiagnosis(query.getQueryDesc().getDiagnosis());
            queryForm.setIndexcase(query.getQueryDesc().getIndexCase());
            queryForm.setInvestigation(query.getQueryDesc().getInvestigations());
            queryForm.setPmh(query.getQueryDesc().getPmh());
            queryForm.setSignsymp(query.getQueryDesc().getSignsSymp());
            comments = query.getCaseComment();
            drugs = new ArrayList<Drug>();
            for (Object o : query.getDrugs()) {
            	DrugDTO dto = (DrugDTO) o;
            	Drug d = new Drug();
            	d.setDose(dto.getDose());
            	d.setDrugRetrievalName(dto.getRetrName());
            	d.setFormulation(dto.getFormulation());
            	d.setInnGenericName(dto.getGenericName());
            	d.setRocheDrugFlag(StringUtils.isNotBlank(dto.getRetrName()) ? 'Y' : 'N');
            	d.setRoute(dto.getRoute());
            	drugs.add(d);
            }
            preg.setPrgyExptype(new PrgyExptype());
            preg.getPrgyExptype().setExposureTypeId(query.getPregnancy().getExposureType());
            preg.setTimeInPregnancy(query.getPregnancy().getTiming());
            preg.setOutcome(query.getPregnancy().getOutcome());
            preg.setComments(query.getPregnancy().getComments());
            if (query.getCases() != null && query.getCases().size() > 0) {
	            CaseDTO dto = (CaseDTO) query.getCases().get(0);
	            caze.setAerNumber(dto.getAerNumber());
	            caze.setExternalReferenceNumber(dto.getExtRefNumber());
	            caze.setLocalReferenceNumber(dto.getLocalRefNumber());
            }
            getSession().put(ActionConstants.FRM_QUERY_DESC, queryForm);
        }
        return queryForm;
    }

    public String execute() {
        return SUCCESS;
    }

	public List<PrgyExptype> getPrgyExptypes() {
        return qrfService.getPrgyExptypes();
    }

	public List<Drug> getDrugs() {
		return drugs;
	}

	public void setDrugs(List<Drug> drugs) {
		this.drugs = drugs;
	}

	public Pregnancy getPreg() {
		return preg;
	}

	public void setPreg(Pregnancy preg) {
		this.preg = preg;
	}

	public Case getCaze() {
		return caze;
	}

	public void setCaze(Case caze) {
		this.caze = caze;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
}
