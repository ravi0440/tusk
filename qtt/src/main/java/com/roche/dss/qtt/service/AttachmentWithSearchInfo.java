package com.roche.dss.qtt.service;

import com.roche.dss.qtt.model.Attachment;

public class AttachmentWithSearchInfo {
	protected Attachment attachment;
	
	protected String highlightedText;

	public AttachmentWithSearchInfo(Attachment attachment,
			String highlightedText) {
		this.attachment = attachment;
		this.highlightedText = highlightedText;
	}

	public Attachment getAttachment() {
		return attachment;
	}

	public String getHighlightedText() {
		return highlightedText;
	}
}
