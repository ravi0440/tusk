package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.query.web.model.InitiationEmailModel;
import com.roche.dss.qtt.utility.EmailSender;
import com.roche.dss.qtt.utility.config.Config;
import com.roche.dss.qtt.utility.pdf.QrfPdfInterface;
import java.io.ByteArrayOutputStream;

@ParentPackage("default")
@Action(value = "SendInitiationEmail", results = {
        @Result(name = "input", type="tiles", location = "/initiation.email"),
        @Result(name = "amdsuccess", type="chain", params = {"queryId", "-1"}, location = "QueryReview"),
        @Result(name = "rejsuccess", type = "redirectAction", location = "RejectQuery", params = {"id", "${model.queryId}"}),
        @Result(name = "amdsuccessmsg", type="tiles", location = "/amend.success"),
        @Result(name = "error", location = "/error.jsp")
})
public class SendInitiationEmailAction extends CommonActionSupport implements ModelDriven {

    private static final Logger logger = LoggerFactory.getLogger(SendInitiationEmailAction.class);
    @Autowired
    private QrfPdfInterface qrfPdf;
    InitiationEmailModel model = new InitiationEmailModel();
    @Autowired
    EmailSender emailsender;
    @Autowired
    protected Config config;

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String execute() throws Exception {

        // retrieve email ignore preference from request param's
        String isIgnoreEmail = model.getIgnoreEmail();

        //if ignoreEmail is not 'yes' then send email 
        if (!isIgnoreEmail.equals("yes")) {
                sendEmail();
        }
        if (model.getEmailType().equals("amd")) {
            if (model.getMsg() != null) {
                return "amdsuccessmsg";
            } else {
                return "amdsuccess";
            }
        } else if (model.getEmailType().equals("rej")) {
            return "rejsuccess";
        } else {
            return ERROR;
        }

    }

    @Override
	public void validate() {


        if (!"".equals(model.getCc())) {
            if (!validateEmail(model.getCc())) {
                addActionError("emailForm.cc.displayname");
            }
        }

        if (!validateEmail(model.getTo())) {
            addActionError("emailForm.to.displayname");
        }

        if (model.getSubject()==null || "".equals(model.getSubject())) {
           addActionError("emailForm.subject.displayname");
        }

        // check if the file is empty
        if (model.getFileOne() != null) {
            if (model.getFileOneFileName().length() > 100) {
                addActionError("emailForm.maxlength.fileOne");
            }
            // validate file length
            if ((model.getFileOneFileName().length() > 0) && (model.getFileOne().getTotalSpace() < 1)) {
                addActionError("emailForm.valid.fileOne");
            }
        }

        if (model.getFileTwo() != null) {
            if (model.getFileTwoFileName().length() > 100) {
                addActionError("emailForm.maxlength.fileTwo");
            }

            if ((model.getFileTwoFileName().length() > 0) && (model.getFileTwo().getTotalSpace() < 1)) {
                addActionError("emailForm.valid.fileTwo");
            }
        }

        if (model.getFileThree() != null) {
            // validate file length
            if (model.getFileThreeFileName().length() > 100) {
                addActionError("emailForm.maxlength.fileThree");
            }
            if ((model.getFileThreeFileName().length() > 0) && (model.getFileThree().getTotalSpace() < 1)) {
                addActionError("emailForm.valid.fileThree");
            }
        }
        super.validate();
    }

    public boolean validateEmail(String email) {
        return (email != null && email.matches(EmailValidator.emailAddressPattern));
    }

    public void sendEmail() {

        String typePhrase = "";

        if (model.getEmailType().equals("amd")) {
            typePhrase = "Provisional";
        } else if (model.getEmailType().equals("rej")) {
            typePhrase = "Rejected";
        }
        ByteArrayOutputStream out = qrfPdf.qrfBuilder(Long.valueOf(model.getQueryId()).intValue(), typePhrase);
        String attachName = "QRF-" + model.getQueryId() + ".pdf";


        try {
            emailsender.sendMessage(config.getInititateDsclEmail(), model.getTo(), model.getCc(), model.getSubject(), model.getBody(), attachName, out,
                    new EmailSender.FormFile(model.getFileTwo(), model.getFileTwoFileName(), model.getFileTwoContentType()), new EmailSender.FormFile(model.getFileThree(), model.getFileThreeFileName(), model.getFileThreeContentType()));
            logger.info("SendInitiationEmailAction: Sending mailto: for query:" + model.getQueryId() + " email:" + model.getTo());
        } catch (Exception e) {
            logger.error("Error SendInitiationEmailAction:sending email" + e);
        }
    }

    @Override
    public Object getModel() {
        return model;
    }
}
