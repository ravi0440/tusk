
package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.QueryDescriptionEAO;
import com.roche.dss.qtt.model.QueryDescription;

@Repository("queryDescriptionEAO")
public class QueryDescriptionEAOImpl extends AbstractQueryRelatedEAO<QueryDescription> implements QueryDescriptionEAO {

}