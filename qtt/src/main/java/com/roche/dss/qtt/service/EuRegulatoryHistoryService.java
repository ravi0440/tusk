package com.roche.dss.qtt.service;

import com.roche.dss.qtt.model.EuRegulatoryHistory;

public interface EuRegulatoryHistoryService {
     void saveEuRegulatoryHistory(EuRegulatoryHistory euRegulatory);
}
