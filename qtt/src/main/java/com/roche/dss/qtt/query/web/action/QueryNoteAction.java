package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.service.QueryNoteService;
import com.roche.dss.qtt.service.QueryService;
import javax.annotation.Resource;

/**
 * This action contains query note CRUD actions
 *
 * User: pruchnil
 */
public class QueryNoteAction extends CommonActionSupport{
    private static final Logger logger = LoggerFactory.getLogger(QueryNoteAction.class);
    private long id;
    private long queryId;
    private Query query;
    private String content;
    private QueryService queryService;
    private QueryNoteService noteService;
    private boolean fromTask;


    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Resource
    public void setNoteService(QueryNoteService noteService) {
        this.noteService = noteService;
    }

    @SkipValidation
    @Override
    public String execute() throws Exception {
        query = queryService.find(queryId);
        return SUCCESS;
    }

    public String save() throws Exception {
        noteService.saveQueryNote(id, queryId, content, isAffiliate(), getLoggedUserName());
        return fromTask ? OTHER : SUCCESS;
    }

    @SkipValidation
    public String manage() throws Exception {
        query = queryService.find(queryId);
        content = noteService.findContent(id, isAffiliate());
        return SUCCESS;
    }

    @SkipValidation
    public String delete() throws Exception {
        noteService.removeQueryNote(id, isAffiliate());
        return fromTask ? OTHER : SUCCESS;
    }

    @Override
    public void onActionErrors() {
         query = queryService.find(queryId);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @RequiredStringValidator(type = ValidatorType.FIELD, key = "content.required")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Query getQuery() {
        return query;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public boolean isFromTask() {
        return fromTask;
    }

    public void setFromTask(boolean fromTask) {
        this.fromTask = fromTask;
    }
}
