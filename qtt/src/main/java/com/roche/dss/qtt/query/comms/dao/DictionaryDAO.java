package com.roche.dss.qtt.query.comms.dao;

import com.roche.dss.qtt.model.AttachmentCategory;
import com.roche.dss.qtt.model.LdocType;
import com.roche.dss.qtt.model.QttCountry;
import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.model.ReporterType;
import com.roche.dss.qtt.model.WorkflowRouteType;

import java.util.List;

/**
 * This DAO retrieves all neeeded dictionaries.
 * It's equivalent to DatabaseLookupsHelper in former version of QTT.
 *
 * User: pruchnil
 */
public interface DictionaryDAO {
    List<AttachmentCategory> getAllAttachmentCategories();
    
    public List<String> getUsedRetrievalNames();
    
    public List<String> getUsedGenericNames();
    
    public List<String> getUsedIndications();
    
    public List<QueryType> getQueryTypes();
    
    public List<String> getUsedResponseAuthors();
    
    public List<LdocType> getUsedLabellingDocs();
    
    public List<WorkflowRouteType> getProcessTypes();
    
    public List<String> getDMGUserInvolvedNames();

    public List<String> getPSUserInvolvedNames();

    public List<String> getDSCLUserInvolvedNames();

    public List<String> getUsedRequestorLastNames();

    public List<QttCountry> getUsedRequestorCountries();

    public List<ReporterType> getUsedReporterTypes();

    public List<QttCountry> getUsedSourceCountries();

	List<String> getAllTaskNames();

	List<QueryType> getAllQueryTypes();

	List<String> getAllQueryTypeNames();
}
