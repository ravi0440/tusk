/*
====================================================================
  $Header$
  @author $Author: hudowskr $
  @version $Revision: 1281 $ $Date: 2008-10-10 14:34:07 +0200 (Pt, 10 paź 2008) $

====================================================================
*/
package com.roche.dss.qtt.query.web.qrf.model;

import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.Audit;
import com.roche.dss.qtt.model.Case;
import com.roche.dss.qtt.model.ClinicalTrial;
import com.roche.dss.qtt.model.DataSearch;
import com.roche.dss.qtt.model.Drug;
import com.roche.dss.qtt.model.Manufacturing;
import com.roche.dss.qtt.model.PerformanceMetric;
import com.roche.dss.qtt.query.comms.dto.AttachmentDTO;

import java.util.List;

public class QueryReviewDisplayModel extends AbstractDisplayBean {

    private List<Attachment> finalAttachments;
    private String firstname = "";
	private String lastname = "";
	private String telephone = "";
	private String fax = "";
	private String email = "";
	private String reqCountry = "";
	private String orgName = "";
	private String orgType = "";

	private String sourceCountry = "";
	private String reporterType = "";

	private String repository = "";
	private String psur = "";
	private String cds = "";
	private String issue = "";
	private String literature = "";
	private String others = "";
	private String othersComment = "";
	private String comments = "";

	private String allDrugs = "";
	private String retrDrug = "";
	private String genericDrug = "";
	private String route = "";
	private String formulation = "";
	private String indication = "";
	private String dose = "";
	private String othersDrug = "";
	private String dateRequested = "";
	private String urgency = "";

	private String cumulative = "";
	private String dateFrom = "";
	private String dateTo = "";
	private String interval = "";
	private String queryType = "";
	private String followUp = "";
	private int mId;

	private String natureOfCase = "";
	private String indexCase = "";
	private String signsSymp = "";
	private String pmh = "";
	private String conMeds = "";
	private String investigations = "";
	private String diagnosis = "";
	private String otherInfo = "";

	private String aeTerm = "";

	private String aerNumber = "";
	private String localRefNumber = "";
	private String extRefNumber = "";
	private String additionalComments = "";

	private List attachments;

    private List<AttachmentDTO> attachmentDTOList;
    private List<AttachmentDTO> attachmentDTOListWithOldFinal;


	private List<Case> cases;

	private List<Drug> drugs;

	private String batch = "";
	private String outputFormat = "";
	private String outputOther = "";
	private String exposureType = "";
	private String timing = "";
	private String outcome = "";

	private String protocolNumber = "";
	private String crtnNumber = "";
	private String patientNumber = "";
	private String caseSelection = "";
	private String outputFormatDesc = "";

	private String indNumber = "";
	private String queryTypeCode = "";
	private String queryNumber = "";

    private String previewMode = "";
    private boolean haLegalNoDoc;
    private boolean differentRequester;
    private String requesterQueryLabel = "";
    private String followupNumber = "";

    private boolean deleteQueryAllowed = false;
    private boolean rejectQueryAllowed = false;
    private String pageTitle;
    
    private String specialRequest = "";
    
    private Audit audit;
    private ClinicalTrial clinical;
    private PerformanceMetric performanceMetrics;
    private Manufacturing manufacturing;
    private DataSearch dataSearch;

    public boolean isQueryResponse() {
        return isQueryResponse;
    }

    public void setQueryResponse(boolean queryResponse) {
        isQueryResponse = queryResponse;
    }

    private boolean isQueryResponse;


	public QueryReviewDisplayModel() {
		//default constructor
	}





    /**
     * @return
     */
    public List getAttachments() {
        return attachments;
    }



    /**
     * @return
     */
    public String getComments() {
        return  comments ;
    }





    /**
     * @return
     */
    public String getDose() {
        return dose;
    }

    /**
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return
     */
    public String getFax() {
        return fax;
    }

    /**
     * @return
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @return
     */
    public String getFollowUp() {
        return followUp;
    }

    /**
     * @return
     */
    public String getFormulation() {
        return formulation;
    }

    /**
     * @return
     */
    public String getGenericDrug() {
        return genericDrug;
    }

    /**
     * @return
     */
    public int getMid() {
        return mId;
    }

    /**
     * @return
     */
    public String getIndication() {
        return indication;
    }

    /**
     * @return
     */
    public String getInterval() {
        return interval;
    }


    /**
     * @return
     */
    public String getLastname() {
        return lastname;
    }




    /**
     * @return
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * @return
     */
    public String getOrgType() {
        return orgType;
    }


    /**
     * @return
     */
    public String getOthersComment() {
        return  othersComment ;
    }

    /**
     * @return
     */
    public String getOthersDrug() {
        return othersDrug ;
    }

    /**
     * @return
     */
    public String getRetrDrug() {
        return retrDrug;
    }


    /**
     * @return
     */
    public String getQueryType() {
        return queryType;
    }

    /**
     * @return
     */
    public String getReporterType() {
        return reporterType;
    }


    /**
     * @return
     */
    public String getReqCountry() {
        return reqCountry;
    }

    /**
     * @return
     */
    public String getRoute() {
        return route;
    }

    /**
     * @return
     */
    public String getSourceCountry() {
        return sourceCountry;
    }

    /**
     * @return
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @return
     */
    public String getUrgency() {
        return  urgency ;
    }



    /**
     * @param b
     */
    public void setAllDrugs(String b) {
        allDrugs = b;
    }

    /**
     * @param list
     */
    public void setAttachments(List list) {
        attachments = list;
    }

    /**
     * @param b
     */
    public void setCds(String b) {
        cds = b;
    }

    /**
     * @param string
     */
    public void setComments(String string) {
        comments = string;
    }

    /**
     * @param b
     */
    public void setCumulative(String b) {
        cumulative = b;
    }



    /**
     * @param string
     */
    public void setDose(String string) {
        dose = string;
    }

    /**
     * @param string
     */
    public void setEmail(String string) {
        email = string;
    }

    /**
     * @param string
     */
    public void setFax(String string) {
        fax = string;
    }

    /**
     * @param string
     */
    public void setFirstname(String string) {
        firstname = string;
    }

    /**
     * @param string
     */
    public void setFollowUp(String string) {
        followUp = string;
    }

    /**
     * @param string
     */
    public void setFormulation(String string) {
        formulation = string;
    }

    /**
     * @param string
     */
    public void setGenericDrug(String string) {
        genericDrug = string;
    }

    /**
     * @param i
     */
    public void setMid(int i) {
        mId = i;
    }

    /**
     * @param string
     */
    public void setIndication(String string) {
        indication = string;
    }

    /**
     * @param string
     */
    public void setInterval(String string) {
        interval = string;
    }

    /**
     * @param b
     */
    public void setIssue(String b) {
        issue = b;
    }

    /**
     * @param string
     */
    public void setLastname(String string) {
        lastname = string;
    }

    /**
     * @param b
     */
    public void setLiterature(String b) {
        literature = b;
    }



    /**
     * @param string
     */
    public void setOrgName(String string) {
        orgName = string;
    }

    /**
     * @param string
     */
    public void setOrgType(String string) {
        orgType = string;
    }

    /**
     * @param b
     */
    public void setOthers(String b) {
        others = b;
    }

    /**
     * @param string
     */
    public void setOthersComment(String string) {
        othersComment = string;
    }

    /**
     * @param string
     */
    public void setOthersDrug(String string) {
        othersDrug = string;
    }

    /**
     * @param string
     */
    public void setRetrDrug(String string) {
        retrDrug = string;
    }

    /**
     * @param b
     */
    public void setPsur(String b) {
        psur = b;
    }


    public void setQueryType(String s) {
        queryType = s;
    }

    /**
     * @param string
     */
    public void setReporterType(String string) {
        reporterType = string;
    }

    /**
     * @param b
     */
    public void setRepository(String b) {
        repository = b;
    }

    /**
     * @param string
     */
    public void setReqCountry(String string) {
        reqCountry = string;
    }

    /**
     * @param string
     */
    public void setRoute(String string) {
        route = string;
    }

    /**
     * @param string
     */
    public void setSourceCountry(String string) {
        sourceCountry = string;
    }

    /**
     * @param string
     */
    public void setTelephone(String string) {
        telephone = string;
    }

    /**
     * @param string
     */
    public void setUrgency(String string) {
        urgency = string;
    }



    /**
     * @return
     */
    public String getDateFrom() {
        return dateFrom;
    }

    /**
     * @return
     */
    public String getDateRequested() {
        return dateRequested;
    }

    /**
     * @return
     */
    public String getDateTo() {
        return dateTo;
    }

    /**
     * @param string
     */
    public void setDateFrom(String string) {
        dateFrom = string;
    }

    /**
     * @param string
     */
    public void setDateRequested(String string) {
        dateRequested = string;
    }

    /**
     * @param string
     */
    public void setDateTo(String string) {
        dateTo = string;
    }

    /**
     * @return
     */
    public String getAllDrugs() {
        return allDrugs;
    }

    /**
     * @return
     */
    public String getCds() {
        return  cds ;
    }

    /**
     * @return
     */
    public String getCumulative() {
        return cumulative;
    }

    /**
     * @return
     */
    public String getIssue() {
        return issue;
    }

    /**
     * @return
     */
    public String getLiterature() {
        return  literature ;
    }

    /**
     * @return
     */
    public String getOthers() {
        return  others ;
    }

    /**
     * @return
     */
    public String getPsur() {
        return  psur ;
    }

    /**
     * @return
     */
    public String getRepository() {
        return  repository ;
    }



    /**
     * @return
     */
    public String getConMeds() {
        return  conMeds ;
    }

    /**
     * @return
     */
    public String getDiagnosis() {
        return  diagnosis ;
    }

    /**
     * @return
     */
    public String getIndexCase() {
        return  indexCase ;
    }

    /**
     * @return
     */
    public String getInvestigations() {
        return  investigations ;
    }

    /**
     * @return
     */
    public String getNatureOfCase() {
        return  natureOfCase ;
    }

    /**
     * @return
     */
    public String getOtherInfo() {
        return  otherInfo ;
    }

    /**
     * @return
     */
    public String getPmh() {
        return  pmh ;
    }

    /**
     * @return
     */
    public String getSignsSymp() {
        return  signsSymp ;
    }

    /**
     * @param string
     */
    public void setConMeds(String string) {
        conMeds = string;
    }

    /**
     * @param string
     */
    public void setDiagnosis(String string) {
        diagnosis = string;
    }

    /**
     * @param string
     */
    public void setIndexCase(String string) {
        indexCase = string;
    }

    /**
     * @param string
     */
    public void setInvestigations(String string) {
        investigations = string;
    }

    /**
     * @param string
     */
    public void setNatureOfCase(String string) {
        natureOfCase = string;
    }

    /**
     * @param string
     */
    public void setOtherInfo(String string) {
        otherInfo = string;
    }

    /**
     * @param string
     */
    public void setPmh(String string) {
        pmh = string;
    }

    /**
     * @param string
     */
    public void setSignsSymp(String string) {
        signsSymp = string;
    }

    /**
     * @return
     */
    public String getAeTerm() {
        return  aeTerm ;
    }

    /**
     * @return
     */
    public String getExtRefNumber() {
        return  extRefNumber ;
    }

    /**
     * @return
     */
    public String getLocalRefNumber() {
        return  localRefNumber ;
    }

    /**
     * @return
     */
    public String getAerNumber() {
        return aerNumber;
    }

    /**
     * @param string
     */
    public void setAeTerm(String string) {
        aeTerm = string;
    }

    /**
     * @param string
     */
    public void setExtRefNumber(String string) {
        extRefNumber = string;
    }

    /**
     * @param string
     */
    public void setLocalRefNumber(String string) {
        localRefNumber = string;
    }

    /**
     * @param string
     */
    public void setAerNumber(String string) {
        aerNumber = string;
    }

    /**
     * @param string
     */
    public void setAdditionalComments(String string) {
        additionalComments = string;
    }

    /**
     * @return
     */
    public String getAdditionalComments() {
        return  additionalComments ;
    }

    /**
     * @return
     */
    public List<Case> getCases() {
        return cases;
    }

    /**
     * @param list
     */
    public void setCases(List<Case> list) {
        cases = list;
    }

    /**
     * @return
     */
    public List<Drug> getDrugs() {
        return drugs;
    }

    /**
     * @param list
     */
    public void setDrugs(List<Drug> list) {
        drugs = list;
    }

    /**
     * @return
     */
    public String getBatch() {
        return batch;
    }

    /**
     * @param string
     */
    public void setBatch(String string) {
        batch = string;
    }

    /**
     * @return
     */
    public String getOutputFormat() {
        return outputFormat;
    }

    /**
     * @return
     */
    public String getOutputOther() {
        return  outputOther ;
    }

    /**
     * @param string
     */
    public void setOutputFormat(String string) {
        outputFormat = string;
    }

    /**
     * @param string
     */
    public void setOutputOther(String string) {
        outputOther = string;
    }

    /**
     * @return
     */
    public String getExposureType() {
        return exposureType;
    }

    /**
     * @return
     */
    public String getOutcome() {
        return  outcome ;
    }

    /**
     * @return
     */
    public String getTiming() {
        return  timing ;
    }

    /**
     * @param string
     */
    public void setExposureType(String string) {
        exposureType = string;
    }

    /**
     * @param string
     */
    public void setOutcome(String string) {
        outcome = string;
    }

    /**
     * @param string
     */
    public void setTiming(String string) {
        timing = string;
    }

    /**
     * @return
     */
    public String getCaseSelection() {
        return  caseSelection ;
    }

    /**
     * @return
     */
    public String getCrtnNumber() {
        return crtnNumber;
    }

    /**
     * @return
     */
    public String getOutputFormatDesc() {
        return  outputFormatDesc ;
    }

    /**
     * @return
     */
    public String getPatientNumber() {
        return patientNumber;
    }

    /**
     * @return
     */
    public String getProtocolNumber() {
        return protocolNumber;
    }

    /**
     * @return Returns the previewMode.
     */
    public String getPreviewMode() {
        return previewMode;
    }

    /**
     * @param string
     */
    public void setCaseSelection(String string) {
        caseSelection = string;
    }

    /**
     * @param string
     */
    public void setCrtnNumber(String string) {
        crtnNumber = string;
    }

    /**
     * @param string
     */
    public void setOutputFormatDesc(String string) {
        outputFormatDesc = string;
    }

    /**
     * @param string
     */
    public void setPatientNumber(String string) {
        patientNumber = string;
    }

    /**
     * @param string
     */
    public void setProtocolNumber(String string) {
        protocolNumber = string;
    }

    /**
     * @return
     */
    public String getIndNumber() {
        return indNumber;
    }

    /**
     * @param string
     */
    public void setIndNumber(String string) {
        indNumber = string;
    }

    /**
     * @return
     */
    public String getQueryTypeCode() {
        return queryTypeCode;
    }

    /**
     * @param string
     */
    public void setQueryTypeCode(String string) {
        queryTypeCode = string;
    }



    /**
     * @return
     */
    public String getQueryNumber() {
        return queryNumber;
    }

    /**
     * @param string
     */
    public void setQueryNumber(String string) {
        queryNumber = string;
    }



    public void setPreviewMode(String buttonType) {
        this.previewMode = buttonType;
    }





	public boolean isHaLegalNoDoc() {
		return haLegalNoDoc;
	}





	public void setHaLegalNoDoc(boolean haLegalNoDoc) {
		this.haLegalNoDoc = haLegalNoDoc;
	}





	public boolean isDifferentRequester() {
		return differentRequester;
	}





	public void setDifferentRequester(boolean differentRequester) {
		this.differentRequester = differentRequester;
	}





    /**
     * @return Returns the requesterQueryLabel.
     */
    public String getRequesterQueryLabel() {
        return requesterQueryLabel;
    }





    /**
     * @param requesterQueryLabel The requesterQueryLabel to set.
     */
    public void setRequesterQueryLabel(String requesterQueryLabel) {
        this.requesterQueryLabel = requesterQueryLabel;
    }





    /**
     * @return the followupNumber
     */
    public String getFollowupNumber() {
        return followupNumber;
    }





    /**
     * @param followupNumber the followupNumber to set
     */
    public void setFollowupNumber(String followupNumber) {
        this.followupNumber = followupNumber;
    }





	public boolean isDeleteQueryAllowed() {
		return deleteQueryAllowed;
	}





	public void setDeleteQueryAllowed(boolean isDeleteQueryAllowed) {
		this.deleteQueryAllowed = isDeleteQueryAllowed;
	}

    public boolean isRejectQueryAllowed() {
        return rejectQueryAllowed;
    }

    public void setRejectQueryAllowed(boolean rejectQueryAllowed) {
        this.rejectQueryAllowed = rejectQueryAllowed;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {

        this.pageTitle = pageTitle;
    }

    public List<Attachment> getFinalAttachments() {
        return finalAttachments;
    }

    public void setFinalAttachments(List<Attachment> finalAttachments) {
        this.finalAttachments = finalAttachments;
    }

	public PerformanceMetric getPerformanceMetrics() {
		return performanceMetrics;
	}

	public void setPerformanceMetrics(PerformanceMetric performanceMetrics) {
		this.performanceMetrics = performanceMetrics;
	}

	public ClinicalTrial getClinical() {
		return clinical;
	}

	public void setClinical(ClinicalTrial clinical) {
		this.clinical = clinical;
	}

	public Audit getAudit() {
		return audit;
	}

	public void setAudit(Audit audit) {
		this.audit = audit;
	}

	public String getSpecialRequest() {
		return specialRequest;
	}

	public void setSpecialRequest(String specialRequest) {
		this.specialRequest = specialRequest;
	}

	public Manufacturing getManufacturing() {
		return manufacturing;
	}

	public void setManufacturing(Manufacturing manufacturing) {
		this.manufacturing = manufacturing;
	}

	public DataSearch getDataSearch() {
		return dataSearch;
	}

	public void setDataSearch(DataSearch dataSearch) {
		this.dataSearch = dataSearch;
	}

    public List<AttachmentDTO> getAttachmentDTOList() {
        return attachmentDTOList;
    }

    public void setAttachmentDTOList(List<AttachmentDTO> attachmentDTOList) {
        this.attachmentDTOList = attachmentDTOList;
    }

    public List<AttachmentDTO> getAttachmentDTOListWithOldFinal() {
        return attachmentDTOListWithOldFinal;
    }

    public void setAttachmentDTOListWithOldFinal(List<AttachmentDTO> attachmentDTOListWithOldFinal) {
        this.attachmentDTOListWithOldFinal = attachmentDTOListWithOldFinal;
    }
}
