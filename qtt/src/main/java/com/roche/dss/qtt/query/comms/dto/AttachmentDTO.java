/* 
 ====================================================================
 $Header$
 @author $Author: komisarp $
 @version $Revision: 2453 $ $Date: 2012-06-27 16:07:46 +0200 (Śr, 27 cze 2012) $

 ====================================================================
 */
package com.roche.dss.qtt.query.comms.dto;

import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.AttachmentCategory;
import com.roche.dss.qtt.query.web.qrf.Constants;
import com.roche.dss.qtt.utility.date.DateHelper;
import java.util.Date;

public class AttachmentDTO {
	
	
    private int id;

    private int categoryId;

    private Date dateTime;

    private String title;

    private int version;

    private boolean isMedOpinion;

    private String fileName;

    private String author;

    private boolean isAffiliateVisible;

    private String category;

    private boolean activationFlag = true;

    private String followupNumberString;

    private String followupNumber;

    /**
     * Empty constructor
     */
    public AttachmentDTO() {
    }

    /**
     * COnstructor populating all variables
     *
     * @param categoryId
     * @param dateTime
     * @param title
     * @param version
     * @param isMedOpinion
     * @param fileName
     * @param author
     * @param isAffiliateVisible
     */
    public AttachmentDTO(int id, int categoryId, Date dateTime, String title, int version, boolean isMedOpinion,
                         String fileName, String author, boolean isAffiliateVisible, String category, String followupNumber) {
        this.categoryId = categoryId;
        this.dateTime = dateTime;
        this.title = title;
        this.version = version;
        this.isMedOpinion = isMedOpinion;
        this.fileName = fileName;
        this.author = author;
        this.isAffiliateVisible = isAffiliateVisible;
        this.id = id;
        this.category = category;
        this.followupNumber = followupNumber;
    }

    /**
     * COnstructor populating all variables
     *
     * @param categoryId
     * @param dateTime
     * @param title
     * @param version
     * @param isMedOpinion
     * @param fileName
     * @param author
     * @param isAffiliateVisible
     */
    public AttachmentDTO(int id, int categoryId, Date dateTime, String title, int version, boolean isMedOpinion,
                         String fileName, String author, boolean isAffiliateVisible, String category, boolean activationFlag,
                         String followupNumberString, String followupNumber) {
        this.categoryId = categoryId;
        this.dateTime = dateTime;
        this.title = title;
        this.version = version;
        this.isMedOpinion = isMedOpinion;
        this.fileName = fileName;
        this.author = author;
        this.isAffiliateVisible = isAffiliateVisible;
        this.id = id;
        this.category = category;
        this.activationFlag = activationFlag;
        this.followupNumberString = followupNumberString;
        this.followupNumber = followupNumber;
    }

    /**
     * @return
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @return
     */
    public int getCategoryId() {
        return categoryId;
    }

    /**
     * @return
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @return
     */
    public boolean isAffiliateVisible() {
        return isAffiliateVisible;
    }

    /**
     * @return
     */
    public boolean isMedOpinion() {
        return isMedOpinion;
    }

    /**
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return
     */
    public int getVersion() {
        return version;
    }

    /**
     * @param string
     */
    public void setAuthor(String string) {
        author = string;
    }

    /**
     * @param i
     */
    public void setCategoryId(int i) {
        categoryId = i;
    }

    /**
     * @param string
     */
    public void setFileName(String string) {
        fileName = string;
    }

    /**
     * @param b
     */
    public void setAffiliateVisible(boolean b) {
        isAffiliateVisible = b;
    }

    /**
     * @param b
     */
    public void setMedOpinion(boolean b) {
        isMedOpinion = b;
    }

    /**
     * @param string
     */
    public void setTitle(String string) {
        title = string;
    }

    /**
     * @param i
     */
    public void setVersion(int i) {
        version = i;
    }

    /**
     * @return
     */
    public Date getDateTime() {
        return dateTime;
    }

    /**
     * @param date
     */
    public void setDateTime(Date date) {
        dateTime = date;
    }

    /**
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * @return
     */
    public void setId(int i) {
        id = i;
    }

    /**
     * @return
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param string
     */
    public void setCategory(String string) {
        category = string;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (activationFlag ? 1231 : 1237);
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((category == null) ? 0 : category.hashCode());
		result = prime * result + categoryId;
		result = prime * result
				+ ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result
				+ ((followupNumber == null) ? 0 : followupNumber.hashCode());
		result = prime
				* result
				+ ((followupNumberString == null) ? 0 : followupNumberString
						.hashCode());
		result = prime * result + id;
		result = prime * result + (isAffiliateVisible ? 1231 : 1237);
		result = prime * result + (isMedOpinion ? 1231 : 1237);
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + version;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		 if (!(obj instanceof AttachmentDTO)) {
	            return false;
	        }
		AttachmentDTO other = (AttachmentDTO) obj;
		if (activationFlag != other.activationFlag)
			return false;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (categoryId != other.categoryId)
			return false;
		if (!DateHelper.getInstance().compareDate(this.dateTime, other.getDateTime())) {
			return false;
		}
		if (fileName == null) {
			if (other.fileName != null)
				return false;
		} else if (!fileName.equals(other.fileName))
			return false;
		if (followupNumber == null) {
			if (other.followupNumber != null)
				return false;
		} else if (!followupNumber.equals(other.followupNumber))
			return false;
		if (followupNumberString == null) {
			if (other.followupNumberString != null)
				return false;
		} else if (!followupNumberString.equals(other.followupNumberString))
			return false;
		if (id != other.id)
			return false;
		if (isAffiliateVisible != other.isAffiliateVisible)
			return false;
		if (isMedOpinion != other.isMedOpinion)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (version != other.version)
			return false;
		return true;
	}

	/**
     * @return Returns the activationFlag.
     */
    public boolean isActivationFlag() {
        return activationFlag;
    }

    /**
     * @param activationFlag The activationFlag to set.
     */
    public void setActivationFlag(boolean activationFlag) {
        this.activationFlag = activationFlag;
    }

    /**
     * @return Returns the followupNumberString.
     */
    public String getFollowupNumberString() {
        return followupNumberString;
    }

    /**
     * @param followupNumberString The followupNumberString to set.
     */
    public void setFollowupNumberString(String followupNumberString) {
        this.followupNumberString = followupNumberString;
    }

    @Override
	public String toString() {
        StringBuffer buf = new StringBuffer("AttachmentDTO ");
        buf.append(id);
        buf.append(" [");
        buf.append("CategoryId: ");
        buf.append(this.categoryId);
        buf.append(", isAffiliateVisible: ");
        buf.append(this.isAffiliateVisible);
        buf.append(", isMedOpinion: ");
        buf.append(this.isMedOpinion);
        buf.append(", version: ");
        buf.append(this.version);
        buf.append(", author: ");
        buf.append(this.author);
        buf.append(", dateTime: ");
        buf.append(this.dateTime);
        buf.append(", filename: ");
        buf.append(this.fileName);
        buf.append(", title: ");
        buf.append(this.title);
        buf.append(", category: ");
        buf.append(this.category);
        buf.append(", activationFlag: ");
        buf.append(this.activationFlag);
        buf.append(", followupNumber: ");
        buf.append(this.followupNumber);

        buf.append("]");

        return buf.toString();
    }

    /**
     * @return the followupNumber
     */
    public String getFollowupNumber() {
        return followupNumber;
    }

    /**
     * @param followupNumber the followupNumber to set
     */
    public void setFollowupNumber(String followupNumber) {
        this.followupNumber = followupNumber;
    }

    public Attachment createAttachment(int queryId) {
        Attachment attachment = new Attachment();
        attachment.setCreateTs(new DateTime());
        attachment.setFilename(fileName);
        attachment.setActivationFlag(activationFlag ? 'Y' : 'N');
        attachment.setMedicalOpinionFlag('N');
        attachment.setAuthor(author);
        attachment.setVersion(Short.valueOf("1"));
        attachment.setTitle(title);
        attachment.setAttachmentCategory(new AttachmentCategory(String.valueOf(Constants.QRF_ATTACHMENT_CATEGORY)));
        attachment.setQuerySeq(queryId);
        attachment.setFollowupNumber(getFollowupNumber());
        return attachment;
    }
}
