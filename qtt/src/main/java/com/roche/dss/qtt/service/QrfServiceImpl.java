package com.roche.dss.qtt.service;


import com.roche.dss.qtt.eao.*;
import com.roche.dss.qtt.model.*;
import com.roche.dss.qtt.query.comms.dao.*;
import com.roche.dss.qtt.query.comms.dto.*;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.Constants;
import com.roche.dss.qtt.query.web.qrf.model.QueryDescFormModel;
import com.roche.dss.qtt.service.workflow.jbpm.JbpmService;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */

@Service("qrfService")
public class QrfServiceImpl implements QrfService {
    private static final Logger logger = LoggerFactory.getLogger(QrfServiceImpl.class);

    private CountryDAO countryDAO;
    private ReporterTypeDAO reporterTypeDAO;
    private OrganisationTypeDAO organisationTypeDAO;
    private DrugDAO drugDAO;

    private QueryTypeDAO queryTypeDAO;
    private QueryDAO queryDAO;
    @Autowired
    private CaseOformatEAO caseOformatEAO;
    private DataSearchEAO dataSearchEAO;
    private QueryStatusCodeEAO queryStatusCodeEAO;
    private QueryService queryService;
    private PregnancyEAO pregnancyEAO;
    private PrgyExptypeEAO prgyExptypeEAO;
  	protected EntityManager entityManager;
    @Autowired
    private CtrlOformatEAO ctrlOformatEAO;
    @Autowired
    private AttachmentEAO attachmentEAO;
    @Autowired
    private QueryEAO queryEAO;
    @Autowired
    private JbpmService jbpmService;
    @Autowired
    private FollowupQueryService followupQueryService;

    @PersistenceContext
	@SuppressWarnings("unchecked")
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

    @Resource
    public void setQueryStatusCodeEAO(QueryStatusCodeEAO queryStatusCodeEAO) {
        this.queryStatusCodeEAO = queryStatusCodeEAO;
    }

    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Resource
    public void setPregnancyEAO(PregnancyEAO pregnancyEAO) {
        this.pregnancyEAO = pregnancyEAO;
    }

    @Resource
    public void setDataSearchEAO(DataSearchEAO dataSearchEAO) {
        this.dataSearchEAO = dataSearchEAO;
    }

    @Resource
    public void setPrgyExptypeEAO(PrgyExptypeEAO prgyExptypeEAO) {
		this.prgyExptypeEAO = prgyExptypeEAO;
	}

	@Resource
    public void setReporterTypeDAO(QueryTypeDAO queryTypeDAO) {
        this.queryTypeDAO = queryTypeDAO;
    }

    @Resource
    public void setReporterTypeDAO(ReporterTypeDAO reporterTypeDAO) {
        this.reporterTypeDAO = reporterTypeDAO;
    }

    @Resource
    public void setCountryDAO(CountryDAO countryDAO) {
        this.countryDAO = countryDAO;
    }

    @Resource
    public void setDrugDAO(DrugDAO drugDAO) {
        this.drugDAO = drugDAO;
    }

    @Resource
    public void setOrganisationTypeDAO(OrganisationTypeDAO organisationTypeDAO) {
        this.organisationTypeDAO = organisationTypeDAO;
    }

    @Resource
    public void setQueryDAO(QueryDAO queryDAO) {
        this.queryDAO = queryDAO;
    }


    @Override
    public void deleteOldQueryType(int queryId, QueryType.Code oldQueryTypeCode) {
        this.queryDAO.deleteOldQueryType(queryId, oldQueryTypeCode);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateQRF(QueryDTO queryDto, int queryId, List<Integer> idsAttachmentsToRemove) {
        logger.info("QrfServiceBean:updateQRF called for query:" + queryId);
        queryDAO.updateQRF(queryDto, queryId, idsAttachmentsToRemove);
    }

    @Override
    public int createQRF(QueryDTO queryDto) {
        logger.info("QrfServiceBean:createQRF called");
        return queryDAO.createQRF(queryDto);
    }

    @Override
    public QueryDTO openQuery(int queryId, String emailAddress, boolean isReview) {
        return queryDAO.openQuery(queryId, emailAddress, isReview);
    }


    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isFinalDocExists(long queryId) {
        DateTime latestQueryStartDate = queryEAO.getLatestQueryStartDate(queryId);
        DateTime latestFinalDocDate = attachmentEAO.getLatestFinalDocCreationDate(queryId);
        if ((latestQueryStartDate == null) || (latestFinalDocDate == null)) {
            return false;
        } else if (latestFinalDocDate.isAfter(latestQueryStartDate)) {
            return true;
        }
        return false;
    }

    @Override
    public void addDrugEvent(QueryDescDTO queryDescDto, List caseDtoList,
                             String aeTerm, int queryNumber, String addComment) {

        logger.info("QrfServiceBean:addDrugEvent called for query:" + queryNumber);
        queryDAO.addDrugEvent(queryDescDto, caseDtoList, aeTerm, queryNumber, addComment);
    }

    @Override
    public void updateDrugEvent(QueryDescDTO queryDescDto, List caseDtoList,
                                String aeTerm, int queryNumber, String addComment) {
        logger.info("QrfServiceBean:updateDrugEvent called for query:" + queryNumber);
        queryDAO.updateDrugEvent(queryDescDto, caseDtoList, aeTerm, queryNumber, addComment);
    }

    /**
     * updates the status of a query based on its query number
     *
     * @param queryNumber the query number to add this query type to
     * @param status      the status of the query
     */
    @Override
    public void updateQRFStatus(int queryNumber, String status) {
        logger.info("QrfServiceBean:updateQRFStatus called for query:" + queryNumber);
        queryDAO.updateStatus(queryNumber, status);
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void createAttachments(List attachments, int queryId) {
        logger.debug("QrfServiceBean:createAttachments called");
        queryDAO.createAttachments(attachments, queryId);
    }


    /**
     * Updates a drug-interaction query type details to an existing query.
     * Also sets its status to submitted
     * @param queryDescDto  QueryDescDTO object
     * @param caseDtoList   a list consisting of case dto's
     * @param queryNumber   the query number to add this query type to
     * @param addComment    any additional comments
     * @param drugDtoList   a list consisting of drug dto's
     */
     @Override
    public void updateDrugInteraction( QueryDescDTO queryDescDto, List caseDtoList,
                                    int queryNumber, String addComment, List drugDtoList )
                                    {

        logger.info("QrfServiceBean:updateDrugInteraction called for query:" + queryNumber);
        queryDAO.updateDrugInteraction(queryDescDto, caseDtoList, queryNumber, addComment,
                                        drugDtoList);
    }


    /**
     * Adds a drug-interaction query type details to an existing query.
     * Also sets its status to submitted
     * @param queryDescDto	QueryDescDTO object
     * @param caseDtoList	a list consisting of case dto's
     * @param queryNumber	the query number to add this query type to
     * @param addComment	any additional comments
     * @param drugDtoList	a list consisting of drug dto's
     */
    @Override
    public void addDrugInteraction( QueryDescDTO queryDescDto, List caseDtoList,
                                    int queryNumber, String addComment, List drugDtoList ) {

        logger.info("QrfServiceBean:addDrugInteraction called for query:" + queryNumber);
        queryDAO.addDrugInteraction(queryDescDto, caseDtoList, queryNumber, addComment,
                                        drugDtoList);
    }

    /**
     * Updates a manufacturing query type details to an existing query.
     * Also sets its status to submitted
     * @param queryDescDto  QueryDescDTO object
     * @param caseDtoList   a list consisting of case dto's
     * @param batch         the manufacturing batch
     * @param queryNumber   the query number to add this query type to
     * @param addComment    any additional comments
     * @param outputId      the case output type
     * @param others        the other value for cases
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateManufacturing (QueryDescDTO queryDescDTO, Manufacturing manufacturing, ClinicalTrial clinical,
    		String aeTerm, String specialRequest, Integer queryNumber) {

        logger.info("QrfServiceBean:updateManufacturing called for query:" + queryNumber);

        queryDAO.updateManufacturing(queryDescDTO, manufacturing, clinical, aeTerm, specialRequest, queryNumber);
    }

    /**
     * Adds a manufacturing query type details to an existing query.
     * Also sets its status to submitted
     *
     * @param queryDescDto QueryDescDTO object
     * @param caseDtoList  a list consisting of case dto's
     * @param batch        the manufacturing batch
     * @param queryNumber  the query number to add this query type to
     * @param addComment   any additional comments
     * @param outputId     the case output type
     * @param others       the other value for cases
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void addManufacturing(QueryDescDTO queryDescDTO, Manufacturing manufacturing, ClinicalTrial clinical,
    		String aeTerm, String specialRequest, Integer queryNumber) {

        logger.info("QrfServiceBean:addManufacturing called for query:" + queryNumber);

        queryDAO.addManufacturing(queryDescDTO, manufacturing, clinical, aeTerm, specialRequest, queryNumber);
    }

    @Override
    public List<CtrlOformat> getCtrlOformats() {
        return ctrlOformatEAO.getAll();
    }

    @Override
    public List<PrgyExptype> getPrgyExptypes() {
        return prgyExptypeEAO.getAll();
    }

    /**
     * Updates a case clarification and case data request query type details to an existing query.
     * Also sets its status to submitted
     * @param caseDtoList   a list consisting of case dto's
     * @param queryNumber   the query number to add this query type to
     * @param addComment    any addiitonal comments
     * @param outputId      the case output type
     * @param others        the other value for cases
     */
       @Override
    public void updateCaseClarification (  List caseDtoList, int queryNumber, String addComment,
                                        int outputId, String others )
                                        {

        logger.info("QrfServiceBean:updateCaseClarification called for query:" + queryNumber);

        queryDAO.updateCaseClarification(caseDtoList, queryNumber, addComment, outputId, others);
    }

   /**
	 * Adds a case clarification and case data request query type details to an existing query.
	 * Also sets its status to submitted
	 * @param caseDtoList	a list consisting of case dto's
	 * @param queryNumber	the query number to add this query type to
	 * @param addComment	any addiitonal comments
	 * @param outputId		the case output type
	 * @param others		the other value for cases
	 */
   @Override
	public void addCaseClarification ( 	List caseDtoList, int queryNumber, String addComment,
										int outputId, String others ) {

		logger.info("QrfServiceBean:addCaseClarification called for query:" + queryNumber);

		queryDAO.addCaseClarification(caseDtoList, queryNumber, addComment, outputId, others);
	}


	/**
	 * Adds an IND updates query type details to an existing query.
	 * Also sets its status to submitted
	 * @param indNumber	the ind number
	 * @param queryNumber	the query number to add this query type to
	 */
    @Override
	public void addINDUpdates (String indNumber, int queryNumber) {

		logger.info("QrfServiceBean:addINDUpdates called for query:" + queryNumber);

		queryDAO.addINDUpdates(indNumber, queryNumber);
	}

   /**
     * Updates a literature query type details to an existing query.
     * Also sets its status to not submitted
     * @param queryDescDto  QueryDescDTO object
     * @param queryNumber   the query number to add this query type to
     * @param aeTerm        the ae term
     */
   @Override
    public void updateLiterature (QueryDescDTO queryDescDto, int queryNumber, String aeTerm)
                                        {

        logger.info("QrfServiceBean:updateLiterature called for query:" + queryNumber);

        queryDAO.updateLiterature(queryDescDto,queryNumber,aeTerm);
    }


    /**
	 * Adds a literature query type details to an existing query.
	 * Also sets its status to not submitted
	 * @param queryDescDto	QueryDescDTO object
	 * @param queryNumber	the query number to add this query type to
	 * @param aeTerm		the ae term
	 */
    @Override
	public void addLiterature (QueryDescDTO queryDescDto, int queryNumber, String aeTerm)
										{

		logger.info("QrfServiceBean:addLiterature called for query:" + queryNumber);

		queryDAO.addLiterature(queryDescDto,queryNumber,aeTerm);
	}

    /**
     * Adds a performance metrics query type details to an existing query.
     * Also sets its status to submitted
     * @param perf  PerformanceMetric object
     * @param queryNumber   the query number to add this query type to
     */
    @Override
    public void addPerformanceMetrics (PerformanceMetric perf, int queryNumber) {

        logger.info("QrfServiceBean:addPerformanceMetrics called for query:" + queryNumber);

        queryDAO.addPerformanceMetrics(perf, queryNumber);
    }
    /**
         * Updates a performance metrics query type details to an existing query.
         * Also sets its status to not submitted
         * @param perf	PerformanceMetric object
         * @param queryNumber	the query number to add this query type to
         */
    @Override
        public void updatePerformanceMetrics (PerformanceMetric perf, int queryNumber) {

            logger.info("QrfServiceBean:updatePerformanceMetrics called for query:" + queryNumber);

            queryDAO.updatePerformanceMetrics(perf, queryNumber);
        }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updatePregnancy(QueryDescDTO queryDesc, List caseDTOList,
        String timing, String outcome, int expType, Integer queryId, String comments,
        int outputformat, String othercomments) {
        logger.info("QrfService: updatePregnancy");

        QueryDescription qd = entityManager.find(QueryDescription.class, Long.valueOf(queryId));
        if (qd == null) {
            queryDAO.addQueryDesc(queryDesc, queryId);
        } else {
            queryDAO.updateQueryDesc(queryDesc, queryId);
        }
        Query query = queryEAO.find(Long.valueOf(queryId));
        entityManager.refresh(query);
        Pregnancy preg = query.getPregnancy();
         if (preg == null) {
             preg = new Pregnancy();
             preg.setQuery(query);
             preg.setQuerySeq(query.getQuerySeq());
             PrgyExptype prge = entityManager.find(PrgyExptype.class, Long.valueOf(expType));
             preg.setPrgyExptype(prge);
             preg.setTimeInPregnancy(timing);
             preg.setOutcome(outcome);
             query.setPregnancy(preg);
             pregnancyEAO.persist(preg);

        } else {
             entityManager.refresh(preg);
             PrgyExptype prge = entityManager.find(PrgyExptype.class, Long.valueOf(expType));
             preg.setPrgyExptype(prge);
             preg.setTimeInPregnancy(timing);
             preg.setOutcome(outcome);
             pregnancyEAO.merge(preg);
        }
        query.setCaseComment(StringUtils.upperCase(comments));
        query.setOformatSpecialRequest(StringUtils.upperCase(othercomments));
        query.setQueryStatusCode(entityManager.find(QueryStatusCode.class, Constants.QUERY_STATUS_NOT_SUBMITTED));
        query.setDatetimeSubmitted(new DateTime());
        if (outputformat != 0) {
            CaseOformat caseOutputFormat = entityManager.find(CaseOformat.class, Long.valueOf(outputformat));
            query.setCaseOformat(caseOutputFormat);
        }
        queryEAO.merge(query);
        queryDAO.updateCase(caseDTOList, queryId);
    }


    /**
     * Updates a signal detection query type details to an existing query.
     * Also sets its status to not submitted
     * @param queryDescDto  QueryDescDTO object
     * @param queryNumber   the query number to add this query type to
     * @param aeTerm        the ae term
     */
    @Override
    public void updateSignalDetection (QueryDescDTO queryDescDto, int queryNumber, String aeTerm)
                                    {

        logger.info("QrfServiceBean:updateSignalDetection called for query:" + queryNumber);

        queryDAO.updateSignalDetection(queryDescDto,queryNumber,aeTerm);
    }

    /**
	 * Adds a signal detection query type details to an existing query.
	 * Also sets its status to submitted
	 * @param queryDescDto	QueryDescDTO object
	 * @param queryNumber	the query number to add this query type to
	 * @param aeTerm		the ae term
	 */
    @Override
	public void addSignalDetection (QueryDescDTO queryDescDto, int queryNumber, String aeTerm)
									{

		logger.info("QrfServiceBean:addSignalDetection called for query:" + queryNumber);

		queryDAO.addSignalDetection(queryDescDto, queryNumber, aeTerm);
	}

    @Override
    public List<CaseOformat> getCaoTypes() {
        return caseOformatEAO.getAll();
    }

    @Override
    public List<CaseOformat> getActiveCaoTypes() {
        return caseOformatEAO.getAllActive();
    }


    @Override
    @Transactional
    public void addPregnancy(QueryDescDTO queryDescDTO, List<CaseDTO> caseDTOList,
                             Pregnancy pregnancy, int expType, int queryNumber, String comments, int outputformat, String othercomments) {

        logger.info("QrfService: addPregnancy");

        Query query = queryService.find(Long.valueOf(queryNumber));
        query.setCaseComment(comments.toUpperCase());
        query.setOformatSpecialRequest(othercomments.toUpperCase());
        QueryStatusCode queryStatCode = queryStatusCodeEAO.getByIdObj(Constants.QUERY_STATUS_NOT_SUBMITTED);
        query.setQueryStatusCode(queryStatCode);
        query.setDatetimeSubmitted(new DateTime());
        if (outputformat != 0) {
            query.setCaseOformat(caseOformatEAO.getReference(Long.valueOf(outputformat)));
        }

        queryService.merge(query);

        PrgyExptype prgyExptype = entityManager.find(PrgyExptype.class, Long.valueOf(expType));
        pregnancy.setPrgyExptype(prgyExptype);
        pregnancyEAO.persist(pregnancy);

        QueryDescription qd = queryDescDTO.getQueryDescription();
        qd.setQuerySeq(Long.valueOf(queryNumber));
        qd.setQuery(query);
        entityManager.persist(qd);

        for (CaseDTO caseDto : caseDTOList) {
            Case caseEnt = caseDto.getCase();
            caseEnt.setQuery(query);
            entityManager.persist(caseEnt);
        }

    }

    /**
     * Updates a external audit query type details to an existing query.
     * Also sets its status to not submitted
     *
     * @param queryDto QueryDTO object
     */
    @Override
    public void updateExternalAudit(QueryDescDTO queryDescDto, Audit audit, ClinicalTrial clinical, String specialRequest, int queryNumber) {
        queryDAO.updateExternalAudit(queryDescDto, audit, clinical, specialRequest, queryNumber);
    }

    /**
	 * Adds a external audit query type details to an existing query.
	 * Also sets its status to not submitted
	 * @param queryDto	QueryDTO object
	 */
    @Override
	public void addExternalAudit(QueryDescDTO queryDescDto, Audit audit, ClinicalTrial clinical, String specialRequest, int queryNumber) {
		queryDAO.addExternalAudit(queryDescDto, audit, clinical, specialRequest, queryNumber);
	}

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ReporterTypeDTO> retrieveReporterTypes() {
        return reporterTypeDAO.getReporterTypes();
    }

    @Override
    public List<OrganisationTypeDTO> retrieveOrganisationTypes() {
        return organisationTypeDAO.getOrganisationTypes();
    }

    private
    @Value("${qtt.help.queryType.DRUG_EVENT}")
    String DRUG_EVENT;
    private
    @Value("${qtt.help.queryType.DRUG_INTERACTION}")
    String DRUG_INTERACTION;
    private
    @Value("${qtt.help.queryType.MANUFACTURING}")
    String MANUFACTURING;
    private
    @Value("${qtt.help.queryType.PREGNANCY}")
    String PREGNANCY;
    private
    @Value("${qtt.help.queryType.EXTERNAL_AUDIT}")
    String EXTERNAL_AUDIT;
    private
    @Value("${qtt.help.queryType.MEDICAL_COMPLAINTS}")
    String MEDICAL_COMPLAINTS;
    private
    @Value("${qtt.help.queryType.CASE_CLARIFICATION}")
    String CASE_CLARIFICATION;
    private
    @Value("${qtt.help.queryType.CASE_DATA_REQUEST}")
    String CASE_DATA_REQUEST;
    private
    @Value("${qtt.help.queryType.SAE_RECONCILIATION}")
    String SAE_RECONCILIATION;
    private
    @Value("${qtt.help.queryType.IND_UPDATES}")
    String IND_UPDATES;
    private
    @Value("${qtt.help.queryType.PERFORMANCE_METRICS}")
    String PERFORMANCE_METRICS;
    private
    @Value("${qtt.help.queryType.PSUR}")
    String PSUR;
    private
    @Value("${qtt.help.queryType.SIGNAL_DETECTION}")
    String SIGNAL_DETECTION;

    private
    @Value("${qtt.help.queryType.LITERATURE_SEARCH}")
    String LITERATURE_SEARCH;

    @Override
    public Map<String, String> getQueryTypeUrls() {
        Map map = new TreeMap();

        map.put("1", DRUG_EVENT);
        map.put("2", DRUG_INTERACTION);
        map.put("3", MANUFACTURING);
        map.put("4", PREGNANCY);
        map.put("5", EXTERNAL_AUDIT);
        map.put("6", MEDICAL_COMPLAINTS);
        map.put("7", CASE_CLARIFICATION);
        map.put("8", CASE_DATA_REQUEST);
        map.put("9", SAE_RECONCILIATION);
        map.put("10", IND_UPDATES);
        map.put("11", PERFORMANCE_METRICS);
        map.put("12", PSUR);
        map.put("13", SIGNAL_DETECTION);
        map.put("14", LITERATURE_SEARCH);

        return map;
    }

    @Override
    public List<PartialQueryDTO> getPartialQueries(String email, String status) {
        return queryDAO.getPartialQueries(email, status);
    }

    public void setCtrlOformatEAO(CtrlOformatEAO ctrlOformatEAO) {
        this.ctrlOformatEAO = ctrlOformatEAO;
    }

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void addMedical(QueryDescDTO queryDescDTO, Case caze, Pregnancy pregnancy, List<Drug> drugs, String comments, Integer queryNumber) {
        logger.info("QrfService: addPregnancy");

        Query query = queryService.find(Long.valueOf(queryNumber));
        query.setCaseComment(comments.toUpperCase());
        QueryStatusCode queryStatCode = queryStatusCodeEAO.getByIdObj(Constants.QUERY_STATUS_NOT_SUBMITTED);
        query.setQueryStatusCode(queryStatCode);
        query.setDatetimeSubmitted(new DateTime());

        queryService.merge(query);

        if (pregnancy.getPrgyExptype() != null
        		|| StringUtils.isNotBlank(pregnancy.getTimeInPregnancy())
        		|| StringUtils.isNotBlank(pregnancy.getOutcome())
        		|| StringUtils.isNotBlank(pregnancy.getComments())) {
        	if (pregnancy.getPrgyExptype() != null) {
    	        pregnancy.setPrgyExptype(entityManager.getReference(PrgyExptype.class, Long.valueOf(pregnancy.getPrgyExptype().getExposureTypeId())));
        	}
	        query.setPregnancy(pregnancy);
	        pregnancy.setQuery(query);
	        pregnancy.setQuerySeq(query.getQuerySeq());
	        pregnancyEAO.persist(pregnancy);
        }

        QueryDescription qd = queryDescDTO.getQueryDescription();
        //  qd.setQuerySeq(Long.valueOf(queryNumber));
        qd.setQuery(query);
        entityManager.persist(qd);

        if (StringUtils.isNotBlank(caze.getAerNumber()) 
        		|| StringUtils.isNotBlank(caze.getLocalReferenceNumber()) 
        		|| StringUtils.isNotBlank(caze.getExternalReferenceNumber())) {
	        caze.setQuery(query);
	        caze.setQuerySeq(query.getQuerySeq());
	        entityManager.persist(caze);
        }

        for (Drug d : drugs) {
        	if (StringUtils.isNotBlank(d.getDrugRetrievalName())
        			|| StringUtils.isNotBlank(d.getInnGenericName())) {
        		d.setQuery(query);
        		d.setFirstFlag('N');
        		entityManager.persist(d);
        	}
        }
	}

	@Override
    @Transactional(propagation = Propagation.REQUIRED)
	public void updateMedical(QueryDescDTO queryDescDTO, Case caze, Pregnancy pregnancy, List<Drug> drugs, String comments, Integer queryNumber) {
		queryDAO.updateMedical(queryDescDTO, caze, pregnancy, drugs, comments, queryNumber);
	}

	@Override
    @Transactional(propagation = Propagation.REQUIRED)
	public void addDataSearch(QueryDescDTO queryDesc,
			DataSearch dataSearch, ClinicalTrial clinicalTrial,
			CaseOformat caseOformat, String other, String meddraTerm,
			String additionalComments, Integer queryNumber) {
        Query query = queryService.find(Long.valueOf(queryNumber));
        query.setCaseComment(additionalComments != null ? additionalComments.toUpperCase() : null);
        QueryStatusCode queryStatCode = queryStatusCodeEAO.getByIdObj(Constants.QUERY_STATUS_NOT_SUBMITTED);
        query.setQueryStatusCode(queryStatCode);
        query.setDatetimeSubmitted(new DateTime());
        
        if (caseOformat != null) {
	        query.setCaseOformat(entityManager.getReference(CaseOformat.class, caseOformat.getCaseOformatId()));
	        if (CaseOformat.OTHER == caseOformat.getCaseOformatId()) {
	        	query.setOformatSpecialRequest(other);
	        }
        }

        query.setDataSearch(dataSearch);
        dataSearch.setQuery(query);
        dataSearch.setMeddraPreferredTerm(meddraTerm);
        
        dataSearchEAO.persist(dataSearch);

        queryService.merge(query);

        if (StringUtils.isNotBlank(queryDesc.getNatureOfCase())) {
	        QueryDescription qd = queryDesc.getQueryDescription();
	        //  qd.setQuerySeq(Long.valueOf(queryNumber));
	        qd.setQuery(query);
	        entityManager.persist(qd);
        }

        clinicalTrial.setQuery(query);
        clinicalTrial.setQuerySeq(query.getQuerySeq());
        entityManager.persist(clinicalTrial);

	}

	@Override
    @Transactional(propagation = Propagation.REQUIRED)
	public void updateDataSearch(QueryDescDTO queryDesc,
			DataSearch dataSearch, ClinicalTrial clinicalTrial,
			CaseOformat caseOformat, String other, String meddraTerm,
			String additionalComments, Integer queryNumber) {
		queryDAO.updateDataSearch(queryDesc, dataSearch, clinicalTrial, caseOformat, 
				other, meddraTerm, additionalComments, queryNumber);
	}

    @Override
    public String retrievePossibleEmailDomains() {
        return queryDAO.getPossibleEmailDomains().toString().replace("[","").replace("]","");
    }

}
