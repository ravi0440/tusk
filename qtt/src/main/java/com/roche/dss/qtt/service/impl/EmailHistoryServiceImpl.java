package com.roche.dss.qtt.service.impl;


import com.roche.dss.qtt.eao.EmailHistoryEAO;
import com.roche.dss.qtt.model.EmailHistory;
import com.roche.dss.qtt.service.EmailHistoryService;

@Service("emailHistoryService")
@Transactional
public class EmailHistoryServiceImpl implements EmailHistoryService {


    @Autowired
    private EmailHistoryEAO emailHistoryEAO;

    @Override
    @Transactional
    public void saveEmailHistory(String subject, String content, String to, String from) {
        EmailHistory emailHistory = new EmailHistory();
        emailHistory.setSentDate(new DateTime());
        emailHistory.setSubject(subject);
        emailHistory.setContent(content);
        emailHistory.setSentFrom(from);
        emailHistory.setSentTo(to);

        emailHistoryEAO.persist(emailHistory);
        emailHistoryEAO.flush();
    }
}
