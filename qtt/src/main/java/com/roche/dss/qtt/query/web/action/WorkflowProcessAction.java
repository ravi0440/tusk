package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.eao.QueryWorkflowHistoryEAO;
import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.EuRegulatoryHistory;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryWorkflowHistory;
import com.roche.dss.qtt.service.EuRegulatoryHistoryService;
import com.roche.dss.qtt.service.FileOperationsService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.workflow.WorkflowService;
import com.roche.dss.qtt.service.workflow.exception.TaskAlreadyBegunException;
import com.roche.dss.qtt.service.workflow.exception.TaskAlreadyCompletedException;
import com.roche.dss.qtt.utility.EmailSender;
import com.roche.dss.qtt.utility.WorkflowRoute;
import com.roche.dss.qtt.utility.config.Config;
import com.roche.dss.qtt.utility.pdf.QrfPdfInterface;

import javax.annotation.Resource;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * User: pruchnil
 */
public class WorkflowProcessAction extends CommonActionSupport {
    private static final Logger logger = LoggerFactory.getLogger(WorkflowProcessAction.class);
    private long queryId;
    private long processId;
    private Query query;
    protected Config config;
    private QrfPdfInterface qrfPdf;

    private QueryService queryService;
    private WorkflowService workflowService;

    private String selectedRoute;
    private String queryLabel;
    private String taskOwner;
    private long taskId;
    protected EmailSender emailSender;
    
    
    private boolean euqppv;
    
    @Autowired
	private FileOperationsService fileOpsService;
    
    @Autowired
    private QueryWorkflowHistoryEAO queryWorkflowHistoryEAO;

    @Autowired
    private EuRegulatoryHistoryService euRegulatoryHistoryService;

    @Resource
    public void setConfig(Config config) {
        this.config = config;
    }
    
    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Resource
    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }
    
    @Resource
    public void setQrfPdfInterface(QrfPdfInterface qrfPdf) {
        this.qrfPdf = qrfPdf;
    }
    
    @Resource
    public void setEmailSender(EmailSender emailSender) {
        this.emailSender = emailSender;
    }


    @SkipValidation
    @Override
    public String execute() throws Exception {
        if (!isDscl()) {
            return DENIED;
        }
        query = queryService.find(queryId);
        return SUCCESS;
    }

    @SkipValidation
    public String restart() throws Exception {
        if (!isDscl()) {
            return DENIED;
        }
        taskId = workflowService.restartProcess(processId, getLoggedUserName());
        return SUCCESS;
    }

    @SkipValidation
    public String select() {
        if (!isDscl()) {
            return DENIED;
        }
		try {
			workflowService.lockTask(getLoggedUserName(), taskId);
		} catch (TaskAlreadyBegunException e) {
			return TASK_ALREADY_BEGUN;
		} catch (TaskAlreadyCompletedException e) {
			return TASK_ALREADY_COMPLETED;
		} catch (UnsupportedOperationException e) {
			return ERROR;
		}
        query = queryService.find(queryId);
        return SUCCESS;
    }

    
    public void sendEmail(Query query) {
        // send email
        String from = "";
        String subject = "";
        StringBuilder body = new StringBuilder();
        StringBuilder to = new StringBuilder();
        ByteArrayOutputStream out = qrfPdf.qrfBuilder(Long.valueOf(query.getQuerySeq()).intValue(), "Accepted");
        String attachName = "QRF-" + query.getQueryNumber() + ".pdf";
        from = config.getInititateDsclEmail();
        String queryLabel = "";
        if (!StringUtils.isEmpty(query.getRequesterQueryLabel())) {
            queryLabel = " - [" + query.getRequesterQueryLabel() + "]";
        }
        subject = "Query " + query.getQueryNumber() + " "
                + config.getInititateAcceptEmailSubject() + queryLabel;

        body.append("\n");
        body.append(config.getInititateAcceptBody1Para());
        body.append("\n\n");
        body.append(config.getInititateAcceptBody2Para());
        body.append(" ");
        body.append(query.getQueryNumber());
        body.append(".");
        body.append("\n");
        body.append(config.getInititateAcceptBody3Para()); 
        body.append("\n\n");
 		body.append(config.getInititateAcceptBody4Para());
 		body.append(" ");
 		body.append(DateTimeFormat.forPattern("dd-MMM-yyyy").print(query.getDateRequested()));
 		body.append("\n");
 		body.append(config.getInititateAcceptBodyLastPara());

 		to.append(config.getEuqppvEmail());

 		
 		EmailSender.FormFile[] files = new EmailSender.FormFile[query.getAttachments().size()];
 		int i=0;
 		for(Attachment attach: query.getAttachments()){
 			

            File file = new File(fileOpsService.findDestinationDirectory(Long.toString(query.getQuerySeq()))+attach.getFilename());
            String fileName = file.getName();
					
			String mimeType = fileOpsService.getMimeType( fileName );
 			
 			files[i]=new EmailSender.FormFile(file, attach.getFilename(),mimeType);
 			i++;
 		}
 		
        try {
            emailSender.sendMessage(from, to.toString(),"", subject, body.toString(), attachName, out,files);
            logger.info("WorkflowProcesAction:sending email to:" + to + " query no:" + query.getQueryNumber());
        } catch (Exception e) {
            logger.error("Error WorkflowProcesAction:sending:sending email " + to + " query no:" + query.getQueryNumber(), e);
        }
    }

    public String update() {
        if (!isDscl()) {
            return DENIED;
        }
        // save euqppv
      	 queryService.setInformEUQPPV(euqppv, queryId);

        // save euRegulatoryHistory
        saveEuRegulatoryHistorii();

      	 //send qrf email to euqppv 
      	 if(euqppv){
      		sendEmail(queryService.find(queryId));
      	 }
      		
        try {
        	workflowService.selectWorkflowRoute(queryId, selectedRoute, getLoggedUserName(), queryLabel);
        } catch (HibernateOptimisticLockingFailureException e){
            logger.warn("Occurred ", e);
            return MODIFIED;
        } catch (TaskAlreadyCompletedException e) {
			return TASK_ALREADY_COMPLETED;
        } catch (UnsupportedOperationException e) {
        	logger.error("Unsupported Operation for query with query seq: " + queryId + " " + e.getMessage());
			return ERROR;
        } catch (Exception e) {
			logger.error("Error while dispatching query with query seq: " + queryId + " " + e.getMessage());
			return ERROR;
		}
        
        Query querytmp = queryService.find(queryId);
        QueryWorkflowHistory queryWorkflowHistory = new QueryWorkflowHistory();
        queryWorkflowHistory.setStartDate(new DateTime());
        queryWorkflowHistory.setModifiedBy(getLoggedUserName());
        queryWorkflowHistory.setWorkflowId(String.valueOf(queryId));
        queryWorkflowHistory.setStatus("WORKFLOW_DISPATCH");
        queryWorkflowHistoryEAO.save(queryWorkflowHistory);
        
        return SUCCESS;
    }

    private void saveEuRegulatoryHistorii() {
        EuRegulatoryHistory euRegulatoryHistorii = new EuRegulatoryHistory();
        euRegulatoryHistorii.setModifiedBy(getLoggedUserName());
        euRegulatoryHistorii.setModificationDate(new DateTime());
        euRegulatoryHistorii.setValue(euqppv ? "Y" : "N");

        Query query = queryService.find(queryId);
        euRegulatoryHistorii.setFollowupSeq(query.getFollowupSeq());
        euRegulatoryHistorii.setQuerySeq(query.getQuerySeq());

        euRegulatoryHistoryService.saveEuRegulatoryHistory(euRegulatoryHistorii);
    }

    @Override
    public void onActionErrors() {
        if (queryId > 0) {
            query = queryService.find(queryId);
        }
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public long getProcessId() {
        return processId;
    }

    public void setProcessId(long processId) {
        this.processId = processId;
    }

    public Query getQuery() {
        return query;
    }

    public List<WorkflowRoute> getRoutes() {
        return Arrays.asList(WorkflowRoute.values());
    }

    public String getSelectedRoute() {
        return selectedRoute;
    }

    @RequiredStringValidator(type = ValidatorType.FIELD, key = "route.required")
    public void setSelectedRoute(String selectedRoute) {
        this.selectedRoute = selectedRoute;
    }

    public String getQueryLabel() {
        return queryLabel;
    }

    @RequiredStringValidator(type = ValidatorType.FIELD, key = "label.required")
    public void setQueryLabel(String queryLabel) {
        this.queryLabel = queryLabel;
    }

    public String getTaskOwner() {
        return taskOwner;
    }

    public void setTaskOwner(String taskOwner) {
        this.taskOwner = taskOwner;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }
    
    public void setEuqppv(boolean euqppv){
    this.euqppv=euqppv;	
    }
    
    public boolean getEuqppv(boolean euqppv){
    	return this.euqppv;
    }
    
    
}
