package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.CaseEAO;
import com.roche.dss.qtt.model.Case;
import java.util.List;

@Repository("caseEAO")
public class CaseEAOImpl extends AbstractQueryRelatedEAO<Case> implements CaseEAO {

    @Override
    public List<Case> findAllByQueryId(long queryId) {
        return getEntityManager().createNamedQuery("case.findByQuerySeq", Case.class).setParameter("querySeq", queryId).getResultList();
    }
}