/* 
====================================================================
  $Header$
  @author $Author: savovp $
  @version $Revision: 2594 $ $Date: 2012-12-11 15:26:15 +0100 (Wt, 11 gru 2012) $

====================================================================
*/
package com.roche.dss.qtt.query.comms.dto;


public class PregnancyDTO  {
	
	private int exposureType;
	private String timing;
	private String outcome;
	private String exposureTypeDesc;
	private String comments;
	
    /**
     * @return
     */
    public int getExposureType() {
        return exposureType;
    }

    /**
     * @return
     */
    public String getOutcome() {
        return outcome;
    }

    /**
     * @return
     */
    public String getTiming() {
        return timing;
    }

    /**
     * @param i
     */
    public void setExposureType(int i) {
        exposureType = i;
    }

    /**
     * @param string
     */
    public void setOutcome(String string) {
        outcome = string;
    }

    /**
     * @param string
     */
    public void setTiming(String string) {
        timing = string;
    }

    /**
     * @return
     */
    public String getExposureTypeDesc() {
        return exposureTypeDesc;
    }

    /**
     * @param string
     */
    public void setExposureTypeDesc(String string) {
        exposureTypeDesc = string;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + exposureType;
		result = prime
				* result
				+ ((exposureTypeDesc == null) ? 0 : exposureTypeDesc.hashCode());
		result = prime * result + ((outcome == null) ? 0 : outcome.hashCode());
		result = prime * result + ((timing == null) ? 0 : timing.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if(!(obj instanceof PregnancyDTO))
			return false;
		PregnancyDTO other = (PregnancyDTO) obj;
		if (exposureType != other.exposureType)
			return false;
		if (exposureTypeDesc == null) {
			if (other.exposureTypeDesc != null)
				return false;
		} else if (!exposureTypeDesc.equals(other.exposureTypeDesc))
			return false;
		if (outcome == null) {
			if (other.outcome != null)
				return false;
		} else if (!outcome.equals(other.outcome))
			return false;
		if (timing == null) {
			if (other.timing != null)
				return false;
		} else if (!timing.equals(other.timing))
			return false;
		return true;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
