package com.roche.dss.qtt.query.web.qrf.model;

public class SearchScopeEntry {
	protected String key;
	
	protected String value;
	
	public SearchScopeEntry(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
