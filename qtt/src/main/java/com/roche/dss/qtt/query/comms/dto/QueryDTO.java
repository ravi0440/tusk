/*
====================================================================
  $Header$
  @author $Author: hudowskr $
  @version $Revision: 1281 $ $Date: 2008-10-10 14:34:07 +0200 (Pt, 10 paź 2008) $

====================================================================
*/
package com.roche.dss.qtt.query.comms.dto;

import com.roche.dss.qtt.model.AeTerm;
import com.roche.dss.qtt.model.Audit;
import com.roche.dss.qtt.model.DataSearch;
import com.roche.dss.qtt.model.Manufacturing;
import com.roche.dss.qtt.model.PerformanceMetric;
import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.util.ApplicationUtils;

import java.util.List;

public class QueryDTO  {

	private String queryNumber;
    private String followupNumber;
//    private String followupNumberString;
	private String queryNumberString;
	private String sourceCountry;
	private QueryType.Code queryType;
	private String statusCode;
	private int reporterType;
	private boolean isAllDrug;
	private boolean isUrgent;
	private String urgency;
	private String followUpNumber;
	private long currDueDate;
	private long originalDueDate;
	private long dateRequested;
	private long expirydate;
	private String others;
	private String querySummary;
	private String querySummaryUpper;
	private String prepComments;
	private RequesterDTO requester;
	private List preparatory;
	//private String prepOthers;
	private DrugDTO drug;
	private List<AttachmentDTO> attachments;
	private CumulativePeriodDTO cumulative;
	private String queryTypeDescription;
	private String reporterTypeDescription;
	private String sourceCountryDescription;
	private QueryDescDTO queryDesc;
	private String aeTerm;
	private List cases;
	private String caseComment;
	private List drugs;
	private String specialRequest;
	private String batch;
	private String outputFormat;
	private int outputFormatId;
	private PregnancyDTO pregnancy;
	private ClinicalTrialDTO clinical;
	private String INDNumber;
	private String action;
    private String accessKey;
    private String requesterQueryLabel;
    private boolean reopen;

    private long querySeq;
    private String drugName;
    private org.joda.time.DateTime initiationDate;
    private org.joda.time.DateTime responseDate;
    private String activity;
    private String email;
    
    private PerformanceMetric performanceMetric;
    private Audit audit;
    private Manufacturing manufacturing;
    private DataSearch dataSearch;


    public long getQuerySeq() {
        return querySeq;
    }

    public void setQuerySeq(long querySeq) {
        this.querySeq = querySeq;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public org.joda.time.DateTime getInitiationDate() {
        return initiationDate;
    }

    public void setInitiationDate(org.joda.time.DateTime initiationDate) {
        this.initiationDate = initiationDate;
    }

    public org.joda.time.DateTime getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(org.joda.time.DateTime responseDate) {
        this.responseDate = responseDate;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }


	/**
	 * @default constructor
	 */
	public QueryDTO() {

	}

    /**
     * @return
     */
    public long getCurrDueDate() {
        return currDueDate;
    }

    /**
     * @return
     */
    public long getDateRequested() {
        return dateRequested;
    }

    /**
     * @return
     */
    public long getExpirydate() {
        return expirydate;
    }

    /**
     * @return
     */
    public String getFollowUpNumber() {
        return followUpNumber;
    }


    /**
     * @return
     */
    public boolean isAllDrug() {
        return isAllDrug;
    }

    /**
     * @return
     */
    public boolean isUrgent() {
        return isUrgent;
    }

    /**
     * @return
     */
    public long getOriginalDueDate() {
        return originalDueDate;
    }

    /**
     * @return
     */
    public String getPrepComments() {
        return prepComments;
    }

    /**
     * @return
     */
    public String getQueryNumber() {
        return queryNumber;
    }

    /**
     * @return
     */
    public String getQuerySummary() {
        return querySummary;
    }

    /**
     * @return
     */
    public String getQuerySummaryUpper() {
        return querySummaryUpper;
    }

    /**
     * @return
     */
    public QueryType.Code getQueryType() {
        return queryType;
    }

    /**
     * @return
     */
    public int getReporterType() {
        return reporterType;
    }

    /**
     * @return
     */
    public String getSourceCountry() {
        return sourceCountry;
    }


    /**
     * @return
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @return
     */
    public String getUrgency() {
        return urgency;
    }

    /**
     * @param l
     */
    public void setCurrDueDate(long l) {
        currDueDate = l;
    }

    /**
     * @param l
     */
    public void setDateRequested(long l) {
        dateRequested = l;
    }

    /**
     * @param l
     */
    public void setExpirydate(long l) {
        expirydate = l;
    }

    /**
     * @param string
     */
    public void setFollowUpNumber(String string) {
        followUpNumber = string;
    }


    /**
     * @param b
     */
    public void setAllDrug(boolean b) {
        isAllDrug = b;
    }

    /**
     * @param b
     */
    public void setUrgent(boolean b) {
        isUrgent = b;
    }

    /**
     * @param l
     */
    public void setOriginalDueDate(long l) {
        originalDueDate = l;
    }

    /**
     * @param string
     */
    public void setPrepComments(String string) {
        prepComments = string;
    }

    /**
     * @param i
     */
    public void setQueryNumber(String i) {
        queryNumber = i;
    }

    /**
     * @param string
     */
    public void setQuerySummary(String string) {
        querySummary = string;
    }

    /**
     * @param string
     */
    public void setQuerySummaryUpper(String string) {
        querySummaryUpper = string;
    }

    /**
     * @param queryType
     */
    public void setQueryType(QueryType.Code queryType) {
        this.queryType = queryType;
    }

    /**
     * @param i
     */
    public void setReporterType(int i) {
        reporterType = i;
    }

    /**
     * @param string
     */
    public void setSourceCountry(String string) {
        sourceCountry = string;
    }

    /**

     */
    public void setStatusCode(String string) {
        statusCode = string;
    }

    /**
     * @param string
     */
    public void setUrgency(String string) {
        urgency = string;
    }

    /**
     * @return
     */
    public List<AttachmentDTO> getAttachments() {
        return attachments;
    }

    /**
     * @return
     */
    public CumulativePeriodDTO getCumulative() {
        return cumulative;
    }

    /**
     * @return
     */
    public DrugDTO getDrug() {
        return drug;
    }

    /**
     * @return
     */
    public List getPreparatory() {
        return preparatory;
    }

    /**
     * @return
     */
    /*public String getPrepOthers() {
        return prepOthers;
    }*/

    /**
     * @return
     */
    public RequesterDTO getRequester() {
        return requester;
    }

    /**
     * @param list
     */
    public void setAttachments(List<AttachmentDTO> list) {
        attachments = list;
    }

    /**
     * @param periodDTO
     */
    public void setCumulative(CumulativePeriodDTO periodDTO) {
        cumulative = periodDTO;
    }

    /**
     * @param drugDTO
     */
    public void setDrug(DrugDTO drugDTO) {
        drug = drugDTO;
    }

    /**
     * @param list
     */
    public void setPreparatory(List list) {
        preparatory = list;
    }

    /**
     * @param string
     */
   /* public void setPrepOthers(String string) {
        prepOthers = string;
    }*/

    /**
     * @param requesterDTO
     */
    public void setRequester(RequesterDTO requesterDTO) {
        requester = requesterDTO;
    }


    /**
     * @return
     */
    public String getQueryTypeDescription() {
        return queryTypeDescription;
    }

    /**
     * @return
     */
    public String getReporterTypeDescription() {
        return reporterTypeDescription;
    }

    /**
     * @param string
     */
    public void setQueryTypeDescription(String string) {
        queryTypeDescription = string;
    }

    /**
     * @param string
     */
    public void setReporterTypeDescription(String string) {
        reporterTypeDescription = string;
    }

    /**
     * @return
     */
    public String getSourceCountryDescription() {
        return sourceCountryDescription;
    }

    /**
     * @param string
     */
    public void setSourceCountryDescription(String string) {
        sourceCountryDescription = string;
    }

    /**
     * @return
     */
    public QueryDescDTO getQueryDesc() {
        return queryDesc;
    }

    /**
     * @param descDTO
     */
    public void setQueryDesc(QueryDescDTO descDTO) {
        queryDesc = descDTO;
    }

    /**
     * @return
     */
    public String getAeTerm() {
        return aeTerm;
    }

    /**
     * @param string
     */
    public void setAeTerm(String string) {
        aeTerm = string;
    }

    /**
     * @return
     */
    public List getCases() {
        return cases;
    }

    /**
     * @param caseDTO
     */
    public void setCases(List caseDTO) {
        cases = caseDTO;
    }

    /**
     * @return
     */
    public String getCaseComment() {
        return caseComment;
    }

    /**
     * @param string
     */
    public void setCaseComment(String string) {
        caseComment = string;
    }

    /**
     * @return
     */
    public List getDrugs() {
        return drugs;
    }

    /**
     * @param list
     */
    public void setDrugs(List list) {
        drugs = list;
    }

    /**
     * @return
     */
    public String getSpecialRequest() {
        return specialRequest;
    }

    /**
     * @param string
     */
    public void setSpecialRequest(String string) {
        specialRequest = string;
    }

    /**
     * @return
     */
    public String getBatch() {
        return batch;
    }

    /**
     * @param string
     */
    public void setBatch(String string) {
        batch = string;
    }

    /**
     * @return
     */
    public String getOutputFormat() {
        return outputFormat;
    }

    /**
     * @param string
     */
    public void setOutputFormat(String string) {
        outputFormat = string;
    }

	/**
		 * @return
		 */
		public int getOutputFormatId() {
			return outputFormatId;
		}

		/**

		 */
		public void setOutputFormatId(int i) {
			outputFormatId = i;
		}

    /**
     * @return
     */
    public PregnancyDTO getPregnancy() {
        return pregnancy;
    }

    /**
     * @param pregnancyDTO
     */
    public void setPregnancy(PregnancyDTO pregnancyDTO) {
        pregnancy = pregnancyDTO;
    }

    /**
     * @return
     */
    public ClinicalTrialDTO getClinical() {
        return clinical;
    }

    /**
     * @param trialDTO
     */
    public void setClinical(ClinicalTrialDTO trialDTO) {
        clinical = trialDTO;
    }

    /**
     * @return
     */
    public String getINDNumber() {
        return INDNumber;
    }

    /**
     * @param string
     */
    public void setINDNumber(String string) {
        INDNumber = string;
    }

    /**
     * @return
     */
    public String getAction() {
        return action;
    }

    /**
     * @param string
     */
    public void setAction(String string) {
        action = string;
    }

    /**
     * @return
     */
    public String getQueryNumberString() {
        return queryNumberString;
    }

    /**
     * @param string
     */
    public void setQueryNumberString(String string) {
        queryNumberString = string;
    }

    /**
     * @return
     */
    public String getOthers()
    {
        return others;
    }

    /**
     * @param string
     */
    public void setOthers(String string)
    {
        others = string;
    }

    /**
     * @return Returns the accessKey.
     */
    public String getAccessKey() {
        return accessKey;
    }

    /**
     * @param accessKey The accessKey to set.
     */
    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    /**
     * @return Returns the requesterQueryLabel.
     */
    public String getRequesterQueryLabel() {
        return requesterQueryLabel;
    }

    /**
     * @param requesterQueryLabel The requesterQueryLabel to set.
     */
    public void setRequesterQueryLabel(String requesterQueryLabel) {
        this.requesterQueryLabel = requesterQueryLabel;
    }

    /**
     * @return the followupNumber
     */
    public String getFollowupNumber() {
        return followupNumber;
    }

    /**
     * @param followupNumber the followupNumber to set
     */
    public void setFollowupNumber(String followupNumber) {
        this.followupNumber = followupNumber;
    }

//    /**
//     * @return the followupNumberString
//     */
//    public String getFollowupNumberString() {
//        return followupNumberString;
//    }

    /**
     * @param followupNumberString the followupNumberString to set
     */
//    public void setFollowupNumberString(String followupNumberString) {
//        this.followupNumberString = followupNumberString;
//    }

    public boolean isReopen() {
		return reopen;
	}

	public void setReopen(boolean reopen) {
		this.reopen = reopen;
	}


    public void setRequesterEmail(String email) {
        this.email = email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInitiationDateFormatted() {
            return ApplicationUtils.DATE_TIME_FORMATTER.print(initiationDate);
        }

    public String getResponseDateFormatted() {
           return ApplicationUtils.DATE_TIME_FORMATTER.print(responseDate);
       }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((INDNumber == null) ? 0 : INDNumber.hashCode());
		result = prime * result
				+ ((accessKey == null) ? 0 : accessKey.hashCode());
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result
				+ ((activity == null) ? 0 : activity.hashCode());
		result = prime * result + ((aeTerm == null) ? 0 : aeTerm.hashCode());
		result = prime * result
				+ ((attachments == null) ? 0 : attachments.hashCode());
		result = prime * result + ((batch == null) ? 0 : batch.hashCode());
		result = prime * result
				+ ((caseComment == null) ? 0 : caseComment.hashCode());
		result = prime * result + ((cases == null) ? 0 : cases.hashCode());
		result = prime * result
				+ ((clinical == null) ? 0 : clinical.hashCode());
		result = prime * result
				+ ((cumulative == null) ? 0 : cumulative.hashCode());
		result = prime * result + (int) (currDueDate ^ (currDueDate >>> 32));
		result = prime * result
				+ (int) (dateRequested ^ (dateRequested >>> 32));
		result = prime * result + ((drug == null) ? 0 : drug.hashCode());
		result = prime * result
				+ ((drugName == null) ? 0 : drugName.hashCode());
		result = prime * result + ((drugs == null) ? 0 : drugs.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + (int) (expirydate ^ (expirydate >>> 32));
		result = prime * result
				+ ((followUpNumber == null) ? 0 : followUpNumber.hashCode());
		result = prime * result
				+ ((initiationDate == null) ? 0 : initiationDate.hashCode());
		result = prime * result + (isAllDrug ? 1231 : 1237);
		result = prime * result + (isUrgent ? 1231 : 1237);
		result = prime * result
				+ (int) (originalDueDate ^ (originalDueDate >>> 32));
		result = prime * result + ((others == null) ? 0 : others.hashCode());
		result = prime * result
				+ ((outputFormat == null) ? 0 : outputFormat.hashCode());
		result = prime * result + outputFormatId;
		result = prime * result
				+ ((pregnancy == null) ? 0 : pregnancy.hashCode());
		result = prime * result
				+ ((prepComments == null) ? 0 : prepComments.hashCode());
		result = prime * result
				+ ((preparatory == null) ? 0 : preparatory.hashCode());
		result = prime * result
				+ ((queryDesc == null) ? 0 : queryDesc.hashCode());
		result = prime * result
				+ ((queryNumber == null) ? 0 : queryNumber.hashCode());
		result = prime
				* result
				+ ((queryNumberString == null) ? 0 : queryNumberString
						.hashCode());
		result = prime * result + (int) (querySeq ^ (querySeq >>> 32));
		result = prime * result
				+ ((querySummary == null) ? 0 : querySummary.hashCode());
		result = prime
				* result
				+ ((querySummaryUpper == null) ? 0 : querySummaryUpper
						.hashCode());
		result = prime * result + ((queryType == null) ? 0 : queryType.ordinal());
		result = prime
				* result
				+ ((queryTypeDescription == null) ? 0 : queryTypeDescription
						.hashCode());
		result = prime * result + (reopen ? 1231 : 1237);
		result = prime * result + reporterType;
		result = prime
				* result
				+ ((reporterTypeDescription == null) ? 0
						: reporterTypeDescription.hashCode());
		result = prime * result
				+ ((requester == null) ? 0 : requester.hashCode());
		result = prime
				* result
				+ ((requesterQueryLabel == null) ? 0 : requesterQueryLabel
						.hashCode());
		result = prime * result
				+ ((responseDate == null) ? 0 : responseDate.hashCode());
		result = prime * result
				+ ((sourceCountry == null) ? 0 : sourceCountry.hashCode());
		result = prime
				* result
				+ ((sourceCountryDescription == null) ? 0
						: sourceCountryDescription.hashCode());
		result = prime * result
				+ ((specialRequest == null) ? 0 : specialRequest.hashCode());
		result = prime * result
				+ ((statusCode == null) ? 0 : statusCode.hashCode());
		result = prime * result + ((urgency == null) ? 0 : urgency.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof QueryDTO))
			return false;
		QueryDTO other = (QueryDTO) obj;
		if (INDNumber == null) {
			if (other.INDNumber != null)
				return false;
		} else if (!INDNumber.equals(other.INDNumber))
			return false;
		if (accessKey == null) {
			if (other.accessKey != null)
				return false;
		} else if (!accessKey.equals(other.accessKey))
			return false;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (activity == null) {
			if (other.activity != null)
				return false;
		} else if (!activity.equals(other.activity))
			return false;
		if (aeTerm == null) {
			if (other.aeTerm != null)
				return false;
		} else if (!aeTerm.equals(other.aeTerm))
			return false;
		if (attachments == null) {
			if (other.attachments != null)
				return false;
		} else if (!attachments.equals(other.attachments))
			return false;
		if (batch == null) {
			if (other.batch != null)
				return false;
		} else if (!batch.equals(other.batch))
			return false;
		if (caseComment == null) {
			if (other.caseComment != null)
				return false;
		} else if (!caseComment.equals(other.caseComment))
			return false;
		if (cases == null) {
			if (other.cases != null)
				return false;
		} else if (!cases.equals(other.cases))
			return false;
		if (clinical == null) {
			if (other.clinical != null)
				return false;
		} else if (!clinical.equals(other.clinical))
			return false;
		if (cumulative == null) {
			if (other.cumulative != null)
				return false;
		} else if (!cumulative.equals(other.cumulative))
			return false;
		if (currDueDate != other.currDueDate)
			return false;
		if (dateRequested != other.dateRequested)
			return false;
		if (drug == null) {
			if (other.drug != null)
				return false;
		} else if (!drug.equals(other.drug))
			return false;
		if (drugName == null) {
			if (other.drugName != null)
				return false;
		} else if (!drugName.equals(other.drugName))
			return false;
		if (drugs == null) {
			if (other.drugs != null)
				return false;
		} else if (!drugs.equals(other.drugs))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (expirydate != other.expirydate)
			return false;
		if (followUpNumber == null) {
			if (other.followUpNumber != null)
				return false;
		} else if (!followUpNumber.equals(other.followUpNumber))
			return false;
		if (initiationDate == null) {
			if (other.initiationDate != null)
				return false;
		} else if (!initiationDate.equals(other.initiationDate))
			return false;
		if (isAllDrug != other.isAllDrug)
			return false;
		if (isUrgent != other.isUrgent)
			return false;
		if (originalDueDate != other.originalDueDate)
			return false;
		if (others == null) {
			if (other.others != null)
				return false;
		} else if (!others.equals(other.others))
			return false;
		if (outputFormat == null) {
			if (other.outputFormat != null)
				return false;
		} else if (!outputFormat.equals(other.outputFormat))
			return false;
		if (outputFormatId != other.outputFormatId)
			return false;
		if (pregnancy == null) {
			if (other.pregnancy != null)
				return false;
		} else if (!pregnancy.equals(other.pregnancy))
			return false;
		if (prepComments == null) {
			if (other.prepComments != null)
				return false;
		} else if (!prepComments.equals(other.prepComments))
			return false;
		if (preparatory == null) {
			if (other.preparatory != null)
				return false;
		} else if (!preparatory.equals(other.preparatory))
			return false;
		if (queryDesc == null) {
			if (other.queryDesc != null)
				return false;
		} else if (!queryDesc.equals(other.queryDesc))
			return false;
		if (queryNumber == null) {
			if (other.queryNumber != null)
				return false;
		} else if (!queryNumber.equals(other.queryNumber))
			return false;
		if (queryNumberString == null) {
			if (other.queryNumberString != null)
				return false;
		} else if (!queryNumberString.equals(other.queryNumberString))
			return false;
		if (querySeq != other.querySeq)
			return false;
		if (querySummary == null) {
			if (other.querySummary != null)
				return false;
		} else if (!querySummary.equals(other.querySummary))
			return false;
		if (querySummaryUpper == null) {
			if (other.querySummaryUpper != null)
				return false;
		} else if (!querySummaryUpper.equals(other.querySummaryUpper))
			return false;
		if (queryType != other.queryType)
			return false;
		if (queryTypeDescription == null) {
			if (other.queryTypeDescription != null)
				return false;
		} else if (!queryTypeDescription.equals(other.queryTypeDescription))
			return false;
		if (reopen != other.reopen)
			return false;
		if (reporterType != other.reporterType)
			return false;
		if (reporterTypeDescription == null) {
			if (other.reporterTypeDescription != null)
				return false;
		} else if (!reporterTypeDescription
				.equals(other.reporterTypeDescription))
			return false;
		if (requester == null) {
			if (other.requester != null)
				return false;
		} else if (!requester.equals(other.requester))
			return false;
		if (requesterQueryLabel == null) {
			if (other.requesterQueryLabel != null)
				return false;
		} else if (!requesterQueryLabel.equals(other.requesterQueryLabel))
			return false;
		if (responseDate == null) {
			if (other.responseDate != null)
				return false;
		} else if (!responseDate.equals(other.responseDate))
			return false;
		if (sourceCountry == null) {
			if (other.sourceCountry != null)
				return false;
		} else if (!sourceCountry.equals(other.sourceCountry))
			return false;
		if (sourceCountryDescription == null) {
			if (other.sourceCountryDescription != null)
				return false;
		} else if (!sourceCountryDescription
				.equals(other.sourceCountryDescription))
			return false;
		if (specialRequest == null) {
			if (other.specialRequest != null)
				return false;
		} else if (!specialRequest.equals(other.specialRequest))
			return false;
		if (statusCode == null) {
			if (other.statusCode != null)
				return false;
		} else if (!statusCode.equals(other.statusCode))
			return false;
		if (urgency == null) {
			if (other.urgency != null)
				return false;
		} else if (!urgency.equals(other.urgency))
			return false;
		return true;
	}

	public PerformanceMetric getPerformanceMetric() {
		return performanceMetric;
	}

	public void setPerformanceMetric(PerformanceMetric performanceMetric) {
		this.performanceMetric = performanceMetric;
	}

	public Audit getAudit() {
		return audit;
	}

	public void setAudit(Audit audit) {
		this.audit = audit;
	}

	public Manufacturing getManufacturing() {
		return manufacturing;
	}

	public void setManufacturing(Manufacturing manufacturing) {
		this.manufacturing = manufacturing;
	}

	public DataSearch getDataSearch() {
		return dataSearch;
	}

	public void setDataSearch(DataSearch dataSearch) {
		this.dataSearch = dataSearch;
	}


}
