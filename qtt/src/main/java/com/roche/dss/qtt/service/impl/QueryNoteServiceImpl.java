package com.roche.dss.qtt.service.impl;

import com.roche.dss.qtt.eao.AffiliateNoteEAO;
import com.roche.dss.qtt.eao.NonAffiliateNoteEAO;
import com.roche.dss.qtt.model.AffiliateNote;
import com.roche.dss.qtt.model.NonAffiliateNote;
import com.roche.dss.qtt.service.QueryNoteService;
import com.roche.dss.qtt.service.QueryService;
import javax.annotation.Resource;

@Service("noteService")
public class QueryNoteServiceImpl implements QueryNoteService {
    private static final Logger logger = LoggerFactory.getLogger(QueryNoteServiceImpl.class);
    private NonAffiliateNoteEAO nonAffiliateNoteEAO;
    private AffiliateNoteEAO affiliateNoteEAO;
    private QueryService queryService;

    @Resource
    public void setNonAffiliateNoteEAO(NonAffiliateNoteEAO nonAffiliateNoteEAO) {
        this.nonAffiliateNoteEAO = nonAffiliateNoteEAO;
    }

    @Resource
    public void setAffiliateNoteEAO(AffiliateNoteEAO affiliateNoteEAO) {
        this.affiliateNoteEAO = affiliateNoteEAO;
    }

    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Override
	public void flush() {
        nonAffiliateNoteEAO.flush();
    }

    @Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String findContent(long id, boolean affiliate) {
        if (affiliate) {
            return affiliateNoteEAO.find(id).getText();
        } else {
            return nonAffiliateNoteEAO.find(id).getText();
        }
    }

    @Override
	@Transactional(propagation = Propagation.MANDATORY)
    public NonAffiliateNote mergeNonAffiliateNote(NonAffiliateNote nonAffiliateNote) {
        return nonAffiliateNoteEAO.merge(nonAffiliateNote);
    }

    @Override
	@Transactional(propagation = Propagation.MANDATORY)
    public AffiliateNote mergeAffiliateNote(AffiliateNote affiliateNote) {
        return affiliateNoteEAO.merge(affiliateNote);
    }

    @Override
	@Transactional(propagation = Propagation.MANDATORY)
    public void removeNonAffiliateNote(NonAffiliateNote nonAffiliateNote) {
        nonAffiliateNoteEAO.remove(nonAffiliateNote);
    }

    @Override
	@Transactional(propagation = Propagation.MANDATORY)
    public void removeAffiliateNote(AffiliateNote affiliateNote) {
        affiliateNoteEAO.remove(affiliateNote);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public NonAffiliateNote findNonAffiliateNote(long id) {
        return nonAffiliateNoteEAO.find(id);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public AffiliateNote findAffiliateNote(long id) {
        return affiliateNoteEAO.find(id);
    }

    @Override
	@Transactional(propagation = Propagation.REQUIRED)
    public void saveQueryNote(long noteId, long queryId, String content, boolean affiliate, String userName) {
        if (affiliate) {
            AffiliateNote affiliateNote = findAffiliateNote(noteId);
            if (affiliateNote == null) {
                affiliateNote = new AffiliateNote();
                affiliateNote.setQuerySeq(queryId);

            }
            affiliateNote.setCreateTs(new DateTime());
            affiliateNote.setUsername(userName);
            affiliateNote.setText(StringUtils.upperCase(content));
            mergeAffiliateNote(affiliateNote);
        } else {
            NonAffiliateNote nonAffiliateNote = findNonAffiliateNote(noteId);
            if (nonAffiliateNote == null) {
                nonAffiliateNote = new NonAffiliateNote();
                nonAffiliateNote.setQuerySeq(queryId);

            }
            nonAffiliateNote.setCreateTs(new DateTime());
            nonAffiliateNote.setUsername(userName);
            nonAffiliateNote.setText(StringUtils.upperCase(content));
            mergeNonAffiliateNote(nonAffiliateNote);
        }
        if(!affiliate){
            queryService.updateAlertFlag(queryId, true);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void removeQueryNote(long id, boolean affiliate) {
        logger.debug("removeQueryNote " + id);
        if (affiliate) {
            AffiliateNote affiliateNote = findAffiliateNote(id);
            removeAffiliateNote(affiliateNote);
        } else {
            NonAffiliateNote nonAffiliateNote = findNonAffiliateNote(id);
            removeNonAffiliateNote(nonAffiliateNote);
        }
    }

    public enum NoteType {
        RESPDATE("SYSTEM","[notification] Response Due Date was modified. Reason: "),
        DMGRESPDATE("SYSTEM", "[notification] DMG Response Due Date was modified. Reason: "),
        NEWATTACH("SYSTEM", "[notification] A new attachment was added.");

        private String author;
        private String message;

        private NoteType(String author, String message) {
            this.author = author;
            this.message = message;
        }

        public String getAuthor() {
            return author;
        }

        public String getMessage() {
            return message;
        }
    }
}
