package com.roche.dss.qtt.security.user;

import com.roche.dss.qtt.security.model.DSSUser;
import javax.naming.Name;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author zerkowsm
 *
 */
public class RocheUserDetailsImpl implements RocheUserDetails {

	private static final long serialVersionUID = 1847743230788652970L;
	
	private boolean enabled = true;
	private boolean accountNonExpired = true;;
	private boolean accountNonLocked = true;
	private boolean credentialsNonExpired = true;
    private Collection<GrantedAuthority> authorities = AuthorityUtils.NO_AUTHORITIES;

    private String username;
    private String password;
    private String displayName;
    private String location;
    private String department;
    private String firstName;
    private String surname;
    private String mail;
    private String country;
    private String domain;
    private String DN;
    private String phone;
    private String msExchHomeServerName;
    private String site;
    private String fax;
    private String countryCode;
    private Long id;

    public RocheUserDetailsImpl() {}

	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	public boolean isCredentialsNonExpired()
	{
		return credentialsNonExpired;
	}

	public boolean isAccountNonLocked()
	{
		return accountNonLocked;
	}

    @Override
	public String getDisplayName(){
        return displayName;
    }

    @Override
	public String getLocation(){
        return location;
    }

    @Override
	public String getDepartment(){
        return department;
    }

    @Override
	public String getFirstName(){
        return firstName;
    }

    @Override
	public String getSurname(){
        return surname;
    }

    @Override
	public String getMail(){
        return mail;
    }

    @Override
	public String getCountry() {
        return country;
    }

    @Override
	public String getDomain() {
        return domain;
    }

    @Override
	public String getDN() {
        return DN;
    }

    @Override
	public String getPhone() {
    	return phone;
    }

    @Override
    public String getFax() {
        return fax;
    }

    @Override
    public String getSite() {
        return site;
    }

    @Override
    public String getCountryCode() {
        return countryCode;
    }




    @Override
	public String getMsExchHomeServerName() {
		return msExchHomeServerName;
	}

    @Override
	public String toString() {
        return username;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public static class Essence {
        protected RocheUserDetailsImpl instance = createTarget();
        private List<GrantedAuthority> mutableAuthorities = new ArrayList<GrantedAuthority>();

        public Essence() { }

        public Essence(DirContextOperations ctx) {
            setDN(ctx.getDn());
        }

        public Essence(DSSUser copyMe) {
            setEnabled(true);
            setAccountNonExpired(true);
            setCredentialsNonExpired(true);
            setAccountNonLocked(true);
        	setUsername(copyMe.getUserName());
			setFirstName(copyMe.getFirstName());
			setSurname(copyMe.getLastName());
			setDepartment(copyMe.getUserDepartment());
			setCountry(copyMe.getCountry());
			setDisplayName(copyMe.getUserFullName());
			setMail(copyMe.getEmail());
            setPhone(copyMe.getPhoneNumber());
            setFax(copyMe.getFax());
            setCountryCode(copyMe.getCountryCode());
            setSite(copyMe.getSite());
            setId(copyMe.getEmployeeId());
        }



        protected RocheUserDetailsImpl createTarget() {
            return new RocheUserDetailsImpl();
        }

        public void addAuthority(GrantedAuthority a) {
            if(!hasAuthority(a)) {
                mutableAuthorities.add(a);
            }
        }

        private boolean hasAuthority(GrantedAuthority a) {
            for (GrantedAuthority authority : mutableAuthorities) {
                if(authority.equals(a)) {
                    return true;
                }
            }
            return false;
        }

        public RocheUserDetails createUserDetails() {
            instance.authorities = Collections.unmodifiableList(mutableAuthorities);
            RocheUserDetails newInstance = instance;
            instance = null;

            return newInstance;
        }

        public Collection<GrantedAuthority> getGrantedAuthorities() {
            return mutableAuthorities;
        }

        public void setAccountNonExpired(boolean accountNonExpired) {
            instance.accountNonExpired = accountNonExpired;
        }

        public void setAccountNonLocked(boolean accountNonLocked) {
            instance.accountNonLocked = accountNonLocked;
        }

        public void setAuthorities(Collection<GrantedAuthority> authorities) {
            mutableAuthorities = new ArrayList<GrantedAuthority>();
            mutableAuthorities.addAll(authorities);
        }

        public void setCredentialsNonExpired(boolean credentialsNonExpired) {
            instance.credentialsNonExpired = credentialsNonExpired;
        }

        public void setDN(String DN) {
            instance.DN = DN;
        }

        public void setDN(Name DN) {
            instance.DN = DN.toString();
        }

        public void setEnabled(boolean enabled) {
            instance.enabled = enabled;
        }

        public void setId(long employeeId) {
            instance.id = employeeId;
        }

        public void setPassword(String password) {
            instance.password = password;
        }

        public void setUsername(String username) {
            instance.username = username;
        }
        
        public void setFirstName(String firstName) {
        	instance.firstName = firstName;
        }
        
        public void setSurname(String surname) {
        	instance.surname = surname;
        }
        
        public void setDepartment(String department) {
        	instance.department = department;
        }
        
        public void setCountry(String country) {
        	instance.country = country;
        }
        
        public void setDisplayName(String displayName) {
        	instance.displayName = displayName;
        }
        
        public void setMail(String mail) {
        	instance.mail = mail;
        }

        public void setCountryCode(String countryCode) {
            instance.countryCode = countryCode;
        }


        public void setFax(String fax) {
            instance.fax = fax;
        }


        public void setSite(String site) {
            instance.site = site;
        }

        private void setPhone(String phoneNumber) {
            instance.phone = phoneNumber;
        }
    }

}