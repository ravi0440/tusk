package com.roche.dss.qtt.security.ldap.userdetails;

import java.util.Collection;

import com.roche.dss.qtt.security.model.DSSUser;

/**
 * @author zerkowsm
 *
 */
public interface AuthoritiesPopulator{
	
	Collection<GrantedAuthority> getGrantedAuthorities(DSSUser userData, String username);
}