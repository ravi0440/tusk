package com.roche.dss.qtt.query.web.qrf.model;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public class ProcessQRFActionModel {

    private String activity;
    private String id;
    private String next;

    public String getActivity() {
        return activity;
    }

    public String getId() {
        return id;
    }

    public String getNext() {
        return next;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public Object getMid() {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }
}
