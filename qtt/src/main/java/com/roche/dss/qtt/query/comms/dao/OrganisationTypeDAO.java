package com.roche.dss.qtt.query.comms.dao;

import com.roche.dss.qtt.query.comms.dto.OrganisationTypeDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface OrganisationTypeDAO {
    List<OrganisationTypeDTO> getOrganisationTypes();
}
