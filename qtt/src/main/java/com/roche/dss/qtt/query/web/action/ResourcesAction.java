package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.comms.dto.QueryAssessmentDTO;
import com.roche.dss.qtt.security.model.DSSUser;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.workflow.WorkflowService;
import com.roche.dss.qtt.utility.EmailSender;
import com.roche.dss.qtt.utility.config.Config;
import com.roche.dss.qtt.utility.pdf.QrfPdfInterface;
import com.roche.dss.util.comparator.ResourceListComparator;

import javax.annotation.Resource;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Action to retrieve and assign available resoources to task
 * <p/>
 * User: pruchnil
 */
public class ResourcesAction extends CommonActionSupport {
    private static final Logger logger = LoggerFactory.getLogger(ResourcesAction.class);
    private long queryId;
    private long taskId;
    private String taskName;
    private Query query;
    private List<DSSUser> resourceList = new ArrayList<DSSUser>();
    private String selectedUser;

    private QueryService queryService;
    private WorkflowService workflowService;
    private  TaskInstance task ;
    private QueryAssessmentDTO assessment;
    private String verifierForDMG;
	private String companyRiskLevel;
	private String programCodeCategory;
	private String complexQuery;
	private String highUserExperience;
	private String comment;
	private Date dmgResponseDueDate;
	 protected EmailSender emailSender;
	 protected Config config;
	 private QrfPdfInterface qrfPdf;
    
	 public Date getDmgResponseDueDate() {
			return dmgResponseDueDate;
		}

	    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
		public void setDmgResponseDueDate(Date dmgResponseDueDate) {
			this.dmgResponseDueDate = dmgResponseDueDate;
		}

	 @Resource
	    public void setQrfPdfInterface(QrfPdfInterface qrfPdf) {
	        this.qrfPdf = qrfPdf;
	    }
	 
	 @Resource
	    public void setEmailSender(EmailSender emailSender) {
	        this.emailSender = emailSender;
	    }
	 
    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Resource
    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }
    
    @Resource
    public void setConfig(Config config) {
        this.config = config;
    }
    

    @SuppressWarnings("unchecked")
    @SkipValidation
    @Override
    public String execute() throws Exception {
        if (!(isDscl() || isPS() || isDMGCoord())) {
            return DENIED;
        }
        

        query = queryService.find(queryId);
        task = workflowService.getTaskInstance(taskId);
        Map<String, Object> users;
        if (task.getToken().getNode().getName().startsWith("DMG")) {
        	
        	
            if ("DMG-Manual Assignment".equals(task.getToken().getNode().getName())) {
                users = securityService.getDSSUsers(QTT_DSS_APPLICATION_CODE, new String[]{DMG_COORD_PERMISSION});
            }
            else {
                users = securityService.getDSSUsers(QTT_DSS_APPLICATION_CODE, new String[]{DMG_SCI_PERMISSION});
            }
            
          //prefill assessment if is already at DB
            selectedUser=task.getActorId();
			verifierForDMG=query.getVerifyPerson();
			assessment=queryService.getQueryAssessment(query.getQuerySeq(),query.getFollowupNumber());
			if(assessment!=null){
				companyRiskLevel=assessment.getCompanyRiskLevel();
				programCodeCategory=assessment.getProgramCodeCategory();
				complexQuery=assessment.getComplexQuery();
				highUserExperience=assessment.getHighUserExperienceAndKnowledge();
				comment = assessment.getComment();
			}
            
        } else if (task.getToken().getNode().getName().startsWith("MI")) {
            users = securityService.getDSSUsers(QTT_DSS_APPLICATION_CODE, new String[]{PS_PERMISSION});
        } else {
            users = securityService.getDSSUsers(QTT_DSS_APPLICATION_CODE, new String[]{DSCL_PERMISSION});
        }
        if (MapUtils.isNotEmpty(users)) {
            resourceList = (ArrayList<DSSUser>) users.get(USER_SERVICE_GET_USERS_OUT_PARAM);
            Collections.sort(resourceList, new ResourceListComparator());
        }
        
        
        

        return SUCCESS;
    }

    @Override
    public void validate() {
    	if(query==null){
    		query = queryService.find(queryId);
    	}
    	if(task==null){
    		   task = workflowService.getTaskInstance(taskId);
    	}
    	if (task.getToken().getNode().getName().startsWith("DMG")) {
        try {
        	
        	if(isPeerReviewRequired()&&verifierForDMG==null){
        		addActionError(getText("dmg.dataSearch.QueryAssessment.notCorrectOwner"));
        	}else if(isPeerReviewRequired()&&verifierForDMG.equals(selectedUser)){
        		addActionError(getText("dmg.dataSearch.QueryAssessment.notCorrectVerifier"));
        	}
        	
        	if(companyRiskLevel==null||programCodeCategory==null||complexQuery==null||highUserExperience==null){
        		addActionError(getText("dmg.dataSearch.QueryAssessment.required"));
        	}
        	
            if(Boolean.FALSE.toString().equals(getDMGResponseDueDateNotRequired()) && (dmgResponseDueDate == null && !getFieldErrors().containsKey("DMGResponseDueDate"))) {
            	addActionError(getText("dmg.dataSearch.DMGResponseDueDate.required"));
            } else {
            	if(WF_ROUTE_B.equals(query.getWorkflowRouteType()) && dmgResponseDueDate.after(query.getCurrentDueDate().toDate())) {
            		addActionError(getText("dmg.dataSearch.DMGResponseDueDate.later", new String[]{query.getCurrentDueDateFormatted()}));
            	}
            }
        } catch (IllegalArgumentException e) {
            addActionError(getText("dmg.dataSearch.DMGResponseDueDate.format"));
        }
    	}
        super.validate();
    }
    
    public String getDMGResponseDueDateNotRequired() {
		return WF_ROUTE_B.equals(query.getWorkflowRouteType()) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
	}
    
    public Boolean isPeerReviewRequired(){
    	
    	//every 20 query should have verifier anyway
    			if(query.getQuerySeq()%20 == 0){
    				return true;
    			}
    	
		if (companyRiskLevel!=null&&companyRiskLevel.equals("HIGH")) {
			return true;
		} else if (companyRiskLevel!=null&&companyRiskLevel.equals("MEDIUM")) {
				
				if(programCodeCategory!=null&&programCodeCategory.equals("NEW")){
					if( highUserExperience!=null&&highUserExperience.equals("NO")){
						return true;
					}else{
						return false;
					}
				}else if(programCodeCategory!=null&&programCodeCategory.equals("CHANGED")){
						if(complexQuery!=null&&complexQuery.equals("YES")&& highUserExperience!=null&& highUserExperience.equals("NO")){
							return true;
						}else{
							return false;
						}
				}else if (programCodeCategory!=null&&programCodeCategory.equals("UNCHANGED")){
						return false;
				}
				
		} else if (companyRiskLevel!=null&&companyRiskLevel.equals("LOW")&&programCodeCategory!=null&&programCodeCategory.equals("NEW")&&complexQuery!=null&&complexQuery.equals("YES")&& highUserExperience!=null&&highUserExperience.equals("NO")) {
	         return true;
		}
		return false;
	}
    
    
    public void sendEmailAfterQueryAssessment(Query query, String to) {
        // send email
        String from = "";
        String subject = "";
        StringBuilder body = new StringBuilder();
        ByteArrayOutputStream out = qrfPdf.qrfBuilder(Long.valueOf(query.getQuerySeq()).intValue(), "Accepted");
        String attachName = "QRF-" + query.getQueryNumber() + ".pdf";
        from = config.getInititateDsclEmail();
        String queryLabel = "";
        if (!StringUtils.isEmpty(query.getRequesterQueryLabel())) {
            queryLabel = " - [" + query.getRequesterQueryLabel() + "]";
        }
        subject = "Query " + query.getQueryNumber() + " "
                + config.getInititateAcceptEmailSubject() + queryLabel;

        body.append("\n");
        body.append(config.getInititateAcceptBody1Para());
        body.append("\n\n");
        body.append(config.getInititateAcceptBody2Para());
        body.append(" ");
        body.append(query.getQueryNumber());
        body.append(".");
        body.append("\n");
        body.append(config.getInititateAcceptBody3Para()); 
        body.append("\n\n");
 		body.append(config.getInititateAcceptBody4Para());
 		body.append(" ");
 		body.append(DateTimeFormat.forPattern("dd-MMM-yyyy").print(query.getDateRequested()));
 		body.append("\n");
 		body.append(config.getInititateAcceptBodyLastPara());


 		
 		
        try {
            emailSender.sendMessage(from, to.toString(),"", subject, body.toString(), attachName, out);
            logger.info("WorkflowProcesAction:sending email to:" + to + " query no:" + query.getQueryNumber());
        } catch (Exception e) {
            logger.error("Error WorkflowProcesAction:sending:sending email " + to + " query no:" + query.getQueryNumber(), e);
        }
    }
    
    public String assign() {
        if (!(isDscl() || isPS() || isDMGCoord())) {
            return DENIED;
        }
        DateTime dmgDueDate = null;
        if (task.getToken().getNode().getName().startsWith("DMG")) {
        	
        	if(getDMGResponseDueDateNotRequired().equals(Boolean.FALSE.toString())) {
    			dmgDueDate = new DateTime(dmgResponseDueDate);
    		}
    		
    		
    		QueryAssessmentDTO dto= new QueryAssessmentDTO();
    		dto.setCompanyRiskLevel(companyRiskLevel);
    		dto.setProgramCodeCategory(programCodeCategory);
    		dto.setComplexQuery(complexQuery);
    		dto.setHighUserExperienceAndKnowledge(highUserExperience);
    		dto.setComment(comment);
    		dto.setChangedBy(getLoggedUserName());
    		
    		assessment=queryService.getQueryAssessment(query.getQuerySeq(),query.getFollowupNumber());
    		if(assessment!=null){
    			queryService.updateQueryAssessment(dto, query.getQuerySeq(),query.getFollowupNumber());
    		}else{
    			queryService.addQueryAssessment(dto, query.getQuerySeq(),query.getFollowupNumber());
    		}
    		
    		
    		
    		if(isPeerReviewRequired()){
    			queryService.assignVerifyPerson(verifierForDMG, query.getQuerySeq());
    		
    			//find user for string
    			DSSUser verifier =null;
    			for(DSSUser element: resourceList){
    				if(element.getUserName().equals(verifierForDMG)){
    					verifier=element;
    					break;
    				}
    			}
    			// send mail to verifier
    			sendEmailAfterQueryAssessment(query, verifier.getEmail());
    		}else{
    			queryService.cleanVerifyPerson(query.getQuerySeq());
    		}
    		
    		workflowService.performResourceReassignment(taskId, selectedUser);
			
			//find user for string
			DSSUser dmgUser =null;
			for(DSSUser element: resourceList){
				if(element.getUserName().equals(selectedUser)){
					dmgUser=element;
					break;
				}
			}
			// send mail to assigned DMG user
			sendEmailAfterQueryAssessment(query, dmgUser.getEmail());
        }else{
        	 logger.debug("taskId " + taskId + " selectedUser " + selectedUser);
             workflowService.performResourceReassignment(taskId, selectedUser);
        }
       
        return SUCCESS;
    }
    
    public Boolean isDMGActivity(){
    	 return task.getToken().getNode().getName().startsWith("DMG");
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public String getSelectedUser() {
        return selectedUser;
    }

    @RequiredStringValidator(type = ValidatorType.FIELD, key = "mi.assignment.resource.required")
    public void setSelectedUser(String selectedUser) {
        this.selectedUser = selectedUser;
    }

    public void setResourceList(List<DSSUser> resourceList) {
        this.resourceList = resourceList;
    }

    public List<DSSUser> getResourceList() {
        return resourceList;
    }

	public QueryAssessmentDTO getAssessment() {
		return assessment;
	}

	public void setAssessment(QueryAssessmentDTO assessment) {
		this.assessment = assessment;
	}
	
    public String getVerifierForDMG() {
		return verifierForDMG;
	}

	public void setVerifierForDMG(String verifierForDMG) {
		this.verifierForDMG = verifierForDMG;
	}

	

	public String getCompanyRiskLevel() {
		return companyRiskLevel;
	}

	public void setCompanyRiskLevel(String companyRiskLevel) {
		this.companyRiskLevel = companyRiskLevel;
	}

	public String getProgramCodeCategory() {
		return programCodeCategory;
	}

	public void setProgramCodeCategory(String programCodeCategory) {
		this.programCodeCategory = programCodeCategory;
	}

	public String getComplexQuery() {
		return complexQuery;
	}

	public void setComplexQuery(String complexQuery) {
		this.complexQuery = complexQuery;
	}

	public String getHighUserExperience() {
		return highUserExperience;
	}

	public void setHighUserExperience(String highUserExperience) {
		this.highUserExperience = highUserExperience;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
    
    

}
