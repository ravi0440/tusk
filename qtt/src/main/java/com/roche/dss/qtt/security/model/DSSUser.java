package com.roche.dss.qtt.security.model;

/**
 * @author zerkowsm
 *
 */
public class DSSUser {
	
	private static final long serialVersionUID = -4491584906597632364L;
	
	private Long employeeId;
	private String userName;
	private String title;
	private String firstName;
	private String lastName;
	private String userFullName;
	private String email;
	private String phoneNumber;
	private String fax;
	private String companyCode;
	private String company;
	private String businessUnitCode;
	private String businessUnit;
	private String siteCode;
	private String site;
	private String siteTypeCode;
	private String status;
	private String mailStop;
	private String countryCode;
	private String country;
	private String userDepartment = "";
	
	private String isTeamLead;
	
	public String getBusinessUnit() {
		return businessUnit;
	}

	public String getCompany() {
		return company;
	}

	public String getEmail() {
		return email;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public String getFax() {
		return fax;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getSite() {
		return site;
	}

	public String getStatus() {
		return status;
	}

	public String getTitle() {
		return title;
	}

	public String getUserFullName() {
		return userFullName;
	}

	public String getUserName() {
		return userName;
	}

	public void setBusinessUnit(String string) {
		businessUnit = string;
	}

	public void setCompany(String string) {
		company = string;
	}

	public void setEmail(String string) {
		email = string;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public void setFax(String string) {
		fax = string;
	}

	public void setFirstName(String string) {
		firstName = string;
	}

	public void setLastName(String string) {
		lastName = string;
	}

	public void setPhoneNumber(String string) {
		phoneNumber = string;
	}

	public void setSite(String string) {
		site = string;
	}

	public void setStatus(String string) {
		status = string;
	}

	public void setTitle(String string) {
		title = string;
	}

	public void setUserFullName(String string) {
		userFullName = string;
	}

	public void setUserName(String string) {
		userName = string;
	}

    public String getBusinessUnitCode() {
        return businessUnitCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public String getSiteCode() {
        return siteCode;
    }

    public String getSiteTypeCode() {
        return siteTypeCode;
    }

    public void setBusinessUnitCode(String string) {
        businessUnitCode = string;
    }

    public void setCompanyCode(String string) {
        companyCode = string;
    }

    public void setSiteCode(String string) {
        siteCode = string;
    }

    public void setSiteTypeCode(String string) {
        siteTypeCode = string;
    }

    public String getMailStop() {
        return mailStop;
    }

    public void setMailStop(String string) {
        mailStop = string;
    }

    public String getCountry() {
        return country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountry(String string) {
        country = string;
    }

    public void setCountryCode(String string) {
        countryCode = string;
    }

    public String getIsTeamLead() {
        return isTeamLead;
    }

    public void setIsTeamLead(String string) {
        isTeamLead = string;
    }

    public String getUserDepartment() {
        return userDepartment;
    }

    public void setUserDepartment(String string) {
        userDepartment = string;
    }

}
