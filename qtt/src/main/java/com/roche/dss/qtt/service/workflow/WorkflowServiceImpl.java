package com.roche.dss.qtt.service.workflow;

import com.roche.dss.qtt.QttGlobalConstants;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;
import com.roche.dss.qtt.security.user.RocheUserDetails;
import com.roche.dss.qtt.service.FileOperationsService;
import com.roche.dss.qtt.service.InitiateService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.workflow.jbpm.JbpmService;
import com.roche.dss.qtt.utility.Activities;
import com.roche.dss.qtt.utility.Transitions;
import com.roche.dss.util.comparator.QuerySummaryNumberComparator;
import com.roche.dss.util.comparator.TaskEndDateComparator;
import javax.annotation.Resource;
import java.io.File;
import java.util.*;

import static com.roche.dss.qtt.utility.Activities.UNKNOWN;

/**
 * @author zerkowsm
 */
@Service("workflowService")
public class WorkflowServiceImpl implements WorkflowService {

    private static final Logger logger = LoggerFactory.getLogger(WorkflowServiceImpl.class);

    private static final Map<String, String> leavingTransisionMapper = new HashMap<String, String>();

    static {
        leavingTransisionMapper.put(MI_CLARIFICATION_ACTIVITY, Transitions.SEEK_CLARIFICATION.getTransitionName());
        leavingTransisionMapper.put(MI_REVIEW_ACTIVITY, Transitions.REQUEST_REVIEW.getTransitionName());
        leavingTransisionMapper.put(DMG_CLARIFICATION_ACTIVITY, Transitions.SEEK_CLARIFICATION.getTransitionName());
        leavingTransisionMapper.put(DMG_REVIEW_ACTIVITY, Transitions.REQUEST_REVIEW.getTransitionName());
        leavingTransisionMapper.put(DMG_MI_COMPLETE_ACTIVITY, Transitions.COMPLETE_MI_AND_DMG_SEARCH.getTransitionName());
        leavingTransisionMapper.put(AWAITING_FOLLOW_UP_COMPLETE_ACTIVITY, Transitions.TO_QUERY_ASSIGNMENT.getTransitionName());
        leavingTransisionMapper.put(MI_ASSISTANCE_ACTIVITY, Transitions.REQUEST_DMG_DATA_SEARCH.getTransitionName());
    }

    private JbpmService jbpmService;

    private QueryService queryService;

    @Autowired
    private FileOperationsService fileOpsService;

    @Autowired
    private InitiateService initiateService;


    @Resource
    public void setJbpmService(JbpmService jbpmService) {
        this.jbpmService = jbpmService;
    }

    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Transactional
    @Override
    public void initiateWorkflow(Query query, String userName) {
        ProcessInstance processInstance = jbpmService.createProcessInstance(QTT_WORKFLOW_PROCESS_NAME, query);
        jbpmService.startProcessInstance(processInstance.getId(), userName);
    }

    @Transactional
    @Override
    public void selectWorkflowRoute(long queryId, String transitionName, String userName, String queryLabel) {
        Query query = queryService.find(queryId);
        queryService.updateQueryLabelRoute(queryId, queryLabel, transitionName);
        jbpmService.signal(userName, query, transitionName);
        if(WF_ROUTE_A1.equals(transitionName)) {
        	jbpmService.assignTask(query, null);
        } else if(WF_ROUTE_A2.equals(transitionName)) {
        	jbpmService.assignTask(query, userName, DMG_ASSIGNED_ACTOR_ID);
        } else {
            jbpmService.assignTask(query, userName);
        }
    }

    @Transactional
    @Override
    public void lockTask(String userName, long taskId) {
        jbpmService.lockTask(userName, taskId);
    }

    @Transactional
    @Override
    public void completeActivity(String userName, long taskId, String nextActivity) {
        jbpmService.signal(userName, taskId, nextActivity != null ? leavingTransisionMapper.get(nextActivity) : null);
    }

    @Transactional
    @Override
    public void performMIResourceAssignment(Query query, String userName, long taskId, String nextUserName, String nextUserEmail) {
        Map<String, Object> processInstanceVariables = new HashMap<String, Object>();
        processInstanceVariables.put(PS_ASSIGNED_ACTOR_ID, nextUserName);
        processInstanceVariables.put(PS_ASSIGNED_ACTOR_EMAIL, nextUserEmail);
        jbpmService.signal(userName, taskId, null, query.getProcessInstanceId(), processInstanceVariables);
    }

    @Transactional
    @Override
    public void performDMGResourceAssignment(Query query, String userName, long taskId, String nextUserName, DateTime dmgDueDate, boolean affiliate) {
        if (dmgDueDate != null) {
            queryService.updateDMGResponseDate(query.getQuerySeq(), dmgDueDate, QttGlobalConstants.EMPTY_STRING, affiliate,userName);
        }
        Map<String, Object> processInstanceVariables = new HashMap<String, Object>();
        processInstanceVariables.put(DMG_ASSIGNED_ACTOR_ID, nextUserName);
        jbpmService.signal(userName, taskId, null, query.getProcessInstanceId(), processInstanceVariables);
    }

    @Transactional
    @Override
    public void performResourceReassignment(long taskId, String nextUserName) {
    	TaskInstance taskInstance = jbpmService.getTaskInstance(taskId);
    	Map<String, Object> processInstanceVariables = new HashMap<String, Object>();
		if (taskInstance.getToken().getNode().getName().startsWith("DMG")) {
	        processInstanceVariables.put(DMG_ASSIGNED_ACTOR_ID, nextUserName);
			jbpmService.assignTask(taskId, nextUserName, processInstanceVariables);
		} else if (taskInstance.getToken().getNode().getName().startsWith("MI")) {
	        processInstanceVariables.put(PS_ASSIGNED_ACTOR_ID, nextUserName);
			jbpmService.assignTask(taskId, nextUserName, processInstanceVariables);
		} else {
			jbpmService.assignTask(taskId, nextUserName);
		}
    }

    @Transactional
    @Override
    public void updateSummaryResponseAndComplete(boolean isUpdate, String leavingTransition, String userName, long taskId, long queryId, DateTime expiryDate, String summary, boolean isAffiliateVisibility, boolean ldocUpdate, String oldLabellingDocId, String newLabellingDocId) {
        queryService.updateSummaryResponseAppend(queryId, expiryDate, summary, isAffiliateVisibility, ldocUpdate, oldLabellingDocId, newLabellingDocId);
        if (!isUpdate) {
            jbpmService.signal(userName, taskId, leavingTransition);
        }
    }

    @Transactional
    @Override
    public void sendFinalResponse(String userName, Query query) {
        jbpmService.signal(userName, query, null);
    }

    @Transactional
    @Override
    public void createFollowup(long queryId, String userName, long taskId) {
    	jbpmService.lockTask(userName, taskId);
    	//TODO: execute proper method from service created for followup operations - in previous version it does not work
    	Query query = queryService.find(queryId);
        Map<String, Object> processInstanceVariables = new HashMap<String, Object>();
        processInstanceVariables.put(PROCESS_INSTANCE_START_ACTOR_ID, userName);
    	jbpmService.signal(userName, taskId, Transitions.TO_QUERY_ASSIGNMENT.getTransitionName(), query.getProcessInstanceId(), processInstanceVariables);
    }

    @Transactional
    @Override
    public long restartProcess(Long processInstanceId, String userName) {
        return jbpmService.restart(processInstanceId, userName);
    }

    @Transactional
    @Override
    public TaskInstance getTaskInstance(long taskInstanceId) {
        return jbpmService.getTaskInstance(taskInstanceId);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<QuerySummaryDTO> retrieveSummaryList(boolean affiliate, RocheUserDetails loggedUser, boolean dscl, boolean ps, boolean dmg, boolean pds, boolean dmgsc) {
        List<QuerySummaryDTO> taskListSummaries = new ArrayList<QuerySummaryDTO>();
        List<Query> queryList;
        // TODO this action is not allowed for affiliate - this check may be unneccesary
        if (affiliate) {
            queryList = queryService.findOpenQueryListByCountry(loggedUser.getCountryCode());
        } else {
            queryList = queryService.findOpenQueryList();
        }
      //  boolean isInPSGroup = ps || pds;
        boolean isInDMGGroup = dmg || dmgsc;
        for (Query query : queryList) {
			if (query.getProcessInstanceId() != null) {
				List<TaskInstance> taskList = new ArrayList<TaskInstance>(jbpmService.retrieveProcessActiveTaskList(query.getProcessInstanceId()));
				if (CollectionUtils.isNotEmpty(taskList)) {
					for (TaskInstance task : taskList) {
                        if((dscl) || (isInDMGGroup && ArrayUtils.contains(UNKNOWN.getDmgActivities(), task.getName())) || (ps && ArrayUtils.contains(UNKNOWN.getBWorkflow(), task.getName()))){
                            QuerySummaryDTO summaryDetails = new QuerySummaryDTO(query, task);
                            summaryDetails.setDisplayUserTasks(ps && summaryDetails.isAssigned());
                            summaryDetails.setDisplayManualReassign((dscl || dmg || ps) && summaryDetails.isAssigned());
                            taskListSummaries.add(summaryDetails);
                        }
					}
				}
			}
		}
        return taskListSummaries;
    }

    @Transactional
    @Override
    public List<QuerySummaryDTO> retrieveUserTaskList(RocheUserDetails user, boolean styleCheck) {
        List<QuerySummaryDTO> taskListSummaries = new ArrayList<QuerySummaryDTO>();
        List<String> rolesAndActors = new ArrayList<String>();

        rolesAndActors.add(user.getUsername());

        for (GrantedAuthority role : user.getAuthorities()) {
            rolesAndActors.add(role.getAuthority().substring(ROLE_PREFIX.length()));
        }

        List<TaskInstance> tasks = jbpmService.retrieveUserTaskList(rolesAndActors);

        if (tasks != null && !tasks.isEmpty()) {
            for (TaskInstance task : tasks) {
                Query query = queryService.find(Long.parseLong(task.getProcessInstance().getKey()));
                boolean finalDocExists = queryService.isFinalDocAttachedInCurrentInteraction(query.getQuerySeq(), query.getProcessInstanceId());
                QuerySummaryDTO summaryDetails = new QuerySummaryDTO(query, task, finalDocExists);
                taskListSummaries.add(summaryDetails);
            }
        }
        return taskListSummaries;
    }

    @Transactional
    @Override
    public List<QuerySummaryDTO> retrieveUserTaskList(String userName) {

        List<QuerySummaryDTO> taskListSummaries = new ArrayList<QuerySummaryDTO>();

        List<TaskInstance> tasks = jbpmService.retrieveUserTaskList(userName);

        if (tasks != null && !tasks.isEmpty()) {
            for (TaskInstance task : tasks) {
                Query query = queryService.find(Long.parseLong(task.getProcessInstance().getKey()));
                QuerySummaryDTO summaryDetails = new QuerySummaryDTO(query, task);
                taskListSummaries.add(summaryDetails);
            }
            Collections.sort(taskListSummaries, new QuerySummaryNumberComparator());

        }
        return taskListSummaries;
    }

    @Transactional
    @Override
    public List<TaskInstance> retrieveProcessTaskList(String querySeq) {

        List<TaskInstance> endTaskList = new ArrayList<TaskInstance>();

        Set<TaskInstance> tasks = new HashSet<TaskInstance>();
        Collection<ProcessInstance> processInstances = jbpmService.getProcessInstancesByKey(WorkflowGlobalConstants.QTT_WORKFLOW_PROCESS_NAME, querySeq);
        
        for (ProcessInstance processInstance : processInstances) {
        	Set<TaskInstance> processInstanceTasks = (Set<TaskInstance>) jbpmService.retrieveProcessTaskList(processInstance.getId());
        	if(processInstanceTasks!=null){
        		tasks.addAll(processInstanceTasks);
        	}
		}
        
//        ProcessInstance processInstance = jbpmService.getProcessInstanceById(processInstanceId);

//        endTaskList.add(getJobStartTask(processInstance));

        if (tasks != null && !tasks.isEmpty()) {
            for (TaskInstance task : tasks) {
            	if(Activities.MI_REVIEW.getTaskName().equals(task.getName())) {
            		//task.setName(Activities.MI_REVIEW.getTaskDisplayName()); TODO: does not work properly and do we really need this?? check it like bellow
            	} else if(Activities.AWAITING_FOLLOWUP.getTaskName().equals(task.getName())) {
            		//task.setName(Activities.AWAITING_FOLLOWUP.getTaskDisplayName()); TODO: doble check it  if it is really needed.
            	}
                if (task.getEnd() != null) {
                	if(!(Activities.AWAITING_FOLLOWUP.getTaskName().equals(task.getName()) && task.getActorId().equals("DSCL"))) {
                		endTaskList.add(task);
                	}
                }
            }
            Collections.sort(endTaskList, new TaskEndDateComparator());
            Collections.reverse(endTaskList);
        }
        return endTaskList;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteQueryAndProcess(final Long querySeq) {
        // first delete the attachment
        final String dir = fileOpsService.findDestinationDirectory(String.valueOf(querySeq));
        boolean successful = false;
        try {
            successful = fileOpsService.deleteDir(new File(dir));
        } catch (Exception e) {
            logger.error("deleting dir error", e);
        }
        if (successful) {
            final  Query query = queryService.find(Long.valueOf(querySeq));
            if (query != null) {
                final Long processInstanceId = query.getProcessInstanceId();
                initiateService.deleteQuery(querySeq, query.getQueryType().getQueryTypeCode());
                if (processInstanceId != null) {
                      jbpmService.deleteProcessInstance(processInstanceId);
                }

            }
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteQueryProcess(final Long querySeq) {
        final Query query = queryService.find(Long.valueOf(querySeq));
        if (query != null) {
            final Long processInstanceId = query.getProcessInstanceId();
            query.setProcessInstanceId(null);
            if (processInstanceId != null) {
                jbpmService.deleteProcessInstance(processInstanceId);
            }
        }
    }

    private TaskInstance getJobStartTask(ProcessInstance processInstance) {
        TaskInstance taskInstance = new TaskInstance(Activities.JOB_START.getTaskName());
        taskInstance.setStart(processInstance.getStart());
        taskInstance.setEnd(processInstance.getStart());
        taskInstance.setActorId(processInstance.getContextInstance().getVariable(PROCESS_INSTANCE_START_ACTOR_ID).toString());
        return taskInstance;
    }

	@Override
    @Transactional(propagation = Propagation.REQUIRED)
	public void endQueryProcess(final Long querySeq) {
        final Query query = queryService.find(Long.valueOf(querySeq));
        if (query != null) {
            final Long processInstanceId = query.getProcessInstanceId();
            query.setProcessInstanceId(null);
            if (processInstanceId != null) {
                jbpmService.endProcessInstance(processInstanceId);
            }
        }
	}
    
    

}