package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.NonRocheUserEAO;
import com.roche.dss.qtt.model.NonRocheUser;

@Repository("nonRocheUserEAO")
public class NonRocheUserEAOImpl extends AbstractBaseEAO<NonRocheUser> implements NonRocheUserEAO {

    @Override
    public NonRocheUser checkNonRocheUser(String login, String password) {
        NonRocheUser user = null;
        try {
            user = getEntityManager().createNamedQuery("nonRocheUser.checkUser", NonRocheUser.class).setParameter("login", login).setParameter("password", password).getSingleResult();
        } catch (Exception ex) {
            return null;
        }
        return user;
    }
}
