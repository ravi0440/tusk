package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.ClinicalTrial;

public interface ClinicalTrialEAO extends QueryRelatedEAO<ClinicalTrial>{
	
}