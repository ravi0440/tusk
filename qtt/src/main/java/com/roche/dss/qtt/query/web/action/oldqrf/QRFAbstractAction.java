package com.roche.dss.qtt.query.web.action.oldqrf;

import com.roche.dss.qtt.eao.QttCountryEAO;
import com.roche.dss.qtt.eao.QttDrugEAO;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.action.CommonActionSupport;
import com.roche.dss.qtt.query.web.utils.UserDetailsAware;
import com.roche.dss.qtt.security.model.DSSPermission;
import com.roche.dss.qtt.security.user.RocheUserDetails;
import com.roche.dss.qtt.service.DictionaryService;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public abstract class QRFAbstractAction extends CommonActionSupport implements ModelDriven, UserDetailsAware, SessionAware, RequestAware {

    private Map<String,Object> session;
    private RocheUserDetails userDetails;
    private Map<String,Object> request;
	@Autowired
	protected QttDrugEAO qttDrugEAO;
	@Autowired
	protected QttCountryEAO qttCountryEAO;
	@Autowired
	protected DictionaryService dictionaryService;
	
	public DictionaryService getDictionary () {
		return dictionaryService;
	}

    @Override
    public void setSession(Map<String, Object> session) {
       this.session = session;
    }

    @Override
    public void setUserDetails(RocheUserDetails userDetails) {
        this.userDetails = userDetails;
    }

    @Override
    public void setRequest(Map<String, Object> request) {
        this.request = request;
    }

    public Map<String, Object> getSession() {
        return session;
    }

    public RocheUserDetails getUserDetails() {
        return userDetails;
    }

    public Map<String, Object> getRequest() {
        return request;
    }
    
     protected void cleanUpSession() {

        if (this.session.get(ActionConstants.FRM_QRF) != null)
            this.session.put((ActionConstants.FRM_QRF), null);

        if (this.session.get(ActionConstants.FRM_QUERY_DESC) != null)
            this.session.put((ActionConstants.FRM_QUERY_DESC), null);

        if (this.session.get(ActionConstants.FRM_CASE) != null)
            this.session.put((ActionConstants.FRM_CASE), null);

        if (this.session.get(ActionConstants.FRM_DRUG) != null)
            this.session.put((ActionConstants.FRM_DRUG), null);

        if (this.session.get(ActionConstants.FRM_CLINICAL) != null)
            this.session.put((ActionConstants.FRM_CLINICAL), null);

        if (this.session.get(ActionConstants.FRM_QUERY_LABEL) != null)
            this.session.put((ActionConstants.FRM_QUERY_LABEL), null);

        if (this.session.get(ActionConstants.QRF_QUERY_ID) != null)
            this.session.put((ActionConstants.QRF_QUERY_ID), null);

        if (this.session.get(ActionConstants.QRF_AE_TERM) != null)
            this.session.put((ActionConstants.QRF_AE_TERM), null);

        if (this.session.get(ActionConstants.FRM_IND) != null)
            this.session.put((ActionConstants.FRM_IND), null);
        
        if (this.session.get(ActionConstants.DRUG_EVT_QUERY_DESC) != null)
            this.session.put((ActionConstants.DRUG_EVT_QUERY_DESC), null);
        
        if (this.session.get(ActionConstants.DRUG_INT_DRUG) != null)
            this.session.put((ActionConstants.DRUG_INT_DRUG), null);
        
        if (this.session.get("partialQueriesList") != null)
            this.session.put(("partialQueriesList"), null);

        if (this.session.get(ActionConstants.QRF_EDIT_QUERY_TYPE) != null)
            this.session.put((ActionConstants.QRF_EDIT_QUERY_TYPE), null);

        if (this.session.get(ActionConstants.FRM_QRF_BACK) != null)
            this.session.put((ActionConstants.FRM_QRF_BACK), null);
        
        if (this.session.get(QUERYLIST_SUMMARY) != null)
            this.session.put((QUERYLIST_SUMMARY), null);

        if (this.session.get(RESPONSE_DETAILS) != null)
            this.session.put((RESPONSE_DETAILS), null);

        if (this.session.get(RESPONSE_DISPLAY_EMAIL) != null)
            this.session.put((RESPONSE_DISPLAY_EMAIL), null);
        
        if(this.session.get(TASKLIST_DISPLAY_BEAN) != null)
            this.session.put((TASKLIST_DISPLAY_BEAN), null);

        if(this.session.get(SEARCH_RESULTS_INDEX) != null)
            this.session.put((SEARCH_RESULTS_INDEX), null);
        if(this.session.get(ActionConstants.FRM_SEARCH_CRITERIA) != null)
            this.session.put((ActionConstants.FRM_SEARCH_CRITERIA), null);

         if (this.session.get(ActionConstants.QRF_AE_TERM) != null)
             this.session.put((ActionConstants.QRF_AE_TERM), null);

         if (this.session.get(ActionConstants.FRM_COMM) != null)
             this.session.put((ActionConstants.FRM_COMM), null);
    }
     
    public List<String> getDrugRetrievalNames() {
    	return qttDrugEAO.getRetrievalNames();
	}

	public List<String> getDrugGenericNames() {
		return qttDrugEAO.getGenericNames();
	}
	//Query List session names
    public final static String QUERYLIST_PREFIX              = "querylist.";
    public final static String QUERYLIST_SUMMARY     = QUERYLIST_PREFIX + "details.summary";


   // Response list session names
    //
    public final static String RESPONSE_PREFIX          = "response.";
    public final static String RESPONSE_DETAILS         = RESPONSE_PREFIX + "data.details";
    public final static String RESPONSE_DISPLAY_EMAIL   = RESPONSE_PREFIX + "display.email";



	// Task-list session names
	//
	public final static String TASKLIST_PREFIX				= "tasklist.";

    // public final static String TASKLIST_DISPLAY_PSST        = TASKLIST_PREFIX + "display.PSST";

    public final static String TASKLIST_DISPLAY_BEAN        = TASKLIST_PREFIX + "display.details";

    	// Repository Search session names
	//
	public final static String SEARCH_PREFIX				= "search.";
    public final static String SEARCH_RESULTS_INDEX = SEARCH_PREFIX + "results.index";


   	// Permission definitions
	//
	public final static DSSPermission DSCL_PERMISSION = new DSSPermission( "DSCL", "DSCL" );
	public final static DSSPermission DMG_COORD_PERMISSION = new DSSPermission( "DMGCO", "DMG Coordinator" );
	public final static DSSPermission DMG_SCI_PERMISSION = new DSSPermission( "DMGSC", "DMG Scientist" );
	public final static DSSPermission PS_PERMISSION = new DSSPermission( "PS", "Product Specialist" );
	public final static DSSPermission PDS_USER_PERMISSION = new DSSPermission( "PDMS", "PDMS Manager" );
	public final static DSSPermission AFFILIATE_PERMISSION = new DSSPermission( "AFFIL", "Affiliate user" );
	public final static DSSPermission DSS_USER_PERMISSION = new DSSPermission( "DSS User permission", "DSS User permission" );
}
