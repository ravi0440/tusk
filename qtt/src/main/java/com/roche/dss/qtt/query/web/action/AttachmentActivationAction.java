package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.service.AttachmentService;
import com.roche.dss.qtt.service.QueryService;
import javax.annotation.Resource;


/**
 * This action enables to activate/deactivate attachment
 * <p/>
 * User: pruchnil
 */
public class AttachmentActivationAction extends CommonActionSupport {
    private static final Logger logger = LoggerFactory.getLogger(AttachmentActivationAction.class);
    private Query query;
    private long queryId;
    private long id;
    private QueryService queryService;
    private AttachmentService attachmentService;

    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Resource
    public void setAttachmentService(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    @Override
    public String execute() throws Exception {
        query = queryService.getAttachmentActivationList(queryId, isDscl(), getLoggedUserName());
        return SUCCESS;
    }

    public String activate() throws Exception {
        attachmentService.changeActivationFlag(id, queryId, true);
        return SUCCESS;
    }

    public String deactivate() throws Exception {
        attachmentService.changeActivationFlag(id, queryId, false);
        return SUCCESS;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Query getQuery() {
        return query;
    }

}
