package com.roche.dss.qtt.eao;

import java.util.Collection;
import java.util.List;

/**
 * @author zerkowsm
 *
 */
public interface BaseEAO<T> {

    void persist(T entity);

    void persist(Collection<T> entities);
    
    T merge(T entity);
    
    void remove(T entity);
    
    void flush();

    void clear();

    T find(Long id);

    T getReference(Long id);

    T get(Long id);

    List<T> getAll();

    T getByIdObj(Object id);
}