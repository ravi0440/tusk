package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.QueryWorkflowHistory;

public interface QueryWorkflowHistoryEAO extends BaseEAO<QueryWorkflowHistory>{
	
	public void save(QueryWorkflowHistory entity);
}