package com.roche.dss.qtt.query.web.menu;

import com.roche.dss.qtt.QttGlobalConstants;
import com.roche.dss.qtt.security.service.SecurityService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zerkowsm
 * 
 */
public class MenuServiceImpl implements MenuService, QttGlobalConstants {
    private SecurityService securityService;

    @Resource
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

	@Override
	public List<Menu> getMenuList() {
		List<Menu> menuItems = new ArrayList<Menu>();

		boolean dsclAllowed = securityService.isAllowedCheck(DSCL_PERMISSION);
		boolean dmgCoordAllowed = securityService.isAllowedCheck(DMG_COORD_PERMISSION);
		boolean dmgSciAllowed = securityService.isAllowedCheck(DMG_SCI_PERMISSION);
		boolean psAllowed = securityService.isAllowedCheck(PS_PERMISSION);
		boolean affAllowed = securityService.isAllowedCheck(AFFILIATE_PERMISSION);
		boolean pdsAllowed = securityService.isAllowedCheck(PDS_USER_PERMISSION);

		if(dsclAllowed || dmgCoordAllowed || dmgSciAllowed || psAllowed) {
			menuItems.add(new Menu(MenuItem.HOME.getValue(), MenuItem.HOME.getLink()));
		}

        menuItems.add(new Menu(MenuItem.QRF.getValue(), MenuItem.QRF.getLink()));

        if(!affAllowed){
            menuItems.add(new Menu(MenuItem.QUERY_LIST.getValue(), MenuItem.QUERY_LIST.getLink()));
        }
		
		if(dsclAllowed) {
			menuItems.add(new Menu(MenuItem.QUERY_LIST_UESERS.getValue(), MenuItem.QUERY_LIST_UESERS.getLink()));
            menuItems.add(new Menu(MenuItem.INITIATE.getValue(), MenuItem.INITIATE.getLink()));
		}

		if(dsclAllowed || dmgCoordAllowed || dmgSciAllowed || psAllowed || affAllowed) {
			menuItems.add(new Menu(MenuItem.SEARCH.getValue(), MenuItem.SEARCH.getLink()));
			menuItems.add(new Menu(MenuItem.QUICK_SEARCH.getValue(), MenuItem.QUICK_SEARCH.getLink()));
		}
		
		if(dsclAllowed || dmgCoordAllowed || pdsAllowed) {
			menuItems.add(new Menu(MenuItem.REPORTS.getValue(), MenuItem.REPORTS.getLink()));
		}
		
		menuItems.add(new Menu(MenuItem.HELP.getValue(), MenuItem.HELP.getLink()));
		menuItems.add(new Menu(MenuItem.ABOUT.getValue(), MenuItem.ABOUT.getLink()));
		
		return menuItems;
	}
	
}