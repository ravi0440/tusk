/* 
====================================================================
  $Header$
  @author $Author: komisarp $
  @version $Revision: 2453 $ $Date: 2012-06-27 16:07:46 +0200 (Śr, 27 cze 2012) $

====================================================================
*/
package com.roche.dss.qtt.query.comms.dto;


public class PreparatoryDTO{
	
	private int preparatoryId;
	private String preparatoryName;
	private String others;

    /**
     * @return
     */
    public int getPreparatoryId() {
        return preparatoryId;
    }

    /**
     * @return
     */
    public String getPreparatoryName() {
        return preparatoryName;
    }

    /**
     * @param i
     */
    public void setPreparatoryId(int i) {
        preparatoryId = i;
    }

    /**
     * @param string
     */
    public void setPreparatoryName(String string) {
        preparatoryName = string;
    }

    /**
     * @return
     */
    public String getOthers() {
        return others;
    }

    /**
     * @param string
     */
    public void setOthers(String string) {
        others = string;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((others == null) ? 0 : others.hashCode());
		result = prime * result + preparatoryId;
		result = prime * result
				+ ((preparatoryName == null) ? 0 : preparatoryName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if(!(obj instanceof PreparatoryDTO))
			return false;
		PreparatoryDTO other = (PreparatoryDTO) obj;
		if (others == null) {
			if (other.others != null)
				return false;
		} else if (!others.equals(other.others))
			return false;
		if (preparatoryId != other.preparatoryId)
			return false;
		if (preparatoryName == null) {
			if (other.preparatoryName != null)
				return false;
		} else if (!preparatoryName.equals(other.preparatoryName))
			return false;
		return true;
	}

}
