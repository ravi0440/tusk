package com.roche.dss.qtt.query.web.action.oldqrf;

import com.roche.dss.qtt.query.comms.dao.DictionaryDAO;
import java.util.List;

public class DrugSupplyAction extends ActionSupport {

    @Autowired
    private DictionaryDAO dictionary;

    public String execute() {
        return Action.SUCCESS;
    }

    public List<String> getDrugs() {
         return dictionary.getUsedGenericNames();
    }
}
