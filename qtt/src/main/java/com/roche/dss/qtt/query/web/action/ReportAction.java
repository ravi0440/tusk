package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.query.comms.dto.ReportDTO;
import com.roche.dss.qtt.query.web.model.ReportCriteria;
import com.roche.dss.qtt.query.web.model.StreamResultBean;
import com.roche.dss.qtt.service.ReportService;
import com.roche.dss.qtt.utility.WorkflowRoute;
import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * This Action class retrieves report data and allows to render it on JSP page
 *
 * @author pruchnil
 */
public class ReportAction extends CommonActionSupport {

	private static final long serialVersionUID = -5191244045109741282L;
	
	private static final Logger logger = LoggerFactory.getLogger(ReportAction.class);
    public static final String RESULT_FILE_NAME = "report.csv";
    private ReportCriteria reportCriteria = new ReportCriteria();
    private List<ReportDTO> reportList;
    private StreamResultBean resultsExportBean;

    private ReportService reportService;

    @Resource
    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    @Override
    public String execute() throws Exception {
        return Action.SUCCESS;
    }

    public String createReport() throws Exception {
        reportList = reportService.retrieveReportData(reportCriteria);
        return SUCCESS;
    }

    public String export() throws Exception {
        resultsExportBean = new StreamResultBean();
        resultsExportBean.setContentType("application/csv");
        resultsExportBean.setContentDisposition("attachment; filename=" + RESULT_FILE_NAME);
        resultsExportBean.setInputStream(new ByteArrayInputStream(reportService.exportToCSV(reportCriteria).toByteArray()));
        return SUCCESS;
    }

    @Override
    public void validate() {
        if (reportCriteria.getDateFrom() != null && reportCriteria.getDateTo() != null) {
            if (reportCriteria.getDateFrom().after(reportCriteria.getDateTo())) {
                addActionError(getText("date.order"));
            }
        }
        super.validate();
    }

    public List<ReportDTO> getReportList() {
        return reportList;
    }

    public ReportCriteria getReportCriteria() {
        return reportCriteria;
    }

    public void setReportCriteria(ReportCriteria reportCriteria) {
        this.reportCriteria = reportCriteria;
    }

    public List<WorkflowRoute> getRoutes() {
        return Arrays.asList(WorkflowRoute.values());
    }

    public Map<String, Object> getSession() {
        return getContext().getSession();
    }

    public InputStream getInputStream() {
        return resultsExportBean.getInputStream();
    }

    public String getContentType() {
        return resultsExportBean.getContentType();
    }

    public String getContentDisposition() {
        return resultsExportBean.getContentDisposition();
    }

}
