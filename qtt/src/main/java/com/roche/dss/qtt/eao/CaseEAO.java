package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.Case;

import java.util.List;

public interface CaseEAO extends QueryRelatedEAO<Case>{

    List<Case> findAllByQueryId(long queryId);
}