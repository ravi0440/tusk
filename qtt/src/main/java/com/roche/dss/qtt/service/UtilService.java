package com.roche.dss.qtt.service;

import java.io.IOException;

import com.roche.dss.qtt.model.Attachment;

public interface UtilService {
	public static final int STAGE_UNKNOWN = -1;
	
	public static final int STAGE_INIT = 0;
	
	public static final int STAGE_INDEX = 1;
	
	public static final int STAGE_FINAL = 2;
	
	public static final int STAGE_INDEX_SINGLE = 3;
	
	public static final int STAGE_PURGE_INDEX = 4;
	
	public static final int STAGE_OPTIMIZE = 5;
	
	public static final int STAGE_PURGE_WHOLE_CACHE = 6;
	
	public void cleanSecondLevelCache(boolean skipExtractedTexts);
	
	public void reindexAll(Class entityClass, int stage, int startIndex, int count) throws InterruptedException;
	
	public String getHighlightedText(Attachment attF, String quickSearchKeywords
			, Highlighter highlighter, Analyzer analyzer, String textToHighlight) throws IOException, InvalidTokenOffsetsException;
}
