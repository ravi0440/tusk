package com.roche.dss.qtt.model;
// Generated 2010-10-04 12:00:38 by Hibernate Tools 3.3.0.GA

/**
 * FuAeTerm generated by hbm2java
 */
@Entity
@Table(name = "FU_AE_TERMS")
public class FuAeTerm implements java.io.Serializable {

	private static final long serialVersionUID = -8395435258920330368L;
	
	private FuAeTermId id;
	private String description;

	public FuAeTerm() {
	}

	public FuAeTerm(FuAeTermId id) {
		this.id = id;
	}

	public FuAeTerm(FuAeTermId id, String description) {
		this.id = id;
		this.description = description;
	}

    public FuAeTerm(AeTerm aeTerm, long followupSeq) {
        this.id = new FuAeTermId(followupSeq, aeTerm.getQuerySeq());
		this.description = aeTerm.getDescription();
    }

    @EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "followupSeq", column = @Column(name = "FOLLOWUP_SEQ", nullable = false, precision = 10, scale = 0)),
			@AttributeOverride(name = "querySeq", column = @Column(name = "QUERY_SEQ", nullable = false, precision = 10, scale = 0)) })
	public FuAeTermId getId() {
		return this.id;
	}

	public void setId(FuAeTermId id) {
		this.id = id;
	}

	@Column(name = "DESCRIPTION", length = 600)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
