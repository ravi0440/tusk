package com.roche.dss.qtt.utility;

import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;

import java.util.List;

/**
 * Created by farynam on 15/05/17.
 */
public abstract class StyleManager {

    public static void assignStyle(List<QuerySummaryDTO> taskList, boolean styleCheck) {
        for (QuerySummaryDTO summaryDetails : taskList) {
            if (styleCheck) {
                /*if (summaryDetails.getQuery().getDmgDueDate() != null) {
                    summaryDetails.setCssStyle(summaryDetails.getQuery().getDmgDueDate());
                } else {
                    summaryDetails.setCssStyle(summaryDetails.getQuery().getCurrentDueDate());
                }
            } else {*/
                if (summaryDetails.getQuery() != null && summaryDetails.getQuery().getCurrentDueDate() != null) {
                    summaryDetails.setCssStyle(summaryDetails.getQuery().getCurrentDueDate());
                }
            }
        }
    }

}
