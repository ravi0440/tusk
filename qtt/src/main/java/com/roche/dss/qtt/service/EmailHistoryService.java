package com.roche.dss.qtt.service;


public interface EmailHistoryService {
     void saveEmailHistory(String subject, String content, String to, String from);
}
