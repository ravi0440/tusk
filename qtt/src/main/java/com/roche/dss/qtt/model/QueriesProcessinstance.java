package com.roche.dss.qtt.model;

import com.roche.dss.util.JodaTimeBridge;
import java.util.Date;

@Entity
@Table(name = "QUERIES_PROCESSINSTANCE")
public class QueriesProcessinstance implements java.io.Serializable {

    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    @SequenceGenerator(name = "QUERIES_PROCESSINSTANCE_GENERATOR", sequenceName = "QUERIES_PROCESSINSTANCE_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QUERIES_PROCESSINSTANCE_GENERATOR")
    private Long id;

    @Column(name = "PROCESSINTANCE_ID", precision = 10, scale = 0)
    private Long processInstanceId;

    @Column(name = "QUERY_SEQ", precision = 10, scale = 0)
    private Long querySeq;

    @Column(name = "FOLLOWUP_SEQ", precision = 10, scale = 0)
    private Long followupSeq;

    @Type(type="com.roche.dss.util.PersistentDateTime")
    @Column(name = "CREATION_TS", nullable = false, length = 7)
    private DateTime creationTs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(Long processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public Long getQuerySeq() {
        return querySeq;
    }

    public void setQuerySeq(Long querySeq) {
        this.querySeq = querySeq;
    }

    public Long getFollowupSeq() {
        return followupSeq;
    }

    public void setFollowupSeq(Long followupSeq) {
        this.followupSeq = followupSeq;
    }

    public DateTime getCreationTs() {
        return creationTs;
    }

    public void setCreationTs(DateTime creationTs) {
        this.creationTs = creationTs;
    }
}
