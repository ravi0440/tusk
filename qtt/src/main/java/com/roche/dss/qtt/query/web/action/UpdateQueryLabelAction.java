package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.service.QueryService;
import javax.annotation.Resource;

/**
 * This action enables to change query label
 *
 * User: pruchnil
 */
public class UpdateQueryLabelAction extends CommonActionSupport {
    private static final Logger logger = LoggerFactory.getLogger(UpdateQueryLabelAction.class);
    private long queryId;
    private long taskId;
    private Query query;
    private String queryLabel;
    private String sourceSearch;

    private QueryService queryService;

    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Override
    @SkipValidation
    public String execute() throws Exception {
        if (!isDscl()) {
            return DENIED;
        }
        query = queryService.find(queryId);
        queryLabel = query.getQueryLabel();
        return Action.SUCCESS;
    }

    public String update() throws Exception {
        if (!isDscl()) {
            return DENIED;
        }
        queryService.updateQueryLabelRoute(queryId, queryLabel, null);
        return UPDATE;
    }

    @Override
    public void onActionErrors() {
        if(queryId>0){
            query = queryService.find(queryId);
        }
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public Query getQuery() {
        return query;
    }

    public String getQueryLabel() {
        return queryLabel;
    }

    @RequiredStringValidator(type = ValidatorType.FIELD, key = "label.required")
    public void setQueryLabel(String queryLabel) {
        this.queryLabel = queryLabel;
    }
    
    public String getSourceSearch() {
		return sourceSearch;
	}

	public void setSourceSearch(String sourceSearch) {
		this.sourceSearch = sourceSearch;
	}
}
