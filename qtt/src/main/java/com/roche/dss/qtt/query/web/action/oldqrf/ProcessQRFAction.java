/* 
 ====================================================================
 $Header$
 @author $Author: savovp $
 @version $Revision: 2558 $ $Date: 2012-10-31 16:46:56 +0100 (Śr, 31 paź 2012) $

 ====================================================================
 */
package com.roche.dss.qtt.query.web.action.oldqrf;

import java.util.Date;
import java.util.List;

import com.roche.dss.qtt.eao.EuRegulatoryHistoryEAO;
import com.roche.dss.qtt.eao.QueryStatusCodeEAO;
import com.roche.dss.qtt.eao.QueryWorkflowHistoryEAO;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryWorkflowHistory;
import com.roche.dss.qtt.query.comms.dto.QueryDTO;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.model.ProcessQRFActionModel;
import com.roche.dss.qtt.service.FollowupQueryService;
import com.roche.dss.qtt.service.QrfService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.workflow.WorkflowService;
import com.roche.dss.qtt.utility.EmailSender;
import com.roche.dss.qtt.utility.QueryStatusCodes;
import com.roche.dss.qtt.utility.config.Config;

public class ProcessQRFAction extends QRFAbstractAction {
    private static final Logger logger = LoggerFactory.getLogger(ProcessQRFAction.class);
    @Autowired
	private QrfService qrfService;
    private ProcessQRFActionModel model = new ProcessQRFActionModel();

    @Autowired
    protected EmailSender emailSender;

    @Autowired
    private QueryService queryService;

    @Autowired
    private QueryStatusCodeEAO qscEAO;

    @Autowired
    private FollowupQueryService followupQueryService;
    
    @Autowired
    private QueryWorkflowHistoryEAO queryWorkflowHistoryEAO;
    
	@Autowired
	protected Config config;

    @Autowired
    private WorkflowService workflowService;


    public String execute() {

        boolean isDSCL = isDscl();


        // note: no security check as this page can be accessed by any user
        
        if ((model.getActivity() != null) && (model.getId() != null)) {
            String action = model.getActivity();

            if (action.equals("sav")) {
                logDebug("ProcessQRFAction:saved");                
                return "saveSuccess";
            }
            String queryIdString = model.getId();
            int queryId = new Integer(queryIdString).intValue();
            // fetch query detsils for this query id
            //QrfFacadeBD qrf = (QrfFacadeBD) BusinessDelegateFactory.getFactory().getServiceBD(mapping);
            QueryDTO dto = qrfService.openQuery(queryId, null, false);
            
            if (action.equals("sub")) {
                qrfService.updateQRFStatus(queryId, ActionConstants.QRF_STATUS_SUBMITTED);
                return "submitSuccess";
            }
            //only for DSCL
            if (action.equals("amd")) { 
                
                // perform safety-check to see if query is still in state 'NSUB'
                //or the the user id DSCL
                //if the user is DSCL and the query is being processed then delete the job
                if(isDSCL && queryService.checkHasStatus(queryId, QueryStatusCodes.PROCESSING.getCode())){
                    workflowService.endQueryProcess(Long.valueOf(queryId));
                    logInfo("ProcessQRFAction: Query:" +dto.getQueryNumberString() + " has been amended and the job has been deleted by user:" + getUserDetails().getUsername());
                }
                
                getRequest().put(ActionConstants.QRF_QUERY_ID, queryIdString);
                
                Query query = queryService.find(Long.valueOf(queryId));
                
                QueryWorkflowHistory queryWorkflowHistory = new QueryWorkflowHistory();
                queryWorkflowHistory.setEndDate(new DateTime());
                queryWorkflowHistory.setModifiedBy(getLoggedUserName());
                queryWorkflowHistory.setWorkflowId(String.valueOf(queryId));
                queryWorkflowHistory.setStatus(query.getQueryStatusCode().getStatusCodeLower());
                queryWorkflowHistoryEAO.save(queryWorkflowHistory);
                               
                if("email".equals(model.getNext())){
                    query.setQueryStatusCode(qscEAO.getByIdObj(ActionConstants.QRF_STATUS_AMENDED));
                    query = queryService.merge(query);
                    //
                    
                    QueryWorkflowHistory queryWorkflowHistory2 = new QueryWorkflowHistory();
                    queryWorkflowHistory2.setStartDate(new DateTime());
                    queryWorkflowHistory2.setModifiedBy(getLoggedUserName());
                    queryWorkflowHistory2.setWorkflowId(String.valueOf(queryId));
                    queryWorkflowHistory2.setStatus("WORKFLOW_AMEND_EMAIL");
                    queryWorkflowHistoryEAO.save(queryWorkflowHistory2);
                    return "toEmail";
                }
                qrfService.updateQRFStatus(queryId, ActionConstants.QRF_STATUS_NOT_SUBMITTED);
                
                
                QueryWorkflowHistory queryWorkflowHistory2 = new QueryWorkflowHistory();
                queryWorkflowHistory2.setStartDate(new DateTime());
                queryWorkflowHistory2.setModifiedBy(getLoggedUserName());
                queryWorkflowHistory2.setWorkflowId(String.valueOf(queryId));
                queryWorkflowHistory2.setStatus("WORKFLOW_AMEND");
                queryWorkflowHistoryEAO.save(queryWorkflowHistory2);
                
                return "amendToQrf";                    
            }            
            if (action.equals("del")) {
                boolean isToEmail = false;
                // perform safety-check to see if query is still in state 'NSUB'
                //or the the user id DSCL
                //if the user is DSCL and the query is being processed then delete the job
                if(isDSCL && queryService.checkHasStatus(queryId, QueryStatusCodes.DRAFT.getCode())){
                    // retrieve user information
                    if (model.getNext() != null) {
                        if(model.getNext().equals("email")){
                            getRequest().put(ActionConstants.QRF_QUERY_ID, queryIdString);
                            getRequest().put("type", "rej");
                            isToEmail = true;
                        }
                    }
                }
                if (queryService.checkHasStatus(queryId, QueryStatusCodes.DRAFT.getCode()) || (isDSCL)) {
                   workflowService.deleteQueryAndProcess(Long.valueOf(queryId));
                } else {
                    // if query has already been accepted / rejected / amended,
                    // show "please refresh list" message
                    return "alreadyProcessed";
                }
                String user = null;
                if (getLoggedUser()!=null) {
                    user = getLoggedUser().getUsername();
                } else if(getSession().get("email")!=null) {
                    user = (String) getSession().get("email");
                }
                logInfo("ProcessQRFAction: Query:" +dto.getQueryNumberString() + " has been deleted by user: " + user);
                if (isToEmail) {
                    return "toEmail";
                }
                return "deleteSuccess";
            }
            
            if (action.equals("reo")) { 
                
                getRequest().put(ActionConstants.QRF_QUERY_ID, queryIdString);
                
                // **********************************************************************
                // HR 2008.10.13
                // CRF
                // if a query is reopened by somebody outside DSCL - send an email to DSCL
                if (!isDSCL)
                	sendMesssage(dto);
                // **********************************************************************

                followupQueryService.createFollowup(queryId);
                qrfService.updateQRFStatus(queryId, ActionConstants.QRF_STATUS_REOPENED);
                queryService.cleanVerifyPerson(queryId);
                getRequest().put("activity", "reo");
                return "amendToQrf";                    
            }            
            
        }
        return "failure";
    }

    private void logInfo(String s) {
        logger.info(s);
    }

    private void logDebug(String s) {
        logger.debug(s);
    }

    private void logError(String s, Exception e) {
        logger.error(s, e);
    }
    private void sendMesssage(QueryDTO queryDTO){

    	String from = getUserDetails() != null ? getUserDetails().getMail() : (String) getSession().get("email");
    	String surname = getUserDetails() != null ? getUserDetails().getSurname() : (String) getSession().get("surname");
    	String firstName = getUserDetails() != null ? getUserDetails().getFirstName() : (String) getSession().get("givenName");

    	String subject = config.getSubject()
										+ queryDTO.getQueryNumber()
										+ " ";
    	StringBuilder body = new StringBuilder(config.getBody());
    	body.append(" ");
		body.append(surname); 
    	body.append(" ");
		body.append(firstName); 
    	body.append(" ");
    	
    	try {
    		emailSender.sendMessage(from, config.getTo(), subject, body.toString());
            logInfo("ProcessQRFAction:sending email to:" + config.getTo() + " query no:" + queryDTO.getQueryNumberString());
        } catch (Exception e) {
            logError("Error ProcessQRFAction:sending email " + config.getTo() + " query no:" + queryDTO.getQueryNumberString(), e);
        }
    }

	@Override
    public Object getModel() {
        return model;
    }
}
