package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.EuRegulatoryHistoryEAO;
import com.roche.dss.qtt.model.EuRegulatoryHistory;

@Repository("euRegulatoryHistoryEAO")
public class EuRegulatoryHistoryEAOImpl extends AbstractBaseEAO<EuRegulatoryHistory> implements EuRegulatoryHistoryEAO {

}