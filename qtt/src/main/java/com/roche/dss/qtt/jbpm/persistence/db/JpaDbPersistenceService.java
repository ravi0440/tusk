package com.roche.dss.qtt.jbpm.persistence.db;

/**
 * @author zerkowsm
 *
 */
public class JpaDbPersistenceService extends DbPersistenceService {

	private static final long serialVersionUID = 5887131588230044215L;

	public JpaDbPersistenceService(DbPersistenceServiceFactory persistenceServiceFactory) {
		super(persistenceServiceFactory);
	}
	
}