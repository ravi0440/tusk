package com.roche.dss.qtt.security.utils.storeprocedures.permission;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.roche.dss.qtt.security.model.DSSPermission;

/**
 * @author zerkowsm
 *
 */
public class DSSUserPermissionMapper implements RowMapper<DSSPermission>{

	@Override
	public DSSPermission mapRow(ResultSet rs, int rowNum) throws SQLException {
		DSSPermission permission = new DSSPermission(rs.getString("PERMISSION_CODE"), rs.getString("DESCRIPTION"));
		return permission;
	}	
	
}