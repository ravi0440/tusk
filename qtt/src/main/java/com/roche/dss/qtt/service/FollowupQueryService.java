package com.roche.dss.qtt.service;

import com.roche.dss.qtt.model.FuQuery;
import com.roche.dss.qtt.model.LdocType;
import com.roche.dss.qtt.model.PreQueryPreparation;

import java.util.Set;

/**
 * User: pruchnil
 */
public interface FollowupQueryService {
    FuQuery getFollowupQueryDetails(long queryId, long followupId);

    void addFuQuery(long queryId, String followupNumber, long followupSeq);

    void addFuDrug(long queryId, long followupSeq);

    void addFuLabellingDocument(long queryId, Set<LdocType> labellingDocs, long followupSeq);

    void addFuRetrievalPeriod(long queryId, long followupSeq);

    void addFuPreQueryPreparation(long queryId, Set<PreQueryPreparation> preQueryPreparations, long followupSeq);

    void addFuPregnancy(long queryId, long followupSeq);

    void addFuClinicalTrial(long queryId, long followupSeq);

    void addFuCase(long queryId, long followupSeq);

    void addFuAeTerm(long queryId, long followupSeq);

    void addFuQueryDescription(long queryId, long followupSeq);

    void addFuPerformanceMetric(long queryId, long followupSeq);

    void addFuRequester(long queryId, long followupSeq);

    void createFollowup(int queryId);
}
