package com.roche.dss.qtt.service;

import com.roche.dss.qtt.model.LdocType;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryAssessment;
import com.roche.dss.qtt.model.QueryVerification;
import com.roche.dss.qtt.query.comms.dto.QueryAssessmentDTO;
import com.roche.dss.qtt.query.comms.dto.QueryListDTO;
import com.roche.dss.qtt.query.comms.dto.QueryVerificationDTO;

import java.util.List;
import java.util.Set;

/**
 * This service is responsible for actions related to query
 * User: pruchnil
 */
public interface QueryService {
    List<QueryListDTO> getQueryList(String email, String column, String order);

    Query getPreviewQueryDetails(long querySeq);    
    
    Query getClosedQueryDetails(long querySeq, boolean affiliate);

    void updateQueryLabelRoute(long queryId, String queryLabel, String route);

    void updateQueryExpiryDate(long queryId, DateTime expiryDate, boolean affiliateAccess);
    
    public Set<LdocType> getLabellingDocTypes(long queryId);
    
    public void updateSummaryResponseAppend(long queryId, DateTime expiryDate, String summary, boolean affiliateAccess, boolean ldocUpdate, String oldLabellingDocId, String newLabellingDocId);

    void setAccessKey(long queryId, String nextID);

    Query getAttachmentActivationList(long queryId, boolean dscl, String loggedUserName);

    boolean isFinalDocExists(long queryId, Long processInstanceId);

    boolean checkHasStatus(long queryId, String status);

    Query changeUrgencyFlag(long queryId);

    void updateAlertFlag(long queryId, boolean b);

    List<Query> findOpenQueryListByCountry(String countryCode);

    List<Query> findOpenQueryList();

    void updateResponseDate(long queryId, DateTime expiryDate, String explanation, boolean affiliate, String author);

    void updateDMGResponseDate(long queryId, DateTime expiryDate, String explanation, boolean affiliate,String author);
    
    public QueryAssessmentDTO getQueryAssessment(long queryId, String followupNumber);
    public void addQueryAssessment(QueryAssessmentDTO assessment, long queryId,String followupNumber);
    public void updateQueryAssessment(QueryAssessmentDTO assessment, long queryId,String followupNumber);
   
    
    public QueryVerificationDTO getQueryVerification(QueryVerification.RECORD_TYPE type, long queryId, String followupNumber);
    public void addQueryVerification(QueryVerificationDTO queryVerification, long queryId,String followupNumber);
    public void updateQueryVerification(QueryVerificationDTO queryVerification, long queryId,String followupNumber);
    public void removeQueryVerification(QueryVerification.RECORD_TYPE type, long queryId,String followupNumber);
    
    public void assignVerifyPerson(String person, long queryId);
    public void cleanVerifyPerson(long queryId);
    public void setInformEUQPPV(boolean flag, long queryId);
    
    List<Query> getInitiateList();
        
    List<Query> findClosedQueryList();

    void persist(Query entity);
    Query merge(Query entity);
    void remove(Query entity);
    void flush();
    Query find(Long id);

    boolean checkHaLegalNoDocs(long queryId);

	boolean isFinalDocAttachedInCurrentInteraction(long queryId, Long processInstanceId);

    String findActorIdFromDmgOrMiTask(Query query);

    String findActorIdFromMiTask(Query query);

    boolean checkIfQueryRequestedDmgDataSearch(Query query);
}
