package com.roche.dss.qtt.security.model;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */

import java.util.Hashtable;
import java.util.Iterator;


public class DSSUserPermissions  {

    private DSSUser user;
    private Hashtable permissions;

    public DSSUserPermissions() {
    }

    public DSSUser getUser() {
        return user;
    }

    public void setUser(DSSUser user) {
        this.user = user;
    }

    public Iterator getPermissions() {
        return (Iterator) permissions.elements();
    }

    public void setPermissions(Hashtable permissions) {
        this.permissions = permissions;
    }

    public boolean checkPermission(DSSPermission permission) {
        if (permissions == null)
            return false;
        else
            return permissions.containsKey(permission.getPermissionCode());
    }
}
