package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.AttachmentFromDmgSearchEAO;
import com.roche.dss.qtt.model.AttachmentFromDmgSearch;
import java.util.List;

@Repository("attachmentFromDmgSearchEAO")
public class AttachmentFromDmgSearchEAOImpl extends AbstractQueryRelatedEAO<AttachmentFromDmgSearch> implements AttachmentFromDmgSearchEAO {
    @Override
    public List<AttachmentFromDmgSearch> getAttachmentsForDMGSearch(long taskInstanceId) {
        try {
            return getEntityManager().createNamedQuery("attachment.findAttachmentsByTaskInstance", AttachmentFromDmgSearch.class).setParameter("taskInstanceId", taskInstanceId).getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
