package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.AuditEAO;
import com.roche.dss.qtt.model.Audit;

@Repository("externalAudit")
public class AuditEAOImpl extends AbstractQueryRelatedEAO<Audit> implements AuditEAO {
}
