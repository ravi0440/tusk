package com.roche.dss.qtt.model;
// Generated 2010-10-04 12:00:38 by Hibernate Tools 3.3.0.GA

/**
 * FuRetrievalPeriod generated by hbm2java
 */
@Entity
@Table(name = "FU_RETRIEVAL_PERIODS")
public class FuRetrievalPeriod implements java.io.Serializable {

	private static final long serialVersionUID = -1962630598102952269L;
	
	private FuRetrievalPeriodId id;
	private String cumulativeFlag;
	private DateTime startTs;
	private DateTime endTs;
	private String interval;

	public FuRetrievalPeriod() {
	}

	public FuRetrievalPeriod(FuRetrievalPeriodId id, String cumulativeFlag) {
		this.id = id;
		this.cumulativeFlag = cumulativeFlag;
	}

    public FuRetrievalPeriod(RetrievalPeriod retrievalPeriod, long followupSeq) {
        this.id = new FuRetrievalPeriodId(followupSeq, retrievalPeriod.getQuerySeq());
		this.cumulativeFlag = retrievalPeriod.getCumulativeFlag();
		this.startTs = retrievalPeriod.getStartTs();
		this.endTs = retrievalPeriod.getEndTs();
		this.interval = retrievalPeriod.getInterval();
    }

    @EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "followupSeq", column = @Column(name = "FOLLOWUP_SEQ", nullable = false, precision = 10, scale = 0)),
			@AttributeOverride(name = "querySeq", column = @Column(name = "QUERY_SEQ", nullable = false, precision = 10, scale = 0)) })
	public FuRetrievalPeriodId getId() {
		return this.id;
	}

	public void setId(FuRetrievalPeriodId id) {
		this.id = id;
	}

	@Column(name = "CUMULATIVE_FLAG", nullable = false, length = 1)
	public String getCumulativeFlag() {
		return this.cumulativeFlag;
	}

	public void setCumulativeFlag(String cumulativeFlag) {
		this.cumulativeFlag = cumulativeFlag;
	}

	@Type(type="com.roche.dss.util.PersistentDateTime")
	@Column(name = "START_TS", length = 7)
	public DateTime getStartTs() {
		return this.startTs;
	}

	public void setStartTs(DateTime startTs) {
		this.startTs = startTs;
	}

	@Type(type="com.roche.dss.util.PersistentDateTime")
	@Column(name = "END_TS", length = 7)
	public DateTime getEndTs() {
		return this.endTs;
	}

	public void setEndTs(DateTime endTs) {
		this.endTs = endTs;
	}

	@Column(name = "INTERVAL", length = 60)
	public String getInterval() {
		return this.interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

}
