package com.roche.dss.qtt.service.workflow.handler;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryStatusCode;
import com.roche.dss.qtt.service.workflow.WorkflowGlobalConstants;
import com.roche.dss.qtt.utility.Transitions;
import com.roche.dss.qtt.utility.QueryStatusCodes;

/**
 * @author zerkowsm
 *
 */
public class QTTCloseTimerHandler implements ActionHandler, WorkflowGlobalConstants {

 	private static final long serialVersionUID = -1701940272288140395L;
 	
 	private static final Logger logger = LoggerFactory.getLogger(QTTCloseTimerHandler.class);

	@Override
	public void execute(ExecutionContext executionContext) throws Exception {
        logger.info("Time's up - closing query");
        
        executionContext.getTaskInstance().end(Transitions.CLOSE.getTransitionName());
        
        onQueryClose(executionContext);
        
      	logger.info("Query with id: " + executionContext.getTaskInstance().getTaskMgmtInstance().getProcessInstance().getContextInstance().getVariable(PROCESS_INSTANCE_OWNER_ID).toString() + " closed by timer handler. ");
    }
	
	private void onQueryClose(ExecutionContext executionContext) {        
        Long queryId = (Long) executionContext.getTaskInstance().getTaskMgmtInstance().getProcessInstance().getContextInstance().getVariable(PROCESS_INSTANCE_OWNER_ID);
        Session session = (Session) executionContext.getJbpmContext().getSession();
        Query query = (Query) session.get(Query.class, queryId);
        if (query != null) {
	        query.setQueryStatusCode(new QueryStatusCode(QueryStatusCodes.CLOSED.getCode()));
	        executionContext.getJbpmContext().getSession().update(query);
        }
	}

}
