/*
====================================================================
  $Header$
  @author $Author: savovp $
  @version $Revision: 2630 $ $Date: 2013-01-03 11:12:09 +0100 (Cz, 03 sty 2013) $

====================================================================
*/
package com.roche.dss.qtt.query.web.action.oldqrf;

import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.AttachmentCategory;
import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.query.comms.dto.*;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.AttachmentDisplayBean;
import com.roche.dss.qtt.query.web.qrf.CaseDisplayBean;
import com.roche.dss.qtt.query.web.qrf.Constants;
import com.roche.dss.qtt.query.web.qrf.DrugDisplayBean;
import com.roche.dss.qtt.query.web.qrf.model.QueryReviewDisplayModel;
import com.roche.dss.qtt.service.QrfService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.utility.QueryStatusCodes;
import java.text.SimpleDateFormat;
import java.util.*;

@ParentPackage("default")
public class InitiatePreviewAction extends QRFAbstractAction {

    QueryReviewDisplayModel model = new QueryReviewDisplayModel();

    private static final Logger logger = LoggerFactory.getLogger(InitiatePreviewAction.class);
    @Autowired
    private QrfService qrfService;
    @Autowired
    private QueryService queryService;

    private int queryId;

    private long qno;
    private boolean isFromLink;
    boolean isFormQueryDetail = false;
    private String viewtype;
    private String key;
    boolean isDiffRequster = false;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }



    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int queryId) {
        this.queryId = queryId;
    }

    @Override
    public Object getModel() {
        return model;
    }


    public String execute() {
        boolean isDSCL = isDscl();

        String previewMode = ActionConstants.PREVIEW_MODE_ALL;
        isFromLink = false;


        if (!"partial".equals(getRequest().get("myAction")) && getRequest().get("id") != null) {
            //  String id = (String) getRequest().get("id");
            queryId = (Integer) getRequest().get("id");
            //came here by clicking 'query list' menu or query details link
            isFromLink = true;
            if ((getRequest().get("de") != null) && (getRequest().get("de").equals("y"))) {
                isFormQueryDetail = true;
            }
        }

        if (queryId == 0) {
            isFromLink = false;
            queryId = ((Integer) getSession().get(ActionConstants.QRF_QUERY_ID)).intValue();
        } else {
            previewMode = ActionConstants.PREVIEW_MODE_DUPLCHK_ACC_REJECT_AMNDTYP;
        }
        //fetch query detsils for this query id
        //QrfFacadeBD qrf = (QrfFacadeBD)BusinessDelegateFactory.getFactory().getServiceBD(mapping);
        QueryDTO queryDto = qrfService.openQuery(queryId, null, true);
        previewMode = checkPreviewMode(queryDto);

        logDebug("InitiatePreviewAction:isFinalDoc:" + qrfService.isFinalDocExists(queryId) + " isNotSubmitted:" + queryService.checkHasStatus(queryId, QueryStatusCodes.PROCESSING.getCode()) + " isDSCL" + isDSCL);
        // ### bug fix #278 - attempting to open an already-rejected query (2x DSCL users, one using a stale initiate-list!)
        if (queryDto == null) {
            return "alreadyProcessed";
        }
        // ### end of fix.
        boolean haLegaNoDocs = queryService.checkHaLegalNoDocs(queryId);
        buildModel(queryDto, previewMode, haLegaNoDocs, isDiffRequster);
        //remove any old populated form model from session if still exists
        cleanUpSession();
        getSession().put(ActionConstants.INITIATE_QUERY_SESSION_NAME, Long.valueOf(queryId));
        model.setPageTitle("Query Review");
        return SUCCESS;
    }


    /*    @Action(value = "QueryResponse", results = {
@Result(name = SUCCESS, type = "tiles", location = "/initiate_preview"),
@Result(name = "noaccess", type = "tiles", location = "unauthorizedAccess"),
@Result(name = ERROR, location = "/qtt/global/error.jsp")})*/
    public String response() {
        QueryDTO queryDto = qrfService.openQuery(Long.valueOf(qno).intValue(), null, true);
        if (queryDto==null||queryDto.getAccessKey()==null || !queryDto.getAccessKey().equals(key.trim())) {
            // invalid key
            return "noaccess";
        }
        boolean haLegaNoDocs = queryService.checkHaLegalNoDocs(Long.valueOf(qno).intValue());
        //build model basing on model/query in phase 2
        buildModel(queryDto, previewMode, haLegaNoDocs, isDiffRequster);
        populateFinalAttachments(Long.valueOf(qno), model);
        model.setQueryResponse(true);
        return SUCCESS;
    }


    private String checkPreviewMode(QueryDTO queryDto) {
          //instead of previous duplicate_query_review.jsp
          if ("nolayout".equals(viewtype)) {
              return ActionConstants.PREVIEW_MODE_PRNT_CLOSE;
          }
          //instead of of previous query_review.jsp
          if ("initiatePreview".equals(viewtype)) {
              return ActionConstants.PREVIEW_MODE_DUPLCHK_ACC_REJECT_AMNDTYP;
          }
          if ("assignAmend".equals(viewtype)) {
              return ActionConstants.PREVIEW_MODE_ASSIGNED_AMND_DEL_PRNT;
          }
          if ("assignDelete".equals(viewtype)) {
              return ActionConstants.PREVIEW_MODE_ASSIGNED_DELETE_PRNT;
          }
          String previewMode;
                  //if user is dscl
        if (isDscl()) { //if the final doc exists then only print

            if (!getUserDetails().getMail().toLowerCase().equals(queryDto.getRequester().getEmailAddress().toLowerCase())) {
                logger.debug("InitiatePreviewAction:different Requester: true");
                isDiffRequster = true;
            }
            //differentRequester
            if (qrfService.isFinalDocExists(queryId)) {
                previewMode = ActionConstants.PREVIEW_MODE_PRNT;
            }//else if from query list then show amend, del, and print
            else if (isFromLink) {
                if (queryService.checkHasStatus(queryId, QueryStatusCodes.PROCESSING.getCode())) {
                    if (isFormQueryDetail) {
                        previewMode = ActionConstants.PREVIEW_MODE_ASSIGNED_AMND_DEL_PRNT;
                    } else {//the query is being processed and accessed from querylist so
                        //the query is being process to do special amend
                        previewMode = ActionConstants.PREVIEW_MODE_PRNT;
                    }
                } else {
                    previewMode = ActionConstants.PREVIEW_MODE_AMND_DEL_PRNT;
                }

            }//show all
            else {
                previewMode = ActionConstants.PREVIEW_MODE_ALL;
            }
        } else if (queryService.checkHasStatus(queryId, QueryStatusCodes.PROCESSING.getCode())) {//if in process then only print
            previewMode = ActionConstants.PREVIEW_MODE_PRNT;
        } else {//show all
            previewMode = ActionConstants.PREVIEW_MODE_ALL;
        }
          return previewMode;
      }


    private void logDebug(String s) {
        logger.debug(s);
    }


    private QueryReviewDisplayModel buildModel(QueryDTO dto, String previewMode, boolean haLegalNoDocs, boolean diffRequster) {


        populateRequester(dto, model);

        model.setPreviewMode(previewMode);
        model.setHaLegalNoDoc(haLegalNoDocs);
        model.setDifferentRequester(diffRequster);
        //populate preparatory if organisation type is roche_affiliate
        if (dto.getRequester().getOrg().getTypeId() == ActionConstants.ROCHE_AFFILIATE) {
            populatePreparatory(dto, model);
        }

        model.setSourceCountry(dto.getSourceCountryDescription());
        model.setReporterType(dto.getReporterTypeDescription());

        if (!dto.isAllDrug()) {
            model.setAllDrugs(ActionConstants.NO);
            populateDrugs(dto, model);
        } else
            model.setAllDrugs(ActionConstants.YES);


        model.setDateRequested(new SimpleDateFormat("dd-MMM-yyyy").format(new Date(dto.getDateRequested())));

        model.setUrgency(dto.getUrgency());

        model.setAttachmentDTOList(prepareAttachmentsToDisplay(dto));
        model.setAttachmentDTOListWithOldFinal(prepareAttachmentsToDisplayWithOldFinal(dto));

        //populate attachment
        populateAttachments(dto, model);

        //do cumulative
        populateCumulative(dto, model);

        model.setQueryType(dto.getQueryTypeDescription());
        model.setFollowUp(dto.getFollowUpNumber());
        model.setMid(Integer.valueOf(dto.getQueryNumber()));
        model.setQueryTypeCode(dto.getQueryType().name());
        model.setQueryNumber(dto.getQueryNumberString());
        model.setRequesterQueryLabel(dto.getRequesterQueryLabel());

        //do query type details
        populateQueryTypeDetails(dto, model);

        //check if delete Query is visible
        boolean isNotSubmitted = dto.getStatusCode().equals(ActionConstants.QRF_STATUS_NOT_SUBMITTED);
        boolean isSubmitted = dto.getStatusCode().equals(ActionConstants.QRF_STATUS_SUBMITTED);
        boolean isReopened = dto.isReopen();
        model.setDeleteQueryAllowed(!isReopened);
        model.setRejectQueryAllowed(!isReopened);

        return model;
    }

    private List<AttachmentDTO> prepareAttachmentsToDisplay(QueryDTO query) {
        List<AttachmentDTO> attachments = query.getAttachments();
        List<AttachmentDTO> attachmentsFinal = new ArrayList<AttachmentDTO>();
        List<AttachmentDTO> attachmentsOther = new ArrayList<AttachmentDTO>();
        List<AttachmentDTO> attachmentsAll = new ArrayList<AttachmentDTO>();
        List<AttachmentDTO> attachmentsToShow = new ArrayList<AttachmentDTO>();

        if ((attachments != null) && (!attachments.isEmpty())) {

            Collections.sort(attachments, new Comparator<AttachmentDTO>() {
                @Override
                public int compare(AttachmentDTO o1, AttachmentDTO o2) {
                    if (o1.getId() == o2.getId()){
                        return 0;
                    }
                    return o1.getId() > (o2.getId()) ? -1 : 1;
                }
            });

            for (AttachmentDTO attachDto : attachments) {
                if (attachDto.isActivationFlag()) {
                    if (query.getFollowupNumber() != null) {
                        if (query.getFollowupNumber().equals(attachDto.getFollowupNumber())) {
                            if (attachDto.getCategoryId() == 3) {
                                attachmentsFinal.add(attachDto);
                            } else {
                                attachmentsOther.add(attachDto);
                            }
                        }
                    } else {
                        //without filter for old queries
                        if (attachDto.getCategoryId() == 3) {
                            attachmentsFinal.add(attachDto);
                        } else {
                            attachmentsOther.add(attachDto);
                        }
                    }
                }
            }

            attachmentsAll.addAll(attachmentsFinal);
            attachmentsAll.addAll(attachmentsOther);
        }
        //filter
        if (!attachmentsAll.isEmpty()){
            int counter = 0;
            int max=5;
            for (AttachmentDTO attachDto : attachmentsAll){
                logger.debug("attachDto: "+attachDto.toString());
                if (counter < max){
                    counter++;
                    attachmentsToShow.add(attachDto);
                }
            }
        }
        return attachmentsToShow;
    }

    private List<AttachmentDTO> prepareAttachmentsToDisplayWithOldFinal(QueryDTO query) {
        List<AttachmentDTO> attachments = query.getAttachments();
        List<AttachmentDTO> attachmentsFinal = new ArrayList<AttachmentDTO>();
        List<AttachmentDTO> attachmentsOther = new ArrayList<AttachmentDTO>();
        List<AttachmentDTO> attachmentsAll = new ArrayList<AttachmentDTO>();
        List<AttachmentDTO> attachmentsToShow = new ArrayList<AttachmentDTO>();

        if ((attachments != null) && (!attachments.isEmpty())) {

            Collections.sort(attachments, new Comparator<AttachmentDTO>() {
                @Override
                public int compare(AttachmentDTO o1, AttachmentDTO o2) {
                    if (o1.getId() == o2.getId()){
                        return 0;
                    }
                    return o1.getId() > (o2.getId()) ? -1 : 1;
                }
            });

            for (AttachmentDTO attachDto : attachments) {
                if (attachDto.isActivationFlag()) {
                    if (query.getFollowupNumber() != null) {
                        if (query.getFollowupNumber().equals(attachDto.getFollowupNumber())) {
                            if (attachDto.getCategoryId() == 3) {
                                attachmentsFinal.add(attachDto);
                            } else {
                                attachmentsOther.add(attachDto);
                            }
                        } else {
                            if (attachDto.getCategoryId() == 3) {
                                attachmentsFinal.add(attachDto);
                            }
                        }
                    } else {
                        //without filter for old queries
                        if (attachDto.getCategoryId() == 3) {
                            attachmentsFinal.add(attachDto);
                        } else {
                            attachmentsOther.add(attachDto);
                        }
                    }
                }
            }

            attachmentsAll.addAll(attachmentsFinal);
            attachmentsAll.addAll(attachmentsOther);
        }
        //filter
        if (!attachmentsAll.isEmpty()){
            int counter = 0;
            int max=5;
            for (AttachmentDTO attachDto : attachmentsAll){
                logger.debug("attachDto: "+attachDto.toString());
                if (counter < max){
                    counter++;
                    attachmentsToShow.add(attachDto);
                }
            }
        }
        return attachmentsToShow;
    }


    private void populateQueryTypeDetails(QueryDTO dto, QueryReviewDisplayModel bean) {

    	QueryType.Code queryTypeCode = dto.getQueryType();
        //based on the query type id fetch the information from dto and populate the display model
        switch (queryTypeCode) {

            case DRUG_EVENT:
                populateQueryDesc(dto, bean);
                bean.setAeTerm(dto.getAeTerm());
                populateCases(dto, bean);
                bean.setAdditionalComments(dto.getCaseComment());
                break;
            case DRUG_INTERACTION:
                populateDrugList(dto, bean);
                populateQueryDesc(dto, bean);
                populateCases(dto, bean);
                bean.setAdditionalComments(dto.getCaseComment());
                break;
            case MANUFACTURING:
                populateQueryDesc(dto, bean);
                bean.setBatch(dto.getBatch());
                populateCases(dto, bean);
                bean.setAdditionalComments(dto.getCaseComment());
                bean.setOutputFormat(dto.getOutputFormat());
                bean.setOutputOther(dto.getSpecialRequest());
                break;
            case PREGNANCY:
                populateQueryDesc(dto, bean);
                bean.setExposureType(dto.getPregnancy().getExposureTypeDesc());
                bean.setTiming(dto.getPregnancy().getTiming());
                bean.setOutcome(dto.getPregnancy().getOutcome());
                populateCases(dto, bean);
                bean.setAdditionalComments(dto.getCaseComment());
                bean.setOutputFormat(dto.getOutputFormat());
                bean.setOutputOther(dto.getSpecialRequest());
                break;
            case EXTERNAL_AUDIT:
                populateQueryDesc(dto, bean);
                bean.setAeTerm(dto.getAeTerm());
                populateClinicalTrial(dto, bean);
                bean.setOutputOther(dto.getSpecialRequest());
                break;
            case CASE_CLARIFICATION:
            case CASE_DATA_REQUEST:
                populateCases(dto, bean);
                bean.setAdditionalComments(dto.getCaseComment());
                bean.setOutputFormat(dto.getOutputFormat());
                bean.setOutputOther(dto.getSpecialRequest());
                break;
            case SAE_RECONCILIATION:
                populateClinicalTrial(dto, bean);
                bean.setOutputOther(dto.getSpecialRequest());
                break;
            case IND_UPDATES:
                bean.setIndNumber(dto.getINDNumber());
                break;
            case PERFORMANCE_METRICS:
                populateQueryDesc(dto, bean);
                break;
            case SIGNAL_DETECTION:
            case LITERATURE_SEARCH:
                populateQueryDesc(dto, bean);
                bean.setAeTerm(dto.getAeTerm());
                break;
            case PERFORMANCE_METRICS2:
            	bean.setPerformanceMetrics(dto.getPerformanceMetric());
            	break;
            case EXTERNAL_AUDIT_INSPECTION:
            	bean.setAudit(dto.getAudit());
                populateQueryDesc(dto, bean);
                populateClinicalTrial(dto, bean);
                bean.setSpecialRequest(dto.getSpecialRequest());
                break;
            case MANUFACTURING_RECALL:
            	bean.setManufacturing(dto.getManufacturing());
            	populateQueryDesc(dto, bean);
                bean.setOutputFormat(dto.getClinical().getOutputFormatDesc());
                bean.setOutputOther(dto.getSpecialRequest());
            	bean.setAeTerm(dto.getAeTerm());
            	bean.setSpecialRequest(dto.getSpecialRequest());
            	break;
            case MEDICAL:
                populateQueryDesc(dto, bean);
                bean.setExposureType(dto.getPregnancy().getExposureTypeDesc());
                bean.setTiming(dto.getPregnancy().getTiming());
                bean.setOutcome(dto.getPregnancy().getOutcome());
                bean.setComments(dto.getPregnancy().getComments());
                populateCases(dto, bean);
                bean.setAdditionalComments(dto.getCaseComment());
                populateDrugList(dto, bean);
                break;
            case DATA_SEARCH:
            	populateQueryDesc(dto, bean);
                bean.setAdditionalComments(dto.getCaseComment());
                populateClinicalTrial(dto, bean);
                populateCases(dto, bean);
                bean.setOutputFormat(dto.getOutputFormat());
                bean.setOutputOther(dto.getSpecialRequest());
                bean.setDataSearch(dto.getDataSearch());
                break;
            default:
                //do nothing;
                break;
        }

    }

    private void populateQueryDesc(QueryDTO dto, QueryReviewDisplayModel bean) {

        bean.setNatureOfCase(dto.getQueryDesc().getNatureOfCase());
        bean.setIndexCase(dto.getQueryDesc().getIndexCase());
        bean.setSignsSymp(dto.getQueryDesc().getSignsSymp());
        bean.setInvestigations(dto.getQueryDesc().getInvestigations());
        bean.setPmh(dto.getQueryDesc().getPmh());
        bean.setConMeds(dto.getQueryDesc().getConMeds());
        bean.setOtherInfo(dto.getQueryDesc().getOtherInfo());
        bean.setDiagnosis(dto.getQueryDesc().getDiagnosis());
    }

    private void populateCases(QueryDTO dto, QueryReviewDisplayModel bean) {

        List caseList = new ArrayList();
        if ((dto.getCases() != null) && (!dto.getCases().isEmpty())) {

            Iterator iter = dto.getCases().iterator();
            while (iter.hasNext()) {
                caseList.add(new CaseDisplayBean((CaseDTO) iter.next()));

            }
        }//if
        bean.setCases(caseList);
    }

    private void populateDrugList(QueryDTO dto, QueryReviewDisplayModel bean) {

        List drugList = new ArrayList();
        if ((dto.getDrugs() != null) && (!dto.getDrugs().isEmpty())) {

            Iterator iter = dto.getDrugs().iterator();
            while (iter.hasNext()) {
                drugList.add(new DrugDisplayBean((DrugDTO) iter.next()));

            }
        }//if
        bean.setDrugs(drugList);
    }

    private void populateClinicalTrial(QueryDTO dto, QueryReviewDisplayModel bean) {

        bean.setProtocolNumber(dto.getClinical().getProtocolNumber());
        bean.setCrtnNumber(dto.getClinical().getCrtnNumber());
        bean.setPatientNumber(dto.getClinical().getPatientNumber());
        bean.setCaseSelection(dto.getClinical().getCaseSelection());
        bean.setOutputFormatDesc(dto.getClinical().getOutputFormatDesc());
    }

    private void populateRequester(QueryDTO query, QueryReviewDisplayModel bean) {

        bean.setFirstname(query.getRequester().getFirstName());
        bean.setLastname(query.getRequester().getLastName());
        bean.setTelephone(query.getRequester().getTelephone());
        bean.setFax(query.getRequester().getFax());
        bean.setEmail(query.getRequester().getEmailAddress());
        bean.setReqCountry(query.getRequester().getCountryName());
        bean.setOrgName(query.getRequester().getOrg().getName());
        bean.setOrgType(query.getRequester().getOrg().getTypeDescription());
    }

    private void populateDrugs(QueryDTO query, QueryReviewDisplayModel bean) {

        DrugDTO drugDto = query.getDrug();
        bean.setRetrDrug(drugDto.getRetrName());
        bean.setGenericDrug(drugDto.getGenericName());
        bean.setRoute(drugDto.getRoute());
        bean.setFormulation(drugDto.getFormulation());
        bean.setIndication(drugDto.getIndication());
        bean.setDose(drugDto.getDose());
        //bug fix 346
        //model.setOthersDrug(query.getQuerySummary());
        bean.setOthersDrug(drugDto.getExtraFirstDrugs());
    }

    private void populateCumulative(QueryDTO query, QueryReviewDisplayModel bean) {

        CumulativePeriodDTO cummDto = query.getCumulative();
        //System.out.println("The cumulative period is: "+ cummDto.isCumulative());
        bean.setCumulative(cummDto.isCumulative() ? ActionConstants.YES : ActionConstants.NO);

        bean.setDateFrom(new SimpleDateFormat("dd-MMM-yyyy").format(new Date(query.getCumulative().getFromDate())));
        bean.setDateTo(new SimpleDateFormat("dd-MMM-yyyy").format(new Date(query.getCumulative().getToDate())));
        bean.setInterval(cummDto.getInterval());
    }


    private void populateFinalAttachments(Long queryId, QueryReviewDisplayModel model) {
        Set<Attachment> attachSet = queryService.find(queryId).getAttachments();
        List attachmentList = new ArrayList();
        if (attachSet != null && !attachSet.isEmpty()) {
            for (Attachment a : attachSet) {
                if (a.isActivated() && a.getAttachmentCategory().getAttachmentCategoryId() == AttachmentCategory.CATEGORY_FINAL_RESPONSE) {
                    attachmentList.add(a);
                }
            }
        }
        model.setFinalAttachments(attachmentList);
    }


    private void populateAttachments(QueryDTO query, QueryReviewDisplayModel bean) {

        List<AttachmentDisplayBean> attachmentList = new ArrayList<AttachmentDisplayBean>();
        if (query.getAttachments() != null) {
            for (AttachmentDTO attDTO : query.getAttachments()) {
                if (attDTO.getCategoryId() == 1) {//if the attachment is QRF and the document is activated
                    attachmentList.add(new AttachmentDisplayBean(attDTO));
                }
            }
        }//if
        bean.setAttachments(attachmentList);
    }

    private void populatePreparatory(QueryDTO query, QueryReviewDisplayModel bean) {

        List prepList = query.getPreparatory();
        if ((prepList != null) && (!prepList.isEmpty())) {

            for (int i = 0; i < prepList.size(); i++) {

                PreparatoryDTO prepDto = (PreparatoryDTO) prepList.get(i);
                //System.out.println("Setting the populatePreparatory: "+prepDto.getPreparatoryId());
                switch (prepDto.getPreparatoryId()) {

                    case ActionConstants.REPOSITORY:
                        bean.setRepository(ActionConstants.YES);
                        break;
                    case ActionConstants.PSURS:
                        bean.setPsur(ActionConstants.YES);
                        break;
                    case ActionConstants.CDS:
                        bean.setCds(ActionConstants.YES);
                        break;
                    case ActionConstants.ISSUE_WORKUPS:
                        bean.setIssue(ActionConstants.YES);
                        break;
                    case ActionConstants.LITERATURE:
                        bean.setLiterature(ActionConstants.YES);
                        break;
                    case ActionConstants.OTHERS:
                        bean.setOthers(ActionConstants.YES);
                        bean.setOthersComment(prepDto.getOthers());
                        break;
                    default:
                        break;
                }//end switch
            }//end for
        }//end if

        //also set the comments
        bean.setComments(query.getPrepComments());
    }

    public long getQno() {
        return qno;
    }

    public void setQno(long qno) {
        this.qno = qno;
    }
}
