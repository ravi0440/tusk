package com.roche.dss.qtt.query.comms.dto;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public class CountryDTO {
    private String code;
    private String description;

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }


    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CountryDTO))
			return false;
		CountryDTO other = (CountryDTO) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}

	public void setCode(String code) {
       this.code = code;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
