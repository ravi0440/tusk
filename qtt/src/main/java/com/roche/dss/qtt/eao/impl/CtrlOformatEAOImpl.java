package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.CtrlOformatEAO;
import com.roche.dss.qtt.model.CtrlOformat;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
@Repository("ctrlOformat")
public class CtrlOformatEAOImpl  extends AbstractBaseEAO<CtrlOformat> implements CtrlOformatEAO {

}
