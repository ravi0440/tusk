package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.PreQueryPreparationEAO;
import com.roche.dss.qtt.model.PreQueryPreparation;

@Repository("preQueryPreparationEAO")
public class PreQueryPreparationEAOImpl extends AbstractQueryRelatedEAO<PreQueryPreparation> implements PreQueryPreparationEAO {
	
}