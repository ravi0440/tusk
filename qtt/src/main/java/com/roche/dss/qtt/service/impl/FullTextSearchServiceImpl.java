package com.roche.dss.qtt.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.text.BadLocationException;
import javax.swing.text.rtf.RTFEditorKit;

import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.query.comms.dao.FullTextSearchDAO;
import com.roche.dss.qtt.query.web.model.QuickSearchParams;
import com.roche.dss.qtt.service.AttachmentWithSearchInfo;
import com.roche.dss.qtt.service.FullTextSearchService;
import com.roche.dss.qtt.service.SearchResult;
import com.roche.dss.qtt.service.SearchResults;
import com.roche.dss.qtt.service.UtilService;
import com.roche.dss.util.ApplicationUtils;
import com.roche.dss.util.Msg2txtUnicode;
import com.roche.dss.qtt.utility.config.Config;

@Service("fullTextSearchService")
@Transactional(readOnly = true)
public class FullTextSearchServiceImpl implements FullTextSearchService {
	@Autowired
	protected FullTextSearchDAO dao;
	
	@Autowired
	protected UtilService utilService;
	
    @Autowired
    protected Config config;
	
	protected static final Logger log = LoggerFactory.getLogger(FullTextSearchServiceImpl.class);

	@Override
	public SearchResults searchDatabase(QuickSearchParams searchParams) {
		parseSearchParameters(searchParams);
		if (searchParams.getIsValid()) {
			return dao.searchDatabase(searchParams.getTermParsed());
		} else {
			return null;
		}
	}

	@Override
	public SearchResults searchAttachments(QuickSearchParams searchParams) {
		parseSearchParameters(searchParams);
		if (searchParams.getIsValid()) {
			return highlightResults(dao.searchAttachments(searchParams.getTermParsed()));
		} else {
			return null;
		}
	}

	@Override
	public SearchResults searchBoth(QuickSearchParams searchParams) {
		parseSearchParameters(searchParams);
		if (searchParams.getIsValid()) {
			return highlightResults(dao.searchBoth(searchParams.getTermParsed()));
		} else {
			return null;
		}
	}
	
	public SearchResults highlightResults(SearchResults res) {
		if (res != null) {
			if (res.getResults() != null && res.getResults().size() > 0) {
				Query q = res.getLuceneQuery();
				String quickSearchKeywords = res.getQuickSearchKeywords();
				QueryScorer scorer = new QueryScorer(q);
				SimpleHTMLFormatter formatter = new SimpleHTMLFormatter("<span class=\"highlight\">", "</span>");
				Highlighter highlighter = new Highlighter(formatter, scorer);
				Fragmenter fragmenter = new SimpleSpanFragmenter(scorer);
				highlighter.setTextFragmenter(fragmenter);
				Analyzer analyzer = new SimpleAnalyzer();
				for (SearchResult resItem : res.getResults()) {
					com.roche.dss.qtt.model.Query query = resItem.getQuery();
					if (query == null) {
						continue;
					}
					Document doc = resItem.getDoc();
					String[] attSeqs = doc.getValues("attachments.attachmentSeq");
					String[] attContents = doc.getValues("attachments.content");
					
					List<AttachmentWithSearchInfo> attsWithInfoL = new ArrayList<AttachmentWithSearchInfo>();
					
					if (attSeqs != null && attSeqs.length > 0) {
						for (int i = 0; i < attSeqs.length; ++ i) {
							String attSeqS = attSeqs[i];
							if (attSeqS != null) {
								try {
									long attSeq = Long.parseLong(attSeqS);
									Attachment attF = null;
									for (Attachment att: query.getAttachments()) {
										if (att.getAttachmentSeq() == attSeq) {
											attF = att;
											break;
										}
									}
									
									if (attF != null) {
										String textToHighlight = "";
										textToHighlight = ApplicationUtils.appendText(textToHighlight, attContents[i]);
										String highText = utilService.getHighlightedText(attF, quickSearchKeywords
												, highlighter, analyzer, textToHighlight);								
										if (highText == null) {
											textToHighlight = "";
											textToHighlight = ApplicationUtils.appendText(textToHighlight, attF.getTitle());
											textToHighlight = ApplicationUtils.appendText(textToHighlight, attF.getAuthor());
											textToHighlight = ApplicationUtils.appendText(textToHighlight, attF.getFilename());
											highText = highlighter.getBestFragment(analyzer,
													null, textToHighlight);
										}
										if (highText != null) {
											AttachmentWithSearchInfo attWithInfo = new AttachmentWithSearchInfo(attF, highText);
											attsWithInfoL.add(attWithInfo);
										}
									}
								} catch (IOException e) {
									log.warn("IOException for query: " + query.getQuerySeq(), e);
								} catch (InvalidTokenOffsetsException e) {
									log.warn("InvalidTokenOffsetsException for query: " + query.getQuerySeq(), e);
								} catch (Exception e) {
									log.warn("Exception for query: " + query.getQuerySeq(), e);
								}
							}
						}
						if (attsWithInfoL.size() > 0) {
							log.debug("Query: " + query.getQuerySeq() + ", matching attachments: " + attsWithInfoL.size());
							resItem.setAttsWithInfo(attsWithInfoL);
						}
					}
				}
			}
		}
		return res;
	}
	
	public static void parseSearchParameters(QuickSearchParams searchParams) {
		boolean perPresent = false;
		String r1 = ApplicationUtils.searchDateRange(searchParams.getPeriodDateFrom(), searchParams.getPeriodDateTo(), "retrievalPeriod.startTs");
		if (r1 != null) {
			perPresent = true;
		}
		
		boolean qryPresent = false;
		String r2 = ApplicationUtils.searchDateRange(searchParams.getQryResponseDateFrom(), searchParams.getQryResponseDateTo(), "dateRequested");
		if (r2 != null) {
			qryPresent = true;
		}
		
		if (ApplicationUtils.isEmptyString(searchParams.getTerm())) {
			// we may have dates only
			if (!(perPresent || qryPresent)) {
				// there are no parameters
				searchParams.setIsValid(false);
			} else {
				String term = null;
				if (qryPresent) {
					if (perPresent) {
						term = r2 + " AND " + r1;
					} else {
						term = r2;
					}
				} else {
					term = r1;
				}
				searchParams.setTermParsed(term);
			}
		} else {
			if (perPresent || qryPresent) {
				String term = null;
				if (qryPresent) {
					if (perPresent) {
						term = r2 + " AND " + r1;
					} else {
						term = r2;
					}
				} else {
					term = r1;
				}
				searchParams.setTermParsed("(" + searchParams.getTerm() + ") AND " + term);
			} else {
				searchParams.setTermParsed(searchParams.getTerm());
			}
		}
		
		if (searchParams.getIsValid()) {
			log.debug("Processed search terms: " + searchParams.getTermParsed());
		} else {
			log.warn("Invalid search terms input: " + searchParams.getTerm());
		}
	}
	
	public String notEmpty(String retValue) {
		if (retValue == null || retValue.length() == 0) {
			return " ";
		} else {
			return retValue;
		}
	}
	
	@Override
	@Cacheable(cacheName = "textExtractionCache", keyGeneratorName="filenameKeyGenerator")
	public String getFileTextContent(String filename) {
		String dummy = " ";
		
        int extStart = filename.lastIndexOf('.');
        if (extStart < 0 || extStart == filename.length() - 1) {
        	log.warn("Unknown filename extension for: " + filename);
        	return dummy;
        }
        log.debug("indexing file: " + filename);
        String ext = filename.substring(extStart + 1).toUpperCase();
        if ("PDF".equals(ext)) {
        	String executable = config.getPdfExecutable();
        	
        	String outputFilenameOrig = config.getPdfTemp() + "out.txt";
        	String outputFilename = outputFilenameOrig;
        	File fileToOverwrite = new File(outputFilenameOrig);
        	if (fileToOverwrite.exists()) {
        		if (! fileToOverwrite.delete()) {
        			log.warn("Cannot delete temporary output file: " + outputFilenameOrig);
        			return dummy;
        		}
        	}
        	
        	File fileIn = new File(filename);
        	if (! fileIn.exists()) {
        		log.warn("file does not exist: " + filename);
        		return dummy;
        	}
        	
        	String filenameAsExecParam = filename;
        	boolean spaceFound = false;
        	if (filename.contains(" ")) {
	        	filenameAsExecParam = config.getPdfTemp() + "in.pdf";
	        	fileToOverwrite = new File(filenameAsExecParam);
	        	if (fileToOverwrite.exists()) {
	        		if (! fileToOverwrite.delete()) {
	        			log.warn("Cannot delete temporary input file: " + filenameAsExecParam);
	        			return dummy;
	        		}
	        	}
	        	
	        	try {
					ApplicationUtils.copy(fileIn, fileToOverwrite);
				} catch (IOException e) {
					log.warn("IOException during file copy for file: " + filename);
					return dummy;
				}
				spaceFound = true;
        	}
        	
        	Runtime run = Runtime.getRuntime();
        	
        	boolean extractionSuccess = false;
        	try {
				Process p = run.exec(executable + " -enc UTF-8 " + filenameAsExecParam + " " + outputFilename);
				BufferedReader in = new BufferedReader(new InputStreamReader(p.getErrorStream()));
				String line;
				while ((line = in.readLine()) != null) {
					log.warn("error output: " + line);
				}
				IOUtils.closeQuietly(in);
				int exitVal = p.waitFor();
				if (exitVal == 0) {
					extractionSuccess = true;
				} else {
					log.warn("Process exitValue: " + exitVal + " for file: " + filename);
				}
			} catch (Exception e) {
				log.warn("Exception for file: " + filename + ", exception: " + e.toString());
			}
	        catch (Error e) {
	        	log.warn("Error for file: " + filename + ", exception: " + e.toString());
	        }
	        
	        if (extractionSuccess) {
	        	try {
	        		StringBuffer bufOut = new StringBuffer();
					BufferedReader buf = new BufferedReader(new InputStreamReader(new FileInputStream (outputFilenameOrig), "UTF8"));
					while (true) {
						String line = buf.readLine();
						if (line == null) {
							break;
						}
						bufOut.append(line);
					}
					buf.close();
					log.debug("indexing finished for: " + filename);
					return notEmpty(bufOut.toString());
				} catch (FileNotFoundException e) {
					log.warn("File not found: " + filename);
				} catch (IOException e) {
					log.warn("IOException for file: " + filename, e);
				}
		        catch (Exception e) {
		        	log.warn("Exception for file: " + filename + ", exception: " + e.toString());
		        }
		        catch (Error e) {
		        	log.warn("Error for file: " + filename + ", exception: " + e.toString());
		        } finally {
		        	try {
			        	File outputToDelete = new File(outputFilenameOrig);
			        	if (outputToDelete.exists()) {
			        		if (! outputToDelete.delete()) {
			        			log.warn("Could not delete the temporary output file: " + outputFilenameOrig);
			        		}
			        	}
		        	}
			        catch (Exception e) {
			        	log.warn("Exception for file: " + outputFilenameOrig + ", exception: " + e.toString());
			        }
			        catch (Error e) {
			        	log.warn("Error for file: " + outputFilenameOrig + ", exception: " + e.toString());
			        }
			        if (spaceFound) {
			        	try {
				        	File outputToDelete = new File(filenameAsExecParam);
				        	if (outputToDelete.exists()) {
				        		if (! outputToDelete.delete()) {
				        			log.warn("Could not delete the temporary input file: " + filenameAsExecParam);
				        		}
				        	}
			        	}
				        catch (Exception e) {
				        	log.warn("Exception for file: " + filenameAsExecParam + ", exception: " + e.toString());
				        }
				        catch (Error e) {
				        	log.warn("Error for file: " + filenameAsExecParam + ", exception: " + e.toString());
				        }
			        }
		        }
	        }
        } else if ("TXT".equals(ext)) {
        	try {
        		StringBuffer bufOut = new StringBuffer();
				BufferedReader buf = new BufferedReader(new InputStreamReader(new FileInputStream (filename), "UTF8"));
				while (true) {
					String line = buf.readLine();
					if (line == null) {
						break;
					}
					bufOut.append(line);
				}
				buf.close();
				log.debug("indexing finished for: " + filename);
				return notEmpty(bufOut.toString());
			} catch (FileNotFoundException e) {
				log.warn("File not found: " + filename);
			} catch (IOException e) {
				log.warn("IOException for file: " + filename, e);
			}
	        catch (Exception e) {
	        	log.warn("Exception for file: " + filename + ", exception: " + e.toString());
	        }
	        catch (Error e) {
	        	log.warn("Error for file: " + filename + ", exception: " + e.toString());
	        }
        } else if ("DOC".equals(ext)) {
        	FileInputStream fileIn;
			try {
				fileIn = new FileInputStream (filename);
				WordExtractor wordExt = new WordExtractor(fileIn);
				String text = wordExt.getText();
				IOUtils.closeQuietly(fileIn);
				log.debug("indexing finished for: " + filename);
				return notEmpty(text);
			} catch (FileNotFoundException e) {
				log.warn("File not found: " + filename);
			} catch (IOException e) {
				log.warn("IOException for file: " + filename, e);
			}
	        catch (Exception e) {
	        	log.warn("Exception for file: " + filename + ", exception: " + e.toString());
	        }
	        catch (Error e) {
	        	log.warn("Error for file: " + filename + ", exception: " + e.toString());
	        }
        } else if ("DOCX".equals(ext)) {
        	FileInputStream fileIn;
			try {
				fileIn = new FileInputStream (filename);
				XWPFDocument wordDoc = new XWPFDocument(fileIn);
				XWPFWordExtractor wordExt = new XWPFWordExtractor(wordDoc);
				String text = wordExt.getText();
				IOUtils.closeQuietly(fileIn);
				log.debug("indexing finished for: " + filename);
				return notEmpty(text);
			} catch (FileNotFoundException e) {
				log.warn("File not found: " + filename);
			} catch (IOException e) {
				log.warn("IOException for file: " + filename, e);
			}
	        catch (Exception e) {
	        	log.warn("Exception for file: " + filename + ", exception: " + e.toString());
	        }
	        catch (Error e) {
	        	log.warn("Error for file: " + filename + ", exception: " + e.toString());
	        }
        } else if ("RTF".equals(ext)) {
        	String bodyText = null;
        	BufferedReader buf = null;
        	boolean bufClosed = false;
        	try {
	        	buf = new BufferedReader(new InputStreamReader(new FileInputStream (filename)));
	        	RTFEditorKit rtfEditor = new RTFEditorKit();
	        	javax.swing.text.Document styledDoc = rtfEditor.createDefaultDocument();;
	        	rtfEditor.read(buf, styledDoc, 0);
	        	log.debug("Indexing a RTF file, doc length: " + styledDoc.getLength());
	        	bodyText = styledDoc.getText(0, styledDoc.getLength());
				buf.close();
				bufClosed = true;
				log.debug("indexing finished for: " + filename);
			} catch (FileNotFoundException e) {
				log.warn("File not found: " + filename);
			} catch (IOException e) {
				log.warn("IOException for file: " + filename, e);
			} catch (BadLocationException e) {
				log.warn("BadLocationException for file : " + filename, e);
			} catch (OutOfMemoryError e) {
				log.error("OutOfMemoryError for file: " + filename, e);
			}
	        catch (Exception e) {
	        	log.warn("Exception for file: " + filename + ", exception: " + e.toString());
	        }
	        catch (Error e) {
	        	log.warn("Error for file: " + filename + ", exception: " + e.toString());
	        }
	        if (buf != null && ! bufClosed) {
				try {
					buf.close();
				} catch (IOException e1) {
					log.warn("IOException for file: " + filename, e1);
				}	        	
	        }
        	
        	return notEmpty(bodyText);
        /*} else if ("XLS".equals(ext)) {
        	FileInputStream fileIn;
			try {
				fileIn = new FileInputStream (filename);
				HSSFWorkbook wb = new HSSFWorkbook(fileIn);
				ExcelExtractor excExt = new ExcelExtractor(wb);
				excExt.setFormulasNotResults(false);
				excExt.setIncludeBlankCells(true);
				excExt.setIncludeCellComments(true);
				excExt.setIncludeHeadersFooters(true);
				excExt.setIncludeSheetNames(true);
				
				String bodyText = excExt.getText();
				fileIn.close();
				log.debug("indexing finished for: " + filename);
				return notEmpty(bodyText);
			} catch (FileNotFoundException e) {
				log.warn("File not found: " + filename);
			} catch (IOException e) {
				log.warn("IOException for file: " + filename, e);
			}
	        catch (Exception e) {
	        	log.warn("Exception for file: " + filename + ", exception: " + e.toString());
	        }
	        catch (Error e) {
	        	log.warn("Error for file: " + filename + ", exception: " + e.toString());
	        }
        } else if ("XLSX".equals(ext)) {
			try {
				XSSFExcelExtractor excExt = new XSSFExcelExtractor(filename);
				excExt.setFormulasNotResults(false);
				excExt.setIncludeCellComments(true);
				excExt.setIncludeHeadersFooters(true);
				excExt.setIncludeSheetNames(true);
				String bodyText = excExt.getText();
				log.debug("indexing finished for: " + filename);
				return notEmpty(bodyText);
			} catch (IOException e) {
				log.warn("IOException for file: " + filename);
			} catch (XmlException e) {
				log.warn("XmlException for file: " + filename, e);
			} catch (OpenXML4JException e) {
				log.warn("OpenXML4JException for file: " + filename, e);
			}
	        catch (Exception e) {
	        	log.warn("Exception for file: " + filename + ", exception: " + e.toString());
	        }
	        catch (Error e) {
	        	log.warn("Error for file: " + filename + ", exception: " + e.toString());
	        }
        } else if ("PPT".equals(ext)) {
        	FileInputStream fileIn;
			try {
				fileIn = new FileInputStream (filename);
				PowerPointExtractor pptExt = new PowerPointExtractor(fileIn);
				String bodyText = pptExt.getText();
				fileIn.close();
				log.debug("indexing finished for: " + filename);
				return notEmpty(bodyText);
			} catch (FileNotFoundException e) {
				log.warn("File not found: " + filename);
			} catch (IOException e) {
				log.warn("IOException for file: " + filename, e);
			}
	        catch (Exception e) {
	        	log.warn("Exception for file: " + filename + ", exception: " + e.toString());
	        }
	        catch (Error e) {
	        	log.warn("Error for file: " + filename + ", exception: " + e.toString());
	        }
        } else if ("PPTX".equals(ext)) {
        	XSLFSlideShow slides;
			try {
				slides = new XSLFSlideShow(filename);
				XSLFPowerPointExtractor pptExt = new XSLFPowerPointExtractor(slides);
				String bodyText = pptExt.getText(true, true);
				log.debug("indexing finished for: " + filename);
				return notEmpty(bodyText);
			} catch (OpenXML4JException e) {
				log.warn("OpenXML4JException for file: " + filename, e);
			} catch (IOException e) {
				log.warn("IOException for file: " + filename);
			} catch (XmlException e) {
				log.warn("XmlException for file: " + filename, e);
			}
	        catch (Exception e) {
	        	log.warn("Exception for file: " + filename + ", exception: " + e.toString());
	        }
	        catch (Error e) {
	        	log.warn("Error for file: " + filename + ", exception: " + e.toString());
	        }
        } else if ("HTM".equals(ext) || "HTML".equals(ext)) {
        	File file = new File(filename);
        	try {
				org.jsoup.nodes.Document doc = Jsoup.parse(file, "UTF-8");
				String bodyText = doc.text();
				log.debug("indexing finished for: " + filename);
				return notEmpty(bodyText);
			} catch (IOException e) {
				log.warn("IOException for file: " + filename);
			}
	        catch (Exception e) {
	        	log.warn("Exception for file: " + filename + ", exception: " + e.toString());
	        }
        } else if ("MSG".equals(ext)) {
			try {
				Msg2txtUnicode msgExt = new Msg2txtUnicode(filename);
				msgExt.processMessage();
				log.debug("indexing finished for: " + filename);
				
				filename = filename.substring(0, filename.length() - 4) + ".txt";
				log.debug("indexing intermediate: " + filename);
        		StringBuffer bufOut = new StringBuffer();
				BufferedReader buf = new BufferedReader(new InputStreamReader(new FileInputStream (filename), "UTF8"));
				while (true) {
					String line = buf.readLine();
					if (line == null) {
						break;
					}
					bufOut.append(line);
				}
				buf.close();
				
				log.debug("indexing finished for intermediate: " + filename);
				
				File fileInt = new File (filename);
				if (fileInt.exists()) {
					boolean delRes = fileInt.delete();
					if (! delRes) {
						log.warn("Could not delete the intermediate file: " + filename);
					}
				}
				return notEmpty(bufOut.toString());
			} catch (FileNotFoundException e) {
				log.warn("File not found: " + filename);
			} catch (IOException e) {
				log.warn("IOException for file: " + filename, e);
			}
	        catch (Exception e) {
	        	log.warn("Exception for file: " + filename + ", exception: " + e.toString());
	        }
	        catch (Error e) {
	        	log.warn("Error for file: " + filename + ", exception: " + e.toString());
	        }*/
        }
        log.warn("Unable to index file: " + filename);
        return dummy;		
	}
	
	@Override
	@TriggersRemove(cacheName = "textExtractionCache", keyGeneratorName="filenameKeyGenerator")
	public void clearFileTextContent(String filename) {
		
	}
}
