package com.roche.dss.qtt.service.workflow.handler;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.comms.dto.EmailDTO;
import com.roche.dss.qtt.service.workflow.WorkflowGlobalConstants;
import com.roche.dss.qtt.utility.EmailSender;
import com.roche.dss.qtt.utility.config.Config;
import com.roche.dss.util.ApplicationContextProvider;
import java.util.Properties;


public class QTTMINotificationHandler implements ActionHandler, WorkflowGlobalConstants {

	private static final long serialVersionUID = 4329162100326875804L;
	
	private static final Logger logger = LoggerFactory.getLogger(QTTMINotificationHandler.class);
	  
	
	@Override
	public void execute(ExecutionContext executionContext) throws Exception {
		logger.debug("QTTMINotificationHandler");
		ContextInstance ctx = executionContext.getContextInstance();
        Long queryId = (Long) ctx.getVariable(PROCESS_INSTANCE_OWNER_ID);
        Session session = (Session) executionContext.getJbpmContext().getSession();
        Query query = (Query) session.get(Query.class, queryId);
        if (query != null) {
        	logger.info("sending a notification for query: " + query.getQuerySeq());
        	EmailDTO email = new EmailDTO();
        	
        	ApplicationContext appCtx = ApplicationContextProvider.ctx;
        	EmailSender emailSender = appCtx.getBean(EmailSender.class);
        	Properties properties = (Properties) appCtx.getBean("properties");
        	Config config = appCtx.getBean(Config.class);
        	
        	String assignedUserEmail;        	
        	if(config.isTestEnvironment()){
        		assignedUserEmail = config.getQttTestMiNotificationEmail();
        	} else {
        		assignedUserEmail = (String) executionContext.getContextInstance().getVariable(PS_ASSIGNED_ACTOR_EMAIL);
        	}       	
        	
        	email.buildDTO_AvailabilityAlert(query, assignedUserEmail, properties);
        	
            try {
				emailSender.sendMessage(email.getFrom(), email.getTo(), email.getCc(),
				        email.getSubject(), email.getBody());
			} catch (AddressException e) {
				logger.warn("QTTMINotificationHandler AddressException", e);
			} catch (SendFailedException e) {
				logger.warn("QTTMINotificationHandler SendFailedException", e);
			} catch (MessagingException e) {
				logger.warn("QTTMINotificationHandler MessagingException", e);
			} catch (Exception e) {
				logger.warn("QTTMINotificationHandler Exception", e);			
			}
        } else {
        	logger.warn("problem with sending a notification for query: " + queryId + ", the query is not found in the database");
        }
	
	}

}
