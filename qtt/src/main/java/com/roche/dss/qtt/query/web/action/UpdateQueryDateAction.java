package com.roche.dss.qtt.query.web.action;

import java.util.Date;

import javax.annotation.Resource;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.util.ApplicationUtils;

/**
 * This action enables to change query expire date (enter_expiry_date.jsp)
 * <p/>
 * User: pruchnil
 */
@Conversion()
public class UpdateQueryDateAction extends CommonActionSupport {
	private static final long serialVersionUID = 8422210973249387756L;
	private static final Logger logger = LoggerFactory.getLogger(UpdateQueryDateAction.class);
    private long queryId;
    private Query query;

    private Date date;
    private DateTime newDate;
    private boolean affiliateAccess;
    private String explanation;
    private boolean expiry;
    private String sourceSearch;

    private QueryService queryService;

    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Override
    @SkipValidation
    public String execute() throws Exception {
        if (!securityService.isAllowedCheck(DSCL_PERMISSION)) {
            return DENIED;
        }
        query = queryService.find(queryId);
        date = query.getExpiryDate().toDate();
        affiliateAccess = ApplicationUtils.convertToBoolean(String.valueOf(query.getAffiliateAccess()));
        return SUCCESS;
    }

    @SkipValidation
    public String enterResponseDate() {
        if (!isAllowed()) {
            return DENIED;
        }
        query = queryService.find(queryId);
        return SUCCESS;
    }

    @SkipValidation
    public String enterDMGResponseDate() {
        if (!isAllowed()) {
            return DENIED;
        }
        query = queryService.find(queryId);
        return SUCCESS;
    }

    public String updateExpiryDate() throws Exception {
        queryService.updateQueryExpiryDate(queryId, newDate, affiliateAccess);
        return UPDATE;
    }

    public String updateDMGResponseDate() throws Exception {
        queryService.updateDMGResponseDate(queryId, newDate, explanation, isAffiliate(),getLoggedUserName());
        return UPDATE;
    }

    public String updateResponseDate() throws Exception {
        queryService.updateResponseDate(queryId, newDate, explanation, isAffiliate(),getLoggedUserName());
        return UPDATE;
    }

    @Override
    public void validate() {
    	
        DateTime compareDate = new DateTime();
        try {
            newDate = new DateTime(date);
         
            if (date == null) {
                addActionError(getText("field.required", new String[]{"New date"}));
            }
            compareDate = compareDate.withTime(0,0,0,0);
            if (date != null && newDate.isBefore(compareDate)) {
                addActionError(getText("date.onwward"));
            }
            query = queryService.find(queryId);
            DateTime dmgDueDate = new DateTime(query.getDmgDueDate());
            DateTime currentDueDate = new DateTime(query.getCurrentDueDate());
          
            if(date!=null&&ActionContext.getContext().getName().equals("UpdateDMGResponseDate")&&query.getCurrentDueDate()!=null&&newDate.isAfter(currentDueDate)){
            	addActionError(getText("dmg.dataSearch.DMGResponseDueDate.later", new String[]{query.getCurrentDueDateFormatted()}));
            }
            if(date!=null&&ActionContext.getContext().getName().equals("UpdateResponseDate")&&query.getDmgDueDate()!=null&&newDate.isBefore(dmgDueDate)){
            	addActionError(getText("dmg.dataSearch.ResponseDueDate.later", new String[]{ApplicationUtils.DATE_TIME_FORMATTER.print(newDate)}));
            }
            
        } catch (IllegalArgumentException e) {
            addActionError(getText("date.format", new String[]{"Date"}));
        }
        if (!expiry) {
            if (StringUtils.isEmpty(explanation)) {
                addActionError(getText("field.required", new String[]{"Note content"}));
            }
        }
        super.validate();
    }

    @Override
    public void onActionErrors() {
        if (queryId > 0) {
            query = queryService.find(queryId);
        }
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public Query getQuery() {
        return query;
    }

    public boolean isAffiliateAccess() {
        return affiliateAccess;
    }

    public void setAffiliateAccess(boolean affiliateAccess) {
        this.affiliateAccess = affiliateAccess;
    }

    public Date getDate() {
        return date;
    }

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
    public void setDate(Date date) {
        this.date = date;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public boolean isExpiry() {
        return expiry;
    }

    public void setExpiry(boolean expiry) {
        this.expiry = expiry;
    }

    private boolean isAllowed() {
        return securityService.isAllowedCheck(DSCL_PERMISSION)
                || securityService.isAllowedCheck(DMG_COORD_PERMISSION)
                || securityService.isAllowedCheck(DMG_SCI_PERMISSION)
                || securityService.isAllowedCheck(PS_PERMISSION);
    }

	public String getSourceSearch() {
		return sourceSearch;
	}

	public void setSourceSearch(String sourceSearch) {
		this.sourceSearch = sourceSearch;
	}
}
