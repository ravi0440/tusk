package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.service.QueryService;
import javax.annotation.Resource;

/**
 * This action enables to reopen query
 * <p/>
 * User: pruchnil
 */
public class ReopenQueryAction extends CommonActionSupport {
    private static final Logger logger = LoggerFactory.getLogger(ReopenQueryAction.class);
    private long queryId;
    private Query query;

    private QueryService queryService;

    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Override
    public String execute() throws Exception {
        query = queryService.find(queryId);
        return SUCCESS;
    }

    public String reopen() {
        return SUCCESS;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }
}
