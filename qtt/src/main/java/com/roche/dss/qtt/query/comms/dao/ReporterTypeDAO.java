package com.roche.dss.qtt.query.comms.dao;

import com.roche.dss.qtt.query.comms.dto.ReporterTypeDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface ReporterTypeDAO {

    public List<ReporterTypeDTO> getReporterTypes();

}
