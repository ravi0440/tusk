/* 
====================================================================
  $Header$
  @author $Author: komisarp $
  @version $Revision: 2453 $ $Date: 2012-06-27 16:07:46 +0200 (Śr, 27 cze 2012) $

====================================================================
*/
package com.roche.dss.qtt.query.comms.dto;


public class OrganisationDTO {
	
	private int id;
	private String name;
	private int typeId;
	private String typeDescription;
	
    
    /**
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @return
     */
    public int getTypeId() {
        return typeId;
    }

    /**
     * @param i
     */
    public void setId(int i) {
        id = i;
    }

    /**
     * @param string
     */
    public void setName(String string) {
        name = string;
    }

    /**
     * @param i
     */
    public void setTypeId(int i) {
        typeId = i;
    }

    /**
     * @return
     */
    public String getTypeDescription() {
        return typeDescription;
    }

    /**
     * @param string
     */
    public void setTypeDescription(String string) {
        typeDescription = string;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((typeDescription == null) ? 0 : typeDescription.hashCode());
		result = prime * result + typeId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if(!(obj instanceof OrganisationDTO))
			return false;
		OrganisationDTO other = (OrganisationDTO) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (typeDescription == null) {
			if (other.typeDescription != null)
				return false;
		} else if (!typeDescription.equals(other.typeDescription))
			return false;
		if (typeId != other.typeId)
			return false;
		return true;
	}

}
