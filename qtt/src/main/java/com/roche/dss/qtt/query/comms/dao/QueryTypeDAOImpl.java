package com.roche.dss.qtt.query.comms.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.roche.dss.qtt.query.comms.dao.impl.JDBCTemplateDAO;
import com.roche.dss.qtt.query.comms.dto.QueryTypeDTO;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
@Repository
public class QueryTypeDAOImpl extends JDBCTemplateDAO implements QueryTypeDAO {

    @Override
    public List<QueryTypeDTO> getQueryTypes() {
        String queryStr = "Select query_type_code, query_type from query_types order by query_type";
        return jdbcTemplate.query(queryStr, new QueryTypeRowMapper());
    }

     class QueryTypeRowMapper implements RowMapper<QueryTypeDTO> {

        @Override
        public QueryTypeDTO mapRow(ResultSet resultSet, int i) throws SQLException {
            QueryTypeDTO queryTypeDTO = new QueryTypeDTO();
            queryTypeDTO.setTypeId(resultSet.getString("query_type_code"));
            queryTypeDTO.setType(resultSet.getString("query_type"));
            return queryTypeDTO;
        }
    }

}
