package com.roche.dss.qtt.query.comms.dto;

import com.roche.dss.qtt.model.Query;

import java.io.Serializable;
import java.util.Properties;

/**
 * This DTO class contains sufficient fields to present email response
 * <p/>
 * User: pruchnil
 */
public class EmailDTO implements Serializable {

	private static final long serialVersionUID = -8423959657404067870L;

	private static final Logger logger = LoggerFactory.getLogger(EmailDTO.class);

    private long queryId;
    private String queryNumber;
    private String to;
    private String from;
    private String cc;
    private String subject;

    private String body;

    private long emailId;

    private String emailType;
    private String fileName1;
    private String fileName2;
    private String fileName3;
    private String dateSent;
    private String userSent;
    public long getQueryId() {
        return queryId;
    }


    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public String getQueryNumber() {
        return queryNumber;
    }

    public void setQueryNumber(String queryNumber) {
        this.queryNumber = queryNumber;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getEmailId() {
        return emailId;
    }

    public void setEmailId(long emailId) {
        this.emailId = emailId;
    }

    public String getEmailType() {
        return emailType;
    }

    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }

    public String getFileName1() {
        return fileName1;
    }

    public void setFileName1(String fileName1) {
        this.fileName1 = fileName1;
    }

    public String getFileName2() {
        return fileName2;
    }

    public void setFileName2(String fileName2) {
        this.fileName2 = fileName2;
    }

    public String getFileName3() {
        return fileName3;
    }

    public void setFileName3(String fileName3) {
        this.fileName3 = fileName3;
    }

    public String getDateSent() {
        return dateSent;
    }

    public void setDateSent(String dateSent) {
        this.dateSent = dateSent;
    }

    public String getUserSent() {
        return userSent;
    }

    public void setUserSent(String userSent) {
        this.userSent = userSent;
    }

    public void buildDTO(Query query, String address, Properties properties) {
        setQueryId(query.getQuerySeq());
        setQueryNumber(query.getQueryNumber());
        setTo(query.getRequester().getEmail());

        try {
            setFrom(properties.getProperty("qtt.initiate.dscl.email.address"));
            String queryLabel = "";
            if(StringUtils.isNotEmpty(query.getRequesterQueryLabel())){
                queryLabel = " - [" + query.getRequesterQueryLabel() + "]";
            }
            setSubject(properties.getProperty("qtt.response.query.answer.email.subject") + " "
                    + queryNumber + queryLabel);
            setBody(properties.getProperty("qtt.response.query.answer.email.bodyFirstPara") + "\n"
            		+ properties.getProperty("qtt.response.query.answer.email.bodySecondPara") + "\n\n"
                    + properties.getProperty("qtt.response.query.answer.email.bodyThirdPara") + "\n\n" + address
                    + "/qtt/QueryResponse.action?qno=" + queryId + "&key=" + query.getAccessKey() + "\n\n"
                    + properties.getProperty("qtt.response.query.answer.email.bodyFourthPara") + "\n"
                    + properties.getProperty("qtt.response.query.answer.email.bodyFifthPara") + "\n\n\n"
                    + properties.getProperty("qtt.response.query.answer.email.bodySixthPara") + "\n"
                    + properties.getProperty("qtt.response.query.answer.email.bodyLastPara"));

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
    
    public void buildDTO_DMGMIComplete(Query query, String toAddress, Properties properties) {
    	String queryId = String.valueOf(query.getQuerySeq());
        setQueryNumber(query.getQueryNumber());

        String queryLabel = "";
        if(StringUtils.isNotEmpty(query.getRequesterQueryLabel())){
            queryLabel = " - [" + query.getRequesterQueryLabel() + "]";
        }
        
        from = properties.getProperty("qtt.initiate.dscl.email.address");
        to = toAddress;
        
        subject = properties.getProperty("qtt.dmg.mi.complete.email.subject") + " " + queryNumber + queryLabel;

        body = properties.getProperty("qtt.dmg.mi.complete.email.bodyFirstPara") + " " + queryNumber + queryLabel + " \n\n\n"
			+ properties.getProperty("qtt.dmg.mi.complete.email.bodySecondPara") + "\n"
			+ properties.getProperty("drugsafety.qm.portal.url") // portal
                                                        // address
			+ "\n\n" + properties.getProperty("qtt.dmg.mi.complete.email.bodyThirdPara") + "\n\n\n"
			+ properties.getProperty("qtt.dmg.mi.complete.email.bodyFourthPara") + "\n\n"
			+ properties.getProperty("qtt.dmg.mi.complete.email.bodyLastPara");
    }

    public void buildDTO_DueAlert (Query query, String toAddress, String userName, Properties properties) {
    	String queryId = String.valueOf(query.getQuerySeq());

        String queryLabel = "";
        if(StringUtils.isNotEmpty(query.getRequesterQueryLabel())){
            queryLabel = " - [" + query.getRequesterQueryLabel() + "]";
        }
        
        from = properties.getProperty("qtt.initiate.dscl.email.address");
        to = toAddress;
        
        subject = properties.getProperty("qtt.alert.query.dueSoon.email.subject1") + " " + queryId + " "
        	+ properties.getProperty("qtt.alert.query.dueSoon.email.subject2") + queryLabel;

        body = properties.getProperty("qtt.alert.query.dueSoon.email.bodyOrigPosted") + " "  + userName
			+ " (" + toAddress + ") " + "\n\n"
			+ properties.getProperty("qtt.alert.query.dueSoon.email.bodyFirstPara") + " " + queryId + " "
			+ properties.getProperty("qtt.alert.query.dueSoon.email.bodySecondPara") + "\n\n\n"
			+ properties.getProperty("qtt.alert.query.dueSoon.email.bodyThirdPara") + "\n"
			+ properties.getProperty("drugsafety.qm.portal.url") // portal
                                                        // address
			+ "\n\n" + properties.getProperty("qtt.alert.query.dueSoon.email.bodyFourthPara") + "\n\n\n"
			+ properties.getProperty("qtt.alert.query.dueSoon.email.bodyFifthPara") + "\n\n"
			+ properties.getProperty("qtt.alert.query.dueSoon.email.bodyLastPara");
    }
    
    public void buildDTO_AvailabilityAlert(Query query, String toAddress, Properties properties) {
        String queryLabel = "";
        if(StringUtils.isNotEmpty(query.getRequesterQueryLabel())){
            queryLabel = " - [" + query.getRequesterQueryLabel() + "]";
        }
    	
        from = properties.getProperty("qtt.initiate.dscl.email.address");
        to = toAddress;
        
        subject = properties.getProperty("qtt.alert.query.availability.email.subject") + " " + query.getQueryNumber() + queryLabel;

        body = properties.getProperty("qtt.alert.query.availability.email.bodyFirstPara") + " " + query.getQueryNumber() + " "
                + properties.getProperty("qtt.alert.query.availability.email.bodySecondPara") + "\n\n\n"
                + properties.getProperty("qtt.alert.query.availability.email.bodyThirdPara") + "\n"
                + properties.getProperty("drugsafety.qm.portal.url") // portal
                                                                // address
                + "\n\n" + properties.getProperty("qtt.alert.query.availability.email.bodyFourthPara") + "\n\n\n"
                + properties.getProperty("qtt.alert.query.availability.email.bodyFifthPara") + "\n"
                + properties.getProperty("qtt.alert.query.availability.email.bodyLastPara");
    }


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((body == null) ? 0 : body.hashCode());
		result = prime * result + ((cc == null) ? 0 : cc.hashCode());
		result = prime * result
				+ ((dateSent == null) ? 0 : dateSent.hashCode());
		result = prime * result + (int) (emailId ^ (emailId >>> 32));
		result = prime * result
				+ ((emailType == null) ? 0 : emailType.hashCode());
		result = prime * result
				+ ((fileName1 == null) ? 0 : fileName1.hashCode());
		result = prime * result
				+ ((fileName2 == null) ? 0 : fileName2.hashCode());
		result = prime * result
				+ ((fileName3 == null) ? 0 : fileName3.hashCode());
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + (int) (queryId ^ (queryId >>> 32));
		result = prime * result
				+ ((queryNumber == null) ? 0 : queryNumber.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		result = prime * result
				+ ((userSent == null) ? 0 : userSent.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if(!(obj instanceof EmailDTO))
			return false;
		EmailDTO other = (EmailDTO) obj;
		if (body == null) {
			if (other.body != null)
				return false;
		} else if (!body.equals(other.body))
			return false;
		if (cc == null) {
			if (other.cc != null)
				return false;
		} else if (!cc.equals(other.cc))
			return false;
		if (dateSent == null) {
			if (other.dateSent != null)
				return false;
		} else if (!dateSent.equals(other.dateSent))
			return false;
		if (emailId != other.emailId)
			return false;
		if (emailType == null) {
			if (other.emailType != null)
				return false;
		} else if (!emailType.equals(other.emailType))
			return false;
		if (fileName1 == null) {
			if (other.fileName1 != null)
				return false;
		} else if (!fileName1.equals(other.fileName1))
			return false;
		if (fileName2 == null) {
			if (other.fileName2 != null)
				return false;
		} else if (!fileName2.equals(other.fileName2))
			return false;
		if (fileName3 == null) {
			if (other.fileName3 != null)
				return false;
		} else if (!fileName3.equals(other.fileName3))
			return false;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (queryId != other.queryId)
			return false;
		if (queryNumber == null) {
			if (other.queryNumber != null)
				return false;
		} else if (!queryNumber.equals(other.queryNumber))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		if (userSent == null) {
			if (other.userSent != null)
				return false;
		} else if (!userSent.equals(other.userSent))
			return false;
		return true;
	}
}
