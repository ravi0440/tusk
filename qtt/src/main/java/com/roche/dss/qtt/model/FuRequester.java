package com.roche.dss.qtt.model;
// Generated 2010-10-04 12:00:38 by Hibernate Tools 3.3.0.GA

/**
 * FuRequester generated by hbm2java
 */
@Entity
@Table(name = "FU_REQUESTERS")
public class FuRequester implements java.io.Serializable {

	private static final long serialVersionUID = 3107960056128305666L;
	
	private FuRequesterId id;
	private String firstName;
	private String surname;
	private long organisationId;
	private String telephone;
	private DateTime modifiedTs;
	private String email;
	private String username;
	private String faxNumber;
    private QttCountry country;

    private FuOrganisation organisation;

	public FuRequester() {
	}

    public FuRequester(Requester requester, long followupSeq, FuOrganisation fuOrganisation) {
        Validate.notNull(requester);
        Validate.notNull(requester.getOrganisation());
        this.id = new FuRequesterId(followupSeq, requester.getRequesterId());
		this.firstName = requester.getFirstName();
		this.surname = requester.getSurname();
        this.organisationId = requester.getOrganisation().getOrganisationId();
		this.telephone = requester.getTelephone();
		this.modifiedTs = requester.getModifiedTs();
		this.email = requester.getEmail();
        this.username = requester.getUsername();
        this.faxNumber = requester.getFaxNumber();
        this.country = requester.getCountry();
        this.organisation = fuOrganisation;
    }

    @EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "followupSeq", column = @Column(name = "FOLLOWUP_SEQ", nullable = false, precision = 10, scale = 0)),
			@AttributeOverride(name = "requesterId", column = @Column(name = "REQUESTER_ID", nullable = false, precision = 10, scale = 0)) })
	public FuRequesterId getId() {
		return this.id;
	}

	public void setId(FuRequesterId id) {
		this.id = id;
	}

	@Column(name = "FIRST_NAME", nullable = false, length = 25)
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "SURNAME", nullable = false, length = 50)
	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@Column(name = "ORGANISATION_ID", nullable = false, precision = 10, scale = 0)
	public long getOrganisationId() {
		return this.organisationId;
	}

	public void setOrganisationId(long organisationId) {
		this.organisationId = organisationId;
	}

	@Column(name = "TELEPHONE", nullable = false, length = 25)
	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Type(type="com.roche.dss.util.PersistentDateTime")
	@Column(name = "MODIFIED_TS", nullable = false, length = 7)
	public DateTime getModifiedTs() {
		return this.modifiedTs;
	}

	public void setModifiedTs(DateTime modifiedTs) {
		this.modifiedTs = modifiedTs;
	}

	@Column(name = "EMAIL", nullable = false, length = 50)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "USERNAME", length = 30)
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "FAX_NUMBER", length = 25)
	public String getFaxNumber() {
		return this.faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COUNTRY_CODE", nullable = false)
    public QttCountry getCountry() {
        return country;
    }

    public void setCountry(QttCountry country) {
        this.country = country;
    }

    @Transient
	public FuOrganisation getOrganisation() {
		return this.organisation;
	}

	public void setOrganisation(FuOrganisation organisation) {
		this.organisation = organisation;
	}

}
