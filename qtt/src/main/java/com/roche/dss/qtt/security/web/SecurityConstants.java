/*
====================================================================
  $Header$
  @author $Author: jayaramk $
  @version $Revision: 272 $ $Date: 2004-07-05 14:31:21 +0200 (Pn, 05 lip 2004) $

====================================================================
*/
package com.roche.dss.qtt.security.web;

public interface SecurityConstants {

    public static final String USER_PERMISSIONS_SESSION_KEY = "userPermissions";

    //Access Denied forward
    public static final String ACCESS_DENIED = "accessDenied";

    //Application and permissions related constants
    public static final String APPLICATION_CODE = "ADMIN";
    public static final String APPLICATION_NAME = "SECURITY ADMIN SCREENS";
    public static final String ADMIN_PERM_CODE = "ADMIN";
    public static final String ADMIN_PERM_NAME = "ADMINISTRATOR";

    //Portal
    public static final String PORTAL = "PORTAL";

    //Authentication related constants
    public static final String SSO_COOKIE_NAME = "ONTCred";
    public static final String USER_COOKIE = "DSSAuthCookie";
    public static final String SESSION_USER_KEY = "DSSUser";
    public static final String COOKIE_DOMAIN = ".roche.com";
    public static final String ACTIVE_DIRECTORY = "roche";
    public static final String LDAP_SERVER =
        "ldap://adgc.bas.roche.com:3268/dc=roche,dc=com";
    //public static final String LDAP_SERVER = "ldap://rbamsroott01.rochetest.com:3268/dc=rochetest,dc=com";
    public static final String LOGOUT_PATH = "/dsoportal/logout.do";

    //Base Directory path for property and other config files
    public static final String DSS_CONFIG_DIRECTORY = "dss.conf.dir";

    //Location of property files
    public static final String PROPERTIES_DIRECTORY =
        new StringBuffer()
            .append("security")
            .append(java.io.File.separator)
            .append("config")
            .append(java.io.File.separator)
            .append("properties")
            .append(java.io.File.separator)
            .toString();

	public static final String ADD_TEAMS_PATH = "getTeamsBusinessUnitsList";
}
