package com.roche.dss.qtt.query.web.model;

import java.util.Date;

@Conversion()
public class SummaryListParams {

    private String owner;
	private String[] activity;
	private String[] queryType;
	private Date from;
	private Date to;

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String[] getActivity() {
		return activity;
	}

	public void setActivity(String[] activity) {
		this.activity = activity;
	}

	public String[] getQueryType() {
		return queryType;
	}

	public void setQueryType(String[] queryType) {
		this.queryType = queryType;
	}

	public Date getFrom() {
		return from;
	}

	@TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	@TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setTo(Date to) {
		this.to = to;
	}

}
