package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.Audit;

public interface AuditEAO extends QueryRelatedEAO<Audit>{
}
