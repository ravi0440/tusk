package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.Manufacturing;

public interface ManufacturingEAO extends QueryRelatedEAO<Manufacturing>{
}
