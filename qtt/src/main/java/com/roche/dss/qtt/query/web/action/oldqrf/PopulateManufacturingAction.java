package com.roche.dss.qtt.query.web.action.oldqrf;

import java.util.List;

import com.roche.dss.qtt.model.AeTerm;
import com.roche.dss.qtt.model.ClinicalTrial;
import com.roche.dss.qtt.model.CtrlOformat;
import com.roche.dss.qtt.model.Manufacturing;
import com.roche.dss.qtt.query.comms.dto.ClinicalTrialDTO;
import com.roche.dss.qtt.query.comms.dto.QueryDTO;
import com.roche.dss.qtt.query.comms.dto.QueryDescDTO;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.model.QueryDescFormModel;
import com.roche.dss.qtt.service.QrfService;

import javax.annotation.Resource;

public class PopulateManufacturingAction extends QRFAbstractAction {

    private QrfService qrfService;

    private Manufacturing manufacturing = new Manufacturing();
    
    private ClinicalTrial clinical = new ClinicalTrial();
    
    private String aeTerm;
	
    private String specialRequest;

    @Resource
    public void setQrfService(QrfService qrfService) {
		this.qrfService = qrfService;
	}
    QueryDescFormModel queryForm = new QueryDescFormModel();

    @Override
    public Object getModel() {
    	if (getSession().get(ActionConstants.QRF_EDIT_QUERY_TYPE) != null) {
            int queryId = ((Integer) getSession().get(ActionConstants.QRF_QUERY_ID)).intValue();
            QueryDTO query = qrfService.openQuery(queryId, null, true);
            queryForm.setNatureofcase(query.getQueryDesc().getNatureOfCase());
            queryForm.setOtherinfo(query.getQueryDesc().getOtherInfo());
            manufacturing = query.getManufacturing();
            clinical = toClinicalTrial(query.getClinical());
            aeTerm = query.getAeTerm();
            specialRequest = query.getSpecialRequest();
            getSession().put(ActionConstants.FRM_QUERY_DESC, queryForm);
        }
        return queryForm;
    }

    private ClinicalTrial toClinicalTrial(ClinicalTrialDTO clinicalDto) {
    	//TODO Move elsewhere
    	ClinicalTrial trial = new ClinicalTrial();
    	trial.setCtrlOformat(new CtrlOformat());
    	trial.getCtrlOformat().setCtrlOformatId(clinicalDto.getOutputFormatId());
		return trial;
	}

    public String execute()  {
        return SUCCESS;
    }

	public List<CtrlOformat> getClotypes() {
        return qrfService.getCtrlOformats();
    }

	public Manufacturing getManufacturing() {
		return manufacturing;
	}

	public void setManufacturing(Manufacturing manufacturing) {
		this.manufacturing = manufacturing;
	}

	public ClinicalTrial getClinical() {
		return clinical;
	}

	public void setClinical(ClinicalTrial clinical) {
		this.clinical = clinical;
	}

	public String getSpecialRequest() {
		return specialRequest;
	}

	public void setSpecialRequest(String specialRequest) {
		this.specialRequest = specialRequest;
	}

	public String getAeTerm() {
		return aeTerm;
	}

	public void setAeTerm(String aeTerm) {
		this.aeTerm = aeTerm;
	}
}
