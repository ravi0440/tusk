package com.roche.dss.qtt.query.comms.dao;

import com.roche.dss.qtt.query.comms.dto.QueryTypeDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface QueryTypeDAO {
    List<QueryTypeDTO> getQueryTypes();
}
