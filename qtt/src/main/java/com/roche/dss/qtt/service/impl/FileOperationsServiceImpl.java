package com.roche.dss.qtt.service.impl;

import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.web.model.FileUploadBean;
import com.roche.dss.qtt.service.FileOperationsService;
import java.io.*;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Helper class to assist with file-system operations.
 *
 * @author wisbeya
 */

@Service("fileOpsService")
public class FileOperationsServiceImpl implements FileOperationsService {
    private static final Logger log = LoggerFactory.getLogger(FileOperationsServiceImpl.class);
    private static final String SEPARATOR = System.getProperty("file.SEPARATOR", "/");

    // follow singleton pattern
    //private static FileOperationsServiceImpl singleton = null;
    private String baseDir;

    private Integer outputBufferSize;

    // private 0-arg ctor
//    public FileOperationsServiceImpl() {
//        // read system property values
//        SEPARATOR = System.getProperty("file.SEPARATOR", "/");
//
//    }


    @Value("${qtt.attachment.download.buffer.size}")
    public void setOutputBufferSize(Integer outputBufferSize) {
        this.outputBufferSize = outputBufferSize;
    }

    @Value("${qtt.repository.baseDirectory}")
    public void setBaseDir(String baseDir) {
        this.baseDir = baseDir;
    }

    /**
     * Returns an appropriate base directory (including final SEPARATOR) which
     * is the appropriate location for storing file attachments to be associated with
     * the specified query.<br>
     * <br>
     * e.g. "<DOC_BASE>/123/"
     *
     * @param section the internal identifier for the query.
     * @return directory (terminated with final SEPARATOR).
     */
    @Override
	public String findDestinationDirectory(String section) {
        return baseDir + SEPARATOR + section + SEPARATOR;
    }


    /**
     * Returns an input stream that can be used to read the contents of the
     * specified attachment as input.
     *
     * @param internalQueryId the internal identifier for the query.
     * @param fileName        the name of the attached document.
     * @return an InputStream that can be used to read the document.
     * @throws Exception
     */
    @Override
	public InputStream getInputStream(int internalQueryId,
                                      String fileName)
            throws Exception {
        InputStream stream = null;
        String filename = null;

        try {
            filename = this.findDestinationDirectory(
                    Integer.toString(internalQueryId)) + fileName;
            stream = new FileInputStream(filename);
        } catch (FileNotFoundException fnfe) {
            log.error("Could not locate file: " + fnfe.getMessage());
            throw fnfe;
        }

        return stream;
    }


    /**
     * Returns an output stream that can be used to write the contents of an
     * uploaded attachment as output.  The filename is preserved from the uploaded
     * document.
     *
     * @param internalQueryId the internal identifier for the query.
     * @param filename        filename of the uploaded attached document.
     * @return an OutputStream that can be used to write the document.
     * @throws Exception
     */
    public OutputStream getOutputStream(String internalQueryId,
                                        StringBuffer filename)
            throws Exception {
        OutputStream stream = null;
        File target = this.getDestinationFileRef(internalQueryId, filename);

        stream = new FileOutputStream(target);
        return stream;
    }


    /**
     * Copies binary content between two streams, using a pre-defined buffer size.
     *
     * @param input  input stream.
     * @param output output stream.
     * @throws java.io.IOException
     */
    @Override
	public void streamAttachment(InputStream input,
                                 OutputStream output)
            throws IOException {
        byte buffer[] = new byte[outputBufferSize];
        int bytesRead = 0;

        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    @Override
    public void correctFileNameOfAttachmentIfNeeded(Query query, FileUploadBean attachment) {
        Set<Attachment> attachments = query.getAttachments();

        String[] splitted = attachment.getUploadFileName().split("\\.");
        String originalSuffix = splitted[splitted.length - 1];
        String originalName = attachment.getUploadFileName().substring(0, attachment.getUploadFileName().lastIndexOf("."));
        int index = 1;

        boolean correctName = false;

        for (Attachment att : attachments) {
            if (att.getFilename().equals(attachment.getUploadFileName())) {
                attachment.setUploadFileName(originalName + index + "." + originalSuffix);
                correctName = true;
                break;
            }
        }

    }


    /**
     * Validates that a suitable destination directory and file-name can be
     * created for the specified query.
     */
    public File getDestinationFileRef(String internalQueryId,
                                      StringBuffer fullFileName)
            throws Exception {
        String destination = this.findDestinationDirectory(internalQueryId);

        // validate the parent directory for the specified query exists
        File destDir = new File(destination);
        File destFile = null;
        String destinationPath = null;
        String subDirName = null;
        try {
            // if directory does not exist, attempt to create
            if (!destDir.exists()) {
                log.debug("Destination directory '" + destDir
                        + "' does not exist - attempting to create");

                if (!destDir.mkdir()) {
                    throw new FileNotFoundException("can not create directory: " + destination);
                }
                log.debug("Directory created successfully");
            }
        } catch (SecurityException se) {
            log.error("SecurityManager denied write access to directory '"
                    + destDir + "': " + se.getMessage());
            throw new Exception(se.getMessage());
        }


        destinationPath = destination + fullFileName.toString();

        // validate that the destination file is writable
        destFile = new File(destinationPath);
        try {
            // if destination file does not exist, then create new file
            if (!destFile.exists()) {
                destFile.createNewFile();
            } else {
                destFile = null;
                //create a sub dir to store the file
                subDirName = getNextSubDirName(destination) + SEPARATOR;
                destinationPath = destination + subDirName;
                File destSubDir = new File(destinationPath);
                destSubDir.mkdir();
                destFile = new File(destinationPath + fullFileName.toString());
                destFile.createNewFile();
                fullFileName.insert(0, subDirName);
                log.info("File already exists in query, stored in sub dir, releative path/filename:" + fullFileName);
            }
        } catch (SecurityException se) {
            log.error("SecurityManager denied write access to file '"
                    + destFile + "': " + se.getMessage());
            throw new Exception(se.getMessage());
        } catch (IOException ioe) {
            log.error("Unable to create file '" + destFile + "': " + ioe.getMessage());
            throw new Exception(ioe.getMessage());
        }
        return destFile;
    }


    /**
     * Deletes all files and subdirectories under dir and finally delete the directory itself
     * If a deletion fails, the method stops attempting to delete and returns false.
     *
     * @param dir the File directory
     * @return true if all deletions were successful
     * @throws Exception
     */
    @Override
	public boolean deleteDir(File dir) throws Exception {

        try {

            if (dir.exists()) {

                if (dir.isDirectory()) {

                    log.debug("Directory/File found");
                    String[] children = dir.list();
                    for (int i = 0; i < children.length; i++) {

                        boolean success = deleteDir(new File(dir, children[i]));
                        if (!success) {
                            return false;
                        }
                    }
                    log.debug("Directory deleted successfully");
                }
            } else {
                //if this is not a directory
                log.debug("The directory or file does not exists!");
                return true;
            }

            // The directory is now empty so delete it
            return dir.delete();
        } catch (Exception e) {
            log.debug("Error deleting directory or file" + e);
            throw e;
        }
    }

    /**
     * returns an appropriate MIME-type for a given file, by examining the filename
     * extension.
     *
     * @param filename name of the specified file.
     * @return string denoting an appropriate MIME-type
     */
    @Override
	public String getMimeType(String filename) {
        String retVal = null;

        int indx = filename.lastIndexOf(".");
        String extension = filename.substring(indx + 1);

        if ("doc".equals(extension)) {
            retVal = "application/msword";
        } else if ("xls".equals(extension)) {
            retVal = "application/msexcel";
        } else if ("ppt".equals(extension)) {
            retVal = "application/mspowerpoint";
        } else if ("pdf".equals(extension)) {
            retVal = "application/pdf";
        } else if ("txt".equals(extension)) {
            retVal = "text/plain";
        } else if ("xml".equals(extension)) {
            retVal = "text/plain";
        } else if ("zip".equals(extension)) {
            retVal = "application/zip";
        } else if ("jpg".equals(extension)) {
            retVal = "image/jpeg";
        } else if ("tif".equals(extension)) {
            retVal = "image/tiff";
        } else if ("rtf".equals(extension)) {
            retVal = "application/rtf";
        } else {
            retVal = "plain/text";
        }

        log.debug("File extension (" + extension + ") leads to a MIME-type of " + retVal);
        return retVal;
    }


    /**
     * If a file already exists then this method returns a sub directory name
     * for the file so that it could keep the same-name file in different sub directory
     * in query directory
     *
     * @param destDirPath
     * @return
     */
    public String getNextSubDirName(String destDirPath) {

        File currentDir = new File(destDirPath);
        // Loop thru files and directories in this path
        String[] dirList = currentDir.list();
        //default sub directory name
        String subDirName = "1";

        //find the maximum sub directory number
        List list = new LinkedList();
        for (int i = 0; i < dirList.length; i++) {
            File listItem = new File(destDirPath, dirList[i]);
            if (listItem.isDirectory()) {
                list.add(new Integer(dirList[i]));
            }
        }
        //if there is already sub directory then find next sub directory
        if (!list.isEmpty()) {
            int max = ((Integer) Collections.max(list)).intValue();
            subDirName = String.valueOf(max + 1);
        }

        return subDirName;

    }

    @Override
	public boolean insertFileIntoRepository(String queryId, File file, StringBuffer fileName)
            throws IOException {

        boolean retVal = false;

        InputStream inputStream = null;
        OutputStream outputStream = null;

        try {
            // get handle on input stream to attachment
            inputStream = new FileInputStream(file);

            // get handle on output stream to target destination
            outputStream = getOutputStream(queryId, fileName);

            // stream data from source to target streams
            streamAttachment(inputStream, outputStream);

            retVal = true;
        } catch (Exception e) {
            log.error("Error occurred while streaming attachment to repository: "
                    + e.getMessage(), e);
            retVal = false;
        } finally {
            if (outputStream != null) {
                outputStream.flush();
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }

        return retVal;
    }
}

