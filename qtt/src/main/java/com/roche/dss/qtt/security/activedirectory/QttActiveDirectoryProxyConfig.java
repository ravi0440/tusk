package com.roche.dss.qtt.security.activedirectory;

import javax.naming.Context;
import java.util.HashMap;
import java.util.Map;


@Component
public class QttActiveDirectoryProxyConfig implements ActiveDirectoryProxyConfig {

    @Override
    public String getGroupBaseDN() {
        return "ou=Projects,ou=Groups,dc=emea,DC=roche,DC=com";
    }

    @Override
    public Map getInitialContextEnv() {
        Map env = new HashMap();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldap://rkamsemea01.emea.roche.com:3268");
        env.put(Context.SECURITY_PRINCIPAL, "pvdbldap@emea.roche.com");
        env.put(Context.SECURITY_CREDENTIALS, "zVBWGQ3mrRwt");
        return env;
    }

    @Override
    public String getMembershipFilter() {
        return "(&(member=?member)(objectclass=group)(sAMAccountName=GLOXPortal*))";
    }

    @Override
    public String getUserBaseDN() {
        return "DC=roche,DC=com";
    }

    @Override
    public String getUserFilter() {
        return "(&(cn=?user)(objectclass=user))";
    }

    @Override
    public boolean isEmptyPasswordAllowed() {
        return false;
    }

    @Override
    public boolean isRecursionEnabled() {
        return false;
    }

};
