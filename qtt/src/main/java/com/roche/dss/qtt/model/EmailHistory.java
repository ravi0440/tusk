package com.roche.dss.qtt.model;

@Entity
@Table(name = "EMAIL_HISTORY")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class EmailHistory implements java.io.Serializable {

    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    @SequenceGenerator(name = "EMAIL_HISTORY_GENERATOR", sequenceName = "EMAIL_HISTORY_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMAIL_HISTORY_GENERATOR")
    private Long id;

    @Type(type="com.roche.dss.util.PersistentDateTime")
    @Column(name = "SENT_DATE", nullable = false, length = 7)
    private DateTime sentDate;

    @Column(name = "SUBJECT")
    private String subject;

    @Column(name = "CONTENT")
    private String content;

    @Column(name = "SENT_TO")
    private String sentTo;

    @Column(name = "SENT_FROM")
    private String sentFrom;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DateTime getSentDate() {
        return sentDate;
    }

    public void setSentDate(DateTime sentDate) {
        this.sentDate = sentDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSentTo() {
        return sentTo;
    }

    public void setSentTo(String sentTo) {
        this.sentTo = sentTo;
    }

    public String getSentFrom() {
        return sentFrom;
    }

    public void setSentFrom(String sentFrom) {
        this.sentFrom = sentFrom;
    }


}
