package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.*;

import java.util.List;

/**
 * User: pruchnil
 */
public interface FollowupQueryEAO extends BaseEAO<FuQuery> {
    FuRequester getRequesterById(FuRequesterId fuRequesterId);

    FuRetrievalPeriod getRetrievalPeriodById(FuRetrievalPeriodId fuRetrievalPeriodId);

    FuQueryDescription getQueryDescriptionById(FuQueryDescriptionId fuQueryDescriptionId);

    List<FuCase> getCasesById(FuCaseId fuCaseId);

    List<FuPreQueryPreparation> getPreQueryPreparationsById(FuPreQueryPreparationId fuPreQueryPreparationId);

    FuDrug getDisplayDrug(long followupId, long queryId);

    void mergeFuDrug(FuDrug drug);

    void mergeFuRetrievalPeriod(FuRetrievalPeriod fuRetrievalPeriod);

    void mergeFuPregnancy(FuPregnancy fuPregnancy);

    void mergeFuClinicalTrial(FuClinicalTrial fuClinicalTrial);

    void mergeFuCase(FuCase fuCase);

    void mergeFuAeTerm(FuAeTerm fuAeTerm);

    void mergeFuQueryDescription(FuQueryDescription fuQueryDescription);

    void mergeFuPerformanceMetric(FuPerformanceMetric fuPerformanceMetric);

    void mergeFuRequester(FuRequester fuRequester);

    void mergeFuOrganisation(FuOrganisation fuOrganisation);

    void mergeFuLabellingDocument(FuLabellingDocument doc);

    void mergeFuPreQueryPreparation(FuPreQueryPreparation fuPreQueryPreparation);
}
