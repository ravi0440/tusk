package com.roche.dss.qtt.query.web.model;

import java.util.Date;

@Conversion()
public class QuickSearchParams {
	
    public static final String SCOPE_BOTH = "both";
    public static final String SCOPE_DB = "db";
    public static final String SCOPE_ATT = "att";
	
	protected String term;
	
	protected String termParsed;

	protected String searchScope;
	
	protected Date periodDateFrom;
	
	protected Date periodDateTo;
	
	protected Date qryResponseDateFrom;
	
	protected Date qryResponseDateTo;
	
	protected boolean isValid = true;
	
	/**
	 * Used by Struts to create the object
	 */
	public QuickSearchParams() {
	}
	
	/**
	 * Used by Unit Tests only
	 * 
	 * @param term
	 */
	public QuickSearchParams(String term) {
		this.term = term;
	}
	
	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getSearchScope() {
		return searchScope;
	}

	public void setSearchScope(String searchScope) {
		this.searchScope = searchScope;
	}

	public boolean getIsValid() {
		return isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getTermParsed() {
		return termParsed;
	}

	public void setTermParsed(String termParsed) {
		this.termParsed = termParsed;
	}
	
	public Date getPeriodDateFrom() {
		return periodDateFrom;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setPeriodDateFrom(Date periodDateFrom) {
		this.periodDateFrom = periodDateFrom;
	}

	public Date getPeriodDateTo() {
		return periodDateTo;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setPeriodDateTo(Date periodDateTo) {
		this.periodDateTo = periodDateTo;
	}

	public Date getQryResponseDateFrom() {
		return qryResponseDateFrom;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setQryResponseDateFrom(Date qryResponseDateFrom) {
		this.qryResponseDateFrom = qryResponseDateFrom;
	}

	public Date getQryResponseDateTo() {
		return qryResponseDateTo;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setQryResponseDateTo(Date qryResponseDateTo) {
		this.qryResponseDateTo = qryResponseDateTo;
	}

	protected Integer stringToInteger(String str) {
		if (str == null || "".equals(str.trim())) {
			return null;
		} else {
			return new Integer(Integer.parseInt(str));
		}
	}
}
