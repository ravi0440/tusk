/* 
====================================================================
  $Header$
  @author $Author: komisarp $
  @version $Revision: 2453 $ $Date: 2012-06-27 16:07:46 +0200 (Śr, 27 cze 2012) $

====================================================================
*/
package com.roche.dss.qtt.query.comms.dto;



public class RequesterDTO {
	
	private String firstName;
	private String lastName;
	private String telephone;
	private String fax;
	private String emailAddress;
	private String country;
	private OrganisationDTO org;
	private String countryName;
	private String countryCode;

    /**
     * @return
     */
    public String getCountry() {
        return country;
    }

    /**
     * @return
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @return
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * @return
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @return
     */
    public String getFax() {
        return fax;
    }

    /**
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @return
     */
    public OrganisationDTO getOrg() {
        return org;
    }

    /**
     * @return
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param string
     */
    public void setCountry(String string) {
        country = string;
    }

    /**
     * @param string
     */
    public void setCountryCode(String string) {
        countryCode = string;
    }

    /**
     * @param string
     */
    public void setCountryName(String string) {
        countryName = string;
    }

    /**
     * @param string
     */
    public void setEmailAddress(String string) {
        emailAddress = string;
    }

    /**
     * @param string
     */
    public void setFax(String string) {
        fax = string;
    }

    /**
     * @param string
     */
    public void setFirstName(String string) {
        firstName = string;
    }

    /**
     * @param string
     */
    public void setLastName(String string) {
        lastName = string;
    }

    /**
     * @param organisation
     */
    public void setOrg(OrganisationDTO organisation) {
        org = organisation;
    }

    /**
     * @param string
     */
    public void setTelephone(String string) {
        telephone = string;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result
				+ ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result
				+ ((countryName == null) ? 0 : countryName.hashCode());
		result = prime * result
				+ ((emailAddress == null) ? 0 : emailAddress.hashCode());
		result = prime * result + ((fax == null) ? 0 : fax.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((org == null) ? 0 : org.hashCode());
		result = prime * result
				+ ((telephone == null) ? 0 : telephone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof RequesterDTO))
			return false;
		RequesterDTO other = (RequesterDTO) obj;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (countryName == null) {
			if (other.countryName != null)
				return false;
		} else if (!countryName.equals(other.countryName))
			return false;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		if (fax == null) {
			if (other.fax != null)
				return false;
		} else if (!fax.equals(other.fax))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (org == null) {
			if (other.org != null)
				return false;
		} else if (!org.equals(other.org))
			return false;
		if (telephone == null) {
			if (other.telephone != null)
				return false;
		} else if (!telephone.equals(other.telephone))
			return false;
		return true;
	}
}
