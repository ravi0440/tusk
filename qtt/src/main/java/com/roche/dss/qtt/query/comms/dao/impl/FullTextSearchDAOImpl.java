package com.roche.dss.qtt.query.comms.dao.impl;

import javax.annotation.PostConstruct;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.comms.dao.FullTextSearchDAO;
import com.roche.dss.qtt.service.SearchResults;
import com.roche.dss.util.ApplicationUtils;

@Repository("fullTextSearchDAO")
public class FullTextSearchDAOImpl implements FullTextSearchDAO {
    @PersistenceContext
    protected EntityManager entityManager;
    
    protected JpaTemplate jpaTemplate;
    
    public static String SEARCH_FIELDS_BOTH[];
    
    static {
    	SEARCH_FIELDS_BOTH = ApplicationUtils.concatenateArrays(SEARCH_FIELDS, SEARCH_ATTACH_FIELDS);
    }
    
    protected static final Logger log = LoggerFactory.getLogger(FullTextSearchDAOImpl.class);
        
    @PostConstruct
    public void init() {
        jpaTemplate = new JpaTemplate(entityManager);
    }
    
    @Override
	@SuppressWarnings("unchecked")
    public SearchResults searchFT(final String keywords, final String[] fields) {
    	SearchResults results = jpaTemplate.execute(new JpaCallback<SearchResults>() {
            public SearchResults doInJpa(EntityManager em) throws PersistenceException {

               FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

               MultiFieldQueryParser parser = new MultiFieldQueryParser(Version.LUCENE_29, fields, new StandardAnalyzer(Version.LUCENE_29) );
               parser.setDefaultOperator(QueryParser.OR_OPERATOR);
               org.apache.lucene.search.Query q;
               try
               {
                   q = parser.parse( keywords );
                   FullTextQuery fq = fullTextEntityManager.createFullTextQuery(q, Query.class);
                   fq.setHint(QueryHints.HINT_CACHEABLE, true);
                   fq.setProjection(FullTextQuery.THIS, FullTextQuery.DOCUMENT);
                   
                   return new SearchResults (fq.getResultList(), fq.getResultSize(), q, keywords);
               }
               catch (ParseException e)
               {
                   // FIXME propagate the exception
                   log.warn("Exception for keywords: " + keywords, e);
               }
               return null;
            }
         });
         return results;	
    }
	
	@Override
	public SearchResults searchDatabase(final String keywords) {
		return searchFT(keywords, SEARCH_FIELDS);
	}
	
	@Override
	public SearchResults searchAttachments(final String keywords) {
		return searchFT(keywords, SEARCH_ATTACH_FIELDS);
	}
	
	@Override
	public SearchResults searchBoth(final String keywords) {
		return searchFT(keywords, SEARCH_FIELDS_BOTH);
	}
}
