package com.roche.dss.qtt.security.utils.storeprocedures.user;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;


import com.roche.dss.qtt.security.utils.storeprocedures.DSSStoreProceduresConstants;

/**
 * @author zerkowsm
 *
 */
public class DSSUserStoredProcedure extends StoredProcedure implements DSSStoreProceduresConstants{
    
    private static final String SQL = "user_service.get_user";

    public DSSUserStoredProcedure(DataSource ds) {
        setDataSource(ds);
        setFunction(true);
        setSql(SQL);
        declareParameter(new SqlOutParameter(USER_SERVICE_GET_USER_OUT_PARAM, OracleTypes.CURSOR, new DSSUserMapper()));
        declareParameter(new SqlParameter("p_username", Types.VARCHAR));
        
        compile();
    }

    public Map<String, Object> execute(String username) {
    	Map<String, Object> inputs = new HashMap<String, Object>();
    	inputs.put("p_username", username.toUpperCase());
        return super.execute(inputs);
    }
}