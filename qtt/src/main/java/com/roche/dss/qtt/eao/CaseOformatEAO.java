package com.roche.dss.qtt.eao;


import java.util.List;

import com.roche.dss.qtt.model.CaseOformat;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface CaseOformatEAO extends BaseEAO<CaseOformat>{

	List<CaseOformat> getAllActive();
}
