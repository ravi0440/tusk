
package com.roche.dss.qtt.query.web.qrf.model;

public class ClinicalFormModel {

	private String protocolno = null;
	private String crtnno = null;
	private String patientno = null;
	private String caseselection = null;
	private int outputformat;
	private String special = null;

    /**
     * @return
     */
    public String getCaseselection() {
        return caseselection;
    }

    /**
     * @return
     */
    public String getCrtnno() {
        return crtnno;
    }

    /**
     * @return
     */
    public int getOutputformat() {
        return outputformat;
    }

    /**
     * @return
     */
    public String getPatientno() {
        return patientno;
    }

    /**
     * @return
     */
    public String getProtocolno() {
        return protocolno;
    }

    /**
     * @return
     */
    public String getSpecial() {
        return special;
    }

    /**
     * @param string
     */
    public void setCaseselection(String string) {
        caseselection = string;
    }

    /**
     * @param string
     */
    public void setCrtnno(String string) {
        crtnno = string;
    }

    /**
     * @param id
     */
    public void setOutputformat(int id) {
        outputformat = id;
    }

    /**
     * @param string
     */
    public void setPatientno(String string) {
        patientno = string;
    }

    /**
     * @param string
     */
    public void setProtocolno(String string) {
        protocolno = string;
    }

    @FieldExpressionValidator(expression = "special != null && special!='' || outputformat != 4", key = "clinicalForm.special.displayname")
    public void setSpecial(String string) {
        special = string;
    }


}
