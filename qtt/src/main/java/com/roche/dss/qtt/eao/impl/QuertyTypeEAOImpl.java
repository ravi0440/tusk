package com.roche.dss.qtt.eao.impl;

import java.util.List;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.QueryTypeEAO;
import com.roche.dss.qtt.model.QueryType;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */

@Repository("queryTypeEAO")
public class QuertyTypeEAOImpl extends AbstractBaseEAO<QueryType> implements QueryTypeEAO {

	@Override
	public List<QueryType> getActive() {
		return entityManager.createNamedQuery("list.activeQueryTypes", QueryType.class).getResultList();
	}
}
