package com.roche.dss.qtt.query.web.model;

import java.util.Date;

@Conversion()
public class TaskListParams {

	private String owner;

	private Date from;
    
	private Date to;
	
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Date getFrom() {
		return from;
	}

	@TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	@TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setTo(Date to) {
		this.to = to;
	}

}
