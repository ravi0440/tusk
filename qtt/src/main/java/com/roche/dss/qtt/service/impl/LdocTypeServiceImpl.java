package com.roche.dss.qtt.service.impl;

import java.util.Collection;

import com.roche.dss.qtt.eao.LdocTypeEAO;
import com.roche.dss.qtt.model.LdocType;
import com.roche.dss.qtt.service.LdocTypeService;

/**
 * @author zerkowsm
 * 
 */
@Service("ldocTypeService")
@Transactional
public class LdocTypeServiceImpl implements LdocTypeService {

	@Autowired
	protected LdocTypeEAO ldocTypeEAO;

	@Override
	public void persist(LdocType entity) {
		ldocTypeEAO.persist(entity);
	}

	@Override
	public void persist(Collection<LdocType> entities) {
		ldocTypeEAO.persist(entities);
	}

	@Override
	public LdocType merge(LdocType entity) {
		return ldocTypeEAO.merge(entity);
	}

	@Override
	public void remove(LdocType entity) {
		ldocTypeEAO.remove(entity);
	}

	@Override
	public void flush() {
		ldocTypeEAO.flush();
	}

	@Override
	public void clear() {
		ldocTypeEAO.clear();
	}

	@Override
	public LdocType find(Long id) {
		return ldocTypeEAO.find(id);
	}

	@Override
	public LdocType getReference(Long id) {
		return ldocTypeEAO.getReference(id);
	}

	@Override
	public LdocType get(Long id) {
		return ldocTypeEAO.getReference(id);
	}

}
