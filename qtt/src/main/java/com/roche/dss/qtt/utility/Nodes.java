package com.roche.dss.qtt.utility;

/**
 * @author zerkowsm
 *
 */
public enum Nodes {
	
    START("start"),
    QUERY_ASSIGNMENT("Query Assignment"),
    MI_PS_MAUAL_ASSIGNMENT("MI-PS Manual Assignment"),
    MI_MEDICAL_INTERPRETATION_I("MI-Medical Interpretation I"),
    MI_CLARIFICATION("MI-Clarification"),
    MI_REVIEW("MI-Review"),
    FORK("fork1"),
    MI_MEDICAL_INTERPRETATION_II("MI-Medical Interpretation II"),
    DMG_MANUAL_ASSIGNMENT("DMG-Maunal Assignment"),
    DMG_DATA_SEARCH("DMG-Data Search"),
    DMG_CLARIFICATION("DMG-Clarification"),
    DMG_REVIEW("DMG-Review"),
    SEND_TO_REQUESTER("Send To Requester"),
    AWAITING_FOLLOW_UP("Awaiting Follow Up"),
    END("end");

    private String nodeName;

    Nodes(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getNodeName() {
        return nodeName;
    }

}