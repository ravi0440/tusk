package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.EuRegulatoryHistory;

public interface EuRegulatoryHistoryEAO extends BaseEAO<EuRegulatoryHistory>{
	
}