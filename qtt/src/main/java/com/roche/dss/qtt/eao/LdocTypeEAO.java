package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.LdocType;

/**
 * @author zerkowsm
 *
 */
public interface LdocTypeEAO extends QueryRelatedEAO<LdocType> {

}
