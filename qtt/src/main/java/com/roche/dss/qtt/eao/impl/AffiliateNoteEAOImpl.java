package com.roche.dss.qtt.eao.impl;

import org.springframework.stereotype.Repository;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.AffiliateNoteEAO;
import com.roche.dss.qtt.model.AffiliateNote;

@Repository("affiliateNoteEAO")
public class AffiliateNoteEAOImpl extends AbstractQueryRelatedEAO<AffiliateNote> implements AffiliateNoteEAO {
}
