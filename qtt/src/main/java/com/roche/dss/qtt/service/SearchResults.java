package com.roche.dss.qtt.service;

import java.util.ArrayList;
import java.util.List;

public class SearchResults {
	protected List<SearchResult> results;
	
	protected Query luceneQuery;
	
	protected String quickSearchKeywords;
	
	protected int resultSize;
	
	public static final int TYPE_QUICK = 0;
	
	public static final int TYPE_REPOSITORY = 1;
	
	protected int type;
	
	public SearchResults (List<Object> res, int resultSize, Query luceneQuery, String quickSearchKeywords) {
		if (res != null) {
			results = new ArrayList<SearchResult>(res.size());
			for (Object resItem: res) {
				results.add(new SearchResult(resItem));
			}
		}
		this.resultSize = resultSize;
		this.luceneQuery = luceneQuery;
		this.type = TYPE_QUICK;
		this.quickSearchKeywords = quickSearchKeywords;
	}
	
	public SearchResults(List<com.roche.dss.qtt.model.Query> queries, int resultSize) {
		if (queries != null) {
			results = new ArrayList<SearchResult>(queries.size());
			for (com.roche.dss.qtt.model.Query query: queries) {
				results.add(new SearchResult(query));
			}
		}
		this.resultSize = resultSize;
		this.type = TYPE_REPOSITORY;
	}
	
	public boolean getPartialResults() {
		if (resultSize > 0 && results != null && results.size() < resultSize) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public int getActualResultSize() {
		if (results != null) {
			return results.size();
		} else {
			return 0;
		}
	}
	
	public boolean getIsQuick() {
		if (type == TYPE_QUICK) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean getIsRepository() {
		if (type == TYPE_REPOSITORY) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean contains(com.roche.dss.qtt.model.Query query) {
		if (results != null) {
			for (SearchResult res: results) {
				if (query.equals(res.getQuery())) {
					return true;
				}
			}
		}
		return false;
	}
	
	public List<SearchResult> getResults() {
		return results;
	}

	public void setResults(List<SearchResult> results) {
		this.results = results;
	}

	public int getResultSize() {
		return resultSize;
	}

	public void setResultSize(int resultSize) {
		this.resultSize = resultSize;
	}

	public org.apache.lucene.search.Query getLuceneQuery() {
		return luceneQuery;
	}

	public void setLuceneQuery(org.apache.lucene.search.Query luceneQuery) {
		this.luceneQuery = luceneQuery;
	}

	public String getQuickSearchKeywords() {
		return quickSearchKeywords;
	}

	public void setQuickSearchKeywords(String quickSearchKeywords) {
		this.quickSearchKeywords = quickSearchKeywords;
	}
}
