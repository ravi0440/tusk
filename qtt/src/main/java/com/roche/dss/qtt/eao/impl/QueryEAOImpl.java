package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.QueryEAO;
import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.QttCountry;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryStatusCode;
import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;
import com.roche.dss.qtt.query.web.qrf.Constants;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.roche.dss.util.ApplicationUtils.WHITESPACE;

@Repository("queryEAO")
public class QueryEAOImpl extends AbstractBaseEAO<Query> implements QueryEAO {
    private static final Logger logger = LoggerFactory.getLogger(QueryEAOImpl.class);


    @Override
    @SuppressWarnings("unchecked")
    public List<Object[]> getQueryList(String email, String column, String order) {
        String orderBy = new StringBuilder("").append(column).append(WHITESPACE).append(order).append(WHITESPACE).toString();
        String sql = "select q.querySeq, q.queryNumber, q.queryType.queryType, q.datetimeSubmitted, q.currentDueDate, d.drugRetrievalName, q.urgencyFlag, q.queryStatusCode.statusCode, q.queryLabel, r.email from Query q join q.requester r join q.queryType qt left join q.drugs d where UPPER(r.email)=UPPER(:email) and (UPPER(q.allDrugsFlag)='Y' OR UPPER(d.firstFlag)='Y' OR UPPER(d.firstFlag)='T' OR d.firstFlag='1') ORDER BY " + orderBy;
        return getEntityManager().createQuery(sql).setParameter("email", email).getResultList();
    }
    
    @Override
    public  List<QuerySummaryDTO> findQueryAndTaskForVerifier(String verifier){
    	
    	List<Object[]> queryResult = getEntityManager().createNativeQuery("select q.QUERY_SEQ, q.QUERY_NUMBER, q.DATETIME_SUBMITTED, q.CURRENT_DUE_DATE, q.URGENCY_FLAG, q.STATUS_CODE, q.QUERY_LABEL, q.ALERT_FLAG, q.FOLLOWUP_NUMBER, types.QUERY_TYPE, q.SOURCE_COUNTRY_CODE, countries.DESCRIPTION, t.CREATE_ , t.NAME_ , t.ACTORID_, q.VERIFY_PERSON, q.WORKFLOW_ROUTE_TYPE, t.ID_ from queries q\r\n" + 
             		"inner join QUERY_TYPES types on types.QUERY_TYPE_CODE = q.QUERY_TYPE_CODE "+
    			    "inner join VW_ARISG_QTT_COUNTRIES countries on countries.country_code = q.SOURCE_COUNTRY_CODE "+
    				"inner join JBPM_TASKINSTANCE t on\r\n" + 
             		"t.procinst_=q.PROCESS_INSTANCE\r\n" + 
             		"and t.NAME_='DMG Data Search'\r\n" + 
             		"and t.ACTORID_<>q.verify_person\r\n" + 
             		"and t.ISOPEN_=1"+
             		"where \r\n" + 
             		"q.verify_person = :verify_person" ).setParameter("verify_person", verifier).getResultList();
    	
    	// transform to QuerySummaryDTO
    	List <QuerySummaryDTO> result = new ArrayList<QuerySummaryDTO>();
    	for(Object[] row : queryResult){
    		Query query = new Query();
    		query.setQuerySeq(((BigDecimal) row[0]).longValue());
    		query.setQueryNumber((String) row[1]);
    		query.setDatetimeSubmitted(new DateTime(row[2]));
    		query.setCurrentDueDate(new DateTime(row[3]));
    		query.setUrgencyFlag((Character) row[4]);
    		query.setQueryStatusCode(new QueryStatusCode((String)row[5]));
    		query.setQueryLabel((String) row[6]);
    		query.setAlertFlag((Character) row[7]);
    		query.setFollowupNumber((String) row[8]);
    		
    		QueryType type = new QueryType();
    		type.setQueryType((String) row[9]);
    		query.setQueryType(type);
    		
    		QttCountry country = new QttCountry((String)row[10],(String) row[11]);
    		query.setCountry(country);
    		
    		query.setInitiateDate(new DateTime(row[12]));
    		
    		TaskInstance task = new TaskInstance();
    		task.setName((String)row[13]);
    		task.setActorId((String)row[14]);
    		
    		query.setVerifyPerson((String) row[15]);
    		query.setWorkflowRouteType((String) row[16]);
    		task.setId(((BigDecimal) row[17]).longValue());
    		result.add(new QuerySummaryDTO(query, task));
    		
    	}
             return result;
    }
    
    @Override
    public DateTime getLatestQueryStartDate(long processInstanceId) {
        try {
            Object obj = getEntityManager().createNativeQuery("select min(t.create_) from jbpm_taskinstance t where t.procinst_=:processInstance" ).setParameter("processInstance", processInstanceId).getSingleResult();
            return new DateTime(obj);
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    @Override
    public DateTime getStartDateOfCurrentInteration(Long processInstanceId){
    	 try {
             Object obj = getEntityManager().createNativeQuery("select max(t.create_) from jbpm_taskinstance t where t.procinst_=:processInstance and t.name_ = 'Job Start'"  ).setParameter("processInstance", processInstanceId).getSingleResult();
             return new DateTime(obj);
         } catch (NoResultException nre) {
             return null;
         }
    }

    @Override
    public List<Query> findOpenQueryListByCountry(String countryCode) {
        return getEntityManager().createNamedQuery("query.findOpenQueryListByCountryCode").setParameter("countryCode", countryCode).getResultList();
    }

    @Override
    public List<Query> findOpenQueryList() {
        return getEntityManager().createNamedQuery("query.findOpenQueryList").getResultList();
    }


    /**
	 * Returns a list of queries whose criteria matches the input parameters
	 * among the already processed queries
	 */
    @Override
    public List<Query> getDuplicateQueries(String countryCode, int reporterTypeId, QueryType.Code queryTypeCode, boolean allDrugs, String retrDrug, String genericDrug) {
        List<Query> ret;
        String namedQueryName = "query.getDuplicateQueries";
        if (allDrugs) {
            namedQueryName += "Alldrugs";
        } else {
            if (retrDrug != null && "" != retrDrug) {
                namedQueryName += "ByRetrDrug";
            }
            if (genericDrug != null && "" != genericDrug) {
                namedQueryName += "ByGenDrug";
            }
        }
        javax.persistence.Query namedQuery = getEntityManager().createNamedQuery(namedQueryName)
                .setParameter("countryCode", countryCode)
                .setParameter("queryTypeCode", queryTypeCode)
                .setParameter("statusCode", Constants.QUERY_STATUS_PROCESSED)
                .setParameter("reporterTypeId", (short) reporterTypeId)
                .setParameter("allDrugsFlag", allDrugs ? Constants.YES : Constants.NO);
        if (!allDrugs) {
            namedQuery.setParameter("firstFlag", Constants.YES);
            namedQuery.setParameter("rocheDrugFlag", Constants.YES);
        }
        if (retrDrug != null && "" != retrDrug) {
            namedQuery.setParameter("drugRetrievalName", retrDrug);
        }
        if (genericDrug != null && "" != genericDrug) {
            namedQuery.setParameter("innGenericName", genericDrug);
        }
        return namedQuery.getResultList();
    }

    @Override
    public boolean checkHasStatus(long queryId, String status) {
        boolean retval = false;
        Query query = find(queryId);
        //aid for some lazy fetch problems with queryStatusCode
        getEntityManager().refresh(query);
        if (query != null && status.equals(query.getQueryStatusCode().getStatusCode())) {
            retval = true;
        }
        return retval;
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public List<Query> getInitiateList() {
        return getEntityManager().createNamedQuery("query.getInitiateList").getResultList();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Query> findClosedQueryList() {
        return getEntityManager().createNamedQuery("query.findClosedQueryList").getResultList();
    }

    @Override
    public boolean checkHaLegalNoDocs(long queryId) {
        boolean retval = false;
        Query query = find(queryId);
        getEntityManager().refresh(query);
        short rtId = query.getReporterType().getReporterTypeId();
        if (rtId == Constants.REPORTER_TYPE_REGULATORY_AUTHORITY || rtId == Constants.REPORTER_TYPE_LAWYER) {
            retval = true;
            Set<Attachment> attachList = query.getAttachments();
            if (attachList != null && !attachList.isEmpty()) {
                for (Attachment atmnt : attachList) {
                    if (atmnt.getAttachmentCategory().getAttachmentCategoryId() == 1) {
                        retval = false;
                    }
                }
            }
        }
        return retval;
    }

    @Override
    public Long getMaxFollowupSeq(Long queyrId) {
       return getEntityManager().createNamedQuery("query.getMaxFollowupSeq", Long.class).setParameter("querySeq", queyrId).getSingleResult();
    }

}
