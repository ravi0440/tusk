package com.roche.dss.qtt.model;
// Generated 2010-10-04 12:00:38 by Hibernate Tools 3.3.0.GA

/**
 * FuOrganisationId generated by hbm2java
 */
@Embeddable
public class FuOrganisationId implements java.io.Serializable {

	private static final long serialVersionUID = 1854895692228069677L;
	
	private long followupSeq;
	private long organisationId;

	public FuOrganisationId() {
	}

	public FuOrganisationId(long followupSeq, long organisationId) {
		this.followupSeq = followupSeq;
		this.organisationId = organisationId;
	}

	@Column(name = "FOLLOWUP_SEQ", nullable = false, precision = 10, scale = 0)
	public long getFollowupSeq() {
		return this.followupSeq;
	}

	public void setFollowupSeq(long followupSeq) {
		this.followupSeq = followupSeq;
	}

	@Column(name = "ORGANISATION_ID", nullable = false, precision = 10, scale = 0)
	public long getOrganisationId() {
		return this.organisationId;
	}

	public void setOrganisationId(long organisationId) {
		this.organisationId = organisationId;
	}

	@Override
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof FuOrganisationId))
			return false;
		FuOrganisationId castOther = (FuOrganisationId) other;

		return (this.getFollowupSeq() == castOther.getFollowupSeq())
				&& (this.getOrganisationId() == castOther.getOrganisationId());
	}

	@Override
	public int hashCode() {
		int result = 17;

		result = 37 * result + (int) this.getFollowupSeq();
		result = 37 * result + (int) this.getOrganisationId();
		return result;
	}

}
