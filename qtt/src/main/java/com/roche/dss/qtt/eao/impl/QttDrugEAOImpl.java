package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.QttDrugEAO;
import com.roche.dss.qtt.model.QttDrug;
import java.util.List;

@Repository("qttDrugEAO")
public class QttDrugEAOImpl extends AbstractQueryRelatedEAO<QttDrug> implements QttDrugEAO {

    @Override
    public List<String> getGenericNames() {
         return getEntityManager().createNamedQuery("list.qttGenericNames", String.class).getResultList();
    }

    @Override
    public List<String> getRetrievalNames() {
        return getEntityManager().createNamedQuery("list.qttDrugRetrNames", String.class).getResultList();
    }
}
