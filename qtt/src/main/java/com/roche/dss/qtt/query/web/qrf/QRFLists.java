package com.roche.dss.qtt.query.web.qrf;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class QRFLists {


    class ListItem {
        private String key, value;

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }

        public ListItem(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    public List<ListItem> getMonthNames() {
        List<ListItem> months = new ArrayList<ListItem>();
        int i = 0;
        for (String m : new DateFormatSymbols(Locale.ENGLISH).getShortMonths()) {
            if (!m.isEmpty()) {
                months.add(new ListItem(Integer.valueOf(++i).toString(), m));
            }
        }
        return months;
    }

    public List<String> getDays() {
        ArrayList<String> days = new ArrayList<String>();
        for (int i = 1; i < 32; i++) {
            days.add(Integer.valueOf(i).toString());
        }
        return days;
    }

    public List<ListItem> getYearList(int startYear, int endYear) {
        List<ListItem> years = new ArrayList<ListItem>();
        for (int i = startYear; i <= endYear; i++) {
            years.add(new ListItem(String.valueOf(i), String.valueOf(i)));
        }
        return years;
    }

    public List<ListItem> getYears() {
        int startYear = Calendar.getInstance().get(Calendar.YEAR);
        int endYear = startYear + 10;
        return getYearList(startYear, endYear);
    }

    public List<ListItem> getCumulativeYears() {
        int startYear = 1970;
        int endYear = Calendar.getInstance().get(Calendar.YEAR) + 10;
        return getYearList(startYear, endYear);
    }

}