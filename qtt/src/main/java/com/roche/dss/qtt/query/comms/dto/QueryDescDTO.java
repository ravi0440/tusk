/* 
====================================================================
  $Header$
  @author $Author: savovp $
  @version $Revision: 2654 $ $Date: 2013-01-31 17:15:50 +0100 (Cz, 31 sty 2013) $

====================================================================
*/
package com.roche.dss.qtt.query.comms.dto;


import com.roche.dss.qtt.model.QueryDescription;

public class QueryDescDTO {
	
	private String natureOfCase;
	private String indexCase;
	private String signsSymp;
	private String pmh;
	private String conMeds;
	private String investigations;
	private String diagnosis;
	private String otherInfo;

    /**
     * @return
     */
    public String getConMeds() {
        return conMeds;
    }

    /**
     * @return
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * @return
     */
    public String getIndexCase() {
        return indexCase;
    }

    /**
     * @return
     */
    public String getInvestigations() {
        return investigations;
    }

    /**
     * @return
     */
    public String getNatureOfCase() {
        return natureOfCase;
    }

    /**
     * @return
     */
    public String getOtherInfo() {
        return otherInfo;
    }

    /**
     * @return
     */
    public String getPmh() {
        return pmh;
    }

    /**
     * @return
     */
    public String getSignsSymp() {
        return signsSymp;
    }

    /**
     * @param string
     */
    public void setConMeds(String string) {
        conMeds = string;
    }

    /**
     * @param string
     */
    public void setDiagnosis(String string) {
        diagnosis = string;
    }

    /**
     * @param string
     */
    public void setIndexCase(String string) {
        indexCase = string;
    }

    /**
     * @param string
     */
    public void setInvestigations(String string) {
        investigations = string;
    }

    /**
     * @param string
     */
    public void setNatureOfCase(String string) {
        natureOfCase = string;
    }

    /**
     * @param string
     */
    public void setOtherInfo(String string) {
        otherInfo = string;
    }

    /**
     * @param string
     */
    public void setPmh(String string) {
        pmh = string;
    }

    /**
     * @param string
     */
    public void setSignsSymp(String string) {
        signsSymp = string;
    }

    public QueryDescription getQueryDescription() {
        QueryDescription qd = new QueryDescription();
			qd.setNatureOfCase(getNatureOfCase());
			qd.setIndexCase(getIndexCase());
			qd.setPrevMedicalHistory(getPmh());
			qd.setSignAndSymptom(getSignsSymp());
			qd.setConmed(getConMeds());
			qd.setInvestigation(getInvestigations());
			qd.setDiagnosis(getDiagnosis());
			qd.setOtherInfo(getOtherInfo());
        return qd;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((conMeds == null) ? 0 : conMeds.hashCode());
		result = prime * result
				+ ((diagnosis == null) ? 0 : diagnosis.hashCode());
		result = prime * result
				+ ((indexCase == null) ? 0 : indexCase.hashCode());
		result = prime * result
				+ ((investigations == null) ? 0 : investigations.hashCode());
		result = prime * result
				+ ((natureOfCase == null) ? 0 : natureOfCase.hashCode());
		result = prime * result
				+ ((otherInfo == null) ? 0 : otherInfo.hashCode());
		result = prime * result + ((pmh == null) ? 0 : pmh.hashCode());
		result = prime * result
				+ ((signsSymp == null) ? 0 : signsSymp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if(!(obj instanceof QueryDescDTO))
			return false;
		QueryDescDTO other = (QueryDescDTO) obj;
		if (conMeds == null) {
			if (other.conMeds != null)
				return false;
		} else if (!conMeds.equals(other.conMeds))
			return false;
		if (diagnosis == null) {
			if (other.diagnosis != null)
				return false;
		} else if (!diagnosis.equals(other.diagnosis))
			return false;
		if (indexCase == null) {
			if (other.indexCase != null)
				return false;
		} else if (!indexCase.equals(other.indexCase))
			return false;
		if (investigations == null) {
			if (other.investigations != null)
				return false;
		} else if (!investigations.equals(other.investigations))
			return false;
		if (natureOfCase == null) {
			if (other.natureOfCase != null)
				return false;
		} else if (!natureOfCase.equals(other.natureOfCase))
			return false;
		if (otherInfo == null) {
			if (other.otherInfo != null)
				return false;
		} else if (!otherInfo.equals(other.otherInfo))
			return false;
		if (pmh == null) {
			if (other.pmh != null)
				return false;
		} else if (!pmh.equals(other.pmh))
			return false;
		if (signsSymp == null) {
			if (other.signsSymp != null)
				return false;
		} else if (!signsSymp.equals(other.signsSymp))
			return false;
		return true;
	}
    
    
}
