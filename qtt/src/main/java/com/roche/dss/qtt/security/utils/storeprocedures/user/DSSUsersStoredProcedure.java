package com.roche.dss.qtt.security.utils.storeprocedures.user;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import com.roche.dss.qtt.security.utils.storeprocedures.DSSStoreProceduresConstants;

/**
 * @author zerkowsm
 *
 */
public class DSSUsersStoredProcedure extends StoredProcedure implements DSSStoreProceduresConstants {
    
    private static final String SQL = "user_service.get_users";
    private static final String typeName = "VARCHAR10_ARRAY";

    public DSSUsersStoredProcedure(DataSource ds, boolean jbossServerSide) {
        setDataSource(ds);
        if(jbossServerSide) {
        	getJdbcTemplate().setNativeJdbcExtractor(new JBossNativeJdbcExtractor());
    	} else {
    		getJdbcTemplate().setNativeJdbcExtractor(new C3P0NativeJdbcExtractor());
    	}
        setFunction(true);
        setSql(SQL);
        declareParameter(new SqlOutParameter(USER_SERVICE_GET_USERS_OUT_PARAM, OracleTypes.CURSOR, new DSSUserMapper()));
        declareParameter(new SqlParameter("p_systemCode", Types.VARCHAR));
        declareParameter(new SqlParameter("p_permissionCodeArray", Types.ARRAY, typeName));
        
        compile();
    }

    public Map<String, Object> execute(String applicationCode, final String[] permissionCodes) {
    	Map<String, Object> inputs = new HashMap<String, Object>();
    	inputs.put("p_systemCode", applicationCode);
    	inputs.put("p_permissionCodeArray", new AbstractSqlTypeValue() {
			
			@Override
			protected Object createTypeValue(Connection con, int sqlType, String typeName) throws SQLException {
				ArrayDescriptor desc = new ArrayDescriptor(typeName, con);
				return new ARRAY(desc, con, permissionCodes); 
			}
		});     	
        return super.execute(inputs); 
    }
    
}
