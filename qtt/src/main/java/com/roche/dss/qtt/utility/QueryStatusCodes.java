package com.roche.dss.qtt.utility;

/**
 * This contains all possbile value of query status code
 * <p/>
 * User: pruchnil
 */
public enum QueryStatusCodes {
    UNKNOWN("", "Unknown"),
    AMENDED("AMND", "Amended"),
    DRAFT("NSUB", "Draft"),
    PROCESSING("PROC", "Being Processed"),
    SUBMITTED("SUBM", "Submitted to DSCL"),
    REJECTED("RJCT", "Rejected by DSCL"),
    FOLLOWUP_ATT("FUAT", "Follow-up Attachment"),
    REOPENED("REOP", "Reopened"),
    CLOSED("CLOS", "Closed");


    private String code;
    private String value;

    private QueryStatusCodes(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
