package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.security.service.SecurityService;
import com.roche.dss.qtt.service.FileOperationsService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.workflow.WorkflowService;
import com.roche.dss.qtt.utility.QueryStatusCodes;


@ParentPackage("default")
@Action(value = "RejectQuery", results = {
        @Result(name = "success", type="tiles", location = "/query_reject_success"),
        @Result(name = "alreadyProcessed",type="tiles",  location = "/already_processed"),
        @Result(name = "error", location = "/qtt/global/error.jsp")
})
public class RejectQueryAction  extends CommonActionSupport {

    private static final Logger logger = LoggerFactory.getLogger(RejectQueryAction.class);

    @Autowired
	private FileOperationsService fileOps;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private QueryService queryService;


    private Long id;

    @Autowired
    private WorkflowService workflowService;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String execute() {

        // security check
        boolean allowed =  isDscl();

        String userEmail = securityService.getLoggedUser().getMail();

        if (!allowed) {
            logger.error("RejectQueryAction:Access denied.");
            return ERROR;
        }

        // retrieve query type id from the session
        Query query = queryService.find(id);
        if (query == null) {
            logger.error("Query id not passed to the action!");
            return ERROR;
        }

        //InitiateFacadeBD bd = (InitiateFacadeBD) BusinessDelegateFactory.getFactory().getServiceBD(mapping);

        // change due to bug fix #197 - perform safety-check to see if query is still in state 'SUBM'
        if (queryService.checkHasStatus(id, QueryStatusCodes.SUBMITTED.getCode())) {
            // first delete the attachment
            String dir = fileOps.findDestinationDirectory(String.valueOf(query.getQueryNumber()));

            //TODO fix delete problem for FU queries
            //boolean successful = FileOperationsHelper.getInstance().deleteDir(new File(dir));
            //TODO until the FU problem is fixed, documents should not be deleted
            boolean successful = true;//FileOperationsHelper.getInstance().deleteDir(new File(dir));
            // sendEmail(dto, userEmail);
            // note the return of deleteDir should be true as we are deleting the whole dir and doing show means the
            // dir no longer exists so this method should return true - meaning successful deletion
            if (successful) {
                workflowService.deleteQueryAndProcess(query.getQuerySeq());
            }

        } else {
            return "alreadyProcessed";
        }

        return SUCCESS;
    }
}
