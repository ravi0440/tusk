package com.roche.dss.qtt.query.web.action.oldqrf;

import java.util.List;

import com.roche.dss.qtt.model.CtrlOformat;
import com.roche.dss.qtt.model.PerformanceMetric;
import com.roche.dss.qtt.query.comms.dto.QueryDTO;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.service.QrfService;

import javax.annotation.Resource;

public class PopulatePerformanceAction extends QRFAbstractAction {

    private QrfService qrfService;


    @Resource
    public void setQrfService(QrfService qrfService) {
        this.qrfService = qrfService;
    }

    PerformanceMetric queryform;

    public PerformanceMetric getQueryform() {
		return queryform;
	}

	public void setQueryform(PerformanceMetric queryform) {
		this.queryform = queryform;
	}

	@Override
    public Object getModel() {
        // if the query is update
        if (getSession().get(ActionConstants.QRF_EDIT_QUERY_TYPE) != null) {
            int queryId = ((Integer) getSession().get(ActionConstants.QRF_QUERY_ID)).intValue();

            QueryDTO queryDto = qrfService.openQuery(queryId, null, true);
            queryform = queryDto.getPerformanceMetric();

            getSession().put(ActionConstants.FRM_QUERY_DESC, queryform);
        } else {
        	queryform = new PerformanceMetric();
        }
        return queryform;
    }

    public List<CtrlOformat> getClotypes() {
        return qrfService.getCtrlOformats();
    }
    
    public String execute() {
        return SUCCESS;
    }

}
