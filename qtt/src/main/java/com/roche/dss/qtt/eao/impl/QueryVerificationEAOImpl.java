package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.QueryAssessmentEAO;
import com.roche.dss.qtt.eao.QueryEAO;
import com.roche.dss.qtt.eao.QueryVerificationEAO;
import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.QttCountry;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryAssessment;
import com.roche.dss.qtt.model.QueryStatusCode;
import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.model.QueryVerification;
import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;
import com.roche.dss.qtt.query.web.qrf.Constants;

import javax.management.remote.NotificationResult;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.roche.dss.util.ApplicationUtils.WHITESPACE;

@Repository("queryVerificationEAO")
public class QueryVerificationEAOImpl extends AbstractBaseEAO<QueryVerification> implements QueryVerificationEAO {
   
	@Override
	public QueryVerification findByRecordTypeAndQueryId(QueryVerification.RECORD_TYPE type, long queryId,String followupNumber){
		try{
		 return (QueryVerification)getEntityManager().createNamedQuery("QueryVerification.findbyTypeAndIDAndFollowupNumber").setParameter("querySeq", queryId).setParameter("followupNumber", followupNumber!=null?followupNumber:"0").setParameter("recordType", type.name()).getSingleResult();
		}catch(NoResultException e){
			 return null;
		 }
		 
	}																	
}
