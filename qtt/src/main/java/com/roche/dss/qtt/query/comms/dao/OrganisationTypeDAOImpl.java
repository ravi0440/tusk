package com.roche.dss.qtt.query.comms.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.roche.dss.qtt.query.comms.dao.impl.JDBCTemplateDAO;
import com.roche.dss.qtt.query.comms.dto.OrganisationTypeDTO;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
@Repository
public class OrganisationTypeDAOImpl extends JDBCTemplateDAO implements OrganisationTypeDAO {

    @Override
    public List<OrganisationTypeDTO> getOrganisationTypes() {
        String queryStr = "select type_id, type from organisation_types";
        return jdbcTemplate.query(queryStr, new OrgTypeDTORowMapper());
    }

    class OrgTypeDTORowMapper implements RowMapper<OrganisationTypeDTO> {

        @Override
        public OrganisationTypeDTO mapRow(ResultSet resultSet, int i) throws SQLException {
            OrganisationTypeDTO organisationTypeDTO = new OrganisationTypeDTO();
            organisationTypeDTO.setTypeId(resultSet.getString("type_id"));
            organisationTypeDTO.setType(resultSet.getString("type"));
            return organisationTypeDTO;
        }
    }
}
