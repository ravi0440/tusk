package com.roche.dss.qtt.utility.pdf;

import com.roche.dss.qtt.query.web.qrf.model.QueryReviewDisplayModel;

public interface QueryDetailsInterface {

	public abstract QueryReviewDisplayModel buildQueryDetails(int queryId)
			throws NumberFormatException;

}