package com.roche.dss.qtt.security_old;

import java.io.IOException;

import com.roche.dss.qtt.security.web.authentication.QTTUsernamePasswordAuthenticationToken;


public class DssSecurityProcessingFilter implements Filter, DssSecurityConstants {

	private Log log = LogFactory.getLog(DssSecurityProcessingFilter.class);

	private RememberMeServices rememberMeServices;

	private AuthenticationManager authenticationManager;

	private AuthenticationEntryPoint authenticationEntryPoint;

	private boolean ignoreFailure = false;


	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		Authentication existingAuth = SecurityContextHolder.getContext()
				.getAuthentication();
		try {
			if (existingAuth != null) {				
				if (existingAuth.getAuthorities() == null) {
					 Authentication auth = authenticationManager.authenticate(existingAuth);
					 SecurityContextHolder.getContext().setAuthentication(auth);

					if (rememberMeServices != null) {
						rememberMeServices.loginSuccess(httpRequest,
								httpResponse, auth);
					}
				 }
			} else {
				Cookie[] cookies = httpRequest.getCookies();
				boolean found = false;
				if (cookies != null) {
					for (int i = 0; i < cookies.length; i++) {
						if (cookies[i].getName().equals(USER_COOKIE)) {
							found = true;
							Authentication auth = authenticationManager
									.authenticate(new QTTUsernamePasswordAuthenticationToken(
											cookies[i].getValue(), ""));
							SecurityContextHolder.getContext()
									.setAuthentication(auth);
						}
					}
				}

				if (!found) {
					authenticationEntryPoint.commence(httpRequest,
							httpResponse, null);
					return;
				}
			}
		} catch (AuthenticationException ex) {
			if (log.isDebugEnabled()) {
				log.debug("DSS Security authentication failed. " + ex.getMessage());
			}

			if (!ignoreFailure) {
				authenticationEntryPoint
						.commence(httpRequest, httpResponse, ex);
				return;
			}
		}
		chain.doFilter(httpRequest, httpResponse);
	}

	public void destroy() {
	}

	public void init(FilterConfig arg0) throws ServletException {
	}

	public void setRememberMeServices(RememberMeServices rememberMeServices) {
		this.rememberMeServices = rememberMeServices;
	}

	public void setAuthenticationEntryPoint(
			AuthenticationEntryPoint authenticationEntryPoint) {
		this.authenticationEntryPoint = authenticationEntryPoint;
	}

	public void setAuthenticationManager(
			AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	public void setIgnoreFailure(boolean ignoreFailure) {
		this.ignoreFailure = ignoreFailure;
	}
}
