package com.roche.dss.qtt.model;

@Entity
@Table(name = "DATA_SEARCH")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class DataSearch implements java.io.Serializable {

	private static final long serialVersionUID = -120492411797884135L;
	
	public static enum RequestType {
		DATA_SEARCH("Data Search"),
		LITERATURE_SEARCH("Literature Search"),
		DATA_AND_LITERATURE_SEARCH("Data and Literature Search");
		
		private String label;
		
		private RequestType(String label) {
			this.label = label;
		}
		
		public String getLabel() {
			return label;
		}
	}
	
	public static enum SearchType {
		AUTOMATED_QUERY("Automated Query"),
		SIGNAL_DETECTION("Data Search - Signal Detection / Drug Safety Report"),
		ASIME("Data Search - ASIMEs"),
		PERIODIC_REPORTS("Data Search - Periodic Reports"),
		SAE_RECONCILIATIONS("Data Search - SAE Reconciliations or Internal Audit"),
		OTHER("Data Search - Other"),
		LITERATURE_SEARCH("Literature Search");
		
		private String label;
		
		private SearchType(String label) {
			this.label = label;
		}
		
		public String getLabel() {
			return label;
		}
	}
	
	public static enum Frequency {
		DAILY("Daily"),
		WEEKLY("Weekly"),
		BI_WEEKLY("Bi-Weekly"),
		MONTHLY("Monthly"),
		QUARTERLY("Quarterly"),
		OTHER("Other");

		private String label;
		
		private Frequency(String label) {
			this.label = label;
		}
		
		public String getLabel() {
			return label;
		}
	}
	private long querySeq;
	private Query query;
	private RequestType requestType;
	private SearchType searchType;
	private String specification;
	private String reportParameters;
	private Frequency frequency;
	private String frequencyOther;
	private String meddraPreferredTerm;
	private String boReport;

	public DataSearch() {
	}

	@GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "query"))
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "QUERY_SEQ", nullable = false, precision = 10, scale = 0)
	public long getQuerySeq() {
		return this.querySeq;
	}

	public void setQuerySeq(long querySeq) {
		this.querySeq = querySeq;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	public Query getQuery() {
		return this.query;
	}

	public void setQuery(Query query) {
		this.query = query;
	}

	@Enumerated(EnumType.STRING)
	public RequestType getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}

	@Enumerated(EnumType.STRING)
	public SearchType getSearchType() {
		return searchType;
	}

	public void setSearchType(SearchType searchType) {
		this.searchType = searchType;
	}

	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	public String getReportParameters() {
		return reportParameters;
	}

	public void setReportParameters(String reportParameters) {
		this.reportParameters = reportParameters;
	}

	@Enumerated(EnumType.STRING)
	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	public String getFrequencyOther() {
		return frequencyOther;
	}

	public void setFrequencyOther(String frequencyOther) {
		this.frequencyOther = frequencyOther;
	}

	public String getMeddraPreferredTerm() {
		return meddraPreferredTerm;
	}

	public void setMeddraPreferredTerm(String meddraPreferredTerm) {
		this.meddraPreferredTerm = meddraPreferredTerm;
	}

	public String getBoReport() {
		return boReport;
	}

	public void setBoReport(String boReport) {
		this.boReport = boReport;
	}
}
