package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.eao.QueryEAO;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryVerification;
import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;
import com.roche.dss.qtt.query.web.model.TaskListParams;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.workflow.WorkflowService;

import com.roche.dss.qtt.utility.StyleManager;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author zerkowsm
 */
public class RetrieveQueryTaskListAction extends TaskBaseAction {

	private static final Logger logger = LoggerFactory.getLogger(RetrieveSummaryListAction.class);
    
	private static final long serialVersionUID = 1190964633161500356L;
	
	private static final String SESSION_KEY_PARAMS = "TASK_LIST_PARAMS";
    
	private TaskListParams params = new TaskListParams();
	
	private String user;
    
    private WorkflowService workflowService;
    private QueryService queryService;
    
    @Autowired
    private QueryEAO queryEAO;

    @Resource
    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    @Resource
	public void setQueryService(QueryService queryService) {
		this.queryService = queryService;
	}
    
    
    private List<QuerySummaryDTO> tasks = new ArrayList<QuerySummaryDTO>();

    public String setFilter() {
    	getSession().put(SESSION_KEY_PARAMS, params);
    	return SUCCESS;
    }
    
    public String execute() {
    	if (getSession().containsKey(SESSION_KEY_PARAMS)) {
    		params = (TaskListParams) getSession().get(SESSION_KEY_PARAMS);
    	}
        handleLabelLink();
        List<QuerySummaryDTO> taskList = workflowService.retrieveUserTaskList(getLoggedUser(), isDMGCoord() || isDMGScientist());
        taskList.addAll(queryEAO.findQueryAndTaskForVerifier(getLoggedUser().getUsername()));
		StyleManager.assignStyle(taskList, true);
		filterTasks(taskList);
        Comparator<QuerySummaryDTO> cmp = (SummarySort.values()[sort]).getColumn();
        Collections.sort(tasks, asc ? cmp : Collections.reverseOrder(cmp));
        return SUCCESS;
    }

    private void filterTasks(List<QuerySummaryDTO> taskList) {
		for (QuerySummaryDTO t : taskList) {
			if ((params.getFrom() == null || !t.getQuery().getCurrentDueDate().isBefore(params.getFrom().getTime()))
					&& (params.getTo() == null || !t.getQuery().getCurrentDueDate().isAfter(params.getTo().getTime()))
					&& (StringUtils.isBlank(params.getOwner()) || StringUtils.equalsIgnoreCase(params.getOwner().trim(), t.getTask().getActorId()))) {
				tasks.add(t);
			}
		}
	}

    public boolean readyForVerification(Query query, TaskInstance task){
    	if(query.getVerifyPerson()!=null && task.getName().equals("DMG Data Search")){
    		if(queryService.getQueryVerification(QueryVerification.RECORD_TYPE.FILLED_BY_OWNER, query.getQuerySeq(),query.getFollowupNumber())!=null){
    			return true;
    		}else{
    			return false;
    		}
    	}else{
    		return false;
    	}
    }
    
    public boolean verified(Query query, TaskInstance task){
    	if(readyForVerification(query,task)&&queryService.getQueryVerification(QueryVerification.RECORD_TYPE.FILLED_BY_VERIFIER, query.getQuerySeq(),query.getFollowupNumber())!=null){
    		return true;
    	}else {
    		return false;
    	}
    }
    
	public String retrieveUserTaskList() {
        tasks = workflowService.retrieveUserTaskList(user);
        Comparator<QuerySummaryDTO> cmp = (SummarySort.values()[sort]).getColumn();
        Collections.sort(tasks, asc ? cmp : Collections.reverseOrder(cmp));
        return SUCCESS;
    }

    public List<QuerySummaryDTO> getTasks() {
        return tasks;
    }

	public TaskListParams getParams() {
		return params;
	}

	public void setParams(TaskListParams params) {
		this.params = params;
	}

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
