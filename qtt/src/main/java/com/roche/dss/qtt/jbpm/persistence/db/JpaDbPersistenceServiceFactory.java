package com.roche.dss.qtt.jbpm.persistence.db;

/**
 * @author zerkowsm
 *
 */
public class JpaDbPersistenceServiceFactory extends DbPersistenceServiceFactory {

	private static final long serialVersionUID = -7082517877938203652L;

	private static Log log = LogFactory.getLog(JpaDbPersistenceServiceFactory.class);

	public Service openService() {
		log.debug("creating persistence service");
		return new JpaDbPersistenceService(this);
	}

	public void close() {
		if (getSessionFactory() != null && getSessionFactoryJndiName() == null) {
			log.debug("closing hibernate session factory");
			getSessionFactory().close();
		}
	}

}
