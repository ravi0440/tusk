package com.roche.dss.qtt.query.web.action.oldqrf;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.roche.dss.qtt.model.CaseOformat;
import com.roche.dss.qtt.model.ClinicalTrial;
import com.roche.dss.qtt.model.DataSearch;
import com.roche.dss.qtt.model.DataSearch.Frequency;
import com.roche.dss.qtt.model.DataSearch.RequestType;
import com.roche.dss.qtt.model.DataSearch.SearchType;
import com.roche.dss.qtt.query.comms.dto.QueryDTO;
import com.roche.dss.qtt.query.comms.dto.QueryDescDTO;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.model.QueryDescFormModel;
import com.roche.dss.qtt.service.QrfService;

import javax.annotation.Resource;

public class DataSearchAction extends QRFAbstractAction {

    private QrfService qrfService;

    QueryDescFormModel queryForm = new QueryDescFormModel();

    DataSearch dataSearch = null;
    
	ClinicalTrial clinicalTrial = new ClinicalTrial();
    
    Map<String, CaseOformat> oformat = new HashMap<String, CaseOformat>();
    
    Map<String, String> otherOformat = new HashMap<String, String>();

	Map<String, String> meddraPreferredTerm = new HashMap<String, String>();

    Map<String, String> natureOfCase = new HashMap<String, String>();
    
    Map<String, String> comments = new HashMap<String, String>();

	CaseOformat caseOformat;

	String other;

	String meddraTerm;

	String additionalComments;

    @Resource
    public void setQrfService(QrfService qrfService) {
        this.qrfService = qrfService;
    }
    
	public void prepare() {
    	if (dataSearch.getSearchType() != null) {
			caseOformat = oformat.get(dataSearch.getSearchType().name());
	    	other = caseOformat != null && caseOformat.getCaseOformatId() == CaseOformat.OTHER 
	    			? otherOformat.get(dataSearch.getSearchType().name()) 
	    			: null;
	    	meddraTerm = meddraPreferredTerm.get(dataSearch.getSearchType().name());
	    	additionalComments = comments.get(dataSearch.getSearchType().name());
	    	queryForm.setNatureofcase(natureOfCase.get(dataSearch.getSearchType().name()));
    	}
	}

	@Override
    public Object getModel() {
    	
    	if (getSession().get(ActionConstants.QRF_EDIT_QUERY_TYPE) != null) {
            int queryId = ((Integer) getSession().get(ActionConstants.QRF_QUERY_ID)).intValue();
            QueryDTO query = qrfService.openQuery(queryId, null, true);
            String yourQuestion = null;
            if (query.getQueryDesc() != null) {
            	yourQuestion = query.getQueryDesc().getNatureOfCase();
            	queryForm.setNatureofcase(yourQuestion);
                queryForm.setIndexcase(query.getQueryDesc().getIndexCase());
            }
            if (query.getClinical() != null) {
            	clinicalTrial.setCaseSelection(query.getClinical().getCaseSelection());
            	clinicalTrial.setCrtnNumber(query.getClinical().getCrtnNumber());
            	clinicalTrial.setPatientNumber(query.getClinical().getPatientNumber());
            	clinicalTrial.setProtocolNumber(query.getClinical().getProtocolNumber());
            }
            SearchType searchType = null;
            if (query.getDataSearch() != null) {
            	if(dataSearch==null)      {	dataSearch = query.getDataSearch();
             searchType = dataSearch.getSearchType();
             if (searchType != null) {
            		natureOfCase.put(searchType.name(), yourQuestion);
            		meddraPreferredTerm.put(searchType.name(), query.getDataSearch().getMeddraPreferredTerm());
            		comments.put(searchType.name(), query.getCaseComment());
            		CaseOformat o = new CaseOformat();
            		o.setCaseOformatId(query.getOutputFormatId());
            		oformat.put(searchType.name(), o);
                    if (query.getOutputFormatId() == CaseOformat.OTHER) {
                    	otherOformat.put(searchType.name(), query.getSpecialRequest());
                    }
             		}
            	}
            }
            getSession().put(ActionConstants.FRM_QUERY_DESC, queryForm);
        }
        return queryForm;
    }

	@SkipValidation
    public String populateDataSearch() {
    	return SUCCESS;
    }

	@Override
	public void validate() {
		prepare();
		

		if (dataSearch.getRequestType() == null) {
			addActionError(getText("datasearch.request.displayname"));
		}

		if (dataSearch.getSearchType() == null) {
			addActionError(getText("datasearch.searchType.displayname"));
		} else {

			switch (dataSearch.getSearchType()) {
				case AUTOMATED_QUERY:
					if (StringUtils.isBlank(dataSearch.getSpecification())) {
						addActionError(getText("datasearch.specification.displayname"));
					}
					validateOutputFormat();
					if (dataSearch.getFrequency() == Frequency.OTHER
							&& StringUtils.isBlank(dataSearch.getFrequencyOther())) {
						addActionError(getText("datasearch.otherFrequency.displayname"));
					}
					break;
				case SIGNAL_DETECTION:
					if (StringUtils.isBlank(meddraTerm)) {
						addActionError(getText("datasearch.meddraTerm.displayname"));
					}
					validateOutputFormat();
					break;
				case ASIME:
					if (StringUtils.isBlank(meddraTerm)) {
						addActionError(getText("datasearch.meddraTerm.displayname"));
					}
					break;
				case PERIODIC_REPORTS:
					break;
				case SAE_RECONCILIATIONS:
					if (StringUtils.isBlank(clinicalTrial.getProtocolNumber())) {
						addActionError(getText("protocolno.displayname"));
					}
					validateOutputFormat();
					break;
				case OTHER:
					if (StringUtils.isBlank(queryForm.getNatureofcase())) {
						addActionError(getText("queryDescForm.natureofcase.displayname"));
					}
					validateOutputFormat();
					break;
				case LITERATURE_SEARCH:
					if (StringUtils.isBlank(queryForm.getNatureofcase())) {
						addActionError(getText("queryDescForm.natureofcase.displayname"));
					}
					break;
				default:
					addActionError(getText("datasearch.searchType.displayname"));
			}
		}
		super.validate();
	}

	private void validateOutputFormat() {
		if (caseOformat != null && caseOformat.getCaseOformatId() == CaseOformat.OTHER
				&& StringUtils.isBlank(other)) {
			addActionError(getText("outputformat.displayname"));
		}
	}
	
    public String execute() {
    	prepare();
        if (getSession().get(ActionConstants.QRF_EDIT_QUERY_TYPE) != null) {
            qrfService.updateDataSearch(createQueryDescDTO(queryForm), dataSearch, clinicalTrial, caseOformat, other, meddraTerm, additionalComments, (Integer) getSession().get(ActionConstants.QRF_QUERY_ID));
        } else {
            qrfService.addDataSearch(createQueryDescDTO(queryForm), dataSearch, clinicalTrial, caseOformat, other, meddraTerm, additionalComments, (Integer) getSession().get(ActionConstants.QRF_QUERY_ID));
        }
        return SUCCESS;
    }
    
    private QueryDescDTO createQueryDescDTO(QueryDescFormModel queryDescForm) {
    	QueryDescDTO dto = new QueryDescDTO();
    	if (StringUtils.isBlank(queryDescForm.getNatureofcase()) && StringUtils.isNotBlank(queryDescForm.getIndexcase())) {
    		dto.setNatureOfCase(queryDescForm.getIndexcase());
    	} else {
    		dto.setNatureOfCase(queryDescForm.getNatureofcase());
    	}
    	dto.setOtherInfo(queryDescForm.getOtherinfo());
    	dto.setConMeds(queryDescForm.getConmeds());
    	dto.setDiagnosis(queryDescForm.getDiagnosis());
    	dto.setIndexCase(queryDescForm.getIndexcase());
    	dto.setInvestigations(queryDescForm.getInvestigation());
    	dto.setPmh(queryDescForm.getPmh());
    	dto.setSignsSymp(queryDescForm.getSignsymp());
		return dto;
	}
    public Map<String, String> getRequestTypes() {
    	Map<String, String> types = new HashMap<String, String>();
    	for (RequestType t : RequestType.values()) {
    		types.put(t.name(), t.getLabel());
    	}
    	// this will only remove type from list, due to consistency reasons
    	// in te future it should be removed from enum
    	types.remove("DATA_AND_LITERATURE_SEARCH");
    	
    	return types;
    }

    public Map<String, String> getSearchTypes() {
    	Map<String, String> types = new LinkedHashMap<String, String>();
    	for (SearchType t : SearchType.values()) {
    		types.put(t.name(), t.getLabel());
    	}
    	return types;
    }
    
    public Map<String, String> getFrequencies() {
    	Map<String, String> freq = new LinkedHashMap<String, String>();
    	for (Frequency f : Frequency.values()) {
    		freq.put(f.name(), f.getLabel());
    	}
    	return freq;
    }

    public List<CaseOformat> getOutputFormats() {
    	return qrfService.getActiveCaoTypes();
    }

	public Map<String, CaseOformat> getOformat() {
		return oformat;
	}

	public void setOformat(Map<String, CaseOformat> oformat) {
		this.oformat = oformat;
	}

	public Map<String, String> getOtherOformat() {
		return otherOformat;
	}

	public void setOtherOformat(Map<String, String> otherOformat) {
		this.otherOformat = otherOformat;
	}

    public Map<String, String> getMeddraPreferredTerm() {
		return meddraPreferredTerm;
	}

	public void setMeddraPreferredTerm(Map<String, String> meddraPreferredTerm) {
		this.meddraPreferredTerm = meddraPreferredTerm;
	}

	public Map<String, String> getNatureOfCase() {
		return natureOfCase;
	}

	public void setNatureOfCase(Map<String, String> natureOfCase) {
		this.natureOfCase = natureOfCase;
	}

	public Map<String, String> getComments() {
		return comments;
	}

	public void setComments(Map<String, String> comments) {
		this.comments = comments;
	}

	public DataSearch getDataSearch() {
		return dataSearch;
	}

	public void setDataSearch(DataSearch dataSearch) {
		this.dataSearch = dataSearch;
	}

    public ClinicalTrial getClinicalTrial() {
		return clinicalTrial;
	}

	public void setClinicalTrial(ClinicalTrial clinicalTrial) {
		this.clinicalTrial = clinicalTrial;
	}

}
