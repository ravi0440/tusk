package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.query.web.model.RepositorySearchParams;
import com.roche.dss.qtt.service.DictionaryService;

@ParentPackage("default") 
public class RepositorySearchAction extends CommonActionSupport {	
	@Autowired
	protected DictionaryService dictionaryService;
	
	protected RepositorySearchParams searchParams;
	
	public DictionaryService getDictionary () {
		return dictionaryService;
	}

	public RepositorySearchParams getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(RepositorySearchParams searchParams) {
		this.searchParams = searchParams;
	}
	
	@Action(value="RepositorySearch", results={@Result(name="success", type="tiles", location="/repository_search.search")})
	public String showInput() {	
		return SUCCESS;
	}
}
