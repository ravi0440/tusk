package com.roche.dss.qtt.eao.impl;

import org.springframework.stereotype.Repository;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.AeTermEAO;
import com.roche.dss.qtt.model.AeTerm;

/**
 * @author zerkowsm
 *
 */
@Repository("aeTermEAO")
public class AeTermEAOImpl extends AbstractQueryRelatedEAO<AeTerm> implements AeTermEAO{
	
}