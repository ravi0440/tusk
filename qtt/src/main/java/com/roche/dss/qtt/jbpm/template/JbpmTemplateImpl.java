package com.roche.dss.qtt.jbpm.template;

/**
 * @author zerkowsm
 *
 */
public class JbpmTemplateImpl implements JbpmTemplate {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Object doInJbpm(JbpmCallback callback) {

		JbpmContext jbpmContext = JbpmConfiguration.getInstance()
				.createJbpmContext();
		try {
			jbpmContext.setSession(getJpaSession());

			return callback.execute(jbpmContext);

		} finally {
			jbpmContext.close();
		}
	}

	public Session getJpaSession() {
		return (Session) em.getDelegate();
	}

}
