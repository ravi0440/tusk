package com.roche.dss.qtt.query.web.action;

public class ActionConstants {
	
	/**
	 * look up constants
	 */
	public static final String LOOKUP_COUNTRIES = "countries";
	public static final String LOOKUP_ORG_TYPES = "orgtypes";
	public static final String LOOKUP_EXT_TYPES = "exttypes";
	public static final String LOOKUP_GENERIC_DRUGS = "gnames";
	public static final String LOOKUP_PREF_DRUGS = "pnames";
	public static final String LOOKUP_PRE_PREP = "prep";
	public static final String LOOKUP_QUERY_TYPES = "qtypes";
	public static final String LOOKUP_REPORTER_TYPES = "rtypes";
	public static final String LOOKUP_CASE_TYPES = "caotypes";
	public static final String LOOKUP_CLINICAL_TYPES = "clotypes";
	public static final String LOOKUP_ORG_TYPES_FIRST_BLANK = "orgtypesBlank";
	public static final String LOOKUP_COUNTRIES_FIRST_BLANK = "countriesBlank";
	public static final String LOOKUP_GENERIC_DRUGS_FIRST_BLANK = "gnamesBlank";
	public static final String LOOKUP_PREF_DRUGS_FIRST_BLANK = "pnamesBlank";
	public static final String LOOKUP_QUERY_TYPES_FIRST_BLANK = "qtypesBlank";
	public static final String LOOKUP_REPORTER_TYPES_FIRST_BLANK = "rtypesBlank";
	public static final String LOOKUP_CASE_TYPES_FIRST_BLANK = "caotypesBlank";
	public static final String LOOKUP_CLINICAL_TYPES_FIRST_BLANK = "clotypesBlank";
	public static final String LOOKUP_QUERY_TYPE_URLS = "queryTypeUrls";
	
	/**
	 * user session obejct 
	 */
	public static final String DSS_USER = "DSSUser";
	/**
	 * organisation types
	 */
	public static final int ROCHE_AFFILIATE = 2;
	public static final int LICENSING_PARTNER = 3;
	public static final int OTHER = 4;
				
	//general qrf constants
	public static final String QRF_QUERY_ID = "qrfQueryId";
    public static final String QUERY_ID = "queryId";
	public static final String DRUG_EVT_QUERY_DESC = "queryDescDTO";	
	public static final String DRUG_INT_DRUG = "drugInteract"; 
	public static final String QRF_AE_TERM = "aeTerm";
	public static final String QRF_STATUS_NOT_SUBMITTED = "NSUB";
	public static final String QRF_STATUS_SUBMITTED = "SUBM";
	public static final String QRF_STATUS_AMENDED = "AMND";	
    public static final String QRF_STATUS_REOPENED = "REOP";
	public static final String QRF_ORG_TYPE_AFFILIATE = "Roche Affiliate";
	
	// HR 2008.10.20 removing psst
	// public static final String PSST_QUERY_NUMBER = "personalQueryId";
    
	public static final String QRF_EDIT_QUERY_TYPE = "qrfEditQueryType";
	
	//general initiate constants
	public static final String INITIATE_QUERY_SESSION_NAME = "initiate.query.details";
	
	//day, month , year collection
	public static final String DAY 		= "day";
	public static final String MONTH 	= "month";
	public static final String YEAR 	= "year";
	public static final String CUMULATIVE_YEAR 	= "cumulativeYear";
	
	//constants for preparatory types
	public static final int REPOSITORY = 6;
	public static final int PSURS = 1;
	public static final int CDS = 2;
	public static final int ISSUE_WORKUPS = 5;
	public static final int LITERATURE = 3;
	public static final int OTHERS = 7;
	
	//generic constants
	public static final String YES = "Yes";
	public static final String NO = "No";
	
	//form constants
    public static final String FRM_QRF = "qrfForm";
    public static final String FRM_COMM = "FRM_COMM";
    public static final String FRM_QUERY_DESC = "queryDescForm";
    public static final String FRM_CASE = "caseForm";
    public static final String FRM_DRUG = "drugForm";
    public static final String FRM_CLINICAL = "clinicalForm";
    public static final String FRM_IND = "indForm";
    public static final String FRM_QUERY_LABEL = "queryLabelForm";
    public static final String FRM_SEARCH_CRITERIA = "searchCriteriaForm";

    public static final String FRM_QUERY_EXPIRY_DATE = "queryExpiryDateForm";

    //
    public static final String FRM_QRF_BACK = "QRF_BACK";
    //preview page mode
    public static final String PREVIEW_MODE_ALL = "ALL";
    public static final String PREVIEW_MODE_AMND_DEL_PRNT = "ADP";
    public static final String PREVIEW_MODE_ASSIGNED_AMND_DEL_PRNT = "AADP";
	public static final String PREVIEW_MODE_ASSIGNED_DELETE_PRNT = "ADELP";
    public static final String PREVIEW_MODE_PRNT = "PRNT";
    public static final String PREVIEW_MODE_PRNT_CLOSE = "PRNC";

    public static final String PREVIEW_MODE_DUPLCHK_ACC_REJECT_AMNDTYP = "DARA";
    public static final String FRM_CASES = "FRM_CASES" ;
}