package com.roche.dss.qtt.query.web.menu;

import java.util.List;

/**
 * @author zerkowsm
 * 
 */
public interface MenuService {

	List<Menu> getMenuList();

}