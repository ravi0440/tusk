package com.roche.dss.qtt.service.impl;

import com.roche.dss.qtt.query.comms.dao.ReportDAO;
import com.roche.dss.qtt.query.comms.dto.ReportDTO;
import com.roche.dss.qtt.query.web.model.ReportCriteria;
import com.roche.dss.qtt.service.ReportService;
import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("reportService")
public class ReportServiceImpl implements ReportService {
    private static final Logger logger = LoggerFactory.getLogger(ReportServiceImpl.class);
    private static final String JXLS_FILE_PATH = "/xls/results-jxls.xls";
    private static final String[] REPORT_HEADERS = {"Query Number", "Query Label", "Initiated (dd-mon-yyyy)", "Initiated (mon-yyyy)", "Initiated (yyyy)", "Drug Retrieval Name", "Generic Drug Name", "Drug Indication", "Organisation Type", "Requester Country", "Requester Surname", "Turnaround (days)", "Workflow Route", "Query Type", "Reporter Type","EU QPPV Flag", "Reporter Country", "Query Owner", "Status", "Orig.-Final Due Date (days)", "Final-Delivery Date (days)"};
	private static final char CSV_DELIMITER = ';';
//	private static final char CSV_QUOTE = '"';
    private ReportDAO reportDAO;

    @Resource
    public void setReportDAO(ReportDAO reportDAO) {
        this.reportDAO = reportDAO;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ReportDTO> retrieveReportData(ReportCriteria reportCriteria) {
        return reportDAO.getReportData(reportCriteria);
    }

    @Override
    public ByteArrayOutputStream exportToXLS(ReportCriteria reportCriteria) throws IOException, InvalidFormatException {
        List<ReportDTO> resultList = reportDAO.getReportData(reportCriteria);
        Map<String, List<ReportDTO>> beans = new HashMap<String, List<ReportDTO>>();
        beans.put("queries", resultList);

        InputStream inputStream = new ClassPathResource(JXLS_FILE_PATH).getInputStream();
        logger.debug("inputStream " + inputStream);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        XLSTransformer transformer = new XLSTransformer();
        HSSFWorkbook resultWorkbook = (HSSFWorkbook) transformer.transformXLS(inputStream, beans);
        resultWorkbook.write(outputStream);

        return outputStream;
    }

	@Override
	public ByteArrayOutputStream exportToCSV(ReportCriteria reportCriteria)
			throws IOException, InvalidFormatException {
        List<ReportDTO> resultList = reportDAO.getReportData(reportCriteria);
        
        List<String[]> csvs = new ArrayList<String[]>(resultList.size());
        for (ReportDTO dto : resultList) {
        	csvs.add(dto.toStringArray());
        }
        
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        CSVWriter writer = new CSVWriter(new OutputStreamWriter(out), CSV_DELIMITER);
        writer.writeNext(REPORT_HEADERS);
        writer.writeAll(csvs);
        writer.close();

        return out;
	}
}
