package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.QueryAssessmentEAO;
import com.roche.dss.qtt.eao.QueryEAO;
import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.QttCountry;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryAssessment;
import com.roche.dss.qtt.model.QueryStatusCode;
import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.model.QueryVerification;
import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;
import com.roche.dss.qtt.query.web.qrf.Constants;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.roche.dss.util.ApplicationUtils.WHITESPACE;

@Repository("queryAssessmentEAO")
public class QueryAssessmentEAOImpl extends AbstractBaseEAO<QueryAssessment> implements QueryAssessmentEAO {
   

	@Override
	public QueryAssessment findByQueryIdAndFollowupNumber( long queryId, String followupNUmber){
		try{
			
		 return (QueryAssessment)getEntityManager().createNamedQuery("QueryAssessment.findbyIDAndFollowUpNumber").setParameter("querySeq", queryId).setParameter("followupNumber", followupNUmber!=null?followupNUmber:"0").getSingleResult();
		}catch(NoResultException e){
			 return null;
		 }
		 
	}		
	
}
