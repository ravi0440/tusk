package com.roche.dss.qtt.service;

import com.roche.dss.qtt.model.AffiliateNote;
import com.roche.dss.qtt.model.NonAffiliateNote;

/**
 * User: pruchnil
 */
public interface QueryNoteService {
    void flush();

    String findContent(long id, boolean affiliate);

    NonAffiliateNote mergeNonAffiliateNote(NonAffiliateNote nonAffiliateNote);

    AffiliateNote mergeAffiliateNote(AffiliateNote affiliateNote);

    void removeNonAffiliateNote(NonAffiliateNote nonAffiliateNote);

    void removeAffiliateNote(AffiliateNote affiliateNote);

    NonAffiliateNote findNonAffiliateNote(long id);

    AffiliateNote findAffiliateNote(long id);

    void saveQueryNote(long noteId, long queryId, String content, boolean affiliate, String userName);

    void removeQueryNote(long id, boolean affiliate);
}
