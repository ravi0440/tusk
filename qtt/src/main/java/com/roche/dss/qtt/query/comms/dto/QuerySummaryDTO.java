package com.roche.dss.qtt.query.comms.dto;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.utility.Activities;
import com.roche.dss.qtt.utility.Nodes;
import java.util.Date;

import static com.roche.dss.qtt.utility.Activities.*;

/**
 * @author zerkowsm
 */
public class QuerySummaryDTO {
    private static final Logger logger = LoggerFactory.getLogger(QuerySummaryDTO.class);
    private static final int DRUG_NAME_TRUNC_SIZE = 30;
    private static final int QUERY_LABEL_TRUNC_SIZE = 60;
    //    private static final int COUNTRY_TRUNC_SIZE = 6;
    private static final int TYPE_TRUNC_SIZE = 8;

    protected Query query;
    protected TaskInstance task;
    protected Date dateInitiated;
    protected String truncQueryLabel;
    protected String truncDrugName;
    protected String truncType;
    protected boolean displayDMG;
    protected boolean displayMI;
    protected boolean displayAwaken;
    protected boolean displayUserTasks = true;
    protected boolean displayManualReassign = true;
    protected boolean displayDetails;
    protected boolean finalDocExists;
    protected String cssStyle;

    public QuerySummaryDTO(Query query, TaskInstance task) {
        this.query = query;
        this.task = task;
    }

    public QuerySummaryDTO(Query query, TaskInstance task, boolean finalDocExists) {
        this.query = query;
        this.task = task;
        this.finalDocExists = finalDocExists;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public TaskInstance getTask() {
        return task;
    }

    public void setTask(TaskInstance task) {
        this.task = task;
    }

    public Date getDateInitiated() {
        return task.getProcessInstance().getStart();
    }

    public void setDateInitiated(Date dateInitiated) {
        this.dateInitiated = dateInitiated;
    }

    public String getTruncQueryLabel() {
        return truncate(query.getQueryLabel(), QUERY_LABEL_TRUNC_SIZE);
    }

    public void setTruncQueryLabel(String truncQueryLabel) {
        this.truncQueryLabel = truncQueryLabel;
    }

    public String getQueryLabel() {
        return query.getQueryLabel();
    }

    public String getTruncDrugName() {
        return truncate(query.getDrugName(), DRUG_NAME_TRUNC_SIZE);
    }

    public void setTruncDrugName(String truncDrugName) {
        this.truncDrugName = truncDrugName;
    }

    public String getTruncType() {
        return truncate(query.getQueryType().getQueryType(), TYPE_TRUNC_SIZE);
    }

    public void setTruncType(String truncType) {
        this.truncType = truncType;
    }

    public boolean isDisplayDMG() {
        return ArrayUtils.contains(UNKNOWN.getDmgActivities(), task.getName());
//        return task.getToken().getNode().getName().startsWith("DMG-");
    }

    public void setDisplayDMG(boolean displayDMG) {
        this.displayDMG = displayDMG;
    }

    public boolean isDisplayMI() {    	
        return ArrayUtils.contains(UNKNOWN.getPsActivities(), task.getName());
//        return task.getToken().getNode().getName().startsWith("MI-");
    }

    public void setDisplayMI(boolean displayMI) {
        this.displayMI = displayMI;
    }

    public boolean isDisplayAwaken() {
        return AWAITING_FOLLOWUP.getTaskName().equals(task.getName());
//        return Nodes.AWAITING_FOLLOW_UP.getNodeName().equals(task.getToken().getNode().getName());
    }

    public void setDisplayAwaken(boolean displayAwaken) {
        this.displayAwaken = displayAwaken;
    }

    public boolean isDisplayUserTasks() {
        return displayUserTasks;
    }

    public void setDisplayUserTasks(boolean displayUserTasks) {
        this.displayUserTasks = displayUserTasks;
    }

    public boolean isDisplayManualReassign() {
        return displayManualReassign;
    }

    public void setDisplayManualReassign(boolean displayManualReassign) {
        this.displayManualReassign = displayManualReassign;
    }

    public boolean isDisplayDetails() {
        return (query.getCloseErrorStatus() == null || query.getCloseErrorStatus().equals(Character.valueOf('0')));
    }

    public void setDisplayDetails(boolean displayDetails) {
        this.displayDetails = displayDetails;
    }

    public boolean isAssigned() {
        return (task.getActorId() != null && !task.getActorId().equals("DSCL") && !task.getActorId().equals("DMGCO"));
    }

    public boolean isFinalDocExists() {
        boolean temp = finalDocExists;
        return temp;
    }

    public boolean isFollowup() {
        return query.hasFollowup();
    }

    public String getCssStyle() {
        return cssStyle;
    }

    public String setCssStyle(DateTime date) {
        final int CRITICAL_CEILING = 1;
        final int URGENT_CEILING = 2;
        final int SOON_CEILING = 7;

        final String CSS_NORMAL = "Smalltext";
        final String CSS_SOON = "SmalltextDueSoon";
        final String CSS_URGENT = "SmalltextDueUrgent";
        final String CSS_CRITICAL = "SmalltextDueCritical";

        if (date == null) {
            return cssStyle = CSS_NORMAL;
        }
        if (withinDaysOfToday(date, CRITICAL_CEILING)) {
            cssStyle = CSS_CRITICAL;
        } else if (withinDaysOfToday(date, URGENT_CEILING)) {
            cssStyle = CSS_URGENT;
        } else if (withinDaysOfToday(date, SOON_CEILING)) {
            cssStyle = CSS_SOON;
        } else {
            cssStyle = CSS_NORMAL;
        }
        return cssStyle;
    }

    private boolean withinDaysOfToday(DateTime date, int n) {
        DateTime today = new DateTime();
        today = today.plusDays(n);
        return !today.isBefore(date);
    }

    protected String truncate(String s, int maxsize) {
        if (s == null) {
            return "";
        }
        if (s.length() > maxsize) {
            StringBuffer buf = new StringBuffer(s.substring(0, maxsize - 4));
            buf.append("...");
            return buf.toString();
        } else {
            return s;
        }
    }

}
