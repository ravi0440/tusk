package com.roche.dss.qtt.model;

@Entity
@Table(name = "MANUFACTURING")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Manufacturing implements java.io.Serializable {

	private static final long serialVersionUID = 6661703062479188108L;
	
	public static enum Type {
		TECHNICAL_COMPLAINT("Technical Complaint"), POTENTIAL_RECALL("Potential Recall (GEG)");
		
		private String label;
		
		private Type(String label) {
			this.label = label;
		}
		
		public String getLabel() {
			return label;
		}
	}

    @GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "query"))
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "QUERY_SEQ", nullable = false, precision = 10, scale = 0)
    private long querySeq;

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private Query query;

    private Type type;
	private String batchNo;
	private String trackwiseNo;

	public Manufacturing() {
	}

	public Manufacturing(Query query) {
		this.query = query;
		this.querySeq = query.getQuerySeq();
	}

	public long getQuerySeq() {
		return this.querySeq;
	}

	public void setQuerySeq(long querySeq) {
		this.querySeq = querySeq;
	}

	public Query getQuery() {
		return this.query;
	}

	public void setQuery(Query query) {
		this.query = query;
	}

	@Enumerated(EnumType.STRING)
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getTrackwiseNo() {
		return trackwiseNo;
	}

	public void setTrackwiseNo(String trackwiseNo) {
		this.trackwiseNo = trackwiseNo;
	}

}
