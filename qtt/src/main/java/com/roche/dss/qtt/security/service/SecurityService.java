package com.roche.dss.qtt.security.service;

import java.util.Map;

import com.roche.dss.qtt.QttGlobalConstants;
import com.roche.dss.qtt.model.NonRocheUser;
import com.roche.dss.qtt.security.model.DSSUser;
import com.roche.dss.qtt.security.user.RocheUserDetails;

/**
 * @author zerkowsm
 *
 */
public interface SecurityService extends QttGlobalConstants {

	public RocheUserDetails getLoggedUser();
	public String getLoggedUserDisplayName();
	public String getLoggedUserName();
	public boolean isAllowedCheck(String permission);
	public Map<String, Object> getDSSUsers(String applicationCode, String[] permissionCodes);
    DSSUser getDSSUser(String actorId);
    NonRocheUser checkNonRocheUser(String login, String password);
}