package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;

import java.util.List;

import org.joda.time.DateTime;

public interface QueryEAO extends BaseEAO<Query> {
    List<Object[]> getQueryList(String email, String column, String order);

    DateTime getLatestQueryStartDate(long processInstanceId);

    List<Query> findOpenQueryListByCountry(String countryCode);

    List<Query> findOpenQueryList();
    
    List findQueryAndTaskForVerifier(String verifier);

    List<Query> getDuplicateQueries(String countryCode, int reporterTypeId, QueryType.Code queryTypeCode, boolean allDrugs, String retrDrug, String genericDrug);

    boolean checkHasStatus(long queryId, String status);
    
    List<Query> getInitiateList();
    
    List<Query> findClosedQueryList();

    boolean checkHaLegalNoDocs(long queryId);

    Long getMaxFollowupSeq(Long queyrId);

	DateTime getStartDateOfCurrentInteration(Long processInstanceId);
}
