/*
====================================================================
  $Header$
  @author $Author: cimerl $
  @version $Revision: 2679 $ $Date: 2013-10-29 14:53:21 +0100 (Wt, 29 paź 2013) $

====================================================================
*/
package com.roche.dss.qtt.query.web.action.oldqrf;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.roche.dss.qtt.model.QttCountry;
import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.query.comms.dto.AttachmentDTO;
import com.roche.dss.qtt.query.comms.dto.CumulativePeriodDTO;
import com.roche.dss.qtt.query.comms.dto.DrugDTO;
import com.roche.dss.qtt.query.comms.dto.OrganisationDTO;
import com.roche.dss.qtt.query.comms.dto.OrganisationTypeDTO;
import com.roche.dss.qtt.query.comms.dto.PreparatoryDTO;
import com.roche.dss.qtt.query.comms.dto.QueryDTO;
import com.roche.dss.qtt.query.comms.dto.ReporterTypeDTO;
import com.roche.dss.qtt.query.comms.dto.RequesterDTO;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.Constants;
import com.roche.dss.qtt.query.web.qrf.QRFLists;
import com.roche.dss.qtt.query.web.qrf.model.QrfFormModel;
import com.roche.dss.qtt.service.FileOperationsService;
import com.roche.dss.qtt.service.QrfService;
import com.roche.dss.qtt.utility.date.DateHelper;

public class StdQrfAction extends QRFAbstractAction {

    private QrfService qrfService;
    public QRFLists qrflists = new QRFLists();

    QrfFormModel formModel = new QrfFormModel();

    private static final Logger logger = LoggerFactory.getLogger(StdQrfAction.class);
    private static final long MAX_ATTACHMENT_SIZE = 1073741824;

    private FileOperationsService fileOpsService;

    @Resource
    public void setFileOperationsService(FileOperationsService fileOpsService) {
        this.fileOpsService = fileOpsService;
    }

    @Resource
    public void setQrfService(QrfService qrfService) {
        this.qrfService = qrfService;
    }


    @Override
    @VisitorFieldValidator(message="",appendPrefix=false)
    public Object getModel() {
        return formModel;
    }


    /* (non-Javadoc)
      * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
      */
    public String execute() throws Exception {

        //build the queryDto dto
        QueryDTO queryDto = new QueryDTO();
        List attachmentsList = new ArrayList();

        //build requester
        queryDto.setRequester(doRequester(formModel));

        //if organisation is affiliate build the preparatory
        if (queryDto.getRequester().getOrg().getTypeId() == ActionConstants.ROCHE_AFFILIATE) {
            queryDto.setPreparatory(doPreparatory(formModel));
            queryDto.setPrepComments(formModel.getComments());
        }

        queryDto.setSourceCountry(formModel.getSourceCountry());
        queryDto.setReporterType(formModel.getReporterType());

        queryDto.setAllDrug(formModel.getAllDrugs() == 1);

        if (!queryDto.isAllDrug()) {
            //build the drug details if the 'all drug' is set to 'no;
            queryDto.setDrug(doDrug(formModel));
            //also set the other drug value
            queryDto.setQuerySummary(formModel.getOthersDrug());
        }
        
        //set the date response requested by
        queryDto.setDateRequested(formModel.getDateRequested().getTime());

        if ((formModel.getUrgency() == null) || (formModel.getUrgency().length() <= 0))
            queryDto.setUrgent(false);
        else
            queryDto.setUrgent(true);
        queryDto.setUrgency(formModel.getUrgency());
      

        //build the attachments
        // (AJW) bug-fix #211 - add 'deposited by' username of person who uploaded attachments
        String depositedBy = "";
        try {
            depositedBy = getUserDetails().getUsername();
        } catch (Exception ex) {
            logger.debug("Could not retrieve username for current user - will store empty value for Attachments.'depositedBy'");
        }
        //queryDto.setAttachments( doAttachments(formModel, depositedBy ) );
        // (AJW) end fix.

        //build cumulative period
        queryDto.setCumulative(doCumulative(formModel));

        queryDto.setQueryType(formModel.getQueryType());
        queryDto.setRequesterQueryLabel(formModel.getRequesterQueryLabel());

        //based on the queryDto type set the status and get the next page
        String nextPage = setStatus(queryDto);

        int queryId = 0;


        // now add/update this record
        //QrfFacadeBD qrf = (QrfFacadeBD)BusinessDelegateFactory.getFactory().getServiceBD(mapping);

        //if we need to update an existing queryDto else create a new one
        if (formModel.getId() > 0) {

            logger.debug("StdQrfAction: Updating Query");
            //set the action type for the dao i.e. if partial or provisional....see dao.updateQRF() for details
            queryDto.setAction(formModel.getMyAction());

            //Note: If the update is after actioning 'find partial' then we delete any exisiting attachments for this queryDto.
            //Else if the update is after actioning 'find provisional' we simply add new ones and keep the old attachments as it is.
            if (queryDto.getAction().equalsIgnoreCase("partial")
                    && (queryDto.getAttachments() != null)
                    && (!queryDto.getAttachments().isEmpty())) {

                /*	String dir = FileOperationsHelper.getInstance().findDestinationDirectory(String.valueOf(formModel.getId()));

                        boolean successful = FileOperationsHelper.getInstance().deleteDir(new File(dir));
                        if (!successful) {
                            logger.error("QRF: The File path for dsfgsdfgattachments would not be found!");
                            return failure;
                        }			*/
            }

            //upload the attachments if any added


            List<Integer> idsAttachmentsToRemove = new ArrayList<Integer>();
            boolean successful = doUpload(formModel.getId(), depositedBy, attachmentsList, idsAttachmentsToRemove);
            queryDto.setAttachments(attachmentsList);

            if (!successful) {
                logger.error("QRF: One or more Attachment file could not be uploaded!");
                return "failure";
            }


            //update this queryDto
            if (getSession().get(ActionConstants.QRF_EDIT_QUERY_TYPE) != null) {
            	QueryType.Code oldQueryTypeCode = QueryType.Code.valueOf((String) getSession().get(ActionConstants.QRF_EDIT_QUERY_TYPE));
                //if queryDto type changed then delete old queryDto types data
                if (queryDto.getQueryType() != oldQueryTypeCode) {
                    qrfService.deleteOldQueryType(formModel.getId(), oldQueryTypeCode);
                }
            }
            qrfService.updateQRF(queryDto, formModel.getId(), idsAttachmentsToRemove);

            //set the id in the session
            getSession().put(ActionConstants.QRF_QUERY_ID, formModel.getId());
        } else {
            logger.debug("StdQrfAction: Creating Query");
            queryId = qrfService.createQRF(queryDto);

            //set the id in the session
            getSession().put(ActionConstants.QRF_QUERY_ID, queryId);
            List<Integer> idsAttachmentsToRemove = new ArrayList<Integer>();
            boolean successful = doUpload(queryId, depositedBy, attachmentsList, idsAttachmentsToRemove);
            queryDto.setAttachments(attachmentsList);
            if (!successful) {
                nextPage = "failure";
            } else {
                qrfService.createAttachments(queryDto.getAttachments(), queryId);
            }
        }
        //populate the nature of case as request parameter if coming from psst
        // HR 2008.10.20 removing psst
        /*
          if (getSession().get(ActionConstants.PSST_QUERY_NUMBER) != null) {
              return new ForwardingActionForward (mapping.findForward(nextPage).getPath() + "?" + ActionConstants.PSST_QUERY_NUMBER + "=" + getSession().get(ActionConstants.PSST_QUERY_NUMBER)) ;
          }
          else
          */
        return nextPage;
    }


    @Override
    public void validate() {

        logger.debug("validate");
        /*         addActionError(getText("dateTo.format"));*/

        GregorianCalendar currentDate = new GregorianCalendar();
        DateHelper dateHelper = DateHelper.getInstance();

        // fix bug #188 - avoid "invalid email address" message appearing alongside "..is required" message
        if (formModel.getEmail() != null && !"".equals(formModel.getEmail())) {
            if (!validateEmail(formModel.getEmail())) {
                addActionError(getText("qrfForm.email.invalid.displayname"));
            }
        }
        // end fix.

        //validate is "blank"is selected for these drop-downs
        if (formModel.getOrgType() == 0) {
            addActionError(getText("qrfForm.orgType.displayname"));
        }

        if ("0".equals(formModel.getReqCountry())) {
            addActionError(getText("qrfForm.reqCountry.displayname"));
        }

        if ("0".equals(formModel.getSourceCountry())) {
            addActionError(getText("qrfForm.sourceCountry.displayname"));
        }

        if (0 == formModel.getReporterType()) {
            addActionError(getText("qrfForm.reporterType.displayname"));
        }

        //if all drug is set to 'no' either the drug retrieval name or generic drug name is required
        if (formModel.getAllDrugs() == 0) {

            //check if drug retrieval name is "0"
            if ("0".equals(formModel.getRetrDrug()))
                addActionError(getText("qrfForm.retrDrug.displayname"));
        }

        //check for the organisation type and the query type
        if (formModel.getOrgType() == ActionConstants.ROCHE_AFFILIATE
                || formModel.getOrgType() == ActionConstants.LICENSING_PARTNER
                || formModel.getOrgType() == ActionConstants.OTHER) {


            //the above organisation types are not allowed to raise the query types below
            if (formModel.getQueryType() == QueryType.Code.PERFORMANCE_METRICS
                    || formModel.getQueryType() == QueryType.Code.PSUR_ASR
                    || formModel.getQueryType() == QueryType.Code.SIGNAL_DETECTION
                    || formModel.getQueryType() == QueryType.Code.LITERATURE_SEARCH)
                addActionError(getText("qrfForm.orgType.notAllowed.displayname"));
        }

        //validate file length
        if ((formModel.getFileOne() != null) && (formModel.getFileOneFileName().length() > 100)) {
        	formModel.setFileOne(null);
        	formModel.setFileOneFileName(null);
            addActionError(getText("qrfForm.maxlength.fileOne"));
        }

        if ((formModel.getFileTwo() != null) && (formModel.getFileTwoFileName().length() > 100)) {
        	formModel.setFileTwo(null);
        	formModel.setFileTwoFileName(null);
            addActionError(getText("qrfForm.maxlength.fileTwo"));
        }
        if ((formModel.getFileThree() != null) && (formModel.getFileThreeFileName().length() > 100)) {
        	formModel.setFileThree(null);
        	formModel.setFileThreeFileName(null);
            addActionError(getText("qrfForm.maxlength.fileThree"));
        }
        if ((formModel.getFileFour() != null) && (formModel.getFileFourFileName().length() > 100)) {
        	formModel.setFileFour(null);
        	formModel.setFileFourFileName(null);
            addActionError(getText("qrfForm.maxlength.fileFour"));
        }
        if ((formModel.getFileFive() != null) && (formModel.getFileFiveFileName().length() > 100)) {
        	formModel.setFileFive(null);
        	formModel.setFileFiveFileName(null);
            addActionError(getText("qrfForm.maxlength.fileFive"));
        }

        //validate
        if ((formModel.getFileOne() == null && formModel.getTempFile1()==null) && (formModel.getDescOne() != null) && (formModel.getIdOne() == null)) {
            addActionError(getText("qrfForm.fileOne.displayname"));
        }
        if ((formModel.getFileTwo() == null && formModel.getTempFile2()==null) && (formModel.getDescTwo() != null) && (formModel.getIdTwo() == null)) {
            addActionError(getText("qrfForm.fileTwo.displayname"));
        }
        if ((formModel.getFileThree() == null && formModel.getTempFile3()==null) && (formModel.getDescThree() != null) && (formModel.getIdThree() == null)) {
            addActionError(getText("qrfForm.fileThree.displayname"));
        }
        if ((formModel.getFileFour() == null && formModel.getTempFile4()==null) && (formModel.getDescFour() != null) && (formModel.getIdFour() == null)) {
            addActionError(getText("qrfForm.fileFour.displayname"));
        }
        if ((formModel.getFileFive() == null && formModel.getTempFile5()==null) && (formModel.getDescFive() != null) && (formModel.getIdFive() == null)) {
            addActionError(getText("qrfForm.fileFive.displayname"));
        }
        
        if ((formModel.getFileOne() != null || formModel.getTempFile1()!=null) && (formModel.getDescOne() == null)) {
            addActionError(getText("field.required", new String[]{getText("qrfForm.descOne.displayname")}));
        }
        if ((formModel.getFileTwo() != null || formModel.getTempFile2()!=null) && (formModel.getDescTwo() == null)) {
        	addActionError(getText("field.required", new String[]{getText("qrfForm.descTwo.displayname")}));
        }
        if ((formModel.getFileThree() != null || formModel.getTempFile3()!=null) && (formModel.getDescThree() == null)) {
        	addActionError(getText("field.required", new String[]{getText("qrfForm.descThree.displayname")}));
        }
        if ((formModel.getFileFour() != null || formModel.getTempFile4()!=null) && (formModel.getDescFour() == null)) {
        	addActionError(getText("field.required", new String[]{getText("qrfForm.descFour.displayname")}));
        }
        if ((formModel.getFileFive() != null || formModel.getTempFile5()!=null) && (formModel.getDescFive() == null)) {
        	addActionError(getText("field.required", new String[]{getText("qrfForm.descFive.displayname")}));
        }       

        if (formModel.getFileOne() != null && formModel.getFileOne().length() > MAX_ATTACHMENT_SIZE) {
            addActionError(getText("qrf.attachment.file.too.large", new String[]{"1"}));
        }
        if (formModel.getFileTwo() != null && formModel.getFileTwo().length() > MAX_ATTACHMENT_SIZE) {
            addActionError(getText("qrf.attachment.file.too.large", "2"));
        }
        if (formModel.getFileThree() != null && formModel.getFileThree().length() > MAX_ATTACHMENT_SIZE) {
            addActionError(getText("qrf.attachment.file.too.large", "3"));
        }
        if (formModel.getFileFour() != null && formModel.getFileFour().length() > MAX_ATTACHMENT_SIZE) {
            addActionError(getText("qrf.attachment.file.too.large", "4"));
        }
        if (formModel.getFileFive() != null && formModel.getFileFive().length() > MAX_ATTACHMENT_SIZE) {
            addActionError(getText("qrf.attachment.file.too.large", "5"));
        }

        //validate date requested by is entered at all
        if (formModel.getDateRequested() == null && !getFieldErrors().containsKey("dateRequested")) {
            addActionError(getText("qrfForm.dateResponse.displayname"));
        } else if (formModel.getDateRequested() != null) {
            GregorianCalendar dateRequested = new GregorianCalendar();
            dateRequested.setTime(formModel.getDateRequested());
            dateRequested.set(Calendar.HOUR_OF_DAY, 23);
            dateRequested.set(Calendar.MINUTE, 59);
            dateRequested.set(Calendar.SECOND, 59);

            // date is valid, so perform other checks...
            // 1) if less than todays date
            if (dateRequested.before(currentDate)) {
                addActionError(getText("qrfForm.dd.displayname"));
            } else {
                // 2) validate if the urgency is entered or not when date is less than 14 days from now
                byte forward = 0;         // check forward direction only
                if (dateHelper.withinDaysOfToday(dateRequested, 14, forward)) {
                     if ((formModel.getUrgency() == null) || (formModel.getUrgency().length() <= 0)) {
                        addActionError(getText("qrfForm.urgency.displayname"));
                    }
                }
            }
        }

        // perform check for cumulative to/from dates - changes due to fix of bug #189
        // (needed to refactor the validation logic)
        GregorianCalendar fromDate = null;
        GregorianCalendar toDate = null;
        if (formModel.getCumulative() == 0) {
            // validate cumulative date 'from' is entered at all
            if (formModel.getFrom() == null && !getFieldErrors().containsKey("from")) {
                addActionError(getText("qrfForm.cumulative.ddFrom.displayname"));
            } else if (formModel.getFrom() != null) {
                // set the cumulative 'from' date
                fromDate = new GregorianCalendar();
                fromDate.setTime(formModel.getFrom());
                fromDate.set(Calendar.HOUR_OF_DAY, 23);
                fromDate.set(Calendar.MINUTE, 59);
                fromDate.set(Calendar.SECOND, 59);
            }

            //validate cumulative date 'to' is entered at all
            if (formModel.getTo() == null && !getFieldErrors().containsKey("to")) {
                addActionError(getText("qrfForm.cumulative.ddTo.displayname"));
            } else if (formModel.getTo() != null) {
                // set the cumulative 'to' date
                toDate = new GregorianCalendar();
                toDate.setTime(formModel.getTo());
                toDate.set(Calendar.HOUR_OF_DAY, 23);
                toDate.set(Calendar.MINUTE, 59);
                toDate.set(Calendar.SECOND, 59);
            }

            // if all dates are entered
            if (fromDate != null && toDate != null) {
                //if cumulative to date less than cumulative from date
                if (toDate.before(fromDate)) {
                    addActionError(getText("qrfForm.mmTo.displayname"));
                }
            }
        }
        // end fix.

        if (formModel.getQueryType() == null) {
            addActionError(getText("qrfForm.queryType.displayname"));
        }

        //set the pref and generic name to empty if "0" (blank)
        if ((formModel.getRetrDrug() != null) && (formModel.getRetrDrug().equals("0")))
            formModel.setRetrDrug(null);
        if ((formModel.getGenericDrug() != null) && (formModel.getGenericDrug().equals("0")))
            formModel.setGenericDrug(null);

        if (hasActionErrors() || hasFieldErrors()) {
            onError();
        }
        super.validate();
    }

    public void onError() {
        try {
        	File[] files = new File[]{formModel.getFileOne(), formModel.getFileTwo(), formModel.getFileThree(), formModel.getFileFour(), formModel.getFileFive()};
        	String[] tempFileNames = new String[]{formModel.getTempFileName1(), formModel.getTempFileName2(), formModel.getTempFileName3(), formModel.getTempFileName4(), formModel.getTempFileName5()};
            for (int i=0; i < files.length; i++) {
            	File file = files[i];
            	String tempFileName = tempFileNames[i];
                if (file != null && file.getName().length() <= 100 ) {
                    fileOpsService.insertFileIntoRepository("temp_folder", file, new StringBuffer(file.getName()));                    
                } else if (tempFileName == null || tempFileName.length() > 100) {
                	//do not delete the temp file unless the filename is longer than 100 characters
                	switch(i){                	
                		case 0 : formModel.setFileOneFileName(null); formModel.setFileOne(null); formModel.setTempFile1(null); formModel.setTempFileName1(null); break;
                		case 1 : formModel.setFileTwoFileName(null); formModel.setFileTwo(null); formModel.setTempFile2(null); formModel.setTempFileName2(null); break;
                		case 2 : formModel.setFileThreeFileName(null); formModel.setFileThree(null); formModel.setTempFile3(null); formModel.setTempFileName3(null); break;
                		case 3 : formModel.setFileFourFileName(null); formModel.setFileFour(null); formModel.setTempFile4(null); formModel.setTempFileName4(null); break;
                		case 4 : formModel.setFileFiveFileName(null); formModel.setFileFive(null); formModel.setTempFile5(null); formModel.setTempFileName5(null); break;                		
                	}
                	getSession().put("formModel", formModel);
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
    /**
     * String
     *
     * @author shaikhi
     * <p/>
     * Validate the email address
     */
    public boolean validateEmail(String email) {
        return (email != null && email.matches(EmailValidator.emailAddressPattern));
    }

    public List<ReporterTypeDTO> getReporterTypes() {
        return qrfService.retrieveReporterTypes();
    }

    public List<OrganisationTypeDTO> getOrganisationTypes() {
        return qrfService.retrieveOrganisationTypes();
    }

    public QRFLists getQrfLists() {
        return qrflists;
    }


    public Map<String, String> getQueryTypeUrls() {
        return qrfService.getQueryTypeUrls();
    }

    /*
      * Populate the first drug details
      */
    private DrugDTO doDrug(QrfFormModel formModel) {

        DrugDTO drug = new DrugDTO();
        drug.setRetrName(formModel.getRetrDrug());
        drug.setGenericName(formModel.getGenericDrug());
        drug.setRoute(formModel.getRoute());
        drug.setFormulation(formModel.getFormulation());
        drug.setIndication(formModel.getIndication());
        drug.setDose(formModel.getDose());
        return drug;
    }

    /*
      * Populate the requester and organisation details
      */
    private RequesterDTO doRequester(QrfFormModel formModel) {

        //lets create the reporter dto first
        RequesterDTO requester = new RequesterDTO();
        requester.setFirstName(convertFirstUpper(formModel.getFirstname()));
        requester.setLastName(convertFirstUpper(formModel.getLastname()));
        requester.setTelephone(formModel.getTelephone());
        requester.setFax(formModel.getFax());
        requester.setEmailAddress(formModel.getEmail());
        requester.setCountryCode(formModel.getReqCountry());

        //build organisation dto
        OrganisationDTO org = new OrganisationDTO();
        org.setName(formModel.getOrgName());
        org.setTypeId(formModel.getOrgType());
        requester.setOrg(org);

        return requester;
    }

    /*
       * Populate the cumulative period details
       */
    private CumulativePeriodDTO doCumulative(QrfFormModel formModel) {

        CumulativePeriodDTO cum = new CumulativePeriodDTO();
        cum.setCumulative(formModel.getCumulative() == 1 ? true : false);
        if (!cum.isCumulative()) {
            //from date
            cum.setFromDate(formModel.getFrom().getTime());

            //to date
            cum.setToDate(formModel.getTo().getTime());
        }

        cum.setInterval(formModel.getInterval());
        return cum;

    }

    /*
    *//*
	 * Populate the attachment details into the query dto

	 */
    private boolean doUpload(int queryId, String depositedBy, List attachmentsList, List<Integer> idsAttachmentsToRemove) {

        logger.debug("Attachments to remove: "+formModel.getIdsToDelete());

        String idsToDelete = formModel.getIdsToDelete();
        String[] idsToDeleteArray = null;
        if (idsToDelete != null && idsToDelete.length()>0){
            idsToDeleteArray = idsToDelete.split(":");
        }
        if (idsToDeleteArray != null && !idsToDelete.isEmpty()){
            Integer i = null;
            for (String s : idsToDeleteArray){
                if (s != null && !s.isEmpty()){
                    s = s.trim();
                    if (!s.isEmpty()){
                        i = Integer.valueOf(s);
                        if (i > 0){
                            idsAttachmentsToRemove.add(i);
                        }
                    }
                }
            }
        }

        AttachmentDTO attach = null;
        StringBuffer fullFileName = null;
        try {
            if (formModel.getDescOne() != null) {
                attach = new AttachmentDTO();
                attach.setTitle(formModel.getDescOne());
                attach.setId(formModel.getIdOne() != null ? formModel.getIdOne().intValue() : 0);
                attach.setActivationFlag(!idsAttachmentsToRemove.contains(attach.getId()));
                attachmentsList.add(attach);
                if (formModel.getFileOne() != null && (!formModel.getFileOneFileName().equals(""))) {
                    fullFileName = new StringBuffer(formModel.getFileOneFileName());
                    if (!fileOpsService.insertFileIntoRepository(String.valueOf(queryId), formModel.getFileOne(), fullFileName)) {
                        return false;
                    }
                    attach.setFileName(fullFileName.toString());
                    attach.setAuthor(depositedBy);
                }  else if(formModel.getTempFile1()!=null && !formModel.getTempFileName1().equals("")) {
                    fullFileName = new StringBuffer(formModel.getTempFileName1());
                    if (!fileOpsService.insertFileIntoRepository(String.valueOf(queryId), new File(fileOpsService.findDestinationDirectory("temp_folder")+formModel.getTempFile1()), fullFileName)) {
                        return false;
                    }
                    attach.setFileName(fullFileName.toString());
                    attach.setAuthor(depositedBy);
                }
            }

            if (formModel.getDescTwo() != null) {
                attach = new AttachmentDTO();
                attach.setTitle(formModel.getDescTwo());
                attach.setId(formModel.getIdTwo() != null ? formModel.getIdTwo().intValue() : 0);
                attach.setActivationFlag(!idsAttachmentsToRemove.contains(attach.getId()));
                attachmentsList.add(attach);

                if (formModel.getFileTwo() != null && (!formModel.getFileTwoFileName().equals(""))) {
                    fullFileName = new StringBuffer(formModel.getFileTwoFileName());
                    if (!fileOpsService.insertFileIntoRepository(String.valueOf(queryId), formModel.getFileTwo(), fullFileName)) {
                        return false;
                    }
                    attach.setFileName(fullFileName.toString());
                    attach.setAuthor(depositedBy);
                }  else if(formModel.getTempFile2()!=null && !formModel.getTempFileName2().equals("")) {
                    fullFileName = new StringBuffer(formModel.getTempFileName2());
                    if (!fileOpsService.insertFileIntoRepository(String.valueOf(queryId), new File(fileOpsService.findDestinationDirectory("temp_folder")+formModel.getTempFile2()), fullFileName)) {
                        return false;
                    }
                    attach.setFileName(fullFileName.toString());
                    attach.setAuthor(depositedBy);
                }
            }

            if (formModel.getDescThree() != null) {
                attach = new AttachmentDTO();
                attach.setTitle(formModel.getDescThree());
                attach.setId(formModel.getIdThree() != null ? formModel.getIdThree().intValue() : 0);
                attach.setActivationFlag(!idsAttachmentsToRemove.contains(attach.getId()));
                attachmentsList.add(attach);

                if (formModel.getFileThree() != null && (!formModel.getFileThreeFileName().equals(""))) {
                    fullFileName = new StringBuffer(formModel.getFileThreeFileName());
                    if (!fileOpsService.insertFileIntoRepository(String.valueOf(queryId), formModel.getFileThree(), fullFileName)) {
                        return false;
                    }
                    attach.setFileName(fullFileName.toString());
                    attach.setAuthor(depositedBy);
                }  else if(formModel.getTempFile3()!=null && !formModel.getTempFileName3().equals("")) {
                    fullFileName = new StringBuffer(formModel.getTempFileName3());
                    if (!fileOpsService.insertFileIntoRepository(String.valueOf(queryId), new File(fileOpsService.findDestinationDirectory("temp_folder")+formModel.getTempFile3()), fullFileName)) {
                        return false;
                    }
                    attach.setFileName(fullFileName.toString());
                    attach.setAuthor(depositedBy);
                }
            }

            if (formModel.getDescFour() != null) {
                attach = new AttachmentDTO();
                attach.setTitle(formModel.getDescFour());
                attach.setId(formModel.getIdFour() != null ? formModel.getIdFour().intValue() : 0);
                attach.setActivationFlag(!idsAttachmentsToRemove.contains(attach.getId()));
                attachmentsList.add(attach);

                if (formModel.getFileFour() != null && (!formModel.getFileFourFileName().equals(""))) {
                     fullFileName = new StringBuffer(formModel.getFileFourFileName());
                    if (!fileOpsService.insertFileIntoRepository(String.valueOf(queryId), formModel.getFileFour(), fullFileName)) {
                        return false;
                    }
                    attach.setFileName(fullFileName.toString());
                    attach.setAuthor(depositedBy);
                }    else if(formModel.getTempFile4()!=null && !formModel.getTempFileName4().equals("")) {
                    fullFileName = new StringBuffer(formModel.getTempFileName4());
                    if (!fileOpsService.insertFileIntoRepository(String.valueOf(queryId), new File(fileOpsService.findDestinationDirectory("temp_folder")+formModel.getTempFile4()), fullFileName)) {
                        return false;
                    }
                    attach.setFileName(fullFileName.toString());
                    attach.setAuthor(depositedBy);
                }
            }

            if (formModel.getDescFive() != null) {
                attach = new AttachmentDTO();
                attach.setTitle(formModel.getDescFive());
                attach.setId(formModel.getIdFive() != null ? formModel.getIdFive().intValue() : 0);
                attach.setActivationFlag(!idsAttachmentsToRemove.contains(attach.getId()));
                attachmentsList.add(attach);

                if (formModel.getFileFive() != null && (!formModel.getFileFiveFileName().equals(""))) {
                    fullFileName = new StringBuffer(formModel.getFileFiveFileName());
                    if (!fileOpsService.insertFileIntoRepository(String.valueOf(queryId), formModel.getFileFive(), fullFileName)) {
                        return false;
                    }
                    attach.setFileName(formModel.getFileFiveFileName());
                    attach.setAuthor(depositedBy);
                }     else if(formModel.getTempFile5()!=null && !formModel.getTempFileName5().equals("")) {
                    fullFileName = new StringBuffer(formModel.getTempFileName5());
                    if (!fileOpsService.insertFileIntoRepository(String.valueOf(queryId), new File(fileOpsService.findDestinationDirectory("temp_folder")+formModel.getTempFile5()), fullFileName)) {
                        return false;
                    }
                    attach.setFileName(fullFileName.toString());
                    attach.setAuthor(depositedBy);
                }
            }
        } catch (IOException ioe) {
            logger.error("Problem while inserting attachment into repository: ", ioe);
        }

        return true;
    }

    /*
      * gets the next page based on the query type and also sets the status for this query.
      */
    private String setStatus(QueryDTO query) {

    	QueryType.Code queryTypeCode = query.getQueryType();
        query.setStatusCode(Constants.QUERY_STATUS_NOT_SUBMITTED.toString());

        String nextPage = "";
        switch (queryTypeCode) {
        case DATA_SEARCH:
        	nextPage = "dataSearch";
        	break;
        case EXTERNAL_AUDIT_INSPECTION:
        	nextPage = "extAudit";
        	break;
        case MEDICAL:
        	nextPage = "medical";
        	break;
        case MANUFACTURING_RECALL:
        	nextPage = "manufacturing";
        	break;
        case PERFORMANCE_METRICS2:
            nextPage = "perfMetrics";
            break;
        default:
            //for periodic complaints and psurs set this to submitted
            query.setStatusCode(Constants.QUERY_STATUS_NOT_SUBMITTED.toString());
            nextPage = "preview";
            break;
        }
        return nextPage;
    }

    /*
      * Populate the preparatory work
      */
    private List doPreparatory(QrfFormModel formModel) {

        List preparatoryList = new ArrayList();

        PreparatoryDTO preparatory = null;
        //get the values of all preparatory
        if (formModel.getRepository() != 0) {
            preparatory = new PreparatoryDTO();
            preparatory.setPreparatoryId(formModel.getRepository());
            preparatoryList.add(preparatory);
        }
        if (formModel.getPsur() != 0) {
            preparatory = new PreparatoryDTO();
            preparatory.setPreparatoryId(formModel.getPsur());
            preparatoryList.add(preparatory);
        }
        if (formModel.getCds() != 0) {
            preparatory = new PreparatoryDTO();
            preparatory.setPreparatoryId(formModel.getCds());
            preparatoryList.add(preparatory);
        }
        if (formModel.getIssue() != 0) {
            preparatory = new PreparatoryDTO();
            preparatory.setPreparatoryId(formModel.getIssue());
            preparatoryList.add(preparatory);
        }
        if (formModel.getLiterature() != 0) {
            preparatory = new PreparatoryDTO();
            preparatory.setPreparatoryId(formModel.getLiterature());
            preparatoryList.add(preparatory);
        }
        if (formModel.getOthers() != 0) {
            preparatory = new PreparatoryDTO();
            preparatory.setPreparatoryId(formModel.getOthers());
            //since the other is selected also get the others comment
            preparatory.setOthers(formModel.getOthersComment());
            preparatoryList.add(preparatory);
        }

        return preparatoryList;
    }

    /*
    * Utility method to convert the first character of a string to uppercase.
    */
    private String convertFirstUpper(String name) {

        if (name != null) {

            char[] letters = name.toCharArray();
            letters[0] = Character.toUpperCase(letters[0]);
            return new String(letters);
        }
        return "";

    }


    public List<QttCountry> getOrderedCountries() {
        return qttCountryEAO.getOrderedCountries();
    }
}
