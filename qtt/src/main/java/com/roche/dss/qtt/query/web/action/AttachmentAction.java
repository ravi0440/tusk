package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.web.model.FileUploadBean;
import com.roche.dss.qtt.service.AttachmentService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.utility.EmailSender;
import com.roche.dss.qtt.utility.config.Config;
import javax.annotation.Resource;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.roche.dss.qtt.query.web.action.AttachmentAction.AttachCategory.FINAL_RESP;
import static com.roche.dss.qtt.query.web.action.AttachmentAction.AttachCategory.SUPPORT_DOC;

/**
 * This action takes cares of action related to attachment: add, upload, view
 * <p/>
 * User: pruchnil
 */
public class AttachmentAction extends CommonActionSupport {
	
    private static final int MAX_FILENAME_LENGTH = 100;
    
	private static final Logger logger = LoggerFactory.getLogger(AttachmentAction.class);
    private Query query;
    private long queryId;
    private long attachId;
    private FileUploadBean attachment = new FileUploadBean();
    private Long freeDiskSpace;
    private QueryService queryService;
    private AttachmentService attachmentService;
    private String fileName;

    private InputStream inputStream;
    private String contentType;
    private String contentDisposition;

    private String nextActionName;
    private boolean closed;

    @Autowired
    private EmailSender emailSender;

    @Autowired
    private Config config;

    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Resource
    public void setAttachmentService(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    @Override
    @SkipValidation
    public String execute() throws Exception {
        query = queryService.find(queryId);
        setFreeDiskSpace();
        return SUCCESS;
    }

    public String upload() throws Exception {
    	
    	//check if file with the same name exist
    	query = queryService.find(queryId);
    	Set<Attachment> attachments = query.getAttachments();
    	
    	String[] splitted= attachment.getUploadFileName().split("\\.");
    	String originalSuffix= splitted[splitted.length-1];
    	String originalName= attachment.getUploadFileName().substring(0,attachment.getUploadFileName().lastIndexOf("."));    	 	
    	int index=1;
    	
    	boolean correctName=true;
    	do{
    		correctName=true;
	    	for(Attachment att:attachments){
	    		if(att.getFilename().equals(attachment.getUploadFileName())){
	    			// change attachment name
	    			attachment.setUploadFileName(originalName+index+"."+originalSuffix);
	    			index++;
	    			correctName=false;
	    			break;
	    		}
	    	}
    	}while(!correctName);

        Attachment addedAttachment = attachmentService.addFileToRepo(queryId, attachment, getLoggedUserName(), isAffiliate());

        if (query != null && addedAttachment != null && queryService.checkIfQueryRequestedDmgDataSearch(query)) {
            attachmentService.saveAttachmentFromDmg(query, addedAttachment);
        }

        if(closed){
            nextActionName = "ClosedQueryDetails";
        }else{
            nextActionName = "RetrieveQueryDetails";
        }
        return SUCCESS;
    }

    @SkipValidation
    public String view() throws Exception {
        Attachment attachment = attachmentService.getFileFromRepo(attachId, queryId);
        inputStream = attachment.getFileStream();
        contentType = "application/octet-stream";//new MimetypesFileTypeMap().getContentType(new File(attachment.getFilename()));
        contentDisposition = "attachment;filename=\"" + attachment.getFilename() + "\";";
        return SUCCESS;
    }

    @Override
    public void validate() {
        if (StringUtils.isEmpty(attachment.getGivenFileName())) {
            addActionError(getText("attach.name.required"));
        }
        if (StringUtils.isEmpty(attachment.getType())) {
            addActionError(getText("attach.type.required"));
        }
        if (attachment.getUpload() == null) {
            addActionError(getText("attach.file.required"));
        } else {
        	if (attachment.getUpload().length() == 0) {
                addActionError(getText("attach.file.empty"));
            }
        	if (StringUtils.length(attachment.getUploadFileName()) > MAX_FILENAME_LENGTH) {
             	addActionError(getText("attach.name.length"));
            }
        }
        super.validate();
    }

    @Override
    public void onActionErrors() {
        query = queryService.find(queryId);
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getContentType() {
        return contentType;
    }

    public String getContentDisposition() {
        return contentDisposition;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public Query getQuery() {
        return query;
    }

    public FileUploadBean getAttachment() {
        return attachment;
    }

    public void setAttachment(FileUploadBean attachment) {
        this.attachment = attachment;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getAttachId() {
        return attachId;
    }

    public void setAttachId(long attachId) {
        this.attachId = attachId;
    }

    public String getNextActionName() {
        return nextActionName;
    }

    public void setNextActionName(String nextActionName) {
        this.nextActionName = nextActionName;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public Long getFreeDiskSpace() {
        return freeDiskSpace;
    }

    public void setFreeDiskSpace(Long freeDiskSpace) {
        this.freeDiskSpace = freeDiskSpace;
    }

    public List<AttachCategory> getAttachmentCategoryList() {
        List<AttachCategory> attachCategoryList = new ArrayList<AttachCategory>();
        boolean isDMGOnly = (isDMGScientist() && !isDscl());
        if ((("B").equals(query.getWorkflowRouteType()) && isDMGOnly)) {
            attachCategoryList.add(SUPPORT_DOC);
        } else {
            attachCategoryList.add(SUPPORT_DOC);
            attachCategoryList.add(FINAL_RESP);
        }
        return attachCategoryList;
    }

    public enum AttachCategory {
        QRF(1, "QRF"), SUPPORT_DOC(2, "Supporting Documentation"), FINAL_RESP(3, "Final Response");
        private int id;
        private String name;

        AttachCategory(int i, String s) {
            this.id = i;
            this.name = s;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    private void setFreeDiskSpace() {
        File tempDir = (File) getServletContext().getAttribute("javax.servlet.context.tempdir");
        freeDiskSpace = tempDir.getFreeSpace();
        logger.info("tempDir: {}, freeDiskSpace: {}", tempDir, freeDiskSpace);
    }
}
