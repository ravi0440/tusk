package com.roche.dss.qtt.utility.pdf;

import com.roche.dss.qtt.query.web.action.InitiationEmailAction;

import java.awt.*;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class QTTpdfHelper {

	private static final Logger logger = LoggerFactory.getLogger(QTTpdfHelper.class);
	
    private Document document;

    //return the current chapter
    private Chapter currentChapter = null;

    //return the current section
    private Section currentSection = null;

    //return the current sub section
    private Paragraph currentParagraph = null;

    //return the current Table
    private PdfPTable currentTable = null;


    //return the current Table
    private PdfPTable currentNestedTable = null;
    
    // define the out
    private OutputStream outStream = null;

    //define the name of the PDF file
    private String pdfFileName = null;

    //define the font of the chapter
    private Font chapterFont = null;

    private Font headerFont = null;

    //define the font of the section
    private Font sectionFont = null;

    //define the font of the sub section
    private Font paragraphFont = null;

    //define the font of the title of colmuns
    private Font columnTitleFont = null;

    //define the font of the value in the columns
    private Font columnValueFont = null;
    
    //define the font of the nested table header
    private Font nestedTableHeaderFont = null;
    
    //define the font of the footer
    private Font footerFont = null;

    //define Header footer
    HeaderFooter footer = null;
    /**
     * @return Returns the footerFont.
     */
    protected Font getFooterFont() {
        return footerFont;
    }
    /**
     * @param footerFont The footerFont to set.
     */
    protected void setFooterFont(Font footerFont) {
        this.footerFont = footerFont;
    }
    //images folder location
    private String imagesPath = null;
    /**
     * define the document per default
     * 
     * @return document com.lowagie.text
     */
    private Document documentDefine() {

        this.document = new Document(PageSize.A4, 50, 50, 50, 50);

        return document;

    }

    /**
     * add meta information to the document
     */
    private void addMetaInformation() {

        // we add some meta information to the document
        //this.getDocument().addAuthor(author);
        //this.getDocument().addSubject(subject);

    }

    /**
     * create an instance of PDFWriter
     */
    private void createInstanceOfPDFWriter(OutputStream out) {

        try {

            PdfWriter.getInstance(this.getDocument(), out);
        } catch (DocumentException de) {
        	logger.error(de.getMessage());
        }

    }

    /**
     * initialize param of document and open it
     */
    public void open() {

        //initialize
        this.addMetaInformation();
        this.createInstanceOfPDFWriter(this.getOutStream());
        this.getDocument().setFooter(this.getFooter());
        //open
        this.getDocument().open();
    }

    /**
     * Close the document
     */
    public void close() {

        //close
        this.getDocument().close();
    }

    /**
     * Constructor: use the fonts defined by default
     *
     * @param out
     *            java.io.OutputStream out of the pdf file
     */
    public QTTpdfHelper(OutputStream out) {

        super();
        this.fontsDefine();
        this.documentDefine();

        this.setOutStream(out);
    }

    /**
     * define the font per default
     */
    private void fontsDefine() {

        this.setChapterFont(FontFactory.getFont(FontFactory.HELVETICA, 18,
                Font.NORMAL, new Color(40, 87, 139)));
        this.setHeaderFont(FontFactory.getFont(FontFactory.HELVETICA, 15,
                Font.NORMAL, new Color(40, 87, 139)));
        this.setSectionFont(FontFactory.getFont(FontFactory.HELVETICA, 12,
                Font.NORMAL, new Color(40, 87, 139)));
        this.setColumnTitleFont(FontFactory.getFont(FontFactory.HELVETICA, 10,
                Font.BOLD, new Color(40, 87, 139)));
        this.setColumnValueFont(FontFactory.getFont(FontFactory.HELVETICA, 10,
                Font.NORMAL, new Color(2, 53, 110)));
        this.setNestedTableHeaderFont(FontFactory.getFont(FontFactory.HELVETICA, 10,
                Font.BOLD, new Color(40, 87, 139)));
        this.setFooterFont(FontFactory.getFont(FontFactory.HELVETICA, 8,
                Font.BOLD, new Color(40, 87, 139)));

    }

    /**
     * @return Returns the chapterFont.
     */
    public Font getChapterFont() {
        return chapterFont;
    }

    /**
     * @param chapterFont
     *            The chapterFont to set.
     */
    public void setChapterFont(Font chapterFont) {
        this.chapterFont = chapterFont;
    }

    /**
     * @return Returns the columnTitleFont.
     */
    public Font getColumnTitleFont() {
        return columnTitleFont;
    }

    /**
     * @param columnTitleFont
     *            The columnTitleFont to set.
     */
    public void setColumnTitleFont(Font columnTitleFont) {
        this.columnTitleFont = columnTitleFont;
    }

    /**
     * @return Returns the columnValueFont.
     */
    public Font getColumnValueFont() {
        return columnValueFont;
    }

    /**
     * @param columnValueFont
     *            The columnValueFont to set.
     */
    public void setColumnValueFont(Font columnValueFont) {
        this.columnValueFont = columnValueFont;
    }

    /**
     * @return Returns the currentChapter.
     */
    public Chapter getCurrentChapter() {
        return currentChapter;
    }

    /**
     * @param currentChapter
     *            The currentChapter to set.
     */
    public void setCurrentChapter(Chapter currentChapter) {
        this.currentChapter = currentChapter;
    }

    /**
     * @return Returns the currentParagraph.
     */
    public Paragraph getCurrentParagraph() {
        return currentParagraph;
    }

    /**
     * @param currentParagraph
     *            The currentParagraph to set.
     */
    public void setCurrentParagraph(Paragraph currentParagraph) {
        this.currentParagraph = currentParagraph;
    }

    /**
     * @return Returns the currentSection.
     */
    public Section getCurrentSection() {
        return currentSection;
    }

    /**
     * @param currentSection
     *            The currentSection to set.
     */
    public void setCurrentSection(Section currentSection) {
        this.currentSection = currentSection;
    }

    /**
     * @return Returns the document.
     */
    public Document getDocument() {
        return document;
    }

    /**
     * @param document
     *            The document to set.
     */
    public void setDocument(Document document) {
        this.document = document;
    }

    /**
     * @return Returns the outStream.
     */
    public OutputStream getOutStream() {
        return outStream;
    }

    /**
     * @param outStream
     *            The outStream to set.
     */
    public void setOutStream(OutputStream outStream) {
        this.outStream = outStream;
    }

    /**
     * @return Returns the paragraphFont.
     */
    public Font getParagraphFont() {
        return paragraphFont;
    }

    /**
     * @param paragraphFont
     *            The paragraphFont to set.
     */
    public void setParagraphFont(Font paragraphFont) {
        this.paragraphFont = paragraphFont;
    }

    /**
     * @return Returns the pdfFileName.
     */
    public String getPdfFileName() {
        return pdfFileName;
    }

    /**
     * @param pdfFileName
     *            The pdfFileName to set.
     */
    public void setPdfFileName(String pdfFileName) {
        this.pdfFileName = pdfFileName;
    }

    /**
     * @return Returns the sectionFont.
     */
    public Font getSectionFont() {
        return sectionFont;
    }

    /**
     * @param sectionFont
     *            The sectionFont to set.
     */
    public void setSectionFont(Font sectionFont) {
        this.sectionFont = sectionFont;
    }

    /**
     * @return Returns the currentTable.
     */
    protected PdfPTable getCurrentTable() {
        return currentTable;
    }

    /**
     * @param currentTable
     *            The currentTable to set.
     */
    protected void setCurrentTable(PdfPTable currentTable) {
        this.currentTable = currentTable;
    }

    /**
     * Add section to the document
     * 
     * @param chapter
     */
    public void addDocument(Chapter chapter) {

        try {
            this.getDocument().add(chapter);
        } catch (DocumentException de) {
        	logger.error(de.getMessage());
        }
    }

    public void createHeader(String queryId) {

        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100); // percentage

            PdfPCell cell = new PdfPCell(new Paragraph("") );
            cell.setBackgroundColor(new Color(4,54,103));
            cell.setFixedHeight(4);
            cell.setBorderWidth(1);
            table.setSpacingBefore(4);
            //cell.setPaddingTop(4);
            //cell.setPaddingBottom(4);
            cell.setBorderColor(new Color(178, 210, 235));
            cell.setBorder(Rectangle.BOTTOM);
            table.addCell(cell);
            this.getCurrentChapter().add(table);

        Paragraph paragraphQueryId = new Paragraph(queryId, this
                .getHeaderFont());
        this.getCurrentChapter().add(paragraphQueryId);
    }

    public void createChapter(String title) {

        //We create a chapter for the ship status
        Paragraph p = new Paragraph(title, this.getChapterFont());
        Chapter chapter = new Chapter(p, 1);
        chapter.setNumberDepth(0);
        this.setCurrentChapter(chapter);
    }

    public void addCurrentChapterToDocument() {

        this.addDocument(this.getCurrentChapter());
    }

    public void createSectionToCurrentChapter(String sectionHeader) {

        Paragraph header = new Paragraph(sectionHeader, getSectionFont());
        Section section = this.getCurrentChapter().addSection(header, 1);
        section.setNumberDepth(0);
        this.setCurrentSection(section);
    }

    public void addTableToCurrentSection() {

        PdfPTable table = new PdfPTable(2);
        table.setSpacingAfter(10);
        table.setWidthPercentage(100); // percentage
        float[] columns = { 30f, 70f };

        try {
            table.setTotalWidth(columns);
        } catch (DocumentException e) {
            logger.error(e.getMessage());
        }

        PdfPCell cell = new PdfPCell(new Paragraph(""));
        cell.setBorder(Rectangle.BOTTOM);
        cell.setBorderColor(new Color(40, 87, 139));
        cell.setFixedHeight(3);
        cell.setColspan(2);
        table.addCell(cell);


        this.getCurrentSection().add(table);
        this.setCurrentTable(table);
    }

    
    public void addCurrentNestedTable(String header1, String header2, String header3) {
        
        if (header1 == null)
            header1 = "";
        if (header2 == null)
            header2 = "";
        if (header3 == null)
            header3 = "";           

        PdfPTable table = new PdfPTable(3);
        table.spacingBefore();
        table.setWidthPercentage(100); // percentage
        float[] columns = { 34f, 33f, 33f };

        try {
            table.setTotalWidth(columns);
        } catch (DocumentException e) {
            logger.error(e.getMessage());
        }

        PdfPCell nestTableCell = new PdfPCell(table);
        nestTableCell.setColspan(2);
        nestTableCell.setBorder(Rectangle.NO_BORDER);
        this.getCurrentTable().addCell(nestTableCell);
        
        Paragraph header1Paragraph = new Paragraph(header1,   getNestedTableHeaderFont());
        Paragraph header2Paragraph = new Paragraph(header2,   getNestedTableHeaderFont());
        Paragraph header3Paragraph = new Paragraph(header3,   getNestedTableHeaderFont());
        
        PdfPCell header1Cell = new PdfPCell(header1Paragraph);
        header1Cell.setBorder(Rectangle.NO_BORDER);
        
        table.addCell(header1Cell);

        PdfPCell header2Cell = new PdfPCell(header2Paragraph);
        header2Cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(header2Cell);

        PdfPCell header3Cell = new PdfPCell(header3Paragraph);
        header3Cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(header3Cell);
        this.setCurrentNestedTable(table);
    }
    
    public void addCurrentSectionRow(String columnTitle, String columnValue) {

        if (columnTitle == null)
            columnTitle = "";
        if (columnValue == null)
            columnValue = "";
        //if a nested table before then set it null
        if (this.getCurrentNestedTable() != null)
            this.setCurrentNestedTable(null);
        
        PdfPTable pdfTable = this.getCurrentTable();

        Paragraph titleParagraph = new Paragraph(columnTitle,
                getColumnTitleFont());
        PdfPCell titleCell = new PdfPCell(titleParagraph);
        titleCell.setBorder(Rectangle.NO_BORDER);
        pdfTable.addCell(titleCell);

        Paragraph valueParagraph = new Paragraph(columnValue,
                getColumnValueFont());
        PdfPCell valueCell = new PdfPCell(valueParagraph);
        valueCell.setBorder(Rectangle.NO_BORDER);
        pdfTable.addCell(valueCell);

    }

    public void addCurrentNestedSectionRow(String columnOne, String columnTwo, String columnThree) {

        if (columnOne == null)
            columnOne = "";
        if (columnTwo == null)
            columnTwo = "";
        if (columnThree == null)
            columnThree = "";        
        

        Paragraph paragraphOne = new Paragraph(columnOne,
                getColumnValueFont());
        PdfPCell cellOne = new PdfPCell(paragraphOne);
        cellOne.setBorder(Rectangle.NO_BORDER);
        this.getCurrentNestedTable().addCell(cellOne);

        Paragraph paragraphTwo = new Paragraph(columnTwo,
                getColumnValueFont());
        PdfPCell cellTwo = new PdfPCell(paragraphTwo);
        cellTwo.setBorder(Rectangle.NO_BORDER);
        this.getCurrentNestedTable().addCell(cellTwo);

        Paragraph paragraphThree = new Paragraph(columnThree,
                getColumnValueFont());
        PdfPCell cellThree = new PdfPCell(paragraphThree);
        cellThree.setBorder(Rectangle.NO_BORDER);
        this.getCurrentNestedTable().addCell(cellThree);
    }    
    /**
     * @return Returns the headerFont.
     */
    protected Font getHeaderFont() {
        return headerFont;
    }

    /**
     * @param headerFont
     *            The headerFont to set.
     */
    protected void setHeaderFont(Font headerFont) {
        this.headerFont = headerFont;
    }
    /**
     * @return Returns the currentNestedTable.
     */
    protected PdfPTable getCurrentNestedTable() {
        return currentNestedTable;
    }
    /**
     * @param currentNestedTable The currentNestedTable to set.
     */
    protected void setCurrentNestedTable(PdfPTable currentNestedTable) {
        this.currentNestedTable = currentNestedTable;
    }
    /**
     * @return Returns the nestedTableHeaderFont.
     */
    protected Font getNestedTableHeaderFont() {
        return nestedTableHeaderFont;
    }
    /**
     * @param nestedTableHeaderFont The nestedTableHeaderFont to set.
     */
    protected void setNestedTableHeaderFont(Font nestedTableHeaderFont) {
        this.nestedTableHeaderFont = nestedTableHeaderFont;
    }
    /**
     * @return Returns the imagesPath.
     */
    protected String getImagesPath() {
        return imagesPath;
    }
    /**
     * @param imagesPath The imagesPath to set.
     */
    protected void setImagesPath(String imagesPath) {
        this.imagesPath = imagesPath;
    }
    /**
     * @return Returns the footer.
     */
    public HeaderFooter getFooter() {
        return footer;
    }
    /**
     * @param queryNoPhrase The footer to set.
     */
    public void setFooter(String queryNoPhrase) {

        String today =  new SimpleDateFormat( "dd-MMM-yyyy", Locale.UK ).format( new Date() ).toUpperCase();
        
        Phrase footerParagraph = new Phrase("Query Tracking Tool - "+ queryNoPhrase +"                             Page ", getFooterFont());
        Phrase footerafter = new Phrase("                                                                      "+ today, getFooterFont());
        HeaderFooter footer = new HeaderFooter(footerParagraph, footerafter);
        footer.setBorder(Rectangle.TOP);
        this.footer = footer;
    }
    
}