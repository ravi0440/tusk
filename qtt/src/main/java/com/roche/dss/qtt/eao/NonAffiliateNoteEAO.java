package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.NonAffiliateNote;

/**
 * User: pruchnil
 */
public interface NonAffiliateNoteEAO extends QueryRelatedEAO<NonAffiliateNote> {
}
