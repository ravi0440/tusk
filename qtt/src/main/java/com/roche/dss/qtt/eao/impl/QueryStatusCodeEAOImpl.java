package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.QueryStatusCodeEAO;
import com.roche.dss.qtt.model.QueryStatusCode;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
@Repository("queryStatusCode")
public class QueryStatusCodeEAOImpl extends AbstractBaseEAO<QueryStatusCode>  implements QueryStatusCodeEAO  {

    //TODO simple find() from AbstractBaseEAO do the same because qsc.statusCode is PK so to refactor
    @Override
    public QueryStatusCode findStatus(String status) {
        String sql = "select qsc from QueryStatusCode qsc where qsc.statusCode= :status";
        return (QueryStatusCode) getEntityManager().createQuery(sql).setParameter("status", status).getSingleResult();
    }
}
