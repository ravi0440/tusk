package com.roche.dss.qtt.model;
// Generated 2010-10-04 12:00:38 by Hibernate Tools 3.3.0.GA

import com.roche.dss.qtt.query.comms.dto.PreparationDTO;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.util.ApplicationUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * FuQuery generated by hbm2java
 */
@Entity
@Table(name = "FU_QUERIES")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FuQuery implements java.io.Serializable {
    private static final Logger log = LoggerFactory.getLogger(FuQuery.class);
    private static final long serialVersionUID = -8646277382658048927L;

    private FuQueryId id;
    private String queryNumber;
    private long requesterId;
    private QttCountry country;
    private QueryType queryType;
    private ReporterType reporterType;
    private String statusCode;
    private DateTime currentDueDate;
    private DateTime originalDueDate;
    private char allDrugsFlag;
    private char alertFlag;
    private char urgencyFlag;
    private String urgencyReason;
    private String workflowRouteType;
    private DateTime dateRequested;
    private DateTime dmgDueDate;
    private String followUpNumber;
    private DateTime expiryDate;
    private String querySummary;
    private String querySummaryUpper;
    private String preQueryPrepComment;
    private Long caseOformatId;
    private String caseComment;
    private String oformatSpecialRequest;
    private String mnftBatchNumber;
    private String indNumber;
    private Character psstState;
    private Character closeErrorStatus;
    private DateTime datetimeSubmitted;
    private String accessKey;
    private String queryLabel;
    private String requesterQueryLabel;
    private char followupFlag;
    private Short followupId;
    private Character affiliateAccess;
    private String followupNumber;
    private Query query;
    /*
    * virtual relations - don't exist in db
    */
//    Transient
    private FuRequester requester;
    private FuRetrievalPeriod retrievalPeriod;
    private FuQueryDescription queryDescription;
    private FuDrug drugForPreview;
    private List<FuCase> cases = new ArrayList<FuCase>();
    private List<FuPreQueryPreparation> preQueryPreparations = new ArrayList<FuPreQueryPreparation>();
    private PreparationDTO preparation;

    public FuQuery() {
    }

    public FuQuery(Query query, long followupSeq, String followupNumber) {
        this.id = new FuQueryId(followupSeq, query.getQuerySeq());
        this.queryNumber = query.getQueryNumber();
        this.requesterId = query.getRequester().getRequesterId();
        this.country = query.getCountry();
        this.queryType = query.getQueryType();
        this.reporterType = query.getReporterType();
        this.statusCode = query.getQueryStatusCode().getStatusCode();
        this.currentDueDate = query.getCurrentDueDate();
        this.originalDueDate = query.getOriginalDueDate();
        this.allDrugsFlag = query.getAllDrugsFlag();
        this.alertFlag = query.getAlertFlag();
        this.urgencyFlag = query.getUrgencyFlag();
        this.urgencyReason = query.getUrgencyReason();
        this.workflowRouteType = query.getWorkflowRouteType();
        this.dateRequested = query.getDateRequested();
        this.dmgDueDate = query.getDmgDueDate();
        this.followUpNumber = query.getFollowUpNumber();
        this.expiryDate = query.getExpiryDate();
        this.querySummary = query.getQuerySummary();
        this.querySummaryUpper = query.getQuerySummaryUpper();
        this.preQueryPrepComment = query.getPreQueryPrepComment();
        this.caseOformatId = query.getCaseOformat() != null ? query.getCaseOformat().getCaseOformatId() : null;
        this.caseComment = query.getCaseComment();
        this.oformatSpecialRequest = query.getOformatSpecialRequest();
        this.mnftBatchNumber = query.getMnftBatchNumber();
        this.indNumber = query.getIndNumber();
        this.psstState = query.getPsstState();
        this.closeErrorStatus = query.getCloseErrorStatus();
        this.datetimeSubmitted = query.getDatetimeSubmitted();
        this.accessKey = query.getAccessKey();
        this.queryLabel = query.getQueryLabel();
        this.requesterQueryLabel = query.getRequesterQueryLabel();
        this.followupFlag = query.getFollowupFlag();
        this.followupId = query.getFollowupId();
        this.affiliateAccess = query.getAffiliateAccess();
        this.followupNumber = followupNumber;
    }

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "followupSeq", column = @Column(name = "FOLLOWUP_SEQ", nullable = false, precision = 10, scale = 0)),
            @AttributeOverride(name = "querySeq", column = @Column(name = "QUERY_SEQ", nullable = false, precision = 10, scale = 0))})
    public FuQueryId getId() {
        return this.id;
    }

    public void setId(FuQueryId id) {
        this.id = id;
    }

    @Transient
    public FuRequester getRequester() {
        return requester;
    }

    public void setRequester(FuRequester requester) {
        this.requester = requester;
    }

    @Column(name = "QUERY_NUMBER", length = 20)
    public String getQueryNumber() {
        return this.queryNumber;
    }

    public void setQueryNumber(String queryNumber) {
        this.queryNumber = queryNumber;
    }

    @Column(name = "REQUESTER_ID", nullable = false, precision = 10, scale = 0)
    public long getRequesterId() {
        return this.requesterId;
    }

    public void setRequesterId(long requesterId) {
        this.requesterId = requesterId;
    }

    @Column(name = "STATUS_CODE", nullable = false, length = 4)
    public String getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @Type(type = "com.roche.dss.util.PersistentDateTime")
    @Column(name = "CURRENT_DUE_DATE", length = 7)
    public DateTime getCurrentDueDate() {
        return this.currentDueDate;
    }

    public void setCurrentDueDate(DateTime currentDueDate) {
        this.currentDueDate = currentDueDate;
    }

    @Type(type = "com.roche.dss.util.PersistentDateTime")
    @Column(name = "ORIGINAL_DUE_DATE", length = 7)
    public DateTime getOriginalDueDate() {
        return this.originalDueDate;
    }

    public void setOriginalDueDate(DateTime originalDueDate) {
        this.originalDueDate = originalDueDate;
    }

    @Column(name = "ALL_DRUGS_FLAG", nullable = false, length = 1)
    public char getAllDrugsFlag() {
        return this.allDrugsFlag;
    }

    public void setAllDrugsFlag(char allDrugsFlag) {
        this.allDrugsFlag = allDrugsFlag;
    }

    @Column(name = "ALERT_FLAG", nullable = false, length = 1)
    public char getAlertFlag() {
        return this.alertFlag;
    }

    public void setAlertFlag(char alertFlag) {
        this.alertFlag = alertFlag;
    }

    @Column(name = "URGENCY_FLAG", nullable = false, length = 1)
    public char getUrgencyFlag() {
        return this.urgencyFlag;
    }

    public void setUrgencyFlag(char urgencyFlag) {
        this.urgencyFlag = urgencyFlag;
    }

    @Column(name = "URGENCY_REASON", length = 300)
    public String getUrgencyReason() {
        return this.urgencyReason;
    }

    public void setUrgencyReason(String urgencyReason) {
        this.urgencyReason = urgencyReason;
    }

    @Column(name = "WORKFLOW_ROUTE_TYPE", length = 2)
    public String getWorkflowRouteType() {
        return this.workflowRouteType;
    }

    public void setWorkflowRouteType(String workflowRouteType) {
        this.workflowRouteType = workflowRouteType;
    }

    @Type(type = "com.roche.dss.util.PersistentDateTime")
    @Column(name = "DATE_REQUESTED", length = 7)
    public DateTime getDateRequested() {
        return this.dateRequested;
    }

    public void setDateRequested(DateTime dateRequested) {
        this.dateRequested = dateRequested;
    }

    @Type(type = "com.roche.dss.util.PersistentDateTime")
    @Column(name = "DMG_DUE_DATE", length = 7)
    public DateTime getDmgDueDate() {
        return this.dmgDueDate;
    }

    public void setDmgDueDate(DateTime dmgDueDate) {
        this.dmgDueDate = dmgDueDate;
    }

    @Column(name = "FOLLOW_UP_NUMBER", length = 75)
    public String getFollowUpNumber() {
        return this.followUpNumber;
    }

    public void setFollowUpNumber(String followUpNumber) {
        this.followUpNumber = followUpNumber;
    }

    @Type(type = "com.roche.dss.util.PersistentDateTime")
    @Column(name = "EXPIRY_DATE", length = 7)
    public DateTime getExpiryDate() {
        return this.expiryDate;
    }

    public void setExpiryDate(DateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Column(name = "QUERY_SUMMARY", length = 1300)
    public String getQuerySummary() {
        return this.querySummary;
    }

    public void setQuerySummary(String querySummary) {
        this.querySummary = querySummary;
    }

    @Column(name = "QUERY_SUMMARY_UPPER", length = 1300)
    public String getQuerySummaryUpper() {
        return this.querySummaryUpper;
    }

    public void setQuerySummaryUpper(String querySummaryUpper) {
        this.querySummaryUpper = querySummaryUpper;
    }

    @Column(name = "PRE_QUERY_PREP_COMMENT", length = 200)
    public String getPreQueryPrepComment() {
        return this.preQueryPrepComment;
    }

    public void setPreQueryPrepComment(String preQueryPrepComment) {
        this.preQueryPrepComment = preQueryPrepComment;
    }

    @Column(name = "CASE_OFORMAT_ID", precision = 10, scale = 0)
    public Long getCaseOformatId() {
        return this.caseOformatId;
    }

    public void setCaseOformatId(Long caseOformatId) {
        this.caseOformatId = caseOformatId;
    }

    @Column(name = "CASE_COMMENT", length = 600)
    public String getCaseComment() {
        return this.caseComment;
    }

    public void setCaseComment(String caseComment) {
        this.caseComment = caseComment;
    }

    @Column(name = "OFORMAT_SPECIAL_REQUEST", length = 300)
    public String getOformatSpecialRequest() {
        return this.oformatSpecialRequest;
    }

    public void setOformatSpecialRequest(String oformatSpecialRequest) {
        this.oformatSpecialRequest = oformatSpecialRequest;
    }

    @Column(name = "MNFT_BATCH_NUMBER", length = 70)
    public String getMnftBatchNumber() {
        return this.mnftBatchNumber;
    }

    public void setMnftBatchNumber(String mnftBatchNumber) {
        this.mnftBatchNumber = mnftBatchNumber;
    }

    @Column(name = "IND_NUMBER", length = 50)
    public String getIndNumber() {
        return this.indNumber;
    }

    public void setIndNumber(String indNumber) {
        this.indNumber = indNumber;
    }

    @Column(name = "PSST_STATE", length = 1)
    public Character getPsstState() {
        return this.psstState;
    }

    public void setPsstState(Character psstState) {
        this.psstState = psstState;
    }

    @Column(name = "CLOSE_ERROR_STATUS", length = 1)
    public Character getCloseErrorStatus() {
        return this.closeErrorStatus;
    }

    public void setCloseErrorStatus(Character closeErrorStatus) {
        this.closeErrorStatus = closeErrorStatus;
    }

    @Type(type = "com.roche.dss.util.PersistentDateTime")
    @Column(name = "DATETIME_SUBMITTED", length = 7)
    public DateTime getDatetimeSubmitted() {
        return this.datetimeSubmitted;
    }

    public void setDatetimeSubmitted(DateTime datetimeSubmitted) {
        this.datetimeSubmitted = datetimeSubmitted;
    }

    @Column(name = "ACCESS_KEY", length = 32)
    public String getAccessKey() {
        return this.accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    @Column(name = "QUERY_LABEL", length = 300)
    public String getQueryLabel() {
        return this.queryLabel;
    }

    public void setQueryLabel(String queryLabel) {
        this.queryLabel = queryLabel;
    }

    @Column(name = "REQUESTER_QUERY_LABEL", length = 250)
    public String getRequesterQueryLabel() {
        return this.requesterQueryLabel;
    }

    public void setRequesterQueryLabel(String requesterQueryLabel) {
        this.requesterQueryLabel = requesterQueryLabel;
    }

    @Column(name = "FOLLOWUP_FLAG", nullable = false, length = 1)
    public char getFollowupFlag() {
        return this.followupFlag;
    }

    public void setFollowupFlag(char followupFlag) {
        this.followupFlag = followupFlag;
    }

    @Column(name = "FOLLOWUP_ID", precision = 4, scale = 0)
    public Short getFollowupId() {
        return this.followupId;
    }

    public void setFollowupId(Short followupId) {
        this.followupId = followupId;
    }

    @Column(name = "AFFILIATE_ACCESS", length = 1)
    public Character getAffiliateAccess() {
        return this.affiliateAccess;
    }

    public void setAffiliateAccess(Character affiliateAccess) {
        this.affiliateAccess = affiliateAccess;
    }

    @Column(name = "FOLLOWUP_NUMBER", length = 20)
    public String getFollowupNumber() {
        return this.followupNumber;
    }

    public void setFollowupNumber(String followupNumber) {
        this.followupNumber = followupNumber;
    }

    /**
     * This method checks if this query is related to all drugs.
     * ALL_DRUGS_FLAG is converted from char to boolean.
     *
     * @return
     */
    @Transient
    public boolean isAllDrugs() {
        return ApplicationUtils.convertToBoolean(String.valueOf(allDrugsFlag));
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUERY_SEQ", nullable = false, insertable = false, updatable = false)
    public Query getQuery() {
        return this.query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOURCE_COUNTRY_CODE", nullable = false)
    public QttCountry getCountry() {
        return country;
    }

    public void setCountry(QttCountry country) {
        this.country = country;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REPORTER_TYPE_ID", nullable = false)
    public ReporterType getReporterType() {
        return this.reporterType;
    }

    public void setReporterType(ReporterType reporterType) {
        this.reporterType = reporterType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUERY_TYPE_CODE", nullable = false)
    public QueryType getQueryType() {
        return this.queryType;
    }

    public void setQueryType(QueryType queryType) {
        this.queryType = queryType;
    }

    @Transient
    public FuRetrievalPeriod getRetrievalPeriod() {
        return retrievalPeriod;
    }

    public void setRetrievalPeriod(FuRetrievalPeriod retrievalPeriod) {
        this.retrievalPeriod = retrievalPeriod;
    }

    @Transient
    public FuQueryDescription getQueryDescription() {
        return queryDescription;
    }

    public void setQueryDescription(FuQueryDescription queryDescription) {
        this.queryDescription = queryDescription;
    }

    @Transient
    public FuDrug getDrugForPreview() {
        return drugForPreview;
    }

    public void setDrugForPreview(FuDrug drugForPreview) {
        this.drugForPreview = drugForPreview;
    }

    @Transient
    public List<FuCase> getCases() {
        return cases;
    }

    public void setCases(List<FuCase> cases) {
        this.cases = cases;
    }

    @Transient
    public List<FuPreQueryPreparation> getPreQueryPreparations() {
        return preQueryPreparations;
    }

    public void setPreQueryPreparations(List<FuPreQueryPreparation> preQueryPreparations) {
        this.preQueryPreparations = preQueryPreparations;
    }

    @Transient
    public boolean isDrugEvent() {
        return QueryType.Code.DRUG_EVENT == queryType.getQueryTypeCode();
    }

    @Transient
    public boolean isDrugInteraction() {
        return QueryType.Code.DRUG_INTERACTION == queryType.getQueryTypeCode();
    }

    @Transient
    public boolean isManufacturingType() {
        return QueryType.Code.MANUFACTURING == queryType.getQueryTypeCode();
    }

    @Transient
    public boolean isPregnancyEvent() {
        return QueryType.Code.PREGNANCY == queryType.getQueryTypeCode();
    }

    @Transient
    public boolean isCaseDataRequest() {
        return QueryType.Code.CASE_DATA_REQUEST == queryType.getQueryTypeCode();
    }

    @Transient
    public boolean isExternalAudit() {
        return QueryType.Code.EXTERNAL_AUDIT == queryType.getQueryTypeCode();
    }

    @Transient
    public boolean isCase() {
        return QueryType.Code.CASE_CLARIFICATION == queryType.getQueryTypeCode() 
        		|| QueryType.Code.CASE_DATA_REQUEST == queryType.getQueryTypeCode();
    }

    @Transient
    public boolean isSaeReconcilation() {
        return QueryType.Code.SAE_RECONCILIATION == queryType.getQueryTypeCode();
    }

    @Transient
    public boolean isIndUpdates() {
        return QueryType.Code.IND_UPDATES == queryType.getQueryTypeCode();
    }

    @Transient
    public boolean isPerfMetrics() {
        return QueryType.Code.PERFORMANCE_METRICS == queryType.getQueryTypeCode();
    }

    @Transient
    public boolean isSignalDetection() {
        return QueryType.Code.SIGNAL_DETECTION == queryType.getQueryTypeCode();
    }

    @Transient
    public boolean isLiterSearch() {
        return QueryType.Code.LITERATURE_SEARCH == queryType.getQueryTypeCode();
    }

    @Transient
    public PreparationDTO getPreparation() {
        return preparation;
    }

    public void setPreparation(PreparationDTO preparation) {
        this.preparation = preparation;
    }
}
