package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.NonRocheUser;
import com.roche.dss.qtt.security.ldap.authentication.LdapAuthenticator;
import com.roche.dss.qtt.security.ldap.authentication.LdapUser;

/**
 * User: pruchnil
 */
public class OtherLoginAction extends CommonActionSupport {
    private static final Logger logger = LoggerFactory.getLogger(OtherLoginAction.class);
    private String login;
    private String password;
    private String domain;

    @Autowired
    LdapAuthenticator authenticator;

    @Override
    @SkipValidation
    public String execute() throws Exception {
        return SUCCESS;
    }

    public String login() throws Exception {
        String email = null;
        String givenName = null;
        String surname = null;
        if (Domain.NONDOMAIN.getKey().equals(domain)) {
            NonRocheUser user = securityService.checkNonRocheUser(login, password);
            if (user != null) {
                email = user.getEmail();
            }
        } else {
        	LdapUser user = authenticator.authenticateLdap(login, password, domain);
        	if (user != null) {
        		email = user.getMail();
        		givenName = user.getGivenName();
        		surname = user.getSurname();
        	}
        }
        if (StringUtils.isNotEmpty(email)) {
            ActionContext.getContext().getSession().put("email", email);
            ActionContext.getContext().getSession().put("givenName", givenName);
            ActionContext.getContext().getSession().put("surname", surname);            
        } else {
            addActionError(getText("login.invalid"));
            return INPUT;
        }
        return SUCCESS;
    }

    public Domain[] getDomainList() {
        return Domain.values();
    }

    public String getLogin() {
        return login;
    }

    @RequiredStringValidator(type = ValidatorType.FIELD, key = "login.required")
    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    @RequiredStringValidator(type = ValidatorType.FIELD, key = "password.required")
    public void setPassword(String password) {
        this.password = password;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public enum Domain {
        EMEA("emea", "EMEA"), ASIA("asia", "ASIA"), NALA("nala", "NALA"), EXBP("exbp", "EXBP"), NONDOMAIN("nonDomainUser", "NON DOMAIN");
        private String key;
        private String value;

        Domain(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }

    }
}
