package com.roche.dss.qtt.service.workflow;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;
import com.roche.dss.qtt.security.user.RocheUserDetails;
import java.util.List;

/**
 * @author zerkowsm
 *
 */
public interface WorkflowService extends WorkflowGlobalConstants {
	
	void initiateWorkflow(Query query, String userName);
	void selectWorkflowRoute(long queryId, String transitionName, String userName, String queryLabel);
	void lockTask(String userName, long taskId);
	void completeActivity(String userName, long currentTaskId, String nextActivity);
	void performMIResourceAssignment(Query query, String userName, long taskId, String nextUserName, String nextUserNameEmail);
	void performDMGResourceAssignment(Query query, String userName, long taskId, String nextUserName, DateTime dmgDueDate, boolean affiliate); 
    void performResourceReassignment(long taskId, String nextUserName);
	void updateSummaryResponseAndComplete(boolean isUpdate, String leavingTransition, String userName, long taskId, long queryId, DateTime expiryDate, String summary, boolean isAffiliateVisibility, boolean ldocUpdate, String oldLabellingDocId, String newLabellingDocId);
	void sendFinalResponse(String userName, Query query);
	void createFollowup(long queryId, String userName, long taskId);
    long restartProcess(Long processInstanceId, String userName);
    TaskInstance getTaskInstance(long taskInstanceId);
    List<QuerySummaryDTO> retrieveSummaryList(boolean affiliate, RocheUserDetails loggedUser, boolean dscl, boolean ps, boolean dmg, boolean pds, boolean dmgsc);
	List<QuerySummaryDTO> retrieveUserTaskList(RocheUserDetails user, boolean styleCheck);
	List<QuerySummaryDTO> retrieveUserTaskList(String userName);
	List<TaskInstance> retrieveProcessTaskList(String querySeq);
    void deleteQueryAndProcess(final Long querySeq);
    public void deleteQueryProcess(final Long querySeq);
	void endQueryProcess(final Long querySeq);
}