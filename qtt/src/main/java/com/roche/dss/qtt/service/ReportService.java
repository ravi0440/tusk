package com.roche.dss.qtt.service;

import com.roche.dss.qtt.query.comms.dto.ReportDTO;
import com.roche.dss.qtt.query.web.model.ReportCriteria;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * This service passes on report data to action (view layer)
 * User: pruchnil
 */
public interface ReportService {
    List<ReportDTO> retrieveReportData(ReportCriteria reportCriteria);

    ByteArrayOutputStream exportToXLS(ReportCriteria reportCriteria) throws IOException, InvalidFormatException;

    ByteArrayOutputStream exportToCSV(ReportCriteria reportCriteria) throws IOException, InvalidFormatException;
}
