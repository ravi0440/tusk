package com.roche.dss.qtt.security.utils.storeprocedures.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.roche.dss.qtt.security.model.DSSUser;

/**
 * @author zerkowsm
 *
 */
public class DSSUserMapper implements RowMapper<DSSUser>{

	@Override
	public DSSUser mapRow(ResultSet rs, int rowNum) throws SQLException {
		DSSUser user = new DSSUser();
		user.setEmployeeId(rs.getLong("EMPLOYEE_ID"));

		user.setTitle(rs.getString("TITLE"));
		user.setUserName(rs.getString("USERNAME"));
		user.setFirstName(rs.getString("FIRST_NAME"));
		user.setLastName(rs.getString("LAST_NAME"));
		user.setUserFullName(rs.getString("FIRST_NAME")+" "+rs.getString("LAST_NAME"));
		user.setEmail(rs.getString("EMAIL_ADDRESS"));
		user.setMailStop(rs.getString("MAILSTOP"));
		user.setPhoneNumber(rs.getString("PHONE_NUM"));
		user.setFax(rs.getString("FAX_NUM"));
		
		user.setCompanyCode(rs.getString("COMPANY_CODE"));
		user.setCompany(rs.getString("COMPANY_DESCRIPTION"));
							
		user.setSiteCode(rs.getString("SITE_CODE"));
		user.setSiteTypeCode(rs.getString("SITE_TYPE_CODE"));
		user.setSite(rs.getString("SITE_DESCRIPTION"));
							
		user.setBusinessUnitCode(rs.getString("BUSINESS_UNIT_CODE"));
		user.setBusinessUnit(rs.getString("BUSINESS_UNIT_DESCRIPTION"));
							
		user.setCountryCode(rs.getString("COUNTRY_CODE"));
		user.setCountry(rs.getString("COUNTRY_DESCRIPTION"));
							
		user.setStatus(getUserStatus(rs.getString("ACTIVE_FLAG"),rs.getString("ACCESS_REVOKED_FLAG")));
		
        return user;
	}
	
	private String getUserStatus(String activeFlag, String accessRevokedFlag){		
		String status = null;
		
		if (activeFlag == null && accessRevokedFlag == null)
			status = "Inactive";
		else if ((activeFlag != null && activeFlag .equals ("Y")) && accessRevokedFlag == null)
			status = "Active";
		else if ((activeFlag != null && activeFlag .equals ("Y")) && (accessRevokedFlag != null && accessRevokedFlag .equals ("Y")))
			status = "Suspended";
			
		return status;
	}
	
}