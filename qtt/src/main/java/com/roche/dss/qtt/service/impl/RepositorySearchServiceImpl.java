package com.roche.dss.qtt.service.impl;

import com.roche.dss.qtt.query.comms.dao.RepositorySearchDAO;
import com.roche.dss.qtt.query.web.model.RepositorySearchParams;
import com.roche.dss.qtt.service.SearchResults;

@Service("repositorySearchService")
public class RepositorySearchServiceImpl implements
		com.roche.dss.qtt.service.RepositorySearchService {

	@Autowired
	protected RepositorySearchDAO dao;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public SearchResults search(RepositorySearchParams searchParams) {
		return dao.search(searchParams);
	}

}
