/* 
====================================================================
  $Header$
  @author $Author: rahmanh1 $
  @version $Revision: 628 $ $Date: 2004-04-16 14:24:45 +0200 (Pt, 16 kwi 2004) $

====================================================================
*/
package com.roche.dss.qtt.utility.cache;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * A static object that manages the data cache.  Any operations on the cache should happen through this class.
 * 
 * @author  Manik Surtani
 */
public class CacheManager
{

    private static final Logger logger = LoggerFactory.getLogger(CacheManager.class);

    private static final String CLASS_NAME = "CacheManager";


    private Map cacheHashMap = Collections.synchronizedMap(new HashMap());

    /**
     *Constructor
     */
    private CacheManager() { }
    
    private static class SingletonHolder { 
        public static final CacheManager instance = new CacheManager();
    }

    /**
     * Get the ServiceLocator instance
     * @return ServiceLocator Object
     *
     * @since 1.0
     */
    private static CacheManager getInstance()
    {
    	return SingletonHolder.instance;
    }

    /**
     * Tests whether an element with the given key exists in the cache.
     * @param key
     * @return true if the object is found in the cache, false otherwise. Also returns false if the object is found but has expired.
     */
    public static boolean contains(Object key)
    {
        boolean contains = getInstance().containsKey(key);
        if (logger.isDebugEnabled())
        {
            logger.debug("Testing if cache contains key [" + key + "] --> " + contains);
        }
        return contains;
    }

    /**
     * Puts a Cacheable object in the cache.  The object will be identified 
     * by what's returned by it's ((Cacheable) object).getIdentifier() method.
     * @param object
     */
    public static void putCache(Cacheable object)
    {
        logger.debug("Putting object in cache.");
        getInstance().putInCache(object);
        if (logger.isDebugEnabled())
        {
            logger.debug("Successful?  Lets test with a CONTAINS: " + contains(object.getIdentifier()));
        }
    }

    /**
     * Retrieves an object from the cache.  If the object is not found or has timed 
     * out, this method returns a null. 
     * @param identifier
     * @return
     */
    public static Cacheable getCache(Object identifier)
    {
        logger.debug("Trying to get object from cache.");
        Cacheable c = getInstance().getFromCache(identifier);
        if (logger.isDebugEnabled())
        {
            logger.debug("Retrieved [" + c + "] for key [" + identifier + "]");
        }
        return c;
    }

    /**
     * Removes an object from the cache.
     * @param key
     */
    public static void removeFromCache(String key)
    {
        getInstance().removeFromTheCache(key);
    }

    /**
     * Debug method, to return the contents of the cache as a String.
     *
     */
    public static String dump()
    {
        return "CACHE CONTENTS: " + getInstance().getMap();
    }

    // ----------------------------------------------------- 
    // Internal helper methods
    // -----------------------------------------------------

    protected boolean containsKey(Object key)
    {
        if (cacheHashMap.containsKey(key))
        {
            Cacheable object = (Cacheable) cacheHashMap.get(key);

            if (object.isExpired())
            {
                cacheHashMap.remove(key);
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }

    protected void putInCache(Cacheable object)
    {
        // Remember if the HashMap previously contains a mapping for the key, the old value
        // will be replaced.  This is valid functioning.
        cacheHashMap.put(object.getIdentifier(), object);
    } //end put

    protected Cacheable getFromCache(Object identifier)
    {
        Cacheable object = (Cacheable) cacheHashMap.get(identifier);

        if (object == null)
        {
            return null;
        }

        if (object.isExpired())
        {
            cacheHashMap.remove(identifier);
            return null;
        }
        else
        {
            return object;
        }
    } //end getCache

    protected void removeFromTheCache(String key)
    {
        cacheHashMap.remove(key);
    }

    protected Map getMap()
    {
        return cacheHashMap;
    }

}
