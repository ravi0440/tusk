package com.roche.dss.qtt.service.impl;

import com.roche.dss.qtt.eao.AttachmentEAO;
import com.roche.dss.qtt.eao.AttachmentFromDmgSearchEAO;
import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.AttachmentCategory;
import com.roche.dss.qtt.model.AttachmentFromDmgSearch;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.web.model.FileUploadBean;
import com.roche.dss.qtt.service.AttachmentService;
import com.roche.dss.qtt.service.FileOperationsService;
import com.roche.dss.qtt.service.QueryNoteService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.util.ApplicationUtils;
import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

import static com.roche.dss.qtt.service.impl.QueryNoteServiceImpl.NoteType;

@Service("attachmentService")
@Transactional
public class AttachmentServiceImpl implements AttachmentService {
    private AttachmentEAO attachmentEAO;
    private FileOperationsService fileOpsService;
    private QueryNoteService noteService;
    private AttachmentFromDmgSearchEAO attachmentFromDmgSearchEAO;
    
    @Autowired
    private QueryService queryService;
    
    @Resource
    public void setFileOperationsService(FileOperationsService fileOpsService) {
        this.fileOpsService = fileOpsService;
    }

    @Resource
    public void setAttachmentEAO(AttachmentEAO attachmentEAO) {
        this.attachmentEAO = attachmentEAO;
    }

    @Resource
    public void setNoteService(QueryNoteService noteService) {
        this.noteService = noteService;
    }

    @Resource
    public void setAttachmentFromDmgSearchEAO(AttachmentFromDmgSearchEAO attachmentFromDmgSearchEAO) {
        this.attachmentFromDmgSearchEAO = attachmentFromDmgSearchEAO;
    }

    @Override
    public void persist(Attachment entity) {
        attachmentEAO.persist(entity);
    }

    @Override
    public Attachment merge(Attachment entity) {
        return attachmentEAO.merge(entity);
    }

    @Override
    public void remove(Attachment entity) {
        attachmentEAO.remove(entity);
    }

    @Override
    public void flush() {
        attachmentEAO.flush();
    }

    @Override
    public Attachment find(Long id) {
        return attachmentEAO.find(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveAttachmentFromDmg(Query query, Attachment addedAttachment) {
        Set<TaskInstance> taskInstances = query.getProcessInstance().getTaskInstances();
        Long currentTaskId = taskInstances.iterator().next().getId();

        if(currentTaskId != null) {
            AttachmentFromDmgSearch attachmentFromDmgSearch = new AttachmentFromDmgSearch();
            attachmentFromDmgSearch.setAttachmentSeq(addedAttachment.getAttachmentSeq());
            attachmentFromDmgSearch.setQuerySeq(query.getQuerySeq());
            attachmentFromDmgSearch.setTaskInstanceId(currentTaskId);
            attachmentFromDmgSearchEAO.persist(attachmentFromDmgSearch);
        }
    }

    @Override
    public List<AttachmentFromDmgSearch> getAllAttachmentsFromDmgSearchbyTaskInstanceId(long taskInstanceId) {
        return attachmentFromDmgSearchEAO.getAttachmentsForDMGSearch(taskInstanceId);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void changeActivationFlag(long id, long queryId, boolean b) {
        Attachment attachment = find(id);
        attachment.setActivationFlag(ApplicationUtils.booleanToChar(b));
        merge(attachment);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Attachment getFileFromRepo(long attachId, long queryId) throws Exception {
        Attachment attachment = find(attachId);
        InputStream inputStream = fileOpsService.getInputStream(Integer.valueOf(String.valueOf(queryId)), attachment.getFilename());
        attachment.setFileStream(inputStream);
        return attachment;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Attachment addFileToRepo(long queryId, FileUploadBean fileUpload, String userName, boolean affiliate) throws IOException {
        Query query = queryService.find(queryId);
        Attachment attachment = new Attachment();
        attachment.setCreateTs(new DateTime());
        attachment.setFilename(fileUpload.getUploadFileName());
        attachment.setActivationFlag('Y');
        attachment.setMedicalOpinionFlag('N');
        attachment.setAuthor(userName);
        attachment.setVersion(Short.valueOf("1"));
        attachment.setTitle(fileUpload.getGivenFileName());
        attachment.setAttachmentCategory(new AttachmentCategory(fileUpload.getType()));
        attachment.setQuerySeq(queryId);
        attachment.setFollowupSeq(query.getFollowupSeq());

//        attachment.setFollowupNumber(attachmentEAO.getMaxFollowupNumber(queryId));
        if(StringUtils.isEmpty(query.getFollowupNumber())){
            attachment.setFollowupNumber("FU0000");
        } else {
            attachment.setFollowupNumber(query.getFollowupNumber());
        }

        persist(attachment);

        query.getAttachments().add(attachment);
        queryService.merge(query);
        
        fileOpsService.insertFileIntoRepository(String.valueOf(queryId), fileUpload.getUpload(), new StringBuffer(fileUpload.getUploadFileName()));
        noteService.saveQueryNote(0, queryId, NoteType.NEWATTACH.getMessage(), affiliate, userName);

        return attachment;
    }
}
