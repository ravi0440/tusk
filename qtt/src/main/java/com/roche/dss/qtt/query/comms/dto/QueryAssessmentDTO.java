/* 
====================================================================
  $Header$
  @author $Author: savovp $
  @version $Revision: 2654 $ $Date: 2013-01-31 17:15:50 +0100 (Cz, 31 sty 2013) $

====================================================================
*/
package com.roche.dss.qtt.query.comms.dto;


import com.roche.dss.qtt.model.QueryAssessment;

public class QueryAssessmentDTO {
	
	private String companyRiskLevel;
	private String programCodeCategory;
	private String complexQuery;
	private String highUserExperienceAndKnowledge;
	private String comment;
	
	private String changedBy;

	

    public String getCompanyRiskLevel() {
		return companyRiskLevel;
	}

	public void setCompanyRiskLevel(String companyRiskLevel) {
		this.companyRiskLevel = companyRiskLevel;
	}

	public String getProgramCodeCategory() {
		return programCodeCategory;
	}

	public void setProgramCodeCategory(String programCodeCategory) {
		this.programCodeCategory = programCodeCategory;
	}

	public String getComplexQuery() {
		return complexQuery;
	}

	public void setComplexQuery(String complexQuery) {
		this.complexQuery = complexQuery;
	}

	public String getHighUserExperienceAndKnowledge() {
		return highUserExperienceAndKnowledge;
	}

	public void setHighUserExperienceAndKnowledge(
			String highUserExperienceAndKnowledge) {
		this.highUserExperienceAndKnowledge = highUserExperienceAndKnowledge;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public String getChangedBy() {
		return changedBy;
	}

	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}

	public QueryAssessment getQueryDescription() {
		QueryAssessment qa = new QueryAssessment();
			qa.setComment(getComment());
			qa.setCompanyRiskLevel(getCompanyRiskLevel());
			qa.setComplexQuery(getComplexQuery());
			qa.setHighUserExperienceAndKnowledge(getHighUserExperienceAndKnowledge());
			qa.setProgramCodeCategory(getProgramCodeCategory());
        return qa;
    }
	
	public QueryAssessmentDTO(){};
	public QueryAssessmentDTO(QueryAssessment assessment){
		
		this.companyRiskLevel=assessment.getCompanyRiskLevel();
		this.programCodeCategory=assessment.getProgramCodeCategory();
		this.complexQuery=assessment.getComplexQuery();
		this.highUserExperienceAndKnowledge= assessment.getHighUserExperienceAndKnowledge();
		this.comment=assessment.getComment();
	}
    
    
}
