package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.EmailHistoryEAO;
import com.roche.dss.qtt.model.EmailHistory;

@Repository("emailHistoryEAO")
public class EmailHistoryEAOImpl extends AbstractBaseEAO<EmailHistory> implements EmailHistoryEAO {

}