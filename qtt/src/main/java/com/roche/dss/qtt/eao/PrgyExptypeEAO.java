package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.PrgyExptype;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface PrgyExptypeEAO extends QueryRelatedEAO<PrgyExptype>{
}
