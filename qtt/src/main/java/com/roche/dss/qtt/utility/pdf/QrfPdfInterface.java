package com.roche.dss.qtt.utility.pdf;

import java.io.ByteArrayOutputStream;

public interface QrfPdfInterface {

	/**
	 * @param queryId
	 * @param prefix
	 *            required
	 * @return ByteArrayOutputStream
	 */
	public abstract ByteArrayOutputStream qrfBuilder(int queryId, String prefix);

	public abstract ByteArrayOutputStream qrfBuilder(String queryNo,
                                                     String prefix);

	public abstract ByteArrayOutputStream pdfBuilder(int queryId,
                                                     String queryNoPhrase);

}