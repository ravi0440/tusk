package com.roche.dss.qtt.model;

@Entity
@Table(name = "AUDITS")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Audit implements java.io.Serializable {

	private static final long serialVersionUID = 6661703062479188108L;
	
	private long querySeq;
	private Query query;
	private Boolean allCases;
	private Boolean allStudies;
	private String studyNo;
	private Boolean allSpontaneousCases;
	private Boolean allSubm;
	private Boolean susarSubm;
	private Boolean seriousCases;
	private Boolean nonSeriousCases;
	private String submissionTitles;

	public Audit() {
	}

	public Audit(Query query) {
		this.query = query;
		this.querySeq = query.getQuerySeq();
	}

	@GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "query"))
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "QUERY_SEQ", nullable = false, precision = 10, scale = 0)
	public long getQuerySeq() {
		return this.querySeq;
	}

	public void setQuerySeq(long querySeq) {
		this.querySeq = querySeq;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	public Query getQuery() {
		return this.query;
	}

	public void setQuery(Query query) {
		this.query = query;
	}

	public Boolean getAllCases() {
		return allCases;
	}

	public void setAllCases(Boolean allCases) {
		this.allCases = allCases;
	}

	public Boolean getAllStudies() {
		return allStudies;
	}

	public void setAllStudies(Boolean allStudies) {
		this.allStudies = allStudies;
	}

	public String getStudyNo() {
		return studyNo;
	}

	public void setStudyNo(String studyNo) {
		this.studyNo = studyNo;
	}

	public Boolean getAllSpontaneousCases() {
		return allSpontaneousCases;
	}

	public void setAllSpontaneousCases(Boolean allSpontaneousCases) {
		this.allSpontaneousCases = allSpontaneousCases;
	}

	public Boolean getAllSubm() {
		return allSubm;
	}

	public void setAllSubm(Boolean allSubm) {
		this.allSubm = allSubm;
	}

	public Boolean getSusarSubm() {
		return susarSubm;
	}

	public void setSusarSubm(Boolean susarSubm) {
		this.susarSubm = susarSubm;
	}

	public Boolean getSeriousCases() {
		return seriousCases;
	}

	public void setSeriousCases(Boolean seriousCases) {
		this.seriousCases = seriousCases;
	}

	public Boolean getNonSeriousCases() {
		return nonSeriousCases;
	}

	public void setNonSeriousCases(Boolean nonSeriousCases) {
		this.nonSeriousCases = nonSeriousCases;
	}

	public String getSubmissionTitles() {
		return submissionTitles;
	}

	public void setSubmissionTitles(String submissionTitles) {
		this.submissionTitles = submissionTitles;
	}


}
