package com.roche.dss.qtt.query.web.model;

import java.io.File;

public class InitiationEmailModel {

    private String to;
    private String cc;
    private String subject;
    private String body;
    private File fileOne;
    private File fileTwo;
    private File fileThree;
    private String fileOneFileName;
    private String fileTwoFileName;
    private String fileThreeFileName;
    private String fileOneContentType;
    private String fileTwoContentType;
    private String fileThreeContentType;
    private String msg;
    private String ignoreEmail;
    private String from;
    private String emailType;
    private long queryId;


    /**
     * @return Returns the body.
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body The body to set.
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return Returns the cc.
     */
    public String getCc() {
        return cc;
    }

    /**
     * @param cc The cc to set.
     */
    public void setCc(String cc) {
        this.cc = cc;
    }

    /**
     * @return Returns the docFile1.
     */
    public File getFileOne() {
        return fileOne;
    }

    /**
     * @param docFile1 The docFile1 to set.
     */
    public void setFileOne(File docFile1) {
        this.fileOne = docFile1;
    }

    /**
     * @return Returns the docFile2.
     */
    public File getFileTwo() {
        return fileTwo;
    }

    /**
     * The docFile2 to set.
     */
    public void setFileTwo(File fileTwo) {
        this.fileTwo = fileTwo;
    }

    /**
     * @return Returns the subject.
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject The subject to set.
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return Returns the docFile3.
     */
    public File getFileThree() {
        return fileThree;
    }

    /**
     * @param docFile3 The docFile3 to set.
     */
    public void setFileThree(File docFile3) {
        this.fileThree = docFile3;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }

    public String getFileOneFileName() {
        return fileOneFileName;
    }

    public void setFileOneFileName(String fileOneFileName) {
        this.fileOneFileName = fileOneFileName;
    }

    public String getFileTwoFileName() {
        return fileTwoFileName;
    }

    public void setFileTwoFileName(String fileTwoFileName) {
        this.fileTwoFileName = fileTwoFileName;
    }

    public String getFileThreeFileName() {
        return fileThreeFileName;
    }

    public void setFileThreeFileName(String fileThreeFileName) {
        this.fileThreeFileName = fileThreeFileName;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getIgnoreEmail() {
        return ignoreEmail;
    }

    public void setIgnoreEmail(String ignoreEmail) {
        this.ignoreEmail = ignoreEmail;
    }

    public String getFileOneContentType() {
        return fileOneContentType;
    }

    public void setFileOneContentType(String fileOneContentType) {
        this.fileOneContentType = fileOneContentType;
    }

    public String getFileTwoContentType() {
        return fileTwoContentType;
    }

    public void setFileTwoContentType(String fileTwoContentType) {
        this.fileTwoContentType = fileTwoContentType;
    }

    public String getFileThreeContentType() {
        return fileThreeContentType;
    }

    public void setFileThreeContentType(String fileThreeContentType) {
        this.fileThreeContentType = fileThreeContentType;
    }


    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFrom() {
        return from;
    }

    public String getEmailType() {
        return emailType;
    }

    public long getQueryId() {
        return queryId;
    }
}