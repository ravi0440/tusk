/* 
====================================================================
  $Header$
  @author $Author: borowieb $
  @version $Revision: 1241 $ $Date: 2008-07-01 11:50:42 +0200 (Wt, 01 lip 2008) $

====================================================================
*/
package com.roche.dss.qtt.query.web.qrf.model;


/**
 * This form bean is used by Struts to interact with forms on the QTT JSP pages.
 * @author shaikhi
 */
public class DrugForm {
	
	//first drug
	int drugtype1;
	String retrDrug1;
	String genericDrug1;
	String nongeneric1;
	String route1;
	String formulation1;
	String indication1;
	String dose1; 
	
	//second drug
	int drugtype2;
	String retrDrug2;
	String genericDrug2;
	String nongeneric2;
	String route2;
	String formulation2;
	String indication2;
	String dose2; 
	
	//third drug
	int drugtype3;
	String retrDrug3;
	String genericDrug3;
	String nongeneric3;
	String route3;
	String formulation3;
	String indication3;
	String dose3; 
	  
	//fourth drug
	int drugtype4;
	String retrDrug4;
	String genericDrug4;
	String nongeneric4;
	String route4;
	String formulation4;
	String indication4;
	String dose4; 
	  
	//fifth drug
	int drugtype5;
	String retrDrug5;
	String genericDrug5;
	String nongeneric5;
	String route5;
	String formulation5;
	String indication5;
	String dose5;


    String edit;

    public String getEdit() {
        return edit;
    }

    public void setEdit(String edit) {
        this.edit = edit;
    }
    /**
     * @return
     */
    public String getDose1() {
        return dose1;
    }

    /**
     * @return
     */
    public String getDose2() {
        return dose2;
    }

    /**
     * @return
     */
    public String getDose3() {
        return dose3;
    }

    /**
     * @return
     */
    public String getDose4() {
        return dose4;
    }

    /**
     * @return
     */
    public String getDose5() {
        return dose5;
    }

    public int getDrugtype1() {
        return drugtype1;
    }

    /**
     * @return
     */
    public int getDrugtype2() {
        return drugtype2;
    }

    /**
     * @return
     */
    public int getDrugtype3() {
        return drugtype3;
    }

    /**
     * @return
     */
    public int getDrugtype4() {
        return drugtype4;
    }

    /**
     * @return
     */
    public int getDrugtype5() {
        return drugtype5;
    }

    /**
     * @return
     */
    public String getFormulation1() {
        return formulation1;
    }

    /**
     * @return
     */
    public String getFormulation2() {
        return formulation2;
    }

    /**
     * @return
     */
    public String getFormulation3() {
        return formulation3;
    }

    /**
     * @return
     */
    public String getFormulation4() {
        return formulation4;
    }

    /**
     * @return
     */
    public String getFormulation5() {
        return formulation5;
    }

    /**
     * @return
     */
    public String getGenericDrug1() {
        return genericDrug1;
    }

    /**
     * @return
     */
    public String getGenericDrug2() {
        return genericDrug2;
    }

    /**
     * @return
     */
    public String getGenericDrug3() {
        return genericDrug3;
    }

    /**
     * @return
     */
    public String getGenericDrug4() {
        return genericDrug4;
    }

    /**
     * @return
     */
    public String getGenericDrug5() {
        return genericDrug5;
    }

    /**
     * @return
     */
    public String getIndication1() {
        return indication1;
    }

    /**
     * @return
     */
    public String getIndication2() {
        return indication2;
    }

    /**
     * @return
     */
    public String getIndication3() {
        return indication3;
    }

    /**
     * @return
     */
    public String getIndication4() {
        return indication4;
    }

    /**
     * @return
     */
    public String getIndication5() {
        return indication5;
    }

    /**
     * @return
     */
    public String getNongeneric1() {
        return nongeneric1;
    }

    /**
     * @return
     */
    public String getNongeneric2() {
        return nongeneric2;
    }

    /**
     * @return
     */
    public String getNongeneric3() {
        return nongeneric3;
    }

    /**
     * @return
     */
    public String getNongeneric4() {
        return nongeneric4;
    }

    /**
     * @return
     */
    public String getNongeneric5() {
        return nongeneric5;
    }

    /**
     * @return
     */
    public String getRetrDrug1() {
        return retrDrug1;
    }

    /**
     * @return
     */
    public String getRetrDrug2() {
        return retrDrug2;
    }

    /**
     * @return
     */
    public String getRetrDrug3() {
        return retrDrug3;
    }

    /**
     * @return
     */
    public String getRetrDrug4() {
        return retrDrug4;
    }

    /**
     * @return
     */
    public String getRetrDrug5() {
        return retrDrug5;
    }

    /**
     * @return
     */
    public String getRoute1() {
        return route1;
    }

    /**
     * @return
     */
    public String getRoute2() {
        return route2;
    }

    /**
     * @return
     */
    public String getRoute3() {
        return route3;
    }

    /**
     * @return
     */
    public String getRoute4() {
        return route4;
    }

    /**
     * @return
     */
    public String getRoute5() {
        return route5;
    }

    /**
     * @param string
     */
    public void setDose1(String string) {
        dose1 = string;
    }

    /**
     * @param string
     */
    public void setDose2(String string) {
        dose2 = string;
    }

    /**
     * @param string
     */
    public void setDose3(String string) {
        dose3 = string;
    }

    /**
     * @param string
     */
    public void setDose4(String string) {
        dose4 = string;
    }

    /**
     * @param string
     */
    public void setDose5(String string) {
        dose5 = string;
    }

    /**
     * @param i
     */
    public void setDrugtype1(int i) {
        drugtype1 = i;
    }

    /**
     * @param i
     */
    public void setDrugtype2(int i) {
        drugtype2 = i;
    }

    /**
     * @param i
     */
    public void setDrugtype3(int i) {
        drugtype3 = i;
    }

    /**
     * @param i
     */
    public void setDrugtype4(int i) {
        drugtype4 = i;
    }

    /**
     * @param i
     */
    public void setDrugtype5(int i) {
        drugtype5 = i;
    }

    /**
     * @param string
     */
    public void setFormulation1(String string) {
        formulation1 = string;
    }

    /**
     * @param string
     */
    public void setFormulation2(String string) {
        formulation2 = string;
    }

    /**
     * @param string
     */
    public void setFormulation3(String string) {
        formulation3 = string;
    }

    /**
     * @param string
     */
    public void setFormulation4(String string) {
        formulation4 = string;
    }

    /**
     * @param string
     */
    public void setFormulation5(String string) {
        formulation5 = string;
    }

    /**
     * @param string
     */
    public void setGenericDrug1(String string) {
        genericDrug1 = string;
    }

    /**
     * @param string
     */
    public void setGenericDrug2(String string) {
        genericDrug2 = string;
    }

    /**
     * @param string
     */
    public void setGenericDrug3(String string) {
        genericDrug3 = string;
    }

    /**
     * @param string
     */
    public void setGenericDrug4(String string) {
        genericDrug4 = string;
    }

    /**
     * @param string
     */
    public void setGenericDrug5(String string) {
        genericDrug5 = string;
    }

    /**
     * @param string
     */
    public void setIndication1(String string) {
        indication1 = string;
    }

    /**
     * @param string
     */
    public void setIndication2(String string) {
        indication2 = string;
    }

    /**
     * @param string
     */
    public void setIndication3(String string) {
        indication3 = string;
    }

    /**
     * @param string
     */
    public void setIndication4(String string) {
        indication4 = string;
    }

    /**
     * @param string
     */
    public void setIndication5(String string) {
        indication5 = string;
    }

    @FieldExpressionValidator(expression = "nongeneric1 != null && nongeneric1!='' || drugtype1 != 0",  key = "drugForm.nongeneric1.displayname")
    public void setNongeneric1(String string) {
        nongeneric1 = string;
    }

    @FieldExpressionValidator(expression = "nongeneric2 != null && nongeneric2!='' || drugtype2 != 0", key = "drugForm.nongeneric2.displayname")
    public void setNongeneric2(String string) {
        nongeneric2 = string;
    }

    @FieldExpressionValidator(expression = "nongeneric3 != null && nongeneric3!='' || drugtype3 != 0", key = "drugForm.nongeneric3.displayname")
    public void setNongeneric3(String string) {
        nongeneric3 = string;
    }

    @FieldExpressionValidator(expression = "nongeneric4 != null && nongeneric4!='' || drugtype4 != 0", key = "drugForm.nongeneric4.displayname")
    public void setNongeneric4(String string) {
        nongeneric4 = string;
    }

    @FieldExpressionValidator(expression = "nongeneric5 != null && nongeneric5!='' || drugtype5 != 0", key = "drugForm.nongeneric5.displayname")
    public void setNongeneric5(String string) {
        nongeneric5 = string;
    }

    /**
     * @param string
     */
    public void setRetrDrug1(String string) {
        retrDrug1 = string;
    }

    /**
     * @param string
     */
    public void setRetrDrug2(String string) {
        retrDrug2 = string;
    }

    /**
     * @param string
     */
    public void setRetrDrug3(String string) {
        retrDrug3 = string;
    }

    /**
     * @param string
     */
    public void setRetrDrug4(String string) {
        retrDrug4 = string;
    }

    /**
     * @param string
     */
    public void setRetrDrug5(String string) {
        retrDrug5 = string;
    }

    /**
     * @param string
     */
    public void setRoute1(String string) {
        route1 = string;
    }

    /**
     * @param string
     */
    public void setRoute2(String string) {
        route2 = string;
    }

    /**
     * @param string
     */
    public void setRoute3(String string) {
        route3 = string;
    }

    /**
     * @param string
     */
    public void setRoute4(String string) {
        route4 = string;
    }

    /**
     * @param string
     */
    public void setRoute5(String string) {
        route5 = string;
    }
    
	public void validate(ActionSupport action) {
		
		//if roche drug is selected then either the drug retrieval name or generic drug name is required
		if (drugtype1 == 1) {
		
			//check if both pref and generic are "0". as one of them should be selected
			if  ( (retrDrug1.equals("0")) && (genericDrug1.equals("0")) )  			
				  action.addActionError(action.getText("drugForm.one.retrDrug.genericDrug.displayname"));
			
			//set the pref and generic name to null if "0" (blank)
			if (retrDrug1.equals("0"))
				this.retrDrug1 = null;
			if (genericDrug1.equals("0"))
				this.genericDrug1 = null;	
		}
		
		//if roche drug is selected then either the drug retrieval name or generic drug name is required
		if (drugtype2 == 1) {
		
			  //check if both pref and generic are "0". as one of them should be selected
			  if  ( (retrDrug2.equals("0")) && (genericDrug2.equals("0")) )  			
					action.addActionError(action.getText("drugForm.two.retrDrug.genericDrug.displayname"));
					
			//set the pref and generic name to null if "0" (blank)
			if (retrDrug2.equals("0"))
				this.retrDrug2 = null;
			if (genericDrug2.equals("0"))
				this.genericDrug2 = null;
		}
		
		//if roche drug is selected then either the drug retrieval name or generic drug name is required
		if (drugtype3 == 1) {
		
			//check if both pref and generic are "0". as one of them should be selected
			if  ( (retrDrug3.equals("0")) && (genericDrug3.equals("0")) )  			
				  action.addActionError(action.getText("drugForm.three.retrDrug.genericDrug.displayname"));

			//set the pref and generic name to null if "0" (blank)
			if (retrDrug3.equals("0"))
				this.retrDrug3 = null;
			if (genericDrug3.equals("0"))
				this.genericDrug3 = null;
		}
		
		//if roche drug is selected then either the drug retrieval name or generic drug name is required
		if (drugtype4 == 1) {
		
			//check if both pref and generic are "0". as one of them should be selected
			if  ( (retrDrug4.equals("0")) && (genericDrug4.equals("0")) )  			
				  action.addActionError(action.getText("drugForm.four.retrDrug.genericDrug.displayname"));
			
			//set the pref and generic name to null if "0" (blank)
			if (retrDrug4.equals("0"))
				this.retrDrug4 = null;
			if (genericDrug4.equals("0"))
				this.genericDrug4 = null;
		}
		
		//if roche drug is selected then either the drug retrieval name or generic drug name is required
		if (drugtype5 == 1) {
		
			//check if both pref and generic are "0". as one of them should be selected
			if  ( (retrDrug5.equals("0")) && (genericDrug5.equals("0")) )  			
				action.addActionError(action.getText("drugForm.five.retrDrug.genericDrug.displayname"));
				
			//set the pref and generic name to null if "0" (blank)
			if (retrDrug5.equals("0"))
				this.retrDrug5 = null;
			if (genericDrug5.equals("0"))
				this.genericDrug5 = null;
		}
	}

    @Override
	public String toString() {
        
        return ("Drug Form:-" + 
                "\n drugType1:" + drugtype1 + " retrDrug1: " + retrDrug1 + " genericDrug1:" + genericDrug1 + 
                " nongeneric1" + nongeneric1 + " route1" + route1 + " formulation1" + formulation1 + 
                " indication1" + indication1 + " dose1" + dose1 +          

                "\n drugType2:" + drugtype2 + " retrDrug2: " + retrDrug2 + " genericDrug2:" + genericDrug2 + 
                " nongeneric2" + nongeneric2 + " route2" + route2 + " formulation2" + formulation2 + 
                " indication2" + indication2 + " dose2" + dose2 +          

                "\n drugType3:" + drugtype3 + " retrDrug3: " + retrDrug3 + " genericDrug3:" + genericDrug3 + 
                " nongeneric3" + nongeneric3 + " route3" + route3 + " formulation3" + formulation3 + 
                " indication3" + indication3 + " dose3" + dose3 +          

                "\n drugType4:" + drugtype4 + " retrDrug4: " + retrDrug4 + " genericDrug4:" + genericDrug4 + 
                " nongeneric4" + nongeneric4 + " route4" + route4 + " formulation4" + formulation4 + 
                " indication4" + indication4 + " dose4" + dose4 +          

                "\n drugType5:" + drugtype5 + " retrDrug5: " + retrDrug5 + " genericDrug5:" + genericDrug5 + 
                " nongeneric5" + nongeneric5 + " route5" + route5 + " formulation2" + formulation5 + 
                " indication5" + indication5 + " dose5" + dose5);          

    }
}
