package com.roche.dss.qtt.service.impl;

import com.roche.dss.qtt.eao.*;
import com.roche.dss.qtt.model.*;
import com.roche.dss.qtt.query.web.qrf.Constants;
import com.roche.dss.qtt.service.InitiateService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.workflow.WorkflowService;
import java.util.List;


@Service("initiateService")
@Transactional
public class InitiateServiceImpl implements InitiateService {

    private static final Logger logger = LoggerFactory.getLogger(InitiateServiceImpl.class);

    @Autowired
    private QueryEAO queryEAO;
    @Autowired
    private QueryStatusCodeEAO queryStatusCodeEAO;
    @Autowired
    private QueryTypeEAO queryTypeEAO;
    @Autowired
    private WorkflowService workflowService;
    @Autowired
    private QueryService queryService;
    @Autowired
    private QueryDescriptionEAO queryDescriptionEAO;
    @Autowired
    private CaseEAO caseEAO;
    @Autowired
    private AeTermEAO aeTermEAO;
    @Autowired
    private DrugEAO drugEAO;
    @Autowired
    private PregnancyEAO pregnancyEAO;
    @Autowired
    private ClinicalTrialEAO clinicalTrialEAO;
    @Autowired
    private PerformanceMetricEAO performanceMetricEAO;
    @Autowired
    private RetrievalPeriodEAO retrievalPeriodEAO;
    @Autowired
    private AttachmentEAO attachmentEAO;
    @Autowired
    private AffiliateNoteEAO affiliateNoteEAO;
    @Autowired
    private NonAffiliateNoteEAO nonAffiliateNoteEAO;
    @Autowired
    private PreQueryPreparationEAO preQueryPreparationEAO;
    @Autowired
    private RequesterEAO requesterEAO;
    @Autowired
    private OrganisationEAO organisationEAO;
    @Autowired
    private LdocTypeEAO ldocTypeEAO;
    @Autowired
	private DataSearchEAO dataSearchEAO;
    @Autowired
	private AuditEAO auditEAO;
    @Autowired
    private ManufacturingEAO manufacturingEAO;

	
    @Override
	public List<Query> getInitiateList() {
		return queryService.getInitiateList();
	}

    @Override
    public List<Query> getDuplicateQueries(String countryCode, int reporterTypeId,
    		QueryType.Code queryTypeCode, boolean allDrugs, String retrDrug, String genericDrug) {
        return queryEAO.getDuplicateQueries(countryCode, reporterTypeId,
                queryTypeCode, allDrugs, retrDrug,
                genericDrug);
    }

    @Override
    public void amendQueryType(long queryId, QueryType.Code oldQueryTypeCode, QueryType.Code newQueryTypeCode) {
        //lets delete/update the old query type details for this query first
        Query query = queryEAO.get(queryId);
        switch (oldQueryTypeCode) {

            case DRUG_EVENT:
                doDrugEvent(query);
                break;
            case DRUG_INTERACTION:
                doDrugInteraction(query);
                break;
            case MANUFACTURING:
                doManufacturing(query);
                break;
            case PREGNANCY:
                doPregnancy(query);
                break;
            case EXTERNAL_AUDIT:
                doExternalAudit(query);
                break;
            case CASE_CLARIFICATION:
            case CASE_DATA_REQUEST:
                doCaseClarification(query);
                break;
            case SAE_RECONCILIATION:
                doSAEReconciliation(query);
                break;
            case IND_UPDATES:
                doINDUpdates(query);
                break;
            case PERFORMANCE_METRICS:
            case PERFORMANCE_METRICS2:
                doPerformanceMetrics(query);
                break;
            case SIGNAL_DETECTION:
                doSignalDetection(query);
                break;
            case LITERATURE_SEARCH:
                doLiterature(query);
                break;
            default:
                //do nothing
                break;
        }

        QueryType qType = queryTypeEAO.getByIdObj(newQueryTypeCode);
        query.setQueryType(qType);
        QueryStatusCode qStatus = queryStatusCodeEAO.findStatus(Constants.QUERY_STATUS_AMENDED);
        query.setQueryStatusCode(qStatus);
    }

    @Override
    public void deleteQuery(long queryId, QueryType.Code queryTypeCode) {

        Query query = queryEAO.get(queryId);

        //lets delete/update the old query type details for this query first
        switch (queryTypeCode) {

            case DRUG_EVENT:
                doDrugEvent(query);
                break;
            case DRUG_INTERACTION:
                doDrugInteraction(query);
                break;
            case MANUFACTURING:
                doManufacturing(query);
                break;
            case PREGNANCY:
                doPregnancy(query);
                break;
            case EXTERNAL_AUDIT:
                doExternalAudit(query);
                break;
            case CASE_CLARIFICATION:
            case CASE_DATA_REQUEST:
                doCaseClarification(query);
                break;
            case SAE_RECONCILIATION:
                doSAEReconciliation(query);
                break;
            case IND_UPDATES:
                doINDUpdates(query);
                break;
            case PERFORMANCE_METRICS:
            case PERFORMANCE_METRICS2:
                doPerformanceMetrics(query);
                break;
            case SIGNAL_DETECTION:
                doSignalDetection(query);
                break;
            case LITERATURE_SEARCH:
                doLiterature(query);
                break;
            case DATA_SEARCH:
            	doDataSearch(query);
            	break;
            case EXTERNAL_AUDIT_INSPECTION:
            	doExternalAuditInspection(query);
            	break;
            case MANUFACTURING_RECALL:
            	doManufacturingRecall(query);
            	break;
            case MEDICAL:
            	doMedical(query);
            	break;
            default:
                //do nothing
                break;
        }

        retrievalPeriodEAO.removeQueryRelated(query.getQuerySeq());
        drugEAO.removeQueryRelated(query);
        attachmentEAO.removeQueryRelated(query);
        affiliateNoteEAO.removeQueryRelated(query);
        nonAffiliateNoteEAO.removeQueryRelated(query);
        preQueryPreparationEAO.removeQueryRelated(query);

        //delete the query
        //first get the requester to delete it

        Requester requester = query.getRequester();
       //  ldocTypeEAO deals with LdocType.. check it
       // ldocTypeEAO.removeQueryRelated(query);

        queryEAO.remove(query);

        if (requester != null) {
            Organisation organisation = requester.getOrganisation();
            requesterEAO.remove(requester);
            if (organisation != null) {
                organisationEAO.remove(organisation);
            }
        }
    }

    private void doMedical(Query query) {
        queryDescriptionEAO.removeQueryRelated(query.getQuerySeq());
        clinicalTrialEAO.removeQueryRelated(query.getQuerySeq());
        aeTermEAO.removeQueryRelated(query.getQuerySeq());
        pregnancyEAO.removeQueryRelated(query.getQuerySeq());
        caseEAO.removeQueryRelated(query.getQuerySeq());
	}

	private void doManufacturingRecall(Query query) {
		manufacturingEAO.removeQueryRelated(query.getQuerySeq());
        queryDescriptionEAO.removeQueryRelated(query.getQuerySeq());
        aeTermEAO.removeQueryRelated(query.getQuerySeq());
        clinicalTrialEAO.removeQueryRelated(query.getQuerySeq());
	}

	private void doExternalAuditInspection(Query query) {
        queryDescriptionEAO.removeQueryRelated(query.getQuerySeq());
        auditEAO.removeQueryRelated(query.getQuerySeq());
        clinicalTrialEAO.removeQueryRelated(query.getQuerySeq());
	}

	private void doDataSearch(Query query) {
        queryDescriptionEAO.removeQueryRelated(query.getQuerySeq());
        dataSearchEAO.removeQueryRelated(query.getQuerySeq());
        clinicalTrialEAO.removeQueryRelated(query.getQuerySeq());
        caseEAO.removeQueryRelated(query.getQuerySeq());
	}

	private void doLiterature(Query query) {
        queryDescriptionEAO.removeQueryRelated(query.getQuerySeq());
        aeTermEAO.removeQueryRelated(query.getQuerySeq());
    }

    private void doSignalDetection(Query query) {
        queryDescriptionEAO.removeQueryRelated(query.getQuerySeq());
        aeTermEAO.removeQueryRelated(query.getQuerySeq());
    }


    private void doPerformanceMetrics(Query query) {
        performanceMetricEAO.removeQueryRelated(query.getQuerySeq());
        queryDescriptionEAO.removeQueryRelated(query.getQuerySeq());
        clinicalTrialEAO.removeQueryRelated(query.getQuerySeq());
    }

    private void doINDUpdates(Query query) {
        query.setIndNumber(null);
    }


    private void doSAEReconciliation(Query query) {
        clinicalTrialEAO.removeQueryRelated(query.getQuerySeq());
        query.setOformatSpecialRequest(null);
    }


    private void doCaseClarification(Query query) {
        caseEAO.removeQueryRelated(query.getQuerySeq());
        query.setCaseComment(null);
        query.setCaseOformat(null);
        query.setOformatSpecialRequest(null);
    }

    private void doExternalAudit(Query query) {
        queryDescriptionEAO.removeQueryRelated(query.getQuerySeq());
        aeTermEAO.removeQueryRelated(query.getQuerySeq());
        clinicalTrialEAO.removeQueryRelated(query.getQuerySeq());
        query.setOformatSpecialRequest(null);
    }

    private void doPregnancy(Query query) {
        pregnancyEAO.removeQueryRelated(query.getQuerySeq());
        queryDescriptionEAO.removeQueryRelated(query.getQuerySeq());
        caseEAO.removeQueryRelated(query.getQuerySeq());
        query.setCaseComment(null);
        query.setCaseOformat(null);
        query.setOformatSpecialRequest(null);
    }

    private void doManufacturing(Query query) {
        queryDescriptionEAO.removeQueryRelated(query.getQuerySeq());
        caseEAO.removeQueryRelated(query.getQuerySeq());
        query.setCaseComment(null);
        query.setMnftBatchNumber(null);
        query.setCaseOformat(null);
        query.setOformatSpecialRequest(null);
    }

    private void doDrugInteraction(Query query) {
        drugEAO.removeFlaggedQueryDrugs(query, Constants.NO);
        queryDescriptionEAO.removeQueryRelated(query.getQuerySeq());
        caseEAO.removeQueryRelated(query.getQuerySeq());
        query.setCaseComment(null);
    }

    private void doDrugEvent(Query query) {
        queryDescriptionEAO.removeQueryRelated(query.getQuerySeq());
        caseEAO.removeQueryRelated(query.getQuerySeq());
        aeTermEAO.removeQueryRelated(query.getQuerySeq());
        query.setCaseComment(null);
    }

    private boolean acceptQuery(Query query, String status) {
        boolean isReopened = false;
        QueryStatusCode qStatus = queryStatusCodeEAO.findStatus(status);
        query.setQueryStatusCode(qStatus);

        if (query.getReopen() != null && query.getReopen().equals('Y')) {
            query.setReopen('N');
            isReopened = true;
        }
        return isReopened;
    }


    @Override
    public void acceptQuery(Long queryId, String status, String userName) {
        Query query = queryEAO.get(queryId);
        logger.info(" acceptQuery called on server with the query number:" + query.getQueryNumber() + " by userId:" + userName + " ");
        boolean isReopen = acceptQuery(query, status);
        query = queryService.merge(query);

        try {
            if (isReopen ||
                    // in case of amending initiatted query...
                    query.getProcessInstanceId() == null) {
                workflowService.initiateWorkflow(query, userName);
            }
        } catch (JbpmPersistenceException e) {
            logger.error("JbpmPersistenceException! " + e.getMessage());
        }


        // what about audithistory?
        //auditEventDAO.insertJobStartEvent(queryNumber, dsclUserId);
    }

    @Override
    public List<QueryType> getQueryTypes() {
        return queryTypeEAO.getActive();
    }
	
}
