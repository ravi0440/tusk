package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.service.UtilService;

@ParentPackage("default") 
public class FullTextIndexAction extends CommonActionSupport {
	@Autowired
	protected UtilService utilService;
	
	protected int stage = UtilService.STAGE_UNKNOWN;
	protected int startIndex = 0;
	protected int count = 0;
	protected int step = 10;
	
	protected static final Logger log = LoggerFactory.getLogger(FullTextIndexAction.class);
	
	@Action(value="rebuildIndex", results={@Result(name="success", type="tiles", location="/full_text_rebuild")})
	public String rebuildIndex() {
		int idx=startIndex;
		try {
			
			
			while ((idx+step)<count){
				utilService.reindexAll(Query.class, stage, idx, step);
				idx+=step;
				log.info("INDEXATION IN PROGRESS idx "+idx );
			}
			
		} catch (InterruptedException e) {
			log.error(e.toString(), e);
			log.error("INDEXATION ERROR idx "+idx +"step "+step);
			return ERROR;
		}
		log.error("INDEXATION COMPLETED idx "+idx );
		return SUCCESS;
	}

	public int getStage() {
		return stage;
	}

	public void setStage(int stage) {
		this.stage = stage;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}
	
	
}
