package com.roche.dss.qtt.service;

import com.roche.dss.qtt.model.LdocType;

/**
 * @author zerkowsm
 *
 */
public interface LdocTypeService extends BaseEAOService<LdocType> {

}
