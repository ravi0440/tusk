package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.QueryStatusCode;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface QueryStatusCodeEAO extends BaseEAO<QueryStatusCode> {
    QueryStatusCode findStatus(String status);
}
