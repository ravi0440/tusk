package com.roche.dss.qtt.query.web.action;

import java.util.Arrays;
import java.util.List;

import com.roche.dss.qtt.query.web.model.QuickSearchParams;
import com.roche.dss.qtt.query.web.qrf.model.SearchScopeEntry;

@ParentPackage("default") 
public class QuickSearchAction extends CommonActionSupport {
	
	private static final long serialVersionUID = 6544316219973413128L;

	protected QuickSearchParams searchParams;
	
	protected List<SearchScopeEntry> scopes = Arrays.asList(
			new SearchScopeEntry("both", "Search both Database and Attachments")
			, new SearchScopeEntry("db", "Search only in Database")
			, new SearchScopeEntry("att", "Search only in Attachments"));
	
	@Action(value="QuickSearch", results={@Result(name="success", type="tiles", location="/quick_search.search")})
	public String showInput() {
		return SUCCESS;
	}
	
	public QuickSearchParams getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(QuickSearchParams searchParams) {
		this.searchParams = searchParams;
	}

	public List<SearchScopeEntry> getScopes() {
		return scopes;
	}
}
