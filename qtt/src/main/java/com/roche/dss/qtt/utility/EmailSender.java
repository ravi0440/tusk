/* 
 ====================================================================
 $Header$
 @author $Author: borowieb $
 @version $Revision: 1241 $ $Date: 2008-07-01 11:50:42 +0200 (Wt, 01 lip 2008) $

 ====================================================================
 */
package com.roche.dss.qtt.utility;


import com.roche.dss.qtt.eao.AttachmentFromDmgSearchEAO;
import com.roche.dss.qtt.eao.EmailHistoryEAO;
import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.AttachmentFromDmgSearch;
import com.roche.dss.qtt.model.EmailHistory;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.security.service.SecurityService;
import com.roche.dss.qtt.service.EmailHistoryService;
import com.roche.dss.qtt.utility.config.Config;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * A helper class to assist with the automatic generation and sending of emails.
 *
 * @author rahmanh1
 */
@Component
public class EmailSender {
	 private static final Logger logger = LoggerFactory.getLogger(EmailSender.class);
	 
	public static String ADDRESS_DELIMITER = ",";
	
    @Autowired
    protected Config config;

    @Autowired
    private AttachmentFromDmgSearchEAO attachmentFromDmgSearchEAO;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private EmailHistoryService emailHistoryService;

    public EmailSender() {
    }

    /**
     * Sends an email without any attachments
     *
     * @param fromAddress the from email address
     * @param toAddress   the to email address
     * @param subject     the subject of the email
     * @param body        the body of the email
     * @return true if all deletions were successful
     * @throws javax.mail.internet.AddressException,
     *          SendFailedException, MessagingException
     */
    public void sendMessage(String fromAddress, String toAddress, String subject, String body) throws AddressException,
            SendFailedException, MessagingException {

        Properties p = System.getProperties();
        p.put("mail.host", config.getHost());

        MimeMessage message = new MimeMessage(Session.getInstance(p, null));
        message.setFrom(new InternetAddress(fromAddress));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddress));
        message.setSubject(subject);
 		if (config.isTestEnvironment()) {
 			message.setText(config.getTestEnvironmentEmailHeader() + "\n" + body);
 		} else {
 			message.setText(body);
 		}

 		Transport.send(message);
        saveEmailHistory(subject, body, toAddress, fromAddress);
    }


    /**
     * @param fromAddress
     * @param toAddress
     * @param ccAddress
     * @param subject
     * @param body
     * @throws javax.mail.internet.AddressException
     *
     * @throws javax.mail.SendFailedException
     * @throws javax.mail.MessagingException
     */
    public void sendMessage(String fromAddress, String toAddress, String ccAddress, String subject, String body)
            throws AddressException, SendFailedException, MessagingException {

        Properties p = System.getProperties();
        p.put("mail.host", config.getHost());

        MimeMessage message = new MimeMessage(Session.getInstance(p, null));
        message.setFrom(new InternetAddress(fromAddress));
        // message.setRecipients(Message.RecipientType.TO, InternetAddress
        // .parse(toAddress));
        // Extract the recipients and assign them to the message.
        // Recipient is a comma-separated list of e-mail addresses as per
        // RFC822.
        {
            InternetAddress[] theToAddresses = InternetAddress.parse(toAddress);
            message.addRecipients(Message.RecipientType.TO, theToAddresses);
        }

        // Extract the Cc-recipients and assign them to the message;
        // CcRecipient is a comma-separated list of e-mail addresses as per
        // RFC822
        if (null != ccAddress) {
            InternetAddress[] theCcAddresses = InternetAddress.parse(ccAddress);
            message.addRecipients(Message.RecipientType.CC, theCcAddresses);
        }

        message.setSubject(subject);

        // Create the Multipart to be added the parts to
        Multipart mp = new MimeMultipart();

        // Create and fill the first message part
        {
            MimeBodyPart mbp = new MimeBodyPart();
     		if (config.isTestEnvironment()) {
     			mbp.setText(config.getTestEnvironmentEmailHeader() + "\n" + body);
     		} else {
     			mbp.setText(body);
     		}

            // Attach the part to the multipart;
            mp.addBodyPart(mbp);
        }

        message.setContent(mp);
        message.setSentDate(new Date());
        Transport.send(message);
        saveEmailHistory(subject, body, toAddress, fromAddress);
    }

    public void sendMessage(String fromAddress, String toAddress, String subject, String body, String attachfileName, ByteArrayOutputStream out)
            throws AddressException, SendFailedException, MessagingException {

        Properties p = System.getProperties();
        p.put("mail.host", config.getHost());

        MimeMessage message = new MimeMessage(Session.getInstance(p, null));
        message.setFrom(new InternetAddress(fromAddress));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddress));
        message.setSubject(subject);

        // Create the message part
        BodyPart messageBodyPart = new MimeBodyPart();

        // Fill the message
 		if (config.isTestEnvironment()) {
 			messageBodyPart.setText(config.getTestEnvironmentEmailHeader() + "\n" + body);
 		} else {
 			messageBodyPart.setText(body);
 		}

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        String applicationType = "application/pdf";

        // Part two is attachment
        messageBodyPart = new MimeBodyPart();
        DataSource source = new ByteArrayDataSource(out.toByteArray(), applicationType);

        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(attachfileName);
        multipart.addBodyPart(messageBodyPart);

        message.setContent(multipart);
        message.setSentDate(new Date());

        // message.setText(body);

        Transport.send(message);
        saveEmailHistory(subject, body, toAddress, fromAddress);
    }

    public static class FormFile {
        File file;
        String filename;
        String contentType;

        public FormFile(File file, String filename, String contentType) {
            this.file = file;
            this.filename = filename;
            this.contentType = contentType;
        }

        public File getFile() {
            return file;
        }

        public String getFilename() {
            return filename;
        }

        public String getContentType() {
            return contentType;
        }
    }

    public void sendMessage(String fromAddress, String toAddress, String ccAddress, String subject, String body,
                            String attachfileName, ByteArrayOutputStream outSream, FormFile... files)
            throws MessagingException, FileNotFoundException {

        Properties p = System.getProperties();
        p.put("mail.host", config.getHost());

        MimeMessage message = new MimeMessage(Session.getInstance(p, null));

        message.setFrom(new InternetAddress(fromAddress));
        {
            InternetAddress[] theToAddresses = InternetAddress.parse(toAddress);
            message.addRecipients(Message.RecipientType.TO, theToAddresses);
        }

        if (null != ccAddress) {
            InternetAddress[] theCcAddresses = InternetAddress.parse(ccAddress);
            message.addRecipients(Message.RecipientType.CC, theCcAddresses);
        }

        message.setSubject(subject);
        message.setSentDate(new Date());
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
 		if (config.isTestEnvironment()) {
 			helper.setText(config.getTestEnvironmentEmailHeader() + "\n" + body);
 		} else {
 			helper.setText(body);
 		}
        // Last, or only, attachment file;
        if (outSream != null) {
            String applicationType = "application/pdf";
            DataSource source1 = new ByteArrayDataSource(outSream.toByteArray(), applicationType);
            helper.addAttachment(attachfileName, source1);
        }

        try{
        for (FormFile file : files) {
            if (file.getFile()!= null) {
            helper.addAttachment(file.getFilename(), file.getFile());
            }
        }
        }catch(Exception e){
        	logger.error("Problem sending attachements "+subject, e);
        }
        Transport.send(message);
        saveEmailHistory(subject, body, toAddress, fromAddress);
    }

    public void sendNotificationForAddedAttachment(Query query, Attachment addedAttachment, String sendToAddress,
                                                    String bodyPartOne, String bodyPartTwo) throws MessagingException {
        StringBuilder header = new StringBuilder();
        header.append(config.getAttachmentMailHeader());
        header.append(" ").append(query.getQueryNumber());
        StringBuilder body = new StringBuilder();
        body.append(bodyPartOne).append(" ")
                .append(query.getQueryNumber()).append(" ")
                .append(bodyPartTwo)
                .append("\n")
                .append(config.getApplicationAddress())
                .append("/Attachment.action?attachId=")
                .append(addedAttachment.getAttachmentSeq())
                .append("&queryId=")
                .append(query.getQuerySeq())
                .append("\n\n")
                .append(config.getAttachmentMailLastPara());

        String mailFromUserThatAttachedFile = securityService.getLoggedUser().getMail();

        if(mailFromUserThatAttachedFile != null) {
            sendMessage(mailFromUserThatAttachedFile, sendToAddress, header.toString(), body.toString());
        } else {
            sendMessage(config.getInititateDsclEmail(), sendToAddress, header.toString(), body.toString());
        }

    }

    public void sendNotificationForAddedAttachmentFromDmgSearch(Query query, String sendToAddress, List<AttachmentFromDmgSearch> attachments) throws MessagingException {

        Set<TaskInstance> taskInstances = query.getProcessInstance().getTaskInstances();
        Long currentTaskId = taskInstances.iterator().next().getId();

        if(currentTaskId != null) {
            StringBuilder header = new StringBuilder();
            header.append(config.getAttachmentMailHeader());
            header.append(" ").append(query.getQueryNumber());

            StringBuilder body = new StringBuilder();
            body.append(config.getAttachmentMailDmgToBQueryBodyPartOne()).append(" ")
                    .append(query.getQueryNumber()).append(" ")
                    .append(config.getAttachmentMailDmgToBQueryBodyPartTwo())
                    .append("\n");

            for (AttachmentFromDmgSearch attachmentFromDmgSearch : attachments) {
                body.append(config.getApplicationAddress())
                        .append("/Attachment.action?attachId=")
                        .append(attachmentFromDmgSearch.getAttachmentSeq())
                        .append("&queryId=")
                        .append(attachmentFromDmgSearch.getQuerySeq())
                        .append("\n");
            }

            body.append("\n\n").append(config.getAttachmentMailLastPara());

            String mailFromUserThatAttachedFile = securityService.getLoggedUser().getMail();

            if(mailFromUserThatAttachedFile != null) {
                sendMessage(mailFromUserThatAttachedFile, sendToAddress, header.toString(), body.toString());
            } else {
                sendMessage(config.getInititateDsclEmail(), sendToAddress, header.toString(), body.toString());
            }

        }

    }

    public List<AttachmentFromDmgSearch> getAttachmentsAddedInDagSearch(Query query){
        Set<TaskInstance> taskInstances = query.getProcessInstance().getTaskInstances();
        Long currentTaskId = taskInstances.iterator().next().getId();
        return attachmentFromDmgSearchEAO.getAttachmentsForDMGSearch(currentTaskId);
    }

    public void saveEmailHistory(String subject, String content, String to, String from) {
        emailHistoryService.saveEmailHistory(subject,content,to, from );
    }

}