package com.roche.dss.qtt.query.comms.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.roche.dss.qtt.query.comms.dao.impl.JDBCTemplateDAO;
import com.roche.dss.qtt.query.comms.dto.CountryDTO;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */

@Repository
public class CountryDAOImpl extends JDBCTemplateDAO implements CountryDAO {
    
    private String countryTab;

    /**
     */
    @Override
    public List<CountryDTO> getCountries() {
        String queryStr = "select country_code,description from "+countryTab + " order by description";
        return jdbcTemplate.query(queryStr, new CountriesDTORowMapper());
    }

    class CountriesDTORowMapper implements RowMapper<CountryDTO> {

        @Override
        public CountryDTO mapRow(ResultSet resultSet, int i) throws SQLException {
            CountryDTO countryDTO = new CountryDTO();
            countryDTO.setCode(resultSet.getString("country_code"));
            countryDTO.setDescription(resultSet.getString("description"));
            return countryDTO;
        }
    }

    @Value("${ext.table.countries}")
    public void setCountryTab(String countryTab) {
        this.countryTab = countryTab;
    }

}
