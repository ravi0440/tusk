package com.roche.dss.qtt.query.web.qrf.model;

import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.query.comms.dto.AttachmentDTO;

import java.io.File;
import java.util.Date;
import java.util.List;

@Conversion()
public class QrfFormModel {

    private String firstname;
    private String lastname;
    private String telephone;
    private String fax;
    private String email;
    private String reqCountry;
    private String orgName;
    private int orgType;
    private int repository;
    private int psur;
    private int cds;
    private int issue;
    private int literature;
    private int others;
    private String othersComment;
    private String comments;
    private String sourceCountry;
    private int reporterType;
    private int allDrugs;
    private String retrDrug;
    private String genericDrug;
    private String route;
    private String formulation;
    private String indication;
    private String dose;
    private String othersDrug;
    private Date dateRequested;
    private String urgency = null;
    private String descOne;
    private File fileOne = null;
    private Integer idOne;
    private String descTwo;
    private File fileTwo = null;
    private Integer idTwo;
    private String descThree;
    private File fileThree = null;
    private Integer idThree;
    private String descFour;
    private File fileFour = null;
    private Integer idFour;
    private String descFive;
    private File fileFive = null;

    private String tempFile1;
    private String tempFileName1;
    private String tempFile2;
    private String tempFileName2;
    private String tempFile3;
    private String tempFileName3;
    private String tempFile4;
    private String tempFileName4;
    private String tempFile5;
    private String tempFileName5;


    private String fileOneFileName;
    private String fileTwoFileName;
    private String fileThreeFileName;
    private String fileFourFileName;
    private String fileFiveFileName;

    private String idsToDelete;
    private AttachmentDTO[] attachmentDTOArray = new AttachmentDTO[5];

    private Integer idFive;
    private int cumulative;
    private Date from;
    private Date to;
    private String interval;
    private QueryType.Code queryType;
    private String followUp;
    private String requesterQueryLabel;
    private int id;
    private String myAction;
    private String no;
    private String action;
    private String qrfQueryId;
    private String activity;
    private boolean userLogged;
    private Long freeDiskSpace;
    private String possibleEmailDomains;


    public String getTempFile2() {
        return tempFile2;
    }

    public void setTempFile2(String tempFile2) {
        this.tempFile2 = tempFile2;
    }

    public String getTempFileName2() {
        return tempFileName2;
    }

    public void setTempFileName2(String tempFileName2) {
        this.tempFileName2 = tempFileName2;
    }

    public String getTempFile3() {
        return tempFile3;
    }

    public void setTempFile3(String tempFile3) {
        this.tempFile3 = tempFile3;
    }

    public String getTempFileName3() {
        return tempFileName3;
    }

    public void setTempFileName3(String tempFileName3) {
        this.tempFileName3 = tempFileName3;
    }

    public String getTempFile4() {
        return tempFile4;
    }

    public void setTempFile4(String tempFile4) {
        this.tempFile4 = tempFile4;
    }

    public String getTempFileName4() {
        return tempFileName4;
    }

    public void setTempFileName4(String tempFileName4) {
        this.tempFileName4 = tempFileName4;
    }

    public String getTempFile5() {
        return tempFile5;
    }

    public void setTempFile5(String tempFile5) {
        this.tempFile5 = tempFile5;
    }

    public String getTempFileName5() {
        return tempFileName5;
    }

    public void setTempFileName5(String tempFileName5) {
        this.tempFileName5 = tempFileName5;
    }

    public String getTempFile1() {
        return tempFile1;
    }

    public void setTempFile1(String tempFile1) {
        this.tempFile1 = tempFile1;
    }

    public String getTempFileName1() {
        return tempFileName1;
    }

    public void setTempFileName1(String tempFileName1) {
        this.tempFileName1 = tempFileName1;
    }


    public String getFileThreeFileName() {
        return fileThreeFileName;
    }

    public void setFileThreeFileName(String fileThreeFileName) {
        this.fileThreeFileName = fileThreeFileName;
    }

    public String getFileFourFileName() {
        return fileFourFileName;
    }

    public void setFileFourFileName(String fileFourFileName) {
        this.fileFourFileName = fileFourFileName;
    }

    public String getFileFiveFileName() {
        return fileFiveFileName;
    }

    public void setFileFiveFileName(String fileFiveFileName) {
        this.fileFiveFileName = fileFiveFileName;
    }

    public String getIdsToDelete() {
        return idsToDelete;
    }

    public void setIdsToDelete(String string) {
        idsToDelete = (string != null && !string.isEmpty()) ? string.trim() : null ;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String n) {
        this.no = n;
    }

    /**
     * @return
     */
    public int getAllDrugs() {
        return allDrugs;
    }


    /**
     * @return
     */
    public int getCds() {
        return cds;
    }

    /**
     * @return
     */
    public String getComments() {
        return comments;
    }

    /**
     * @return
     */
    public int getCumulative() {
        return cumulative;
    }

    /**
     * @return
     */
    public String getDescFive() {
        return descFive;
    }

    /**
     * @return
     */
    public String getDescFour() {
        return descFour;
    }

    /**
     * @return
     */
    public String getDescOne() {
        return descOne;
    }

    /**
     * @return
     */
    public String getDescThree() {
        return descThree;
    }

    /**
     * @return
     */
    public String getDescTwo() {
        return descTwo;
    }

    /**
     * @return
     */
    public String getDose() {
        return dose;
    }

    /**
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return
     */
    public String getFax() {
        return fax;
    }

    /**
     * @return
     */
    public File getFileFive() {
        return fileFive;
    }

    /**
     * @return
     */
    public File getFileFour() {
        return fileFour;
    }

    /**
     * @return
     */
    public File getFileOne() {
        return fileOne;
    }

    /**
     * @return
     */
    public File getFileThree() {
        return fileThree;
    }

    /**
     * @return
     */
    public File getFileTwo() {
        return fileTwo;
    }

    /**
     * @return
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @return
     */
    public String getFollowUp() {
        return followUp;
    }

    /**
     * @return
     */
    public String getFormulation() {
        return formulation;
    }

    /**
     * @return
     */
    public String getGenericDrug() {
        return genericDrug;
    }

    /**
     * @return
     */
    public String getIndication() {
        return indication;
    }

    /**
     * @return
     */
    public String getInterval() {
        return interval;
    }

    /**
     * @return
     */
    public int getIssue() {
        return issue;
    }

    /**
     * @return
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @return
     */
    public int getLiterature() {
        return literature;
    }

    @RequiredStringValidator(type = ValidatorType.FIELD, key = "qrfForm.orgName.displayname")
    public String getOrgName() {
        return orgName;
    }

    /**
     * @return
     */
    public int getOrgType() {
        return orgType;
    }

    /**
     * @return
     */
    public int getOthers() {
        return others;
    }

    @FieldExpressionValidator(expression = "othersComment != null && othersComment!='' || others != 7", key = "qrfForm.othersComment.displayname")
    public String getOthersComment() {
        return othersComment;
    }

    /**
     * @return
     */
    public String getOthersDrug() {
        return othersDrug;
    }

    /**
     * @return
     */
    public String getRetrDrug() {
        return retrDrug;
    }

    /**
     * @return
     */
    public int getPsur() {
        return psur;
    }

    /**
     * @return
     */
    public QueryType.Code getQueryType() {
        return queryType;
    }

    /**
     * @return
     */
    public int getReporterType() {
        return reporterType;
    }

    /**
     * @return
     */
    public int getRepository() {
        return repository;
    }

    /**
     * @return
     */
    public String getReqCountry() {
        return reqCountry;
    }

    /**
     * @return
     */
    public String getRoute() {
        return route;
    }

    /**
     * @return
     */
    public String getSourceCountry() {
        return sourceCountry;
    }

    /**
     * @return
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @return
     */
    public String getUrgency() {
        return urgency;
    }

    /**
     * String
     */
    public void setAllDrugs(int i) {
        allDrugs = i;
    }

    /**
     * String
     */
    public void setCds(int i) {
        cds = i;
    }

    @StringLengthFieldValidator(maxLength = "200", key = "qrfForm.comm.displayname")
    public void setComments(String string) {
        comments = string;
    }

    public void setCumulative(int i) {
        cumulative = i;
    }

    /**
     * String
     */
    @StringLengthFieldValidator(maxLength = "100", key = "qrfForm.descFive.displayname")
    public void setDescFive(String string) {
        descFive = (string != null && string.equals("")) ? null : string;
    }

    /**
     * String
     */
    @StringLengthFieldValidator(maxLength = "100", key = "qrfForm.descFour.displayname")
    public void setDescFour(String string) {
        descFour = (string != null && string.equals("")) ? null : string;
    }

    /**
     * String
     */
    @StringLengthFieldValidator(maxLength = "100", key = "qrfForm.descOne.displayname")
    public void setDescOne(String string) {
        descOne = (string != null && string.equals("")) ? null : string;
    }

    /**
     * String
     */
    @StringLengthFieldValidator(maxLength = "100", key = "qrfForm.descThree.displayname")
    public void setDescThree(String string) {
        descThree = (string != null && string.equals("")) ? null : string;
    }

    /**
     * String
     */
    @StringLengthFieldValidator(maxLength = "100", key = "qrfForm.descTwo.displayname")
    public void setDescTwo(String string) {
        descTwo = (string != null && string.equals("")) ? null : string;
    }

    /**
     * String
     */
    public void setDose(String string) {
        dose = string;
    }

    @RequiredStringValidator(type = ValidatorType.FIELD, key = "email.required")
    public void setEmail(String string) {
        email = string;
    }

    /**
     * String
     */
    public void setFax(String string) {
        fax = string;
    }

    /**
     * String
     */
    public void setFileFive(File file) {
        fileFive = file;
    }

    /**
     * String
     */
    public void setFileFour(File file) {
        fileFour = file;
    }

    /**
     * String
     */
    public void setFileOne(File file) {
        fileOne = file;
    }

    /**
     * String
     */
    public void setFileThree(File file) {
        fileThree = file;
    }

    /**
     * String
     */
    public void setFileTwo(File file) {
        fileTwo = file;
    }

    @RequiredStringValidator(type = ValidatorType.FIELD, key = "qrfForm.firstname.displayname")
    public void setFirstname(String string) {
        firstname = string;
    }

    /**
     * String
     */
    public void setFollowUp(String string) {
        followUp = string;
    }

    /**
     * String
     */
    public void setFormulation(String string) {
        formulation = string;
    }

    /**
     * String
     */
    public void setGenericDrug(String string) {
        genericDrug = string;
    }

    /**
     * String
     */
    public void setIndication(String string) {
        indication = string;
    }

    /**
     * String
     */
    public void setInterval(String string) {
        interval = string;
    }

    /**
     * String
     */
    public void setIssue(int i) {
        issue = i;
    }

    @RequiredStringValidator(type = ValidatorType.FIELD, key = "qrfForm.lastname.displayname")
    public void setLastname(String string) {
        lastname = string;
    }

    /**
     * String
     */
    public void setLiterature(int i) {
        literature = i;
    }

    /**
     * String
     */
    public void setOrgName(String string) {
        orgName = string;
    }

    /**
     * String
     */
    public void setOrgType(int i) {
        orgType = i;
    }

    /**
     * String
     */
    public void setOthers(int i) {
        others = i;
    }

    /**
     * String
     */
    public void setOthersComment(String string) {
        othersComment = string;
    }

    @StringLengthFieldValidator(maxLength = "200", key = "qrfForm.othersDrug.displayname")
    public void setOthersDrug(String string) {
        othersDrug = string;
    }

    /**
     * String
     */
    public void setRetrDrug(String string) {
        retrDrug = string;
    }

    /**
     * String
     */
    public void setPsur(int i) {
        psur = i;
    }

    @TypeConversion(converter = "com.opensymphony.xwork2.conversion.impl.EnumTypeConverter")
    public void setQueryType(QueryType.Code i) {
        queryType = i;
    }

    /**
     * String
     */
    public void setReporterType(int i) {
        reporterType = i;
    }

    /**
     * String
     */
    public void setRepository(int i) {
        repository = i;
    }

    /**
     * String
     */
    public void setReqCountry(String string) {
        reqCountry = string;
    }

    /**
     * String
     */
    public void setRoute(String string) {
        route = string;
    }

    /**
     * String
     */
    public void setSourceCountry(String string) {
        sourceCountry = string;
    }

    @RequiredStringValidator(type = ValidatorType.FIELD, key = "qrfForm.telephone.displayname")
    public void setTelephone(String string) {
        telephone = string;
    }

    /**
     * String
     */
    public void setUrgency(String string) {
        urgency = string;
    }

    public Date getDateRequested() {
		return dateRequested;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setDateRequested(Date dateRequested) {
		this.dateRequested = dateRequested;
	}

    public Date getFrom() {
		return from;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setTo(Date to) {
		this.to = to;
	}

	public int getId() {
        return id;
    }

    /**
     * @param i
     */
    public void setId(int i) {
        id = i;
    }

    /**
     * @return
     */
    public String getMyAction() {
        return myAction;
    }

    /**
     * String
     */
    public void setMyAction(String string) {
        myAction = string;
    }

    /**
     * String
     *
     * @author shaikhi
     * <p/>
     * Validate the email address
     */
    public boolean validateEmail(String email) {

        String data = email.trim();        // trim leading / trailing white-space

        //look for @
        if (data.indexOf("@") > -1) {

            //ensure no spaces exist.
            if (data.indexOf(" ") == -1) {
                //make sure at least 1 dot exists.
                if (data.indexOf(".") > -1) {
                    //check the length of the email address
                    if (data.length() >= 5 && data.length() <= 100) {
                        //get the end of the email address (.com,.uk)
                        String temp = data.substring(data.lastIndexOf(".") + 1);
                        //check its length is between 2 and 4 chars.
                        if (temp.length() >= 2 && temp.length() <= 4) {
                            return true;
                        } //end if
                    } //end if length
                } //end if .
            } //end if space
        } //end if @

        //invalid email
        return false;
    }


    /**
     * @return Returns the requesterQueryLabel.
     */
    public String getRequesterQueryLabel() {
        return requesterQueryLabel;
    }

    /**
     * @param requesterQueryLabel The requesterQueryLabel to set.
     */
    public void setRequesterQueryLabel(String requesterQueryLabel) {
        this.requesterQueryLabel = requesterQueryLabel;
    }

    public Integer getIdFive() {
        return idFive;
    }

    public void setIdFive(Integer idFive) {
        this.idFive = idFive;
    }

    public Integer getIdFour() {
        return idFour;
    }

    public void setIdFour(Integer idFour) {
        this.idFour = idFour;
    }

    public Integer getIdOne() {
        return idOne;
    }

    public void setIdOne(Integer idOne) {
        this.idOne = idOne;
    }

    public Integer getIdThree() {
        return idThree;
    }

    public void setIdThree(Integer idThree) {
        this.idThree = idThree;
    }

    public Integer getIdTwo() {
        return idTwo;
    }

    public void setIdTwo(Integer idTwo) {
        this.idTwo = idTwo;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public void setQrfQueryId(String qrfQueryId) {
        this.qrfQueryId = qrfQueryId;
    }


    public String getQrfQueryId() {
        return qrfQueryId;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getActivity() {
        return activity;
    }

    public String getFileOneFileName() {
        return fileOneFileName;
    }

    public void setFileOneFileName(String fileOneFileName) {
        this.fileOneFileName = fileOneFileName;
    }

    public String getFileTwoFileName() {
        return fileTwoFileName;
    }

    public void setFileTwoFileName(String fileTwoFileName) {
        this.fileTwoFileName = fileTwoFileName;
    }

    public AttachmentDTO[] getAttachmentDTOArray() {
        return attachmentDTOArray;
    }

    public void setAttachmentDTOArray(AttachmentDTO[] attachmentDTOArray) {
        this.attachmentDTOArray = attachmentDTOArray;
    }

    public boolean isUserLogged() {
        return userLogged;
    }

    public void setUserLogged(boolean userLogged) {
        this.userLogged = userLogged;
    }

    public Long getFreeDiskSpace() {
        return freeDiskSpace;
    }

    public void setFreeDiskSpace(Long freeDiskSpace) {
        this.freeDiskSpace = freeDiskSpace;
    }

    public String getPossibleEmailDomains() {
        return possibleEmailDomains;
    }

    public void setPossibleEmailDomains(String possibleEmailDomains) {
        this.possibleEmailDomains = possibleEmailDomains;
    }

}
