package com.roche.dss.qtt.service;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryType;

import java.util.List;

public interface InitiateService {

	/**
	 * Returns a list of queries whose status is 'SUBM'
	 * @return List
	 */
	public List<Query> getInitiateList();
	
	/**
	 * Returns a list of queries whose criteria matches the input parameters
	 * @return List 	containing partialquerydto
     * @param countryCode		the country code of the requester
     * @param reporterTypeId    the type id of the reporter
     * @param queryTypeCode		the type id of the query
     * @param allDrugs			a boolean value to indicate if the all drugs
     * @param retrDrug			the drug retrieval name
     * @param genericDrug		the generic drug name
     */
	public List<Query> getDuplicateQueries(String countryCode,
			int reporterTypeId, QueryType.Code queryTypeCode, boolean allDrugs,
			String retrDrug, String genericDrug);
	
	/**
	 * Amends the query type of an existing query	
	 * @param queryId	the query number to add this query type to
	 * @param oldQueryTypeCode	the old query type id
	 * @param newQueryTypeCode	the new query type id
	 */
	public void amendQueryType(long queryId, QueryType.Code oldQueryTypeCode, QueryType.Code newQueryTypeCode) ;
	
	/**
	 * deletes a submitted query and all its associated details	
	 * @param queryNumber	the query number to add this query type to
	 * @param queryTypeCode	the new query type id
	 */
	public void deleteQuery ( long queryNumber, QueryType.Code queryTypeCode);

    public List<QueryType> getQueryTypes();

    public void acceptQuery(Long queryId, String queryStatusProcessed, String userName);

}
