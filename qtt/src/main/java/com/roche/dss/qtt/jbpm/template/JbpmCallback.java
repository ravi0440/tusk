package com.roche.dss.qtt.jbpm.template;

/**
 * @author zerkowsm
 *
 */
public interface JbpmCallback {

	Object execute(JbpmContext jbpmCon);

}
