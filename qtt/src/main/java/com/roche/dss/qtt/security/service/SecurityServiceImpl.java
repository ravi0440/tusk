package com.roche.dss.qtt.security.service;

import com.roche.dss.qtt.eao.NonRocheUserEAO;
import com.roche.dss.qtt.model.NonRocheUser;
import com.roche.dss.qtt.security.model.DSSUser;
import com.roche.dss.qtt.security.user.RocheUserDetails;
import com.roche.dss.qtt.security.utils.storeprocedures.DSSStoreProceduresConstants;
import com.roche.dss.qtt.security.utils.storeprocedures.user.DSSUserStoredProcedure;
import com.roche.dss.qtt.security.utils.storeprocedures.user.DSSUsersStoredProcedure;
import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author zerkowsm
 */
@Service("securityService")
public class SecurityServiceImpl implements SecurityService {

    private DataSource securityDataSource;

    @Autowired
    private NonRocheUserEAO nonRocheUserEAO;

    @Resource(name = "securityDataSource")
    public void setDataSource(DataSource securityDataSource) {
        this.securityDataSource = securityDataSource;
    }

    @Override
    public String getLoggedUserDisplayName() {
        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        if (auth != null) {
            return ((RocheUserDetails) auth.getPrincipal()).getDisplayName();
        } else {
            return null;
        }
    }

    @Override
    public String getLoggedUserName() {
        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        if (auth != null) {
            return ((RocheUserDetails) auth.getPrincipal()).getUsername();
        } else {
            return null;
        }
    }

    @Override
    public RocheUserDetails getLoggedUser() {
        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        if (auth != null) {
            return (RocheUserDetails) auth.getPrincipal();
        } else {
            return null;
        }
    }

    @Override
    public boolean isAllowedCheck(String permission) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            return false;
        }
        if (auth.getAuthorities() == null) {
            return false;
        }

        Collection<GrantedAuthority> authorities = auth.getAuthorities();

        if (authorities.contains(new GrantedAuthorityImpl(permission))) {
            return true;
        }

        return false;
    }

    @Override
    public Map<String, Object> getDSSUsers(String applicationCode, String[] permissionCodes) {

        DSSUsersStoredProcedure sproc = null;
        Map<String, Object> result =  null;
        boolean fail = false;

        try {
        	Class.forName("org.jboss.resource.adapter.jdbc.WrappedConnection");
        	sproc = new DSSUsersStoredProcedure(securityDataSource, true);
        	result = sproc.execute(applicationCode, convertSecurityPermissionsIntoDSSPermissions(permissionCodes));
        } catch (ClassNotFoundException cnfe) {
        	fail = true;//in case of lack of JBOSS api fall back to c3p0
        }
        
        if(fail) {
        	sproc = new DSSUsersStoredProcedure(securityDataSource, false);
        	result = sproc.execute(applicationCode, convertSecurityPermissionsIntoDSSPermissions(permissionCodes));
        }
        
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public DSSUser getDSSUser(String actorId) { 
        DSSUserStoredProcedure sproc = new DSSUserStoredProcedure(securityDataSource);
        List<DSSUser> result = (List<DSSUser>) sproc.execute(actorId).get(DSSStoreProceduresConstants.USER_SERVICE_GET_USER_OUT_PARAM);
        if(CollectionUtils.isNotEmpty(result)){
            return result.get(0);
        }else{
            return null;
        }
    }

    @Override
    public NonRocheUser checkNonRocheUser(String login, String password) {
        return nonRocheUserEAO.checkNonRocheUser(login, password);
    }

    private String[] convertSecurityPermissionsIntoDSSPermissions(String[] permissionCodes) {
        String[] dssPermissionCodes = new String[permissionCodes.length];

        for (int i = 0; i < permissionCodes.length; i++) {
            if (permissionCodes[i].startsWith(ROLE_PREFIX)) {
                dssPermissionCodes[i] = permissionCodes[i].substring(ROLE_PREFIX.length());
            } else {
                dssPermissionCodes[i] = permissionCodes[i];
            }
        }

        return dssPermissionCodes;
    }



}
