package com.roche.dss.qtt.model;

@Entity
@Table(name = "EU_REGULATORY_HISTORY")
public class EuRegulatoryHistory implements java.io.Serializable {

    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    @SequenceGenerator(name = "EU_REGULATORY_HISTORY_GENERATOR", sequenceName = "EU_REGULATORY_HISTORY_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EU_REGULATORY_HISTORY_GENERATOR")
    private Long id;

    @Column(name = "QUERY_SEQ", precision = 10, scale = 0)
    private Long querySeq;

    @Column(name = "FOLLOWUP_SEQ", precision = 10, scale = 0)
    private Long followupSeq;

    @Type(type="com.roche.dss.util.PersistentDateTime")
    @Column(name = "MODIFICATION_DATE", nullable = false, length = 7)
    private DateTime modificationDate;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuerySeq() {
        return querySeq;
    }

    public void setQuerySeq(Long querySeq) {
        this.querySeq = querySeq;
    }

    public Long getFollowupSeq() {
        return followupSeq;
    }

    public void setFollowupSeq(Long followupSeq) {
        this.followupSeq = followupSeq;
    }

    public DateTime getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(DateTime modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
