package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.RetrievalPeriodEAO;
import com.roche.dss.qtt.model.RetrievalPeriod;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
@Repository("retrievalPeriodEAO")
public class RetrievalPeriodEAOImpl extends AbstractQueryRelatedEAO<RetrievalPeriod> implements RetrievalPeriodEAO {
}
