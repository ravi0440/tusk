package com.roche.dss.qtt.service.impl;


import com.roche.dss.qtt.eao.EuRegulatoryHistoryEAO;
import com.roche.dss.qtt.model.EuRegulatoryHistory;
import com.roche.dss.qtt.service.EuRegulatoryHistoryService;

@Service("eaRegulatoryHistoryService")
@Transactional
public class EuRegulatoryHistoryServiceImpl implements EuRegulatoryHistoryService {

    @Autowired
    private EuRegulatoryHistoryEAO euRegulatoryHistoryEAO;

    @Override
    @Transactional
    public void saveEuRegulatoryHistory(EuRegulatoryHistory euRegulatory) {
        euRegulatoryHistoryEAO.persist(euRegulatory);
        euRegulatoryHistoryEAO.flush();
    }

}
