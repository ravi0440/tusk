package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.PerformanceMetricEAO;
import com.roche.dss.qtt.model.PerformanceMetric;

@Repository("performanceMetricEAO")
public class PerformanceMetricEAOImpl extends AbstractQueryRelatedEAO<PerformanceMetric> implements PerformanceMetricEAO {

}