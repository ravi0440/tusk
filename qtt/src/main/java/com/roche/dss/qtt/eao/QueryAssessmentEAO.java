package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.QueryAssessment;

public interface QueryAssessmentEAO extends BaseEAO<QueryAssessment> {
	
	public QueryAssessment findByQueryIdAndFollowupNumber( long queryId, String followupNUmber);
}
