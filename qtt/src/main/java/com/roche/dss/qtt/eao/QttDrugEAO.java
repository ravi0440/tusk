package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.QttDrug;

import java.util.List;


public interface QttDrugEAO extends BaseEAO<QttDrug> {
    List<String> getGenericNames();

    List<String> getRetrievalNames();
}
