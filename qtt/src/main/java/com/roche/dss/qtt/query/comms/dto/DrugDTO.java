package com.roche.dss.qtt.query.comms.dto;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public class DrugDTO {


	private boolean isFirstDrug;
    private boolean isRocheDrug;
    private String genericName;
    private String retrName;
    private String extraFirstDrugs;
    private String route;
    private String formulation;
    private String indication;
    private String dose;



    @Override
	public String toString(){
        return retrName;
    }

    /**
     * @return
     */
    public String getDose() {
        return dose;
    }

    /**
     * @return
     */
    public String getFormulation() {
        return formulation;
    }

    /**
     * @return
     */
    public String getGenericName() {
        return genericName;
    }

    /**
     * @return
     */
    public String getIndication() {
        return indication;
    }

    /**
     * @return
     */
    public boolean isFirstDrug() {
        return isFirstDrug;
    }

    /**
     * @return
     */
    public boolean isRocheDrug() {
        return isRocheDrug;
    }

    /**
     * @return
     */
    public String getRetrName() {
        return retrName;
    }

    /**
     * @return
     */
    public String getRoute() {
        return route;
    }

    /**
     * @param string
     */
    public void setDose(String string) {
        dose = string;
    }

    /**
     * @param string
     */
    public void setFormulation(String string) {
        formulation = string;
    }

    /**
     * @param string
     */
    public void setGenericName(String string) {
        genericName = string;
    }

    /**
     * @param string
     */
    public void setIndication(String string) {
        indication = string;
    }

    /**
     * @param b
     */
    public void setFirstDrug(boolean b) {
        isFirstDrug = b;
    }

    /**
     * @param b
     */
    public void setRocheDrug(boolean b) {
        isRocheDrug = b;
    }

    /**
     * @param string
     */
    public void setRetrName(String string) {
        retrName = string;
    }

    /**
     * @param string
     */
    public void setRoute(String string) {
        route = string;
    }

    /**
     * @return
     */
    public String getExtraFirstDrugs()
    {
        return extraFirstDrugs;
    }

    /**
     * @param string
     */
    public void setExtraFirstDrugs(String string)
    {
        extraFirstDrugs = string;
    }

    @Override
  	public int hashCode() {
  		final int prime = 31;
  		int result = 1;
  		result = prime * result + ((dose == null) ? 0 : dose.hashCode());
  		result = prime * result
  				+ ((extraFirstDrugs == null) ? 0 : extraFirstDrugs.hashCode());
  		result = prime * result
  				+ ((formulation == null) ? 0 : formulation.hashCode());
  		result = prime * result
  				+ ((genericName == null) ? 0 : genericName.hashCode());
  		result = prime * result
  				+ ((indication == null) ? 0 : indication.hashCode());
  		result = prime * result + (isFirstDrug ? 1231 : 1237);
  		result = prime * result + (isRocheDrug ? 1231 : 1237);
  		result = prime * result
  				+ ((retrName == null) ? 0 : retrName.hashCode());
  		result = prime * result + ((route == null) ? 0 : route.hashCode());
  		return result;
  	}

  	@Override
  	public boolean equals(Object obj) {
  		if (this == obj)
  			return true;
  		if (obj == null)
  			return false;
  		if(!(obj instanceof DrugDTO))
  			return false;
  		DrugDTO other = (DrugDTO) obj;
  		if (dose == null) {
  			if (other.dose != null)
  				return false;
  		} else if (!dose.equals(other.dose))
  			return false;
  		if (extraFirstDrugs == null) {
  			if (other.extraFirstDrugs != null)
  				return false;
  		} else if (!extraFirstDrugs.equals(other.extraFirstDrugs))
  			return false;
  		if (formulation == null) {
  			if (other.formulation != null)
  				return false;
  		} else if (!formulation.equals(other.formulation))
  			return false;
  		if (genericName == null) {
  			if (other.genericName != null)
  				return false;
  		} else if (!genericName.equals(other.genericName))
  			return false;
  		if (indication == null) {
  			if (other.indication != null)
  				return false;
  		} else if (!indication.equals(other.indication))
  			return false;
  		if (isFirstDrug != other.isFirstDrug)
  			return false;
  		if (isRocheDrug != other.isRocheDrug)
  			return false;
  		if (retrName == null) {
  			if (other.retrName != null)
  				return false;
  		} else if (!retrName.equals(other.retrName))
  			return false;
  		if (route == null) {
  			if (other.route != null)
  				return false;
  		} else if (!route.equals(other.route))
  			return false;
  		return true;
  	}
}
