/* 
====================================================================
  $Header$
  @author $Author: savovp $
  @version $Revision: 2644 $ $Date: 2013-01-21 16:08:33 +0100 (Pn, 21 sty 2013) $

====================================================================
*/
package com.roche.dss.qtt.query.web.action.oldqrf;

import com.roche.dss.qtt.model.QttCountry;
import com.roche.dss.qtt.query.comms.dto.*;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.QRFLists;
import com.roche.dss.qtt.query.web.qrf.model.QrfFormModel;
import com.roche.dss.qtt.security.user.RocheUserDetails;
import com.roche.dss.qtt.service.QrfService;
import com.roche.dss.qtt.utility.QueryStatusCodes;

import java.io.File;
import java.util.*;

public class PopulateQueryAction extends QRFAbstractAction {

    private String qAction;
    public QRFLists qrflists = new QRFLists();
    private static final Logger logger = LoggerFactory.getLogger(StdQrfAction.class);
    private RocheUserDetails userDetails;
   
   
    @Autowired
    private QrfService qrfService;

    QrfFormModel formModel = new QrfFormModel();
    private boolean msgPresent;

    @Override
    public Object getModel() {
        return formModel;
    }

    public String execute() {

        int queryId = 0;
        if ((formModel.getNo() != null) && (formModel.getNo().length() >= 0)) {
            queryId = new Long((Long.parseLong(formModel.getNo()) / 2)).intValue();
            //set the action if its through 'find partial' or 'find provisional' see StdQrfAction for details
            getRequest().put("action", formModel.getAction());
        } else if (formModel.getId() != 0) {
            queryId = formModel.getId();
            getRequest().put("action", formModel.getAction());
        }
        else if (formModel.getQrfQueryId() != null) {
            queryId = new Integer(formModel.getQrfQueryId()).intValue();
            //set the action if its through 'find partial' or 'find provisional' see StdQrfAction for details
            getRequest().put("action", "keepDoc");
        } else if ((formModel.getNo() == null) || (formModel.getNo().length() <= 0)) {
            logger.error("PopulateQueryAction: The id of the query is either NULL or 0");
            return "error";
        }
        logger.debug("PopulateQueryAction: queryId" + queryId);

        //the id(no) is encrypted by multiplying with 2 i.e. the id * 2 e.g. if the query no is 214 the "no" will be 418
        //so we divide it by 2 to get the original id.
        //QrfFacadeBD qrf = (QrfFacadeBD)BusinessDelegateFactory.getFactory().getServiceBD(mapping);
        QueryDTO dto = qrfService.openQuery(queryId, null, false);


        // create a whole bunch of lookups.

        GregorianCalendar currentDate = new GregorianCalendar();
        int currentYear = currentDate.get(Calendar.YEAR);
        int end = currentYear + 10;
        //loop till 10 years from now
        String key = "year";
        //  getSession().put(ActionConstants.YEAR, getYearsList( key, currentYear, end ) );

        //set year for cumulative drop downs
        GregorianCalendar cumulativeDate = new GregorianCalendar();
        cumulativeDate.set(Calendar.YEAR, 1970);
        int startYear = cumulativeDate.get(Calendar.YEAR);
        key = "cumulativeYear";
        //  getSession().put(ActionConstants.CUMULATIVE_YEAR, getYearsList( key, startYear, end) );

        populateRequester(dto, formModel);

        //populate preparatory if organisation type is roche_affiliate
        if (dto.getRequester().getOrg().getTypeId() == ActionConstants.ROCHE_AFFILIATE) {
            populatePreparatory(dto, formModel);
        }

        formModel.setSourceCountry(dto.getSourceCountry());
        formModel.setReporterType(dto.getReporterType());

        if (!dto.isAllDrug()) {
            formModel.setAllDrugs(0);
            populateDrugs(dto, formModel);
        } else
            formModel.setAllDrugs(1);

        //date requested
        GregorianCalendar cal = new GregorianCalendar();
        if ("reo".equals(formModel.getActivity())) {            
            cal.add(Calendar.DATE, 14);
            formModel.setMyAction("partial");
        } else {
            cal.setTime(new Date(dto.getDateRequested()));
            populateAttachments(dto, formModel);
        }
        formModel.setDateRequested(cal.getTime());

        formModel.setUrgency(dto.getUrgency());

        //do cumulative
        populateCumulative(dto, formModel);

        formModel.setQueryType(dto.getQueryType());
        formModel.setFollowUp(dto.getFollowUpNumber());
        formModel.setRequesterQueryLabel(dto.getRequesterQueryLabel());
        formModel.setId(Integer.parseInt(dto.getQueryNumber()));
        //getRequest().put("formModel", formModel);

        //set the form into session so the jsp can use it to populate data
        getSession().put("formModel", formModel);


        //set the id. as we will now need to update this query and not create a new one see StdQrfAction for details
        //getRequest().put("id", String.valueOf(dto.getQueryNumber()));       
        getSession().put(ActionConstants.QRF_EDIT_QUERY_TYPE, dto.getQueryType().name());

        formModel.setUserLogged(userDetails != null);
        setFreeDiskSpace();
        setPossibleEmailDomains();

        return "success";
    }

    private void populateRequester(QueryDTO query, QrfFormModel form) {

        form.setFirstname(query.getRequester().getFirstName());
        form.setLastname(query.getRequester().getLastName());
        form.setTelephone(query.getRequester().getTelephone());
        form.setFax(query.getRequester().getFax());
        form.setEmail(query.getRequester().getEmailAddress());
        form.setReqCountry(query.getRequester().getCountryCode());
        form.setOrgName(query.getRequester().getOrg().getName());
        form.setOrgType(query.getRequester().getOrg().getTypeId());

    }

    private void populateDrugs(QueryDTO query, QrfFormModel form) {

        DrugDTO drugDto = query.getDrug();
        form.setRetrDrug(drugDto.getRetrName());
        form.setGenericDrug(drugDto.getGenericName());
        form.setRoute(drugDto.getRoute());
        form.setFormulation(drugDto.getFormulation());
        form.setIndication(drugDto.getIndication());
        form.setDose(drugDto.getDose());
        form.setOthersDrug(drugDto.getExtraFirstDrugs());
        logger.debug("The drug details are:" + drugDto.getGenericName() + " drugDto.getRetrName()" + drugDto.getRetrName()
                + " drugDto.getRoute():" + drugDto.getRoute() + " drugDto.getFormulation():" + drugDto.getFormulation()
                + " drugDto.getIndication()" + drugDto.getIndication() + " drugDto.getDose()" + drugDto.getDose() + " query.getQuerySummary()" + query.getQuerySummary());
    }

    private void populateCumulative(QueryDTO query, QrfFormModel form) {

        CumulativePeriodDTO cummDto = query.getCumulative();

        form.setCumulative(cummDto.isCumulative() ? 1 : 0);
        form.setFrom(new Date(cummDto.getFromDate()));
        form.setTo(new Date(cummDto.getToDate()));

        form.setInterval(cummDto.getInterval());
    }

    //TODO attachments to set for formfile form object
    private void populateAttachments(QueryDTO query, QrfFormModel form) {
        List<AttachmentDTO> attachments = query.getAttachments();

        List<AttachmentDTO> attachmentsFinal = new ArrayList<AttachmentDTO>();
        List<AttachmentDTO> attachmentsOther = new ArrayList<AttachmentDTO>();

        List<AttachmentDTO> attachmentsAll = new ArrayList<AttachmentDTO>();

        if ((attachments != null) && (!attachments.isEmpty())
                /*&& !(query.getStatusCode().equals(QueryStatusCodes.REOPENED.getCode()))*/
                ) {

            Collections.sort(attachments, new Comparator<AttachmentDTO>() {
                @Override
                public int compare(AttachmentDTO o1, AttachmentDTO o2) {
                    if (o1.getId() == o2.getId()){
                        return 0;
                    }
                    return o1.getId() > (o2.getId()) ? -1 : 1;
                }
            });

            for (AttachmentDTO attachDto : attachments) {
                if (attachDto.isActivationFlag()){
                    if(query.getFollowupNumber() != null){
                        if (query.getFollowupNumber().equals(attachDto.getFollowupNumber())){
                            if (attachDto.getCategoryId()==3){
                                attachmentsFinal.add(attachDto);
                            } else {
                                attachmentsOther.add(attachDto);
                            }
                        }
                    } else {
                        //without filter for old queries
                        if (attachDto.getCategoryId()==3){
                            attachmentsFinal.add(attachDto);
                        } else {
                            attachmentsOther.add(attachDto);
                        }
                    }

                }
            }

            attachmentsAll.addAll(attachmentsFinal);
            attachmentsAll.addAll(attachmentsOther);

                int i = 0;
            for (AttachmentDTO attachDto : attachmentsAll){
                logger.debug("attachDto: "+attachDto.toString());
                //if final response category which active
                if (attachDto.isActivationFlag()){
                        switch (i) {

                        case 0:
                            form.setDescOne(attachDto.getTitle());
                            form.setIdOne(new Integer(attachDto.getId()));
                            form.getAttachmentDTOArray()[i] = attachDto;
                            //form.setFileOne(attachDto.getFileName());
                            break;
                        case 1:
                            form.setDescTwo(attachDto.getTitle());
                            form.setIdTwo(new Integer(attachDto.getId()));
                            form.getAttachmentDTOArray()[i] = attachDto;
                            //form.setFileTwo(attachDto.getFileName());
                            break;
                        case 2:
                            form.setDescThree(attachDto.getTitle());
                            form.setIdThree(new Integer(attachDto.getId()));
                            form.getAttachmentDTOArray()[i] = attachDto;
                            //form.setFileThree(attachDto.getFileName());
                            break;
                        case 3:
                            form.setDescFour(attachDto.getTitle());
                            form.setIdFour(new Integer(attachDto.getId()));
                            form.getAttachmentDTOArray()[i] = attachDto;
                            //form.setFileFour(attachDto.getFileName());
                            break;
                        case 4:
                            form.setDescFive(attachDto.getTitle());
                            form.setIdFive(new Integer(attachDto.getId()));
                            form.getAttachmentDTOArray()[i] = attachDto;
                            //form.setFileFive(attachDto.getFileName());
                            break;
                        default:
                            //there cant be more than 5 attachments for a query
                            break;
                    }//switch

                    i++;
                }
            }

        }//if

    }

    private void populatePreparatory(QueryDTO query, QrfFormModel form) {

        List prepList = query.getPreparatory();
        if ((prepList != null) && (!prepList.isEmpty())) {

            for (int i = 0; i < prepList.size(); i++) {

                PreparatoryDTO prepDto = (PreparatoryDTO) prepList.get(i);

                switch (prepDto.getPreparatoryId()) {

                    case ActionConstants.REPOSITORY:
                        form.setRepository(prepDto.getPreparatoryId());
                        break;
                    case ActionConstants.PSURS:
                        form.setPsur(prepDto.getPreparatoryId());
                        break;
                    case ActionConstants.CDS:
                        form.setCds(prepDto.getPreparatoryId());
                        break;
                    case ActionConstants.ISSUE_WORKUPS:
                        form.setIssue(prepDto.getPreparatoryId());
                        break;
                    case ActionConstants.LITERATURE:
                        form.setLiterature(prepDto.getPreparatoryId());
                        break;
                    case ActionConstants.OTHERS:
                        form.setOthers(prepDto.getPreparatoryId());
                        form.setOthersComment(prepDto.getOthers());
                        break;
                    default:
                        break;
                }//end switch
            }//end for
        }//end if

        //also set the comments
        form.setComments(query.getPrepComments());
    }

    public List<QttCountry> getOrderedCountries() {
        return qttCountryEAO.getOrderedCountries();
    }


    public List<ReporterTypeDTO> getReporterTypes() {
        return qrfService.retrieveReporterTypes();
    }

    public List<OrganisationTypeDTO> getOrganisationTypes() {
        return qrfService.retrieveOrganisationTypes();
    }

    public QRFLists getQrfLists() {
        return qrflists;
    }

    public Map<String, String> getQueryTypeUrls() {
        return qrfService.getQueryTypeUrls();
    }

	@Override
	public List<String> getDrugRetrievalNames() {
		List<String> drugs = qttDrugEAO.getRetrievalNames();
		if ("reo".equals(formModel.getActivity()) 
				&& StringUtils.isNotBlank(formModel.getRetrDrug()) 
				&& !drugs.contains(formModel.getRetrDrug())) {
			drugs.add(0, formModel.getRetrDrug());
		}
		return drugs;
	}

    @Override
    public void setUserDetails(RocheUserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public boolean isMsgPresent() {
        return msgPresent;
    }

    public void setMsgPresent(boolean msgPresent) {
        this.msgPresent = msgPresent;
    }

        /**
     * @return
     */
    public String getQAction() {
        return qAction;
    }

    /**
     * String
     */
    public void setQAction(String qAction) {
        this.qAction = qAction;
    }

    private void setFreeDiskSpace() {
        File tempDir = (File) getServletContext().getAttribute("javax.servlet.context.tempdir");
        Long freeDiskSpace = tempDir.getFreeSpace();
        formModel.setFreeDiskSpace(freeDiskSpace);
        logger.info("tempDir: {}, freeDiskSpace: {}", tempDir, freeDiskSpace);
    }

    private void setPossibleEmailDomains() {
        formModel.setPossibleEmailDomains(qrfService.retrievePossibleEmailDomains());
    }

}
