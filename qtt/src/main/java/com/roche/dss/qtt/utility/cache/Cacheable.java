/* 
====================================================================
  $Header$
  @author $Author: rahmanh1 $
  @version $Revision: 628 $ $Date: 2004-04-16 14:24:45 +0200 (Pt, 16 kwi 2004) $

====================================================================
*/
package com.roche.dss.qtt.utility.cache;

import java.io.Serializable;

/**
 * Interface denoting that implementing objects may be cached.
 * 
 * @author  Manik Surtani
 */
public interface Cacheable extends Serializable
{

    /**
     * By requiring all objects to determine their own expirations, the algorithm
     * is abstracted from the caching service, thereby providing maximum
     * flexibility since each object can adopt a different expiration strategy.
     */
    public boolean isExpired();
    /**
     * This method will ensure that the caching service is not responsible
     * for uniquely identifying objects placed into the cache.
     */
    public Object getIdentifier();

	/**
	 * This forces a timeout on any Cacheable object.
	 *
	 */
    public void timeCall();

}
