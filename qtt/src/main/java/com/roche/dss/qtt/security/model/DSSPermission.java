package com.roche.dss.qtt.security.model;

/**
 * @author zerkowsm
 *
 */
public class DSSPermission {

	private String permissionCode;
	private String permissionName;
	private String applicationCode;
	private String applicationName;
	
	public DSSPermission(String permissionCode, String permissionName){
		this.permissionCode = permissionCode;
		this.permissionName = permissionName;
	}

	public String getPermissionCode() {
		return permissionCode;
	}

	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	
}
