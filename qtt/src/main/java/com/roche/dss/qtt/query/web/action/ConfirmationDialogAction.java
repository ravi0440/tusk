package com.roche.dss.qtt.query.web.action;

/**
 * @author zerkowsm
 *
 */
public class ConfirmationDialogAction extends CommonActionSupport {

	private static final long serialVersionUID = 499352198342836992L;
	
	public static final String MEG_TO_DMG = "megToDMG";
	public static final String DMG_TO_MEG = "dmgToMeg";
	public static final String FINAL_DOC = "finalDoc";
	public static final String AFFILIATE_ALERT = "affiliateAlert";

	private String option;
	private long taskId;
    private long queryId;
	
	
	public String execute() {

        if ("mTd".equals(option)) {
            return MEG_TO_DMG;
        }
        if ("dTm".equals(option)) {
            return DMG_TO_MEG;
        }
        if ("exp".equals(option)) {
            return "exp";
        }
        if ("fDoc".equals(option)) {
            return FINAL_DOC;
        }
        if ("dsclAmnd".equals(option)) {
            return "dsclAmnd";
        }
        if ("dsclDelete".equals(option)) {
            return "dsclDelete";
        }
        if ("affiliateAlert".equals(option)) {
        	return AFFILIATE_ALERT;
        }

        return ERROR;
		
		
        // get the query details from the session
		
		
		
//        QueryDetailsDTO query = (QueryDetailsDTO) getFromSession(req,
//                QTTBaseAction.TASKLIST_QUERY_DETAIL);
//        String boxName = req.getParameter("b");
//        String queryNo = query.getNumber();
//        String queryId = String.valueOf(query.getId());
//        req.setAttribute("no", queryNo);
//        req.setAttribute("id", queryId);

        /*
        try {
            // TODO
            //QueryFacadeBD delegate = getQueryFacadeDelegate(mapping);
        	//???
        } catch (DelegateException de) {
            logDebug("Problem while updating urgency flag: " + de.getMessage(),
                    de);
            return mapping.findForward("failure");
        }*/

//        if (boxName.equals("mTd")) {
//            return mapping.findForward("megToDMG");
//        }
//        if (boxName.equals("dTm")) {
//            return mapping.findForward("dmgToMeg");
//        }
//        if (boxName.equals("fDoc")) {
//            return mapping.findForward("finalDoc");
//        }
//        if (boxName.equals("exp")) {
//            return mapping.findForward("exp");
//        }
//        if (boxName.equals("dsclAmnd")) {
//            return mapping.findForward("dsclAmnd");
//        }
//        if (boxName.equals("dsclDelete")) {
//            return mapping.findForward("dsclDelete");
//        }
//        
//        return mapping.findForward("failure");
    }
	
	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	public long getQueryId() {
		return queryId;
	}

	public void setQueryId(long queryId) {
		this.queryId = queryId;
	}
		

}
