package com.roche.dss.qtt.query.comms.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * This DTO class contains sufficient fields to transport report data   \
 * <p/>
 * User: pruchnil
 */
public class ReportDTO implements Serializable {
	
	private static final int REPORT_FIELDS = 20;
	
    private static final Logger logger = LoggerFactory.getLogger(ReportDTO.class);
    private long querySeq;
    private String queryNumber;
    private String queryLabel;
    private Date initiateDate;
    private String drugGenericName;
    private String drugRetrievalName;
    private String drugIndication;
    private String organisationType;
    private String requesterCountry;
    private String requesterLastName;
    private int turnAround;
    private String workflowRoute;
    private String queryType;
    private String reporterType;
    private String reporterCountry;
    private String queryOwner;
    private String currentStatus;
    private int daysOriginalToFinal;
    private int daysFinalToDelivery;
    private String euQPPV;

    public ReportDTO() {
        super();
    }

    public long getQuerySeq() {
        return querySeq;
    }

    public void setQuerySeq(long querySeq) {
        this.querySeq = querySeq;
    }

    public String getQueryNumber() {
        return queryNumber;
    }

    public void setQueryNumber(String queryNumber) {
        this.queryNumber = queryNumber;
    }

    public String getQueryLabel() {
        return queryLabel;
    }

    public void setQueryLabel(String queryLabel) {
        this.queryLabel = queryLabel;
    }

    public Date getInitiateDate() {
        return initiateDate;
    }

    public void setInitiateDate(Date initiateDate) {
        this.initiateDate = initiateDate;
    }

    public String getDrugGenericName() {
        return drugGenericName;
    }

    public void setDrugGenericName(String drugGenericName) {
        this.drugGenericName = drugGenericName;
    }

    public String getDrugRetrievalName() {
        return drugRetrievalName;
    }

    public void setDrugRetrievalName(String drugRetrievalName) {
        this.drugRetrievalName = drugRetrievalName;
    }

    public String getDrugIndication() {
        return drugIndication;
    }

    public void setDrugIndication(String drugIndication) {
        this.drugIndication = drugIndication;
    }

    public String getOrganisationType() {
        return organisationType;
    }

    public void setOrganisationType(String organisationType) {
        this.organisationType = organisationType;
    }

    public String getRequesterCountry() {
        return requesterCountry;
    }

    public void setRequesterCountry(String requesterCountry) {
        this.requesterCountry = requesterCountry;
    }

    public String getRequesterLastName() {
        return requesterLastName;
    }

    public void setRequesterLastName(String requesterLastName) {
        this.requesterLastName = requesterLastName;
    }

    public int getTurnAround() {
        return turnAround;
    }

    public void setTurnAround(int turnAround) {
        this.turnAround = turnAround;
    }

    public String getWorkflowRoute() {
        return workflowRoute;
    }

    public void setWorkflowRoute(String workflowRoute) {
        this.workflowRoute = workflowRoute;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getReporterType() {
        return reporterType;
    }

    public void setReporterType(String reporterType) {
        this.reporterType = reporterType;
    }

    public String getReporterCountry() {
        return reporterCountry;
    }

    public void setReporterCountry(String reporterCountry) {
        this.reporterCountry = reporterCountry;
    }

    public String getQueryOwner() {
        return queryOwner;
    }

    public void setQueryOwner(String queryOwner) {
        this.queryOwner = queryOwner;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public int getDaysOriginalToFinal() {
        return daysOriginalToFinal;
    }

    public void setDaysOriginalToFinal(int daysOriginalToFinal) {
        this.daysOriginalToFinal = daysOriginalToFinal;
    }

    public int getDaysFinalToDelivery() {
        return daysFinalToDelivery;
    }

    public void setDaysFinalToDelivery(int daysFinalToDelivery) {
        this.daysFinalToDelivery = daysFinalToDelivery;
    }

    public String getLongInitiateDate() {
        return new SimpleDateFormat("dd-MMM-yyyy", Locale.US).format(initiateDate);
    }

    public String getMediumInitiateDate() {
        return new SimpleDateFormat("MMM-yyyy", Locale.US).format(initiateDate);
    }

    public String getShortInitiateDate() {
        return new SimpleDateFormat("yyyy", Locale.US).format(initiateDate);
    }
    
	public String getEuQPPV() {
		return euQPPV;
	}
	
	public String getEuQPPVLong() {
		return euQPPV.equals("Y")?"YES":"NO";
	}

	public void setEuQPPV(String euQPPV) {
		this.euQPPV = euQPPV;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((currentStatus == null) ? 0 : currentStatus.hashCode());
		result = prime * result + daysFinalToDelivery;
		result = prime * result + daysOriginalToFinal;
		result = prime * result
				+ ((drugGenericName == null) ? 0 : drugGenericName.hashCode());
		result = prime * result
				+ ((drugIndication == null) ? 0 : drugIndication.hashCode());
		result = prime
				* result
				+ ((drugRetrievalName == null) ? 0 : drugRetrievalName
						.hashCode());
		result = prime * result
				+ ((initiateDate == null) ? 0 : initiateDate.hashCode());
		result = prime
				* result
				+ ((organisationType == null) ? 0 : organisationType.hashCode());
		result = prime * result
				+ ((queryLabel == null) ? 0 : queryLabel.hashCode());
		result = prime * result
				+ ((queryNumber == null) ? 0 : queryNumber.hashCode());
		result = prime * result
				+ ((queryOwner == null) ? 0 : queryOwner.hashCode());
		result = prime * result + (int) (querySeq ^ (querySeq >>> 32));
		result = prime * result
				+ ((queryType == null) ? 0 : queryType.hashCode());
		result = prime * result
				+ ((reporterCountry == null) ? 0 : reporterCountry.hashCode());
		result = prime * result
				+ ((reporterType == null) ? 0 : reporterType.hashCode());
		result = prime
				* result
				+ ((requesterCountry == null) ? 0 : requesterCountry.hashCode());
		result = prime
				* result
				+ ((requesterLastName == null) ? 0 : requesterLastName
						.hashCode());
		result = prime * result + turnAround;
		result = prime * result
				+ ((workflowRoute == null) ? 0 : workflowRoute.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if(!(obj instanceof ReportDTO))
			return false;
		ReportDTO other = (ReportDTO) obj;
		if (currentStatus == null) {
			if (other.currentStatus != null)
				return false;
		} else if (!currentStatus.equals(other.currentStatus))
			return false;
		if (daysFinalToDelivery != other.daysFinalToDelivery)
			return false;
		if (daysOriginalToFinal != other.daysOriginalToFinal)
			return false;
		if (drugGenericName == null) {
			if (other.drugGenericName != null)
				return false;
		} else if (!drugGenericName.equals(other.drugGenericName))
			return false;
		if (drugIndication == null) {
			if (other.drugIndication != null)
				return false;
		} else if (!drugIndication.equals(other.drugIndication))
			return false;
		if (drugRetrievalName == null) {
			if (other.drugRetrievalName != null)
				return false;
		} else if (!drugRetrievalName.equals(other.drugRetrievalName))
			return false;
		if (initiateDate == null) {
			if (other.initiateDate != null)
				return false;
		} else if (!initiateDate.equals(other.initiateDate))
			return false;
		if (organisationType == null) {
			if (other.organisationType != null)
				return false;
		} else if (!organisationType.equals(other.organisationType))
			return false;
		if (queryLabel == null) {
			if (other.queryLabel != null)
				return false;
		} else if (!queryLabel.equals(other.queryLabel))
			return false;
		if (queryNumber == null) {
			if (other.queryNumber != null)
				return false;
		} else if (!queryNumber.equals(other.queryNumber))
			return false;
		if (queryOwner == null) {
			if (other.queryOwner != null)
				return false;
		} else if (!queryOwner.equals(other.queryOwner))
			return false;
		if (querySeq != other.querySeq)
			return false;
		if (queryType == null) {
			if (other.queryType != null)
				return false;
		} else if (!queryType.equals(other.queryType))
			return false;
		if (reporterCountry == null) {
			if (other.reporterCountry != null)
				return false;
		} else if (!reporterCountry.equals(other.reporterCountry))
			return false;
		if (reporterType == null) {
			if (other.reporterType != null)
				return false;
		} else if (!reporterType.equals(other.reporterType))
			return false;
		if (requesterCountry == null) {
			if (other.requesterCountry != null)
				return false;
		} else if (!requesterCountry.equals(other.requesterCountry))
			return false;
		if (requesterLastName == null) {
			if (other.requesterLastName != null)
				return false;
		} else if (!requesterLastName.equals(other.requesterLastName))
			return false;
		if (turnAround != other.turnAround)
			return false;
		if (workflowRoute == null) {
			if (other.workflowRoute != null)
				return false;
		} else if (!workflowRoute.equals(other.workflowRoute))
			return false;
		return true;
	}
	
	public String[] toStringArray() {
		List<String> fields = new ArrayList<String>(REPORT_FIELDS);
		fields.add(queryNumber);
		fields.add(queryLabel);
		fields.add(getLongInitiateDate());
		fields.add(getMediumInitiateDate());
		fields.add(getShortInitiateDate());
		fields.add(drugRetrievalName);
		fields.add(drugGenericName);
		fields.add(drugIndication);
		fields.add(organisationType);
		fields.add(requesterCountry);
		fields.add(requesterLastName);
		fields.add(Integer.valueOf(turnAround).toString());
		fields.add(workflowRoute);
		fields.add(queryType);
		fields.add(reporterType);
		fields.add(getEuQPPVLong());
		fields.add(reporterCountry);
		fields.add(queryOwner);
		fields.add(currentStatus);
		fields.add(Integer.valueOf(daysOriginalToFinal).toString());
		fields.add(Integer.valueOf(daysFinalToDelivery).toString());
		
		
		return fields.toArray(new String[REPORT_FIELDS]);
	}

}
