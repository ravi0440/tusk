package com.roche.dss.qtt.eao.impl;

import org.springframework.stereotype.Repository;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.ManufacturingEAO;
import com.roche.dss.qtt.model.Manufacturing;

@Repository("manufacturingRecalls")
public class ManufacturingEAOImpl extends AbstractQueryRelatedEAO<Manufacturing> implements ManufacturingEAO {
}
