package com.roche.dss.qtt.query.web.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.security.model.DSSUser;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.workflow.WorkflowService;
import com.roche.dss.qtt.service.workflow.exception.TaskAlreadyCompletedException;
import com.roche.dss.qtt.utility.Activities;
import com.roche.dss.util.comparator.ResourceListComparator;

/**
 * @author zerkowsm
 *
 */
public class MedicalInterpretationAction extends TaskBaseAction {

	private static final long serialVersionUID = -6356052533908874482L;

	private static final Logger logger = LoggerFactory.getLogger(MedicalInterpretationAction.class);
		
	private Query query;
	private long queryId;
	private long taskId;
	private String taskName;
	
	private List<TaskInstance> tasks = new ArrayList<TaskInstance>();
	private List<DSSUser> resourceList = new ArrayList<DSSUser>();
	private String userForMI;
	
	private WorkflowService workflowService;	
	
    private QueryService queryService;

    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }
    
    @Resource
    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    @SuppressWarnings("unchecked")
    @SkipValidation
	public String execute() {
		
    	try {
			if (!(isMIAllowed())) {
				logger.error("Access denied user:" + getLoggedUserName());
				return DENIED;
			}
			
			query = queryService.find(queryId);
		    if (query == null) {
		    	logger.error("Could not retrieve query (number=" + queryId + ") from list of summaries");
		    	return ERROR;
		    }
		    
		    tasks = workflowService.retrieveProcessTaskList(Long.toString(query.getQuerySeq()));
		    
			if (Activities.PS_ASSIGN.getTaskName().equals(taskName)) {			
				Map<String, Object> users = securityService.getDSSUsers(QTT_DSS_APPLICATION_CODE, new String[]{PS_PERMISSION});
				resourceList = (ArrayList<DSSUser>) users.get(USER_SERVICE_GET_USERS_OUT_PARAM);
				Collections.sort(resourceList, new ResourceListComparator());
	
				return ASSIGNMENT;
	        } 
    	} catch(Exception e) {
    		logger.error(e.getMessage());
    	}
			
		return SUCCESS;
    }
    
    public String assign() {
    	
		if (!(isDscl())) {
			logger.error("Access denied user:" + getLoggedUserName());
			return DENIED;
		}
    	
		try {
			workflowService.performMIResourceAssignment(query, getLoggedUserName(), taskId, userForMI, getAssignedUserEmail(userForMI));
		} catch (TaskAlreadyCompletedException e) {
			return TASK_ALREADY_COMPLETED;
		} catch (UnsupportedOperationException e) {
			return ERROR;
		}
    	
    	return SUCCESS;    	
    }
    
    private String getAssignedUserEmail(String assignedUserName) {    	
    	for(DSSUser dssUser : resourceList) {
    		if(dssUser.getUserName().equals(assignedUserName)) {
    			return dssUser.getEmail();
    		}
    	}
    	return null;
    }
    
	public String getUserForMI() {
		return userForMI;
	}

    @RequiredStringValidator(type = ValidatorType.FIELD, key = "mi.assignment.resource.required")
	public void setUserForMI(String userForMI) {
		this.userForMI = userForMI;
	}
    
	public boolean isMedicalInterpretation() {
		return Activities.MEDICAL_INTERPRETATION_I.getTaskName().equals(taskName);
	}
	
	public boolean isMIClarification() {
		return Activities.MI_CLARIFICATION.getTaskName().equals(taskName);
	}
	
	public boolean isMIReview() {
		return Activities.MI_REVIEW.getTaskName().equals(taskName);
	}
    
	public boolean isMIDMG() {
		return Activities.MEDICAL_INTERPRETATION_II.getTaskName().equals(taskName);
	}
	
	public boolean isDateExpiredWarning() {
		DateTime queryDueDate = query.getCurrentDueDate();
		DateMidnight lastMidnight = new DateTime().toDateMidnight();
		return lastMidnight.isAfter(queryDueDate); 
	}
	
	public boolean isWorkflowRouteB() {
		return WF_ROUTE_B.equals(query.getWorkflowRouteType());
	}
	
    public Query getQuery() {
        return query;
    }
        
    public void setQuery(Query query) {
		this.query = query;
	}

	public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public List<TaskInstance> getTasks() {
		return tasks;
	}
	
	public void setResourceList(List<DSSUser> resourceList) {
		this.resourceList = resourceList;
	}
	
	public List<DSSUser> getResourceList() {
		return resourceList;
	}
	
}
