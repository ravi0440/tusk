package com.roche.dss.qtt.service.workflow.handler;

import com.roche.dss.qtt.service.workflow.WorkflowGlobalConstants;

/**
 * @author zerkowsm
 *
 */
public class QTTMIAssignmentHandler implements AssignmentHandler, WorkflowGlobalConstants {

	private static final long serialVersionUID = -6612108677487098497L;
	
	private static final Logger logger = LoggerFactory.getLogger(QTTMIAssignmentHandler.class);

	@Override
	public void assign(Assignable assignable, ExecutionContext executionContext)
			throws Exception {
		logger.debug("QTTMIAssignmentHandler");
		assignable.setActorId((String) executionContext.getContextInstance().getVariable(PS_ASSIGNED_ACTOR_ID));
	}	

}
