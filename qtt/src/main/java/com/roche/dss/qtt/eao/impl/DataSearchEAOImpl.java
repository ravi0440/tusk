package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.DataSearchEAO;
import com.roche.dss.qtt.model.DataSearch;

@Repository("datasearch")
public class DataSearchEAOImpl extends AbstractQueryRelatedEAO<DataSearch> implements DataSearchEAO {
}
