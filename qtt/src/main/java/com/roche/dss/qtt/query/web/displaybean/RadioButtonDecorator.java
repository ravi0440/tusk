package com.roche.dss.qtt.query.web.displaybean;

/**
 * @author zerkowsm
 *
 */
public class RadioButtonDecorator implements DisplaytagColumnDecorator {
	  
	public RadioButtonDecorator() {}
		   
	@Override
	public Object decorate(Object columnValue, PageContext pageContext,
			MediaTypeEnum media) throws DecoratorException {
		return "<input type=radio name=qttradio value=" + columnValue+ ">";
	}
	  
}
