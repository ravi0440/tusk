package com.roche.dss.qtt.service;

import com.roche.dss.qtt.query.web.model.QuickSearchParams;

public interface FullTextSearchService {
	public SearchResults searchDatabase(QuickSearchParams searchParams);
	
    public SearchResults searchAttachments(QuickSearchParams searchParams);
    
    public SearchResults searchBoth(QuickSearchParams searchParams);
    
    public String getFileTextContent(String filename);
    
    public void clearFileTextContent(String filename);
}
