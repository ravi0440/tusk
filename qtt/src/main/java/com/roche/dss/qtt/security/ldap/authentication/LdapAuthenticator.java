package com.roche.dss.qtt.security.ldap.authentication;

import com.roche.dss.qtt.security.model.DSSUser;

/**
 * @author zerkowsm
 *
 */
public interface LdapAuthenticator {
	
	DSSUser authenticate(Authentication authentication);

    LdapUser authenticateLdap(String login, String password, String domain);
	
}