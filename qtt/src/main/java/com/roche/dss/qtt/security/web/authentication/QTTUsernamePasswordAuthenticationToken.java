package com.roche.dss.qtt.security.web.authentication;

import java.util.Collection;

/**
 * @author zerkowsm
 *
 */
public class QTTUsernamePasswordAuthenticationToken extends UsernamePasswordAuthenticationToken {

	private static final long serialVersionUID = -8375903649875255428L;
	
	private String domain;

	public QTTUsernamePasswordAuthenticationToken(Object principal,	Object credentials, String domain) {
		super(principal,credentials);
		this.domain = domain;
	}
	
	public QTTUsernamePasswordAuthenticationToken(Object principal,	Object credentials) {
		super(principal,credentials);
	}

	public QTTUsernamePasswordAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, String domain) {
		super(principal, credentials, authorities);
		this.domain = domain;
	}

	public String getDomain() {
		return this.domain;
	}

}
