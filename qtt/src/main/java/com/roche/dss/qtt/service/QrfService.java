package com.roche.dss.qtt.service;

import com.roche.dss.qtt.model.*;
import com.roche.dss.qtt.query.comms.dto.*;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface QrfService {

    List<ReporterTypeDTO> retrieveReporterTypes();

    List<OrganisationTypeDTO> retrieveOrganisationTypes();

    Map<String, String> getQueryTypeUrls();

    List<PartialQueryDTO> getPartialQueries(String email, String qrfStatusNotSubmitted);

    void deleteOldQueryType(int id, QueryType.Code oldQueryTypeCode);

    void updateQRF(QueryDTO queryDto, int queryId, List<Integer> idsAttachmentsToRemove);

    int createQRF(QueryDTO queryDto);

    QueryDTO openQuery(int queryId, String emailAddress, boolean b);

    boolean isFinalDocExists(long queryId);

    void addDrugEvent(QueryDescDTO queryDescDTO, List list, String aeTerm, int i, String comments);

    void updateDrugEvent(QueryDescDTO queryDescDTO, List list, String aeTerm, int i, String comments);

    void updateQRFStatus(int queryId, String qrfStatusAmended);

    void createAttachments(List attachments, int queryId);

    public void addExternalAudit(QueryDescDTO queryDescDto, Audit audit, ClinicalTrial clinical, String specialRequest, int i);

    public void updateExternalAudit(QueryDescDTO queryDescDto, Audit audit, ClinicalTrial clinical, String specialRequest, int queryNumber);

    public void addPregnancy(QueryDescDTO queryDescDTO, List<CaseDTO> caseDTOList,
                             Pregnancy pregnancy, int expType, int queryNumber, String comments, int outputformat, String othercomments);

    public List<CaseOformat> getCaoTypes();

    public void addDrugInteraction(QueryDescDTO queryDescDto, List caseDtoList,
                                   int queryNumber, String addComment, List drugDtoList);

    public void updateDrugInteraction(QueryDescDTO queryDescDto, List caseDtoList,
                                      int queryNumber, String addComment, List drugDtoList);

    public List<CtrlOformat> getCtrlOformats();

    void updateCaseClarification(List list, int i, String comments, int outputformat, String othercomments);

    void addCaseClarification(List list, int i, String comments, int outputformat, String othercomments);

    void addINDUpdates(String indNumber, int i);

    void updateLiterature(QueryDescDTO dto, int i, String aeTerm);

    void addLiterature(QueryDescDTO dto, int i, String aeTerm);

    void addPerformanceMetrics(PerformanceMetric perf, int i);

    void updatePerformanceMetrics(PerformanceMetric perf, int i);

    void updateSignalDetection(QueryDescDTO dto, int i, String aeTerm);

    void addSignalDetection(QueryDescDTO dto, int i, String aeTerm);

    void updatePregnancy(QueryDescDTO queryDescDTO, List<CaseDTO> caseDTOs, String timing, String outcome, int exposuretype, Integer queryId1, String comments, int outputformat, String othercomments);

	void addManufacturing(QueryDescDTO createQueryDescDTO, Manufacturing manufacturing, ClinicalTrial clinical,
			String aeTerm, String specialRequest, Integer integer);

	void updateManufacturing(QueryDescDTO createQueryDescDTO, Manufacturing manufacturing, ClinicalTrial clinical,
			String aeTerm, String specialRequest, Integer integer);

	List<PrgyExptype> getPrgyExptypes();

	void addMedical(QueryDescDTO createQueryDescDTO, Case caze, Pregnancy pregnancy, List<Drug> drugs, String comments, Integer queryNumber);

	void updateMedical(QueryDescDTO createQueryDescDTO, Case caze, Pregnancy pregnancy, List<Drug> drugs, String comments, Integer queryNumber);

	List<CaseOformat> getActiveCaoTypes();

	void addDataSearch(QueryDescDTO queryDesc, DataSearch dataSearch,
			ClinicalTrial clinicalTrial, CaseOformat caseOformat, String other,
			String meddraTerm, String additionalComments, Integer queryNumber);

	void updateDataSearch(QueryDescDTO queryDesc, DataSearch dataSearch, 
			ClinicalTrial clinicalTrial, CaseOformat caseOformat, String other, 
			String meddraTerm, String additionalComments, Integer queryNumber);

    String retrievePossibleEmailDomains();
}
