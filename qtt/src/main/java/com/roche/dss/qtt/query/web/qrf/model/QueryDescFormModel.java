/* 
====================================================================
  $Header$
  @author $Author: rahmanh1 $
  @version $Revision: 975 $ $Date: 2004-11-16 17:20:57 +0100 (Wt, 16 lis 2004) $

====================================================================
*/
package com.roche.dss.qtt.query.web.qrf.model;

/**
 * This form bean is used by Struts to interact with forms on the QTT JSP pages.
 * @author shaikhi
 */
public class QueryDescFormModel  {
	
	private String natureofcase = null;
	private String indexcase = null;
	private String signsymp = null;
	private String investigation = null;
	private String pmh = null;
	private String conmeds = null;
	private String otherinfo = null;
	private String diagnosis = null;
	private String aeterm = null;



	
    /**
     * @return
     */
    public String getConmeds() {
        return conmeds;
    }

    /**
     * @return
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * @return
     */
    public String getIndexcase() {
        return indexcase;
    }

    /**
     * @return
     */
    public String getInvestigation() {
        return investigation;
    }

    @ExpressionValidator(message = "Your question is required.", key = "i18n.key", expression = "(((back!=null) && back.equals('true')) || (getNatureofcase()!=null && !getNatureofcase().equals('')))")                                                                                                                               
    @StringLengthFieldValidator(maxLength = "1000", key = "queryDescForm.natureofcase.maxlength.displayname")
    public String getNatureofcase() {
        return natureofcase;
    }

    /**
     * @return
     */
    public String getOtherinfo() {
        return otherinfo;
    }

    /**
     * @return
     */
    public String getPmh() {
        return pmh;
    }

    /**
     * @return
     */
    public String getSignsymp() {
        return signsymp;
    }

    @StringLengthFieldValidator(maxLength = "3000", key = "queryDescForm.conmeds.maxlength.displayname")
    public void setConmeds(String string) {
        conmeds = string;
    }

    @StringLengthFieldValidator(maxLength = "3000", key = "queryDescForm.diagnosis.maxlength.displayname")
    public void setDiagnosis(String string) {
        diagnosis = string;
    }

    @StringLengthFieldValidator(maxLength = "1000", key = "queryDescForm.indexcase.maxlength.displayname")
    public void setIndexcase(String string) {
        indexcase = string;
    }

        @StringLengthFieldValidator(maxLength = "3000", key = "queryDescForm.investigation.maxlength.displayname")
    public void setInvestigation(String string) {
        investigation = string;
    }

    /**
     * @param string
     */
    public void setNatureofcase(String string) {
        natureofcase = string;
    }

    @StringLengthFieldValidator(maxLength = "3000", key = "queryDescForm.otherinfo.maxlength.displayname")
    public void setOtherinfo(String string) {
        otherinfo = string;
    }

    @StringLengthFieldValidator(maxLength = "3000", key = "queryDescForm.pmh.maxlength.displayname")
    public void setPmh(String string) {
        pmh = string;
    }

    @StringLengthFieldValidator(maxLength = "3000", key = "queryDescForm.signsymp.maxlength.displayname")
    public void setSignsymp(String string) {
        signsymp = string;
    }

    public String getAeterm() {
        return aeterm;
    }

    /**
     * @param string
     */
    public void setAeterm(String string) {
        aeterm = string;
    }

}
