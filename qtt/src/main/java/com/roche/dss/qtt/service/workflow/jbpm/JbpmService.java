package com.roche.dss.qtt.service.workflow.jbpm;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.service.workflow.WorkflowGlobalConstants;

/**
 * @author zerkowsm
 *
 */
public interface JbpmService extends WorkflowGlobalConstants {

	void deployProcessDefinitionParFile(final File processDefinitionParFile);
	ProcessInstance createProcessInstance(final String workflowProcessName, final Query query);
	ProcessInstance createProcessInstance(final String workflowProcessName, final Map<String, Object> variables);
    ProcessInstance createNewProcessInstance(final String processDefinition);
    void setProcessVariable(final long processInstanceId, final String name, final Object value);
    ProcessInstance getProcessInstanceById(final Long processInstanceId);
	ProcessInstance getProcessInstanceByKey(final String processDefinition, final String key);
	Collection<ProcessInstance> getProcessInstancesByKey(final String processDefinition, final String key);
	void startProcessInstance(final long processInstanceId, final String userName);
	void assignTask(final long taskId, final String userName);
	void assignTask(final long taskId, final String userName, final Map<String, Object> variables);
	void assignTask(final Query query, String userName);
	void assignTask(final Query query, final String userName, final String processVariable);
    void signal(final String actorId, final Query query, final String transitionName);
	void signal(final String actorId, final long taskInstanceId, final String transitionName);
	void signal(final String actorId, final long taskInstanceId, final String transitionName, final Long processInstanceId, final Map<String, Object> variables);
	void lockTask(final String actorId, final long taskInstanceId);
	TaskInstance getTaskInstance(final long taskInstanceId);
	Collection<TaskInstance> retrieveProcessTaskList(final long processInstanceId);
	Collection<TaskInstance> retrieveProcessActiveTaskList(final long processInstanceId);
	List<TaskInstance> retrieveUserTaskList(final String userName);
	List<TaskInstance> retrieveUserTaskList(final List<String> actors);
	TaskInstance retrieveProcessTaskCurrent(final long processInstanceId);
	Long restart(final Long processInstanceId, final String userName);
	void deleteProcessInstance(final Long processInstanceId);
	void endProcessInstance(final Long processInstanceId);
	
}
