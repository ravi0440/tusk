package com.roche.dss.qtt.model;

@Entity
@Table(name = "QUERY_WORKFLOW_HISTORY")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class QueryWorkflowHistory implements java.io.Serializable {

    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    @SequenceGenerator(name = "QUERY_WORKFLOW_HISTORY_GENERATOR", sequenceName = "QUERY_WORKFLOW_HISTORY_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QUERY_WORKFLOW_HISTORY_GENERATOR")
    private Long id;

    @Type(type="com.roche.dss.util.PersistentDateTime")
    @Column(name = "START_DATE", length = 7)
    private DateTime startDate;
    
    @Type(type="com.roche.dss.util.PersistentDateTime")
    @Column(name = "END_DATE", length = 7)
    private DateTime endDate;


    @Column(name = "MODIFIED_BY")
    private String modifiedBy;
    
    @Column(name = "WORKFLOW_ID")
    private String workflowId;
    
    @Column(name = "STATUS")
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public DateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(DateTime startDate) {
		this.startDate = startDate;
	}

	public DateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(DateTime endDate) {
		this.endDate = endDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(String workflowId) {
		this.workflowId = workflowId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

  


}
