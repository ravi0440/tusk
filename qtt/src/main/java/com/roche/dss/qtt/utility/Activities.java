package com.roche.dss.qtt.utility;

/**
 * Activities in the QTT workflow process
 *
 * User: pruchnil
 */
public enum Activities {
	
	JOB_START("Job Start", "Job Start"),
    ASSIGN("Unassigned", "Unassigned"),
    DMG_ASSESS("Assign DMG", "Assign DMG"),
    PS_ASSIGN("Assign PS", "Assign PS"),
    DMG_DATA_SEARCH("DMG Data Search", "DMG Data Search"),
    MEDICAL_INTERPRETATION_I("Medical Interpretation", "Medical Interpretation"),
    MI_CLARIFICATION("MI Clarification", "M.I. Clarification"),
    MI_REVIEW("MI Review", "M.I. Review"),
    MEDICAL_INTERPRETATION_II("Medical Interpretation+DMG", "Medical Interpretation II"),
    DMG_CLARIFICATION("DMG Clarification", "DMG Clarification"),
    DMG_REVIEW("DMG Review", "DMG Review"),
    SEND_TO_REQUESTER("Ready to Send", "Ready to Send"),
    AWAITING_FOLLOWUP("Sent to Requester", "Send to Requester"),
    UNKNOWN("<unknown>", "<unknown>");

    private String taskName;
    private String taskDisplayName;

    Activities(String taskName, String taskDisplayName) {
        this.taskName = taskName;
        this.taskDisplayName = taskDisplayName;
    }

    public String getTaskName() {
        return taskName;
    }

    public String getTaskDisplayName() {
        return taskDisplayName;
    }

    public String[] getDmgActivities() {
        return new String[]{DMG_ASSESS.getTaskName(), DMG_REVIEW.getTaskName(), DMG_DATA_SEARCH.getTaskName(), DMG_CLARIFICATION.getTaskName()};
    }

    public String[] getPsActivities() {
        return new String[]{PS_ASSIGN.getTaskName(), MEDICAL_INTERPRETATION_I.getTaskName(), MEDICAL_INTERPRETATION_II.getTaskName(), MI_CLARIFICATION.getTaskName(), MI_REVIEW.getTaskName()};
    }

    public String[] getDsclActivities() {
        return new String[]{ASSIGN.getTaskName(), SEND_TO_REQUESTER.getTaskName(), AWAITING_FOLLOWUP.getTaskName()};
    }
    
    public String[] getBWorkflow(){
    	return new String[]{MEDICAL_INTERPRETATION_I.getTaskName(), MEDICAL_INTERPRETATION_II.getTaskName(), MI_CLARIFICATION.getTaskName(), MI_REVIEW.getTaskName()};
    };
}
