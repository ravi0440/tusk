package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.Query;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface QueryRelatedEAO<T> extends BaseEAO<T> {
    void removeQueryRelated(Query query);

    void removeQueryRelated(long queryId);
}
