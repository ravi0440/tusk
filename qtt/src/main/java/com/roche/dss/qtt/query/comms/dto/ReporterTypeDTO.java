package com.roche.dss.qtt.query.comms.dto;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public class ReporterTypeDTO {
    private String reporterTypeId;
    private String reporterType;

    public String getReporterTypeId() {
        return reporterTypeId;
    }

    public void setReporterTypeId(String reporterTypeId) {
        this.reporterTypeId = reporterTypeId;
    }

    public String getReporterType() {
        return reporterType;
    }

    public void setReporterType(String reporterType) {
        this.reporterType = reporterType;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((reporterType == null) ? 0 : reporterType.hashCode());
		result = prime * result
				+ ((reporterTypeId == null) ? 0 : reporterTypeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if(!(obj instanceof ReporterTypeDTO))
			return false;
		ReporterTypeDTO other = (ReporterTypeDTO) obj;
		if (reporterType == null) {
			if (other.reporterType != null)
				return false;
		} else if (!reporterType.equals(other.reporterType))
			return false;
		if (reporterTypeId == null) {
			if (other.reporterTypeId != null)
				return false;
		} else if (!reporterTypeId.equals(other.reporterTypeId))
			return false;
		return true;
	}
}
