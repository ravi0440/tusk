package com.roche.dss.qtt.query.web.action;

/**
 * @author zerkowsm
 *
 */
public class LoginAction extends ActionSupport {
	
	private static final long serialVersionUID = 4551790125303026771L;
	
	private static final String LOGIN_FAILED = "loginFailed";
	
	private boolean authenticationFailed;
	
    public String execute() {
    	return SUCCESS;
    }
    
    public String showAuthenticationEntryPoint(){
    	setAuthenticationFailed(true);
    	return LOGIN_FAILED;
    }
    
    public boolean isAuthenticationFailed() {
        return authenticationFailed;
    }

    public void setAuthenticationFailed(boolean authenticationFailed) {
        this.authenticationFailed = authenticationFailed;
    }
	
}