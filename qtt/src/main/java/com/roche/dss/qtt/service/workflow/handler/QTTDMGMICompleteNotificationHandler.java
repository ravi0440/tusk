package com.roche.dss.qtt.service.workflow.handler;

import com.roche.dss.qtt.model.AttachmentFromDmgSearch;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.comms.dto.EmailDTO;
import com.roche.dss.qtt.service.workflow.WorkflowGlobalConstants;
import com.roche.dss.qtt.utility.EmailSender;
import com.roche.dss.util.ApplicationContextProvider;
import java.util.List;
import java.util.Properties;


public class QTTDMGMICompleteNotificationHandler implements ActionHandler, WorkflowGlobalConstants {

	private static final long serialVersionUID = -775105692488742520L;

	private static final Logger logger = LoggerFactory.getLogger(QTTDMGMICompleteNotificationHandler.class);
	  
	
	@Override
	public void execute(ExecutionContext executionContext) throws Exception {
		logger.debug("QTTDMGMICompleteNotificationHandler");
		ContextInstance ctx = executionContext.getContextInstance();
        Long queryId = (Long) ctx.getVariable(PROCESS_INSTANCE_OWNER_ID);
        Session session = (Session) executionContext.getJbpmContext().getSession();
        Query query = (Query) session.get(Query.class, queryId);
        if (query != null) {
        	logger.info("sending a notification for query: " + query.getQuerySeq());
        	EmailDTO email = new EmailDTO();
        	
        	ApplicationContext appCtx = ApplicationContextProvider.ctx;
        	EmailSender emailSender = appCtx.getBean(EmailSender.class);
        	Properties properties = (Properties) appCtx.getBean("properties");
        	
        	String assignedUserEmail = (String) executionContext.getContextInstance().getVariable(PS_ASSIGNED_ACTOR_EMAIL);
        	
        	email.buildDTO_DMGMIComplete(query, assignedUserEmail, properties);
        	
            try {
				List<AttachmentFromDmgSearch> attachments = emailSender.getAttachmentsAddedInDagSearch(query);
				// send mail with attachments if attachments have not been added then send regular mail
				if (attachments != null && !attachments.isEmpty()) {
					emailSender.sendNotificationForAddedAttachmentFromDmgSearch(query, assignedUserEmail, attachments);
				} else {
					emailSender.sendMessage(email.getFrom(), email.getTo(), email.getCc(),
							email.getSubject(), email.getBody());
				}
			} catch (AddressException e) {
				logger.warn("QTTDMGMICompleteNotificationHandler AddressException", e);
			} catch (SendFailedException e) {
				logger.warn("QTTDMGMICompleteNotificationHandler SendFailedException", e);
			} catch (MessagingException e) {
				logger.warn("QTTDMGMICompleteNotificationHandler MessagingException", e);
			} catch (Exception e) {
				logger.warn("QTTDMGMICompleteNotificationHandler Exception", e);			
			}
        } else {
        	logger.warn("problem with sending a notification for query: " + queryId + ", the query is not found in the database");
        }
	
	}

}
