package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.Query;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public abstract class AbstractQueryRelatedEAO<T> extends AbstractBaseEAO<T> implements QueryRelatedEAO<T>{
    @Override
    public void removeQueryRelated(Query query) {
        getEntityManager().createQuery("delete from "+entityClass.getName()+" where query=:query").setParameter("query", query).executeUpdate();
    }

    @Override
    public void removeQueryRelated(long querySeq) {
        getEntityManager().createQuery("delete from "+entityClass.getName()+" where querySeq=:querySeq").setParameter("querySeq", querySeq).executeUpdate();
    }
}
