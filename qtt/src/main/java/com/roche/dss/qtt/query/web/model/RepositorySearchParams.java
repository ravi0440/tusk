package com.roche.dss.qtt.query.web.model;

import java.util.Date;

@Conversion()
public class RepositorySearchParams {
	
	protected String[] drugRetrievalName;
	
	protected String[] innGenericName;
	
	protected String[] indication;
	
	protected String queryType;
	
	protected String aeTermDescription;
	
	protected String queryDescriptionPrevMedicalHistory;
	
	protected String interactingDrugName;
	
	protected String[] author;
	
	protected Date periodDateFrom;
	
	protected Date periodDateTo;
	
	protected Long[] labelingDocTypeIds;
	
	protected String querySummary;
	
	protected String[] workflowRouteTypeIds;
	
	protected String[] dmgMembers;
	
	protected String[] psMembers;
	
	protected String[] dsclMembers;
	
	protected String nonExpiredResponsesOnly;
	
	protected String requesterSurname;
	
	protected String[] requesterCountryCode;
	
	public Short[] reporterTypeId;
	
	protected String[] sourceCountryCode;
	
	protected Date qryResponseDateFrom;
	
	protected Date qryResponseDateTo;
	
	protected Date qryInitiateDateFrom;
	
	protected Date qryInitiateDateTo;
	
	protected Date qryFinalDeliveryDateFrom;
	
	protected Date qryFinalDeliveryDateTo;
	
	protected String requesterQueryLabel;
	
	protected String queryNumber;
	
	protected Integer firstResult;
	
	protected Integer maxResults;
	
	protected String orderBy;
	
	protected Boolean asc;
	
	public RepositorySearchParams () {
		
	}

	public String[] getDrugRetrievalName() {
		return drugRetrievalName;
	}

	public void setDrugRetrievalName(String[] drugRetrievalName) {
		this.drugRetrievalName = drugRetrievalName;
	}

	public String[] getInnGenericName() {
		return innGenericName;
	}

	public void setInnGenericName(String[] innGenericName) {
		this.innGenericName = innGenericName;
	}

	public String[] getIndication() {
		return indication;
	}

	public void setIndication(String[] indication) {
		this.indication = indication;
	}

	public String getQueryType() {
		return queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	public String getAeTermDescription() {
		return aeTermDescription;
	}

	public void setAeTermDescription(String aeTermDescription) {
		this.aeTermDescription = aeTermDescription;
	}

	public String getQueryDescriptionPrevMedicalHistory() {
		return queryDescriptionPrevMedicalHistory;
	}

	public void setQueryDescriptionPrevMedicalHistory(
			String queryDescriptionPrevMedicalHistory) {
		this.queryDescriptionPrevMedicalHistory = queryDescriptionPrevMedicalHistory;
	}

	public String getInteractingDrugName() {
		return interactingDrugName;
	}

	public void setInteractingDrugName(String interactingDrugName) {
		this.interactingDrugName = interactingDrugName;
	}

	public String[] getAuthor() {
		return author;
	}

	public void setAuthor(String[] author) {
		this.author = author;
	}

	public Long[] getLabelingDocTypeIds() {
		return labelingDocTypeIds;
	}

	public void setLabelingDocTypeIds(Long[] labelingDocTypeIds) {
		this.labelingDocTypeIds = labelingDocTypeIds;
	}

	public String getQuerySummary() {
		return querySummary;
	}

	public void setQuerySummary(String querySummary) {
		this.querySummary = querySummary;
	}

	public String[] getWorkflowRouteTypeIds() {
		return workflowRouteTypeIds;
	}

	public void setWorkflowRouteTypeIds(String[] workflowRouteTypeIds) {
		this.workflowRouteTypeIds = workflowRouteTypeIds;
	}

	public String[] getDmgMembers() {
		return dmgMembers;
	}

	public void setDmgMembers(String[] dmgMembers) {
		this.dmgMembers = dmgMembers;
	}

	public String[] getPsMembers() {
		return psMembers;
	}

	public void setPsMembers(String[] psMembers) {
		this.psMembers = psMembers;
	}

	public String[] getDsclMembers() {
		return dsclMembers;
	}

	public void setDsclMembers(String[] dsclMembers) {
		this.dsclMembers = dsclMembers;
	}

	public String getNonExpiredResponsesOnly() {
		return nonExpiredResponsesOnly;
	}

	public void setNonExpiredResponsesOnly(String nonExpiredResponsesOnly) {
		this.nonExpiredResponsesOnly = nonExpiredResponsesOnly;
	}

	public String getRequesterSurname() {
		return requesterSurname;
	}

	public void setRequesterSurname(String requesterSurname) {
		this.requesterSurname = requesterSurname;
	}

	public String[] getRequesterCountryCode() {
		return requesterCountryCode;
	}

	public void setRequesterCountryCode(String[] requesterCountryCode) {
		this.requesterCountryCode = requesterCountryCode;
	}

	public Short[] getReporterTypeId() {
		return reporterTypeId;
	}

	public void setReporterTypeId(Short[] reporterTypeId) {
		this.reporterTypeId = reporterTypeId;
	}

	public String[] getSourceCountryCode() {
		return sourceCountryCode;
	}

	public void setSourceCountryCode(String[] sourceCountryCode) {
		this.sourceCountryCode = sourceCountryCode;
	}

	public String getRequesterQueryLabel() {
		return requesterQueryLabel;
	}

	public void setRequesterQueryLabel(String requesterQueryLabel) {
		this.requesterQueryLabel = requesterQueryLabel;
	}

	public String getQueryNumber() {
		return queryNumber;
	}

	public void setQueryNumber(String queryNumber) {
		this.queryNumber = queryNumber;
	}
	
	public Integer getFirstResult() {
		return firstResult;
	}

	public void setFirstResult(Integer firstResult) {
		this.firstResult = firstResult;
	}

	public Integer getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public Boolean getAsc() {
		return asc;
	}

	public void setAsc(Boolean asc) {
		this.asc = asc;
	}

	public Date getPeriodDateFrom() {
		return periodDateFrom;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setPeriodDateFrom(Date periodDateFrom) {
		this.periodDateFrom = periodDateFrom;
	}

	public Date getPeriodDateTo() {
		return periodDateTo;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setPeriodDateTo(Date periodDateTo) {
		this.periodDateTo = periodDateTo;
	}

	public Date getQryResponseDateFrom() {
		return qryResponseDateFrom;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setQryResponseDateFrom(Date qryResponseDateFrom) {
		this.qryResponseDateFrom = qryResponseDateFrom;
	}

	public Date getQryResponseDateTo() {
		return qryResponseDateTo;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setQryResponseDateTo(Date qryResponseDateTo) {
		this.qryResponseDateTo = qryResponseDateTo;
	}

	public Date getQryInitiateDateFrom() {
		return qryInitiateDateFrom;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setQryInitiateDateFrom(Date qryInitiateDateFrom) {
		this.qryInitiateDateFrom = qryInitiateDateFrom;
	}

	public Date getQryInitiateDateTo() {
		return qryInitiateDateTo;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setQryInitiateDateTo(Date qryInitiateDateTo) {
		this.qryInitiateDateTo = qryInitiateDateTo;
	}

	public Date getQryFinalDeliveryDateFrom() {
		return qryFinalDeliveryDateFrom;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setQryFinalDeliveryDateFrom(Date qryFinalDeliveryDateFrom) {
		this.qryFinalDeliveryDateFrom = qryFinalDeliveryDateFrom;
	}

	public Date getQryFinalDeliveryDateTo() {
		return qryFinalDeliveryDateTo;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setQryFinalDeliveryDateTo(Date qryFinalDeliveryDateTo) {
		this.qryFinalDeliveryDateTo = qryFinalDeliveryDateTo;
	}

	protected Integer stringToInteger(String str) {
		if (str == null || "".equals(str.trim())) {
			return null;
		} else {
			return new Integer(Integer.parseInt(str));
		}
	}
}
