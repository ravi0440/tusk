package com.roche.dss.qtt.query.web.utils;

import com.roche.dss.qtt.security.model.DSSUser;
import com.roche.dss.qtt.security.user.RocheUserDetails;
import com.roche.dss.qtt.security.user.RocheUserDetailsImpl;


public class UserDataInterceptor implements Interceptor {

	public static final Long testEmployeeId = 1L;
	
	public static final String testFirstName = "PAWEL";
	
	public static final String testSurname = "SIDORYK";
	
	public static final String testUserId = "SIDORYKP";
	
	public static final String testPhone = "123123123";
	
	public static final String testFax = "122";
	
	public static final String testMail = "PAWEL.SIDORYK@ROCHE.COM";
	
	public static final String testCountryCode = "CH";
	
	public static final String testSite = "BASEL";
	
    public void destroy() {
    }

    public void init() {
    }

    protected String testMode;
   
    
    public String getTestMode() {
		return testMode;
	}

	public void setTestMode(String testMode) {
		this.testMode = testMode;
	}

	public String intercept(ActionInvocation actionInvocation) throws Exception {
        RocheUserDetails userDetails = null;
        if ("true".equals(testMode)) {
        	DSSUser user = new DSSUser();
        	user.setEmployeeId(testEmployeeId);
           	user.setFirstName(testFirstName);
        	user.setLastName(testSurname);
        	user.setPhoneNumber(testPhone);
        	user.setFax(testFax);
        	user.setEmail(testMail);
        	user.setCountryCode(testCountryCode);
        	user.setSite(testSite);
            RocheUserDetailsImpl.Essence essence = new RocheUserDetailsImpl.Essence(user);
            userDetails = essence.createUserDetails();
        } else {
	        Object obj = SecurityContextHolder.getContext().getAuthentication();
	        if (null != obj) {
	            userDetails = (RocheUserDetails) SecurityContextHolder.getContext()
	                    .getAuthentication().getPrincipal();
	        }
        }

        Object action = actionInvocation.getAction();

        if (action instanceof UserDetailsAware) {
            ((UserDetailsAware) action).setUserDetails(userDetails);
        }
        return actionInvocation.invoke();
    }


}
