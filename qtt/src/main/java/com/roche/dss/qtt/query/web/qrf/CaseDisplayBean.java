/* 
====================================================================
  $Header$
  @author $Author: rahmanh1 $
  @version $Revision: 975 $ $Date: 2004-11-16 17:20:57 +0100 (Wt, 16 lis 2004) $

====================================================================
*/
package com.roche.dss.qtt.query.web.qrf;

import com.roche.dss.qtt.query.comms.dto.CaseDTO;
import com.roche.dss.qtt.query.web.qrf.model.AbstractDisplayBean;

/**
 * Display-Bean for transporting information at the web tier, relating to the
 * QRF screens.
 * @author wisbeya
 */
public class CaseDisplayBean extends AbstractDisplayBean
{

	private String aerNumber;
	private String localRefNumber;
	private String extRefNumber;
	
	public CaseDisplayBean(CaseDTO dto) {
		
		aerNumber = dto.getAerNumber();
		localRefNumber = dto.getLocalRefNumber();
		extRefNumber = dto.getExtRefNumber();
	}
	
    /**
     * @return
     */
    public String getExtRefNumber() {
        return formatStringAsHtml(extRefNumber);
    }

    /**
     * @return
     */
    public String getLocalRefNumber() {
        return localRefNumber;
    }

    /**
     * @return
     */
    public String getAerNumber() {
        return aerNumber;
    }

}
