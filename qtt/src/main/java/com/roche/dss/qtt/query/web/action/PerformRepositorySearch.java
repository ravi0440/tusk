package com.roche.dss.qtt.query.web.action;

import java.util.Date;
import java.util.Map;

import com.roche.dss.qtt.query.web.model.RepositorySearchParams;
import com.roche.dss.qtt.service.DictionaryService;
import com.roche.dss.qtt.service.RepositorySearchService;
import com.roche.dss.qtt.service.SearchResults;
import com.roche.dss.util.ApplicationUtils;

@ParentPackage("default") 
public class PerformRepositorySearch extends CommonActionSupport implements SessionAware{
	
	private static final Integer MAX_RESULTS = 100;

	private abstract class PeriodValidator{
		
		abstract protected boolean checkNegativeCondition();
		
		abstract protected String getMessage();		
		
		public void validate(){
			if(checkNegativeCondition()){
				addActionError(getMessage());
			}
		}		
	}
	
	@Autowired
	RepositorySearchService service;
	
	protected Map<String, Object> session;
	
	protected RepositorySearchParams searchParams;
	
	protected String orderBy;
	
	protected Boolean asc;
	
	protected SearchResults results;
	
	protected Integer resultPage;
	
	protected Integer numPages;
	
	protected static final Logger log = LoggerFactory.getLogger(PerformRepositorySearch.class);
	
	@Autowired
	protected DictionaryService dictionaryService;

	private Date periodFrom;

	private Date periodTo;

	private Date qryResponseFrom;

	private Date qryResponseTo;
	
	public DictionaryService getDictionary () {
		return dictionaryService;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
	
	public RepositorySearchParams getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(RepositorySearchParams searchParams) {
		this.searchParams = searchParams;
	}
	
	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public SearchResults getResults() {
		return results;
	}

	public void setResults(SearchResults results) {
		this.results = results;
	}
	
	public Integer getResultPage() {
		return resultPage;
	}

	public void setResultPage(Integer resultPage) {
		this.resultPage = resultPage;
	}

	public Integer getNumPages() {
		return numPages;
	}

	public Boolean getAsc() {
		return asc;
	}

	public void setAsc(Boolean asc) {
		this.asc = asc;
	}

	@Action(value="RepositorySearchApplyParameters"
		, results={
			@Result(name="success", type="redirect", location="/PerformRepositorySearch.action")
			, @Result(name="input", type="tiles", location="/repository_search.search")
		}
	)
	public String applyParameters() {
		Date now = new Date();
		
		periodFrom = null;
		periodTo = null;
		
		
		if (searchParams.getPeriodDateFrom() != null) {
			periodFrom = searchParams.getPeriodDateFrom();
			if (periodFrom.after(now)) {
				addActionError("Time Period 'FROM' date cannot be in the future.");
			}
		}
		if (searchParams.getPeriodDateTo() != null) {
			periodTo = searchParams.getPeriodDateTo();
			if (periodTo.after(now)) {
				addActionError("Time Period 'TO' date cannot be in the future.");
			}
			if (periodFrom != null && periodFrom.after(periodTo)) {
				addActionError("Time Period 'FROM' date cannot be later than the corresponding 'TO' date.");
			}
		}
		
		qryResponseFrom = null;
		qryResponseTo = null;
		
		if (searchParams.getQryResponseDateFrom() != null) {
			qryResponseFrom = searchParams.getQryResponseDateFrom();
			if (qryResponseFrom.after(now)) {
				addActionError("Query Response 'FROM' date cannot be in the future.");
			}
		}
		if (searchParams.getQryResponseDateTo() != null) {
			qryResponseTo = searchParams.getQryResponseDateTo();
			if (qryResponseTo.after(now)) {
				addActionError("Query Response 'TO' date cannot be in the future.");
			}
			if (qryResponseFrom != null && qryResponseFrom.after(qryResponseTo)) {
				addActionError("Query Response 'FROM' date cannot be later than the corresponding 'TO' date.");
			}
		}
				
		initializePagination();
				
		if (hasActionErrors()) {
			return INPUT;
		}
		
		session.put(ApplicationUtils.SESSION_REPOSITORY_SEARCH_PARAMS, searchParams);
		
		return SUCCESS;
	}
	
	@Action(value="PerformRepositorySearch"
		, results={
			@Result(name="success", type="tiles", location="/repository_search.list")
		}
	)
	public String showResults() {
		searchParams = (RepositorySearchParams) session.get(ApplicationUtils.SESSION_REPOSITORY_SEARCH_PARAMS);
		if (searchParams == null) {
			log.warn("Unable to sort repository search results, unknown searchParams");
			return SUCCESS;
		}
		
		initializePagination();
		
		results = service.search(searchParams);
		
		numPages = 1;
		if (results.getActualResultSize() != 0) {
			numPages = results.getResultSize() % results.getActualResultSize() != 0
					? results.getResultSize() / results.getActualResultSize() + 1
					: results.getResultSize() / results.getActualResultSize();
		}
		orderBy = searchParams.getOrderBy();
		asc = searchParams.getAsc();
		
		return SUCCESS;
	}

	private void initializePagination() {
		if (resultPage == null) {
			resultPage = Integer.valueOf(1);
		}
		searchParams.setOrderBy(orderBy);
		searchParams.setAsc(asc);
		searchParams.setFirstResult(MAX_RESULTS * (resultPage - 1));
		searchParams.setMaxResults(MAX_RESULTS);
	}
}
