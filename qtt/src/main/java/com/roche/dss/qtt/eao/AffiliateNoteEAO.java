package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.AffiliateNote;

/**
 * User: pruchnil
 */
public interface AffiliateNoteEAO extends QueryRelatedEAO<AffiliateNote> {
}
