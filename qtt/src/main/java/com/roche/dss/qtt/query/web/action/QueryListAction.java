package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.query.comms.dto.QueryListDTO;
import com.roche.dss.qtt.service.QueryService;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This action class presents list of queries
 * <p/>
 * User: pruchnil
 */
public class QueryListAction extends CommonActionSupport {
    private static final Logger logger = LoggerFactory.getLogger(QueryListAction.class);
    private List<QueryListDTO> queryList = new ArrayList<QueryListDTO>();
    private String email;
    private int sort = 0;
    private int last = 0;    

    private QueryService queryService;

    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Override
    @SkipValidation
    public String execute() throws Exception {
        return SUCCESS;
    }

    public String showQueryList() {
        queryList = queryService.getQueryList(email, Sort.QUERYSEQ.toString(), "asc");
        return SUCCESS;
    }

    @SkipValidation
    public String showOwnQueryList() {
        if(getLoggedUser()!=null){
            email = getLoggedUser().getMail();
        }else if(getSession().get("email")!=null){
            email = (String) getSession().get("email");
        }else{
            return OTHER;
        }
        logger.debug("email " + email);
        return showQueryList();
    }

    @SkipValidation
    public String sortQueryList() {
        queryList = queryService.getQueryList(email, (Sort.values()[sort]).toString(), "asc");
        last = sort;
        return SUCCESS;
    }

    public String getEmail() {
        return email;
    }

    @RequiredStringValidator(type = ValidatorType.FIELD, key = "email.required")
    @EmailValidator(type = ValidatorType.FIELD, key = "email.invalid")
    public void setEmail(String email) {
        this.email = email;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getLast() {
        return last;
    }

    public void setLast(int last) {
        this.last = last;
    }

    public List getQueryList() {
        return queryList;
    }
    

    public Map<String, Object> getSession(){
        return ActionContext.getContext().getSession();
    }

    public enum Sort {
        QUERYSEQ("q.querySeq"), DRUG("d.drugRetrievalName"), TYPE("q.queryType.queryType"), SUBMITDATE("q.datetimeSubmitted"),
        RESPONSEDATE("q.currentDueDate"), STATUS("q.queryStatusCode.status");

        private String column;

        Sort(String column) {
            this.column = column;
        }

        @Override
        public String toString() {
            return column;
        }
    }

}
