package com.roche.dss.qtt.utility;

/**
 * @author zerkowsm
 *
 */
public enum Transitions {
	//TODO add all and adjust names into more proper 
	
	REQUEST_DMG_DATA_SEARCH("request a DMG datasearch"),
	COMPLETE_MI_AND_DMG("for mi and dmg"),
	MI_ASSIGN_DMG("assign someone for dmg"),
	COMPLETE_MI_AND_DMG_SEARCH("MI dmg search complete"),
	COMPLETE_DMG_SEARCH_DATA("complete DMG search"),
	COMPOSE_QUERY_SUMMARY("compose a query summary (submit to DSCL)"),
	SEEK_CLARIFICATION("seek clarification"),
	REQUEST_REVIEW("request review"),	
	TO_QUERY_ASSIGNMENT("to Query assignment"),
	CLOSE("close");

    private String transitionName;

    Transitions(String transitionName) {
        this.transitionName = transitionName;
    }

    public String getTransitionName() {
        return transitionName;
    }

}