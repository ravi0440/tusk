package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.DrugEAO;
import com.roche.dss.qtt.model.Drug;
import com.roche.dss.qtt.model.Query;
import java.util.List;

/**
 * User: pruchnil
 */
@Repository("drugEAO")
public class DrugEAOImpl extends AbstractQueryRelatedEAO<Drug> implements DrugEAO {

    @Override
    public Drug findDrugForQueryPreview(long querySeq) {
        return getEntityManager().createNamedQuery("drug.findByQuerySeqFlags", Drug.class).setParameter("firstFlag", 'Y').setParameter("rocheDrugFlag", 'Y').setParameter("querySeq", querySeq).getSingleResult();
    }

    @Override
    public void removeFlaggedQueryDrugs(Query query, Character firstFlag) {
           getEntityManager().createQuery("delete from Drug where query=:query and firstFlag=:firstFlag")
                   .setParameter("query", query).setParameter("firstFlag", firstFlag).executeUpdate();
    }

    @Override
    public List<String> getGenericNames() {
         return getEntityManager().createNamedQuery("list.genericNames", String.class).getResultList();
    }

    @Override
    public List<String> getRetrievalNames() {
        return getEntityManager().createNamedQuery("list.drugRetrNames", String.class).getResultList();
    }

    @Override
    public List<Drug> findAllByQueryId(long queryId) {
        return getEntityManager().createNamedQuery("drug.findByQuerySeq", Drug.class).setParameter("querySeq", queryId).getResultList();
    }
}
