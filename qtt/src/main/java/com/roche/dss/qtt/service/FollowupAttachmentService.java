package com.roche.dss.qtt.service;

import com.roche.dss.qtt.model.FollowupAttachment;

/**
 * User: pruchnil
 */
public interface FollowupAttachmentService {
    void openFollowupAttachment(long queryId);
    void closeFollowupAttachment(long queryId);
    void persist(FollowupAttachment entity);
    FollowupAttachment merge(FollowupAttachment entity);
    void flush();
}
