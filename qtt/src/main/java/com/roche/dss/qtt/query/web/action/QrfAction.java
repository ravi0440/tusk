/**
 *
 */
package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.eao.QttDrugEAO;
import com.roche.dss.qtt.model.QttCountry;
import com.roche.dss.qtt.query.comms.dto.OrganisationTypeDTO;
import com.roche.dss.qtt.query.comms.dto.ReporterTypeDTO;
import com.roche.dss.qtt.query.web.action.oldqrf.QRFAbstractAction;
import com.roche.dss.qtt.query.web.qrf.QRFLists;
import com.roche.dss.qtt.query.web.qrf.model.QrfFormModel;
import com.roche.dss.qtt.security.user.RocheUserDetails;
import com.roche.dss.qtt.service.QrfService;
import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

/**
 * @author tomasinp
 */
public class QrfAction extends QRFAbstractAction {

    private static final Logger logger = LoggerFactory.getLogger(QrfAction.class);

    private static final long serialVersionUID = 1L;

    QrfFormModel formModel = new QrfFormModel();

    private RocheUserDetails userDetails;
    public QRFLists qrflists = new QRFLists();

    @Autowired
    private QrfService qrfService;
    @Autowired
    private QttDrugEAO drugEAO;

    private boolean msgPresent;


    @Override
    public Object getModel() {
        return formModel;
    }

    @Override
    public String execute() throws Exception {
        if (userDetails != null) {
            formModel.setUserLogged(true);
            formModel.setFirstname(userDetails.getFirstName());
            formModel.setLastname(userDetails.getSurname());
            formModel.setTelephone(userDetails.getPhone());
            formModel.setFax(userDetails.getFax());
            formModel.setEmail(userDetails.getMail());
            // change if ET or ETH TODO this is hack because of data inconsistency with security module
            if(userDetails.getCountryCode().equals("ET")){
            	  formModel.setReqCountry("EG");
            }else if(userDetails.getCountryCode().equals("ETH")){
            	  formModel.setReqCountry("ET");
            }else if(userDetails.getCountryCode().equals("IND")){
          	  formModel.setReqCountry("IN");
            }else {
            	  formModel.setReqCountry(userDetails.getCountryCode());
            }
            formModel.setOrgName(userDetails.getDepartment() + " " + userDetails.getSite());
        } else {
            formModel.setUserLogged(false);
        }
        setFreeDiskSpace();
        setPossibleEmailDomains();
        GregorianCalendar currentDate = new GregorianCalendar();
        int currentYear = currentDate.get(Calendar.YEAR);
        currentDate.add(Calendar.DATE, 14);
        formModel.setDateRequested(currentDate.getTime());

        cleanUpSession();
        return Action.SUCCESS;
    }


    public List<QttCountry> getOrderedCountries() {
        return qttCountryEAO.getOrderedCountries();
    }

    public List<ReporterTypeDTO> getReporterTypes() {
        return qrfService.retrieveReporterTypes();
    }

    public List<OrganisationTypeDTO> getOrganisationTypes() {
        return qrfService.retrieveOrganisationTypes();
    }

    public QRFLists getQrfLists() {
        return qrflists;
    }


    public Map<String, String> getQueryTypeUrls() {
        return qrfService.getQueryTypeUrls();
    }

    @Override
    public void setUserDetails(RocheUserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public boolean isMsgPresent() {
        return msgPresent;
    }


    public void setMsgPresent(boolean msgPresent) {
        this.msgPresent = msgPresent;
    }

    @Override
	public RocheUserDetails getUserDetails() {
        return userDetails;
    }

    private void setFreeDiskSpace() {
        File tempDir = (File) getServletContext().getAttribute("javax.servlet.context.tempdir");
        Long freeDiskSpace = tempDir.getFreeSpace();
        formModel.setFreeDiskSpace(freeDiskSpace);
        logger.info("tempDir: {}, freeDiskSpace: {}", tempDir, freeDiskSpace);
    }

    private void setPossibleEmailDomains() {
        formModel.setPossibleEmailDomains(qrfService.retrievePossibleEmailDomains());
    }
}
