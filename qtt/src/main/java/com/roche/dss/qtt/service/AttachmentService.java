package com.roche.dss.qtt.service;

import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.AttachmentFromDmgSearch;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.web.model.FileUploadBean;

import java.io.IOException;
import java.util.List;

/**
 * This service is responsible for actions on attachments
 * User: pruchnil
 */
public interface AttachmentService {
    void changeActivationFlag(long id, long queryId, boolean b);

    Attachment getFileFromRepo(long attachId, long queryId) throws Exception;

    Attachment addFileToRepo(long queryId, FileUploadBean uploadFile, String userName, boolean affiliate) throws IOException;

    void persist(Attachment entity);
    Attachment merge(Attachment entity);
    void remove(Attachment entity);
    void flush();
    Attachment find(Long id);

    void saveAttachmentFromDmg(Query query, Attachment addedAttachment);
    List<AttachmentFromDmgSearch> getAllAttachmentsFromDmgSearchbyTaskInstanceId(long taskInstanceId);
}
