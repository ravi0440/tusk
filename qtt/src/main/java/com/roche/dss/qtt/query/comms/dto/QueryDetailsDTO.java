package com.roche.dss.qtt.query.comms.dto;

import java.util.Date;
import java.util.List;

/**
 *  This DTO isn't needed anymore - entity used instead of this class
 *  To remove.
 *
 * User: pruchnil
 */
public class QueryDetailsDTO {

    private long querySeq;
    private String queryNumber;
    private String followupNumber;
    private String requesterName;
    private String drugName;
    private Date expiryDate;
    private Date responseDate;
    private String queryLabel;
    private String queryType;
    private boolean affiliateAccess;

    private List attachmentFollowups;
    private List followupList;
    private List attachments;
    private List notes;

    public long getQuerySeq() {
        return querySeq;
    }

    public void setQuerySeq(long querySeq) {
        this.querySeq = querySeq;
    }

    public String getQueryNumber() {
        return queryNumber;
    }

    public String getFollowupNumber() {
        return followupNumber;
    }

    public void setFollowupNumber(String followupNumber) {
        this.followupNumber = followupNumber;
    }

    public void setQueryNumber(String queryNumber) {
        this.queryNumber = queryNumber;
    }

    public String getRequesterName() {
        return requesterName;
    }

    public void setRequesterName(String requesterName) {
        this.requesterName = requesterName;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public String getQueryLabel() {
        return queryLabel;
    }

    public void setQueryLabel(String queryLabel) {
        this.queryLabel = queryLabel;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public boolean isAffiliateAccess() {
        return affiliateAccess;
    }

    public void setAffiliateAccess(boolean affiliateAccess) {
        this.affiliateAccess = affiliateAccess;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (affiliateAccess ? 1231 : 1237);
		result = prime * result
				+ ((drugName == null) ? 0 : drugName.hashCode());
		result = prime * result
				+ ((expiryDate == null) ? 0 : expiryDate.hashCode());
		result = prime * result
				+ ((followupNumber == null) ? 0 : followupNumber.hashCode());
		result = prime * result
				+ ((queryLabel == null) ? 0 : queryLabel.hashCode());
		result = prime * result
				+ ((queryNumber == null) ? 0 : queryNumber.hashCode());
		result = prime * result + (int) (querySeq ^ (querySeq >>> 32));
		result = prime * result
				+ ((queryType == null) ? 0 : queryType.hashCode());
		result = prime * result
				+ ((requesterName == null) ? 0 : requesterName.hashCode());
		result = prime * result
				+ ((responseDate == null) ? 0 : responseDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof QueryDetailsDTO))
			return false;
		QueryDetailsDTO other = (QueryDetailsDTO) obj;
		if (affiliateAccess != other.affiliateAccess)
			return false;
		if (drugName == null) {
			if (other.drugName != null)
				return false;
		} else if (!drugName.equals(other.drugName))
			return false;
		if (expiryDate == null) {
			if (other.expiryDate != null)
				return false;
		} else if (!expiryDate.equals(other.expiryDate))
			return false;
		if (followupNumber == null) {
			if (other.followupNumber != null)
				return false;
		} else if (!followupNumber.equals(other.followupNumber))
			return false;
		if (queryLabel == null) {
			if (other.queryLabel != null)
				return false;
		} else if (!queryLabel.equals(other.queryLabel))
			return false;
		if (queryNumber == null) {
			if (other.queryNumber != null)
				return false;
		} else if (!queryNumber.equals(other.queryNumber))
			return false;
		if (querySeq != other.querySeq)
			return false;
		if (queryType == null) {
			if (other.queryType != null)
				return false;
		} else if (!queryType.equals(other.queryType))
			return false;
		if (requesterName == null) {
			if (other.requesterName != null)
				return false;
		} else if (!requesterName.equals(other.requesterName))
			return false;
		if (responseDate == null) {
			if (other.responseDate != null)
				return false;
		} else if (!responseDate.equals(other.responseDate))
			return false;
		return true;
	}
}
