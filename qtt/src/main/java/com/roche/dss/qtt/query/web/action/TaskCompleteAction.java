package com.roche.dss.qtt.query.web.action;

import javax.annotation.Resource;

import com.roche.dss.qtt.service.workflow.WorkflowService;
import com.roche.dss.qtt.service.workflow.exception.TaskAlreadyCompletedException;

/**
 * @author zerkowsm
 *
 */
public class TaskCompleteAction extends CommonActionSupport {

	private static final long serialVersionUID = -5716341387391601592L;
	
    private String nextActivity;
    private String lock;
    private long taskId;

	private WorkflowService workflowService;

    @Resource
    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

	public String execute()	{		
		
		try {
			workflowService.completeActivity(getLoggedUserName(), taskId, nextActivity);
		} catch (TaskAlreadyCompletedException e) {
			return TASK_ALREADY_COMPLETED;
		} catch (UnsupportedOperationException e) {
			return ERROR;
		}
		
		return SUCCESS;
		
//		String userId = getDSSUserObject( req ).getUserName();
//		
//		// retrieve (optional) nextActivity parameter & translate into transition ID
//		String transitionId = null;
//		String nextActivity = req.getParameter( "nextActivity" );
//		boolean isLockNeeded = Boolean.valueOf(req.getParameter("lock")).booleanValue(); 
//		if ( nextActivity != null )
//		{
//			transitionId = this.translateNextActivity( nextActivity );
//		}
//		// retrieve query detail from session
//		QuerySummaryDTO dto = (QuerySummaryDTO)this.getFromSession( req, QTTBaseAction.TASKLIST_QUERY_DETAIL );
//		if ( dto == null )
//		{
//			dto = (QuerySummaryDTO)this.getFromSession( req, QTTBaseAction.REASSIGNMENT_QUERY_DETAIL );
//		}
//		
//		DSSTaskReference taskRef = dto.getTask().getTaskReference();
//		
//		try {
//			//QttWorkflowFacadeBD delegate = getQttWorkflowFacadeDelegate( mapping );
//			//if the token is valid 
//				if(isLockNeeded)
//				{
//					qttWorkflowService.completeActivity(userId, taskRef, transitionId, true);
//				}
//				else 
//				{
//					qttWorkflowService.completeActivity( userId, 
//										   taskRef, 
//										   transitionId, 
//										   false );
//				}
//				logInfo( "Task successfully completed by:" + userId + " for query:" + dto.getNumber() + " transitionId:" +transitionId);
//			}
//
//		catch ( ActivityAlreadyCompletedException aace ) {
//			logWarn( "WARNING!! Activity has already completed - redirecting to warning page!" );
//			return mapping.findForward( "alreadyCompleted" );
//		}
//		// ############ REMOVE THIS CATCH-BLOCK ONCE WORKFLOW PROVIDES CORRECT ERROR CODE !! ###########
//		catch ( ServiceException de ) {
//			logWarn( "WARNING!! Activity has already completed - redirecting to warning page!" );
//			return mapping.findForward( "alreadyCompleted" );
//		}
//		// ############ end ###########
//		catch ( Exception e )
//		{
//			logError( "Error occurred while signalling completion of task: " 
//					  + e.getMessage(), e );
//			return mapping.findForward( "failure" );
//		}
//		
//		this.doSessionCleanup( req.getSession(), QTTBaseAction.TASKLIST_PREFIX );
//
//		return mapping.findForward( "success" );
		
	}
	
	
	
//	private String translateNextActivity( String nextActivity )
//	{
//		if ( nextActivity == null )
//		{
//			return null;
//		}
//		
//		String retVal = Transitions.getWorkflowNameFromWebName( nextActivity );	
//		return retVal;
//	}

	public String getNextActivity() {
		return nextActivity;
	}

	public void setNextActivity(String nextActivity) {
		this.nextActivity = nextActivity;
	}

	public String getLock() {
		return lock;
	}

	public void setLock(String lock) {
		this.lock = lock;
	}

	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

}
