package com.roche.dss.qtt.model;
// Generated 2010-10-04 12:00:38 by Hibernate Tools 3.3.0.GA

/**
 * FuClinicalTrial generated by hbm2java
 */
@Entity
@Table(name = "FU_CLINICAL_TRIALS")
public class FuClinicalTrial implements java.io.Serializable {

	private static final long serialVersionUID = 1812284525545898219L;
	
	private FuClinicalTrialId id;
	private String caseSelection;
	private String protocolNumber;
	private String patientNumber;
	private Long ctrlOformatId;
	private String crtnNumber;

	public FuClinicalTrial() {
	}

	public FuClinicalTrial(FuClinicalTrialId id) {
		this.id = id;
	}

    public FuClinicalTrial(ClinicalTrial trial, long followupSeq) {
        this.id = new FuClinicalTrialId(followupSeq, trial.getQuerySeq());
		this.caseSelection = trial.getCaseSelection();
		this.protocolNumber = trial.getProtocolNumber();
		this.patientNumber = trial.getPatientNumber();
		this.ctrlOformatId = trial.getCtrlOformat()!=null ? trial.getCtrlOformat().getCtrlOformatId() : null;
		this.crtnNumber = trial.getCrtnNumber();
    }

    @EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "followupSeq", column = @Column(name = "FOLLOWUP_SEQ", nullable = false, precision = 10, scale = 0)),
			@AttributeOverride(name = "querySeq", column = @Column(name = "QUERY_SEQ", nullable = false, precision = 10, scale = 0)) })
	public FuClinicalTrialId getId() {
		return this.id;
	}

	public void setId(FuClinicalTrialId id) {
		this.id = id;
	}

	@Column(name = "CASE_SELECTION", length = 600)
	public String getCaseSelection() {
		return this.caseSelection;
	}

	public void setCaseSelection(String caseSelection) {
		this.caseSelection = caseSelection;
	}

	@Column(name = "PROTOCOL_NUMBERS", length = 100)
	public String getProtocolNumber() {
		return this.protocolNumber;
	}

	public void setProtocolNumber(String protocolNumber) {
		this.protocolNumber = protocolNumber;
	}

	@Column(name = "PATIENT_NUMBERS", length = 300)
	public String getPatientNumber() {
		return this.patientNumber;
	}

	public void setPatientNumber(String patientNumber) {
		this.patientNumber = patientNumber;
	}

	@Column(name = "CTRL_OFORMAT_ID", precision = 10, scale = 0)
	public Long getCtrlOformatId() {
		return this.ctrlOformatId;
	}

	public void setCtrlOformatId(Long ctrlOformatId) {
		this.ctrlOformatId = ctrlOformatId;
	}

	@Column(name = "CRTN_NUMBERS", length = 300)
	public String getCrtnNumber() {
		return this.crtnNumber;
	}

	public void setCrtnNumber(String crtnNumber) {
		this.crtnNumber = crtnNumber;
	}

}
