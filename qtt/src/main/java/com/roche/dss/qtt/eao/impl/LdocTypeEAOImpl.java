package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.LdocTypeEAO;
import com.roche.dss.qtt.model.LdocType;

/**
 * @author zerkowsm
 *
 */
@Repository("ldocTypeEAO")
public class LdocTypeEAOImpl extends AbstractQueryRelatedEAO<LdocType> implements LdocTypeEAO {

}