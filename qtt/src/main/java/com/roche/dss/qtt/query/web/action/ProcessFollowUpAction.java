package com.roche.dss.qtt.query.web.action;

import javax.annotation.Resource;

import com.roche.dss.qtt.service.workflow.WorkflowService;
import com.roche.dss.qtt.service.workflow.exception.TaskAlreadyBegunException;
import com.roche.dss.qtt.service.workflow.exception.TaskAlreadyCompletedException;

/**
 * @author zerkowsm
 *
 */
public class ProcessFollowUpAction extends CommonActionSupport {
	
	private static final long serialVersionUID = -8478658838624447601L;

	private static final Logger logger = LoggerFactory.getLogger(ProcessFollowUpAction.class);
	
	private long queryId;
	private long taskId;
	
	private WorkflowService workflowService;	
	
    @Resource
    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }
	
	public String execute() {
		
		if (!isDscl()) {
			logger.error("Access denied user:" + getLoggedUserName());
			return DENIED;
		}

		try {
			workflowService.createFollowup(queryId, getLoggedUserName(), taskId);
		} catch (TaskAlreadyBegunException e) {
			return TASK_ALREADY_BEGUN;
		} catch (TaskAlreadyCompletedException e) {
			return TASK_ALREADY_COMPLETED;
		} catch (UnsupportedOperationException e) {
			return ERROR;
		}
				
		return SUCCESS;
	}

	public long getQueryId() {
		return queryId;
	}
	
	public void setQueryId(long queryId) {
		this.queryId = queryId;
	}

	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}	
	
}