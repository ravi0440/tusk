package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.Organisation;

public interface OrganisationEAO extends BaseEAO<Organisation>{
	
}