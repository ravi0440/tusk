package com.roche.dss.qtt.service;

import com.roche.dss.qtt.query.web.model.RepositorySearchParams;

public interface RepositorySearchService {
	public SearchResults search(RepositorySearchParams searchParams);
}
