package com.roche.dss.qtt.service.impl;

import java.util.Collection;

import com.roche.dss.qtt.eao.AeTermEAO;
import com.roche.dss.qtt.model.AeTerm;
import com.roche.dss.qtt.service.AeTermService;

@Service("aeTermService")
@Transactional
public class AeTermServiceImpl implements AeTermService {
	@Autowired
	protected AeTermEAO eao;
	
	@Override
	public void persist(AeTerm term) {
		eao.persist(term);
	}

	@Override
	public void persist(Collection<AeTerm> terms) {
		eao.persist(terms);
	}

	@Override
	public AeTerm merge(AeTerm term) {
		return eao.merge(term);
	}

	@Override
	public void remove(AeTerm term) {
		eao.remove(term);
	}

	@Override
	public void flush() {
		eao.flush();
	}

	@Override
	public void clear() {
		eao.clear();
	}

	@Override
	public AeTerm find(Long id) {
		return eao.find(id);
	}

	@Override
	public AeTerm getReference(Long id) {
		return eao.getReference(id);
	}

	@Override
	public AeTerm get(Long id) {
		return eao.get(id);
	}

}
