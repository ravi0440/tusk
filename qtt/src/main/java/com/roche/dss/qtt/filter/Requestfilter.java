package com.roche.dss.qtt.filter;

import com.roche.dss.qtt.utility.HTMLEntity;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by farynam on 01/05/17.
 */
public class Requestfilter implements Filter{


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public static String getCharacters(int from, int to, char [] array) {
        StringBuilder sb = new StringBuilder("");
        int max = Math.min(array.length, to);
        for (int i = from; i < max; i++) {
            sb.append(array[i]);
        }
        return sb.toString();
    }

    public static String [] filter(String [] in) {
        if (in == null) {
            return null;
        }

        String [] out = new String [in.length];

        for (int n =  0 ; n < in.length; n++) {
            String text = in[n];
            char [] chars = text.toCharArray();
            StringBuilder newText = new StringBuilder();
            for (int i = 0; i < chars.length; i++) {

                if (chars[i] == '&') {
                    String toCheck = getCharacters(i, i + 10, chars).toLowerCase();
                    int save = i;
                    for (int t = 0; t < HTMLEntity.ISO_8859_1.length; t+=2) {
                        String entity = HTMLEntity.ISO_8859_1[t];
                        int index = toCheck.indexOf(entity);
                        if (index == 0) {
                            i += entity.length() - 1;
                            newText.append(HTMLEntity.ISO_8859_1[t + 1]);
                            break;
                        }
                    }

                    if (save == i) {
                        newText.append(chars[i]);
                    }
                } else {
                    newText.append(chars[i]);
                }
            }
            out[n] = newText.toString();
        }

        return out;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        Map<String,Object> oldMap =  servletRequest.getParameterMap();

        if (oldMap.size() == 0) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        final Map<String,Object> filteredMap = new HashMap<String, Object>();

        Iterator<Map.Entry<String, Object>> it = oldMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = it.next();
            filteredMap.put(entry.getKey(), filter((String [])entry.getValue()));
        }

        final Map<String,Object> filteredMapUn = Collections.unmodifiableMap(filteredMap);

        HttpServletRequest filteredRequest = new HttpServletRequestWrapper((HttpServletRequest) servletRequest) {
            @Override
            public String getParameter(String name) {
                String [] val = getParameterValues(name);

                if (val == null) {
                    return null;
                }

                return val[0];
            }

            @Override
            public Map getParameterMap() {
                return filteredMapUn;
            }

            @Override
            public String[] getParameterValues(String name) {
                Object values =  filteredMapUn.get(name);

                if (values == null) {
                    return null;
                }

                if (! (values.getClass().isArray() )) {
                    return new String [] {(String)values};
                }
                return (String[])values;
            }
        };

        filterChain.doFilter(filteredRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
