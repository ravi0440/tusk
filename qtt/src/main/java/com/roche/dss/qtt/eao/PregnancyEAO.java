package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.Pregnancy;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface PregnancyEAO extends QueryRelatedEAO<Pregnancy>{
}
