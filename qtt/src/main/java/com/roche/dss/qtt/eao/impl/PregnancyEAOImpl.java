package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.PregnancyEAO;
import com.roche.dss.qtt.model.Pregnancy;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
@Repository("pregnancy")
public class PregnancyEAOImpl extends AbstractQueryRelatedEAO<Pregnancy> implements PregnancyEAO {
}
