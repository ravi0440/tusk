package com.roche.dss.qtt.service.impl;

import java.io.IOException;
import java.util.List;

import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.service.FullTextSearchService;
import com.roche.dss.qtt.service.UtilService;
import com.roche.dss.util.DocHandlerBridge;

@Service("utilService")
public class UtilServiceImpl implements UtilService {
    @PersistenceContext
    protected EntityManager em;
    
    @Autowired
    protected FullTextSearchService fullTextSearchService;
   
    protected static final Logger log = LoggerFactory.getLogger(UtilServiceImpl.class);
	
    @Override
    public void cleanSecondLevelCache(boolean skipExtractedTexts)
    {   
        CacheManager cacheManager = CacheManager.getInstance();
        
        if (skipExtractedTexts) {
	        cacheManager.clearAllStartingWith("org");
	        cacheManager.clearAllStartingWith("com");
	        cacheManager.clearAllStartingWith("textHighlightCache");
        } else {
        	cacheManager.clearAll();
        }
    }
    
    @SuppressWarnings(value="unchecked")
	@Override
	@Transactional(readOnly=true)
	public void reindexAll(Class entityClass, int stage, int startIndex, int count) throws InterruptedException {
        FullTextEntityManager emFT = Search.getFullTextEntityManager(em);
        
        switch(stage) {
        case STAGE_INIT:
        	log.info("reindexAll, purging index and clearing the whole cache");
            cleanSecondLevelCache(false);
            emFT.purgeAll(entityClass);
        	break;
        case STAGE_INDEX:
        	log.info("reindexAll, indexing stage");
        	javax.persistence.Query q = em.createQuery("from Query as query order by query.querySeq");
        	q.setFirstResult(startIndex);
        	q.setMaxResults(count);
            List<Query> queries = q.getResultList();
            log.info("found " + queries.size() + " Query entities for indexing");
            for (Query query: queries) {
            	emFT.index(query);
            }
        	break;
        case STAGE_INDEX_SINGLE:
        	log.info("reindexing a single Query: " + startIndex);
            queries = em.createQuery("from Query as query where query.querySeq = :querySeq").setParameter("querySeq", Long.valueOf(startIndex)).getResultList();
            if (queries.size() == 1) {
            	Query query = queries.get(0);
            	if (query.getAttachments() != null) {
            		for (Attachment att: query.getAttachments()) {
            			String filename = DocHandlerBridge.getFileRepositoryBase();
            			if (! DocHandlerBridge.getTestMode()) {
            				filename += query.getQuerySeq() + "/" + att.getFilename();
            			} else {
            				filename += att.getFilename();
            			}
            			fullTextSearchService.clearFileTextContent(filename);
            		}
            	}
            	emFT.index(query);
            }     	
        	break;
        case STAGE_FINAL:
        	log.info("reindexAll, clearing ehcache");
        	cleanSecondLevelCache(true);
        	break;
        case STAGE_PURGE_INDEX:
        	log.info("reindexAll, purging index and ehcache");
            cleanSecondLevelCache(true);
            emFT.purgeAll(entityClass);
        	break;
        case STAGE_OPTIMIZE:
        	log.info("reindexAll, optimizing the index");
        	emFT.getSearchFactory().optimize();
        	break;
        case STAGE_PURGE_WHOLE_CACHE:
        	log.info("reindexAll, purging the whole cache");
        	cleanSecondLevelCache(false);
        	break;
        default:
        	log.warn("reindexAll, unknown stage");
        	break;
        }
	}

	@Cacheable(cacheName = "textHighlightCache",
	        keyGenerator = @KeyGenerator (
	                name = "ListCacheKeyGenerator",
	                properties = @Property( name="includeMethod", value="false")
	        )
		)
	public String getHighlightedText(@PartialCacheKey Attachment attF, @PartialCacheKey String quickSearchKeywords
			, Highlighter highlighter, Analyzer analyzer, String textToHighlight) throws IOException, InvalidTokenOffsetsException {
		return highlighter.getBestFragment(analyzer,
				null, textToHighlight);
	}
}
