package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;
import com.roche.dss.qtt.query.web.model.SummaryListParams;
import com.roche.dss.qtt.service.DictionaryService;
import com.roche.dss.qtt.service.workflow.WorkflowService;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author zerkowsm
 */
@Conversion()
public class RetrieveSummaryListAction extends TaskBaseAction {
    private static final Logger logger = LoggerFactory.getLogger(RetrieveSummaryListAction.class);
    private static final long serialVersionUID = 519986614053742613L;

	private static final String SESSION_KEY_PARAMS = "SUMMARY_LIST_PARAMS";
        
	private SummaryListParams params = new SummaryListParams();
	
    private WorkflowService workflowService;
	private DictionaryService dictionaryService;
	
	List<QuerySummaryDTO> tasks = new ArrayList<QuerySummaryDTO>();

    @Resource
    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    @Override
	@Resource
    public void setDictionaryService(DictionaryService dictionaryService) {
		this.dictionaryService = dictionaryService;
	}

	public DictionaryService getDictionary () {
		return dictionaryService;
	}

    public String setFilter() {
    	getSession().put(SESSION_KEY_PARAMS, params);
    	return SUCCESS;
    }
    
	public String execute() {
        boolean allowed = isDscl() || isDMGCoord() || isPS() || isAffiliate();
        if (!allowed) {
            return DENIED;
        }
    	if (getSession().containsKey(SESSION_KEY_PARAMS)) {
    		params = (SummaryListParams) getSession().get(SESSION_KEY_PARAMS);
    	}
        handleLabelLink();
        List<QuerySummaryDTO> taskList = workflowService.retrieveSummaryList(isAffiliate(), getLoggedUser(), isDscl(), isPS(), isDMGCoord(), isPDS(), isDMGScientist());
        filterTasks(taskList);
        Comparator<QuerySummaryDTO> cmp = (SummarySort.values()[sort]).getColumn();
        Collections.sort(tasks, asc ? cmp : Collections.reverseOrder(cmp));
        return SUCCESS;
    }

    private void filterTasks(List<QuerySummaryDTO> taskList) {
		for (QuerySummaryDTO t : taskList) {
			if ((params.getFrom() == null || !t.getQuery().getCurrentDueDate().isBefore(params.getFrom().getTime()))
					&& (params.getTo() == null || !t.getQuery().getCurrentDueDate().isAfter(params.getTo().getTime()))
					&& (StringUtils.isBlank(params.getOwner()) || StringUtils.equalsIgnoreCase(params.getOwner().trim(), t.getTask().getActorId()))
					&& (params.getActivity() == null || params.getActivity().length == 0 || Arrays.asList(params.getActivity()).contains(t.getTask().getName()))
					&& (params.getQueryType() == null || params.getQueryType().length == 0 || Arrays.asList(params.getQueryType()).contains(t.getQuery().getQueryType().getQueryType()))) {			
				tasks.add(t);
			}
		}
	}

    public List<QuerySummaryDTO> getTasks() {
        return tasks;
    }

	public SummaryListParams getParams() {
		return params;
	}

	public void setParams(SummaryListParams params) {
		this.params = params;
	}

}