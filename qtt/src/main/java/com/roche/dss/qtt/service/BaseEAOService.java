package com.roche.dss.qtt.service;

import java.util.Collection;

public interface BaseEAOService<T> {
    public void persist(T entity);

    public void persist(Collection<T> entities);
    
    public T merge(T entity);
    
    public void remove(T entity);
    
    public void flush();

    public void clear();

    public T find(Long id);

    public T getReference(Long id);

    public T get(Long id);
}
