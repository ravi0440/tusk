package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.RequesterEAO;
import com.roche.dss.qtt.model.Requester;

@Repository("requesterEAO")
public class RequesterEAOImpl extends AbstractBaseEAO<Requester> implements RequesterEAO {

}