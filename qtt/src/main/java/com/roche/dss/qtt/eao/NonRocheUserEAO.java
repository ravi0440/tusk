package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.NonRocheUser;

/**
 * User: pruchnil
 */
public interface NonRocheUserEAO extends BaseEAO<NonRocheUser>{
    NonRocheUser checkNonRocheUser(String login, String password);
}
