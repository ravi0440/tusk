package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.EmailHistory;

public interface EmailHistoryEAO extends BaseEAO<EmailHistory>{
	
}