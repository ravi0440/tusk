package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.PrgyExptypeEAO;
import com.roche.dss.qtt.model.PrgyExptype;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
@Repository("prgExptype")
public class PrgyExptypeEAOImpl extends AbstractQueryRelatedEAO<PrgyExptype> implements PrgyExptypeEAO {
}
