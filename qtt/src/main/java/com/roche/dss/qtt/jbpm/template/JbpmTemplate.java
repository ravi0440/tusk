package com.roche.dss.qtt.jbpm.template;

/**
 * @author zerkowsm
 *
 */
public interface JbpmTemplate {

	Object doInJbpm(JbpmCallback callback);

}
