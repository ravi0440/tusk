package com.roche.dss.qtt.query.web.qrf;



public class Constants {

    public static final int ROCHE_AFFILIATE = 2;

	/**
	 * boolean 'yes' in a single char format to be in sync with db column type
	 */
	public static final Character YES = 'Y';

	/**
	 * boolean 'no' a single char format to be in sync with db column type
	 */
	public static final Character NO = 'N';


	/**
	 * A query with status submitted.
	 */
	public static final String QUERY_STATUS_SUBMITTED = "SUBM";

	/**
	 * A query with status not-submitted.
	 */
	public static final String QUERY_STATUS_NOT_SUBMITTED = "NSUB";

	/**
	 * A query with status being processed.
	 */
	public static final String QUERY_STATUS_PROCESSED = "PROC";

	/**
	 * A query with status amended.
	 */
	public static final String QUERY_STATUS_AMENDED = "AMND";

	/**
	 * QRF attachment category
	 */
	public static final int QRF_ATTACHMENT_CATEGORY = 1;

	/**
	 * QRF attachment version
	 */
	public static final Float QRF_ATTACHMENT_VERSION = new Float(1.0);

	public static final int REPORTER_TYPE_REGULATORY_AUTHORITY = 3;
	public static final int REPORTER_TYPE_LAWYER = 8;
}
