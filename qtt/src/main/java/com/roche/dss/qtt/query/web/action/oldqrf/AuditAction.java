package com.roche.dss.qtt.query.web.action.oldqrf;

import com.roche.dss.qtt.model.Audit;
import com.roche.dss.qtt.model.ClinicalTrial;
import com.roche.dss.qtt.model.CtrlOformat;
import com.roche.dss.qtt.query.comms.dto.QueryDescDTO;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.model.QueryDescFormModel;
import com.roche.dss.qtt.service.QrfService;

import java.util.List;

public class AuditAction extends QRFAbstractAction {

    private QueryDescFormModel queryForm = new QueryDescFormModel();
    
    private Audit audit;

    private ClinicalTrial clinical = new ClinicalTrial();
	
    private String specialRequest;
        
    private static final Logger logger = LoggerFactory.getLogger(AuditAction.class);
    
    @Autowired
    private QrfService qrfService;

    @Override
    public Object getModel() {
        return queryForm;
    }


    public String execute() {
        // the query is update
        if (getSession().get(ActionConstants.QRF_EDIT_QUERY_TYPE) != null) {
            qrfService.updateExternalAudit(createQueryDescDTO(queryForm), audit, clinical, specialRequest, (Integer) getSession().get(ActionConstants.QRF_QUERY_ID));
        } else {
            qrfService.addExternalAudit(createQueryDescDTO(queryForm), audit, clinical, specialRequest, (Integer) getSession().get(ActionConstants.QRF_QUERY_ID));
        }
        return SUCCESS;
    }

    private QueryDescDTO createQueryDescDTO(QueryDescFormModel queryDescForm) {
    	QueryDescDTO dto = new QueryDescDTO();
    	dto.setNatureOfCase(queryDescForm.getNatureofcase());
    	dto.setOtherInfo(queryDescForm.getOtherinfo());
		return dto;
	}


	@Override
    public void validate() {
        logger.debug("validate");
        if (StringUtils.isBlank(queryForm.getNatureofcase())) {
            addActionError(getText("queryDescForm.natureofcase.displayname"));
        }
        if (audit != null && audit.getAllStudies() != null && !audit.getAllStudies()
        		&& StringUtils.isBlank(audit.getStudyNo())) {
            addActionError(getText("studyno.displayname"));
        }
        if (clinical == null || clinical.getCtrlOformat() == null || clinical.getCtrlOformat().getCtrlOformatId() == 0) {
        	addActionError(getText("outputformat.displayname"));
        } else if (clinical.getCtrlOformat().getCtrlOformatId() == CtrlOformat.OUTPUT_FORMAT_OTHER
				&& StringUtils.isBlank(specialRequest)) {
			addActionError(getText("specialrequests.displayname"));
		}
        super.validate();
    }

    public List<CtrlOformat> getClotypes() {
        return qrfService.getCtrlOformats();
    }


	public Audit getAudit() {
		return audit;
	}


	public void setAudit(Audit audit) {
		this.audit = audit;
	}


	public ClinicalTrial getClinical() {
		return clinical;
	}


	public void setClinical(ClinicalTrial clinical) {
		this.clinical = clinical;
	}


	public String getSpecialRequest() {
		return specialRequest;
	}


	public void setSpecialRequest(String specialRequest) {
		this.specialRequest = specialRequest;
	}
}
