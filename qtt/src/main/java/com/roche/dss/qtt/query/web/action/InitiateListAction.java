package com.roche.dss.qtt.query.web.action;

import java.util.ArrayList;
import java.util.List;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.service.InitiateService;

/**
 * @author zerkowsm
 *
 */
public class InitiateListAction extends CommonActionSupport {

	private static final long serialVersionUID = -8718873034665295370L;

	private static final Logger logger = LoggerFactory.getLogger(InitiateListAction.class);

	private List<Query> initiateList = new ArrayList<Query>();

	@Autowired
	private InitiateService initiateService;

	public String execute() {

		if (!isDscl()) {
			logger.error("Access denied. ");
			return ERROR;
		}

		initiateList = initiateService.getInitiateList();

		return SUCCESS;
	}

	public List<Query> getInitiateList() {
		return initiateList;
	}

	public int getInitiateListSize() {
		return initiateList.size();
	}

}
