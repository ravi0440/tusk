package com.roche.dss.qtt.model;
// Generated 2010-10-04 12:00:38 by Hibernate Tools 3.3.0.GA

/**
 * FuRequesterId generated by hbm2java
 */
@Embeddable
public class FuRequesterId implements java.io.Serializable {

	private static final long serialVersionUID = -4719606331166904788L;
	
	private long followupSeq;
	private long requesterId;

	public FuRequesterId() {
	}

	public FuRequesterId(long followupSeq, long requesterId) {
		this.followupSeq = followupSeq;
		this.requesterId = requesterId;
	}

	@Column(name = "FOLLOWUP_SEQ", nullable = false, precision = 10, scale = 0)
	public long getFollowupSeq() {
		return this.followupSeq;
	}

	public void setFollowupSeq(long followupSeq) {
		this.followupSeq = followupSeq;
	}

	@Column(name = "REQUESTER_ID", nullable = false, precision = 10, scale = 0)
	public long getRequesterId() {
		return this.requesterId;
	}

	public void setRequesterId(long requesterId) {
		this.requesterId = requesterId;
	}

	@Override
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof FuRequesterId))
			return false;
		FuRequesterId castOther = (FuRequesterId) other;

		return (this.getFollowupSeq() == castOther.getFollowupSeq())
				&& (this.getRequesterId() == castOther.getRequesterId());
	}

	@Override
	public int hashCode() {
		int result = 17;

		result = 37 * result + (int) this.getFollowupSeq();
		result = 37 * result + (int) this.getRequesterId();
		return result;
	}

}
