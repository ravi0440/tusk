package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.eao.QueryWorkflowHistoryEAO;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryWorkflowHistory;
import com.roche.dss.qtt.query.comms.dto.EmailDTO;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.workflow.WorkflowService;
import com.roche.dss.qtt.service.workflow.exception.TaskAlreadyCompletedException;
import com.roche.dss.qtt.utility.EmailSender;
import com.roche.dss.qtt.utility.config.Config;
import com.roche.dss.util.KeyGenerator;

import javax.annotation.Resource;

/**
 * This action prepares mail response to query
 * <p/>
 * User: pruchnil
 */
public class ResponseEmailAction extends CommonActionSupport {
    private static final Logger logger = LoggerFactory.getLogger(ResponseEmailAction.class);
    private long queryId;
    private EmailDTO email = new EmailDTO();
    private QueryService queryService;

    private String ignoreEmail;
    
    private String re;
    
    protected Config config;
    
    @Resource
    public void setConfig(Config config) {
        this.config = config;
    }
    
    @Autowired
    protected EmailSender emailSender;
    
    @Autowired
    protected WorkflowService workflowService;
    
    @Autowired
    private QueryWorkflowHistoryEAO queryWorkflowHistoryEAO;
    

    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Override
    public String execute() throws Exception {
        if (!isDscl()) {
            return DENIED;
        }
        Query query = queryService.find(queryId);
        if (StringUtils.isEmpty(query.getAccessKey())) {
            queryService.setAccessKey(queryId, KeyGenerator.getInstance().getNextID());
        }
        String address = "http://" + ServletActionContext.getRequest().getServerName() + ":" + ServletActionContext.getRequest().getServerPort();
        email.buildDTO(query, address, properties);
        
        if(query.getInformEuQPPV().toString().equals("Y")){
        	email.setCc(config.getEuqppvEmail());
        }
        
        
        return SUCCESS;
    }

    public String process(){
    	logger.debug("ResponseEmailAction.process");
    	Query query = queryService.find(queryId);
    	String mailFrom = properties.getProperty("qtt.initiate.dscl.email.address");
    	
    	
    	
    	if ("yes".equals(ignoreEmail)) {
			try {
				workflowService.sendFinalResponse(getLoggedUserName(), query);
				
				   QueryWorkflowHistory queryWorkflowHistory = new QueryWorkflowHistory();
			        queryWorkflowHistory.setEndDate(new DateTime());
			        queryWorkflowHistory.setModifiedBy(getLoggedUserName());
			        queryWorkflowHistory.setWorkflowId(String.valueOf(queryId));
			        queryWorkflowHistory.setStatus("WORKFLOW_END");
			        queryWorkflowHistoryEAO.save(queryWorkflowHistory);
				
			} catch (TaskAlreadyCompletedException e) {
				return TASK_ALREADY_COMPLETED;
			} catch (UnsupportedOperationException e) {
				return ERROR;
			}
    		return SUCCESS;
    	} else {
            if (!isDscl()) {
                return DENIED;
            }
            
            if (email.getTo() == null || "".equals(email.getTo().trim())) {
            	this.addFieldError("email.to", "Email Address is required");
            	if (query != null) {
            		email.setTo(query.getRequester().getEmail());
            	}
            	return INPUT;
            }
            
           
            try {
				emailSender.sendMessage(mailFrom, email.getTo(), email.getCc(), email.getSubject(), email.getBody());
			} catch (AddressException e) {
				logger.warn("Query Response Email", e);
				return "emailFailed";
			} catch (SendFailedException e) {
				logger.warn("Query Response Email", e);
				return "emailFailed";
			} catch (MessagingException e) {
				logger.warn("Query Response Email", e);
				return "emailFailed";
			} catch (Exception e) {
				logger.warn("Query Response Email", e);
				return "emailFailed";
			}

            if (! "y".equals(re)) {         
				try {
					workflowService.sendFinalResponse(getLoggedUserName(), query);
					
					 QueryWorkflowHistory queryWorkflowHistory = new QueryWorkflowHistory();
				        queryWorkflowHistory.setEndDate(new DateTime());
				        queryWorkflowHistory.setModifiedBy(getLoggedUserName());
				        queryWorkflowHistory.setWorkflowId(String.valueOf(queryId));
				        queryWorkflowHistory.setStatus("WORKFLOW_END");
				        queryWorkflowHistoryEAO.save(queryWorkflowHistory);
				} catch (TaskAlreadyCompletedException e) {
					return TASK_ALREADY_COMPLETED;
				} catch (UnsupportedOperationException e) {
					return ERROR;
				}
            }
            
    		return "emailSuccess";
    	}
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public EmailDTO getEmail() {
        return email;
    }

    public void setEmail(EmailDTO email) {
        this.email = email;
    }

	public String getIgnoreEmail() {
		return ignoreEmail;
	}

	public void setIgnoreEmail(String ignoreEmail) {
		this.ignoreEmail = ignoreEmail;
	}

	public String getRe() {
		return re;
	}

	public void setRe(String re) {
		this.re = re;
	}

}
