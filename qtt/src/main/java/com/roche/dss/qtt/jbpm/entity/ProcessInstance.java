package com.roche.dss.qtt.jbpm.entity;


import java.util.*;

public class ProcessInstance {

    private static final long serialVersionUID = 1L;

    long id;
    int version;
    protected String key;
    protected Date start;
    protected Date end;
    protected ProcessDefinition processDefinition;
    protected Token rootToken;
    protected Token superProcessToken;
    protected boolean isSuspended;
    protected Map<String, ModuleInstance> instances;
    protected List<RuntimeAction> runtimeActions;
    Set<TaskInstance> taskInstances = new HashSet<TaskInstance>(0);

    // constructors /////////////////////////////////////////////////////////////

    public ProcessInstance() {
    }

    public ProcessInstance(org.jbpm.graph.exe.ProcessInstance process) {
        this.id = process.getId();
        this.version = process.getVersion();
        this.key = process.getKey();
        this.start = process.getStart();
        this.end = process.getEnd();
        this.processDefinition = process.getProcessDefinition();
        this.rootToken = process.getRootToken();
        this.superProcessToken = process.getSuperProcessToken();
        this.isSuspended = process.isSuspended();
        this.instances = process.getInstances();
        this.runtimeActions = process.getRuntimeActions();
        this.taskInstances = (Set<TaskInstance>) process.getTaskMgmtInstance().getTaskInstances();
    }

    /**
     * is the list of all runtime actions.
     */
    public List<RuntimeAction> getRuntimeActions() {
        return runtimeActions;
    }

    // various information retrieval methods ////////////////////////////////////

    /**
     * tells if this process instance is still active or not.
     */
    public boolean hasEnded() {
        return (end != null);
    }

    /**
     * calculates if this process instance has still options to continue.
     */
    public boolean isTerminatedImplicitly() {
        boolean isTerminatedImplicitly = true;
        if (end == null) {
            isTerminatedImplicitly = rootToken.isTerminatedImplicitly();
        }
        return isTerminatedImplicitly;
    }

    /**
     * looks up the token in the tree, specified by the slash-separated token path.
     *
     * @param tokenPath is a slash-separated name that specifies a token in the tree.
     * @return the specified token or null if the token is not found.
     */
    public Token findToken(String tokenPath) {
        return (rootToken != null ? rootToken.findToken(tokenPath) : null);
    }

    /**
     * collects all instances for this process instance.
     */
    public List<Token> findAllTokens() {
        List<Token> tokens = new ArrayList<Token>();
        tokens.add(rootToken);
        rootToken.collectChildrenRecursively(tokens);
        return tokens;
    }

    // equals ///////////////////////////////////////////////////////////////////
    // hack to support comparing hibernate proxies against the real objects
    // since this always falls back to ==, we don't need to overwrite the hashcode
    @Override
	public boolean equals(Object o) {
        return EqualsUtil.equals(this, o);
    }

    @Override
	public String toString() {
        return "ProcessInstance" + (key != null ? '(' + key + ')'
                : id != 0 ? "(" + id + ')' : '@' + Integer.toHexString(hashCode()));
    }

    // getters and setters //////////////////////////////////////////////////////

    public long getId() {
        return id;
    }

    public Token getRootToken() {
        return rootToken;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    public Map<String, ModuleInstance> getInstances() {
        return instances;
    }

    public ProcessDefinition getProcessDefinition() {
        return processDefinition;
    }

    public Token getSuperProcessToken() {
        return superProcessToken;
    }

    public void setSuperProcessToken(Token superProcessToken) {
        this.superProcessToken = superProcessToken;
    }

    public boolean isSuspended() {
        return isSuspended;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public void setProcessDefinition(ProcessDefinition processDefinition) {
        this.processDefinition = processDefinition;
    }

    public void setRootToken(Token rootToken) {
        this.rootToken = rootToken;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    /**
     * a unique business key
     */
    public String getKey() {
        return key;
    }

    /**
     * set the unique business key
     */
    public void setKey(String key) {
        this.key = key;
    }

    public Set<TaskInstance> getTaskInstances() {
        return taskInstances;
    }

    public void setTaskInstances(Set<TaskInstance> taskInstances) {
        this.taskInstances = taskInstances;
    }
}
