/* 
====================================================================
  $Header$
  @author $Author: rahmanh1 $
  @version $Revision: 628 $ $Date: 2004-04-16 14:24:45 +0200 (Pt, 16 kwi 2004) $

====================================================================
*/
package com.roche.dss.qtt.query.web.qrf.model;

/**
 * This form bean is used by Struts to interact with forms on the QTT JSP pages.
 * @author shaikhi
 */
public class CaseFormModel  {
	
	private String aer1 = null;
	private String local1 = null;
	private String ext1 = null;
	private String aer2 = null;
	private String local2 = null;
	private String ext2 = null;
	private String aer3 = null;
	private String local3 = null;
	private String ext3 = null;
	private String aer4 = null;
	private String local4 = null;
	private String ext4 = null;
	private String aer5 = null;
	private String local5 = null;
	private String ext5 = null;
	private String aer6 = null;
	private String local6 = null;
	private String ext6 = null;
	private String aer7 = null;
	private String local7 = null;
	private String ext7 = null;
	private String aer8 = null;
	private String local8 = null;
	private String ext8 = null;
	private String aer9 = null;
	private String local9 = null;
	private String ext9 = null;
	private String aer10 = null;
	private String local10 = null;
	private String ext10 = null;
	private String comments = null;
	private String aeterm = null;
	private String batchno = null;
	private int outputformat;
	private String othercomments = null; 
	private int exposuretype;
	private String timing = null;
	private String outcome = null; 
	private String action = null;
	
    /**
     * @return
     */
    public String getComments() {
        return comments;
    }

    /**
     * @return
     */
    public String getExt1() {
        return ext1;
    }

    /**
     * @return
     */
    public String getExt10() {
        return ext10;
    }

    /**
     * @return
     */
    public String getExt2() {
        return ext2;
    }

    /**
     * @return
     */
    public String getExt3() {
        return ext3;
    }

    /**
     * @return
     */
    public String getExt4() {
        return ext4;
    }

    /**
     * @return
     */
    public String getExt5() {
        return ext5;
    }

    /**
     * @return
     */
    public String getExt6() {
        return ext6;
    }

    /**
     * @return
     */
    public String getExt7() {
        return ext7;
    }

    /**
     * @return
     */
    public String getExt8() {
        return ext8;
    }

    /**
     * @return
     */
    public String getExt9() {
        return ext9;
    }

    /**
     * @return
     */
    public String getLocal1() {
        return local1;
    }

    /**
     * @return
     */
    public String getLocal10() {
        return local10;
    }

    /**
     * @return
     */
    public String getLocal2() {
        return local2;
    }

    /**
     * @return
     */
    public String getLocal3() {
        return local3;
    }

    /**
     * @return
     */
    public String getLocal4() {
        return local4;
    }

    /**
     * @return
     */
    public String getLocal5() {
        return local5;
    }

    /**
     * @return
     */
    public String getLocal6() {
        return local6;
    }

    /**
     * @return
     */
    public String getLocal7() {
        return local7;
    }

    /**
     * @return
     */
    public String getLocal8() {
        return local8;
    }

    /**
     * @return
     */
    public String getLocal9() {
        return local9;
    }

    /**
     * @return
     */
    public String getAer1() {
        return aer1;
    }

    /**
     * @return
     */
    public String getAer10() {
        return aer10;
    }

    /**
     * @return
     */
    public String getAer4() {
        return aer4;
    }

    /**
     * @return
     */
    public String getAer5() {
        return aer5;
    }

    /**
     * @return
     */
    public String getAer6() {
        return aer6;
    }

    /**
     * @return
     */
    public String getAer7() {
        return aer7;
    }

    /**
     * @return
     */
    public String getAer8() {
        return aer8;
    }

    /**
     * @return
     */
    public String getAer9() {
        return aer9;
    }

    /**
     * @return
     */
    public String getAer2() {
        return aer2;
    }

    /**
     * @return
     */
    public String getAer3() {
        return aer3;
    }

    @StringLengthFieldValidator(maxLength = "300", key = "caseForm.comments.maxlength.displayname")
    public void setComments(String string) {
        comments = string;
    }

    @StringLengthFieldValidator(maxLength = "400", key = "caseForm.ext1.maxlength.displayname")
    public void setExt1(String string) {
        ext1 = string;
    }

    @StringLengthFieldValidator(maxLength = "400", key = "caseForm.ext10.maxlength.displayname")
    public void setExt10(String string) {
        ext10 = string;
    }

    @StringLengthFieldValidator(maxLength = "400", key = "caseForm.ext2.maxlength.displayname")
    public void setExt2(String string) {
        ext2 = string;
    }

    @StringLengthFieldValidator(maxLength = "400", key = "caseForm.ext3.maxlength.displayname")
    public void setExt3(String string) {
        ext3 = string;
    }

    @StringLengthFieldValidator(maxLength = "400", key = "caseForm.ext4.maxlength.displayname")
    public void setExt4(String string) {
        ext4 = string;
    }

    @StringLengthFieldValidator(maxLength = "400", key = "caseForm.ext5.maxlength.displayname")
    public void setExt5(String string) {
        ext5 = string;
    }

    @StringLengthFieldValidator(maxLength = "400", key = "caseForm.ext6.maxlength.displayname")
    public void setExt6(String string) {
        ext6 = string;
    }

    @StringLengthFieldValidator(maxLength = "400", key = "caseForm.ext7.maxlength.displayname")
    public void setExt7(String string) {
        ext7 = string;
    }

    @StringLengthFieldValidator(maxLength = "400", key = "caseForm.ext8.maxlength.displayname")
    public void setExt8(String string) {
        ext8 = string;
    }

    @StringLengthFieldValidator(maxLength = "400", key = "caseForm.ext9.maxlength.displayname")
    public void setExt9(String string) {
        ext9 = string;
    }

    /**
     * @param string
     */
    public void setLocal1(String string) {
        local1 = string;
    }

    /**
     * @param string
     */
    public void setLocal10(String string) {
        local10 = string;
    }

    /**
     * @param string
     */
    public void setLocal2(String string) {
        local2 = string;
    }

    /**
     * @param string
     */
    public void setLocal3(String string) {
        local3 = string;
    }

    /**
     * @param string
     */
    public void setLocal4(String string) {
        local4 = string;
    }

    /**
     * @param string
     */
    public void setLocal5(String string) {
        local5 = string;
    }

    /**
     * @param string
     */
    public void setLocal6(String string) {
        local6 = string;
    }

    /**
     * @param string
     */
    public void setLocal7(String string) {
        local7 = string;
    }

    /**
     * @param string
     */
    public void setLocal8(String string) {
        local8 = string;
    }

    /**
     * @param string
     */
    public void setLocal9(String string) {
        local9 = string;
    }

    /**
     * @param string
     */
    public void setAer1(String string) {
        aer1 = string;
    }

    /**
     * @param string
     */
    public void setAer10(String string) {
        aer10 = string;
    }

    /**
     * @param string
     */
    public void setAer4(String string) {
        aer4 = string;
    }

    /**
     * @param string
     */
    public void setAer5(String string) {
        aer5 = string;
    }

    /**
     * @param string
     */
    public void setAer6(String string) {
        aer6 = string;
    }

    /**
     * @param string
     */
    public void setAer7(String string) {
        aer7 = string;
    }

    /**
     * @param string
     */
    public void setAer8(String string) {
        aer8 = string;
    }

    /**
     * @param string
     */
    public void setAer9(String string) {
        aer9 = string;
    }

    /**
     * @param string
     */
    public void setAer2(String string) {
        aer2 = string;
    }

    /**
     * @param string
     */
    public void setAer3(String string) {
        aer3 = string;
    }

	public void reset() {
	
		/*aer1 = null;
		local1 = null;
		ext1 = null;
		aer2 = null;
		local2 = null;
		ext2 = null;
		aer3 = null;
		local3 = null;
		ext3 = null;
		aer4 = null;
		local4 = null;
		ext4 = null;
		aer5 = null;
		local5 = null;
		ext5 = null;
		aer6 = null;
		local6 = null;
		ext6 = null;
		aer7 = null;
		local7 = null;
		ext7 = null;
		aer8 = null;
		local8 = null;
		ext8 = null;
		aer9 = null;
		local9 = null;
		ext9 = null;
		aer10 = null;
		local10 = null;
		ext10 = null;
		comments = null;
		aeterm = null;
		batchno = null;
		othercomments = null;			
		timing = null;		
		outcome = null;*/
		action = null;
	}

    /**
     * @return
     */
    public String getAeterm() {
        return aeterm;
    }

    /**
     * @param string
     */
    public void setAeterm(String string) {
        aeterm = string;
    }

   

    /**
     * @return
     */
    public String getBatchno() {
        return batchno;
    }

     public String getOthercomments() {
        return othercomments;
    }

    /**
     * @return
     */
    public int getOutputformat() {
        return outputformat;
    }

    /**
     * @param string
     */
    public void setBatchno(String string) {
        batchno = string;
    }

   @StringLengthFieldValidator(maxLength = "200", key = "caseForm.othercomments.maxlength.displayname")
   @FieldExpressionValidator(expression = "othercomments != null && othercomments!='' || outputformat != 4", key = "caseForm.othercomments.displayname")
   public void setOthercomments(String string) {
        othercomments = string;
    }

    /**
     * @param i
     */
    public void setOutputformat(int i) {
        outputformat = i;
    }

    /**
     * @return
     */
    public int getExposuretype() {
        return exposuretype;
    }

    /**
     * @return
     */
    public String getOutcome() {
        return outcome;
    }

    /**
     * @return
     */
    public String getTiming() {
        return timing;
    }

    /**
     * @param i
     */
    public void setExposuretype(int i) {
        exposuretype = i;
    }

    @StringLengthFieldValidator(maxLength = "100", key = "caseForm.timing.maxlength.displayname")
    public void setOutcome(String string) {
        outcome = string;
    }

    @StringLengthFieldValidator(maxLength = "100", key = "caseForm.outcome.maxlength.displayname")
    public void setTiming(String string) {
        timing = string;
    }

    /**
     * @return
     */
    public String getAction() {
        return action;
    }

    /**
     * @param string
     */
    public void setAction(String string) {
        action = string;
    }

    public void copyValuesFrom(CaseFormModel copyFrom) {
        aer1 = copyFrom.getAer1();
        local1 = copyFrom.getLocal1();
        ext1 = copyFrom.getExt1();
        aer2 = copyFrom.getAer2();
        local2 = copyFrom.getLocal2();
        ext2 = copyFrom.getExt2();
        aer3 = copyFrom.getAer3();
        local3 = copyFrom.getLocal3();
        ext3 = copyFrom.getExt3();
        aer4 = copyFrom.getAer4();
        local4 = copyFrom.getLocal4();
        ext4 = copyFrom.getExt4();
        aer5 = copyFrom.getAer5();
        local5 = copyFrom.getLocal5();
        ext5 = copyFrom.getExt5();
        aer6 = copyFrom.getAer6();
        local6 = copyFrom.getLocal6();
        ext6 = copyFrom.getExt6();
        aer7 = copyFrom.getAer7();
        local7 = copyFrom.getLocal7();
        ext7 = copyFrom.getExt7();
        aer8 = copyFrom.getAer8();
        local8 = copyFrom.getLocal8();
        ext8 = copyFrom.getExt8();
        aer9 = copyFrom.getAer9();
        local9 = copyFrom.getLocal9();
        ext9 = copyFrom.getExt9();
        aer10 = copyFrom.getAer10();
        local10 = copyFrom.getLocal10();
        ext10 = copyFrom.getExt10();
        comments = copyFrom.getComments();
        aeterm = copyFrom.getAeterm();
        batchno = copyFrom.getBatchno();
        outputformat = copyFrom.getOutputformat();
        othercomments = copyFrom.getOthercomments();
        exposuretype = copyFrom.getExposuretype();
        timing = copyFrom.getTiming();
        outcome = copyFrom.getOutcome();
        action = copyFrom.getAction();
    }
}
