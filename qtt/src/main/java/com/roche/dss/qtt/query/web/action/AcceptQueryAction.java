/* 
 ====================================================================
 $Header$
 @author $Author: cimerl $
 @version $Revision: 2665 $ $Date: 2013-10-14 11:38:26 +0200 (Pn, 14 paź 2013) $

 ====================================================================
 */
package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.web.qrf.Constants;
import com.roche.dss.qtt.service.InitiateService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.utility.EmailSender;
import com.roche.dss.qtt.utility.QueryStatusCodes;
import com.roche.dss.qtt.utility.config.Config;
import com.roche.dss.qtt.utility.pdf.QrfPdfInterface;
import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
@ParentPackage("default")
public class AcceptQueryAction extends CommonActionSupport {
    
	private static final long serialVersionUID = 8705740955741016908L;

	private static final Logger logger = LoggerFactory.getLogger(AcceptQueryAction.class);

	private InitiateService initiateService;
    private QueryService queryService;
	private QrfPdfInterface qrfPdf;
    protected EmailSender emailSender;
    protected Config config;
    private Long id;
    private List<String> emailRecipients = new ArrayList<String>();

    @Resource
    public void setConfig(Config config) {
        this.config = config;
    }

    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }
    @Resource
    public void setInitiateService(InitiateService initiateService) {
        this.initiateService = initiateService;
    }
    @Resource
    public void setQrfPdfInterface(QrfPdfInterface qrfPdf) {
        this.qrfPdf = qrfPdf;
    }
    @Resource
    public void setEmailSender(EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public List<String> getEmailRecipients() {
		return emailRecipients;
	}

	public void setEmailRecipients(List<String> emailRecipients) {
		this.emailRecipients = emailRecipients;
	}
	
	@Override
	public void validate() {
		List<String> emails = new ArrayList<String>(emailRecipients.size());
		for (String email : emailRecipients) {
			if (StringUtils.isNotBlank(email)) {
				if (!hasActionErrors() && !email.matches(EmailValidator.emailAddressPattern)) {
					addActionError(getText("qrfForm.email.invalid.displayname"));
				}
	        	emails.add(email);
			}
		}
		if (emails.size() == 0) {
			addActionError(getText("qrfForm.email.displayname"));
		}
		emailRecipients = emails;
		
        super.validate();
	}
	
	@Action(value = "AcceptQuery", results = {
            @Result(name = "success", type = "chain", location = "InitiateList"),
            @Result(name = "failure", location = "/error.jsp"),
            @Result(name = "already_processed", type = "tiles", location = "/already_processed"),
            @Result(name = "input", type = "tiles", location = "/accept.email"),
            @Result(name = "error", location = "/qtt/global/error.jsp")
    })
    public String execute() {

        // security check
        boolean allowed = isDscl();

        if (!allowed) {
            logger.error("AcceptQueryAction:Access denied.");
            return "failure";
        }

        // retrieve query type id from the session
        Query query = queryService.find(id);
        if (query == null) {
            logger.error("Query id not passed to the action!");
            return ERROR;
        }

        // accept query
        //InitiateFacadeBD bd = (InitiateFacadeBD) BusinessDelegateFactory.getFactory().getServiceBD(mapping);

        // change due to bug fix #197 - perform safety-check to see if query is still in state 'SUBM'
        if (queryService.checkHasStatus(query.getQuerySeq(), QueryStatusCodes.SUBMITTED.getCode())) {
            initiateService.acceptQuery(query.getQuerySeq(), Constants.QUERY_STATUS_PROCESSED, getLoggedUserName());
            sendEmail(query);
        } else {
            // if query has already been accepted / rejected / amended, show "please refresh list" message
            return "already_processed";
        }

        logger.info("AcceptQueryAction query:" + query.getQueryNumber());
        return SUCCESS;
    }
    
    @Action(value = "AcceptQueryListRecipients", results = {
            @Result(name = "success", type = "tiles", location = "/accept.email"),
            @Result(name = "error", location = "/qtt/global/error.jsp")
    })
    @SkipValidation
    public String listRecipients() {
        Query query = queryService.find(id);
        if (query == null) {
            logger.error("Query id not passed to the action!");
            return ERROR;
        }
    	emailRecipients = new ArrayList<String>();
    	emailRecipients.add(query.getRequester().getEmail());
    	return SUCCESS;
    }

    public void sendEmail(Query query) {
        // send email
        String from = "";
        String subject = "";
        StringBuilder body = new StringBuilder();
        StringBuilder to = new StringBuilder();
        ByteArrayOutputStream out = qrfPdf.qrfBuilder(Long.valueOf(query.getQuerySeq()).intValue(), "Accepted");
        String attachName = "QRF-" + query.getQueryNumber() + ".pdf";
        from = config.getInititateDsclEmail();
        String queryLabel = "";
        if (!StringUtils.isEmpty(query.getRequesterQueryLabel())) {
            queryLabel = " - [" + query.getRequesterQueryLabel() + "]";
        }
        subject = "Query " + query.getQueryNumber() + " "
                + config.getInititateAcceptEmailSubject() + queryLabel;

        body.append("\n");
        body.append(config.getInititateAcceptBody1Para());
        body.append("\n\n");
        body.append(config.getInititateAcceptBody2Para());
        body.append(" ");
        body.append(query.getQueryNumber());
        body.append(".");
        body.append("\n");
        body.append(config.getInititateAcceptBody3Para()); 
        body.append("\n\n");
 		body.append(config.getInititateAcceptBody4Para());
 		body.append(" ");
 		body.append(DateTimeFormat.forPattern("dd-MMM-yyyy").print(query.getDateRequested()));
 		body.append("\n");
 		body.append(config.getInititateAcceptBodyLastPara());

 		for (int i = 0; i < emailRecipients.size(); i++) {
 			if (i > 0) {
 				to.append(EmailSender.ADDRESS_DELIMITER);
 			}
 			to.append(emailRecipients.get(i));
 		}

        try {
            emailSender.sendMessage(from, to.toString(), subject, body.toString(), attachName, out);
            logger.info("AcceptQueryAction:sending email to:" + to + " query no:" + query.getQueryNumber());
        } catch (Exception e) {
            logger.error("Error AcceptQueryAction:sending email " + to + " query no:" + query.getQueryNumber(), e);
        }
    }

}
