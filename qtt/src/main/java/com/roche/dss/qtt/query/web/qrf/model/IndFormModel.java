
package com.roche.dss.qtt.query.web.qrf.model;

public class IndFormModel {
	
    private String indNumber = null;

    /**
     * @return
     */
    public String getIndNumber() {
        return indNumber;
    }

    /**
     * @param string
     */
    public void setIndNumber(String string) {
        indNumber = string;
    }


}
