package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.AttachmentEAO;
import com.roche.dss.qtt.model.Attachment;
import java.util.List;

/**
 * User: pruchnil
 */
@Repository("attachmentEAO")
public class AttachmentEAOImpl extends AbstractQueryRelatedEAO<Attachment> implements AttachmentEAO {

    @Override
    public List<Attachment> findForClosedQuery(long querySeq, String followupNumber) {
        return getEntityManager().createNamedQuery("attachment.findByFollowupNumber", Attachment.class).setParameter("querySeq", querySeq).setParameter("followupNumber", followupNumber).getResultList();
    }

    @Override
    public DateTime getLatestFinalDocCreationDate(long queryId) {
        try {
            return getEntityManager().createNamedQuery("attachment.getLatestFinalDocCreationDate", DateTime.class).setParameter("querySeq", queryId).getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    @Override
    public String getMaxFollowupNumber(long queryId) {
        try {
            return getEntityManager().createNamedQuery("followupAttachment.findMaxFollowupNumber", String.class).setParameter("queryId", queryId).getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
