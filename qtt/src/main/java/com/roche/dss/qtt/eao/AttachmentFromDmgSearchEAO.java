package com.roche.dss.qtt.eao;


import com.roche.dss.qtt.model.AttachmentFromDmgSearch;

import java.util.List;

public interface AttachmentFromDmgSearchEAO extends QueryRelatedEAO<AttachmentFromDmgSearch> {
    List<AttachmentFromDmgSearch> getAttachmentsForDMGSearch(long taskInstanceId);
}
