package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;
import com.roche.dss.util.comparator.*;

import java.util.Comparator;
import java.util.Map;

/**
 * @author zerkowsm
 *
 */
public class TaskBaseAction extends CommonActionSupport implements SessionAware {
	private static final long serialVersionUID = 151772374353024287L;
	
    public static final String FILTER_QUERY_LABEL_ON = "QLon";
    public static final String FILTER_QUERY_LABEL_OFF = "QLoff";
    public static final String FILTER_SUMMARY_QUERY_LABEL_ON = "sQLon";
    public static final String FILTER_SUMMARY_QUERY_LABEL_OFF = "sQLoff";
    
    protected boolean DMGAllwed;
    protected boolean MIAllowed;
    
    protected boolean showLabelLink;
	protected boolean displayQueryLabel;
	protected boolean displayContactRequester;
	protected String filterKey;

    protected int sort = 0;
    protected int last = 0;
    protected boolean asc = true;
    
    private Map<String, Object> session;
	
	public Map<String, Object> getSession() {
		return session;
	}

	public void handleLabelLink(){
		if(filterKey == null) {
			filterKey = (String) session.get(TASKLIST_LINK_TO_QUERY_LABEL);
		}
		if(filterKey != null){
			if (filterKey.equals(FILTER_QUERY_LABEL_ON)) {
				setShowLabelLink(true);
				setDisplayQueryLabel(true);	
				session.put(TASKLIST_LINK_TO_QUERY_LABEL, FILTER_QUERY_LABEL_ON);
			} else if (filterKey.equals(FILTER_QUERY_LABEL_OFF)) {
				setShowLabelLink(false);
				setDisplayQueryLabel(false);
				session.put(TASKLIST_LINK_TO_QUERY_LABEL, FILTER_QUERY_LABEL_OFF);
			} else if (filterKey.equals(FILTER_SUMMARY_QUERY_LABEL_ON)) {
				setShowLabelLink(true);
				setDisplayQueryLabel(true);
				session.put(TASKLIST_LINK_TO_QUERY_LABEL, FILTER_SUMMARY_QUERY_LABEL_ON);
	        } else if (filterKey.equals(FILTER_SUMMARY_QUERY_LABEL_OFF)) {
				setShowLabelLink(false);
				setDisplayQueryLabel(false);
				session.put(TASKLIST_LINK_TO_QUERY_LABEL, FILTER_SUMMARY_QUERY_LABEL_OFF);
	        }
		}
		
	}
	
    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
	
	public boolean isShowLabelLink() {
		return showLabelLink;
	}

	public void setShowLabelLink(boolean showLabelLink) {
		this.showLabelLink = showLabelLink;
	}

	public String getFilterKey() {
		return filterKey;
	}

	public void setFilterKey(String filterKey) {
		this.filterKey = filterKey;
	}

	public boolean isDisplayQueryLabel() {
		return displayQueryLabel;
	}

	public void setDisplayQueryLabel(boolean displayQueryLabel) {
		this.displayQueryLabel = displayQueryLabel;
	}
	
	public boolean isDMGAllowed() {
		return isDscl() || isDMGCoord() || isDMGScientist();
	}
	
	public boolean isMIAllowed() {
		return isDscl() || isPS();
	}
	
	public boolean isDisplayContactRequester() {
		return !isAffiliate();
	}

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getLast() {
        return last;
    }

    public void setLast(int last) {
        this.last = last;
    }

    public boolean isAsc() {
        return asc;
    }

    public void setAsc(boolean asc) {
        this.asc = asc;
    }

    public boolean checkOrder() {
        if (sort == last) {
            asc = !asc;
        } else {
            asc = true;
        }
        return asc;
    }

    public enum SummarySort {
        QUERYNUM(new QuerySummaryNumberComparator()), DRUG(new QuerySummaryDrugComparator()),
        QUERYTYPE(new QuerySummaryTypeComparator()), COUNTRY(new QuerySummaryCountryComparator()),
        INITDATE(new QuerySummaryInitDateComparator()), RESPDATE(new QuerySummaryRespDateComparator()),
        DMGRESPDATE(new QuerySummaryDMGRespDateComparator()), ACTIVITY(new QuerySummaryActivityComparator()),
        OWNER(new QuerySummaryOwnerComparator()), WORKFLOWTYPE(new QuerySummaryWorkflowTypeComparator());

        private Comparator<QuerySummaryDTO> column;

        SummarySort(Comparator<QuerySummaryDTO> column) {
            this.column = column;
        }

        public Comparator<QuerySummaryDTO> getColumn() {
            return column;
        }
    }
	
}