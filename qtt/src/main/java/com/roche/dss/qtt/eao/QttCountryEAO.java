package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.QttCountry;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface QttCountryEAO extends BaseEAO<QttCountry> {

    public List<QttCountry> getOrderedCountries();
}
