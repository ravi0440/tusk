package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.AttachmentCategory;
import com.roche.dss.qtt.model.FuQuery;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryVerification;
import com.roche.dss.qtt.query.comms.dto.QueryVerificationDTO;
import com.roche.dss.qtt.security.model.DSSUser;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.workflow.WorkflowService;
import com.roche.dss.qtt.service.workflow.jbpm.JbpmService;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static com.roche.dss.qtt.utility.Activities.*;

/**
 * This action presents details of query
 * <p/>
 * User: pruchnil
 */
public class RetrieveQueryDetailsAction extends CommonActionSupport {
    private static final Logger logger = LoggerFactory.getLogger(RetrieveQueryDetailsAction.class);

    private Query query;
    private TaskInstance taskInstance;
    private long queryId;
    private Long taskId;
    private String ownerEmail;

    private boolean displayUpdateResponseDate = false;
    private boolean displayUpdateDMGDate = false;
    private boolean displayRestartWorkflow = false;
    private boolean displayDispatch = false;
    private boolean displayContactOwner = false;
    private boolean displayFinalResponseSent = false;
    private boolean displayAmendQuery = false;
    private boolean displayDeleteQuery = false;
    private boolean displaywaitingForVerification = false;

    private List<TaskInstance> tasks = new ArrayList<TaskInstance>();

    private QueryService queryService;
    private WorkflowService workflowService;
    
    private JbpmService jbpmService;

    
    @Resource
    public void setJbpmService(JbpmService jbpmService) {
        this.jbpmService = jbpmService;
    }
	
    
    // either "quickSearch" or "repositorySearch"
    protected String sourceSearch;

    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @Resource
    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    @Override
    public String execute() throws Exception {
        query = queryService.getClosedQueryDetails(queryId, isAffiliate());
        
        //get any task assigned to query
        if(taskId ==null){
            taskInstance = jbpmService.retrieveProcessTaskCurrent(query.getProcessInstanceId());
            taskId = taskInstance.getId();
        }else{
        	taskInstance = workflowService.getTaskInstance(taskId);
        }
        if (query.getProcessInstanceId() != null) {
            tasks = workflowService.retrieveProcessTaskList(Long.toString(query.getQuerySeq()));
        }
        if (ownerEmailNeeded() && isRealUser(taskInstance.getActorId())) {
            DSSUser user = securityService.getDSSUser(taskInstance.getActorId());
            if (user != null) {
                ownerEmail = user.getEmail();
            }
        }
        checkPermissions();
        return SUCCESS;
    }

    public String retrieveClosedQueryDetails() throws Exception {
        query = queryService.getClosedQueryDetails(queryId, isAffiliate());
        return SUCCESS;
    }

    public String changeUrgencyFlag() throws Exception {
        query = queryService.changeUrgencyFlag(queryId);
        return SUCCESS;
    }

    private void checkPermissions() {
        if (taskInstance != null) {
            boolean finalDocExists = queryService.isFinalDocAttachedInCurrentInteraction(queryId, query.getProcessInstanceId());

            displayUpdateDMGDate = displayUpdateDMGDate(query);
            if (query.getWorkflowRouteType() == null) {
                displayUpdateResponseDate = isDscl();
            } else if (query.getWorkflowRouteType().startsWith("A")) {
                displayUpdateResponseDate = (isDscl() || isDMGCoord() || (isDMGScientist() && getLoggedUserName().equalsIgnoreCase(taskInstance.getActorId())));
            } else if (query.getWorkflowRouteType().startsWith("B")) {
                displayUpdateResponseDate = (isDscl() || (isPS() && getLoggedUserName().equalsIgnoreCase(taskInstance.getActorId())));
            }

            displayRestartWorkflow = isDscl() && !(ASSIGN.getTaskName().equals(taskInstance.getName()) ||
                    SEND_TO_REQUESTER.getTaskName().equals(taskInstance.getName()) ||
                    AWAITING_FOLLOWUP.getTaskName().equals(taskInstance.getName()) ||
                    UNKNOWN.getTaskName().equals(taskInstance.getName()) || finalDocExists);
            //      TODO if getIsFromTaskListToken(req) is neccessary
            //      displayDispatch = getIsFromTaskListToken(req) && isDscl() && taskInstance.getName().equals(Activities.ASSIGN.getDisplayName());
            displayDispatch = isDscl() && ASSIGN.getTaskName().equals(taskInstance.getName());
            displayAmendQuery = isDscl() && (!finalDocExists);
            displayDeleteQuery = isDscl() && !query.hasFollowup() && !finalDocExists;
            displayContactOwner = ownerEmailNeeded() && isRealUser(taskInstance.getActorId());
            displayFinalResponseSent = (isDscl() && taskInstance.getName().equals(SEND_TO_REQUESTER.getTaskName()));
            
            
            QueryVerificationDTO dto=queryService.getQueryVerification(QueryVerification.RECORD_TYPE.FILLED_BY_VERIFIER, queryId,query.getFollowupNumber());
            
            if(dto==null && query.getVerifyPerson()!=null){
            	displaywaitingForVerification=true;
            }else{
            	displaywaitingForVerification=false;
            }
            
            
        }
    }
    
    private Boolean hasFinalResponse(){
    	
    	for(Attachment att: query.getAttachmentsForDisplay()){
    		if(att.getAttachmentCategory().getAttachmentCategoryId()==AttachmentCategory.CATEGORY_FINAL_RESPONSE){
    			if(query.getFollowupNumber()==null){
    				return true;
                }else if (query.getFollowupSeq() != null && query.getFollowupSeq().equals(att.getFollowupSeq())){
                    return true;
                }else if (query.getFollowupSeq() == null && att.getFollowupSeq()==null){
                    return true;
    			}
    			
    		}
    	}
    	return false;
    }
    
    private Boolean hasSupportingDocument(){
    	for(Attachment att: query.getAttachmentsForDisplay()){
    		if(att.getAttachmentCategory().getAttachmentCategoryId()!=AttachmentCategory.CATEGORY_FINAL_RESPONSE){
    			if(query.getFollowupNumber()==null){
    				return true;
                }else if (query.getFollowupSeq() != null && query.getFollowupSeq().equals(att.getFollowupSeq())){
                    return true;
                }else if (query.getFollowupSeq() == null && att.getFollowupSeq()==null){
                    return true;
                }
    		}
    	}
    	return false;
    }
    
    public boolean displayVerification(){
        if (taskInstance.getName().equals(DMG_DATA_SEARCH.getTaskName())) {

            if (query.getWorkflowRouteType().startsWith("A") && hasFinalResponse()) {
                return true;
            }
            if (query.getWorkflowRouteType().startsWith("B") && hasSupportingDocument()) {
                return true;
            }
            return false;
        }
        return query.getVerifyPerson().equals(getLoggedUserName());
    }

	private boolean displayUpdateDMGDate(Query query) {
		if (query.getWorkflowRouteType() == null) {
			return false;
		}
		
		
		if (query.getWorkflowRouteType().startsWith("A")) {
		    /*
		     * Provide the ability to change the â€œDMG due dateâ€� for follow-up questions via the Query Details View screen 
		     * (specifically when a Process-Type B query has been answered and during a follow up request was changed to a Process-Type A query).
		     */
			//latest FuQuery is first (orderBy in mapping)
			FuQuery fuQuery = query.getFollowupQueries().size() > 0 ? query.getFollowupQueries().get(0) : null; 
			return query.getDmgDueDate() != null
					&& fuQuery != null
					&& fuQuery.getWorkflowRouteType().startsWith("B") 
					&& isInDmgSubProcess(taskInstance.getName())
					&& isUserAllowedToChangeDMGDate();
		} else if (query.getWorkflowRouteType().startsWith("B")) {
			return isInDmgSubProcess(taskInstance.getName()) && isUserAllowedToChangeDMGDate();
		}
		return false;
	}

	private boolean isUserAllowedToChangeDMGDate() {
		return isDscl()	
				|| isDMGCoord() 
				|| (isDMGScientist() && getLoggedUserName().equalsIgnoreCase(taskInstance.getActorId()));	
	}

	
    private boolean ownerEmailNeeded() {
        return (!isAffiliate() && (!EMPTY_STRING.equals(taskInstance.getActorId()) && !getLoggedUserName().equals(taskInstance.getActorId())));
    }

    private boolean isRealUser(String assignedActorId) {
        return (assignedActorId != null && !EMPTY_STRING.equals(assignedActorId) && !"DMGCO".equals(assignedActorId) && !"DSCL".equals(assignedActorId));
    }

    private boolean isInDmgSubProcess(String currentActivity) {
        return currentActivity.equals(DMG_ASSESS.getTaskName())
                || currentActivity.equals(DMG_DATA_SEARCH.getTaskName())
                || currentActivity.equals(DMG_CLARIFICATION.getTaskName())
                || currentActivity.equals(DMG_REVIEW.getTaskName());

    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Query getQuery() {
        return query;
    }

    public TaskInstance getTaskInstance() {
        return taskInstance;
    }
    
    public String getCurrentOwner() {
        return taskInstance.getActorId() == null ? taskInstance.getActorId() : (taskInstance.getActorId().equals("DSCL") || taskInstance.getActorId().equals("DMGCO")) ? EMPTY_STRING : taskInstance.getActorId();
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public boolean isDisplayUpdateResponseDate() {
        return displayUpdateResponseDate;
    }

    public boolean isDisplayUpdateDMGDate() {
        return displayUpdateDMGDate;
    }

    public boolean isDisplayRestartWorkflow() {
        return displayRestartWorkflow;
    }

    public boolean isDisplayDispatch() {
        return displayDispatch;
    }

    public boolean isDisplayContactOwner() {
        return displayContactOwner;
    }

    public boolean isDisplayFinalResponseSent() {
        return displayFinalResponseSent;
    }

    public boolean isDisplayAmendQuery() {
        return displayAmendQuery;
    }

    public boolean isDisplayDeleteQuery() {
        return displayDeleteQuery;
    }

    public boolean isDisplayWaitingForVerification() {
        return displaywaitingForVerification;
    }

    public List<TaskInstance> getTasks() {
        return tasks;
    }

	public String getSourceSearch() {
		return sourceSearch;
	}

	public void setSourceSearch(String sourceSearch) {
		this.sourceSearch = sourceSearch;
	}
}
