package com.roche.dss.qtt.query.web.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import com.roche.dss.qtt.model.LdocType;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.workflow.WorkflowService;
import com.roche.dss.qtt.service.workflow.exception.TaskAlreadyCompletedException;
import com.roche.dss.qtt.utility.Transitions;
import com.roche.dss.util.ApplicationUtils;

/**
 * @author zerkowsm
 *
 */
@Conversion()
public class QuerySummaryAction extends CommonActionSupport {
	
	private static final long serialVersionUID = -7422619389525709835L;

	private static final Logger logger = LoggerFactory.getLogger(QuerySummaryAction.class);
	
	public static final String DRAFT_DOC = "draftDoc";
	
    private Query query;
	private long queryId;
	private long taskId;
	private String taskName;
	private String draft;
	private boolean finalDocWarning;
    private boolean affiliateAccess;
    private Date date;
    private List<LdocType> labellingDocs = new ArrayList<LdocType>();
    private String labellingDoc;
    private String summary;
    private DateTime expiryDate;
    
    private static final String DRAFT = "draft";
        
	private WorkflowService workflowService;	
	private QueryService queryService;
    
    @Resource
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }
    
    @Resource
    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }
    

	@SkipValidation
    public String execute() {
    	if (!(isDscl() || isPS() || isDMGScientist())) {
			return DENIED;
		}
		
    	query = queryService.find(queryId);
    	if (query == null) {
	    	return ERROR;
	    }
		
    	boolean isFinalDocAttachedInCurrentInteration = queryService.isFinalDocAttachedInCurrentInteraction(queryId, query.getProcessInstanceId());
    	        
        affiliateAccess = ApplicationUtils.convertToBoolean(String.valueOf(query.getAffiliateAccess()));
        
        labellingDocs = dictionaryService.getUsedLabellingDocs();
        
        summary = getSummary(query);
        
        if(query.getExpiryDate() == null) {
            Calendar now = Calendar.getInstance();
        	if(WF_ROUTE_B.equals(query.getWorkflowRouteType())){
        		labellingDoc = "1";
        		now.add(Calendar.YEAR, 1);
        	} else {
        		labellingDoc = "6";
        		now.add(Calendar.DATE, 1);
        	}
            date = now.getTime();
        } else {
        	date = query.getExpiryDate().toDate();      
        	Set<LdocType> lDoc = query.getLabelingDocTypes();       	
            if (lDoc != null && !lDoc.isEmpty()){
            	labellingDoc = String.valueOf(lDoc.iterator().next().getLdocTypeId());
            }
        }      

        //IMPOSSIBLE HERE ?!?! //(in the previous version)
//        if ((WF_ROUTE_B.equals(query.getWorkflowRouteType()) && DMG_DATA_SEARCH.equals(taskName))) {
//        	attachmentNoteWarning = true;
//        } else 
        	
        if (!isFinalDocAttachedInCurrentInteration) {
        	finalDocWarning = true;
        }
    	    	
    	return SUCCESS;
    }
    
    public String update() {
    	
    	boolean isFinalDocWarning = false;
    	boolean isUpdate = false;
    	
    	
    	   if(query.getExpiryDate() == null) {
               Calendar now = Calendar.getInstance();
           	if(WF_ROUTE_B.equals(query.getWorkflowRouteType())){
           		now.add(Calendar.YEAR, 1);
           	} else {
           		now.add(Calendar.DATE, 1);
           	}
               date = now.getTime();
           } else {
           	date = query.getExpiryDate().toDate();      
           }      
    	
    	    	
		if ((draft != null) && (DRAFT.equals(draft) || DRAFT_DOC.equals(draft))) {
			if (DRAFT_DOC.equals(draft)) {
				isFinalDocWarning = true;
			}
			isUpdate = true;
			if (logger.isDebugEnabled()) {
				logger.debug("Final response for query: " + queryId + " - update only. ");
			}
		}
		
		String leavingTransition = null;
		
		if(WF_ROUTE_A1.equals(query.getWorkflowRouteType()) || WF_ROUTE_A2.equals(query.getWorkflowRouteType())) {
			leavingTransition = Transitions.COMPLETE_DMG_SEARCH_DATA.getTransitionName();
		} else {
			leavingTransition = Transitions.COMPOSE_QUERY_SUMMARY.getTransitionName();
		}
		
		Set<LdocType> labellingDocs = queryService.getLabellingDocTypes(queryId);
		
		try {
			if (labellingDocs != null && !labellingDocs.isEmpty()) {
				if (!labellingDoc.equals(String.valueOf(labellingDocs.iterator().next().getLdocTypeId()))) {
					workflowService.updateSummaryResponseAndComplete(isUpdate, leavingTransition,
							getLoggedUserName(), taskId, queryId, expiryDate,
							summary, affiliateAccess, true, String.valueOf(labellingDocs.iterator().next()
									.getLdocTypeId()), labellingDoc);
				} else {
					workflowService.updateSummaryResponseAndComplete(isUpdate, leavingTransition,
							getLoggedUserName(), taskId, queryId, expiryDate,
							summary, affiliateAccess, false, null,
							null);
				}
			} else {
				workflowService.updateSummaryResponseAndComplete(isUpdate, leavingTransition,
						getLoggedUserName(), taskId, queryId, expiryDate, summary,
						affiliateAccess, true, null, labellingDoc);
			}	
		} catch (TaskAlreadyCompletedException e) {
			return TASK_ALREADY_COMPLETED;
		} catch (UnsupportedOperationException e) {
			return ERROR;
		}

		if (isFinalDocWarning) {
			return DRAFT_DOC;
		}
		
		return SUCCESS;
	}
	
	@Override
	public void validate() {
		if(EMPTY_STRING.equals(labellingDoc)) {
			addActionError(getText(getText("field.required", new String[]{"Labelling document"})));
		}
		if(summary == null || EMPTY_STRING.equals(summary)) {
			addActionError(getText(getText("field.required", new String[]{"Query Summary"})));
		}
//		try {
//			expiryDate = new DateTime(date);
//			if (expiryDate == null) {
//				addActionError(getText("field.required", new String[]{"Expiry Date"}));
//			} else if (expiryDate.isBefore(new DateTime().toDateMidnight())) {
//				addActionError(getText("query.summary.expiryDate.later"));
//			}
//		} catch (IllegalArgumentException e) {
//			addActionError(getText("date.format", new String[]{"Expiry Date"}));
//		}
		super.validate();
	}
    
    private String getSummary(Query query) {
        String queryLabel = query.getQueryLabel();
    	String summary = query.getQuerySummary();
		if(summary != null && queryLabel != null && !queryLabel.trim().isEmpty()) {
			if (!queryLabel.equalsIgnoreCase(summary)) {
                summary = " " + queryLabel + " \n" + query.getQuerySummary();
            }
        } else {
        	summary = " " + queryLabel + " \n";//WTF - we display null!? (same as in the previous version)
        }
        return summary;    	
    }
    
	public Query getQuery() {
		return query;
	}

	public void setQuery(Query query) {
		this.query = query;
	}

	public long getQueryId() {
		return queryId;
	}

	public void setQueryId(long queryId) {
		this.queryId = queryId;
	}

	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	
	public String getDraft() {
		return draft;
	}

	public void setDraft(String draft) {
		this.draft = draft;
	}

	public boolean isFinalDocWarning() {
		return finalDocWarning;
	}

	public void setFinalDocWarning(boolean finalDocWarning) {
		this.finalDocWarning = finalDocWarning;
	}

	public boolean isAffiliateAccess() {
		return affiliateAccess;
	}

	public void setAffiliateAccess(boolean affiliateAccess) {
		this.affiliateAccess = affiliateAccess;
	}

	public List<LdocType> getLabellingDocs() {
		return labellingDocs;
	}

	public Date getDate() {
		return date;
	}

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setDate(Date date) {
		this.date = date;
	}

	public void setLabellingDocs(List<LdocType> labellingDocs) {
		this.labellingDocs = labellingDocs;
	}

	public String getLabellingDoc() {
		return labellingDoc;
	}

	public void setLabellingDoc(String labellingDoc) {
		this.labellingDoc = labellingDoc;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

}
