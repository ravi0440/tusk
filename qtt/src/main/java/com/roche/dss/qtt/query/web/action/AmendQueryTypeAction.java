package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.query.web.utils.UserDetailsAware;
import com.roche.dss.qtt.security.user.RocheUserDetails;
import com.roche.dss.qtt.service.InitiateService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.utility.QueryStatusCodes;
import com.roche.dss.qtt.utility.config.Config;
import java.util.List;
import java.util.Map;
@ParentPackage("default")
@Action(value = "AmendQueryType", results = {
        @Result(name = "input",  location = "/qtt/initiate/amend_query_type.jsp"),
        @Result(name = "success", location = "/qtt/initiate/query_type_success.jsp"),
        @Result(name = "failure", location = "/qtt/initiate/amend_query_type.jsp"),
        @Result(name = "already_processed", type = "tiles", location = "/already_processed"),
        @Result(name = "error", location = "/qtt/global/error.jsp")
})
public class AmendQueryTypeAction extends CommonActionSupport implements UserDetailsAware, SessionAware {


    private static final Logger logger = LoggerFactory.getLogger(AmendQueryTypeAction.class);

    @Autowired
    protected Config config;
    @Autowired
    private InitiateService initiateService;
    @Autowired
    QueryService queryService;

    List<QueryType> qTypes;

    private RocheUserDetails userDetails;
    private Map<String, Object> session;
    private String sessionQueryTypeCode;
    private String queryType;


    public String execute() throws Exception {

        //security check
        boolean allowed = isDscl();
        // retrieve user information
        if (!allowed) {
            logger.error("AmendQueryTypeAction:Access denied.");
            return ERROR;
        }

        //retrieve query type id from the session
        Long queryId = (Long) session.get(ActionConstants.INITIATE_QUERY_SESSION_NAME);
        Query query = queryService.find(queryId);
        if (query == null) {
            logger.error("Query object could not be located from the session!");
            return ERROR;
        }
        sessionQueryTypeCode = query.getQueryType().getQueryTypeCode().name();

        logger.debug("The session id is: " + sessionQueryTypeCode + " and the form id is:" + queryType);

        //InitiateFacadeBD bd = (InitiateFacadeBD)BusinessDelegateFactory.getFactory().getServiceBD(mapping);

        // change due to bug fix #197 - perform safety-check to see if query is still in state 'SUBM'
        if (queryService.checkHasStatus(query.getQuerySeq(), QueryStatusCodes.SUBMITTED.getCode())) {
            //sendEmail(query, userEmail);
            initiateService.amendQueryType(query.getQuerySeq(), QueryType.Code.valueOf(sessionQueryTypeCode), QueryType.Code.valueOf(queryType));

            //set the query id back into the request object to be used by jsp
            //--request.setAttribute("queryId",String.valueOf(query.getQueryNumber()));

            //set the mailto parameters to the request object
            //openMailTo(query, request);
        } else {
            // if query has already been accepted / rejected / amended, show "please refresh list" message
            return "already_processed";
        }
        logger.info("AmendQueryTypeAction: user:" + userDetails.getUsername() + " query:" + query.getQueryNumber());
        return SUCCESS;
    }

    public List<QueryType> getQTypes() {
        return initiateService.getQueryTypes();
    }

    @Override
    public void validate() {
        logger.debug("validate");
        Long queryId = (Long) session.get(ActionConstants.INITIATE_QUERY_SESSION_NAME);
        Query query = queryService.find(queryId);
        if (query != null) {
            sessionQueryTypeCode = query.getQueryType().getQueryTypeCode().name();
        }
        if (sessionQueryTypeCode.equals(queryType)) {
            addActionError(getText("initiate.queryType.displayname"));
        }
        if (queryType == null) {
            addActionError(getText("qrfForm.queryType.displayname"));
        }
    }

/*    //not used in the reference version of QTT 3.6
	public void openMailTo(QueryDTO query, HttpServletRequest req) {

		//send email
		String from = "";
		String to = "";
		String subject = "";
		String body = "";


		String queryLabel = "";
        if(query.getRequesterQueryLabel() != null){
            queryLabel = " - [" + query.getRequesterQueryLabel() + "]";
        }
		from = config.getInititateDsclEmail();
		//BUG FIX #323
		subject = config.getInititateDsclEmailSubject()
					+" "
					+ query.getQueryNumber()
                    + queryLabel;

		body = 		config.getInititateAmendBody1Para()
					+ config.getInititateAmendBody2Para()
					+ query.getQueryNumber() + ". "
					+ config.getInititateAmendBody3Para()
					+ config.getInititateAmendBodyLastPara();


		to = query.getRequester().getEmailAddress();

		//set the attributes to open a mailto: using client email application e.g. outlook
		req.setAttribute("initiate.amend.mailto","true");
		req.setAttribute("initiate.amend.to",to);
		req.setAttribute("initiate.amend.subject",subject);
		req.setAttribute("initiate.amend.body",body);

		logger.info("AmendQueryTypeAction: Opening mailto: for query:" + query.getQueryNumber() + " email:" + to);
	}*/


    @Override
    public void setUserDetails(RocheUserDetails userDetails) {
        this.userDetails = userDetails;
    }


    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
}
