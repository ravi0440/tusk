/* 
====================================================================
  $Header$
  @author $Author: komisarp $
  @version $Revision: 2453 $ $Date: 2012-06-27 16:07:46 +0200 (Śr, 27 cze 2012) $

====================================================================
*/
package com.roche.dss.qtt.query.comms.dto;


public class CumulativePeriodDTO  {
	
	private boolean isCumulative;
	private long fromDate;
	private long toDate;
	private String interval;

    /**
     * @return
     */
    public long getFromDate() {
        return fromDate;
    }

    /**
     * @return
     */
    public String getInterval() {
        return interval;
    }

    /**
     * @return
     */
    public boolean isCumulative() {
        return isCumulative;
    }

    /**
     * @return
     */
    public long getToDate() {
        return toDate;
    }

    /**
     * @param l
     */
    public void setFromDate(long l) {
        fromDate = l;
    }

    /**
     * @param string
     */
    public void setInterval(String string) {
        interval = string;
    }

    /**
     * @param b
     */
    public void setCumulative(boolean b) {
        isCumulative = b;
    }

    /**
     * @param l
     */
    public void setToDate(long l) {
        toDate = l;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (fromDate ^ (fromDate >>> 32));
		result = prime * result
				+ ((interval == null) ? 0 : interval.hashCode());
		result = prime * result + (isCumulative ? 1231 : 1237);
		result = prime * result + (int) (toDate ^ (toDate >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CumulativePeriodDTO))
			return false;
		CumulativePeriodDTO other = (CumulativePeriodDTO) obj;
		if (fromDate != other.fromDate)
			return false;
		if (interval == null) {
			if (other.interval != null)
				return false;
		} else if (!interval.equals(other.interval))
			return false;
		if (isCumulative != other.isCumulative)
			return false;
		if (toDate != other.toDate)
			return false;
		return true;
	}

}
