package com.roche.dss.qtt.utility.pdf;

import com.roche.dss.qtt.eao.QttCountryEAO;
import com.roche.dss.qtt.model.*;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.Constants;
import com.roche.dss.qtt.query.web.qrf.model.QueryReviewDisplayModel;
import com.roche.dss.qtt.service.QueryService;
import java.util.ArrayList;
import java.util.Set;

@Component
public class QueryDetails implements QueryDetailsInterface {

     private static final Logger logger = LoggerFactory.getLogger(QueryDetails.class);
    @Autowired
    private QueryService queryService;

    @Autowired
    private QttCountryEAO qttCountryEAO;

    /* (non-Javadoc)
      * @see com.roche.dss.qtt.utility.pdf.QueryDetailsIface#buildQueryDetails(int)
      */
    @Override
	public QueryReviewDisplayModel buildQueryDetails(int queryId)
            throws NumberFormatException {

        //QueryDAO queryDao = null;
        //fetch query detsils for this query id
        //if (queryDao == null)
        //queryDao = new QueryDAO();

        Query query;
        query = queryService.getPreviewQueryDetails(Long.valueOf(queryId));

        // ### bug fix #278 - attempting to open an already-rejected query (2x
        // DSCL users, one using a stale initiate-list!)
        if (query == null) {
            return null;//TODO
        }
        // ### end of fix.

        return buildDisplayBean(query);
    }

    private QueryReviewDisplayModel buildDisplayBean(Query query) {

        QueryReviewDisplayModel qdModel = new QueryReviewDisplayModel();

        populateRequester(query, qdModel);

        //populate preparatory if organisation type is roche_affiliate
        if (query.getRequester().getOrganisation().getOrganisationType().getTypeId() == ActionConstants.ROCHE_AFFILIATE) {
            populatePreparatory(query, qdModel);
        }
        QttCountry sCountry = qttCountryEAO.getByIdObj(query.getSourceCountryCode());
        qdModel.setSourceCountry(sCountry == null ? "" : sCountry.getDescription());
        qdModel.setReporterType(query.getReporterType().getReporterType());

        if (!query.isAllDrugs()) {
            qdModel.setAllDrugs(ActionConstants.NO);
            populateDrugs(query, qdModel);
        } else
            qdModel.setAllDrugs(ActionConstants.YES);
        qdModel.setDateRequested(DateTimeFormat.forPattern("dd-MMM-yyyy").print(query.getDateRequested()));
        qdModel.setUrgency(query.getUrgencyReason());

        //populate attachment
        populateAttachments(query, qdModel);

        //do cumulative
        populateCumulative(query, qdModel);

        qdModel.setQueryType(query.getQueryType().getQueryType());
        qdModel.setFollowUp(query.getFollowUpNumber());
        qdModel.setFollowupNumber(query.getFollowupNumber());
        qdModel.setQueryNumber(query.getQueryNumber());
        qdModel.setQueryTypeCode(query.getQueryType().getQueryTypeCode().name());
        qdModel.setQueryNumber(query.getQueryNumber());

        //do query type details
        populateQueryTypeDetails(query, qdModel);
        return qdModel;
    }

    private void populateQueryTypeDetails(Query query,
                                          QueryReviewDisplayModel qdModel) {

    	QueryType.Code queryTypeCode = query.getQueryType().getQueryTypeCode();
        //based on the query type id fetch the information from query and
        // populate the display qdModel
        switch (queryTypeCode) {

            case DRUG_EVENT:
                populateQueryDesc(query, qdModel);
                if (query.getAeTerm()!=null)
                qdModel.setAeTerm(query.getAeTerm().getDescription());
                populateCases(query, qdModel);
                qdModel.setAdditionalComments(query.getCaseComment());
                break;
            case DRUG_INTERACTION:
                populateDrugList(query, qdModel);
                populateQueryDesc(query, qdModel);
                populateCases(query, qdModel);
                qdModel.setAdditionalComments(query.getCaseComment());
                break;
            case MANUFACTURING:
                populateQueryDesc(query, qdModel);
                qdModel.setBatch(query.getMnftBatchNumber());
                populateCases(query, qdModel);
                qdModel.setAdditionalComments(query.getCaseComment());
                if (query.getCaseOformat()!=null) {
                    qdModel.setOutputFormat(query.getCaseOformat().getCaseOformat());
                }
                qdModel.setOutputOther(query.getOformatSpecialRequest());
                break;
            case PREGNANCY:
                populateQueryDesc(query, qdModel);
                if (query.getPregnancy() != null) {
                    if (query.getPregnancy().getPrgyExptype() != null) {
                        qdModel.setExposureType(query.getPregnancy().getPrgyExptype().getExposureType());
                    }
                    qdModel.setTiming(query.getPregnancy().getTimeInPregnancy());
                    qdModel.setOutcome(query.getPregnancy().getOutcome());
                }
                populateCases(query, qdModel);
                qdModel.setAdditionalComments(query.getCaseComment());
                if (query.getCaseOformat() != null) {
                    qdModel.setOutputFormat(query.getCaseOformat().getCaseOformat());
                }
                qdModel.setOutputOther(query.getOformatSpecialRequest());
                break;
            case EXTERNAL_AUDIT:
                populateQueryDesc(query, qdModel);
                if (query.getAeTerm() != null) {
                	qdModel.setAeTerm(query.getAeTerm().getDescription());
                }
                populateClinicalTrial(query, qdModel);
                qdModel.setOutputOther(query.getOformatSpecialRequest());
                break;
            case CASE_CLARIFICATION:
            case CASE_DATA_REQUEST:
                populateCases(query, qdModel);
                qdModel.setAdditionalComments(query.getCaseComment());
                if (query.getCaseOformat() != null) {
                    qdModel.setOutputFormat(query.getCaseOformat().getCaseOformat());
                }
                qdModel.setOutputOther(query.getOformatSpecialRequest());
                break;
            case SAE_RECONCILIATION:
                populateClinicalTrial(query, qdModel);
                qdModel.setOutputOther(query.getOformatSpecialRequest());
                break;
            case IND_UPDATES:
                qdModel.setIndNumber(query.getIndNumber());
                break;
            case PERFORMANCE_METRICS:
                populateQueryDesc(query, qdModel);
                break;
            case SIGNAL_DETECTION:
            case LITERATURE_SEARCH:
                populateQueryDesc(query, qdModel);
                if (query.getAeTerm()!=null)
                qdModel.setAeTerm(query.getAeTerm().getDescription());
                break;
            case PERFORMANCE_METRICS2:
            	qdModel.setPerformanceMetrics(query.getPerformanceMetric());
            	break;
            case EXTERNAL_AUDIT_INSPECTION:
            	qdModel.setAudit(query.getAudit());
            	qdModel.setOutputOther(query.getOformatSpecialRequest());
                populateQueryDesc(query, qdModel);
            	populateClinicalTrial(query, qdModel);
            	break;
            case MANUFACTURING_RECALL:
                populateQueryDesc(query, qdModel);
            	populateClinicalTrial(query, qdModel);
                qdModel.setManufacturing(query.getManufacturing());
                qdModel.setAeTerm(query.getAeTerm().getDescription());
                qdModel.setOutputOther(query.getOformatSpecialRequest());
            	break;
            case DATA_SEARCH:
                populateQueryDesc(query, qdModel);
            	populateClinicalTrial(query, qdModel);
            	qdModel.setDataSearch(query.getDataSearch());
                if (query.getCaseOformat() != null) {
                    qdModel.setOutputFormat(query.getCaseOformat().getCaseOformat());
                }
                qdModel.setOutputOther(query.getOformatSpecialRequest());
                qdModel.setAdditionalComments(query.getCaseComment());
            	break;
            case MEDICAL:
                populateQueryDesc(query, qdModel);
                if (query.getPregnancy() != null) {
                    if (query.getPregnancy().getPrgyExptype() != null) {
                        qdModel.setExposureType(query.getPregnancy().getPrgyExptype().getExposureType());
                    }
                    qdModel.setTiming(query.getPregnancy().getTimeInPregnancy());
                    qdModel.setOutcome(query.getPregnancy().getOutcome());
                    qdModel.setComments(query.getPregnancy().getComments());
                }
                populateCases(query, qdModel);
                populateDrugList(query, qdModel);
                qdModel.setAdditionalComments(query.getCaseComment());
                qdModel.setOutputOther(query.getOformatSpecialRequest());
            	break;
            default:
                //do nothing;
                break;
        }

    }

    private void populateQueryDesc(Query query, QueryReviewDisplayModel qdModel) {
        if (query.getQueryDescription() != null) {
            qdModel.setNatureOfCase(query.getQueryDescription().getNatureOfCase());
            qdModel.setIndexCase(query.getQueryDescription().getIndexCase());
            qdModel.setSignsSymp(query.getQueryDescription().getSignAndSymptom());
            qdModel.setInvestigations(query.getQueryDescription().getInvestigation());
            qdModel.setPmh(query.getQueryDescription().getPrevMedicalHistory());
            qdModel.setConMeds(query.getQueryDescription().getConmed());
            qdModel.setOtherInfo(query.getQueryDescription().getOtherInfo());
            qdModel.setDiagnosis(query.getQueryDescription().getDiagnosis());
        }
    }

    private void populateCases(Query query, QueryReviewDisplayModel qdModel) {
        qdModel.setCases(new ArrayList());
        qdModel.getCases().addAll(query.getCases());
    }

    private void populateDrugList(Query query, QueryReviewDisplayModel qdModel) {
        qdModel.setDrugs(new ArrayList());
        qdModel.getDrugs().addAll(query.getDrugs());
    }

    private void populateClinicalTrial(Query query, QueryReviewDisplayModel qdModel) {

        qdModel.setProtocolNumber(query.getTrial().getProtocolNumber());
        qdModel.setCrtnNumber(query.getTrial().getCrtnNumber());
        qdModel.setPatientNumber(query.getTrial().getPatientNumber());
        qdModel.setCaseSelection(query.getTrial().getCaseSelection());
        if (query.getTrial().getCtrlOformat() != null) {
        	qdModel.setOutputFormatDesc(query.getTrial().getCtrlOformat().getCtrlOformat());
        }
    }

    private void populateRequester(Query query, QueryReviewDisplayModel qdModel) {

        qdModel.setFirstname(query.getRequester().getFirstName());
        qdModel.setLastname(query.getRequester().getSurname());
        qdModel.setTelephone(query.getRequester().getTelephone());
        qdModel.setFax(query.getRequester().getFaxNumber());
        qdModel.setEmail(query.getRequester().getEmail());
        try {
        qdModel.setReqCountry(query.getRequester().getCountry().getDescription());
        } catch (EntityNotFoundException ent) {
            logger.warn("query.getRequester().getCountry() threw EntityNotFoundException...");
        }
        qdModel.setOrgName(query.getRequester().getOrganisation().getName());
        qdModel.setOrgType(query.getRequester().getOrganisation().getOrganisationType().getType());
    }

    private void populateDrugs(Query query, QueryReviewDisplayModel qdModel) {

        Drug drug = query.getDrugForPreview();
        qdModel.setRetrDrug(drug.getDrugRetrievalName());
        qdModel.setGenericDrug(drug.getInnGenericName());
        qdModel.setRoute(drug.getRoute());
        qdModel.setFormulation(drug.getFormulation());
        qdModel.setIndication(drug.getIndication());
        qdModel.setDose(drug.getDose());
        //bug fix 346
        //qdModel.setOthersDrug(query.getQuerySummary());
        qdModel.setOthersDrug(drug.getExtraFirstDrug());
    }

    private void populateCumulative(Query query, QueryReviewDisplayModel qdModel) {
        RetrievalPeriod cummDto = query.getRetrievalPeriod();
        if (cummDto!= null) {
        qdModel.setCumulative(Constants.YES.toString().equals(cummDto.getCumulativeFlag()) ? ActionConstants.YES : ActionConstants.NO);
        qdModel.setDateFrom(DateTimeFormat.forPattern("dd-MMM-yyyy").print(cummDto.getStartTs()));
        qdModel.setDateTo(DateTimeFormat.forPattern("dd-MMM-yyyy").print(cummDto.getEndTs()));
        qdModel.setInterval(cummDto.getInterval());
        }
    }

    private void populateAttachments(Query query, QueryReviewDisplayModel qdModel) {
        qdModel.setAttachments(new ArrayList());
        qdModel.getAttachments().addAll(query.getAttachments());
    }

    private void populatePreparatory(Query query, QueryReviewDisplayModel qdModel) {

        Set<PreQueryPreparation> prepList = query.getPreQueryPreparations();
        if ((prepList != null) && (!prepList.isEmpty())) {
            for (PreQueryPreparation prepDto : prepList) {
                switch (prepDto.getId().getPqpTypeId()) {

                    case ActionConstants.REPOSITORY:
                        qdModel.setRepository(ActionConstants.YES);
                        break;
                    case ActionConstants.PSURS:
                        qdModel.setPsur(ActionConstants.YES);
                        break;
                    case ActionConstants.CDS:
                        qdModel.setCds(ActionConstants.YES);
                        break;
                    case ActionConstants.ISSUE_WORKUPS:
                        qdModel.setIssue(ActionConstants.YES);
                        break;
                    case ActionConstants.LITERATURE:
                        qdModel.setLiterature(ActionConstants.YES);
                        break;
                    case ActionConstants.OTHERS:
                        qdModel.setOthers(ActionConstants.YES);
                        qdModel.setOthersComment(prepDto.getOtherComment());
                        break;
                    default:
                        break;
                }//end switch
            }//end for
        }//end if
        //also set the comments
        qdModel.setComments(query.getPreQueryPrepComment());
    }

}