package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.EmailHistoryEAO;
import com.roche.dss.qtt.eao.QueryWorkflowHistoryEAO;
import com.roche.dss.qtt.model.EmailHistory;
import com.roche.dss.qtt.model.QueryWorkflowHistory;

@Repository("queryWorkflowHistoryEAO")
@Transactional
public class QueryWorkflowHistoryEAOImpl extends AbstractBaseEAO<QueryWorkflowHistory> implements QueryWorkflowHistoryEAO {

	@Override
	public void save(QueryWorkflowHistory entity) {
		this.persist(entity);
		this.flush();
	}
	
	
	

}