package com.roche.dss.qtt.query.comms.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;
import com.roche.dss.qtt.eao.QueryEAO;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryStatusCode;
import com.roche.dss.qtt.query.comms.dao.RepositorySearchDAO;
import com.roche.dss.qtt.query.web.model.RepositorySearchParams;
import com.roche.dss.qtt.service.SearchResults;
import com.roche.dss.util.ApplicationUtils;

import static com.roche.dss.qtt.utility.Activities.*;

@Repository("repositorySearchDAO")
public class RepositorySearchDAOImpl implements RepositorySearchDAO {
	public static int MAX_RESULTS = 100;
	
    @PersistenceContext
    protected EntityManager entityManager;
    
    @Autowired
    protected QueryEAO queryEAO;
    
    protected static final Logger log = LoggerFactory.getLogger(RepositorySearchDAOImpl.class);
    
    protected JpaTemplate jpaTemplate;
    
    @PostConstruct
    public void init() {
        jpaTemplate = new JpaTemplate(entityManager);
    }
    
	@Override
    @SuppressWarnings("unchecked")
	public SearchResults search(final RepositorySearchParams searchParams) {
    	SearchResults results = jpaTemplate
        		.execute(new JpaCallback<SearchResults> ()
        		{
                    public SearchResults doInJpa(EntityManager em) throws PersistenceException
    		        {
                    	Session s = (Session) em.getDelegate();
                    	Criteria c = s.createCriteria(Query.class);
                    	Criteria cCount = s.createCriteria(Query.class);
                    	setUpCriteria(c, searchParams);
                    	setUpCriteria(cCount, searchParams);
                    	
                    	int maxResults = searchParams.getMaxResults() != null ? searchParams.getMaxResults() : MAX_RESULTS;
               			c.setMaxResults(maxResults);
               			c.setFetchSize(maxResults);
                		if (searchParams.getFirstResult() != null) {
                			c.setFirstResult(searchParams.getFirstResult());
                		}

                		cCount.setProjection(Projections.countDistinct("querySeq"));
                    	int allResultsCount = ((Long)cCount.list().get(0)).intValue();
                    	
                    	// this is the cure for an improperly imposed maxResults
                    	// c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY) need not be used
                    	List<Query> queries = c.list();

                    	return new SearchResults (queries, allResultsCount);
    		        }
        		});
        	return results;
	}
    
    public DetachedCriteria createMemberCriteria(String alias, String[] members, String[] wtpSeq) {
//        DetachedCriteria dc = DetachedCriteria.forClass(Query.class).setProjection(Projections.id());
//        dc = dc.createAlias("auditHistorySet", alias);
//        dc = dc.add(Restrictions.conjunction());
//        dc = dc.add(Restrictions.in(alias + ".responsible", members));
//		DetachedCriteria cwp = dc.createCriteria(alias + ".qttWorkflowProcess");
//		cwp = cwp.add(Restrictions.in("wfProcessSeq", wtpSeq));

        DetachedCriteria dc = DetachedCriteria.forClass(Query.class).setProjection(Projections.id());
        dc = dc.createCriteria("processInstance").createAlias("taskInstances", alias);
        dc = dc.add(Restrictions.conjunction());
        dc = dc.add(Restrictions.in(alias + ".actorId", members)).add(Restrictions.in(alias + ".name", wtpSeq));
		return dc;
    }
    
    public boolean onlyOnePresent(boolean[] present) {
    	int presentCount = 0;
    	for (boolean p: present) {
    		if (p) {
    			++ presentCount;
    		}
    	}
    	if (presentCount == 1) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    private void addOrderBy(Criteria c, String prop, boolean asc) {
    	Criteria crit = c;
    	int i = 0;
    	String[] props = prop.split("\\.");
    	while (i < props.length - 1) {
    		crit = crit.createCriteria(props[i++]);
    	}
		Property orderBy = Property.forName(props[i]);
    	crit.addOrder(asc ? orderBy.asc() : orderBy.desc());
    }
    
    private Date endOfDay(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		return cal.getTime();   	
    }

    public void setUpCriteria (Criteria c, RepositorySearchParams searchParams) {
    	c = c.add(Restrictions.conjunction());
    	
		if (! ApplicationUtils.isEmptyArray(searchParams.getDrugRetrievalName())
				||
				! ApplicationUtils.isEmptyArray(searchParams.getInnGenericName())
				||
				! ApplicationUtils.isEmptyArray(searchParams.getIndication())
				||
				! ApplicationUtils.isEmptyString(searchParams.getInteractingDrugName())
			) {
			Criteria cd = c.createCriteria("drugs");
			
			if (ApplicationUtils.isEmptyString(searchParams.getInteractingDrugName())) {
				// we do not have InteractingDrugName
    			if (! ApplicationUtils.isEmptyArray(searchParams.getDrugRetrievalName())) {
    				cd = cd.add(Restrictions.in("drugRetrievalName", searchParams.getDrugRetrievalName()));
    			}
    			if (! ApplicationUtils.isEmptyArray(searchParams.getInnGenericName())) {
    				cd = cd.add(Restrictions.in("innGenericName", searchParams.getInnGenericName()));
    			}
    			if (! ApplicationUtils.isEmptyArray(searchParams.getIndication())) {
    				cd = cd.add(Restrictions.in("indication", searchParams.getIndication()));
    			}
			} else if (ApplicationUtils.isEmptyArray(searchParams.getDrugRetrievalName())
					&&
					ApplicationUtils.isEmptyArray(searchParams.getInnGenericName())
					&&
					ApplicationUtils.isEmptyArray(searchParams.getIndication())
				) {
				// we only have interactingDrugName
				cd = cd.add(Restrictions.ilike("innGenericName", searchParams.getInteractingDrugName(), MatchMode.ANYWHERE));
				cd = cd.add(Restrictions.eq("firstFlag", 'N'));
			} else {
				Conjunction conjunction = Restrictions.conjunction();
				
                DetachedCriteria detachedCriteria1 = DetachedCriteria.forClass(Query.class).setProjection(Projections.id());
                detachedCriteria1 = detachedCriteria1.createAlias("drugs", "drug1");
                detachedCriteria1 = detachedCriteria1.add(Restrictions.conjunction());
    			if (! ApplicationUtils.isEmptyArray(searchParams.getDrugRetrievalName())) {
    				detachedCriteria1 = detachedCriteria1.add(Restrictions.in("drug1.drugRetrievalName", searchParams.getDrugRetrievalName()));
    			}
    			if (! ApplicationUtils.isEmptyArray(searchParams.getInnGenericName())) {
    				detachedCriteria1 = detachedCriteria1.add(Restrictions.in("drug1.innGenericName", searchParams.getInnGenericName()));
    			}
    			if (! ApplicationUtils.isEmptyArray(searchParams.getIndication())) {
    				detachedCriteria1 = detachedCriteria1.add(Restrictions.in("drug1.indication", searchParams.getIndication()));
    			}                            

                DetachedCriteria detachedCriteria2 = DetachedCriteria.forClass(Query.class).setProjection(Projections.id());
                detachedCriteria2 = detachedCriteria2.createAlias("drugs", "drug2");
                detachedCriteria2 = detachedCriteria2.add(Restrictions.conjunction());
                detachedCriteria2 = detachedCriteria2.add(Restrictions.eq("drug2.innGenericName", searchParams.getInteractingDrugName()));
                detachedCriteria2 = detachedCriteria2.add(Restrictions.eq("drug2.firstFlag", 'N'));
                
                conjunction.add(Property.forName("id").in(detachedCriteria1)); 
                conjunction.add(Property.forName("id").in(detachedCriteria2));
                
                c = c.add(conjunction);
			}
		}
    	
		if (StringUtils.isNotBlank(searchParams.getQueryType())) {
			Criteria ct = c.createCriteria("queryType");
			ct = ct.add(Restrictions.eq("queryType", searchParams.getQueryType()));
		}
		
		if (! ApplicationUtils.isEmptyString(searchParams.getAeTermDescription())) {
			Criteria cae = c.createCriteria("aeTerm");
			String[] aeTermDescriptions = searchParams.getAeTermDescription().split(",");
			for(int i=0; i<aeTermDescriptions.length; i++ ){
				cae = cae.add(Restrictions.ilike("description", aeTermDescriptions[i].trim(), MatchMode.ANYWHERE));
			}			
		}
		
		if (! ApplicationUtils.isEmptyString(searchParams.getQueryDescriptionPrevMedicalHistory())) {
			Criteria cdesc = c.createCriteria("queryDescription");
			String[] queryDescriptionPrevMedicalHistories = searchParams.getQueryDescriptionPrevMedicalHistory().split(",");
			for(String medicalHistory : queryDescriptionPrevMedicalHistories ){				
				cdesc = cdesc.add(Restrictions.ilike("prevMedicalHistory", medicalHistory.trim(), MatchMode.ANYWHERE));
			}
		}
		
		if (! ApplicationUtils.isEmptyArray(searchParams.getAuthor())) {
			Criteria catt = c.createCriteria("attachments");
			
			catt = catt.add(Restrictions.in("author", searchParams.getAuthor()));
			catt = catt.add(Restrictions.eq("attachmentCategory.attachmentCategoryId", new Byte((byte)3)));
		}
    	
    	if (searchParams.getPeriodDateFrom() != null || searchParams.getPeriodDateTo() != null) {
    		Criteria crp = c.createCriteria("retrievalPeriod");
    		if (searchParams.getPeriodDateFrom() != null) {
        		crp = crp.add(Restrictions.ge("endTs", new DateTime(searchParams.getPeriodDateFrom().getTime())));
    		}
    		if (searchParams.getPeriodDateTo() != null) {
        		crp = crp.add(Restrictions.le("startTs", new DateTime(searchParams.getPeriodDateTo().getTime())));
    		}                		
    	}
        
    	if (! ApplicationUtils.isEmptyArray(searchParams.getLabelingDocTypeIds())) {
    		Criteria cld = c.createCriteria("labelingDocTypes");
    		cld = cld.add(Restrictions.in("ldocTypeId", searchParams.getLabelingDocTypeIds()));
    	}
    	
		if (! ApplicationUtils.isEmptyString(searchParams.getQuerySummary())) {
			c = c.add(Restrictions.ilike("querySummary", searchParams.getQuerySummary(), MatchMode.ANYWHERE));
		}
		
    	if (! ApplicationUtils.isEmptyArray(searchParams.getWorkflowRouteTypeIds())) {
    		c = c.add(Restrictions.in("workflowRouteType", searchParams.getWorkflowRouteTypeIds()));
    	}
    	
    	DetachedCriteria ah1 = null;
    	DetachedCriteria ah2 = null;
    	DetachedCriteria ah3 = null;
    	
    	boolean[] ahPresent = new boolean[5];
    	ahPresent[0] = ! ApplicationUtils.isEmptyArray(searchParams.getDmgMembers());
    	ahPresent[1] = ! ApplicationUtils.isEmptyArray(searchParams.getPsMembers());
    	ahPresent[2] = ! ApplicationUtils.isEmptyArray(searchParams.getDsclMembers());
   		ahPresent[3] = searchParams.getQryInitiateDateFrom() != null || searchParams.getQryInitiateDateTo() != null;
   		ahPresent[4] = searchParams.getQryFinalDeliveryDateFrom() != null || searchParams.getQryFinalDeliveryDateTo() != null;
    	boolean onlyOnePresent = onlyOnePresent(ahPresent);
    	
    	if (! ApplicationUtils.isEmptyArray(searchParams.getDmgMembers())) {
    		if (!onlyOnePresent) {
    			ah1 = createMemberCriteria("ah1", searchParams.getDmgMembers(), UNKNOWN.getDmgActivities());
    		} else {
        		Criteria cah = c.createCriteria("processInstance").createCriteria("taskInstances")
        		.add(Restrictions.in("actorId", searchParams.getDmgMembers()))
                .add(Restrictions.in("name", UNKNOWN.getDmgActivities()));
    		}
    	}
    	
    	if (! ApplicationUtils.isEmptyArray(searchParams.getPsMembers())) {
    		if (!onlyOnePresent) {
    			ah2 = createMemberCriteria("ah2", searchParams.getPsMembers(), UNKNOWN.getPsActivities());
    		} else {
                Criteria cah = c.createCriteria("processInstance").createCriteria("taskInstances")
        		.add(Restrictions.in("actorId", searchParams.getPsMembers()))
                .add(Restrictions.in("name", UNKNOWN.getPsActivities()));
    		}
    	}
    	
    	if (! ApplicationUtils.isEmptyArray(searchParams.getDsclMembers())) {
    		if (!onlyOnePresent) {
    			ah3 = createMemberCriteria("ah3", searchParams.getDsclMembers(), UNKNOWN.getDsclActivities());
    		} else {
                Criteria cah = c.createCriteria("processInstance").createCriteria("taskInstances")
        		.add(Restrictions.in("actorId", searchParams.getDsclMembers()))
                .add(Restrictions.in("name", UNKNOWN.getDsclActivities()));
    		}
    	}
    	
    	boolean timeDependent = false;
    	
    	if ("true".equals(searchParams.getNonExpiredResponsesOnly())) {
    		timeDependent = true;
    		DateTime now = new DateTime().dayOfMonth().roundFloorCopy();
    		
    		Criterion expiryNull = Restrictions.isNull("expiryDate");
    		Criterion notExpired = Restrictions.gt("expiryDate", now);
    		c = c.add(Restrictions.or(expiryNull, notExpired));
    	}
    	
		if (! ApplicationUtils.isEmptyString(searchParams.getRequesterSurname())
			||
			! ApplicationUtils.isEmptyArray(searchParams.getRequesterCountryCode())
			) {
			Criteria cr = c.createCriteria("requester");
			if (! ApplicationUtils.isEmptyString(searchParams.getRequesterSurname())) {
				cr = cr.add(Restrictions.eq("surname", searchParams.getRequesterSurname()));
			}
			if (! ApplicationUtils.isEmptyArray(searchParams.getRequesterCountryCode())) {
				Criteria crc = cr.createCriteria("country");
				crc = crc.add(Restrictions.in("countryCode", searchParams.getRequesterCountryCode()));
			}
		}
		
		if (! ApplicationUtils.isEmptyArray(searchParams.getReporterTypeId())) {
			Criteria crp = c.createCriteria("reporterType");
			crp = crp.add(Restrictions.in("reporterTypeId", searchParams.getReporterTypeId()));
		}
		
		if (! ApplicationUtils.isEmptyArray(searchParams.getSourceCountryCode())) {
			c = c.add(Restrictions.in("sourceCountryCode", searchParams.getSourceCountryCode()));
		}
    	
    	if (searchParams.getQryResponseDateFrom() != null || searchParams.getQryResponseDateTo() != null) {
    		DateTime to = null;
        	if (searchParams.getQryResponseDateTo() != null) {
        		to = new DateTime(endOfDay(searchParams.getQryResponseDateTo()).getTime());
        	}
        	
        	if (! ApplicationUtils.isEmptyArray(searchParams.getDmgMembers())) {
        		if (!onlyOnePresent) {
        			ah1 = createMemberCriteria("ah1", searchParams.getDmgMembers(), UNKNOWN.getDmgActivities());
        		} else {
                    Criteria cah = c.createCriteria("processInstance").createCriteria("taskInstances")
                    .add(Restrictions.in("actorId", searchParams.getDmgMembers()))
                    .add(Restrictions.in("name", UNKNOWN.getDmgActivities()));
        		}
        	}
        	
        	Criterion dsNull = Restrictions.isNull("datetimeSubmitted");
        	if (searchParams.getQryResponseDateFrom() != null && to != null) {
        		LogicalExpression dsLE = Restrictions.and(Restrictions.ge("datetimeSubmitted", new DateTime(searchParams.getQryResponseDateFrom().getTime())), Restrictions.le("datetimeSubmitted", to));
        		c = c.add(Restrictions.or(dsNull, dsLE));
        	} else if (searchParams.getQryResponseDateFrom() != null) {
        		SimpleExpression dsSE = Restrictions.ge("datetimeSubmitted", new DateTime(searchParams.getQryResponseDateFrom().getTime()));
        		c = c.add(Restrictions.or(dsNull, dsSE));
        	} else {
        		SimpleExpression dsSE = Restrictions.le("datetimeSubmitted", to);
        		c = c.add(Restrictions.or(dsNull, dsSE));                    		
        	}
    	}
    	
    	DetachedCriteria ah4 = null;
    	try {
	    	if (searchParams.getQryInitiateDateFrom() != null || searchParams.getQryInitiateDateTo() != null) {
	    		Date from = null;
	        	if (searchParams.getQryInitiateDateFrom() != null) {
	        		from = searchParams.getQryInitiateDateFrom();
	        	}
	    		Date to = null;
	        	if (searchParams.getQryInitiateDateTo() != null) {
	        		to = endOfDay(searchParams.getQryInitiateDateTo());
	        	}
	        	
	        	if (onlyOnePresent) {
	            	Criteria cah = c.createCriteria("processInstance").createCriteria("taskInstances")
	                .add(Restrictions.eq("name", JOB_START.getTaskName()));
	
	            	if (from != null && to != null) {
	            		LogicalExpression wpLE = Restrictions.and(Restrictions.ge("create", from), Restrictions.le("create", to));
	            		cah = cah.add(wpLE);
	            	} else if (from != null) {
	            		cah = cah.add(Restrictions.ge("create", from));
	            	} else {
	            		cah = cah.add(Restrictions.le("create", to));
	            	}
	        	}
	        	else {
	                ah4 = DetachedCriteria.forClass(Query.class).setProjection(Projections.id());
	                ah4 = ah4.createCriteria("processInstance").createCriteria("taskInstances", "ah4")
	        		.add(Restrictions.eq("ah4.name", JOB_START.getTaskName()));
	                
	            	if (from != null && to != null) {
	            		LogicalExpression wpLE = Restrictions.and(Restrictions.ge("ah4.create", from), Restrictions.le("ah4.create", to));
	            		ah4 = ah4.add(wpLE);
	            	} else if (from != null) {
	            		ah4 = ah4.add(Restrictions.ge("ah4.create", from));
	            	} else {
	            		ah4 = ah4.add(Restrictions.le("ah4.create", to));
	            	}	                		
	        	}
	    	}
    	} catch (NumberFormatException e) {
    		log.info("wrong QryInitiateDate");
    	}

    	DetachedCriteria ah5 = null;
    	try {
	    	if (searchParams.getQryFinalDeliveryDateFrom() != null || searchParams.getQryFinalDeliveryDateTo() != null) {
	    		Date from = null;
	        	if (searchParams.getQryFinalDeliveryDateFrom() != null) {
	        		from = searchParams.getQryFinalDeliveryDateFrom();
	        		
	        	}
	    		Date to = null;
	        	if (searchParams.getQryFinalDeliveryDateTo() != null) {
	        		to = endOfDay(searchParams.getQryFinalDeliveryDateTo());
	        	}
	        	
	        	if (onlyOnePresent) {
	                Criteria cah = c.createCriteria("processInstance").createCriteria("taskInstances")
	                .add(Restrictions.eq("name", SEND_TO_REQUESTER.getTaskName()));
	
	            	if (from != null && to != null) {
	            		LogicalExpression wpLE = Restrictions.and(Restrictions.ge("create", from), Restrictions.le("create", to));
	            		cah = cah.add(wpLE);
	            	} else if (from != null) {
	            		cah = cah.add(Restrictions.ge("create", from));
	            	} else {
	            		cah = cah.add(Restrictions.le("create", to));
	            	}
	        	}
	        	else {
	                ah5 = DetachedCriteria.forClass(Query.class).setProjection(Projections.id());
	                ah5 = ah5.createCriteria("processInstance").createCriteria("taskInstances", "ah5")
	        		.add(Restrictions.eq("ah5.name", SEND_TO_REQUESTER.getTaskName()));
	
	            	if (from != null && to != null) {
	            		LogicalExpression wpLE = Restrictions.and(Restrictions.ge("ah5.create", from), Restrictions.le("ah5.create", to));
	            		ah5 = ah5.add(wpLE);
	            	} else if (from != null) {
	            		ah5 = ah5.add(Restrictions.ge("ah5.create", from));
	            	} else {
	            		ah5 = ah5.add(Restrictions.le("ah5.create", to));
	            	}	                		
	        	}
	    	}
    	} catch (NumberFormatException e) {
    		log.info("wrong QryFinalDeliveryDate");
    	}
    	
    	if (ah1 != null || ah2 != null || ah3 != null || ah4 != null || ah5 != null) {
    		Conjunction conjunction = Restrictions.conjunction();
    		
    		if (ah1 != null) {
    			conjunction.add(Property.forName("id").in(ah1));
    		}
    		if (ah2 != null) {
    			conjunction.add(Property.forName("id").in(ah2));
    		}
    		if (ah3 != null) {
    			conjunction.add(Property.forName("id").in(ah3));
    		}
    		if (ah4 != null) {
    			conjunction.add(Property.forName("id").in(ah4));
    		}
    		if (ah5 != null) {
    			conjunction.add(Property.forName("id").in(ah5));
    		}
            
            c = c.add(conjunction);
    	}
		
		if (! ApplicationUtils.isEmptyString(searchParams.getRequesterQueryLabel())) {
			c = c.add(Restrictions.ilike("requesterQueryLabel", searchParams.getRequesterQueryLabel(), MatchMode.ANYWHERE));
		}
		
		if (! ApplicationUtils.isEmptyString(searchParams.getQueryNumber())) {
			c = c.add(Restrictions.ilike("queryNumber", searchParams.getQueryNumber(), MatchMode.ANYWHERE));
		}
    	
    	if (! timeDependent) {
    		c.setCacheable(true);
    	}
    	
		if (StringUtils.isNotBlank(searchParams.getOrderBy())) {
			addOrderBy(c, searchParams.getOrderBy(), searchParams.getAsc() == null || searchParams.getAsc());
		}
    }

}
