package com.roche.dss.qtt.query.comms.dao;

// DAO class for temporary usage - > change all methods into jpa

import static com.roche.dss.qtt.query.web.qrf.Constants.ROCHE_AFFILIATE;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import com.roche.dss.qtt.model.*;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.model.DataSearch.Frequency;
import com.roche.dss.qtt.model.DataSearch.RequestType;
import com.roche.dss.qtt.model.DataSearch.SearchType;
import com.roche.dss.qtt.query.comms.dao.impl.JDBCTemplateDAO;
import com.roche.dss.qtt.query.comms.dto.AttachmentDTO;
import com.roche.dss.qtt.query.comms.dto.CaseDTO;
import com.roche.dss.qtt.query.comms.dto.ClinicalTrialDTO;
import com.roche.dss.qtt.query.comms.dto.CumulativePeriodDTO;
import com.roche.dss.qtt.query.comms.dto.DrugDTO;
import com.roche.dss.qtt.query.comms.dto.OrganisationDTO;
import com.roche.dss.qtt.query.comms.dto.PartialQueryDTO;
import com.roche.dss.qtt.query.comms.dto.PregnancyDTO;
import com.roche.dss.qtt.query.comms.dto.PreparatoryDTO;
import com.roche.dss.qtt.query.comms.dto.QueryAssessmentDTO;
import com.roche.dss.qtt.query.comms.dto.QueryDTO;
import com.roche.dss.qtt.query.comms.dto.QueryDescDTO;
import com.roche.dss.qtt.query.comms.dto.QueryVerificationDTO;
import com.roche.dss.qtt.query.comms.dto.RequesterDTO;
import com.roche.dss.qtt.query.web.qrf.Constants;
import com.roche.dss.qtt.service.AttachmentService;
import com.roche.dss.qtt.service.FollowupQueryService;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
@Repository
public class QueryDAOImpl extends JDBCTemplateDAO implements QueryDAO {

    private static final Logger logger = LoggerFactory.getLogger(QueryDAOImpl.class);
    private String countryTab;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private FollowupQueryService followupQueryService;

    @Autowired
    private QueryService queryService;

    @Override
    public List<PartialQueryDTO> getPartialQueries(String email, String status) {
        String queryStr = "SELECT "
                + "datetime_submitted, email, "
                + "rt.reporter_type as rt_reporter_type, "
                + "qt.query_type as qt_query_type, query_seq FROM "
                + "queries q, reporter_types rt, query_types qt, "
                + "requesters r WHERE "
                + "q.reporter_type_id = rt.reporter_type_id"
                + " AND q.query_type_code = qt.query_type_code"
                + " AND q.requester_id = r.requester_id"
                + " AND UPPER(r.email) = ? and q.status_code = ?";
        List<Object> params = new ArrayList<Object>();
        params.add(email.toUpperCase());
        params.add(status);
        return jdbcTemplate.query(queryStr, params.toArray(new Object[params.size()]), new QueryDTORowMapper());
    }


    @Override
    public void deleteOldQueryType(int queryNumber, QueryType.Code oldQueryTypeCode) {

        //lets delete the old query type details for this query
        switch (oldQueryTypeCode) {

            case DRUG_EVENT:
                doDrugEvent(queryNumber);
                break;
            case DRUG_INTERACTION:
                doDrugInteraction(queryNumber);
                break;
            case MANUFACTURING:
                doManufacturing(queryNumber);
                break;
            case PREGNANCY:
                doPregnancy(queryNumber);
                break;
            case EXTERNAL_AUDIT:
                doExternalAudit(queryNumber);
                break;
            case CASE_CLARIFICATION:
            case CASE_DATA_REQUEST:
                doCaseClarification(queryNumber);
                break;
            case SAE_RECONCILIATION:
                doSAEReconciliation(queryNumber);
                break;
            case IND_UPDATES:
                doINDUpdates(queryNumber);
                break;
            case PERFORMANCE_METRICS:
                doPerformanceMetrics(queryNumber);
                break;
            case SIGNAL_DETECTION:
                doSignalDetection(queryNumber);
                break;
            case LITERATURE_SEARCH:
                doLiterature(queryNumber);
                break;
            default:
                //do nothing
                break;
        }

    }

    @Override
    public void updateQRF(QueryDTO query, int queryId, List<Integer> idsAttachmentsToRemove) {

        List<Object> params = new ArrayList<Object>();
        String sql = "";


        // sqlExec.setAutoCommit(false);

        // update the preparatory
        // FIRST DELETE ALL RECORDS
        sql = "DELETE FROM PRE_QUERY_PREPARATIONS WHERE query_seq=?";
        params.add(new Integer(queryId));
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();

        // add any new selections
        List preparatory = query.getPreparatory();
        if (preparatory != null) {
            sql = "INSERT INTO PRE_QUERY_PREPARATIONS ( " + "query_seq, "
                    + "pqp_type_id," + "other_comment ) " + "VALUES ( "
                    + "?, " + "?, " + "upper(?) ) "; // (AJW) SQL
            // statement altered
            // to include
            // upper(..)
            // conversions

            for (int prep = 0; prep < preparatory.size(); prep++) {
                params.add(new Integer(queryId));
                params.add(new Integer(((PreparatoryDTO) preparatory
                        .get(prep)).getPreparatoryId()));
                params.add(((PreparatoryDTO) preparatory.get(prep))
                        .getOthers());
                jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
                params.clear();
            }
        }

        // update the drugs
        sql = "DELETE FROM DRUGS WHERE query_seq=? and first_flag = 'Y'";
        params.add(new Integer(queryId));
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
        boolean isAllDrugs = query.isAllDrug();
        // insert if alldrug is set to false
        if (!isAllDrugs) {
            Long seq=jdbcTemplate.queryForLong("select max(drug_number_seq)+1 from DRUGS");
            sql = "INSERT INTO DRUGS ( " + "query_seq, "
                    + "drug_number_seq, " + "first_flag, "
                    + "roche_drug_flag, " + "drug_retrieval_name, "
                    + "inn_generic_name, " + "route, " + "formulation, "
                    + "indication, " + "dose, " + "extra_first_drugs ) "
                    + " VALUES ( " + "?, "
                    + "?, " + "?, " + "?, "
                    + "?, " + "?, " + "upper(?), " + "upper(?), "
                    + "upper(?), " + "upper(?), " + "? ) "; // (AJW) SQL
            // statement
            // altered to
            // include
            // upper(..)
            // conversions

            params.add(new Integer(queryId));
            params.add(seq);
            params.add(Constants.YES.toString());// this will always
            // be the first drug
            params.add(Constants.YES.toString());// this will always
            // be a roche drug
            // as it is the
            // first drug
            params.add(query.getDrug().getRetrName());
            params.add(query.getDrug().getGenericName());
            params.add(query.getDrug().getRoute());
            params.add(query.getDrug().getFormulation());
            params.add(query.getDrug().getIndication());
            params.add(query.getDrug().getDose());
            params.add(query.getQuerySummary());
            jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();
        }

        // do attachments
        List attachments = query.getAttachments();

/* DSCL-6032 - documentation have not info about removing attachments */

        // Note: If the update is after actioning 'find partial' then we
        // delete any exisiting attachments for this query.
        // Else if the update is after actioning 'find provisional' we
        // simply add new ones and keep the old attachments as it is.
        if (query.getAction().equalsIgnoreCase("partial")) {

            //DEACTIVATION INDICATED ATTACHMENTS
            if (idsAttachmentsToRemove != null && !idsAttachmentsToRemove.isEmpty()){
                StringBuffer attachmentsToRemove = new StringBuffer();
                for (Integer idToRemove : idsAttachmentsToRemove){
                    if (attachmentsToRemove.length() > 0) {
                        attachmentsToRemove.append(", ");
                    }
                    attachmentsToRemove.append(""+idToRemove.intValue());
                }

                sql = "UPDATE ATTACHMENTS A SET ACTIVATION_FLAG = 'N' WHERE A.query_seq=? and A.attachment_seq in ("+ attachmentsToRemove.toString() + ")";

                params.add(new Integer(queryId));
                jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
                params.clear();

            }

            // ok this if for partial query so delete any old attachments
/*
            StringBuffer attachmentsToKeep = new StringBuffer();
            for (Iterator it = attachments.iterator(); it.hasNext();) {
                AttachmentDTO att = (AttachmentDTO) it.next();
//                if (att.getTitle() != null && att.getFileName() == null) { //ATTENTION: condition 'att.getTitle() != null && att.getFileName() != null' should work better, present looks wrong
                if (att.getTitle() != null && att.getFileName() != null) {
                    if (attachmentsToKeep.length() > 0) {
                        attachmentsToKeep.append(", ");
                    }
                    attachmentsToKeep.append(att.getId());
                }
            }
            logger.debug("attachmentsToKeep: ["+attachmentsToKeep.toString()+"]");
           if (attachmentsToKeep.length() != 0) {
                sql = "DELETE FROM ATTACHMENTS A WHERE A.query_seq=? and (select Q.status_code from queries Q where Q.query_seq= A.query_seq) <> 'REOP' and A.attachment_seq not in ("
                        + attachmentsToKeep.toString() + ")";
            } else {
                sql = "DELETE FROM ATTACHMENTS A WHERE A.query_seq=? and (select Q.status_code from queries Q where Q.query_seq= A.query_seq) <> 'REOP'";
            }
            params.add(new Integer(queryId));
            jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();
*/
        }
/**/

        // add any newly added attachments
        if(CollectionUtils.isNotEmpty(attachments)){

            Query query1 = queryService.find((long)queryId);

            for (int noOfDocs = 0; noOfDocs < attachments.size(); noOfDocs++) {
                AttachmentDTO attach = (AttachmentDTO) attachments.get(noOfDocs);
                if (attach.getFileName() != null) {
                    Attachment attachment = attach.createAttachment(queryId);

                    if(StringUtils.isEmpty(attachment.getFollowupNumber())){
                        attachment.setFollowupNumber(query1.getFollowupNumber());
                        attachment.setFollowupSeq(query1.getFollowupSeq());
                    }
                    if(StringUtils.isEmpty(attachment.getFollowupNumber())){
                        attachment.setFollowupNumber("FU0000");
                    }

                    logger.debug("Attachment to merge (base): "+attach.toString());
                    logger.debug("Attachment to merge: "+attachment.toString());

                    attachmentService.merge(attachment);
                }
            }
        }

        /*
        if ((attachments != null) && (!attachments.isEmpty())) {

            sql = "INSERT INTO ATTACHMENTS ( "
                    + "query_seq, "
                    + "attachment_seq, "
                    + "attachment_category_id, "
                    + "create_ts, "
                    + "title, "
                    + "version, "
                    + "medical_opinion_flag, "
                    + "filename, "
                    + "author, "
                    + "activation_flag, "
                    + "followup_seq) "
                    + "VALUES ( "
                    + "?, "
                    + "?, "
                    + "?, "
                    + "?, "
                    + "upper(?), "
                    + "?, "
                    + "?, "
                    + "?, "
                    + "upper(?), "
                    + "'Y', "
                    + "(SELECT followup_seq from queries where query_seq = ?)) "; // (AJW)
            // SQL
            // statement
            // altered
            // to
            // include
            // upper(..)
            // conversions

            // iterate for each attached document
            for (int noOfDocs = 0; noOfDocs < attachments.size(); noOfDocs++) {
                AttachmentDTO attach = (AttachmentDTO) attachments
                        .get(noOfDocs);
                if (attach.getFileName() != null) {
                    params.add(queryId);
                    Long seq=jdbcTemplate.queryForLong("select max(attachment_seq)+1 from ATTACHMENTS");
                    params.add(seq);
                    params.add(Constants.QRF_ATTACHMENT_CATEGORY);
                    params.add(Calendar.getInstance());
                    params.add(attach.getTitle());
                    params.add(Constants.QRF_ATTACHMENT_VERSION);
                    params.add(Constants.NO.toString());// we are forced to
                    // enter this as it is
                    // mandatory
                    params.add(attach.getFileName());
                    params.add(attach.getAuthor()); // (AJW) bug fix
                    // #211
                    params.add(new Integer(queryId));
                    jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
                    params.clear();
                }
            }// end for

        }// end if
        */

        // update cumulative
        boolean flagCumulative = query.getCumulative().isCumulative();
        if (!flagCumulative) {
            sql = "UPDATE RETRIEVAL_PERIODS " + "SET "
                    + "cumulative_flag = ?, " + "start_ts = ?, "
                    + "end_ts = ?, " + "interval = ? " + "WHERE "
                    + "query_seq = ? "; // (AJW) SQL statement altered to
            // include upper(..) conversions
            params.add(query.getCumulative().isCumulative() ? Constants.YES.toString()
                    : Constants.NO.toString());
            params.add(new java.sql.Date(query.getCumulative()
                    .getFromDate()));
            params.add(new java.sql.Date(query.getCumulative()
                    .getToDate()));
        } else {
            sql = "UPDATE RETRIEVAL_PERIODS " + "SET "
                    + "cumulative_flag = ?, " + "start_ts = NULL, "
                    + "end_ts = NULL, " + "interval = ? " + "WHERE "
                    + "query_seq = ? "; // (AJW) SQL statement altered to
            // include upper(..) conversions
            params.add(query.getCumulative().isCumulative() ? Constants.YES.toString()
                    : Constants.NO.toString());
        }
        params.add(query.getCumulative().getInterval());
        params.add(new Integer(queryId));
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
        // update the query itself


        sql = "UPDATE QUERIES " + "SET " + "source_country_code = ?, "
                + "query_type_code = ?, " + "reporter_type_id = ?, "
                + "status_code = ?, " + "all_drugs_flag = ?, "
                + "alert_flag = ?, " + "urgency_flag = ?, "
                + "urgency_reason = upper(?), " + "date_requested = ?, "
                + "follow_up_number = upper(?), " + "query_summary = ?, "
                + "query_summary_upper = ?, "
                + "pre_query_prep_comment = upper(?), "
                + "datetime_submitted = ?, "
                + "current_due_date = ?, " + "requester_query_label = ?, "
                + "dmg_due_date = null " + " WHERE " + "query_seq = ? "; // (AJW)


        // SQL
        // statement
        // altered
        // to
        // include
        // upper(..)
        // conversions
        params.add(query.getSourceCountry());
        params.add(query.getQueryType().name());
        params.add(new Integer(query.getReporterType()));
        params.add(query.getStatusCode());
        params.add(query.isAllDrug() ? Constants.YES.toString() : Constants.NO.toString());
        params.add(Constants.NO.toString()); // alert_flag
        params.add(query.isUrgent() ? Constants.YES.toString() : Constants.NO.toString());
        params.add(query.getUrgency());
        params.add(new java.sql.Date(query.getDateRequested()));
        params.add(query.getFollowUpNumber());
        params.add(query.getQuerySummary());
        if (query.getQuerySummary() != null)
            params.add(query.getQuerySummary().toUpperCase());
        else
            params.add("");
        params.add(query.getPrepComments());

        // by default set the current dua dat to date requested

        params.add(Calendar.getInstance());
        params.add(new java.sql.Date(query.getDateRequested()));
        params.add(query.getRequesterQueryLabel());

        params.add(new Integer(queryId));
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();

        // get the requester id
        int requesterId = 0;
        sql = "SELECT REQUESTER_ID FROM QUERIES WHERE query_seq=?";

        params.clear();
        params.add(new Integer(queryId));
        Integer res;
        try {
            res = jdbcTemplate.queryForInt(sql, params.toArray(new Object[params.size()]));
            params.clear();
        } catch (IncorrectResultSizeDataAccessException e) {
            res = null;
        }
        params.clear();
        if (res != null)
            requesterId = res;

        // update requester
        sql = "UPDATE REQUESTERS " + "SET " + "first_name = upper(?), "
                + "surname = upper(?), " + "telephone = ?, "
                + "country_code = ?, " + "modified_ts = ?, "
                + "email = upper(?), " + "fax_number = ? " + "WHERE "
                + "REQUESTER_ID = ? "; // (AJW) SQL statement altered to
        // include upper(..) conversions

        params.add(query.getRequester().getFirstName());
        params.add(query.getRequester().getLastName());
        params.add(query.getRequester().getTelephone());
        params.add(query.getRequester().getCountryCode());
        params.add(Calendar.getInstance());
        params.add(query.getRequester().getEmailAddress());
        params.add(query.getRequester().getFax());
        params.add(new Integer(requesterId));
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
        // get the requester id
        int orgId = 0;
        sql = "SELECT organisation_id FROM REQUESTERS WHERE REQUESTER_ID=?";
        params.add(new Integer(requesterId));
        try {
            res = jdbcTemplate.queryForInt(sql, params.toArray(new Object[params.size()]));
        } catch (IncorrectResultSizeDataAccessException e) {
            res = null;
        }
        params.clear();
        if (res != null)
            orgId = res;

        // update organisation
        sql = "UPDATE ORGANISATIONS " + "SET " + "name = upper(?), "
                + "type_id = ? " + "WHERE " + "organisation_id = ? "; // (AJW)
        // SQL
        // statement
        // altered
        // to
        // include
        // upper(..)
        // conversions

        params.add(query.getRequester().getOrg().getName());
        params.add(new Integer(query.getRequester().getOrg()
                .getTypeId()));
        params.add(new Integer(orgId));
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
    }

    @Override
    public int createQRF(QueryDTO query) {
        List<Object> params = new ArrayList<Object>();

        int queryNumber = 0;
        // sqlExec.setAutoCommit(false);

        // add the organization details
        Long orgSeq=jdbcTemplate.queryForLong("select max(organisation_id)+1 from ORGANISATIONS");
        params.add(orgSeq);
        params.add(query.getRequester().getOrg().getName());
        params.add(new Integer(query.getRequester().getOrg()
                .getTypeId()));
        jdbcTemplate.update("INSERT INTO ORGANISATIONS ( "
                + "organisation_id, name, " + "type_id ) "
                + "VALUES (?, upper(?), " + "? ) ", params.toArray(new Object[params.size()])); // (AJW) SQL statement altered
        // to include upper(..)
        // conversions

        logger.debug("createQRF() Created Organisation");
        params.clear();
        // add the requester details
        Long reqseq=jdbcTemplate.queryForLong("select max(requester_id)+1 from REQUESTERS");
        params.add(reqseq);
        params.add(query.getRequester().getFirstName());
        params.add(query.getRequester().getLastName());
        params.add(orgSeq);
        params.add(query.getRequester().getTelephone());
        params.add(query.getRequester().getCountryCode());
        params.add(Calendar.getInstance());
        params.add(query.getRequester().getEmailAddress());
        params.add(query.getRequester().getFax());
        jdbcTemplate.update("INSERT INTO REQUESTERS ( " + "requester_id, "
                + "first_name, " + "surname, " + "organisation_id, "
                + "telephone, " + "country_code, " + "modified_ts, "
                + "email, " + "fax_number ) " + "VALUES ( "
                + "?, " + "upper(?), "
                + "upper(?), " + "?, "
                + "?, " + "?, " + "?, " + "upper(?), " + "? ) ", params.toArray(new Object[params.size()])); // (AJW)
        // SQL
        // statement
        // altered
        // to
        // include
        // upper(..)
        // conversions

        logger.debug("createQRF() Created Requester");

        params.clear();
        // add the query details
        Long qsq=jdbcTemplate.queryForLong("select max(query_seq)+1 from QUERIES"); //change this meth into jpa2
        params.add(qsq);
        params.add(qsq);
        params.add(reqseq);
        params.add(query.getSourceCountry());
        params.add(query.getQueryType().name());
        params.add(new Integer(query.getReporterType()));
        params.add(query.getStatusCode());
        params.add(query.isAllDrug() ? Constants.YES.toString() : Constants.NO.toString());
        params.add(Constants.NO.toString()); // alert_flag
        params.add(query.isUrgent() ? Constants.YES.toString() : Constants.NO.toString());
        params.add(query.getUrgency());
        params.add(new java.sql.Date(query.getDateRequested()));
        params.add(query.getFollowUpNumber());
        params.add(query.getQuerySummary());
        if (query.getQuerySummary() != null)
            params.add(query.getQuerySummary().toUpperCase());
        else
            params.add(""); // unlike PreparedStatement null cannot
        // be added to the sqlExec.addParam
        params.add(query.getPrepComments());

        params.add(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        // by default set the current due date to be same as date requested.
        params.add(new java.sql.Date(query.getDateRequested()));

        // the original due date is used by the performance metric so yy
        // populate it with the date requested.

        params.add(new java.sql.Date(query.getDateRequested()));
        params.add(query.getRequesterQueryLabel());
        //temporary code

        //todo change into JPA code!!  - this is temp code to test Action/ JSP

        jdbcTemplate.update("INSERT INTO QUERIES ( " + "query_seq, "
                + "query_number, " + "requester_id, "
                + "source_country_code, " + "query_type_code, "
                + "reporter_type_id, " + "status_code, "
                + "all_drugs_flag, " + "alert_flag, " + "urgency_flag, "
                + "urgency_reason, " + "date_requested, "
                + "follow_up_number, " + "query_summary, "
                + "query_summary_upper, " + "pre_query_prep_comment, "
                + "datetime_submitted, " + "current_due_date, "
                + "original_due_date, " + "requester_query_label, followup_number ) "
                + "VALUES ( ?, "
                + "'QTT' || lpad(''||?, 6, '0'), "
                + "?, " + "?, " + "?, " + "?, "
                + "?, " + "?, " + "?, " + "?, " + "upper(?), " + "?, "
                + "upper(?), " + "?, " + "?, " + "upper(?), " + "?, "
                + "?, " + "?, " + "?, 'FU0000' ) ", params.toArray(new Object[params.size()])); // (AJW) SQL statement altered
        // to include upper(..)
        // conversions

        logger.debug("createQRF() Created Query");
        Long qseq=jdbcTemplate.queryForLong("select max(query_seq) from QUERIES");
        // add cumulative
        // the from and the to date will only be required if the cumulative
        // period id set to 'no'
        params.clear();
        boolean flagCumulative = query.getCumulative().isCumulative();
        if (!flagCumulative) {
            params.add(qseq);
            params.add(query.getCumulative().isCumulative() ? Constants.YES.toString()
                    : Constants.NO.toString());
            params.add(new java.sql.Date(query.getCumulative()
                    .getFromDate()));
            params.add(new java.sql.Date(query.getCumulative()
                    .getToDate()));
            params.add(query.getCumulative().getInterval());
            jdbcTemplate.update("INSERT INTO RETRIEVAL_PERIODS ( "
                    + "query_seq, " + "cumulative_flag, " + "start_ts, "
                    + "end_ts, " + "interval ) " + "VALUES ( "
                    + "?, " + "?, " + "?, " + "?, "
                    + "upper(?) ) ", params.toArray(new Object[params.size()])); // (AJW) SQL statement altered to
            // include upper(..) conversions
        } else {
            params.add(qseq);
            params.add(query.getCumulative().isCumulative() ? Constants.YES.toString()
                    : Constants.NO.toString());
            params.add(query.getCumulative().getInterval());
            jdbcTemplate.update("INSERT INTO RETRIEVAL_PERIODS ( "
                    + "query_seq, " + "cumulative_flag, " + "interval ) "
                    + "VALUES ( " + "?, " + "?, "
                    + "upper(?) ) ", params.toArray(new Object[params.size()])); // (AJW) SQL statement altered to
            // include upper(..) conversions
        }

        logger.debug("createQRF() Created RETRIEVAL_PERIODS");
        params.clear();
        boolean isAllDrugs = query.isAllDrug();
        if (!isAllDrugs) {
              Long seq=jdbcTemplate.queryForLong("select max(drug_number_seq)+1 from DRUGS");
            params.add(qseq);
            params.add(seq);
            params.add(Constants.YES.toString()); // this will always be the
            // first drug
            params.add(Constants.YES.toString()); // this will always be a
            // roche drug as it is the
            // first drug


            params.add(query.getDrug().getRetrName());
            params.add(query.getDrug().getGenericName());
            params.add(query.getDrug().getRoute());
            params.add(query.getDrug().getFormulation());
            params.add(query.getDrug().getIndication());
            params.add(query.getDrug().getDose());
            params.add(query.getQuerySummary());
            jdbcTemplate.update("INSERT INTO DRUGS ( " + "query_seq, "
                    + "drug_number_seq, " + "first_flag, "
                    + "roche_drug_flag, " + "drug_retrieval_name, "
                    + "inn_generic_name, " + "route, " + "formulation, "
                    + "indication, " + "dose, " + "extra_first_drugs ) "
                    + "VALUES ( ?, "
                    + "?, " + "?," + "?, "
                    + "?, " + "?, " + "upper(?), " + "upper(?), "
                    + "upper(?), " + "upper(?), " + "? ) ", params.toArray(new Object[params.size()])); // (AJW) SQL
        }

        logger.debug("createQRF() Created DRUGS");

        // add preparatory
        List preparatory = query.getPreparatory();
        if (preparatory != null) {
            String prepSql = "INSERT INTO PRE_QUERY_PREPARATIONS ( "
                    + "query_seq, " + "pqp_type_id, " + "other_comment ) "
                    + "VALUES ( " + "?, " + "?, "
                    + "upper(?) ) "; // (AJW) SQL statement altered to
            // include upper(..) conversions
            params.clear();
            for (int prep = 0; prep < preparatory.size(); prep++) {
                params.add(qseq);
                params.add(new Integer(((PreparatoryDTO) preparatory
                        .get(prep)).getPreparatoryId()));
                params.add(((PreparatoryDTO) preparatory.get(prep))
                        .getOthers());
                jdbcTemplate.update(prepSql, params.toArray(new Object[params.size()]));
                 params.clear();
            }
        }
        logger.debug("createQRF() Created PRE_QUERY_PREPARATIONS");

        // commit
        // sqlExec.commitTrans();

        // return the query number we got allocated so as to get the handle
        // to this query for further
        // additions if this query isnt complete.


        return qseq.intValue();

    }// createQRF


    @Value("${ext.table.countries}")
    public void setCountryTab(String countryTab) {
        this.countryTab = countryTab;
    }


    @Override
    public QueryDTO openQuery(int queryId, String emailAddress, boolean isReview) {

        List<Object> params = new ArrayList<Object>();

        // select all the query details
        String querySql = "SELECT " + "name, " + "o.type_id as o_type_id, "
                + "first_name, " + "surname, " + "telephone, "
                + "fax_number, " + "email, "
                + "r.country_code as r_country_code, "
                + "q.pre_query_prep_comment as q_pre_query_prep_comment, "
                + "q.source_country_code as q_source_country_code, "
                + "q.reporter_type_id as q_reporter_type, "
                + "all_drugs_flag, " + "date_requested, "
                + "urgency_reason, " + "cumulative_flag, " + "interval, "
                + "q.query_type_code as q_query_type, "
                + "follow_up_number, followup_number, " + "description, "
                + "org.type as o_type, "
                + "qt.query_type as qt_query_type, "
                + "rt.reporter_type as rt_reporter_type, "
                + "case_comment, " + "oformat_special_request, "
                + "mnft_batch_number, " + "case_oformat_id, "
                + "ind_number, " + "query_number, " + "access_key, "
                + "requester_query_label, " + "status_code, " + "reopen " + "FROM "
                + "requesters r, " + "organisations o, " + "queries q, "
                + "reporter_types rt, " + "retrieval_periods rp, "
                + "query_types qt, " + countryTab + " c, "
                //+ "query_types qt, " + "COUNTRIES_LU@ADVENT_DB c, "
                + "organisation_types org " + "WHERE " + "q.query_seq = ?"
                + "AND q.requester_id = r.requester_id "
                + "AND r.organisation_id = o.organisation_id "
                + "AND q.reporter_type_id = rt.reporter_type_id "
                + "AND rp.query_seq = q.query_seq "
                + "AND c.country_code = r.country_code "
                + "AND org.type_id = o.type_id "
                + "AND qt.query_type_code = q.query_type_code ";

        // if calling with the query id and email then add email to the
        // select stmt
        if (emailAddress != null)
            querySql += " AND r.email = ?";

        params.add(new Integer(queryId));


        // if calling with the query id and email then add email to the
        // pstmt
        if (emailAddress != null)
            params.add(emailAddress);


        SqlRowSet rs = jdbcTemplate.queryForRowSet(querySql, params.toArray(new Object[params.size()]));
        params.clear();
        QueryDTO query = null;
        if (rs.next()) {

            query = new QueryDTO();

            // get the organisation details
            OrganisationDTO orgDto = new OrganisationDTO();
            orgDto.setName(rs.getString("name"));
            orgDto.setTypeId(rs.getInt("o_type_id"));
            orgDto.setTypeDescription(rs.getString("o_type"));

            // get the requester details
            RequesterDTO requesterDto = new RequesterDTO();
            requesterDto.setOrg(orgDto);
            requesterDto.setFirstName(rs.getString("first_name"));
            requesterDto.setLastName(rs.getString("surname"));
            requesterDto.setTelephone(rs.getString("telephone"));
            requesterDto.setFax(rs.getString("fax_number"));
            requesterDto.setEmailAddress(rs.getString("email"));
            requesterDto.setCountryCode(rs.getString("r_country_code"));
            requesterDto.setCountryName(rs.getString("description"));
            query.setRequester(requesterDto);

            // get preparatory details if the query is from 'Roche
            // Affiliate'
            if (orgDto.getTypeId() == ROCHE_AFFILIATE) {

                params.clear();
                params.add(new Integer(queryId));
                String sql = "SELECT " + "pqp_type_id, "
                        + "other_comment " + "FROM "
                        + "pre_query_preparations " + "WHERE "
                        + "query_seq = ? ";

                SqlRowSet r = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
                params.clear();
                List prepList = new ArrayList();
                // iterate for each record found
                while (r.next()) {

                    PreparatoryDTO prepDto = new PreparatoryDTO();
                    prepDto.setPreparatoryId(r.getInt("pqp_type_id"));
                    prepDto.setOthers(r.getString("other_comment"));
                    prepList.add(prepDto);
                }
                query.setPreparatory(prepList);
                query.setPrepComments(rs.getString("q_pre_query_prep_comment"));
            }

            query.setSourceCountry(rs.getString("q_source_country_code"));
            query.setReporterType(rs.getInt("q_reporter_type"));
            query.setReporterTypeDescription(rs.getString("rt_reporter_type"));

            // if the all drug flag is set to 'N' we need to get the drug
            // details
            if (rs.getString("all_drugs_flag").equals(Constants.NO.toString())) {
                query.setAllDrug(false);
                params.add(new Integer(queryId));
                params.add(Constants.YES.toString());
                params.add(Constants.YES.toString());
                String sql = "SELECT "
                        + "drug_retrieval_name, " + "inn_generic_name, "
                        + "extra_first_drugs, " + "route, "
                        + "formulation, " + "indication, " + "dose, "
                        + "query_summary " + "FROM " + "drugs d, "
                        + "queries q " + "WHERE "
                        + "d.query_seq = q.query_seq "
                        + "AND q.query_seq = ? " + "AND first_flag = ? "
                        + "AND roche_drug_flag = ? ";

                SqlRowSet r = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
                params.clear();

                // iterate for each record found
                while (r.next()) {

                    DrugDTO drugDto = new DrugDTO();
                    drugDto.setRetrName(r.getString("drug_retrieval_name"));
                    drugDto.setGenericName(r.getString("inn_generic_name"));
                    drugDto.setExtraFirstDrugs(r.getString("extra_first_drugs"));
                    drugDto.setRoute(r.getString("route"));
                    drugDto.setFormulation(r.getString("formulation"));
                    drugDto.setIndication(r.getString("indication"));
                    drugDto.setDose(r.getString("dose"));
                    query.setDrug(drugDto);
                    query.setQuerySummary(r.getString("query_summary"));
                }
            } else
                query.setAllDrug(true);

            query.setDateRequested(rs.getDate("date_requested")
                    .getTime());
            query.setUrgency(rs.getString("urgency_reason"));

            // get attachments
            params.add(new Integer(queryId));
            String sql = "SELECT "
                    + "a.attachment_seq, "
                    + "a.title, "
                    + "a.create_ts, "
                    + "a.author, "
                    + "a.filename, "
                    + "a.attachment_category_id, "
                    + "ac.affiliate_visibility_flag as visibility_flag, "
                    + "a.followup_number, "
                    + "a.activation_flag "
                    + "FROM "
                    + "attachments a, attachment_categories ac "
                    + "WHERE "
                    + " a.attachment_category_id=ac.attachment_category_id and "
                    + "a.query_seq = ? and "
                    + "a.activation_flag = 'Y' " + "ORDER BY "
                    + "a.attachment_seq ";

            SqlRowSet r = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
            params.clear();


            List attachList = new ArrayList();
            // iterate for each record found
            while (r.next()) {
                AttachmentDTO aDto = new AttachmentDTO();
                aDto.setId(r.getInt("attachment_seq"));
                aDto.setDateTime(r.getDate("create_ts"));
                aDto.setAuthor(r.getString("author"));
                aDto.setTitle(r.getString("title"));
                aDto.setFileName(r.getString("filename"));
                aDto.setCategoryId(r.getInt(
                        "attachment_category_id"));
                aDto.setAffiliateVisible(Boolean.valueOf(r
                        .getString("visibility_flag")));
                aDto.setActivationFlag(Boolean.valueOf(r.getString("activation_flag")) || "Y".equalsIgnoreCase(r.getString("activation_flag")));
                aDto.setFollowupNumber(r.getString("followup_number"));
                attachList.add(aDto);
            }
            query.setAttachments(attachList);

            // cumulative
            CumulativePeriodDTO cDto = new CumulativePeriodDTO();
            if (Constants.NO.toString().equals(rs.getString("cumulative_flag"))) {

                cDto.setCumulative(false);

                // get the cumulative details if its set to 'N'
                params.add(new Integer(queryId));
                sql = "SELECT start_ts, end_ts FROM retrieval_periods WHERE query_seq = ?";

                SqlRowSet result = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
                params.clear();

                if (result.next()) {
                    cDto.setFromDate(result.getDate("start_ts")
                            .getTime());
                    cDto.setToDate(result.getDate("end_ts").getTime());
                }
            } else
                cDto.setCumulative(true);

            cDto.setInterval(rs.getString("interval"));
            query.setCumulative(cDto);

            // set other details
            query.setQueryType(QueryType.Code.valueOf(rs.getString("q_query_type")));
            query.setQueryTypeDescription(rs.getString("qt_query_type"));
            query.setFollowUpNumber(rs.getString("follow_up_number"));
            query.setFollowupNumber(rs.getString("followup_number"));
            query.setQueryNumber(Integer.valueOf(queryId).toString());
            query.setCaseComment(rs.getString("case_comment"));
            query.setSpecialRequest(rs.getString("oformat_special_request"));
            query.setBatch(rs.getString("mnft_batch_number"));
            if (rs.getObject("case_oformat_id") != null)
                query.setOutputFormatId(rs.getInt("case_oformat_id"));
            query.setINDNumber(rs.getString("ind_number"));

            // get the description of source country
            params.add(rs.getString("q_source_country_code"));


            sql = "SELECT description FROM " + countryTab + " WHERE country_code=?";

            SqlRowSet result = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
            params.clear();


            //.runQuery("SELECT description FROM COUNTRIES_LU@ADVENT_DB WHERE country_code=?");

            if (result.next())
                query.setSourceCountryDescription(result.getString("description"));

            getQueryTypeDetails(query);

            // set the query number string i.e. 'QTT00XXX'
            query.setQueryNumberString(rs.getString("query_number"));
            query.setAccessKey(rs.getString("access_key"));
            query.setRequesterQueryLabel(rs.getString("requester_query_label"));
            query.setStatusCode(rs.getString("status_code"));
            String reopenText = rs.getString("reopen");
            if (reopenText.equals(Constants.NO.toString())) {
                query.setReopen(false);
            } else {
                query.setReopen(true);
            }
        }
        return query;
    }

    public void getQueryTypeDetails(QueryDTO dto) {

        switch (dto.getQueryType()) {

            case DRUG_EVENT:
                getDrugEvent(dto);
                break;
            case DRUG_INTERACTION:
                getDrugInteraction(dto);
                break;
            case MANUFACTURING:
            case MANUFACTURING_RECALL:
                getManufacturing(dto);
                break;
            case PREGNANCY:
                getPregnancy(dto);
                break;
            case EXTERNAL_AUDIT:
                getExternalAudit(dto);
                break;
            case CASE_CLARIFICATION:
            case CASE_DATA_REQUEST:
                getCaseClarification(dto);
                break;
            case SAE_RECONCILIATION:
                dto.setClinical(getClinical(Integer.parseInt(dto.getQueryNumber())));
                ;
                break;
            case PERFORMANCE_METRICS:
                dto.setQueryDesc(getQueryDesc(Integer.parseInt(dto.getQueryNumber())));
                break;
            case SIGNAL_DETECTION:
            case LITERATURE_SEARCH:
                dto.setQueryDesc(getQueryDesc(Integer.parseInt(dto.getQueryNumber())));
                dto.setAeTerm(getAETerm(Integer.parseInt(dto.getQueryNumber())));
                break;
            case PERFORMANCE_METRICS2:
            	dto.setPerformanceMetric(getPerformanceMetric(Integer.parseInt(dto.getQueryNumber())));
            	break;
            case EXTERNAL_AUDIT_INSPECTION:
            	dto.setAudit(getAudit(Integer.parseInt(dto.getQueryNumber())));
                dto.setQueryDesc(getQueryDesc(Integer.parseInt(dto.getQueryNumber())));
                dto.setClinical(getClinical(Integer.parseInt(dto.getQueryNumber())));
                break;
            case MEDICAL:
            	getDrugInteraction(dto);
                dto.setQueryDesc(getQueryDesc(Integer.parseInt(dto.getQueryNumber())));
            	getPregnancy(dto);
                getCaseClarification(dto);
            	break;
            case DATA_SEARCH:
                dto.setQueryDesc(getQueryDesc(Integer.valueOf(dto.getQueryNumber())));
                dto.setClinical(getClinical(Integer.valueOf(dto.getQueryNumber())));
                dto.setDataSearch(getDataSearch(Integer.valueOf(dto.getQueryNumber())));
                getCaseClarification(dto);
            	break;
            default:
                // nothing
        }

    }

    private DataSearch getDataSearch(Integer querySeq) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT ds.request_type, ds.search_type, ds.specification, ds.bo_report, ds.report_parameters, "
        		+ "ds.frequency, ds.frequency_other, ds.meddra_preferred_term "
        		+ "FROM data_search ds "
        		+ "WHERE ds.QUERY_SEQ = ? ";
        params.add(querySeq);
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));

        DataSearch data = new DataSearch();
        if (rs.next()) {
        	data.setQuerySeq(querySeq);
        	data.setBoReport(rs.getString("bo_report"));
        	data.setFrequency(rs.getString("frequency") != null ? Frequency.valueOf(rs.getString("frequency")) : null);
        	data.setFrequencyOther(rs.getString("frequency_other"));
        	data.setMeddraPreferredTerm(rs.getString("meddra_preferred_term"));
        	data.setReportParameters(rs.getString("report_parameters"));
        	data.setRequestType(RequestType.valueOf(rs.getString("request_type")));
        	data.setSearchType(SearchType.valueOf(rs.getString("search_type")));
        	data.setSpecification(rs.getString("specification"));
        }
        return data;
	}


	private PerformanceMetric getPerformanceMetric(int querySeq) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT pm.request_description, pm.ctrl_oformat_id, pm.special_requests, o.ctrl_oformat "
        		+ "FROM performance_metrics pm LEFT OUTER JOIN ctrl_oformats o ON o.ctrl_oformat_id = pm.ctrl_oformat_id "
        		+ "WHERE pm.QUERY_SEQ = ? ";
        params.add(querySeq);
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));

        PerformanceMetric perf = new PerformanceMetric();
        if (rs.next()) {
        	perf.setRequestDescription(rs.getString("request_description"));
        	perf.setSpecialRequests(rs.getString("special_requests"));
        	Integer oformatId = rs.getInt("ctrl_oformat_id");
        	if (oformatId != null) {
        		CtrlOformat oformat = new CtrlOformat();
        		oformat.setCtrlOformatId(oformatId);
        		oformat.setCtrlOformat(rs.getString("ctrl_oformat"));
        		perf.setOutputFormat(oformat);
        	}
        }
        return perf;
	}


    private Audit getAudit(int querySeq) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT a.all_cases, a.all_studies, a.study_no, a.all_spontaneous_cases, a.all_subm, " +
        		" a.susar_subm, a.serious_cases, a.non_serious_cases, a.submission_titles " +
        		"FROM audits a " +
        		"WHERE a.QUERY_SEQ = ? ";
        params.add(querySeq);
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));

        Audit a = new Audit();
        if (rs.next()) {
        	a.setAllCases(getBoolean(rs, "all_cases"));
        	a.setAllStudies(getBoolean(rs, "all_studies"));
        	a.setStudyNo(rs.getString("study_no"));
        	a.setAllSpontaneousCases(getBoolean(rs, "all_spontaneous_cases"));
        	a.setAllSubm(getBoolean(rs, "all_subm"));
        	a.setSusarSubm(getBoolean(rs, "susar_subm"));
        	a.setSeriousCases(getBoolean(rs, "serious_cases"));
        	a.setNonSeriousCases(getBoolean(rs, "non_serious_cases"));
        	a.setSubmissionTitles(rs.getString("submission_titles"));
        }
        return a;
	}

    private Manufacturing getManufacturing(int querySeq) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT m.batch_no, m.trackwise_no, m.type " +
        		"FROM manufacturing m " +
        		"WHERE m.QUERY_SEQ = ? ";
        params.add(querySeq);
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));

        Manufacturing m = new Manufacturing();
        if (rs.next()) {
        	m.setBatchNo(rs.getString("batch_no"));
        	m.setTrackwiseNo(rs.getString("trackwise_no"));
        	String type = rs.getString("type");
        	m.setType(type != null ? Manufacturing.Type.valueOf(type) : null);
        }
        return m;
	}
	private Boolean getBoolean(SqlRowSet rs, String label) {
		Boolean b = rs.getBoolean(label);
		if (rs.wasNull()) {
			b = null;
		}
		return b;
	}


	public void getCaseClarification(QueryDTO query) {
        // get output format
        query.setOutputFormat(getOutputFormat(query.getOutputFormatId()));
        // get cases
        query.setCases(getCases(Integer.parseInt(query.getQueryNumber())));
    }

    public void getExternalAudit(QueryDTO query) {

        // get auery desc
        query.setQueryDesc(getQueryDesc(Integer.parseInt(query.getQueryNumber())));

        // get ae term and case
        query.setAeTerm(getAETerm(Integer.parseInt(query.getQueryNumber())));

        // get clinical
        query.setClinical(getClinical(Integer.parseInt(query.getQueryNumber())));

    }


    public ClinicalTrialDTO getClinical(int id) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT "
                + "PROTOCOL_NUMBERS, "
                + "CRTN_NUMBERS, "
                + "PATIENT_NUMBERS, "
                + "CASE_SELECTION, "
                // + "CTRL_OFORMAT, "
                + "(SELECT CTRL_OFORMAT FROM CTRL_OFORMATS CF  WHERE CT.CTRL_OFORMAT_ID = CF.CTRL_OFORMAT_ID) as  CTRL_OFORMAT, "
                + "CT.CTRL_OFORMAT_ID " + "FROM " + "CLINICAL_TRIALS CT "
                + "WHERE QUERY_SEQ = ? ";
        params.add(new Integer(id));

        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));

        ClinicalTrialDTO dto = new ClinicalTrialDTO();
        if (rs.next()) {
            dto.setProtocolNumber(rs.getString("PROTOCOL_NUMBERS"));
            dto.setCrtnNumber(rs.getString("CRTN_NUMBERS"));
            dto.setPatientNumber(rs.getString("PATIENT_NUMBERS"));
            dto.setCaseSelection(rs.getString("CASE_SELECTION"));
            dto.setOutputFormatDesc(rs.getString("CTRL_OFORMAT"));
            if (rs.getObject("CTRL_OFORMAT_ID") != null) {
                dto.setOutputFormatId(rs.getInt("CTRL_OFORMAT_ID"));
            }
        }
        return dto;
    }


    public void getPregnancy(QueryDTO query) {

        // get query desc
        query.setQueryDesc(getQueryDesc(Integer.parseInt(query.getQueryNumber())));

        // get ae term and case
        query.setOutputFormat(getOutputFormat(query.getOutputFormatId()));

        // get cases
        query.setCases(getCases(Integer.parseInt(query.getQueryNumber())));

        // get pregnancy info
        query.setPregnancy(getPregnancyInfo(Integer.parseInt(query.getQueryNumber())));

    }

    public PregnancyDTO getPregnancyInfo(int id) {

        List<Object> params = new ArrayList<Object>();

        // get description of query
        String sql = "SELECT " + "TIME_IN_PREGNANCY, " + "EXPOSURE_TYPE, COMMENTS, "
                + "P.EXPOSURE_TYPE_ID, " + "OUTCOME " + "FROM "
                + "PREGNANCIES P LEFT OUTER JOIN PRGY_EXPTYPES PT ON "
                + "P.EXPOSURE_TYPE_ID = PT.EXPOSURE_TYPE_ID "
                + "WHERE QUERY_SEQ = ? ";

        params.add(new Integer(id));

        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
        PregnancyDTO dto = new PregnancyDTO();
        if (rs.next()) {
            dto.setTiming(rs.getString("TIME_IN_PREGNANCY"));
            dto.setExposureTypeDesc(rs.getString("EXPOSURE_TYPE"));
            dto
                    .setExposureType(rs.getInt("EXPOSURE_TYPE_ID")
                    );
            dto.setOutcome(rs.getString("OUTCOME"));
            dto.setComments(rs.getString("COMMENTS"));
        }
        return dto;

    }

    public void getManufacturing(QueryDTO query) {
    	int querySeq = Integer.valueOf(query.getQueryNumber());
        // get query desc
        query.setQueryDesc(getQueryDesc(querySeq));
        // get output format
        query.setOutputFormat(getOutputFormat(query.getOutputFormatId()));
        // get cases
        query.setCases(getCases(querySeq));
        query.setAeTerm(getAETerm(querySeq));
        query.setClinical(getClinical(querySeq));
        query.setManufacturing(getManufacturing(querySeq));
    }

    public String getOutputFormat(int caseOutputId) {

        String output = "";
        List<Object> params = new ArrayList<Object>();
        // get description of query
        String sql = "SELECT CASE_OFORMAT FROM CASE_OFORMATS WHERE CASE_OFORMAT_ID = ?";

        params.add(new Integer(caseOutputId));
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
        if (rs.next())
            output = (rs.getString("CASE_OFORMAT"));
        return output;
    }

    public void getDrugInteraction(QueryDTO query) {
        // get cases
        query.setDrugs(getDrugs(Integer.valueOf(query.getQueryNumber())));

        // get query desc
        query.setQueryDesc(getQueryDesc(Integer.valueOf(query.getQueryNumber())));

        // get ae term and case
        query.setAeTerm(getAETerm(Integer.parseInt(query.getQueryNumber())));

        // get cases
        query.setCases(getCases(Integer.parseInt(query.getQueryNumber())));

    }

    public List getCases(int id) {
        List<Object> params = new ArrayList<Object>();
        // get description of query
        String sql = "SELECT " + "AER_NUMBER, " + "LOCAL_REFERENCE_NUMBER, "
                + "EXTERNAL_REFERENCE_NUMBER " + "FROM " + "CASES " + "WHERE "
                + "QUERY_SEQ = ? " + "ORDER BY " + "CASE_SEQ ASC ";

        params.add(new Integer(id));
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));

        List caseList = new ArrayList();
        while (rs.next()) {
            CaseDTO cases = new CaseDTO();
            cases.setAerNumber(rs.getString("AER_NUMBER"));
            cases
                    .setLocalRefNumber(rs.getString("LOCAL_REFERENCE_NUMBER"));
            cases.setExtRefNumber(rs
                    .getString("EXTERNAL_REFERENCE_NUMBER"));
            caseList.add(cases);
        }
        return caseList;
    }

    public String getAETerm(int id) {

        String aeTerm = "";

        List<Object> params = new ArrayList<Object>();
        // get description of query
        String sql = "SELECT DESCRIPTION FROM AE_TERMS WHERE QUERY_SEQ = ?";

        params.add(new Integer(id));
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
        if (rs.next())
            aeTerm = (rs.getString("DESCRIPTION"));

        return aeTerm;

    }

    public List getDrugs(int id) {
        List<Object> params = new ArrayList<Object>();
        // get description of query
        String sql = "SELECT " + "ROCHE_DRUG_FLAG, " + "DRUG_RETRIEVAL_NAME, "
                + "INN_GENERIC_NAME, " + "ROUTE, " + "FORMULATION, "
                + "INDICATION, " + "DOSE " + "FROM " + "DRUGS " + "WHERE "
                + "QUERY_SEQ = ? " + "AND FIRST_FLAG = 'N' ";

        params.add(new Integer(id));
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
        List drugList = new ArrayList();
        while (rs.next()) {
            DrugDTO drugs = new DrugDTO();
            drugs
                    .setRocheDrug(rs.getString("ROCHE_DRUG_FLAG").equals(
                            "Y") ? true : false);
            drugs.setRetrName(rs.getString("DRUG_RETRIEVAL_NAME"));
            drugs.setGenericName(rs.getString("INN_GENERIC_NAME"));
            drugs.setRoute(rs.getString("ROUTE"));
            drugs.setFormulation(rs.getString("FORMULATION"));
            drugs.setIndication(rs.getString("INDICATION"));
            drugs.setDose(rs.getString("DOSE"));
            drugList.add(drugs);
        }
        return drugList;
    }

    public void getDrugEvent(QueryDTO query) {

        // get query desc
        query.setQueryDesc(getQueryDesc(Integer.valueOf(query.getQueryNumber())));

        // get ae term and case
        query.setAeTerm(getAETerm(Integer.parseInt(query.getQueryNumber())));

        // get cases
        query.setCases(getCases(Integer.parseInt(query.getQueryNumber())));

    }

    public QueryDescDTO getQueryDesc(int id) {

        QueryDescDTO dto = new QueryDescDTO();
        //todo executor
        List<Object> params = new ArrayList<Object>();

        // get description of query
        String sql = "SELECT " + "NATURE_OF_CASE, " + "INDEX_CASE, "
                + "SIGNS_AND_SYMPTOMS, " + "INVESTIGATIONS, "
                + "PREV_MEDICAL_HISTORY, " + "CONMEDS, " + "OTHER_INFO, "
                + "DIAGNOSES " + "FROM " + "QUERY_DESCRIPTIONS " + "WHERE "
                + "QUERY_SEQ = ? ";

        params.add(new Integer(id));
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
        if (rs.next()) {
            dto.setNatureOfCase(rs.getString("NATURE_OF_CASE"));
            dto.setIndexCase(rs.getString("INDEX_CASE"));
            dto.setSignsSymp(rs.getString("SIGNS_AND_SYMPTOMS"));
            dto.setInvestigations(rs.getString("INVESTIGATIONS"));
            dto.setPmh(rs.getString("PREV_MEDICAL_HISTORY"));
            dto.setConMeds(rs.getString("CONMEDS"));
            dto.setOtherInfo(rs.getString("OTHER_INFO"));
            dto.setDiagnosis(rs.getString("DIAGNOSES"));
        }
        return dto;

    }

    @Override
    public void addDrugEvent(QueryDescDTO queryDesc, List caseDTOList,
                             String aeTerm, int queryNumber, String addComment) {

        boolean dbActivity = false;

        // sqlExec.setAutoCommit(false);

        // add description of query
        addQueryDesc(queryDesc, queryNumber);

        List<Object> params = new ArrayList<Object>();
        // add ae term if not null
        if ((aeTerm != null) && (aeTerm.length() > 0)) {
            params.add(new Integer(queryNumber));
            params.add(aeTerm);
            String sql = "INSERT INTO AE_TERMS ( " + "query_seq, "
                    + "description ) " + "VALUES (" + "?, "
                    + "upper(?) ) ";

            jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();


            // altered to include
            // upper(..) conversions
        }

        // add case details
        addCase(caseDTOList, queryNumber);

        // add additional comments for the query of type case only if not
        // null
        if ((addComment != null) && (addComment.length() > 0)) {
            params.add(addComment);
            params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
            params.add(Calendar.getInstance());
            params.add(new Integer(queryNumber));
            String sql = "UPDATE QUERIES " + "SET "
                    + "case_comment = upper(?), " + "status_code = ?, "
                    + "datetime_submitted = ? " + "WHERE "
                    + "query_seq = ? "; // (AJW) SQL statement altered

            jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            // to include upper(..)
            // conversions
        } else {
            // finally lets update the status of this query to submitted
            updateStatus(queryNumber, Constants.QUERY_STATUS_NOT_SUBMITTED);
        }

        // commit
        // sqlExec.commitTrans();
    }

    @Override
    public void updateDrugEvent(QueryDescDTO queryDesc, List caseDTOList,
                                String aeTerm, int queryNumber, String addComment) {

        List<Object> params = new ArrayList<Object>();

        boolean dbActivity = false;

        // sqlExec.setAutoCommit(false);

        // check if QUERY_DESCRIPTIONS already exists
        String sql = "SELECT QUERY_SEQ FROM QUERY_DESCRIPTIONS WHERE QUERY_SEQ = ?";
        params.add(new Integer(queryNumber));
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
        params.clear();
        // QUERY_DESCRIPTIONS is already existed so update it.
        if (rs.next()) {
            // update description of query
            updateQueryDesc(queryDesc, queryNumber);
        } else {
            // add description of query
            addQueryDesc(queryDesc, queryNumber);
        }

        // reset the result set
        rs = null;

        if ((aeTerm != null) && (aeTerm.length() > 0)) {
            // check if AE_TERMS already existed
            // get description of query
            sql = "SELECT DESCRIPTION FROM AE_TERMS WHERE QUERY_SEQ = ?";
            params.add(new Integer(queryNumber));

            rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
            params.clear();
            // ae term is already existed so update it.
            if (rs.next()) {
                params.add(aeTerm);
                params.add(new Integer(queryNumber));
                sql = "UPDATE AE_TERMS " + "SET "
                        + "description = upper(?) " + "WHERE "
                        + "query_seq = ? "; // (AJW) SQL statement
                jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
                params.clear();
                // altered to include
                // upper(..) conversions

                // no AE_TERMS existed so add one
            } else {
                params.add(new Integer(queryNumber));
                params.add(aeTerm);
                sql = "INSERT INTO AE_TERMS ( " + "query_seq, "
                        + "description ) " + "VALUES (" + "?, "
                        + "upper(?) ) "; // (AJW) SQL statement altered
                jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
                params.clear();
                // to include upper(..)
                // conversions
            }
        }
        // update case details
        updateCase(caseDTOList, queryNumber);

        // update additional comments for the query of type case only if not
        // null
        if ((addComment != null) && (addComment.length() > 0)) {
            params.add(addComment);
            params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
            params.add(Calendar.getInstance());
            params.add(new Integer(queryNumber));
            sql = "UPDATE QUERIES " + "SET "
                    + "case_comment = upper(?), " + "status_code = ?, "
                    + "datetime_submitted = ? WHERE "
                    + "query_seq = ? "; // (AJW) SQL statement altered
            jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));

            // to include upper(..)
            // conversions
        } else {
            // finally lets update the status of this query to submitted
            updateStatus(queryNumber, Constants.QUERY_STATUS_NOT_SUBMITTED);
        }

        // commit
        // sqlExec.commitTrans();


    }// updateDrugEvent

    @Override
    public void updateQueryDesc(QueryDescDTO queryDesc, int queryNumber) {

        List<Object> params = new ArrayList<Object>();
        // sqlExec.setAutoCommit(false);

        // add description of query
        params.add(queryDesc.getNatureOfCase());
        params.add(queryDesc.getIndexCase());
        params.add(queryDesc.getPmh());
        params.add(queryDesc.getSignsSymp());
        params.add(queryDesc.getConMeds());
        params.add(queryDesc.getInvestigations());
        params.add(queryDesc.getDiagnosis());
        params.add(queryDesc.getOtherInfo());
        params.add(new Integer(queryNumber));

        String sql = "UPDATE QUERY_DESCRIPTIONS "
                + "SET nature_of_case = upper(?), "
                + "index_case = upper(?), "
                + "prev_medical_history = upper(?), "
                + "signs_and_symptoms = upper(?), "
                + "conmeds = upper(?), " + "investigations = upper(?), "
                + "diagnoses = upper(?), " + "other_info = upper(?) "
                + "WHERE " + "query_seq = ? "; // (AJW) SQL


        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));

    }// updateQueryDesc

    public void deleteQueryDesc(int queryNumber) {
    	jdbcTemplate.update("DELETE FROM QUERY_DESCRIPTIONS WHERE query_seq = ?", queryNumber);
    }

    @Override
	public void updateCase(List caseDTO, int queryNumber) {

        List<Object> params = new ArrayList<Object>();

        // delete from cases
        params.add(new Integer(queryNumber));
        String sql = "DELETE FROM cases WHERE query_seq=?";
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
        // update case details if not null
        if (caseDTO != null) {

            String casesSql = "INSERT INTO CASES ( " + "query_seq, "
                    + "case_seq, " + "aer_number, "
                    + "local_reference_number, "
                    + "external_reference_number ) " + "VALUES ( " + "?, "
                    + "(select max(case_seq)+1 from cases), " + "upper(?), " //change this meth into jpa!
                    + "upper(?), " + "upper(?) ) "; // (AJW) SQL statement
            // altered to include
            // upper(..) conversions

            // iterate for each cases
            for (int i = 0; i < caseDTO.size(); i++) {
                // get the aer, local ref number and external ref number for
                // each case
                String aer = ((CaseDTO) caseDTO.get(i)).getAerNumber();
                String localRef = ((CaseDTO) caseDTO.get(i))
                        .getLocalRefNumber();
                String extRef = ((CaseDTO) caseDTO.get(i))
                        .getExtRefNumber();

                // only add a record if either the AER number, local ref no
                // or the external ref no is entered
                if ((aer.length() > 0) || (localRef.length() > 0)
                        || (extRef.length() > 0)) {

                    params.add(new Integer(queryNumber));
                    params.add(aer);
                    params.add(localRef);
                    params.add(extRef);
                    jdbcTemplate.update(casesSql, params.toArray(new Object[params.size()]));
                    params.clear();
                }// inner if
            }// for

            // sqlExec.commitTrans();

        }// outer if

    }// updateCase


    /**
     * updates the status of a query based on its query number
     *
     * @param queryNumber the query number to add this query type to
     * @param status      the status of the query
     */
    @Override
	public void updateStatus(int queryNumber, String status) {

        List<Object> params = new ArrayList<Object>();
        // update the status of this query
        params.add(status);
        params.add(Calendar.getInstance());
        params.add(new Integer(queryNumber));
        String casesSql = "UPDATE QUERIES " + "SET " + "status_code = ?, "
                + "datetime_submitted = ? " + "WHERE "
                + "query_seq = ? ";
        jdbcTemplate.update(casesSql, params.toArray(new Object[params.size()]));
    }// updateStatus

    /**
     * Adds the details for cases(s) for a given query number
     *
     * @param caseDTO     list of case dto's
     * @param queryNumber the query number to add this query type to
     */
    private void addCase(List caseDTO, int queryNumber) {
        List<Object> params = new ArrayList<Object>();
        // add case details if not null
        if (caseDTO != null) {

            String casesSql = "INSERT INTO CASES ( " + "query_seq, "
                    + "case_seq, " + "aer_number, "
                    + "local_reference_number, "
                    + "external_reference_number ) " + "VALUES ( " + "?, "
                    + "(select max(case_seq)+1 from CASES), " + "upper(?), "
                    + "upper(?), " + "upper(?) ) "; // (AJW) SQL statement
            // altered to include
            // upper(..) conversions

            // iterate for each cases
            for (int i = 0; i < caseDTO.size(); i++) {
                // get the aer, local ref number and external ref number for
                // each case
                String aer = ((CaseDTO) caseDTO.get(i)).getAerNumber();
                String localRef = ((CaseDTO) caseDTO.get(i))
                        .getLocalRefNumber();
                String extRef = ((CaseDTO) caseDTO.get(i))
                        .getExtRefNumber();

                // only add a record if either the AER number, local ref no
                // or the external ref no is entered
                if ((aer.length() > 0) || (localRef.length() > 0)
                        || (extRef.length() > 0)) {

                    params.add(new Integer(queryNumber));
                    params.add(aer);
                    params.add(localRef);
                    params.add(extRef);

                    jdbcTemplate.update(casesSql, params.toArray(new Object[params.size()]));
                    params.clear();
                }// inner if
            }// for
        }// outer if
    }// addCase


    /**
     * Attach docs
     *
     * @param attachments
     */
    @Override
    public void createAttachments(List attachments, int queryId) {
        Query query1 = queryService.find((long)queryId);
        for (Iterator iter = attachments.iterator(); iter.hasNext();){
              AttachmentDTO attachDTO = (AttachmentDTO) iter.next();
              logger.debug("attachDTO " + attachDTO.getFileName());
              logger.debug("attachDTO " + attachDTO.getTitle());
              Attachment attachment = attachDTO.createAttachment(queryId);

            if(StringUtils.isEmpty(attachment.getFollowupNumber())){
                attachment.setFollowupNumber(query1.getFollowupNumber());
                attachment.setFollowupSeq(query1.getFollowupSeq());
            }
            if(StringUtils.isEmpty(attachment.getFollowupNumber())){
                attachment.setFollowupNumber("FU0000");
            }

            attachmentService.merge(attachment);
        }

//        List<Object> params = new ArrayList<Object>();
//
//
//            // add attachments
//            if ((attachments != null) && (!attachments.isEmpty())) {
//
//                String attmtSql = "INSERT INTO ATTACHMENTS ( "
//                        + "query_seq, "
//                        + "attachment_seq, "
//                        + "attachment_category_id, "
//                        + "create_ts, "
//                        + "title, "
//                        + "version, "
//                        + "medical_opinion_flag, "
//                        + "filename, "
//                        + "author, "
//                        + "activation_flag, "
//                        + "followup_seq ) "
//                        + " VALUES ( "
//                        + "?, "
//                        + "?, "
//                        + "?, "
//                        + "?, "
//                        + "upper(?), "
//                        + "?, "
//                        + "?, "
//                        + "?, "
//                        + "upper(?), "
//                        + "'Y' "
//                        + ", (select followup_seq from queries where query_seq=?)"
//                        + " ) "; // (AJW) SQL statement altered to
//                // include upper(..) conversions
//                logger.debug("createAttachments()");
//                // iterate for each attached document
//                for (Object attachment : attachments) {
//                 Long seq=jdbcTemplate.queryForLong("select max(attachment_seq)+1 from ATTACHMENTS");
//                    params.add(queryId);
//                    params.add(seq);
//                    params.add(Constants.QRF_ATTACHMENT_CATEGORY);
//                    params.add(Calendar.getInstance());
//                    params.add(((AttachmentDTO) attachment).getTitle());
//                    params.add(Constants.QRF_ATTACHMENT_VERSION);
//                    params.add(Constants.NO.toString()); // we are forced to enter
//                    // this as it is mandatory
//                    params.add(((AttachmentDTO) attachment).getFileName());
//                    params.add(((AttachmentDTO) attachment).getAuthor()); //
//                    params.add(queryId);
//
//                    jdbcTemplate.update(attmtSql, params.toArray(new Object[params.size()]));
//                    params.clear();
//                }// end for
//                logger.debug("createAttachments() Created " + attachments.size()
//                        + " ATTACHMENTS");
//
//
//    }
    }

    // Updatess the details for drug interaction query type
    @Override
    public void updateDrugInteraction(QueryDescDTO queryDesc, List caseDTOList,
                                      int queryNumber, String addComment, List drugDTOList) {

        List<Object> params = new ArrayList<Object>();

        // first delete the drugs if not first drug
        String sql = "DELETE FROM DRUGS WHERE query_seq=? and first_flag = 'N'";
        params.add(new Integer(queryNumber));
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
        // add drug details

        String drugsSql = "INSERT INTO DRUGS ( " + "query_seq, "
                + "drug_number_seq, " + "first_flag, "
                + "roche_drug_flag, " + "drug_retrieval_name, "
                + "inn_generic_name, " + "route, " + "formulation, "
                + "dose ) " + "VALUES ( " + "?, "
                + "?, " + "?, " + "?, "
                + "?, " + "upper(?), " + "upper(?), " + "upper(?), "
                + "upper(?) ) "; // (AJW) SQL statement altered to
        // include upper(..) conversions
        if (drugDTOList != null) {
            // iterate for each drug details entered
            for (int i = 0; i < drugDTOList.size(); i++) {
                Long seq=jdbcTemplate.queryForLong("select max(drug_number_seq)+1 from DRUGS");
                DrugDTO drug = (DrugDTO) drugDTOList.get(i);
                params.add(new Integer(queryNumber));
                params.add(seq);
                params.add(Constants.NO.toString());// this cannot be first drug
                params.add(drug.isRocheDrug() ? Constants.YES.toString()
                        : Constants.NO.toString());
                params.add(drug.getRetrName());
                params.add(drug.getGenericName());
                params.add(drug.getRoute());
                params.add(drug.getFormulation());
                params.add(drug.getDose());
                jdbcTemplate.update(drugsSql, params.toArray(new Object[params.size()]));
                params.clear();
            }// for
        }// if

        // check if QUERY_DESCRIPTIONS already exists
        sql = "SELECT QUERY_SEQ FROM QUERY_DESCRIPTIONS WHERE QUERY_SEQ = ?";
        params.add(new Integer(queryNumber));

        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
        params.clear();
        // QUERY_DESCRIPTIONS is already existed so update it.
        if (rs.next()) {
            // update description of query
            updateQueryDesc(queryDesc, queryNumber);
        } else {
            // add description of query
            addQueryDesc(queryDesc, queryNumber);
        }

        // reset the result set
        rs = null;

        // update case details
        updateCase(caseDTOList, queryNumber);

        // add additional comments for the query of type case only if not
        // null
        if ((addComment != null) && (addComment.length() > 0)) {
            params.add(addComment);
            params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
            params.add(Calendar.getInstance());
            params.add(new Integer(queryNumber));
            jdbcTemplate.update("UPDATE QUERIES " + "SET "
                    + "case_comment = upper(?), " + "status_code = ?, "
                    + "datetime_submitted = ? " + "WHERE "
                    + "query_seq = ? ", params.toArray(new Object[params.size()])); // (AJW) SQL statement altered
            // to include upper(..)
            // conversions
        } else {
            // finally lets update the status of this query to submitted
            updateStatus(queryNumber, Constants.QUERY_STATUS_NOT_SUBMITTED);
        }
    }// updateDrugInteraction

    /**
	 * Adds the details for drug interaction query type
	 *
	 * @param queryDesc
	 *            QueryDescDTO object
	 * @param caseDTOList
	 *            a list consisting of case dto's
	 * @param queryNumber
	 *            the query number to add this query type to
	 * @param addComment
	 *            any additional comments
	 * @param drugDTOList
	 *            a list consisting of drug dto's
	 */
    @Override
	public void addDrugInteraction(QueryDescDTO queryDesc, List caseDTOList,
			int queryNumber, String addComment, List drugDTOList) {

            List<Object> params = new ArrayList<Object>();
			// add drug details

			String drugsSql = "INSERT INTO DRUGS ( " + "query_seq, "
					+ "drug_number_seq, " + "first_flag, "
					+ "roche_drug_flag, " + "drug_retrieval_name, "
					+ "inn_generic_name, " + "route, " + "formulation, "
					+ "dose ) " + "VALUES ( " + "?, "
					+ "?, " + "?, " + "?, "
					+ "?, " + "upper(?), " + "upper(?), " + "upper(?), "
					+ "upper(?) ) "; // (AJW) SQL statement altered to
			// include upper(..) conversions
			if (drugDTOList != null) {
				// iterate for each drug details entered
                for (Object aDrugDTOList : drugDTOList) {
                    Long seq=jdbcTemplate.queryForLong("select max(drug_number_seq)+1 from DRUGS");
                    DrugDTO drug = (DrugDTO) aDrugDTOList;
                    params.add(new Integer(queryNumber));
                    params.add(seq);
                    params.add(Constants.NO.toString());// this cannot be first drug
                    params.add(drug.isRocheDrug() ? Constants.YES.toString()
                            : Constants.NO.toString());
                    params.add(drug.getRetrName());
                    params.add(drug.getGenericName());
                    params.add(drug.getRoute());
                    params.add(drug.getFormulation());
                    params.add(drug.getDose());
                    jdbcTemplate.update(drugsSql, params.toArray(new Object[params.size()]));
                    params.clear();
                }// for
			}// if

			// add description of query
			addQueryDesc(queryDesc, queryNumber);

			// add case details
			addCase(caseDTOList, queryNumber);

			// add additional comments for the query of type case only if not
			// null
			if ((addComment != null) && (addComment.length() > 0)) {
				params.add(addComment);
				params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
                params.add(Calendar.getInstance());
				params.add(new Integer(queryNumber));
				jdbcTemplate.update("UPDATE QUERIES " + "SET "
						+ "case_comment = upper(?), " + "status_code = ?, "
						+ "datetime_submitted = ?  " + "WHERE "
						+ "query_seq = ? ", params.toArray(new Object[params.size()])); // (AJW) SQL statement altered
				params.clear();
			} else {
				// finally lets update the status of this query to submitted
				updateStatus(queryNumber, Constants.QUERY_STATUS_NOT_SUBMITTED);
			}
	}// addDrugInteraction

    @Override
	public void updateManufacturing(QueryDescDTO queryDescDto, Manufacturing manufacturing, ClinicalTrial clinical,
			String aeTerm, String specialRequest, Integer queryNumber) {
        List<Object> params = new ArrayList<Object>();

		// check if QUERY_DESCRIPTIONS already exists
		String sql = "SELECT QUERY_SEQ FROM QUERY_DESCRIPTIONS WHERE QUERY_SEQ = ?";
		params.add(new Integer(queryNumber));
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
        params.clear();
		// QUERY_DESCRIPTIONS is already existed so update it.
		if (rs.next()) {
			// update description of query
			updateQueryDesc(queryDescDto, queryNumber);
		} else {
			// add description of query
			addQueryDesc(queryDescDto, queryNumber);
		}

		// reset the result set
		rs = null;

		sql = "SELECT QUERY_SEQ FROM AE_TERMS WHERE QUERY_SEQ = ?";
		params.add(new Integer(queryNumber));
        rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
        params.clear();
		if (rs.next()) {
			updateAeTerm(aeTerm, queryNumber);
		} else {
			addAeTerm(aeTerm, queryNumber);
		}

		// reset the result set
		rs = null;
		// check if AUDITS already exists
		sql = "SELECT QUERY_SEQ FROM MANUFACTURING WHERE QUERY_SEQ = ?";
		params.add(new Integer(queryNumber));
        rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
        params.clear();

        if (rs.next()) {
			updateManufacturing(manufacturing, queryNumber);
		} else {
			addManufacturing(manufacturing, queryNumber);
		}

		// reset the result set
		rs = null;

		// update clinical trial details
		updateClinical(clinical, queryNumber);

		params.add(specialRequest);
		params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
        params.add(Calendar.getInstance());
		params.add(new Integer(queryNumber));
		sql="UPDATE QUERIES " + "SET "
				+ "oformat_special_request = upper(?), "
				+ "status_code = ?, " + "datetime_submitted = ? "
				+ "WHERE " + "query_seq = ? ";
		jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
	}// updateManufacturing

    @Override
    public void addManufacturing(QueryDescDTO queryDescDto, Manufacturing manufacturing, ClinicalTrial clinical,
            String aeTerm, String specialRequest, Integer queryNumber) {
        List<Object> params = new ArrayList<Object>();

        // add description of query
        params.add(new Integer(queryNumber));
        params.add(queryDescDto.getNatureOfCase());
        params.add(queryDescDto.getOtherInfo());
        String sql = "INSERT INTO QUERY_DESCRIPTIONS ( "
                + "query_seq, " + "nature_of_case, " + "other_info ) "
                + "VALUES ( " + "?, " + "upper(?), " + "upper(?) ) "; // (AJW)
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();

        addManufacturing(manufacturing, queryNumber);

        // add clinical trial details
        addClinical(clinical, queryNumber);

        addAeTerm(aeTerm, queryNumber);

        // add special requests if not null
        if (StringUtils.isNotBlank(specialRequest)) {
            params.add(specialRequest);
            params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
            params.add(Calendar.getInstance());
            params.add(new Integer(queryNumber));
            sql = ("UPDATE QUERIES " + "SET "
                    + "oformat_special_request = upper(?), "
                    + "status_code = ?, " + "datetime_submitted = ? "
                    + "WHERE " + "query_seq = ? "); // (AJW) SQL statement
           jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();
        } else {
            updateStatus(queryNumber, Constants.QUERY_STATUS_NOT_SUBMITTED);
        }
    }// addManufacturing

    private void addManufacturing(Manufacturing manufacturing,
			Integer queryNumber) {
        List<Object> params = new ArrayList<Object>();
        params.add(queryNumber);
        params.add(manufacturing.getType() != null ? manufacturing.getType().name() : null);
        params.add(manufacturing.getBatchNo());
        params.add(manufacturing.getTrackwiseNo());
        String sql = "INSERT INTO MANUFACTURING(query_seq, type, batch_no, trackwise_no) " +
        		"VALUES(?, ?, upper(?), upper(?))";
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
	}

    private void updateManufacturing(Manufacturing manufacturing,
			Integer queryNumber) {
        List<Object> params = new ArrayList<Object>();
        params.add(manufacturing.getType() != null ? manufacturing.getType().name() : null);
        params.add(manufacturing.getBatchNo());
        params.add(manufacturing.getTrackwiseNo());
        params.add(queryNumber);
        String sql = "UPDATE MANUFACTURING SET type = ?, batch_no = upper(?), trackwise_no = upper(?) " +
        		"WHERE query_seq = ?";
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
	}

	@Override
    public void updateExternalAudit(QueryDescDTO queryDescDto, Audit audit, ClinicalTrial clinical, String specialRequest, int queryNumber) {
            List<Object> params = new ArrayList<Object>();

			// check if QUERY_DESCRIPTIONS already exists
			String sql = "SELECT QUERY_SEQ FROM QUERY_DESCRIPTIONS WHERE QUERY_SEQ = ?";
			params.add(new Integer(queryNumber));
            SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
            params.clear();
			// QUERY_DESCRIPTIONS is already existed so update it.
			if (rs.next()) {
				// update description of query
				updateQueryDesc(queryDescDto, queryNumber);
			} else {
				// add description of query
				addQueryDesc(queryDescDto, queryNumber);
			}

			// reset the result set
			rs = null;

			// check if AUDITS already exists
			sql = "SELECT QUERY_SEQ FROM AUDITS WHERE QUERY_SEQ = ?";
			params.add(new Integer(queryNumber));
            rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
            params.clear();

            if (rs.next()) {
				updateAudit(audit, queryNumber);
			} else {
				addAudit(audit, queryNumber);
			}

			// reset the result set
			rs = null;

			// update clinical trial details
			updateClinical(clinical, queryNumber);

			params.add(specialRequest);
			params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
            params.add(Calendar.getInstance());
			params.add(new Integer(queryNumber));
			sql="UPDATE QUERIES " + "SET "
					+ "oformat_special_request = upper(?), "
					+ "status_code = ?, " + "datetime_submitted = ? "
					+ "WHERE " + "query_seq = ? ";
			jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();
    }

    private void updateAudit(Audit audit, int queryNumber) {
        List<Object> params = new ArrayList<Object>();
        params.add(audit.getAllCases());
        params.add(audit.getAllStudies());
        params.add(audit.getStudyNo());
        params.add(audit.getAllSpontaneousCases());
        params.add(audit.getAllSubm());
        params.add(audit.getSusarSubm());
        params.add(audit.getSeriousCases());
        params.add(audit.getNonSeriousCases());
        params.add(audit.getSubmissionTitles());
        params.add(queryNumber);
        String sql = "UPDATE AUDITS SET all_cases = ?, all_studies = ?, study_no = upper(?), " +
        		"all_spontaneous_cases = ?, all_subm = ?, susar_subm = ?, serious_cases = ?, " +
        		"non_serious_cases = ?, submission_titles = upper(?) " +
        		"WHERE query_seq = ?";
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
	}

    private void updateAeTerm(String aeTerm, int queryNumber) {
        List<Object> params = new ArrayList<Object>();
        params.add(aeTerm);
        params.add(queryNumber);
        String sql = "UPDATE AE_TERMS SET description = upper(?) " +
        		"WHERE query_seq = ?";
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
	}


	/**
     * Updates the details for a case clarification query type
     */
    @Override
    public void updateCaseClarification(List caseDTOList, int queryNumber,
                                        String addComment, int outputId, String others) {

        List<Object> params = new ArrayList<Object>();


        // sqlExec.setAutoCommit(false);

        // add case details if not null
        updateCase(caseDTOList, queryNumber);

        // add output format and others comment
        if (outputId == 0) {
            params.add(addComment);
            params.add(others);
            params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
            params.add(Calendar.getInstance());
            params.add(new Integer(queryNumber));
            String sql = ("UPDATE QUERIES " + "SET "
                    + "case_oformat_id = null, "
                    + "case_comment = upper(?), "
                    + "oformat_special_request = upper(?), "
                    + "status_code = ?, " + "datetime_submitted = ? "
                    + "WHERE " + "query_seq = ? "); // (AJW) SQL statement
           jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();
            // altered to include
            // upper(..) conversions
        } else {
            params.add(new Integer(outputId));
            params.add(addComment);
            params.add(others);
            params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
            params.add(Calendar.getInstance());
            params.add(new Integer(queryNumber));
            String sql = ("UPDATE QUERIES " + "SET "
                    + "case_oformat_id = ?, " + "case_comment = upper(?), "
                    + "oformat_special_request = upper(?), "
                    + "status_code = ?, " + "datetime_submitted = ? "
                    + "WHERE " + "query_seq = ? "); // (AJW) SQL statement
           jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();
        }

        // commit
        // sqlExec.commitTrans();


    }// addCaseClarification

    @Override
    public void addCaseClarification(List caseDTOList, int queryNumber,
                                     String addComment, int outputId, String others) {

        List<Object> params = new ArrayList<Object>();


        // sqlExec.setAutoCommit(false);

        // add case details if not null
        addCase(caseDTOList, queryNumber);

        // add output format and others comment
        if (outputId == 0) {
            params.add(addComment);
            params.add(others);
            params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
            params.add(Calendar.getInstance());
            params.add(new Integer(queryNumber));
            String sql = ("UPDATE QUERIES " + "SET "
                    + "case_comment = upper(?), "
                    + "oformat_special_request = upper(?), "
                    + "status_code = ?, " + "datetime_submitted = ? "
                    + "WHERE " + "query_seq = ? "); // (AJW) SQL statement
           jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();
            // altered to include
            // upper(..) conversions
        } else {
            params.add(new Integer(outputId));
            params.add(addComment);
            params.add(others);
            params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
            params.add(Calendar.getInstance());
            params.add(new Integer(queryNumber));
            String sql = ("UPDATE QUERIES " + "SET "
                    + "case_oformat_id = ?, " + "case_comment = upper(?), "
                    + "oformat_special_request = upper(?), "
                    + "status_code = ?, " + "datetime_submitted = ? "
                    + "WHERE " + "query_seq = ? "); // (AJW) SQL statement
           jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();
            // altered to include
            // upper(..) conversions
        }

        // commit
        // sqlExec.commitTrans();


    }// addCaseClarification

    @Override
    public void addINDUpdates(String indNumber, int queryNumber) {

        List<Object> params = new ArrayList<Object>();


        // sqlExec.setAutoCommit(false);

        params.add(indNumber);
        params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
        params.add(Calendar.getInstance());
        params.add(new Integer(queryNumber));
        String sql = ("UPDATE QUERIES " + "SET "
                + "ind_number = upper(?), " + "status_code = ?, "
                + "datetime_submitted = ? " + "WHERE "
                + "query_seq = ?"); // (AJW) SQL statement altered to
       jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
        // include upper(..) conversions
        // commit
        // sqlExec.commitTrans();

    }// addINDUpdates

    @Override
    public void updateLiterature(QueryDescDTO queryDesc, int queryNumber,
                                 String aeTerm) {

        List<Object> params = new ArrayList<Object>();


        // sqlExec.setAutoCommit(false);

        // check if QUERY_DESCRIPTIONS already exists
        String sql = "SELECT QUERY_SEQ FROM QUERY_DESCRIPTIONS WHERE QUERY_SEQ = ?";
        params.add(new Integer(queryNumber));
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
        params.clear();
        // QUERY_DESCRIPTIONS is already existed so update it.
        if (rs.next()) {
            // update description of query
            updateQueryDesc(queryDesc, queryNumber);
        } else {
            // add description of query
            addQueryDesc(queryDesc, queryNumber);
        }

        // reset the result set
        rs = null;

        if ((aeTerm != null) && (aeTerm.length() > 0)) {
            // check if AE_TERMS already existed
            // get description of query
            sql = "SELECT DESCRIPTION FROM AE_TERMS WHERE QUERY_SEQ = ?";
            params.add(new Integer(queryNumber));
            rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
            params.clear();

            // ae term is already existed so update it.
            if (rs.next()) {
                params.add(aeTerm);
                params.add(new Integer(queryNumber));
                sql = ("UPDATE AE_TERMS " + "SET "
                        + "description = upper(?) " + "WHERE "
                        + "query_seq = ? "); // (AJW) SQL statement
               jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
                params.clear();
                // altered to include
                // upper(..) conversions

                // no AE_TERMS existed so add one
            } else {
                params.add(new Integer(queryNumber));
                params.add(aeTerm);
                sql = ("INSERT INTO AE_TERMS ( " + "query_seq, "
                        + "description ) " + "VALUES (" + "?, "
                        + "upper(?) ) "); // (AJW) SQL statement altered
               jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
                params.clear();
                // to include upper(..)
                // conversions
            }
        }
        // finally lets update the status of this query to submitted
        updateStatus(queryNumber, Constants.QUERY_STATUS_NOT_SUBMITTED);

        // commit
        // sqlExec.commitTrans();
    }// updateLiterature


    @Override
    public void addLiterature(QueryDescDTO queryDesc, int queryNumber,
                              String aeTerm) {

        List<Object> params = new ArrayList<Object>();


        // sqlExec.setAutoCommit(false);

        // add description of query
        params.add(new Integer(queryNumber));
        params.add(queryDesc.getNatureOfCase());
        params.add(queryDesc.getOtherInfo());
        String sql = ("INSERT INTO QUERY_DESCRIPTIONS ( "
                + "query_seq, " + "nature_of_case, " + "other_info ) "
                + "VALUES ( " + "?, " + "upper(?), " + "upper(?) ) "); // (AJW)
       jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
        // SQL
        // statement
        // altered
        // to
        // include
        // upper(..)
        // conversions

        // add ae term if not null
        if ((aeTerm != null) && (aeTerm.length() > 0)) {
            params.add(new Integer(queryNumber));
            params.add(aeTerm);
            sql = ("INSERT INTO AE_TERMS ( " + "query_seq, "
                    + "description ) " + "VALUES ( " + "?, "
                    + "upper(?) ) "); // (AJW) SQL statement altered to
           jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();
            // include upper(..) conversions
        }

        // finally lets update the status of this query to submitted
        updateStatus(queryNumber, Constants.QUERY_STATUS_NOT_SUBMITTED);

    }// addLiterature

    @Override
    public void addPerformanceMetrics(PerformanceMetric perf, int queryNumber) {

        List<Object> params = new ArrayList<Object>();

        params.add(new Integer(queryNumber));
        params.add(perf.getRequestDescription());
        params.add(perf.getOutputFormat().getCtrlOformatId());
        params.add(perf.getSpecialRequests());
        String sql = ("INSERT INTO PERFORMANCE_METRICS (query_seq, request_description, ctrl_oformat_id, special_requests) "
                + "VALUES (?, upper(?), ?, upper(?))");
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();

       	updateStatus(queryNumber, Constants.QUERY_STATUS_NOT_SUBMITTED);
    }// addPerformanceMetrics

    @Override
    public void updatePerformanceMetrics(PerformanceMetric perf, int queryNumber) {

        List<Object> params = new ArrayList<Object>();

        // check if PERFORMANCE_METRICS already exists
        String sql = "SELECT QUERY_SEQ FROM PERFORMANCE_METRICS WHERE QUERY_SEQ = ?";
        params.add(Integer.valueOf(queryNumber));
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
        params.clear();
        // PERFORMANCE_METRICS is already existed so update it.
        if (rs.next()) {
        	params.add(perf.getRequestDescription());
            params.add(perf.getOutputFormat().getCtrlOformatId());
            params.add(perf.getSpecialRequests());
            params.add(Integer.valueOf(queryNumber));
            sql = ("UPDATE PERFORMANCE_METRICS SET "
                    + "request_description = upper(?), "
                    + "ctrl_oformat_id = ?, "
                    + "special_requests = ? "
                    + "WHERE query_seq = ? ");
            jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();

           	updateStatus(queryNumber, Constants.QUERY_STATUS_NOT_SUBMITTED);
        } else {
        	addPerformanceMetrics(perf, queryNumber);
        }

        // reset the result set
        rs = null;
    }// updatePerformanceMetrics


    @Override
    public void updateSignalDetection(QueryDescDTO queryDesc, int queryNumber,
                                      String aeTerm) {

        List<Object> params = new ArrayList<Object>();


        // sqlExec.setAutoCommit(false);

        // check if QUERY_DESCRIPTIONS already exists
        String sql = "SELECT QUERY_SEQ FROM QUERY_DESCRIPTIONS WHERE QUERY_SEQ = ?";
        params.add(new Integer(queryNumber));
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
        params.clear();
        // QUERY_DESCRIPTIONS is already existed so update it.
        if (rs.next()) {
            // update description of query
            updateQueryDesc(queryDesc, queryNumber);
        } else {
            // add description of query
            addQueryDesc(queryDesc, queryNumber);
        }

        // reset the result set
        rs = null;

        if ((aeTerm != null) && (aeTerm.length() > 0)) {
            // check if AE_TERMS already existed
            // get description of query
            sql = "SELECT DESCRIPTION FROM AE_TERMS WHERE QUERY_SEQ = ?";
            params.add(new Integer(queryNumber));
            rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
            params.clear();

            // ae term is already existed so update it.
            if (rs.next()) {
                params.add(aeTerm);
                params.add(new Integer(queryNumber));
                sql = ("UPDATE AE_TERMS " + "SET "
                        + "description = upper(?) " + "WHERE "
                        + "query_seq = ? "); // (AJW) SQL statement
               jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
                params.clear();
                // altered to include
                // upper(..) conversions
                // no AE_TERMS existed so add one
            } else {
                params.add(new Integer(queryNumber));
                params.add(aeTerm);
                sql = ("INSERT INTO AE_TERMS ( " + "query_seq, "
                        + "description ) " + "VALUES (" + "?, "
                        + "upper(?) ) "); // (AJW) SQL statement altered
               jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
                params.clear();
                // to include upper(..)
                // conversions
            }
        }
        // finally lets update the status of this query to submitted
        updateStatus(queryNumber, Constants.QUERY_STATUS_NOT_SUBMITTED);

        // commit
        // sqlExec.commitTrans();

    }// updateSignalDetection

    @Override
    public void addSignalDetection(QueryDescDTO queryDesc, int queryNumber,
                                   String aeTerm) {

        List<Object> params = new ArrayList<Object>();


        // sqlExec.setAutoCommit(false);

        // add description of query
        params.add(new Integer(queryNumber));
        params.add(queryDesc.getNatureOfCase());
        String sql = ("INSERT INTO QUERY_DESCRIPTIONS ( "
                + "query_seq, " + "nature_of_case ) " + "VALUES ( " + "?, "
                + "upper(?) ) "); // (AJW) SQL statement altered to
       jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
        // include upper(..) conversions

        // add ae term if not null
        if ((aeTerm != null) && (aeTerm.length() > 0)) {
            params.add(new Integer(queryNumber));
            params.add(aeTerm);
            sql = ("INSERT INTO AE_TERMS ( " + "query_seq, "
                    + "description ) " + "VALUES ( " + "?, "
                    + "upper(?) ) "); // (AJW) SQL statement altered to
           jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();
            // include upper(..) conversions
        }

        // finally lets update the status of this query to submitted
        updateStatus(queryNumber, Constants.QUERY_STATUS_NOT_SUBMITTED);

        // commit
        // sqlExec.commitTrans();

    }// addSignalDetection

    @Override
    public void addExternalAudit(QueryDescDTO queryDescDto, Audit audit, ClinicalTrial clinical, String specialRequest, int queryNumber) {

        List<Object> params = new ArrayList<Object>();

        // add description of query
        params.add(new Integer(queryNumber));
        params.add(queryDescDto.getNatureOfCase());
        params.add(queryDescDto.getOtherInfo());
        String sql = "INSERT INTO QUERY_DESCRIPTIONS ( "
                + "query_seq, " + "nature_of_case, " + "other_info ) "
                + "VALUES ( " + "?, " + "upper(?), " + "upper(?) ) "; // (AJW)
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();

        addAudit(audit, queryNumber);

        // add clinical trial details
        addClinical(clinical, queryNumber);

        // add special requests if not null
        if (StringUtils.isNotBlank(specialRequest)) {
            params.add(specialRequest);
            params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
            params.add(Calendar.getInstance());
            params.add(new Integer(queryNumber));
            sql = ("UPDATE QUERIES " + "SET "
                    + "oformat_special_request = upper(?), "
                    + "status_code = ?, " + "datetime_submitted = ? "
                    + "WHERE " + "query_seq = ? "); // (AJW) SQL statement
           jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();
        } else {
            updateStatus(queryNumber, Constants.QUERY_STATUS_NOT_SUBMITTED);
        }
    }

    private void addAudit(Audit audit, int queryNumber) {
        List<Object> params = new ArrayList<Object>();
        params.add(queryNumber);
        params.add(audit.getAllCases());
        params.add(audit.getAllStudies());
        params.add(audit.getStudyNo());
        params.add(audit.getAllSpontaneousCases());
        params.add(audit.getAllSubm());
        params.add(audit.getSusarSubm());
        params.add(audit.getSeriousCases());
        params.add(audit.getNonSeriousCases());
        params.add(audit.getSubmissionTitles());
        String sql = "INSERT INTO AUDITS(query_seq, all_cases, all_studies, study_no, all_spontaneous_cases, all_subm, susar_subm, " +
        		"serious_cases, non_serious_cases, submission_titles) " +
        		"VALUES(?, ?, ?, upper(?), ?, ?, ?, ?, ?, upper(?))";
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
	}

    private void addAeTerm(String aeTerm, int queryNumber) {
        List<Object> params = new ArrayList<Object>();
        params.add(queryNumber);
        params.add(aeTerm);
        String sql = "INSERT INTO AE_TERMS(query_seq, description) " +
        		"VALUES(?, upper(?))";
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
        params.clear();
	}


	/**
	 * Updates the details of a clinical trial for a given query number
	 *
	 * @param clinical
	 *            the clinical trial
	 * @param queryNumber
	 *            the query number to add this query type to
	 */
	private void updateClinical(ClinicalTrial clinical, int queryNumber) {
			List<Object> params = new ArrayList<Object>();
			// add clinical details
			if (clinical != null) {
				// check if CLINICAL_TRIALS already exists
				String sql = "SELECT QUERY_SEQ FROM CLINICAL_TRIALS WHERE QUERY_SEQ = ?";
				params.add(Integer.valueOf(queryNumber));
				SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
                params.clear();

				// CLINICAL_TRIALS is already existed so update it.
				if (rs.next()) {
					// reset the result set
					// update description of query
					if (clinical.getCtrlOformat() == null || clinical.getCtrlOformat().getCtrlOformatId() == 0) {
						params.add(clinical.getCaseSelection());
						params.add(clinical.getProtocolNumber());
						params.add(clinical.getPatientNumber());
						params.add(clinical.getCrtnNumber());
						params.add(queryNumber);

						sql = "UPDATE CLINICAL_TRIALS " + "SET "
								+ "case_selection = upper(?), "
								+ "protocol_numbers = upper(?), "
								+ "patient_numbers = upper(?), "
								+ "crtn_numbers = upper(?) " + "WHERE "
								+ "query_seq = ? ";
                       jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
					} else {
						params.add(clinical.getCaseSelection());
						params.add(clinical.getProtocolNumber());
						params.add(clinical.getPatientNumber());
						params.add(clinical.getCtrlOformat().getCtrlOformatId());
						params.add(clinical.getCrtnNumber());
						params.add(queryNumber);
						sql="UPDATE CLINICAL_TRIALS " + "SET "
								+ "case_selection = upper(?), "
								+ "protocol_numbers = upper(?), "
								+ "patient_numbers = upper(?), "
								+ "ctrl_oformat_id = ?, "
								+ "crtn_numbers = upper(?) " + "WHERE "
								+ "query_seq = ? ";
                       jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
                        params.clear();
					}
				} else {
					// add description of query
					addClinical(clinical, queryNumber);
				}
        }
    }

    /**
	 * Adds the details of a clinical trial for a given query number
	 *
	 * @param clinical
	 *            the clinical trial
	 * @param queryNumber
	 *            the query number to add this query type to
	 */
	private void addClinical(ClinicalTrial clinical, int queryNumber) {
          List<Object> params = new ArrayList<Object>();
		// add clinical details
			if (clinical != null) {
				if (clinical.getCtrlOformat() == null || clinical.getCtrlOformat().getCtrlOformatId() == 0) {
					params.add(queryNumber);
					params.add(clinical.getCaseSelection());
					params.add(clinical.getProtocolNumber());
					params.add(clinical.getPatientNumber());
					params.add(clinical.getCrtnNumber());
					String sql = "INSERT INTO CLINICAL_TRIALS ( "
							+ "query_seq, " + "case_selection, "
							+ "protocol_numbers, " + "patient_numbers, "
							+ "crtn_numbers ) " + "VALUES ( " + "?, "
							+ "upper(?), " + "upper(?), " + "upper(?), "
							+ "upper(?) ) "; // (AJW) SQL statement altered
					jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
                    params.clear();
				} else {
					params.add(queryNumber);
					params.add(clinical.getCaseSelection());
					params.add(clinical.getProtocolNumber());
					params.add(clinical.getPatientNumber());
					params.add(clinical.getCtrlOformat().getCtrlOformatId());
					params.add(clinical.getCrtnNumber());
					String sql = ("INSERT INTO CLINICAL_TRIALS ( "
							+ "query_seq, " + "case_selection, "
							+ "protocol_numbers, " + "patient_numbers, "
							+ "ctrl_oformat_id, " + "crtn_numbers ) "
							+ "VALUES ( " + "?, " + "upper(?), " + "upper(?), "
							+ "upper(?), " + "?, " + "upper(?) ) "); // (AJW)
					jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
                    params.clear();
				}

			}// end outer if

	}// addClinical

    @Override
    public void addQueryDesc(QueryDescDTO queryDesc, int queryNumber) {

        List<Object> params = new ArrayList<Object>();

        // add description of query
        params.add(new Integer(queryNumber));
        params.add(queryDesc.getNatureOfCase());
        params.add(queryDesc.getIndexCase());
        params.add(queryDesc.getPmh());
        params.add(queryDesc.getSignsSymp());
        params.add(queryDesc.getConMeds());
        params.add(queryDesc.getInvestigations());
        params.add(queryDesc.getDiagnosis());
        params.add(queryDesc.getOtherInfo());
        String sql = "INSERT INTO QUERY_DESCRIPTIONS ( "
                + "query_seq, nature_of_case, index_case, "
                + "prev_medical_history, signs_and_symptoms, "
                + "conmeds, investigations,diagnoses, "
                + "other_info )  VALUES ( ?,upper(?), "
                + "upper(?), upper(?), upper(?), upper(?), "
                + "upper(?), upper(?), upper(?) ) ";
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
    }// addQueryDesc

    @Override
    public void addQueryAssesment(QueryAssessmentDTO queryDesc, long queryNumber, String followupNumber) {

        List<Object> params = new ArrayList<Object>();

        // add description of query
        params.add(new Long(queryNumber));
        params.add(followupNumber!=null?followupNumber:"0");
        params.add(queryDesc.getCompanyRiskLevel());
        params.add(queryDesc.getProgramCodeCategory());
        params.add(queryDesc.getComplexQuery());
        params.add(queryDesc.getHighUserExperienceAndKnowledge());
        params.add(queryDesc.getComment());
        params.add(queryDesc.getChangedBy());
        String sql = "INSERT INTO QUERY_ASSESSMENTS ( "
                + "query_seq,followup_number, company_risk_level, program_code_category, "
                + "complex_query, HIGH_USER_EXPERIENCE, "
                + "ASSESSMENT_COMMENT, LAST_CHANGE, CHANGED_BY "
                + " )  VALUES ( ?,?,upper(?), "
                + "upper(?), upper(?), upper(?), upper(?) "
                + ",CURRENT_TIMESTAMP,upper(?)) ";
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
    }

    @Override
    public void updateQueryAssesment(QueryAssessmentDTO queryDesc, long queryNumber, String followupNumber) {

        List<Object> params = new ArrayList<Object>();

        // add description of query
        params.add(queryDesc.getCompanyRiskLevel());
        params.add(queryDesc.getProgramCodeCategory());
        params.add(queryDesc.getComplexQuery());
        params.add(queryDesc.getHighUserExperienceAndKnowledge());
        params.add(queryDesc.getComment());
        params.add(queryDesc.getChangedBy());
        params.add(new Long(queryNumber));
        params.add(followupNumber!=null?followupNumber:"0");
        String sql = "UPDATE  QUERY_ASSESSMENTS SET  "
                + "company_risk_level = upper(?), program_code_category=upper(?), "
                + "complex_query=upper(?), HIGH_USER_EXPERIENCE=upper(?), "
                + "ASSESSMENT_COMMENT=upper(?),LAST_CHANGE=CURRENT_TIMESTAMP, CHANGED_BY=upper(?) "
                + "where query_seq = ? and  followup_number=?";
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
    }

    @Override
    public void addQueryVerification(QueryVerificationDTO queryVer, long queryNumber, String followupNumber) {

        List<Object> params = new ArrayList<Object>();

        // add description of query
        params.add(new Long(queryNumber));
        params.add(followupNumber!=null?followupNumber:"0");
        params.add(queryVer.getRequirementsUnderstood());
        params.add(queryVer.getClarificationRequested());
        params.add(queryVer.getAssumptionsDocumented());
        params.add(queryVer.getFurtherRequirements());
        params.add(queryVer.getBuildType());
        params.add(queryVer.getCorrectParametersPassed());
        params.add(queryVer.getCodeReviewPassed());
        params.add(queryVer.getCheckPassed());
        params.add(queryVer.getResultsVerified());
        params.add(queryVer.getFurtherTestReference());
        params.add(queryVer.getResultsAppropriatelyLabeled());
        params.add(queryVer.getResultsStored());
        params.add(queryVer.getSearchCriteriaStated());

        params.add(queryVer.getDateCompleted());
        params.add(queryVer.getFilledBy());
        params.add(queryVer.getRecordType().toString());

        String sql = "INSERT INTO QUERY_VERIFICATION ( "
                + "query_seq, followup_number, REQUIREMENTS_UNDERSTOOD, CLARIFICATION_REQUESTED, "
                + "ASSUMPTIONS_DOCUMENTED, FURTHER_REQUIREMENTS, "
                + "BUILD_TYPE,CORRECT_PARAMETERS_PASSED, CODE_REVIEW_PASSED,CHECK_PASSED,RESULTS_VERIFIED,FURTHER_TEST_REFERENCE,"
                + "RESULTS_APPROPRIATELY_LABELED,RESULTS_STORED, SEARCH_CRITERIA_STATED, DATE_COMPLETED, FILLED_BY, RECORD_TYPE"
                + " )  VALUES ( ?,?,upper(?), "
                + "upper(?), upper(?), upper(?), upper(?),upper(?),upper(?),upper(?),upper(?),upper(?),upper(?),upper(?),upper(?), ?,upper(?),upper(?)"
                + ") ";
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
    }// addQueryDesc

    @Override
    public void updateQueryVerification(QueryVerificationDTO queryVer, long queryNumber, String followupNumber) {

        List<Object> params = new ArrayList<Object>();

        // add description of query
        params.add(queryVer.getRequirementsUnderstood());
        params.add(queryVer.getClarificationRequested());
        params.add(queryVer.getAssumptionsDocumented());
        params.add(queryVer.getFurtherRequirements());
        params.add(queryVer.getBuildType());
        params.add(queryVer.getCorrectParametersPassed());
        params.add(queryVer.getCodeReviewPassed());
        params.add(queryVer.getCheckPassed());
        params.add(queryVer.getResultsVerified());
        params.add(queryVer.getFurtherTestReference());
        params.add(queryVer.getResultsAppropriatelyLabeled());
        params.add(queryVer.getResultsStored());
        params.add(queryVer.getSearchCriteriaStated());

        params.add(queryVer.getDateCompleted());
        params.add(queryVer.getFilledBy());
        params.add(queryVer.getRecordType().toString());
        params.add(new Long(queryNumber));
        params.add(followupNumber!=null?followupNumber:"0");

        String sql = "UPDATE  QUERY_VERIFICATION SET  "
        		+ "REQUIREMENTS_UNDERSTOOD=upper(?), CLARIFICATION_REQUESTED=upper(?), "
                        + "ASSUMPTIONS_DOCUMENTED=upper(?), FURTHER_REQUIREMENTS=upper(?), "
                        + "BUILD_TYPE=upper(?), CORRECT_PARAMETERS_PASSED=upper(?), CODE_REVIEW_PASSED=upper(?),CHECK_PASSED=upper(?),RESULTS_VERIFIED=upper(?),FURTHER_TEST_REFERENCE=upper(?),"
                        + "RESULTS_APPROPRIATELY_LABELED=upper(?),RESULTS_STORED=upper(?), SEARCH_CRITERIA_STATED=upper(?), DATE_COMPLETED=?, FILLED_BY=upper(?)"
                + "where  RECORD_TYPE=upper(?) and query_seq = ? and followup_number=?";
        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
    }

    private void doDrugEvent(int queryId) {
        List<Object> params = new ArrayList<Object>();
        params.add(queryId);

        //delete from query_descriptions
        String queryStr = "DELETE FROM query_descriptions WHERE query_seq=?";
        jdbcTemplate.update(queryStr, params.toArray(new Object[params.size()]));

        //delete from cases
        queryStr = "DELETE FROM cases WHERE query_seq=?";
        jdbcTemplate.update(queryStr, params.toArray(new Object[params.size()]));

        //delete from ae term
        queryStr = "DELETE FROM ae_terms WHERE query_seq=?";
        jdbcTemplate.update(queryStr, params.toArray(new Object[params.size()]));

        //update any case comment
        queryStr = "UPDATE queries SET case_comment=NULL WHERE query_seq=?";
        jdbcTemplate.update(queryStr, params.toArray(new Object[params.size()]));
    }

    private void doDrugInteraction(int queryId) {

        List<Object> params = new ArrayList<Object>();
        params.add(queryId);

        //delete from drugs except the first drug
        params.add(Constants.NO.toString());
        jdbcTemplate.update("DELETE FROM drugs WHERE query_seq=? AND first_flag=?", params.toArray(new Object[params.size()]));

        params.remove(1);   //todo check
        //delete from query_descriptions
        jdbcTemplate.update("DELETE FROM query_descriptions WHERE query_seq=?", params.toArray(new Object[params.size()]));

        //delete from cases
        jdbcTemplate.update("DELETE FROM cases WHERE query_seq=?", params.toArray(new Object[params.size()]));

        //update any case comment to null
        jdbcTemplate.update("UPDATE queries SET case_comment=NULL WHERE query_seq=?", params.toArray(new Object[params.size()]));

    }

    private void doManufacturing(int queryId) {

        List<Object> params = new ArrayList<Object>();
        params.add(queryId);
        //delete from query_descriptions
        jdbcTemplate.update("DELETE FROM query_descriptions WHERE query_seq=?", params.toArray(new Object[params.size()]));

        //delete from cases
        jdbcTemplate.update("DELETE FROM cases WHERE query_seq=?", params.toArray(new Object[params.size()]));

        //update manufacturing, case output format, case details others and case comment to null
        jdbcTemplate.update("UPDATE queries SET case_comment=NULL, mnft_batch_number=NULL,"
                + "case_oformat_id=NULL, oformat_special_request=NULL WHERE query_seq=?", params.toArray(new Object[params.size()]));


    }

    private void doPregnancy(int queryId) {

        List<Object> params = new ArrayList<Object>();
        params.add(queryId);
        //delete from pregnancy
        jdbcTemplate.update("DELETE FROM pregnancies WHERE query_seq=?", params.toArray(new Object[params.size()]));

        //delete from query_descriptions
        jdbcTemplate.update("DELETE FROM query_descriptions WHERE query_seq=?", params.toArray(new Object[params.size()]));

        //delete from cases
        jdbcTemplate.update("DELETE FROM cases WHERE query_seq=?", params.toArray(new Object[params.size()]));

        //update case output format, case details others and case comment to null
        jdbcTemplate.update("UPDATE queries SET case_comment=NULL, case_oformat_id=NULL,"
                + " oformat_special_request=NULL WHERE query_seq=?", params.toArray(new Object[params.size()]));


    }

    private void doExternalAudit(int queryId) {

        List<Object> params = new ArrayList<Object>();
        params.add(queryId);
        //delete from query_descriptions
        jdbcTemplate.update("DELETE FROM query_descriptions WHERE query_seq=?", params.toArray(new Object[params.size()]));

        //delete from ae term
        jdbcTemplate.update("DELETE FROM ae_terms WHERE query_seq=?", params.toArray(new Object[params.size()]));

        //delete from clinical trial
        jdbcTemplate.update("DELETE FROM clinical_trials WHERE query_seq=?", params.toArray(new Object[params.size()]));

        //update clinical output format to null
        jdbcTemplate.update("UPDATE queries SET oformat_special_request=NULL WHERE query_seq=?", params.toArray(new Object[params.size()]));


    }

    private void doCaseClarification(int queryId) {

        List<Object> params = new ArrayList<Object>();
        params.add(queryId);

        //delete from cases
        jdbcTemplate.update("DELETE FROM cases WHERE query_seq=?", params.toArray(new Object[params.size()]));

        //update case output format, case details others and case comment to null
        jdbcTemplate.update("UPDATE queries SET case_comment=NULL, case_oformat_id=NULL,"
                + " oformat_special_request=NULL WHERE query_seq=?", params.toArray(new Object[params.size()]));


    }

    private void doSAEReconciliation(int queryId) {

        List<Object> params = new ArrayList<Object>();
        params.add(queryId);

        //delete from clinical trial
        jdbcTemplate.update("DELETE FROM clinical_trials WHERE query_seq=?", params.toArray(new Object[params.size()]));

        //update case output format, case details others and case comment to null
        jdbcTemplate.update("UPDATE queries SET oformat_special_request=NULL WHERE query_seq=?", params.toArray(new Object[params.size()]));


    }

    private void doINDUpdates(int queryId) {

        List<Object> params = new ArrayList<Object>();
        params.add(queryId);

        //update ind updates to null
        jdbcTemplate.update("UPDATE queries SET ind_number=NULL WHERE query_seq=?", params.toArray(new Object[params.size()]));


    }

    private void doPerformanceMetrics(int queryId) {

        List<Object> params = new ArrayList<Object>();
        params.add(queryId);

        //delete from clinical trial
        jdbcTemplate.update("DELETE FROM performance_metrics WHERE query_seq=?", params.toArray(new Object[params.size()]));

        //delete from query_descriptions
        jdbcTemplate.update("DELETE FROM query_descriptions WHERE query_seq=?", params.toArray(new Object[params.size()]));


    }

    private void doSignalDetection(int queryId) {

        List<Object> params = new ArrayList<Object>();
        params.add(queryId);

        //delete from query_descriptions
        jdbcTemplate.update("DELETE FROM query_descriptions WHERE query_seq=?", params.toArray(new Object[params.size()]));

        //bug fix#12, cascade delete error when rejecting Signal Detection
        //delete from ae term
        jdbcTemplate.update("DELETE FROM ae_terms WHERE query_seq=?", params.toArray(new Object[params.size()]));
        //sqlExec.commitTrans();


    }

    private void doLiterature(int queryId) {

        List<Object> params = new ArrayList<Object>();
        params.add(queryId);
        //delete from query_descriptions
        jdbcTemplate.update("DELETE FROM query_descriptions WHERE query_seq=?", params.toArray(new Object[params.size()]));
        //delete from ae term
        jdbcTemplate.update("DELETE FROM ae_terms WHERE query_seq=?", params.toArray(new Object[params.size()]));
        //sqlExec.commitTrans();


    }


    class QueryDTORowMapper implements RowMapper<PartialQueryDTO> {

        @Override
        public PartialQueryDTO mapRow(ResultSet resultSet, int i) throws SQLException {
            PartialQueryDTO pQueryDTO = new PartialQueryDTO();
            pQueryDTO.setDate(resultSet.getDate("datetime_submitted"));
            pQueryDTO.setEmailAddress(resultSet.getString("email"));
            pQueryDTO.setReporterType(resultSet.getString("rt_reporter_type"));
            pQueryDTO.setQueryType(resultSet.getString("qt_query_type"));
            pQueryDTO.setQueryNumber(resultSet.getInt("query_seq"));
            return pQueryDTO;
        }
    }

        /**
     * create a followup query for the specified queryId
     * @param queryId   the new query type id
     */
    @Override
	public void createFollowup ( int queryId) {
        List<Object> params = new ArrayList<Object>();

            //sqlExec.setAutoCommit(false);
            params.add(new Integer(queryId));

            long followupSeq = jdbcTemplate.queryForLong("SELECT max(followup_seq) FROM fu_queries WHERE query_seq = ? ", params.toArray(new Object[params.size()])) + 1;

            logger.debug("followupSeq " + followupSeq);
            String followupNumber = jdbcTemplate.queryForObject("SELECT followup_number FROM queries WHERE query_seq = ? ", params.toArray(new Object[params.size()]), String.class);
            if(StringUtils.isEmpty(followupNumber)){
                followupNumber = "FU0000";
            }

            params.clear();

            followupQueryService.addFuQuery(queryId, followupNumber, followupSeq);
            followupQueryService.addFuDrug(queryId, followupSeq);
            followupQueryService.addFuRetrievalPeriod(queryId, followupSeq);
            followupQueryService.addFuPregnancy(queryId, followupSeq);
            followupQueryService.addFuClinicalTrial(queryId, followupSeq);
            followupQueryService.addFuCase(queryId, followupSeq);
            followupQueryService.addFuAeTerm(queryId, followupSeq);
            followupQueryService.addFuQueryDescription(queryId, followupSeq);
            followupQueryService.addFuPerformanceMetric(queryId, followupSeq);
            followupQueryService.addFuRequester(queryId, followupSeq);

            ++followupSeq;

            String origQuerySQL = "update queries set followup_seq=?, dmg_due_date = null, followup_number='FU' || lpad(?, 4, '0'), reopen='Y' where query_seq =?";
            params.add(new Long(followupSeq));
            params.add(new Long(followupSeq));
            params.add(new Long(queryId));
             jdbcTemplate.update(origQuerySQL, params.toArray(new Object[params.size()]));

    }


		@Override
		public void updateDataSearch(QueryDescDTO queryDesc,
				DataSearch dataSearch, ClinicalTrial clinicalTrial,
				CaseOformat caseOformat, String other, String meddraTerm,
				String additionalComments, Integer queryNumber) {
			
	        List<Object> params = new ArrayList<Object>();

	        String sql = "SELECT QUERY_SEQ FROM DATA_SEARCH WHERE QUERY_SEQ = ?";
	        params.add(Integer.valueOf(queryNumber));
	        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
	        params.clear();
        	params.add(dataSearch.getRequestType().name());
            params.add(dataSearch.getSearchType().name());
            params.add(dataSearch.getSpecification());
            params.add(dataSearch.getBoReport());
            params.add(dataSearch.getReportParameters());
            params.add(dataSearch.getFrequency() != null ? dataSearch.getFrequency().name() : null);
            params.add(dataSearch.getFrequencyOther());
            params.add(meddraTerm);
            params.add(Integer.valueOf(queryNumber));
	        if (rs.next()) {
	            sql = "UPDATE DATA_SEARCH SET "
	                    + "request_type = ?, "
	                    + "search_type = ?, " 
	                    + "specification = upper(?), "
	                    + "bo_report = upper(?), "
	                    + "report_parameters = upper(?), "
	                    + "frequency = ?, " 
	                    + "frequency_other = upper(?), "
	                    + "meddra_preferred_term = upper(?) "
	                    + "WHERE query_seq = ? ";
	            jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
	        } else {
	            sql = "INSERT INTO DATA_SEARCH (request_type, search_type, specification, "
	                    + "bo_report, report_parameters, frequency, frequency_other, meddra_preferred_term, query_seq) "
	                    + "VALUES (?, ?, upper(?), upper(?), upper(?), ?, upper(?), upper(?), ?)";
	            jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
	        }
            params.clear();
            
            params.add(caseOformat != null && caseOformat.getCaseOformatId() != 0 ? caseOformat.getCaseOformatId() : null);
            params.add(other);
            params.add(additionalComments);
            params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
            params.add(Integer.valueOf(queryNumber));
            sql = "UPDATE QUERIES SET CASE_OFORMAT_ID = ?, OFORMAT_SPECIAL_REQUEST = UPPER(?), CASE_COMMENT = UPPER(?), STATUS_CODE = ? WHERE QUERY_SEQ = ?";
            jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();
            
           	updateStatus(queryNumber, Constants.QUERY_STATUS_NOT_SUBMITTED);

	        // reset the result set
	        rs = null;        

	        updateClinical(clinicalTrial, queryNumber);
			
	        sql = "SELECT QUERY_SEQ FROM QUERY_DESCRIPTIONS WHERE QUERY_SEQ = ?";
	        params.add(Integer.valueOf(queryNumber));
	        rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
	        params.clear();
	        boolean queryDescPresent = rs.next();
	        
	        if (queryDescPresent && StringUtils.isBlank(queryDesc.getNatureOfCase())) {
        		deleteQueryDesc(queryNumber);
	        } else if (!queryDescPresent && StringUtils.isNotBlank(queryDesc.getNatureOfCase())) {
	        	addQueryDesc(queryDesc, queryNumber);
	        } else {
        		updateQueryDesc(queryDesc, queryNumber);
	        }
		}

		@Override
		public void updateMedical(QueryDescDTO queryDescDTO, Case caze,
				Pregnancy pregnancy, List<Drug> drugs, String comments,
				Integer queryNumber) {

			List<Object> params = new ArrayList<Object>();

	        String sql = "SELECT QUERY_SEQ FROM PREGNANCIES WHERE QUERY_SEQ = ?";
	        params.add(queryNumber);
	        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
	        params.clear();
	        boolean pregnancyNotBlank = pregnancy != null && 
					(pregnancy.getPrgyExptype() != null || StringUtils.isNotBlank(pregnancy.getOutcome())
					|| StringUtils.isNotBlank(pregnancy.getTimeInPregnancy())
					|| StringUtils.isNotBlank(pregnancy.getOutcome())
					|| StringUtils.isNotBlank(pregnancy.getComments()));
			if (pregnancyNotBlank) {
				params.add(pregnancy.getComments());
				params.add(pregnancy.getPrgyExptype() != null ? pregnancy.getPrgyExptype().getExposureTypeId() : null);
				params.add(pregnancy.getOutcome());
				params.add(pregnancy.getTimeInPregnancy());
				if (rs.next()) {
					sql = "UPDATE PREGNANCIES SET comments = upper(?), exposure_type_id = ?, outcome = upper(?), "
							+ "time_in_pregnancy = upper(?) WHERE query_seq = ?";
				} else {
					sql = "INSERT INTO PREGNANCIES (comments, exposure_type_id, outcome, time_in_pregnancy, query_seq) " +
							"VALUES (upper(?), ?, upper(?), upper(?), ?)";
				}
			} else {
				sql = "DELETE FROM PREGNANCIES WHERE query_seq = ?";
			}
			params.add(queryNumber);
            jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));					
			params.clear();

	        sql = "SELECT QUERY_SEQ FROM QUERY_DESCRIPTIONS WHERE QUERY_SEQ = ?";
	        params.add(queryNumber);
	        rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
	        params.clear();
	        boolean queryDescPresent = rs.next();
	        
	        if (queryDescPresent && StringUtils.isBlank(queryDescDTO.getNatureOfCase())) {
        		deleteQueryDesc(queryNumber);
	        } else if (!queryDescPresent && StringUtils.isNotBlank(queryDescDTO.getNatureOfCase())) {
	        	addQueryDesc(queryDescDTO, queryNumber);
	        } else {
        		updateQueryDesc(queryDescDTO, queryNumber);
	        }
	        

	        sql = "SELECT QUERY_SEQ FROM CASES WHERE QUERY_SEQ = ?";
	        params.add(queryNumber);
	        rs = jdbcTemplate.queryForRowSet(sql, params.toArray(new Object[params.size()]));
	        params.clear();
	        params.add(caze.getAerNumber());
	        params.add(caze.getLocalReferenceNumber());
            params.add(queryNumber);
	        if (rs.next()) {
	            sql = "UPDATE CASES SET aer_number = upper(?), local_reference_number = upper(?) WHERE query_seq = ?";
	        } else {
	            sql = "INSERT INTO CASES(aer_number, local_reference_number, query_seq, case_seq) "
	                    + "VALUES (upper(?), upper(?), ?, SQ_CASE_CASE_SEQ.nextval)";
	        }
            jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();

            sql = "UPDATE QUERIES SET case_comment = upper(?), status_code = ?, datetime_submitted = ? WHERE query_seq = ?";
            params.add(comments);
            params.add(Constants.QUERY_STATUS_NOT_SUBMITTED);
            params.add(Calendar.getInstance());
            params.add(queryNumber);
            jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
            params.clear();

	        sql = "DELETE FROM DRUGS WHERE query_seq = ? AND first_flag = 'N'";
	        params.add(queryNumber);
	        jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
	        params.clear();

	        sql = "INSERT INTO DRUGS(dose, drug_number_seq, drug_retrieval_name, first_flag, formulation, " +
	        		"inn_generic_name, query_seq, roche_drug_flag, route) " +
	        		"VALUES (upper(?), SQ_DRUG_DRUG_NUMBER_SEQ.nextval, upper(?), 'N', upper(?), upper(?), ?, ?, upper(?))";
	        for (Drug d : drugs) {
	        	params.add(d.getDose());
	        	params.add(d.getDrugRetrievalName());
	        	params.add(d.getFormulation());
	        	params.add(d.getInnGenericName());
	        	params.add(queryNumber);
	        	params.add(String.valueOf(d.getRocheDrugFlag()));
	        	params.add(d.getRoute());
	            jdbcTemplate.update(sql, params.toArray(new Object[params.size()]));
				params.clear();
	        }
		}

    @Override
    public List<String> getPossibleEmailDomains() {
        String sql = "SELECT DOMAIN FROM QRF_POSSIBLE_EMAIL_DOMAINS";
        RowMapper<String> stringRowMapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getString("DOMAIN");
            }
        };
        List<String> resultList = jdbcTemplate.query(sql, stringRowMapper);

        return resultList;
    }

}
