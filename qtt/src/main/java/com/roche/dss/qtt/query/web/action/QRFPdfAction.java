package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.utility.pdf.QrfPdfInterface;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Action(value = "QRFPdfAction", results = {@Result(name = "error", location = "/qtt/global/error.jsp")})
public class QRFPdfAction implements ServletResponseAware {


    private static final Logger logger = LoggerFactory.getLogger(QRFPdfAction.class);

    @Autowired
    private QrfPdfInterface qrfPdf;

    String id;
    String type;

    private HttpServletResponse response;

    public void setId(String id) {
        this.id = id;
    }


    public String execute() {
        try {

            if ((id == null) || (id.length() <= 0)) {
                logger.error("QueryReviewAction: Query id invalid!");
                return "error";
            }
            if ((type == null) || (type.length() <= 0)) {
                logger.error("QueryReviewAction: Query type invalid!");
                return "error";
            }

            String typePhrase = "";

            if (type.equals("amd")) {
                typePhrase = "Provisional";
            } else if (type.equals("rej")) {
                typePhrase = "Rejected";
            }
            ByteArrayOutputStream baos = qrfPdf.qrfBuilder(id, typePhrase);


            // setting some response headers
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            // setting the content type
            response.setContentType("application/pdf");
            // the contentlength is needed for MSIE!!!
            response.setContentLength(baos.size());
            // write ByteArrayOutputStream to the ServletOutputStream
            ServletOutputStream out = response.getOutputStream();
            baos.writeTo(out);
            out.flush();
            response.flushBuffer();
        } catch (IOException e) {
            logger.error("Error occurred while streaming attachment to client: "
                    + e.getMessage(), e);
            return "error";
        }

        return null;
    }

    @Override
    public void setServletResponse(HttpServletResponse httpServletResponse) {
        this.response = httpServletResponse;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
