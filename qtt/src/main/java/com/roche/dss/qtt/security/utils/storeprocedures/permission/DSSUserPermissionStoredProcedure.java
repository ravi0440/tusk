package com.roche.dss.qtt.security.utils.storeprocedures.permission;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;


import com.roche.dss.qtt.security.utils.storeprocedures.DSSStoreProceduresConstants;

/**
 * @author zerkowsm
 *
 */
public class DSSUserPermissionStoredProcedure extends StoredProcedure implements DSSStoreProceduresConstants {
    
    private static final String SQL = "security_service.get_user_permissions";

    public DSSUserPermissionStoredProcedure(DataSource ds) {
        setDataSource(ds);
        setFunction(true);
        setSql(SQL);
        declareParameter(new SqlOutParameter(SECURITY_SERVICE_GET_USER_PERMISSIONS_OUT_PARAM, OracleTypes.CURSOR, new DSSUserPermissionMapper()));
        declareParameter(new SqlParameter("p_emplId", Types.BIGINT));
        declareParameter(new SqlParameter("p_permissionCodeArray", Types.VARCHAR));
        
        compile();
    }

    public Map<String, Object> execute(Long employeeId, String applicationCode) {
    	Map<String, Object> inputs = new HashMap<String, Object>();
    	inputs.put("p_emplId", employeeId);
    	inputs.put("p_permissionCodeArray", applicationCode);
        return super.execute(inputs);
    }
    
}