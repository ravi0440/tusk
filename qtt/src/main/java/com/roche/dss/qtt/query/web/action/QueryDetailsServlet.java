package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.service.QueryService;
import java.io.IOException;

/**
 * Created by farynam on 22/05/17.
 */
public class QueryDetailsServlet extends HttpServlet {

    final String QUERY_SEQ = "QUERY_SEQ";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String querySeq = req.getParameter(QUERY_SEQ);

        if (querySeq == null || querySeq.isEmpty()) {
            return;
        }

        Long querySeqNum = Long.parseLong(querySeq);

        WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        QueryService queryService = (QueryService)context.getBean("queryService");

        String label = queryService.getPreviewQueryDetails(querySeqNum).getQueryLabel();
        String wrap = "<div style='word-wrap: break-word;height: 180px;overflow: auto;'>" + label + "</div>";
        String title = querySeqNum.toString();

        String html = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>" +
                title +
                "</title>\n" +
                "</head>\n" +
                "<body>\n" +
                wrap +
                "</body>\n" +
                "</html>";
        resp.setContentType("text/html;charset=UTF-8");
        resp.getWriter().write(html);
    }
}
