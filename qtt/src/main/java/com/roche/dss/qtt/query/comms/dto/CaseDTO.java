/* 
====================================================================
  $Header$
  @author $Author: savovp $
  @version $Revision: 2506 $ $Date: 2012-09-11 09:17:20 +0200 (Wt, 11 wrz 2012) $

====================================================================
*/
package com.roche.dss.qtt.query.comms.dto;


import com.roche.dss.qtt.model.Case;

public class CaseDTO {
	
	private String aerNumber;
	private String localRefNumber;
	private String extRefNumber;

    /**
     * @return
     */
    public String getExtRefNumber() {
        return extRefNumber;
    }

    /**
     * @return
     */
    public String getLocalRefNumber() {
        return localRefNumber;
    }

    /**
     * @return
     */
    public String getAerNumber() {
        return aerNumber;
    }

    /**
     * @param string
     */
    public void setExtRefNumber(String string) {
        extRefNumber = string;
    }

    /**
     * @param string
     */
    public void setLocalRefNumber(String string) {
        localRefNumber = string;
    }

    /**
     * @param string
     */
    public void setAerNumber(String string) {
        aerNumber = string;
    }

    public Case getCase() {
        Case c = new Case();
        c.setExternalReferenceNumber(getExtRefNumber());
        c.setLocalReferenceNumber(getLocalRefNumber());
        c.setAerNumber(getAerNumber());
        return c;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((extRefNumber == null) ? 0 : extRefNumber.hashCode());
		result = prime * result
				+ ((localRefNumber == null) ? 0 : localRefNumber.hashCode());
		result = prime * result
				+ ((aerNumber == null) ? 0 : aerNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CaseDTO))
			return false;
		CaseDTO other = (CaseDTO) obj;
		if (extRefNumber == null) {
			if (other.extRefNumber != null)
				return false;
		} else if (!extRefNumber.equals(other.extRefNumber))
			return false;
		if (localRefNumber == null) {
			if (other.localRefNumber != null)
				return false;
		} else if (!localRefNumber.equals(other.localRefNumber))
			return false;
		if (aerNumber == null) {
			if (other.aerNumber != null)
				return false;
		} else if (!aerNumber.equals(other.aerNumber))
			return false;
		return true;
	}
}
