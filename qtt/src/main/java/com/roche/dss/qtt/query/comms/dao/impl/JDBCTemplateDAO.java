package com.roche.dss.qtt.query.comms.dao.impl;

import javax.annotation.Resource;

import com.roche.dss.util.InvalidatingJdbcTemplate;

public class JDBCTemplateDAO {

	protected InvalidatingJdbcTemplate jdbcTemplate;

	@Resource(name = "invalidatingJdbcTemplate")
	public void setJdbcTemplate(InvalidatingJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
}