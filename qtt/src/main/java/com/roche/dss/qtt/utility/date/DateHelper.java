/* 
====================================================================
  $Header$
  @author $Author: borowieb $
  @version $Revision: 1241 $ $Date: 2008-07-01 11:50:42 +0200 (Wt, 01 lip 2008) $

====================================================================
*/
package com.roche.dss.qtt.utility.date;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * This class provides functionality for simple date processing to abstract
 * this processing away.
 *
 * @author Manik Surtani (manik.surtani@conchango.com)
 */
public class DateHelper {

    public static final byte DIR_FORWARD_ONLY = 0;
    public static final byte DIR_BACKWARD_ONLY = 1;
    public static final byte DIR_FORWARD_AND_BACKWARD = 2;


    private static class SingletonHolder { 
        public static final DateHelper instance = new DateHelper();
    }
    
    public static DateHelper getInstance() {
    	return SingletonHolder.instance;
    }


    /**
     * Tests if 2 dates are equal.  Only compares day, month and year, ignores times.
     *
     * @param d1
     * @param d2
     * @return
     */
    public boolean compareDate(Date d1, Date d2) {
    	
    	if(d1 == null && d2 == null) {
    		return true;
    	}
    	
    	if(d1 == null || d2 == null) {
    		return false;
    	}
    	
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();

        c1.setTime(d1);
        c2.setTime(d2);

        return (c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH))
                && (c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH))
                && (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR));

    }


    /**
     * Validates a given day, month, and year for days in month and leap
     * years.
     *
     * @param day
     * @param month
     * @param year
     * @return true if the specified date is a valid one; false otherwise.
     */
    public boolean isValidDate(int day, int month, int year)
    {
        boolean isValid = true;

        try
        { 
            isValid = (month >= 1) && (month <= 12);

            isValid = isValid && (year < 2100) && (year > 1900);

            isValid = isValid && (day >= 1) && (day <= maxDays(month, year));
        }
        catch (DateProcessingException dpe)
        {
            // this date is invalid!
            isValid = false;
        }

        return isValid;
    }


     /**
    * Returns the max number of days for a given month and year, taking into
    * account leap years.
    *
    * @param month the month number, 1 for January, 12 for December.
    * @param year the year in question.  Must be 4 digits - E.g., 2002, not
    * 02.  02 will be treated as 2 AD.  No handling for BC dates - yet!
    * @return the max number of days in the month/year combo
    * @throws DateProcessingException if the month is not between 1 and 12
    * inclusive.
    */
    public int maxDays(int month, int year) throws DateProcessingException
    {
        switch (month)
        {
            case 1 :
            case 3 :
            case 5 :
            case 7 :
            case 8 :
            case 10 :
            case 12 :
                return 31;
            case 4 :
            case 6 :
            case 9 :
            case 11 :
                return 30;
            case 2 :
                return new GregorianCalendar().isLeapYear(year) ? 29 : 28;
            default :
                throw new DateProcessingException("Invalid month '" + month + "' specified!");

        }
    }


    class DateProcessingException extends Exception
{

    public DateProcessingException()
    {
        super();
    }

    public DateProcessingException( String s )
    {
        super( s );
    }

}

    /**
    * Checks whether a given date is within 'n', a specified number of days from today.
    * Today is calculated using "new Date()" which defaults to the current system
    * date.<BR><BR>
    * <UL>
    * <LI> If you are searching in a forward direction only, then the
    * date is tested to be 'n' days or less AHEAD of the current date.
    * <LI> If you are searching in a backward direction only, then the date is
    * tested to be 'n' days or less PRIOR to the current date.
    * <LI>If you are searching in both directions, then the date is tested to be
    * 'n' days or less AHEAD of the current date, OR 'n' days or less PRIOR to the
    * current date.
    *</UL><BR>
    * @param requestedDate the Calendar to test
    * @param n the number of days to test for
    * @param direction one of the DIR_ constants in this class.
    */
    public boolean withinDaysOfToday(Calendar requestedDate, int n, byte direction)
    {
        boolean within = true;
        Calendar today = null;

        if ((direction == DIR_FORWARD_AND_BACKWARD) || (direction == DIR_FORWARD_ONLY))
        {
            // lets check forwards
            today = new GregorianCalendar();
            today.add(Calendar.DAY_OF_MONTH, n);
            within = within && !today.before(requestedDate);
        }

        if ((direction == DIR_FORWARD_AND_BACKWARD) || (direction == DIR_BACKWARD_ONLY))
        {
            // lets check backwards
            today = new GregorianCalendar();
            today.add(Calendar.DAY_OF_MONTH, 0 - n);
            within = within && !today.after(requestedDate);
        }

        return within;
    }
}
