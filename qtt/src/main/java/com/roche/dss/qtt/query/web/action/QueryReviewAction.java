package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.service.QueryService;
import java.util.Map;


public class QueryReviewAction extends CommonActionSupport implements SessionAware {
    private static final long serialVersionUID = -3749385595016973400L;
    
    private static final Logger logger = LoggerFactory.getLogger(QueryReviewAction.class);

    private Query query;
    private long queryId;
    @Autowired
    private QueryService queryService;
    private Map<String,Object> session;


    private String viewtype;

    private boolean formQueryDetail = false;
    private boolean differentRequster = false;

    public String execute() {
         // note: no security check as this page can be viewed by any user
        // id check
        if (queryId <= 0) {
            if (session.containsKey(ActionConstants.QRF_QUERY_ID))
            queryId = Long.valueOf((Integer) session.get(ActionConstants.QRF_QUERY_ID));
            else {
            		logger.error("QueryReviewAction: Query id invalid!");
            		return ERROR;
            }
        }

        query = queryService.getPreviewQueryDetails(queryId);
        if (query == null) {
			return ALREADY;
		}
       session.put(ActionConstants.INITIATE_QUERY_SESSION_NAME, query);

        previewMode = checkPreviewMode(query);
        // previewMode = ActionConstants.PREVIEW_MODE_DUPLCHK_ACC_REJECT_AMNDTYP;
        logger.debug("previewMode " + getPreviewMode());

        if ("duplicate".equals(viewtype)) {
            return "duplicate_review";
        } else if(NO_LAYOUT.equals(viewtype)){
            return NO_LAYOUT;
        }

        return SUCCESS;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }



    public Query getQuery() {
        return query;
    }


    public String getViewtype() {
        return viewtype;
    }

    public void setViewtype(String viewtype) {
        this.viewtype = viewtype;
    }

    public boolean isFormQueryDetail() {
        return formQueryDetail;
    }

    public void setFormQueryDetail(boolean formQueryDetail) {
        this.formQueryDetail = formQueryDetail;
    }

    /**
     * This method check which previewMode should be used in initiate_preview.jsp
     * It's taken from execute method in InitiatePreviewAction from former version
     *
     * @param query
     */
    private String checkPreviewMode(Query query) {
        //instead of previous duplicate_query_review.jsp
        if ("nolayout".equals(viewtype)) {
            return ActionConstants.PREVIEW_MODE_PRNT_CLOSE;
        }
        if ("duplicate".equals(viewtype) || query.getQueryStatusCode().isStatusClosed()) {
            return ActionConstants.PREVIEW_MODE_PRNT_CLOSE;
        }
        //instead of of previous query_review.jsp
        if ("initiatePreview".equals(viewtype)) {
            return ActionConstants.PREVIEW_MODE_DUPLCHK_ACC_REJECT_AMNDTYP;
        }
        String previewMode;
        if (isDscl()) {
             if (!getLoggedUser().getMail().toLowerCase().equals(query.getRequester().getEmail().toLowerCase())) {
                differentRequster = true;
            }
            //if the final doc exists then only print
            if (queryService.isFinalDocExists(query.getQuerySeq(),query.getProcessInstanceId())) {
                previewMode = ActionConstants.PREVIEW_MODE_PRNT;
            } //else if from query list then show amend, del, and print
            else if (query.getQuerySeq() > 0) {
                if (query.getQueryStatusCode().isStatusBeingProcessed()) {
                    if (formQueryDetail) {
                        previewMode = ActionConstants.PREVIEW_MODE_ASSIGNED_AMND_DEL_PRNT;
                    } else {
                        previewMode = ActionConstants.PREVIEW_MODE_PRNT;
                    }
                } else {
                    previewMode = ActionConstants.PREVIEW_MODE_AMND_DEL_PRNT;
                }
            } else {
                previewMode = ActionConstants.PREVIEW_MODE_ALL;
            }
        } else if (query.getQueryStatusCode().isStatusBeingProcessed()) {//if in process then only print
            previewMode = ActionConstants.PREVIEW_MODE_PRNT;
        } else if (queryId > 0) {//if from query list then amend, del, and print
            previewMode = ActionConstants.PREVIEW_MODE_AMND_DEL_PRNT;
        } else {//show all
            previewMode = ActionConstants.PREVIEW_MODE_ALL;
        }
        return previewMode;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
}