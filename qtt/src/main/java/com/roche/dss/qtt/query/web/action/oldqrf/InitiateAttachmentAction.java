/* 
====================================================================
  $Header$
  @author $Author: tomasinp $
  @version $Revision: 2250 $ $Date: 2011-04-29 15:18:38 +0200 (Pt, 29 kwi 2011) $

====================================================================
*/
package com.roche.dss.qtt.query.web.action.oldqrf;

import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.comms.dto.AttachmentDTO;
import com.roche.dss.qtt.query.comms.dto.QueryDTO;
import com.roche.dss.qtt.service.AttachmentService;
import com.roche.dss.qtt.service.FileOperationsService;
import com.roche.dss.qtt.service.QrfService;
import com.roche.dss.qtt.service.QueryService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class InitiateAttachmentAction implements ServletResponseAware {
	
    
    private static final Logger logger = LoggerFactory.getLogger(InitiateAttachmentAction.class);

    @Autowired
	private FileOperationsService fileOpsService;

    @Autowired
	private QrfService qrfService;

    @Autowired
    private AttachmentService attachmentService;

    String id;
    String index;
    String key;
    private HttpServletResponse response;


    String tempfile;
    String fileName;

    private long attachId;
    @Autowired
    private QueryService queryService;


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    public String getTempfile() {
        return tempfile;
    }

    public void setTempfile(String tempfile) {
        this.tempfile = tempfile;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIndex(String index) {
        this.index = index;
    }
    

    public String execute()
    {
		
		//id check	  							  
		if ( (id == null) || (id.length()<=0) ) {
			logger.error( "QueryReviewAction: Query id invalid!" );
			return "failure";
		}
		 	
		//fetch query detsils for this query id
		//QrfFacadeBD qrf = (QrfFacadeBD)BusinessDelegateFactory.getFactory().getServiceBD(mapping);
		QueryDTO queryDto = qrfService.openQuery(Integer.parseInt(id),null, true);
		
		//retrieve index parameter from request
		int idx = 0;
		try  {
			 idx = Integer.parseInt( index );
		}
		catch ( NumberFormatException nfe ) {
			logger.error( "Error parsing index parameter: " + nfe.getMessage() );
			return  "error";
		}
		
		//retrieve query dto from the session
		List attachmentList = queryDto.getAttachments();
		if ( attachmentList == null )	 {
			 logger.error( "Query details could not be located from the session!" );
			 return  "error";
		}
		
		int internalQueryId = Integer.parseInt(id);
		
		// retrieve appropriate attachment DTO from query details 
		AttachmentDTO attachment = null;
		try {
			attachment = (AttachmentDTO)attachmentList.get( idx );
		}
		catch ( IndexOutOfBoundsException e ) {
			logger.error( "Specified index is out of bounds!", e );
			return  "error";
		}
		catch ( NullPointerException npe ) {
			logger.error( "Attachments list is null", npe );
			return  "error";
		}
        
		logger.debug("About to stream following attachment: " + attachment);
		
		InputStream inputStream = null;
		try 
		{

		        	
			// get handle on input stream to attachment
			inputStream = fileOpsService.getInputStream( internalQueryId, attachment.getFileName() );

            File file = new File(attachment.getFileName());
            String fileName = file.getName();
					
			String mimeType = fileOpsService.getMimeType( fileName );
			response.setContentType( mimeType );
						
			response.setHeader( "Content-Disposition", "filename=\"" 
							+ fileName
							+ "\";" );
					
			// stream output to servlet response
			fileOpsService.streamAttachment(inputStream, response.getOutputStream());
										   
			response.flushBuffer();
		}
		catch ( Exception e ) {
			logger.error( "Error occurred while streaming attachment to client: " 
							   + e.getMessage(), e );
			return  "error";
		}
		finally {
			if ( inputStream != null ) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                   logger.error( "can not close stream", e);
                }
            }
		}
        
		// should never reach this far ..?
		return null;										 	
	}

    public String showFile() {

        //id check
        if ((tempfile == null) || (tempfile.length() <= 0)) {
            logger.error("QueryReviewAction: file invalid!");
            return "failure";
        }
        InputStream inputStream = null;
        try {
            // get handle on input stream to attachment

            inputStream = new FileInputStream(fileOpsService.findDestinationDirectory("temp_folder")+tempfile);
            String mimeType = fileOpsService.getMimeType(fileName);
            response.setContentType(mimeType);
            response.setHeader("Content-Disposition", "filename=\"" + fileName + "\";");
            // stream output to servlet response
            fileOpsService.streamAttachment(inputStream, response.getOutputStream());
            response.flushBuffer();
        } catch (Exception e) {
            logger.error("Error occurred while streaming attachment to client: " + e.getMessage(), e);
            return "error";
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    logger.error("can not close stream", e);
                }
            }
        }
        return null;
    }


    public String showResponseFile() {
        if ((id == null) || (id.length() <= 0)) {
            logger.error("QueryReviewAction: Query id invalid!");
            return "failure";
        }
        Query query = queryService.find(Long.valueOf(id));
        if (query==null||query.getAccessKey()==null||!query.getAccessKey().equals(key.trim())) {
            return "noaccess";
        }
        InputStream inputStream = null;
        try {
            Attachment attachment = attachmentService.getFileFromRepo(attachId, Long.valueOf(id));
            inputStream = attachment.getFileStream();
            File file = new File(attachment.getFilename());
            String fileName = file.getName();
            String mimeType = fileOpsService.getMimeType(fileName);
            response.setContentType(mimeType);
            response.setHeader("Content-Disposition", "filename=\"" + fileName + "\";");
            fileOpsService.streamAttachment(inputStream, response.getOutputStream());
            response.flushBuffer();
        } catch (Exception e) {
            logger.error("Error occurred while streaming attachment to client: " + e.getMessage(), e);
            return "error";
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    logger.error("can not close stream", e);
                }
            }
        }
        return null;
    }


    public long getAttachId() {
        return attachId;
    }

    public void setAttachId(long attachId) {
        this.attachId = attachId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public void setServletResponse(HttpServletResponse httpServletResponse) {
        this.response = httpServletResponse;
    }
}
