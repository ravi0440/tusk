package com.roche.dss.qtt.query.web.model;

import java.io.Serializable;
import java.util.Date;

/**
 * This class holds search criteria for report page
 * User: pruchnil
 */
@Conversion()
public class ReportCriteria implements Serializable {

	private static final long serialVersionUID = -1667298536007492494L;
	
	private String route;
    private Date dateFrom;
    private Date dateTo;

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    @TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

}
