package com.roche.dss.qtt.service.workflow.exception;

/**
 * @author zerkowsm
 *
 */
public class TaskAlreadyBegunException extends JbpmException {
	
	private static final long serialVersionUID = -6816518897382170308L;

	public TaskAlreadyBegunException() {
		super();
	}

	public TaskAlreadyBegunException(String message, Throwable cause) {
		super(message, cause);
	}

	public TaskAlreadyBegunException(String message) {
		super(message);
	}

	public TaskAlreadyBegunException(Throwable cause) {
		super(cause);
	}
	
}