package com.roche.dss.qtt.service.workflow.exception;

/**
 * @author zerkowsm
 *
 */
public class TaskAlreadyCompletedException extends JbpmException {

	private static final long serialVersionUID = -5612212564623958785L;

	public TaskAlreadyCompletedException() {
		super();
	}

	public TaskAlreadyCompletedException(String message, Throwable cause) {
		super(message, cause);
	}

	public TaskAlreadyCompletedException(String message) {
		super(message);
	}

	public TaskAlreadyCompletedException(Throwable cause) {
		super(cause);
	}
	
}