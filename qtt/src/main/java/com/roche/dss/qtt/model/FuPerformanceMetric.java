package com.roche.dss.qtt.model;
// Generated 2010-10-04 12:00:38 by Hibernate Tools 3.3.0.GA

/**
 * FuPerformanceMetric generated by hbm2java
 */
@Entity
@Table(name = "FU_PERFORMANCE_METRICS")
public class FuPerformanceMetric implements java.io.Serializable {

	private static final long serialVersionUID = 2784857061283545871L;
	
	private FuPerformanceMetricId id;
	private String requestDescription;
	private CtrlOformat outputFormat;
	private String specialRequests;

	public FuPerformanceMetric() {
	}

	public FuPerformanceMetric(FuPerformanceMetricId id,
			String requestDescription) {
		this.id = id;
		this.requestDescription = requestDescription;
	}

    public FuPerformanceMetric(PerformanceMetric performanceMetric, long followupSeq) {
        this.id = new FuPerformanceMetricId(followupSeq, performanceMetric.getQuerySeq());
		this.requestDescription = performanceMetric.getRequestDescription();
		this.outputFormat = performanceMetric.getOutputFormat();
		this.specialRequests = performanceMetric.getSpecialRequests();
    }

    @EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "followupSeq", column = @Column(name = "FOLLOWUP_SEQ", nullable = false, precision = 10, scale = 0)),
			@AttributeOverride(name = "querySeq", column = @Column(name = "QUERY_SEQ", nullable = false, precision = 10, scale = 0)) })
	public FuPerformanceMetricId getId() {
		return this.id;
	}

	public void setId(FuPerformanceMetricId id) {
		this.id = id;
	}

	@Column(name = "REQUEST_DESCRIPTION", nullable = false, length = 1000)
	public String getRequestDescription() {
		return this.requestDescription;
	}

	public void setRequestDescription(String requestDescription) {
		this.requestDescription = requestDescription;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CTRL_OFORMAT_ID")
	public CtrlOformat getOutputFormat() {
		return outputFormat;
	}

	public void setOutputFormat(CtrlOformat outputFormat) {
		this.outputFormat = outputFormat;
	}

	@Column(name = "SPECIAL_REQUESTS", length = 300)
	public String getSpecialRequests() {
		return specialRequests;
	}

	public void setSpecialRequests(String specialRequests) {
		this.specialRequests = specialRequests;
	}

}
