package com.roche.dss.qtt.service;

import java.util.List;

import com.roche.dss.qtt.model.Query;

public class SearchResult {
	protected Query query;
	
	protected Document doc;
	
	protected List<AttachmentWithSearchInfo> attsWithInfo;
	
	public SearchResult(Object result) {
		if (result != null) {
			Object[] resItems = (Object[]) result;
			if (resItems.length > 0) {
				query = (Query) resItems[0];
			}
			if (resItems.length > 1) {
				doc = (Document) resItems[1];
			}
		}
	}
	
	public SearchResult(Query query) {
		this.query = query;
	}
	
	public Query getQuery() {
		return query;
	}
	
	public Document getDoc() {
		return doc;
	}

	public List<AttachmentWithSearchInfo> getAttsWithInfo() {
		return attsWithInfo;
	}

	public void setAttsWithInfo(List<AttachmentWithSearchInfo> attsWithInfo) {
		this.attsWithInfo = attsWithInfo;
	}
	
	public boolean getAttsMatchFound() {
		if (attsWithInfo != null && attsWithInfo.size() > 0) {
			return true;
		}
		return false;
	}
	
	public int getAttMatches() {
		if (attsWithInfo != null) {
			return attsWithInfo.size();
		} else {
			return 0;
		}
	}
}
