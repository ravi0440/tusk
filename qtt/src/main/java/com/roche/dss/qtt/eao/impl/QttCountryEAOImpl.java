package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.QttCountryEAO;
import com.roche.dss.qtt.model.QttCountry;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
@Repository("qttCountry")
public class QttCountryEAOImpl  extends AbstractBaseEAO<QttCountry> implements QttCountryEAO {

    @Override
    public List<QttCountry> getOrderedCountries() {
        return getEntityManager().createNamedQuery("list.Countries", QttCountry.class).getResultList();
    }
}