package com.roche.dss.qtt.model;

@NamedQueries({
        @NamedQuery(name = "attachment.findAttachmentsByTaskInstance", query = "select a from AttachmentFromDmgSearch a where a.taskInstanceId =:taskInstanceId")
})
@Entity
@Table(name = "ATTACHMENTS_FROM_DMG_SEARCH")
public class AttachmentFromDmgSearch implements java.io.Serializable {
    private long id;
    private long attachmentSeq;
    private long querySeq;
    private long taskInstanceId;

    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    @SequenceGenerator(name = "ATTACHMENTS_FROM_DMG_SEARCH_GENERATOR", sequenceName = "ATT_FROM_DMG_SEARCH_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ATTACHMENTS_FROM_DMG_SEARCH_GENERATOR")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "ATTACHMENT_SEQ", nullable = false, precision = 10, scale = 0)
    public long getAttachmentSeq() {
        return attachmentSeq;
    }

    public void setAttachmentSeq(long attachmentSeq) {
        this.attachmentSeq = attachmentSeq;
    }

    @Column(name = "QUERY_SEQ", nullable = false, precision = 10, scale = 0)
    public long getQuerySeq() {
        return querySeq;
    }

    public void setQuerySeq(long querySeq) {
        this.querySeq = querySeq;
    }

    @Column(name = "TASK_INSTANCE_ID", nullable = false, precision = 10, scale = 0)
    public long getTaskInstanceId() {
        return taskInstanceId;
    }

    public void setTaskInstanceId(long taskInstanceId) {
        this.taskInstanceId = taskInstanceId;
    }
}
