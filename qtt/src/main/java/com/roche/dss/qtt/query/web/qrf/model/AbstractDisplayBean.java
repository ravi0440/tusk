/* 
====================================================================
  $Header$
  @author $Author: rahmanh1 $
  @version $Revision: 975 $ $Date: 2004-11-16 17:20:57 +0100 (Wt, 16 lis 2004) $

====================================================================
*/
package com.roche.dss.qtt.query.web.qrf.model;

import java.io.Serializable;
import java.text.BreakIterator;

/**
 * Superclass for all Display-Beans - used as data containers to transfer (and simplify
 * processing of) information from Struts Action to JSP.  Follow standard Javabean 
 * conventions.
 * @author wisbeya
 */
public class AbstractDisplayBean implements Serializable, Cloneable
{
	
	private final static int MAX_LINE_LENGTH = 50;
	
	private final static String LINE_FEED = "\n";
	
	
    @Override
	public Object clone()
    {
        try
        {
            Object other = super.clone();
            return other;

        }
        catch (CloneNotSupportedException e)
        {
            // object not cloneable?!??
            return null;
        }
    }
    
    protected String formatStringAsHtml( String s )
    {
    	if ( s == null )  return s;
    	else return lineBreaker( s );
    }
    
	protected static String lineBreaker( String source )
	{
		final String crlf = "<br/>" + LINE_FEED; 
		StringBuffer retVal = new StringBuffer();
		
		// catch case of null data (return empty string)
		if ( source == null )  return "";
		
		// catch case of short data (don't need to do anything)
		if ( source.length() < MAX_LINE_LENGTH )  return source;
		
		BreakIterator wb = BreakIterator.getWordInstance();
		wb.setText( source );
		
		boolean finished = false;
		int position = 0,
			nextBreak = 0,
			nextPos = 0,
			nextEOL = 0;
		
		while ( !finished )
		{
			// update the pointers
			position = nextPos;
			nextBreak = position + MAX_LINE_LENGTH;
			nextEOL = source.indexOf( '\n', position + 1 );
			
			if ( ! ( nextBreak < source.length() ) )
			{	 
				nextPos = source.length();					// use remainder of string.
				finished = true;
			}
			else if ( ( nextEOL > -1 ) && ( nextEOL - position <= MAX_LINE_LENGTH ) )
			{
				nextPos = nextEOL;							// use up to next physical end-of-line.
			}
			else  nextPos = wb.preceding( nextBreak );		// use up to word break just before logical end-of-line.
			
			if ( nextPos <= position || ( nextPos == 0 ) )
			{
				// append a fixed-size chunk, by re-setting nextPos (!)
				nextPos = position + MAX_LINE_LENGTH;
				if ( ! ( nextPos < source.length() ) )
				{
					nextPos = source.length();
					finished = true;
				} 
				
				// chunk = max-length, insert crlf and hyphenate
				retVal.append( source.substring( position, nextPos ) + "-" + crlf );
			}
			else
			{
				// chunk = as recommended
				retVal.append( source.substring( position, nextPos ) + crlf );
			}
		}

		return retVal.toString();
	}
}
