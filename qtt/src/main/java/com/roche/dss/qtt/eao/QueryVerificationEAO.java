package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.QueryVerification;

public interface QueryVerificationEAO extends BaseEAO<QueryVerification> {
	
	 public QueryVerification findByRecordTypeAndQueryId(QueryVerification.RECORD_TYPE type, long queryId,String followupNumber);
}
