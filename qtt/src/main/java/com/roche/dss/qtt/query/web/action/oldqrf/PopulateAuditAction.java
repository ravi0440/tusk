/* 
 *====================================================================
 *  $Header$
 *  @author $Author: savovp $
 *  @version $Revision: 2592 $ $Date: 2012-12-05 19:12:45 +0100 (Śr, 05 gru 2012) $
 *
 *====================================================================
 */
package com.roche.dss.qtt.query.web.action.oldqrf;

import java.util.List;

import com.roche.dss.qtt.model.Audit;
import com.roche.dss.qtt.model.ClinicalTrial;
import com.roche.dss.qtt.model.CtrlOformat;
import com.roche.dss.qtt.query.comms.dto.ClinicalTrialDTO;
import com.roche.dss.qtt.query.comms.dto.QueryDTO;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.model.QueryDescFormModel;
import com.roche.dss.qtt.service.QrfService;

import javax.annotation.Resource;

public class PopulateAuditAction extends QRFAbstractAction {

    private QrfService qrfService;

    private QueryDescFormModel queryForm = new QueryDescFormModel();
    
    private Audit audit;

    private ClinicalTrial clinical;
	
    private String specialRequest;
    
    public Audit getAudit() {
		return audit;
	}

	public void setAudit(Audit audit) {
		this.audit = audit;
	}

	public ClinicalTrial getClinical() {
		return clinical;
	}

	public void setClinical(ClinicalTrial clinical) {
		this.clinical = clinical;
	}

	public String getSpecialRequest() {
		return specialRequest;
	}

	public void setSpecialRequest(String specialRequest) {
		this.specialRequest = specialRequest;
	}

	@Resource
    public void setQrfService(QrfService qrfService) {
        this.qrfService = qrfService;
    }

    @Override
    public Object getModel() {
        if (getSession().get(ActionConstants.QRF_EDIT_QUERY_TYPE) != null) {
            int queryId = ((Integer) getSession().get(ActionConstants.QRF_QUERY_ID)).intValue();
            QueryDTO query = qrfService.openQuery(queryId, null, true);
            queryForm.setNatureofcase(query.getQueryDesc().getNatureOfCase());
            queryForm.setOtherinfo(query.getQueryDesc().getOtherInfo());
            audit = query.getAudit();
            clinical = toClinicalTrial(query.getClinical());
            specialRequest = query.getSpecialRequest();
            getSession().put(ActionConstants.FRM_QUERY_DESC, queryForm);
        }
        return queryForm;
    }

    private ClinicalTrial toClinicalTrial(ClinicalTrialDTO clinicalDto) {
    	//TODO Move elsewhere
    	ClinicalTrial trial = new ClinicalTrial();
    	trial.setCtrlOformat(new CtrlOformat());
    	trial.getCtrlOformat().setCtrlOformatId(clinicalDto.getOutputFormatId());
		return trial;
	}

	public List<CtrlOformat> getClotypes() {
        return qrfService.getCtrlOformats();
    }

    public String execute() {
        return SUCCESS;
    }

}
