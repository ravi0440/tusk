/* 
====================================================================
  $Header$
  @author $Author: tomasinp $
  @version $Revision: 1959 $ $Date: 2011-03-30 21:22:15 +0200 (Śr, 30 mar 2011) $

====================================================================
*/
package com.roche.dss.qtt.query.web.action.oldqrf;

import com.roche.dss.qtt.query.comms.dto.PartialQueryDTO;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.QRFLists;
import com.roche.dss.qtt.security.user.RocheUserDetails;
import com.roche.dss.qtt.service.QrfService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

public class FindPartialAction extends ActionSupport {


    Map<String, Object> session;
    private RocheUserDetails userDetails;
    public QRFLists qrflists = new QRFLists();
    private QrfService qrfService;
    List<PartialQueryDTO> partialQueries;
    private Map<String, String[]> parameterMap;
    private String email;

    @Resource
    public void setQrfService(QrfService qrfService) {
        this.qrfService = qrfService;
    }

    public String execute() throws Exception {

        if ((email != null) && (email.length() > 0)) {
            partialQueries = qrfService.getPartialQueries(email, ActionConstants.QRF_STATUS_NOT_SUBMITTED);
        }
        return Action.SUCCESS;
    }

    public List getPartialQueriesList() {
        return partialQueries;
    }

  @Override
    public void validate() {
       if ((email == null) || (email.length() == 0)) {
          addActionError(getText("partialquery.emailaddress.displayname"));
        }
    }
    public void setEmail(String mail) {
        this.email = mail;
    }
}
