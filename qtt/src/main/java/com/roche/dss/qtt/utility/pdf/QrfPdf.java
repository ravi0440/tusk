package com.roche.dss.qtt.utility.pdf;

import com.roche.dss.qtt.model.Case;
import com.roche.dss.qtt.model.Drug;
import com.roche.dss.qtt.model.Manufacturing;
import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.model.DataSearch.Frequency;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.Constants;
import com.roche.dss.qtt.query.web.qrf.model.QueryReviewDisplayModel;

import java.io.ByteArrayOutputStream;
import java.util.Iterator;


@Component
public class QrfPdf implements QrfPdfInterface {

	private static final Logger logger = LoggerFactory.getLogger(QrfPdf.class);
	
    @Autowired
    private QueryDetailsInterface queryDetails;

    @Override
	public ByteArrayOutputStream qrfBuilder(int queryId, String prefix) {

        String queryNoPhrase = prefix + " Query No. " + queryId;

        return pdfBuilder(queryId, queryNoPhrase);
    }


    @Override
	public ByteArrayOutputStream qrfBuilder(String queryNo, String prefix) {

        int queryId = Integer.valueOf(queryNo).intValue();
        String queryNoPhrase = null;

        if (prefix == null) {
            queryNoPhrase = "Query No. " + queryNo;
        } else {
            queryNoPhrase = prefix + " Query No. " + queryId;
        }

        return pdfBuilder(queryId, queryNoPhrase);
    }
    
    public static String removeHTML(String param){
    	if(param == null) {
    		return null;
    	}else{
    		return Jsoup.parse(param.replaceAll("\\<.*?>","")).text();
    	}
    	
    }

    /* (non-Javadoc)
	 * @see com.roche.dss.qtt.utility.pdf.QrfPdfInterface#pdfBuilder(int, java.lang.String)
	 */
    @Override
	public ByteArrayOutputStream pdfBuilder(int queryId, String queryNoPhrase) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            //OutputStream out = new FileOutputStream("TestPDFTool.pdf");

            //returen number by skipping QTT prefix

            QueryReviewDisplayModel queryBean = queryDetails.buildQueryDetails(queryId);

            QTTpdfHelper qrf = new QTTpdfHelper(out);
            qrf.setFooter(queryNoPhrase);
            qrf.open();
            qrf.createChapter("Query Tracking Tool - Query Review");
            qrf.createHeader(queryNoPhrase);

            //Requester Details section
            qrf.createSectionToCurrentChapter("Requester Details");
            qrf.addTableToCurrentSection();
            qrf.addCurrentSectionRow("First Name:", removeHTML(queryBean.getFirstname()));
            qrf.addCurrentSectionRow("Last Name:", removeHTML(queryBean.getLastname()));
            qrf.addCurrentSectionRow("Tel No:", removeHTML(queryBean.getTelephone()));
            qrf.addCurrentSectionRow("Email Address:", queryBean.getEmail());
            qrf.addCurrentSectionRow("Country:", removeHTML(queryBean.getReqCountry()));
            qrf.addCurrentSectionRow("Organisation Name:", removeHTML(queryBean
                    .getOrgName()));
            qrf.addCurrentSectionRow("Organisation Type:", removeHTML(queryBean
                    .getOrgType()));

            //Query Origin section
            qrf.createSectionToCurrentChapter("Query Origin");
            qrf.addTableToCurrentSection();
            qrf.addCurrentSectionRow("Source Country of Request:", removeHTML(queryBean
                    .getSourceCountry()));
            qrf.addCurrentSectionRow("Reporter Type:", removeHTML(queryBean
                    .getReporterType()));

            //Drug Details section
            qrf.createSectionToCurrentChapter("Drug Details");
            qrf.addTableToCurrentSection();
            qrf.addCurrentSectionRow("All Drugs:", removeHTML(queryBean.getAllDrugs()));
            if (queryBean.getAllDrugs().equals(ActionConstants.NO)) {
                qrf.addCurrentSectionRow("Drug Retrieval Name:", removeHTML(queryBean
                        .getRetrDrug()));
                qrf.addCurrentSectionRow("Generic Drug Name:", removeHTML(queryBean
                        .getGenericDrug()));
                qrf.addCurrentSectionRow("Route:", removeHTML(queryBean.getRoute()));
                qrf.addCurrentSectionRow("Formulation:", removeHTML(queryBean
                        .getFormulation()));
                qrf.addCurrentSectionRow("Indication:",removeHTML( queryBean
                        .getIndication()));
                qrf.addCurrentSectionRow("Dose:", removeHTML(queryBean.getDose()));
                qrf.addCurrentSectionRow("Other Roche Drugs:", removeHTML(queryBean
                        .getOthersDrug()));
            }

            //Response Timelines section
            qrf.createSectionToCurrentChapter("Response Timelines");
            qrf.addTableToCurrentSection();
            qrf.addCurrentSectionRow("Original Due Date:", removeHTML(queryBean
                    .getDateRequested()));
            qrf.addCurrentSectionRow("Urgency:", removeHTML(queryBean.getUrgency()));

            //Query Details section
            qrf.createSectionToCurrentChapter("Query Details");
            qrf.addTableToCurrentSection();
            qrf.addCurrentSectionRow("Query Type:", removeHTML(queryBean.getQueryType()));
            qrf.addCurrentSectionRow("Followup Number:", removeHTML(queryBean
                    .getFollowUp()));

            //Cumulative Details section
            qrf.createSectionToCurrentChapter("Cumulative Details");
            qrf.addTableToCurrentSection();
            qrf.addCurrentSectionRow("Cumulative Period:",removeHTML( queryBean
                    .getCumulative()));
            if (ActionConstants.NO.equals(queryBean.getCumulative())) {
                qrf.addCurrentSectionRow("Date from:", removeHTML(queryBean.getDateFrom()));
                qrf.addCurrentSectionRow("Date to:", removeHTML(queryBean.getDateTo()));
                qrf.addCurrentSectionRow("Interval:", removeHTML(queryBean.getInterval()));
            }

            //------------------Query type details

            //start- DRUG_EVENT type query
            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.DRUG_EVENT) {

                //Query Description section
                qrf.createSectionToCurrentChapter("Query Description");
                qrf.addTableToCurrentSection();
                qrf.addCurrentSectionRow("Your question:", removeHTML(queryBean
                        .getNatureOfCase()));
                qrf.addCurrentSectionRow("Index Case:", removeHTML(queryBean
                        .getIndexCase()));
                qrf.addCurrentSectionRow("Signs & Symp:", removeHTML(queryBean
                        .getSignsSymp()));
                qrf.addCurrentSectionRow("Investigations:",removeHTML( queryBean
                        .getInvestigations()));
                qrf.addCurrentSectionRow("Past Medical History:", removeHTML(queryBean
                        .getPmh()));
                qrf.addCurrentSectionRow("Comedications:", removeHTML(queryBean
                        .getConMeds()));
                qrf.addCurrentSectionRow("Other pertinent info:", removeHTML(queryBean
                        .getOtherInfo()));
                qrf.addCurrentSectionRow("Diagnosis/Confounders:", removeHTML(queryBean
                        .getDiagnosis()));
                qrf.addCurrentSectionRow("Adverse Event(s):", removeHTML(queryBean
                        .getAeTerm()));

                //Case Details section
                qrf.createSectionToCurrentChapter("Case Details");
                qrf.addTableToCurrentSection();
                qrf.addCurrentNestedTable("AER", "Local Ref Number",
                        "External Ref Number");
                addCaseData(queryBean, qrf);
                qrf.addCurrentSectionRow("Additional comments:", removeHTML(queryBean
                        .getAdditionalComments()));
            }
            //end- DRUG_EVENT type query

            //start- DRUG_INTERACTION type query
            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.DRUG_INTERACTION) {

                //Interacting Drug(s) section
                qrf.createSectionToCurrentChapter("Interacting Drug(s)");
                qrf.addTableToCurrentSection();
                if (queryBean.getDrugs() != null) {
                    int drugIterIndex = 0;
                    for (Drug drug : queryBean.getDrugs()) {
                        qrf.addCurrentSectionRow("Additional Drug Details - "
                                + ++drugIterIndex + ":", "");
                        qrf.addCurrentSectionRow("Drug Type:",removeHTML( Character.toString(drug.getRocheDrugFlag())));
                        qrf.addCurrentSectionRow("Generic name:", removeHTML(drug.getInnGenericName()));
                        qrf.addCurrentSectionRow("Retrieval name:", removeHTML(drug.getDrugRetrievalName()));
                        qrf.addCurrentSectionRow("Route:", removeHTML(drug.getRoute()));
                        qrf.addCurrentSectionRow("Dose:", removeHTML(drug.getDose()));
                        qrf.addCurrentSectionRow("Formulation:", removeHTML(drug.getFormulation()));
                    }
                }//if

                //Query Description section
                qrf.createSectionToCurrentChapter("Query Description");
                qrf.addTableToCurrentSection();
                qrf.addCurrentSectionRow("Your question:", removeHTML(queryBean
                        .getNatureOfCase()));
                qrf.addCurrentSectionRow("Index Case:", removeHTML(queryBean
                        .getIndexCase()));
                qrf.addCurrentSectionRow("Signs & Symp:", removeHTML(queryBean
                        .getSignsSymp()));
                qrf.addCurrentSectionRow("Investigations:",removeHTML( queryBean
                        .getInvestigations()));
                qrf.addCurrentSectionRow("Past Medical History:", removeHTML(queryBean
                        .getPmh()));
                qrf.addCurrentSectionRow("Comedications:", removeHTML(queryBean
                        .getConMeds()));
                qrf.addCurrentSectionRow("Other pertinent info:",removeHTML( queryBean
                        .getOtherInfo()));
                qrf.addCurrentSectionRow("Diagnosis/Confounders:",removeHTML( queryBean
                        .getDiagnosis()));

                //Case Details section
                qrf.createSectionToCurrentChapter("Case Details");
                qrf.addTableToCurrentSection();
                qrf.addCurrentNestedTable("AER", "Local Ref Number",
                        "External Ref Number");
                addCaseData(queryBean, qrf);
                qrf.addCurrentSectionRow("Additional comments:",removeHTML( queryBean
                        .getAdditionalComments()));
            }
            //end- DRUG_INTERACTION type query

            //start- MANUFACTURING type query
            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.MANUFACTURING) {

                //Query Description section
                qrf.createSectionToCurrentChapter("Query Description");
                qrf.addTableToCurrentSection();
                qrf.addCurrentSectionRow("Your question:", removeHTML(queryBean
                        .getNatureOfCase()));
                qrf.addCurrentSectionRow("Index Case:", removeHTML(queryBean
                        .getIndexCase()));
                qrf.addCurrentSectionRow("Signs & Symp:", removeHTML(queryBean
                        .getSignsSymp()));
                qrf.addCurrentSectionRow("Date to:",removeHTML( queryBean.getDateTo()));
                qrf.addCurrentSectionRow("Interval:", removeHTML(queryBean.getInterval()));
                qrf.addCurrentSectionRow("Investigations:",removeHTML( queryBean
                        .getInvestigations()));
                qrf.addCurrentSectionRow("Past Medical History:",removeHTML( queryBean
                        .getPmh()));
                qrf.addCurrentSectionRow("Comedications:",removeHTML(queryBean
                        .getConMeds()));
                qrf.addCurrentSectionRow("Other pertinent info:", removeHTML(queryBean
                        .getOtherInfo()));
                qrf.addCurrentSectionRow("Diagnosis/Confounders:", removeHTML(queryBean
                        .getDiagnosis()));
                qrf.addCurrentSectionRow("Batch No:",removeHTML( queryBean.getBatch()));

                //Case Details section
                qrf.createSectionToCurrentChapter("Case Details");
                qrf.addTableToCurrentSection();
                qrf.addCurrentNestedTable("AER", "Local Ref Number",
                        "External Ref Number");
                addCaseData(queryBean, qrf);
                qrf.addCurrentSectionRow("Additional comments:", removeHTML(queryBean
                        .getAdditionalComments()));
                qrf.addCurrentSectionRow("Output Format:", removeHTML(queryBean
                        .getOutputFormat()));
                qrf.addCurrentSectionRow("'Others' specification:",removeHTML( queryBean
                        .getOutputOther()));
            }
            //end- MANUFACTURING type query

            //start- PREGNANCY type query
            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.PREGNANCY) {

                //Query Description section
                qrf.createSectionToCurrentChapter("Query Description");
                qrf.addTableToCurrentSection();
                qrf.addCurrentSectionRow("Your question:", removeHTML(queryBean
                        .getNatureOfCase()));
                qrf.addCurrentSectionRow("Index Case:", removeHTML(queryBean
                        .getIndexCase()));
                qrf.addCurrentSectionRow("Signs & Symp:",removeHTML( queryBean
                        .getSignsSymp()));
                qrf.addCurrentSectionRow("Investigations:", removeHTML(queryBean
                        .getInvestigations()));
                qrf.addCurrentSectionRow("Past Medical History:", removeHTML(queryBean
                        .getPmh()));
                qrf.addCurrentSectionRow("Comedications:",removeHTML( queryBean
                        .getConMeds()));
                qrf.addCurrentSectionRow("Other pertinent info:",removeHTML( queryBean
                        .getOtherInfo()));
                qrf.addCurrentSectionRow("Diagnosis/Confounders:", removeHTML(queryBean
                        .getDiagnosis()));

                //Pregnancy Information section
                qrf.createSectionToCurrentChapter("Pregnancy Information");
                qrf.addTableToCurrentSection();
                qrf.addCurrentSectionRow("Type of exposure:",removeHTML( queryBean
                        .getExposureType()));
                qrf.addCurrentSectionRow("Timing of exposure in pregnancy:",removeHTML(
                        queryBean.getTiming()));
                qrf.addCurrentSectionRow("Pregnancy/Foetal outcome:", removeHTML(queryBean
                        .getOutcome()));

                //Case Details section
                qrf.createSectionToCurrentChapter("Case Details");
                qrf.addTableToCurrentSection();
                qrf.addCurrentNestedTable("AER", "Local Ref Number",
                        "External Ref Number");
                addCaseData(queryBean, qrf);
                qrf.addCurrentSectionRow("Additional comments:",removeHTML( queryBean
                        .getAdditionalComments()));
                qrf.addCurrentSectionRow("Output Format:", removeHTML(queryBean
                        .getOutputFormat()));
                qrf.addCurrentSectionRow("'Others' specification:", removeHTML(queryBean
                        .getOutputOther()));
            }
            //end- PREGNANCY type query

            //start- EXTERNAL_AUDIT type query
            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.EXTERNAL_AUDIT) {

                //Query Description section
                qrf.createSectionToCurrentChapter("Query Description");
                qrf.addTableToCurrentSection();
                qrf.addCurrentSectionRow("Your question:", removeHTML(queryBean
                        .getNatureOfCase()));
                qrf.addCurrentSectionRow("Other pertinent info:", removeHTML(queryBean
                        .getOtherInfo()));
                qrf.addCurrentSectionRow("Adverse Event(s):",removeHTML( queryBean
                        .getAeTerm()));

                //Clinical Trial Info section
                qrf.createSectionToCurrentChapter("Clinical Trial Info");
                qrf.addTableToCurrentSection();
                qrf.addCurrentSectionRow("Protocol No(s):", removeHTML(queryBean
                        .getProtocolNumber()));
                qrf.addCurrentSectionRow("CRTN No(s):",removeHTML( queryBean
                        .getCrtnNumber()));
                qrf.addCurrentSectionRow("Patient No(s):", removeHTML(queryBean
                        .getPatientNumber()));
                qrf.addCurrentSectionRow("Case Selection:",removeHTML( queryBean
                        .getCaseSelection()));
                qrf.addCurrentSectionRow("Output Format:", removeHTML(queryBean
                        .getOutputFormatDesc()));
                qrf.addCurrentSectionRow("Special requests:", removeHTML(queryBean
                        .getOutputOther()));
            }
            //end- EXTERNAL_AUDIT type query

            //start- CASE_CLARIFICATION type query
            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.CASE_CLARIFICATION) {

                //Case Details section
                qrf.createSectionToCurrentChapter("Case Details");
                qrf.addTableToCurrentSection();
                qrf.addCurrentNestedTable("AER", "Local Ref Number",
                        "External Ref Number");
                addCaseData(queryBean, qrf);
                qrf.addCurrentSectionRow("Additional comments:",removeHTML( queryBean
                        .getAdditionalComments()));
                qrf.addCurrentSectionRow("Output Format:", removeHTML(queryBean
                        .getOutputFormat()));
                qrf.addCurrentSectionRow("'Others' specification:",removeHTML( queryBean
                        .getOutputOther()));
            }
            //end- CASE_CLARIFICATION type query

            //start- CASE_DATA_REQUEST type query
            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.CASE_DATA_REQUEST) {

                //Case Details section
                qrf.createSectionToCurrentChapter("Case Details");
                qrf.addTableToCurrentSection();
                qrf.addCurrentNestedTable("AER", "Local Ref Number",
                        "External Ref Number");
                addCaseData(queryBean, qrf);
                qrf.addCurrentSectionRow("Additional comments:", removeHTML(queryBean
                        .getAdditionalComments()));
                qrf.addCurrentSectionRow("Output Format:", removeHTML(queryBean
                        .getOutputFormat()));
                qrf.addCurrentSectionRow("'Others' specification:",removeHTML( queryBean
                        .getOutputOther()));
            }
            //end- CASE_DATA_REQUEST type query

            //start- SAE_RECONCILIATION type query
            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.SAE_RECONCILIATION) {

                //Query Description section
                qrf.createSectionToCurrentChapter("Clinical Trial Info");
                qrf.addTableToCurrentSection();
                qrf.addCurrentSectionRow("Protocol No(s):",removeHTML( queryBean
                        .getProtocolNumber()));
                qrf.addCurrentSectionRow("CRTN No(s):", removeHTML(queryBean
                        .getCrtnNumber()));
                qrf.addCurrentSectionRow("Patient No(s):", removeHTML(queryBean
                        .getPatientNumber()));
                qrf.addCurrentSectionRow("Case Selection:",removeHTML( queryBean
                        .getCaseSelection()));
                qrf.addCurrentSectionRow("Output Format:",removeHTML( queryBean
                        .getOutputFormatDesc()));
                qrf.addCurrentSectionRow("Special requests:",removeHTML( queryBean
                        .getOutputOther()));
            }
            //end- SAE_RECONCILIATION type query

            //start- IND_UPDATES type query
            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.IND_UPDATES) {

                //Query Description section
                qrf.createSectionToCurrentChapter("IND Updates");
                qrf.addTableToCurrentSection();
                qrf.addCurrentSectionRow("IND Number:", removeHTML(queryBean
                        .getIndNumber()));
            }
            //end- IND_UPDATES type query

            //start- PERFORMANCE_METRICS_S type query
            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.PERFORMANCE_METRICS) {

                //Query Description section
                qrf.createSectionToCurrentChapter("Query Description");
                qrf.addTableToCurrentSection();
                qrf.addCurrentSectionRow("Metrics required:", removeHTML(queryBean
                        .getNatureOfCase()));
                qrf.addCurrentSectionRow("Other pertinent info:",removeHTML( queryBean
                        .getOtherInfo()));
            }
            //end- PERFORMANCE_METRICS_S type query

            //start- SIGNAL_DETECTION_S type query
            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.SIGNAL_DETECTION) {

                //Query Description section
                qrf.createSectionToCurrentChapter("Query Description");
                qrf.addTableToCurrentSection();
                qrf.addCurrentSectionRow("Nature of question:", removeHTML(queryBean
                        .getNatureOfCase()));
                qrf.addCurrentSectionRow("Adverse Event(s):", removeHTML(queryBean
                        .getAeTerm()));
            }
            //end- SIGNAL_DETECTION_S type query

            //start- LITERATURE_SEARCH_S type query
            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.LITERATURE_SEARCH) {

                //Query Description section
                qrf.createSectionToCurrentChapter("Query Description");
                qrf.addTableToCurrentSection();
                qrf.addCurrentSectionRow("Nature of question:",removeHTML( queryBean
                        .getNatureOfCase()));
                qrf.addCurrentSectionRow("Other pertinent info:",removeHTML( queryBean
                        .getOtherInfo()));
                qrf.addCurrentSectionRow("Adverse Event(s):",removeHTML( queryBean
                        .getAeTerm()));
            }
            //end- LITERATURE_SEARCH_S type query

            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.MANUFACTURING_RECALL) {
            	addManufacturing(qrf, queryBean);
            }

            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.PERFORMANCE_METRICS2) {
            	addPerformanceMetrics(qrf, queryBean);
            }

            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.EXTERNAL_AUDIT_INSPECTION) {
            	addExternalAudit(qrf, queryBean);
            }
            
            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.MEDICAL) {
            	addMedical(qrf, queryBean);
            }

            if (QueryType.Code.valueOf(queryBean.getQueryTypeCode()) == QueryType.Code.DATA_SEARCH) {
            	addDataSearch(qrf, queryBean);
            }

            qrf.addCurrentChapterToDocument();
            qrf.close();

        } catch (Exception de) {
            logger.error(de.getMessage(), de);
        }
        return out;
    }


	private void addManufacturing(QTTpdfHelper qrf,
			QueryReviewDisplayModel queryBean) {
        //Query Description section
        qrf.createSectionToCurrentChapter("Manufacturing Details");
        qrf.addTableToCurrentSection();
        Manufacturing.Type manType = queryBean.getManufacturing().getType();
        qrf.addCurrentSectionRow("", manType != null ? manType.getLabel() : "");
        qrf.addCurrentSectionRow("Batch No(s).:", removeHTML(queryBean.getManufacturing().getBatchNo()));
        qrf.addCurrentSectionRow("Trackwise No(s).:", removeHTML(queryBean.getManufacturing().getTrackwiseNo()));

        qrf.createSectionToCurrentChapter("Associated Adverse Event(s)");
        qrf.addTableToCurrentSection();
        qrf.addCurrentSectionRow("Reported Adverse Event(s): ",removeHTML( queryBean.getAeTerm()));

        qrf.createSectionToCurrentChapter("Data Output");
        qrf.addTableToCurrentSection();
        qrf.addCurrentSectionRow("Output Format:",removeHTML( StringUtils.isNotBlank(queryBean.getOutputOther()) ? queryBean.getOutputOther() : queryBean.getOutputFormat()));
        qrf.addCurrentSectionRow("Additional Info:", removeHTML(queryBean.getOtherInfo()));
	}


	private void addDataSearch(QTTpdfHelper qrf,
			QueryReviewDisplayModel queryBean) {
        qrf.createSectionToCurrentChapter("Type of Search");
        qrf.addTableToCurrentSection();
        qrf.addCurrentSectionRow("Request:", removeHTML(queryBean.getDataSearch().getRequestType().getLabel()));
        qrf.addCurrentSectionRow("For:", removeHTML(queryBean.getDataSearch().getSearchType().getLabel()));
		
        switch (queryBean.getDataSearch().getSearchType()) {
        case AUTOMATED_QUERY:
            qrf.createSectionToCurrentChapter("Automated Query");
            qrf.addTableToCurrentSection();
            qrf.addCurrentSectionRow("Specification:",removeHTML( queryBean.getDataSearch().getSpecification()));
            qrf.addCurrentSectionRow("Output Format:",removeHTML( StringUtils.isNotBlank(queryBean.getOutputOther()) ? queryBean.getOutputOther() : queryBean.getOutputFormat()));
            qrf.addCurrentSectionRow("BO Report:", removeHTML(queryBean.getDataSearch().getBoReport()));
            qrf.addCurrentSectionRow("Report Parameters:", removeHTML(queryBean.getDataSearch().getReportParameters()));
            if (queryBean.getDataSearch().getFrequency() != null){
                qrf.addCurrentSectionRow("Frequency:",removeHTML( queryBean.getDataSearch().getFrequency() == Frequency.OTHER ? queryBean.getDataSearch().getFrequencyOther() : queryBean.getDataSearch().getFrequency().getLabel()));
            } else {
                qrf.addCurrentSectionRow("Frequency:", null);
            }
            break;
        case SIGNAL_DETECTION:
            qrf.createSectionToCurrentChapter("Data Search - Signal Detection / Drug Safety Reports");
            qrf.addTableToCurrentSection();
            qrf.addCurrentSectionRow("MedDRA Preferred Term(s):",removeHTML( queryBean.getDataSearch().getMeddraPreferredTerm()));
            qrf.addCurrentSectionRow("Additional Comments:", removeHTML(queryBean.getAdditionalComments()));
            qrf.addCurrentSectionRow("Output Format:", removeHTML(StringUtils.isNotBlank(queryBean.getOutputOther()) ? queryBean.getOutputOther() : queryBean.getOutputFormat()));
            break;
        case ASIME:
            qrf.createSectionToCurrentChapter("Data Search - ASIMEs");
            qrf.addTableToCurrentSection();
            qrf.addCurrentSectionRow("MedDRA Preferred Term(s):", removeHTML(queryBean.getDataSearch().getMeddraPreferredTerm()));
            qrf.addCurrentSectionRow("Index Case (AER No.):",removeHTML( queryBean.getIndexCase()));
            qrf.addCurrentSectionRow("Additional Comments:", removeHTML(queryBean.getAdditionalComments()));
            break;
        case SAE_RECONCILIATIONS:
            qrf.createSectionToCurrentChapter("Data Search - SAE Reconciliations or Internal Audit");
            qrf.addTableToCurrentSection();
            qrf.addCurrentSectionRow("Protocol/Study No.:",removeHTML( queryBean.getProtocolNumber()));
            qrf.addCurrentSectionRow("CRTN/Country/Investigator(s):", removeHTML(queryBean.getCrtnNumber()));
            qrf.addCurrentSectionRow("Patient No.(s):",removeHTML( queryBean.getPatientNumber()));
            qrf.addCurrentSectionRow("Case Selection:", removeHTML(queryBean.getCaseSelection()));
            qrf.addCurrentSectionRow("Output Format:", removeHTML(StringUtils.isNotBlank(queryBean.getOutputOther()) ? queryBean.getOutputOther() : queryBean.getOutputFormat()));
            break;
        case OTHER:
            qrf.createSectionToCurrentChapter("Data Search - Other");
            qrf.addTableToCurrentSection();
            qrf.addCurrentSectionRow("Your Question:",removeHTML( queryBean.getNatureOfCase()));
            qrf.addCurrentSectionRow("Output Format:", removeHTML(StringUtils.isNotBlank(queryBean.getOutputOther()) ? queryBean.getOutputOther() : queryBean.getOutputFormat()));
            break;
        case LITERATURE_SEARCH:
            qrf.createSectionToCurrentChapter("Literature Search");
            qrf.addTableToCurrentSection();
            qrf.addCurrentSectionRow("Your Question:", removeHTML(queryBean.getNatureOfCase()));
            break;
        default:
        }
		
	}


	private void addMedical(QTTpdfHelper qrf, QueryReviewDisplayModel queryBean) {
        //Query Description section
        qrf.createSectionToCurrentChapter("Query Description");
        qrf.addTableToCurrentSection();
        qrf.addCurrentSectionRow("Your question:", removeHTML(queryBean.getNatureOfCase()));

        //Adverse Event section
        qrf.createSectionToCurrentChapter("Adverse Event");
        qrf.addTableToCurrentSection();
        qrf.addCurrentSectionRow("Index Case:", removeHTML(queryBean.getIndexCase()));
        
        if (queryBean.getCases() != null && !queryBean.getCases().isEmpty()) {
        	Case c = queryBean.getCases().get(0); 
            qrf.addCurrentSectionRow("AER No.:", removeHTML(c.getAerNumber()));
            qrf.addCurrentSectionRow("LRN:", removeHTML(c.getLocalReferenceNumber()));
        }
        
        qrf.addCurrentSectionRow("Signs & Symptoms:", removeHTML(queryBean.getSignsSymp()));
        qrf.addCurrentSectionRow("Investigations:", removeHTML(queryBean.getInvestigations()));
        qrf.addCurrentSectionRow("Past Medical History:",removeHTML( queryBean.getPmh()));
        qrf.addCurrentSectionRow("Comedications:",removeHTML( queryBean.getConMeds()));
        qrf.addCurrentSectionRow("Other Pertinent Info:", removeHTML(queryBean.getOtherInfo()));
        qrf.addCurrentSectionRow("Diagnosis/Confounders:", removeHTML(queryBean.getDiagnosis()));

        //Pregnancy section
        qrf.createSectionToCurrentChapter("Pregnancy");
        qrf.addTableToCurrentSection();
        qrf.addCurrentSectionRow("Type of Exposure:",removeHTML( queryBean.getExposureType()));
        qrf.addCurrentSectionRow("Timing of Exposure in Pregnancy:", removeHTML(queryBean.getTiming()));
        qrf.addCurrentSectionRow("Pregnancy / Foetal Outcome:",removeHTML( queryBean.getOutcome()));
        qrf.addCurrentSectionRow("Comments:",removeHTML( queryBean.getComments()));

        //Drug Interaction section
        qrf.createSectionToCurrentChapter("Drug Interaction");
        qrf.addTableToCurrentSection();
        qrf.addCurrentSectionRow("Type of Exposure:", queryBean.getExposureType());

        if (queryBean.getDrugs() != null) {
            for (Drug drug : queryBean.getDrugs()) {
            	if (drug.getFirstFlag() == Constants.NO) {
	                qrf.addCurrentSectionRow("Drug Type:", 'Y' == drug.getRocheDrugFlag() ? "Roche" : "Non-Roche");
	                qrf.addCurrentSectionRow("Generic name:",removeHTML( drug.getInnGenericName()));
	                qrf.addCurrentSectionRow("Drug Retrieval name:", removeHTML(drug.getDrugRetrievalName()));
	                qrf.addCurrentSectionRow("Route:", removeHTML(drug.getRoute()));
	                qrf.addCurrentSectionRow("Dose:",removeHTML( drug.getDose()));
	                qrf.addCurrentSectionRow("Formulation:",removeHTML( drug.getFormulation()));
            	}
            }
        }

        qrf.addCurrentSectionRow("Comment:", queryBean.getAdditionalComments());
	}


	private void addExternalAudit(QTTpdfHelper qrf,
			QueryReviewDisplayModel queryBean) {
        //Query Description section
        qrf.createSectionToCurrentChapter("Query Description");
        qrf.addTableToCurrentSection();
        qrf.addCurrentSectionRow("Your question:", removeHTML(queryBean.getNatureOfCase()));
        qrf.addCurrentSectionRow("Other Pertinent Info:", removeHTML(queryBean.getOtherInfo()));
        
        qrf.createSectionToCurrentChapter("Data Metrics");
        qrf.addTableToCurrentSection();
        qrf.addCurrentSectionRow("All Cases:", getBooleanText(queryBean.getAudit().getAllCases()));
        StringBuilder sb = new StringBuilder();
        String allStudies = getBooleanText(queryBean.getAudit().getAllStudies());
        sb.append(allStudies == null ? "" : allStudies);
        if (queryBean.getAudit().getAllStudies() != null && !queryBean.getAudit().getAllStudies()) {
        	sb.append(", only for Study/Protocol No. ");
        	sb.append(queryBean.getAudit().getStudyNo());
        }
        qrf.addCurrentSectionRow("All Studies:", removeHTML(sb.toString()));
        qrf.addCurrentSectionRow("All Spontaneous Cases:", removeHTML(getBooleanText(queryBean.getAudit().getAllSpontaneousCases())));
        
        qrf.createSectionToCurrentChapter("Data Selection");
        qrf.addTableToCurrentSection();
        qrf.addCurrentSectionRow("All Submission Timelines:",removeHTML( getBooleanText(queryBean.getAudit().getAllSubm())));
        qrf.addCurrentSectionRow("SUSAR Submission Timelines:", removeHTML(getBooleanText(queryBean.getAudit().getSusarSubm())));
        qrf.addCurrentSectionRow("Serious Cases Timelines:",removeHTML( getBooleanText(queryBean.getAudit().getSeriousCases())));
        qrf.addCurrentSectionRow("Non-serious Cases Timelines:", removeHTML(getBooleanText(queryBean.getAudit().getNonSeriousCases())));
        qrf.addCurrentSectionRow("Submission Titles:", removeHTML(queryBean.getAudit().getSubmissionTitles()));

        qrf.createSectionToCurrentChapter("Data Output");
        qrf.addTableToCurrentSection();
        qrf.addCurrentSectionRow("Output Format:", removeHTML(queryBean.getOutputFormatDesc()));
        qrf.addCurrentSectionRow("Special Requests:",removeHTML( queryBean.getOutputOther()));
	}


	private void addPerformanceMetrics(QTTpdfHelper qrf,
			QueryReviewDisplayModel queryBean) {
        //Query Description section
        qrf.createSectionToCurrentChapter("Query Description");
        qrf.addTableToCurrentSection();
        qrf.addCurrentSectionRow("Metrics required:", removeHTML(queryBean
                .getPerformanceMetrics().getRequestDescription()));
        qrf.createSectionToCurrentChapter("Data Output");
        qrf.addTableToCurrentSection();
        qrf.addCurrentSectionRow("Output Format:", removeHTML(queryBean
                .getPerformanceMetrics().getOutputFormat().getCtrlOformat()));
        qrf.addCurrentSectionRow("Special Requests:", removeHTML(queryBean
                .getPerformanceMetrics().getSpecialRequests()));
	}


	private void addCaseData(QueryReviewDisplayModel queryBean, QTTpdfHelper qrf) {
		if (queryBean.getCases() != null) {
		    for (Case c : queryBean.getCases()) {
		        qrf.addCurrentNestedSectionRow(c.getAerNumber(), c.getLocalReferenceNumber(), c.getExternalReferenceNumber());
		    }
		}
	}
	
	private String getBooleanText(Boolean b) {
		if (b == null) {
			return null;
		} else {
			return b ? ActionConstants.YES : ActionConstants.NO;
		}
	}

}