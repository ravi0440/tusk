package com.roche.dss.qtt.query.web.action.oldqrf;

import java.util.ArrayList;
import java.util.List;

import com.roche.dss.qtt.model.Case;
import com.roche.dss.qtt.model.CtrlOformat;
import com.roche.dss.qtt.model.Drug;
import com.roche.dss.qtt.model.Pregnancy;
import com.roche.dss.qtt.model.PrgyExptype;
import com.roche.dss.qtt.query.comms.dto.QueryDescDTO;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.qrf.Constants;
import com.roche.dss.qtt.query.web.qrf.model.QueryDescFormModel;
import com.roche.dss.qtt.service.QrfService;

import javax.annotation.Resource;

public class MedicalAction extends QRFAbstractAction {

    private QrfService qrfService;
    
    private Case caze = new Case();

    private Pregnancy preg = new Pregnancy();
    
    private List<Drug> drugs = new ArrayList<Drug>();
	
    private String comments;
    
    QueryDescFormModel queryForm = new QueryDescFormModel();

    @Resource
    public void setQrfService(QrfService qrfService) {
		this.qrfService = qrfService;
	}

    @Override
    public Object getModel() {
        return queryForm;
    }

    private QueryDescDTO createQueryDescDTO(QueryDescFormModel queryDescForm) {
    	QueryDescDTO dto = new QueryDescDTO();
    	dto.setNatureOfCase(queryDescForm.getNatureofcase());
    	dto.setOtherInfo(queryDescForm.getOtherinfo());
    	dto.setConMeds(queryDescForm.getConmeds());
    	dto.setDiagnosis(queryDescForm.getDiagnosis());
    	dto.setIndexCase(queryDescForm.getIndexcase());
    	dto.setInvestigations(queryDescForm.getInvestigation());
    	dto.setPmh(queryDescForm.getPmh());
    	dto.setSignsSymp(queryDescForm.getSignsymp());
		return dto;
	}

	@SkipValidation
    public String populateMedical() {
    	return SUCCESS;
    }
    
	
	@Override
	public void validate() {
		super.validate();
		if (queryForm.getNatureofcase() == null || queryForm.getNatureofcase().isEmpty()) {
			addActionError(getText("queryDescForm.natureofcase.displayname"));
		}
		
	}
	
    public String execute()  {
        // the query is update
    	List<Drug> drugList = new ArrayList<Drug>(drugs.size());
    	for (Drug d : drugs) {
    		if (d.getRocheDrugFlag() == Constants.YES && StringUtils.isNotBlank(d.getDrugRetrievalName())
    				|| d.getRocheDrugFlag() == Constants.NO && StringUtils.isNotBlank(d.getInnGenericName())) {
    			drugList.add(d);
    		}
    	}
        if (getSession().get(ActionConstants.QRF_EDIT_QUERY_TYPE) != null) {
            qrfService.updateMedical(createQueryDescDTO(queryForm), caze, preg, drugList, comments, (Integer) getSession().get(ActionConstants.QRF_QUERY_ID));
        } else {
            qrfService.addMedical(createQueryDescDTO(queryForm), caze, preg, drugList, comments, (Integer) getSession().get(ActionConstants.QRF_QUERY_ID));
        }
        return SUCCESS;
    }

	public List<PrgyExptype> getPrgyExptypes() {
        return qrfService.getPrgyExptypes();
    }

	public List<Drug> getDrugs() {
		return drugs;
	}

	public void setDrugs(List<Drug> drugs) {
		this.drugs = drugs;
	}

	public Pregnancy getPreg() {
		return preg;
	}

	public void setPreg(Pregnancy preg) {
		this.preg = preg;
	}

	public Case getCaze() {
		return caze;
	}

	public void setCaze(Case caze) {
		this.caze = caze;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
}
