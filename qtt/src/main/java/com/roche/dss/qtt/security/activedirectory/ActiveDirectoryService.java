package com.roche.dss.qtt.security.activedirectory;

import java.util.Map;

@Service("activeDirectoryService")
public class ActiveDirectoryService {

    private static final Log LOG = LogFactory.getLog(ActiveDirectoryService.class);
    public static final String MAIL_AD_PROPERTY_NAME = "mail";
    public static final String SURNAME_AD_PROPERTY_NAME = "sn";
    public static final String FIRSTNAME_AD_PROPERTY_NAME = "givenName";


    private ActiveDirectoryProxy activeDirectoryProxy;

//    private Map<String, String> mappings = new HashMap<String, String>();

    public ActiveDirectoryService(ActiveDirectoryProxy activeDirectoryProxy) throws Exception {
        this.activeDirectoryProxy = activeDirectoryProxy;
        activeDirectoryProxy.init();
//        mappings.put("emea", "rbamouser");
//        mappings.put("nala", "rnumdmas");
//        mappings.put("asia", "RMOASIA");
//        mappings.put("exbp", "EXBP");
    }

//    public boolean isAuthenticated(String login, String password) {
//        try {
//            String domain = findUserDomain(login);
//            //String mail = findUserEmail(login);
//            activeDirectoryProxy.authenticate(domain + "\\" + login, password);
//            return true;
//        } catch (AuthenticationException e) {
//            LOG.debug("Authentication exception for user: " + login);
//            return false;
//        } catch (ActiveDirectoryUserNotFoundException e) {
//            LOG.debug("Authentication exception for user: " + login);
//            return false;
//        }
//    }

//    public List<ActiveDirectoryUser> findUsers(String name) {
//        return activeDirectoryProxy.findUsers(null, name, null, null);
//    }
//
//    private String findUserDomain(String username) throws ActiveDirectoryUserNotFoundException {
//        String propertyName = "userPrincipalName";
//        ActiveDirectoryObjectProperty property = new ActiveDirectoryObjectProperty(propertyName);
//        ActiveDirectoryObjectProperties properties = new ActiveDirectoryObjectProperties();
//        properties.add(property);
//        Map propertyMap = activeDirectoryProxy.getUserProperties(username, properties);
//        String userPrincipalName = (String) propertyMap.get(propertyName);
//        int start = userPrincipalName.indexOf("@") + 1;
//        int end = userPrincipalName.indexOf(".", start);
//        String region = userPrincipalName.substring(start, end);
//        return mappings.get(region.toLowerCase());
//    }

    public Map<String, String> findUserDetails(String username) throws ActiveDirectoryUserNotFoundException {
        //activeDirectoryProxy.getUserProperties(username, null) // taking all user properties

        ActiveDirectoryObjectProperties properties = new ActiveDirectoryObjectProperties();
        properties.add(new ActiveDirectoryObjectProperty(MAIL_AD_PROPERTY_NAME));
        properties.add(new ActiveDirectoryObjectProperty(SURNAME_AD_PROPERTY_NAME));
        properties.add(new ActiveDirectoryObjectProperty(FIRSTNAME_AD_PROPERTY_NAME));
        Map propertyMap = activeDirectoryProxy.getUserProperties(username, properties);
        return propertyMap;
    }

}
