package com.roche.dss.qtt.query.web.utils;
import com.roche.dss.qtt.security.user.RocheUserDetails;

public interface UserDetailsAware {
	
	public void setUserDetails(RocheUserDetails userDetails);
	
}