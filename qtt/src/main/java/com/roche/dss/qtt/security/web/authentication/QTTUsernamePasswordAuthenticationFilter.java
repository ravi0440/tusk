package com.roche.dss.qtt.security.web.authentication;

/**
 * @author zerkowsm
 *
 */
public class QTTUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	public static final String DSS_SECURITY_FORM_DOMAIN_KEY = "j_domain";

	private String domainParameter = DSS_SECURITY_FORM_DOMAIN_KEY;

	private boolean postOnly = true;

	public QTTUsernamePasswordAuthenticationFilter() {
		super();
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request,
			HttpServletResponse response) throws AuthenticationException {

		if (postOnly && !request.getMethod().equals("POST")) {
			throw new AuthenticationServiceException(
					"Authentication method not supported: "
							+ request.getMethod());
		}

		String username = obtainUsername(request);
		String password = obtainPassword(request);
		String domain = obtainDomain(request);

		if (username == null) {
			username = "";
		}

		if (password == null) {
			password = "";
		}

		if (domain == null) {
			domain = "";
		}

		username = username.trim();

		UsernamePasswordAuthenticationToken authRequest = new QTTUsernamePasswordAuthenticationToken(
				username, password, domain);

		HttpSession session = request.getSession(false);

		if (session != null || getAllowSessionCreation()) {
			request.getSession().setAttribute(
					SPRING_SECURITY_LAST_USERNAME_KEY,
					TextEscapeUtils.escapeEntities(username));
		}

		setDetails(request, authRequest);

        Authentication auth = this.getAuthenticationManager().authenticate(authRequest);
		return auth;
	}

	protected String obtainDomain(HttpServletRequest request) {
		return request.getParameter(domainParameter);
	}

}