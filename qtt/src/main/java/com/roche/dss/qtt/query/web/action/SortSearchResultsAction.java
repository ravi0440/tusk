package com.roche.dss.qtt.query.web.action;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.roche.dss.qtt.query.web.model.QuickSearchParams;
import com.roche.dss.qtt.query.web.qrf.model.SearchScopeEntry;
import com.roche.dss.qtt.service.FullTextSearchService;
import com.roche.dss.qtt.service.SearchResults;
import com.roche.dss.util.ApplicationUtils;

@ParentPackage("default") 
public class SortSearchResultsAction extends CommonActionSupport implements SessionAware {
	
	private static final long serialVersionUID = 3353433166491424363L;

	protected static final int MAX_RESULTS = 100;
	
	@Autowired
	protected FullTextSearchService searchService;
	
	protected QuickSearchParams searchParams;
	
	protected String orderBy;
	
	protected Boolean asc;
	
	protected SearchResults results;
	
	protected Integer resultPage;
	
	protected Map<String, Object> session;
		
	protected static final Logger log = LoggerFactory.getLogger(SortSearchResultsAction.class);
	
	protected List<SearchScopeEntry> scopes = Arrays.asList(
			new SearchScopeEntry("both", "Search both Database and Attachments")
			, new SearchScopeEntry("db", "Search only in Database")
			, new SearchScopeEntry("att", "Search only in Attachments"));
	
	public QuickSearchParams getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(QuickSearchParams searchParams) {
		this.searchParams = searchParams;
	}

	public SearchResults getResults() {
		return results;
	}

	public void setResults(SearchResults results) {
		this.results = results;
	}
	
	public Boolean getAsc() {
		return asc;
	}

	public void setAsc(Boolean asc) {
		this.asc = asc;
	}

	public Integer getResultPage() {
		return resultPage;
	}

	public void setResultPage(Integer resultPage) {
		this.resultPage = resultPage;
	}

	@Override
	public void validate() {
		super.validate();
		
		if(searchParams.getTerm().startsWith("*")){
			addActionError("Term cannot start from '*' ");
		}
		
		if(searchParams.getTerm().startsWith("?")){
			addActionError("Term cannot start from '?' ");
		}
		
		Date now = new Date();
		//Validate Period Dates
		if (searchParams.getPeriodDateFrom() != null && searchParams.getPeriodDateFrom().after(now)) {
			addActionError("Time Period 'FROM' date cannot be in the future.");
		}
		if (searchParams.getPeriodDateTo() != null) {
			if (searchParams.getPeriodDateTo().after(now)) {
				addActionError("Time Period 'TO' date cannot be in the future.");
			}
			if (searchParams.getPeriodDateFrom() != null 
					&& searchParams.getPeriodDateFrom().after(searchParams.getPeriodDateTo())) {
				addActionError("Time Period 'FROM' date cannot be later than the corresponding 'TO' date.");
			}
		}
		
		//Validate Query Response Dates
		if (searchParams.getQryResponseDateFrom() != null && searchParams.getQryResponseDateFrom().after(now)) {
			addActionError("Query Response 'FROM' date cannot be in the future.");
		}
		if (searchParams.getQryResponseDateTo() != null) {
			if (searchParams.getQryResponseDateTo().after(now)) {
				addActionError("Query Response 'TO' date cannot be in the future.");
			}
			if (searchParams.getQryResponseDateFrom() != null 
					&& searchParams.getQryResponseDateFrom().after(searchParams.getQryResponseDateTo())) {
				addActionError("Query Response 'FROM' date cannot be later than the corresponding 'TO' date.");
			}
		}
	}

	@Action(value="QuickSearchResults", results={
				@Result(name="success", type="tiles", location="/quick_search.list")
				, @Result(name="input", type="tiles", location="/quick_search.search")
			})
	public String showResults() {
		if (searchParams == null) {
			// we came here from the sorting link
			searchParams = (QuickSearchParams) session.get(ApplicationUtils.SESSION_QUICK_SEARCH_PARAMS);
			if (searchParams == null) {
				log.warn("Unable to sort quick search results, unknown searchParams");
				return SUCCESS;
			}
		} else {
			// we came here from the search parameters page
			session.put(ApplicationUtils.SESSION_QUICK_SEARCH_PARAMS, searchParams);
		}

		if (QuickSearchParams.SCOPE_BOTH.equals(searchParams.getSearchScope())) {
			results = searchService.searchBoth(searchParams);
		} else if (QuickSearchParams.SCOPE_DB.equals(searchParams.getSearchScope())) {
			results = searchService.searchDatabase(searchParams);
		} else if (QuickSearchParams.SCOPE_ATT.equals(searchParams.getSearchScope())) {
			results = searchService.searchAttachments(searchParams);
		}
		
		if (results == null) {
			// we come here when there are no parameters specified
			return SUCCESS;
		}
	
		if (resultPage == null) {
			resultPage = Integer.valueOf(1);
		}
		if (asc == null) {
			asc = true;
		}
		Collections.sort(results.getResults(), ApplicationUtils.getComparator(orderBy));
		if (!asc) {
			Collections.reverse(results.getResults());
		}
		int startIndex = MAX_RESULTS * (resultPage - 1);
		int endIndex = Math.min(startIndex + MAX_RESULTS, results.getResults().size());
		results.setResults(results.getResults().subList(startIndex, endIndex));
		
		return SUCCESS;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public List<SearchScopeEntry> getScopes() {
		return scopes;
	}
}
