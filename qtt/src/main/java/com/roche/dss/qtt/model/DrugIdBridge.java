package com.roche.dss.qtt.model;

public class DrugIdBridge implements TwoWayFieldBridge {
	public Object get(String name, Document document) {
		DrugId id = new DrugId();
		Field field = document.getField(name + ".drugNumberSeq");
		id.setDrugNumberSeq(Long.parseLong(field.stringValue()));
		field = document.getField(name + ".querySeq");
		id.setQuerySeq(Long.parseLong(field.stringValue()));
		return id;
	}

	public String objectToString(Object object) {
		DrugId id = (DrugId) object;
		StringBuilder sb = new StringBuilder();
		sb.append(id.getDrugNumberSeq()).append(id.getQuerySeq());
		return sb.toString();
	}

	public void set(String name, Object value, Document document,
			LuceneOptions luceneOptions) {
		DrugId id = (DrugId) value;
		// store each property in a unique field
		luceneOptions.addFieldToDocument(name + ".drugNumberSeq", String.valueOf(id
				.getDrugNumberSeq()), document);
		luceneOptions.addFieldToDocument(name + ".querySeq", String.valueOf(id
				.getQuerySeq()), document);
		// store the unique string representation in the named field
		luceneOptions.addFieldToDocument(name, objectToString(id), document);		
	}
}
