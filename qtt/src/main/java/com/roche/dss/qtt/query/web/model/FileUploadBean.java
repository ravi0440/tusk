package com.roche.dss.qtt.query.web.model;

import java.io.*;

/**
 * User: pruchnil
 *
 *
 */
public class FileUploadBean implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(FileUploadBean.class);

	private File upload;
	private String uploadContentType;
	private String uploadFileName;
    private String givenFileName;
    private String type;

	public File getUpload() {
		return upload;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getUploadContentType() {
		return uploadContentType;
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

    public String getGivenFileName() {
        return givenFileName;
    }

    public void setGivenFileName(String givenFileName) {
        this.givenFileName = givenFileName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isEmpty() {
		return upload == null;
	}

	public byte[] getBytes() {
		InputStream is = null;
		byte[] retVal = null;
		try {
			is = new FileInputStream(upload);
			retVal = IOUtils.toByteArray(is);
		} catch (IOException e) {
            logger.debug("FileUploadBean Exception " + e);
		} finally {
			try {
				if(is != null) {
					is.close();
				}
			} catch(IOException ioe) {
				logger.error(ioe.getMessage());
			}
		}
		return retVal;
	}
}

