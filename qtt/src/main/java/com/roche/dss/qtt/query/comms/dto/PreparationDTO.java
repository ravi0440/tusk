package com.roche.dss.qtt.query.comms.dto;

import java.io.Serializable;

/**
 * This DTO contains information about "PreQueryPreparation"
 * <p/>
 * Date: 16.02.11
 */
public class PreparationDTO implements Serializable {
    private String repository;
    private String psur;
    private String cds;
    private String issue;
    private String literature;
    private String others;
    private String othersComment;

    public String getRepository() {
        return repository;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }

    public String getPsur() {
        return psur;
    }

    public void setPsur(String psur) {
        this.psur = psur;
    }

    public String getCds() {
        return cds;
    }

    public void setCds(String cds) {
        this.cds = cds;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getLiterature() {
        return literature;
    }

    public void setLiterature(String literature) {
        this.literature = literature;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getOthersComment() {
        return othersComment;
    }

    public void setOthersComment(String othersComment) {
        this.othersComment = othersComment;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cds == null) ? 0 : cds.hashCode());
		result = prime * result + ((issue == null) ? 0 : issue.hashCode());
		result = prime * result
				+ ((literature == null) ? 0 : literature.hashCode());
		result = prime * result + ((others == null) ? 0 : others.hashCode());
		result = prime * result
				+ ((othersComment == null) ? 0 : othersComment.hashCode());
		result = prime * result + ((psur == null) ? 0 : psur.hashCode());
		result = prime * result
				+ ((repository == null) ? 0 : repository.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if(!(obj instanceof PreparationDTO))
			return false;
		PreparationDTO other = (PreparationDTO) obj;
		if (cds == null) {
			if (other.cds != null)
				return false;
		} else if (!cds.equals(other.cds))
			return false;
		if (issue == null) {
			if (other.issue != null)
				return false;
		} else if (!issue.equals(other.issue))
			return false;
		if (literature == null) {
			if (other.literature != null)
				return false;
		} else if (!literature.equals(other.literature))
			return false;
		if (others == null) {
			if (other.others != null)
				return false;
		} else if (!others.equals(other.others))
			return false;
		if (othersComment == null) {
			if (other.othersComment != null)
				return false;
		} else if (!othersComment.equals(other.othersComment))
			return false;
		if (psur == null) {
			if (other.psur != null)
				return false;
		} else if (!psur.equals(other.psur))
			return false;
		if (repository == null) {
			if (other.repository != null)
				return false;
		} else if (!repository.equals(other.repository))
			return false;
		return true;
	}
}
