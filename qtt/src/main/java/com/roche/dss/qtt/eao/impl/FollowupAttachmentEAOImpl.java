package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.FollowupAttachmentEAO;
import com.roche.dss.qtt.model.FollowupAttachment;

/**
 * User: pruchnil
 */
@Repository("followupAttachmentEAO")
public class FollowupAttachmentEAOImpl extends AbstractBaseEAO<FollowupAttachment> implements FollowupAttachmentEAO {

    @Override
    public FollowupAttachment findOpenByQuerySeq(long queryId) {
        return getEntityManager().createNamedQuery("followupAttachment.findOpenByQuerySeq", FollowupAttachment.class).setParameter("queryId", queryId).getSingleResult();
    }

    @Override
    public long getMaxFollowupSeq(long queryId) {
        Long value = getEntityManager().createNamedQuery("followupAttachment.findMaxFollowupSeq", Long.class).setParameter("queryId", queryId).getSingleResult();
        return value == null ? 0 : value;
    }
}
