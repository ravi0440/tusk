package com.roche.dss.qtt.service;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.web.model.FileUploadBean;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * This service takes care of all actions on attachments repo
 * It's equivalent to FileOperationsHelper in former version of QTT.
 * <p/>
 * User: pruchnil
 */
public interface FileOperationsService {
    InputStream getInputStream(int internalQueryId, String fileName) throws Exception;

    boolean insertFileIntoRepository(String queryId, File file, StringBuffer fileName) throws IOException;

    String findDestinationDirectory(String s);

    boolean deleteDir(File file) throws Exception;

    public String getMimeType(String filename);

    public void streamAttachment(InputStream input, OutputStream output) throws IOException;

    void correctFileNameOfAttachmentIfNeeded(Query query, FileUploadBean attachment);

}
