package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.service.FollowupAttachmentService;
import com.roche.dss.qtt.service.QueryService;
import javax.annotation.Resource;

/**
 *
 *
 * User: pruchnil
 */
public class FollowupAttachmentAction extends CommonActionSupport {
    private static final Logger logger = LoggerFactory.getLogger(FollowupAttachmentAction.class);
    private long queryId;
    private String sourceSearch;

    private FollowupAttachmentService service;

    @Resource
    public void setFollowupAttachmentService(FollowupAttachmentService service) {
        this.service = service;
    }

    public String open() throws Exception {
        service.openFollowupAttachment(queryId);
        return SUCCESS;
    }

    public String close() throws Exception {
        service.closeFollowupAttachment(queryId);
        return SUCCESS;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }
    
    public String getSourceSearch() {
		return sourceSearch;
	}

	public void setSourceSearch(String sourceSearch) {
		this.sourceSearch = sourceSearch;
	}
    
}
