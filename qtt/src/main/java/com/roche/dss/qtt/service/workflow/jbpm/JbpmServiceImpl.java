package com.roche.dss.qtt.service.workflow.jbpm;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.zip.ZipInputStream;

import com.roche.dss.qtt.model.QueriesProcessinstance;
import com.roche.dss.qtt.jbpm.template.JbpmCallback;
import com.roche.dss.qtt.jbpm.template.JbpmTemplate;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.service.workflow.exception.TaskAlreadyBegunException;
import com.roche.dss.qtt.service.workflow.exception.TaskAlreadyCompletedException;
import com.roche.dss.qtt.utility.Activities;
import com.roche.dss.qtt.utility.Nodes;

/**
 * @author zerkowsm
 *
 */
@Service("jbpmService")
public class JbpmServiceImpl implements JbpmService {
	
	private static final Logger logger = LoggerFactory.getLogger(JbpmServiceImpl.class);

	@Autowired
	private JbpmTemplate jbpmTemplate;
	
	@Override
	@Transactional
	public void deployProcessDefinitionParFile(final File processDefinitionParFile) {
		jbpmTemplate.doInJbpm(new JbpmCallback() {
		
			public Object execute(JbpmContext jbpmCon) {
				
				boolean alreadyDeployed = false;
				
				try {
					if(jbpmCon.getGraphSession().findLatestProcessDefinition(QTT_WORKFLOW_PROCESS_NAME) != null || (jbpmCon.getGraphSession().findAllProcessDefinitions() != null && !jbpmCon.getGraphSession().findAllProcessDefinitions().isEmpty()) ) {
						if(logger.isDebugEnabled()) {
							logger.debug("There is already process definition, will not create another. ");
						}
						alreadyDeployed = true;						
					}
				} catch (JbpmException e) {
					if(logger.isDebugEnabled()) {
						logger.debug("There is no process definition, one will be created. ");
					}
				}
				if(alreadyDeployed) {
					throw new JbpmException("There is already process definition, will not create another. ");
				}
				
				ProcessDefinition definition;
				FileInputStream inputStream = null;
				try {
					inputStream = new FileInputStream(processDefinitionParFile);					
					ZipInputStream zipInputStream = new ZipInputStream(inputStream);
					try {
						definition = ProcessDefinition.parseParZipInputStream(zipInputStream);
					} finally {
						zipInputStream.close();
					}				
				} catch (IOException e) {
					throw new RuntimeException("IOException during the file processing: " + processDefinitionParFile, e);
				}
				jbpmCon.getGraphSession().deployProcessDefinition(definition);
				return null;
			}		
		});
	}
	
	@Override
	public ProcessInstance createProcessInstance(final String workflowProcessName, final Query query) {
		return (ProcessInstance) jbpmTemplate.doInJbpm(new JbpmCallback() {

			public Object execute(JbpmContext jbpmCon) {
				ProcessInstance processInstance = createNewProcessInstance(workflowProcessName);
				jbpmCon.save(processInstance);
				processInstance.setKey(Long.valueOf(query.getQuerySeq()).toString());
				jbpmCon.save(processInstance);
				setProcessVariable(processInstance.getId(),	PROCESS_INSTANCE_OWNER_ID, Long.valueOf(query.getQuerySeq()));
				jbpmCon.save(processInstance);
				long processInstanceId = processInstance.getId();
				query.setProcessInstanceId(processInstanceId);

				jbpmCon.getSession().persist(createQueriesProcessinstance(processInstanceId, query));

				jbpmCon.getSession().flush();
				jbpmCon.getSession().refresh(query);
				jbpmCon.getSession().saveOrUpdate(query);
				return processInstance;
			}
		});
	}

	private QueriesProcessinstance createQueriesProcessinstance(long processInstanceId, Query query) {
		QueriesProcessinstance queriesProcessinstance = new QueriesProcessinstance();
		queriesProcessinstance.setQuerySeq(query.getQuerySeq());
		queriesProcessinstance.setFollowupSeq(query.getFollowupSeq()==null ? 0 : query.getFollowupSeq());
		queriesProcessinstance.setProcessInstanceId(processInstanceId);
		queriesProcessinstance.setCreationTs(new DateTime());
		return queriesProcessinstance;
	}

	@Override
	public ProcessInstance createProcessInstance(final String workflowProcessName, final Map<String, Object> variables) {
		return (ProcessInstance) jbpmTemplate.doInJbpm(new JbpmCallback() {

			public Object execute(JbpmContext jbpmCon) {
				ProcessInstance processInstance = createNewProcessInstance(workflowProcessName);
				jbpmCon.save(processInstance);
				if (null != variables) {
					for (String variableName : variables.keySet()) {
						setProcessVariable(processInstance.getId(),
								variableName, variables.get(variableName));
					}
				}
				jbpmCon.save(processInstance);
				long procId = processInstance.getId();
				processInstance = jbpmCon.getProcessInstance(procId);
				jbpmCon.save(processInstance);
				return processInstance;
			}
		});
	}

	@Override
	public ProcessInstance createNewProcessInstance(final String processDefinition) {
		return (ProcessInstance) jbpmTemplate.doInJbpm(new JbpmCallback() {

			public Object execute(JbpmContext jbpmCon) {
				ProcessDefinition processdef = jbpmCon.getGraphSession().findLatestProcessDefinition(processDefinition);
				ProcessInstance inst = processdef.createProcessInstance();
				return inst;
			}
		});
	}

	@Override
	public void setProcessVariable(final long processInstanceId, final String name, final Object value) {
		jbpmTemplate.doInJbpm(new JbpmCallback() {

			public Object execute(JbpmContext jbpmCon) {
				ProcessInstance processInstance = getProcessInstanceById(processInstanceId);
				processInstance.getContextInstance().setVariable(name, value);
				jbpmCon.save(processInstance);
				return null;
			}
		});
	}

	@Override
	public ProcessInstance getProcessInstanceById(final Long processInstanceId) {
		return (ProcessInstance) jbpmTemplate.doInJbpm(new JbpmCallback() {

			public Object execute(JbpmContext jbpmCon) {
				return jbpmCon.getProcessInstance(processInstanceId);
			}
		});
	}
	
	/**
	 * @deprecated Please use getProcessInstanceById(Long) instead
	 * @see getProcessInstanceById(Long)
	 */
	@Deprecated
	@Override
	public ProcessInstance getProcessInstanceByKey(final String processDefinition, final String key) {
		return (ProcessInstance) jbpmTemplate.doInJbpm(new JbpmCallback() {

			public Object execute(JbpmContext jbpmCon) {
				ProcessDefinition processDef = jbpmCon.getGraphSession().findLatestProcessDefinition(processDefinition);
				return jbpmCon.getProcessInstance(processDef, key);
			}
		});
	}
	
	@Override
	public Collection<ProcessInstance> getProcessInstancesByKey(
			final String processDefinition,final String key) {
		return (Collection<ProcessInstance>) jbpmTemplate.doInJbpm(new JbpmCallback() {
			public Object execute(JbpmContext jbpmCon) {
				List<ProcessDefinition> processDefs = jbpmCon.getGraphSession().findAllProcessDefinitionVersions(processDefinition);
				List<ProcessInstance> instances = new ArrayList<ProcessInstance>();
				for (ProcessDefinition def : processDefs) {
					List<ProcessInstance> i = jbpmCon.getSession().getNamedQuery("GraphSession.findProcessInstanceByKey")
					          .setEntity("processDefinition", def)
					          .setString("key", key).list();
					instances.addAll(i);
				}
				return instances;
			}
		});
	}

	
	@Override
    public void startProcessInstance(final long processInstanceId, final String userName) {
        jbpmTemplate.doInJbpm(new JbpmCallback() {
        	
            public Object execute(JbpmContext jbpmCon) throws JbpmException {
                ProcessInstance processInstance = jbpmCon.getProcessInstance(processInstanceId);
				setProcessVariable(processInstance.getId(),	PROCESS_INSTANCE_START_ACTOR_ID, userName);
                processInstance.signal();
                jbpmCon.save(processInstance);
                return null;
            }
        });
    }
	
    @Override
    public void assignTask(final long taskId, final String userName) {
        jbpmTemplate.doInJbpm(new JbpmCallback() {
            @Override
            public Object execute(JbpmContext jbpmCon) {
               TaskInstance task = getTaskInstance(taskId);
               if(StringUtils.isNotEmpty(userName)){
                   task.setActorId(userName);
               }
               return null;
            }
        });
    }
    
    @Override
    public void assignTask(final long taskId, final String userName, final Map<String, Object> variables) {
        jbpmTemplate.doInJbpm(new JbpmCallback() {
            @Override
            public Object execute(JbpmContext jbpmCon) {
               TaskInstance task = getTaskInstance(taskId);
               if(StringUtils.isNotEmpty(userName)){
                   task.setActorId(userName);
               }
	       		if(variables != null) {							
					for(Map.Entry<String, Object> entry : variables.entrySet()) {
						setProcessVariable(task.getProcessInstance().getId(), entry.getKey(), entry.getValue());
					}			
				}
               return null;
            }
        });
    }
    
	@Override
	public void assignTask(final Query query, final String userName) {
		jbpmTemplate.doInJbpm(new JbpmCallback() {
			
			public Object execute(JbpmContext jbpmCon) {								
				ProcessInstance processInstance = jbpmCon.getProcessInstance(query.getProcessInstanceId().longValue());
				Token token = processInstance.getRootToken();
				TaskInstance task = getTaskInstance(processInstance, token);
				if(userName != null) {
					task.setActorId(userName);
				}
				return null;
			}
		});		
	}

    @Override
	public void assignTask(final Query query, final String userName, final String processVariable) {
		jbpmTemplate.doInJbpm(new JbpmCallback() {
			
			public Object execute(JbpmContext jbpmCon) {								
				ProcessInstance processInstance = jbpmCon.getProcessInstance(query.getProcessInstanceId().longValue());
				setProcessVariable(query.getProcessInstanceId().longValue(), processVariable, userName);
				Token token = processInstance.getRootToken();
				TaskInstance task = getTaskInstance(processInstance, token);
				if(userName != null) {
					task.setActorId(userName);
				}
				return null;
			}
		});		
	}
    
	@Override
	public void signal(final String actorId, final long taskInstanceId, final String transitionName) {
		jbpmTemplate.doInJbpm(new JbpmCallback() {
			
			public Object execute(JbpmContext jbpmCon) {				
				TaskInstance taskInstance = getTaskInstance(taskInstanceId);
				if (taskInstance != null) {
					if (canTaskBeEndedByCurrentUser(actorId, taskInstance)) {
						if (taskInstance.getEnd() == null && !taskInstance.hasEnded()) {
							if (StringUtils.isBlank(transitionName)) {
								taskInstance.end();
							} else {
								taskInstance.end(transitionName);
							}
						} else {
							logger.error("Task instance with id: " + taskInstanceId + " already completed by another actor! ");
							throw new TaskAlreadyCompletedException("Task instance with id: " + taskInstanceId + " already completed by actor: " + taskInstance.getActorId());
						}
					} else {
						throw new AuthorizationServiceException("The currently logged user cannot end the task. ");
					}
					jbpmCon.save(taskInstance);
				} else {
					logger.error("Task instance with id: " + taskInstanceId + " does not exist! ");
                	throw new UnsupportedOperationException("Task instance with id: " + taskInstanceId + " does not exist! ");
				}
				return null;
			}
		});
	}
	
	
	@Override
	public void signal(final String actorId, final long taskInstanceId, final String transitionName, final Long processInstanceId, final Map<String, Object> variables) {
		jbpmTemplate.doInJbpm(new JbpmCallback() {
			
			public Object execute(JbpmContext jbpmCon) {				
				TaskInstance taskInstance = getTaskInstance(taskInstanceId);
				if (taskInstance != null) {
					if (canTaskBeEndedByCurrentUser(actorId, taskInstance)) {
						if (taskInstance.getEnd() == null && !taskInstance.hasEnded()) {
							if(variables != null) {							
								for(Map.Entry<String, Object> entry : variables.entrySet()) {
									setProcessVariable(processInstanceId.longValue(), entry.getKey(), entry.getValue());
								}			
							}
							if (StringUtils.isBlank(transitionName)) {
								taskInstance.end();
							} else {
								taskInstance.end(transitionName);
							}
						} else {
							logger.error("Task instance with id: " + taskInstanceId + " already completed by another actor! ");
							throw new TaskAlreadyCompletedException("Task instance with id: " + taskInstanceId + " already completed by actor: " + taskInstance.getActorId());
						}
					} else {
						throw new AuthorizationServiceException("The currently logged user cannot end the task. ");
					}
					jbpmCon.save(taskInstance);
				} else {
					logger.error("Task instance with id: " + taskInstanceId + " does not exist! ");
                	throw new UnsupportedOperationException("Task instance with id: " + taskInstanceId + " does not exist! ");
				}				
				return null;
			}
		});
	}	
	
	@Override
	public void signal(final String actorId, final Query query, final String transitionName) {
		jbpmTemplate.doInJbpm(new JbpmCallback() {
			
			public Object execute(JbpmContext jbpmCon) {
				ProcessInstance processInstance = jbpmCon.getProcessInstance(query.getProcessInstanceId().longValue());
				Token token = processInstance.getRootToken();
				TaskInstance taskInstance = getTaskInstance(processInstance, token);
				if (taskInstance != null) {
					if (taskInstance.getProcessInstance().getRootToken() != null) {
						if (canTaskBeEndedByCurrentUser(actorId, taskInstance)) {
							if (taskInstance.getEnd() == null && !taskInstance.hasEnded()) {
								if(actorId != null && !actorId.equals(taskInstance.getActorId())) {
									taskInstance.setActorId(actorId);
								}
								if (StringUtils.isBlank(transitionName)) {
									taskInstance.end();
								} else {
									taskInstance.end(transitionName);
								}
							} else {
								logger.error("Task instance with id: " + taskInstance.getId() + " already completed by another actor! ");
								throw new TaskAlreadyCompletedException("Task instance with id: " + taskInstance.getId() + " already completed by actor: " + taskInstance.getActorId());
							}
						} else {
							throw new AuthorizationServiceException("The currently logged user cannot end the task. ");
						}
							
					} else {
						logger.debug("Root token is null - whole process will be closed. ");
						if (StringUtils.isBlank(transitionName)) {
							processInstance.signal();
						} else {
							processInstance.signal(transitionName);						
						}
					}
				} else {
					logger.error("Task instance does not exist! ");
                	throw new UnsupportedOperationException("Task instance does not exist! ");
				}
				logger.debug("Process instance ready for save. ");
				jbpmCon.save(processInstance);
				logger.debug("Process instance ready for commit. ");
				return null;
			}
		});
	}
	
	@Override
	public void lockTask(final String actorId, final long taskInstanceId) {
		jbpmTemplate.doInJbpm(new JbpmCallback() {
			
			public Object execute(JbpmContext jbpmCon) {
				  TaskInstance taskInstance = getTaskInstance(taskInstanceId);
	                if(taskInstance != null) {
	                    if(taskInstance.getStart() == null && !taskInstance.isCancelled() && !taskInstance.hasEnded()) {
	                        taskInstance.start(actorId);
	                        logger.info("Starting task instance with id: " + taskInstanceId + " by actor: " + actorId);
	                    }else if(taskInstance.hasEnded()) {
	                    	logger.error("Task instance with id: " + taskInstanceId + " already completed by another actor! ");
		                	throw new TaskAlreadyCompletedException("Task instance with id: " + taskInstanceId + " already completed by actor: " + taskInstance.getActorId());
	                    }else {
	                    	if(!canTaskBeContinuedByCurrentUser(actorId, taskInstance)){
                    		logger.error("Task instance with id: " + taskInstanceId + " already started by another actor! ");
		                	throw new TaskAlreadyBegunException("Task instance with id: " + taskInstanceId + " already started by another actor! ");
	                    	}
		                }
	                } else {
	                	logger.error("Task instance with id: " + taskInstanceId + " does not exist! ");
	                	throw new UnsupportedOperationException("Task instance with id: " + taskInstanceId + " does not exist! ");
	                }
	                return null;
			}
			
		});
	}
	
	@Override
    public TaskInstance getTaskInstance(final long taskInstanceId) {
        return (TaskInstance) jbpmTemplate.doInJbpm(new JbpmCallback() {
        	
            public Object execute(JbpmContext jbpmCon) {
            	return jbpmCon.getTaskInstance(taskInstanceId);               
            }
        });
    }
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<TaskInstance> retrieveProcessTaskList(final long processInstanceId) {
		return (Collection<TaskInstance>) jbpmTemplate.doInJbpm(new JbpmCallback() {

			public Object execute(JbpmContext jbpmCon) {
				ProcessInstance processInstance = jbpmCon.getProcessInstance(processInstanceId);
				Collection<TaskInstance> tasks = processInstance.getTaskMgmtInstance().getTaskInstances();
				return tasks;
			}
		});
	}

	@SuppressWarnings("unchecked")
	@Override
	public TaskInstance retrieveProcessTaskCurrent(final long processInstanceId) {
		Collection<TaskInstance> taskInstances =  retrieveProcessTaskList(processInstanceId);
		Iterator<TaskInstance> it = taskInstances.iterator();
		TaskInstance taskInstance = null;
		while(it.hasNext()){
			TaskInstance current = it.next();
			if (taskInstance == null || current.getCreate().getTime() >  taskInstance.getCreate().getTime()) {
				taskInstance = current;
			}
		}
		return taskInstance;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<TaskInstance> retrieveProcessActiveTaskList(final long processInstanceId) {
		return (Collection<TaskInstance>) jbpmTemplate.doInJbpm(new JbpmCallback() {

			public Object execute(JbpmContext jbpmCon) {
				ProcessInstance processInstance = jbpmCon.getProcessInstance(processInstanceId);
				Token rootToken = processInstance.getRootToken();
				Collection<Token> activeChildren = rootToken.getActiveChildren().values();
				Collection<Token> activeTokens = new HashSet<Token>();
				activeTokens.add(rootToken);
				activeTokens.addAll(activeChildren);
				Collection<TaskInstance> unfinishedTasks = new HashSet<TaskInstance>();
				for(Token token : activeTokens){			
					unfinishedTasks.addAll(processInstance.getTaskMgmtInstance().getUnfinishedTasks(token));
				}
				return unfinishedTasks;
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TaskInstance> retrieveUserTaskList(final String userName) {
		return (List<TaskInstance>) jbpmTemplate.doInJbpm(new JbpmCallback() {

			public Object execute(JbpmContext jbpmCon) {
				TaskMgmtSession taskSession = jbpmCon.getTaskMgmtSession();
				List<TaskInstance> tasks = taskSession.findTaskInstances(userName);
				return tasks;
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TaskInstance> retrieveUserTaskList(final List<String> actors) {
		return (List<TaskInstance>) jbpmTemplate.doInJbpm(new JbpmCallback() {

			public Object execute(JbpmContext jbpmCon) {
				TaskMgmtSession taskSession = jbpmCon.getTaskMgmtSession();
				List<TaskInstance> tasks = taskSession.findTaskInstances(actors);
				return tasks;
			}
		});
	}
	
	@Override
	public Long restart(final Long processInstanceId, final String actorId) {
		return (Long) jbpmTemplate.doInJbpm(new JbpmCallback() {

			public Object execute(JbpmContext jbpmCon) {
				
				ProcessInstance processInstance = jbpmCon.getProcessInstance(processInstanceId);
				Token rootToken = processInstance.getRootToken();
				
				org.hibernate.Query taskQuery = jbpmCon.getSession().getNamedQuery("TaskMgmtSession.deleteTaskInstanceForTaskInstanceId");
				org.hibernate.Query tokenQuery = jbpmCon.getSession().getNamedQuery("GraphSession.deleteTokenForTokenId");

				int entityCount = 0;
				
				deleteJobsForInstance(processInstance);

				Collection<TaskInstance> unfinishedTasks = (Collection<TaskInstance>) processInstance.getTaskMgmtInstance().getUnfinishedTasks(rootToken);
				if (unfinishedTasks.size() > 0) {
					for (TaskInstance unfinishedTask : unfinishedTasks) {
                        //DSCL-6729
						processInstance.getTaskMgmtInstance().getTaskInstances().remove(unfinishedTask);
						entityCount = taskQuery.setParameter("taskInstanceId", unfinishedTask.getId()).executeUpdate();
						if(logger.isDebugEnabled()) {
							logger.debug("Deleted " + entityCount + " tasks instances with id: " + unfinishedTask.getId() + " for processInstanceId: " + processInstanceId);
						}
					}

				} else if(rootToken.hasActiveChildren()) {
					Map<String, Token> children = rootToken.getActiveChildren();
					for(Map.Entry<String, Token> entry : children.entrySet()) {
						unfinishedTasks = (Collection<TaskInstance>) processInstance.getTaskMgmtInstance().getUnfinishedTasks(entry.getValue());
						if (unfinishedTasks.size() > 0 ){
							for (TaskInstance unfinishedTask : unfinishedTasks) {
								processInstance.getTaskMgmtInstance().getTaskInstances().remove(unfinishedTask);
							    entityCount = taskQuery.setParameter("taskInstanceId", unfinishedTask.getId()).executeUpdate();
								if(logger.isDebugEnabled()) {
									logger.debug("Deleted " + entityCount + " tasks instances with id: " + unfinishedTask.getId() + " for processInstanceId: " + processInstanceId);
								}
							}
						}
						Collection<TaskInstance> allTasks = (Collection<TaskInstance>) processInstance.getTaskMgmtInstance().getTaskInstances();
						for(TaskInstance taskInstance : allTasks) {
							if(taskInstance.getToken() != null && taskInstance.getToken().getId() ==  entry.getValue().getId()) {
								taskInstance.setToken(null);
								jbpmCon.save(taskInstance);
							}
						}
						processInstance.getRootToken().getChildren().remove(entry.getKey());
						entityCount = tokenQuery.setParameter("tokenId",  entry.getValue().getId()).executeUpdate();
					    if(logger.isDebugEnabled()) {
							logger.debug("Deleted " + entityCount + " tokens with id: " +  entry.getValue().getId() + " for processInstanceId " + processInstanceId);
						}
					}
				}
				
				setProcessVariable(processInstance.getId(),	PROCESS_INSTANCE_START_ACTOR_ID, actorId);
									
				TaskNode node = (TaskNode) processInstance.getProcessDefinition().getNode(Nodes.QUERY_ASSIGNMENT.getNodeName());

				if (node == null) {
					throw new RuntimeException("Node Query Assignmen does not exist. ");
				}

				processInstance.getRootToken().setNode(node);
				processInstance.getRootToken().setNodeEnter(new Date());
				jbpmCon.save(processInstance.getRootToken());

				jbpmCon.save(processInstance);
				
				createJobStartTask(processInstance, actorId);

				TaskMgmtInstance tmi = processInstance.getTaskMgmtInstance();
				Task task = tmi.getTaskMgmtDefinition().getTask(Activities.ASSIGN.getTaskName());
				task.getParent().setProcessDefinition(processInstance.getProcessDefinition());

				TaskInstance taskInstance = tmi.createTaskInstance(task, processInstance.getRootToken());
				taskInstance.getTaskMgmtInstance().setProcessInstance(processInstance);
				jbpmCon.save(taskInstance);
	
				return taskInstance.getId();
			}
		});
	}
	
	@Override
	public void deleteProcessInstance(final Long processInstanceId) {
		jbpmTemplate.doInJbpm(new JbpmCallback() {

			public Object execute(JbpmContext jbpmCon) {
				jbpmCon.getGraphSession().deleteProcessInstance(processInstanceId);
				return null;
			}
		});
	}
	
	private void createJobStartTask(final ProcessInstance processInstance, final String actorId) {
		jbpmTemplate.doInJbpm(new JbpmCallback() {

			@Override
			public Object execute(JbpmContext jbpmCon) {
				
				TaskMgmtInstance tmi = processInstance.getTaskMgmtInstance();
				Task task = tmi.getTaskMgmtDefinition().getTask(Activities.JOB_START.getTaskName());
				task.getParent().setProcessDefinition(processInstance.getProcessDefinition());
				//task.setProcessDefinition(processInstance.getProcessDefinition());

				TaskInstance taskInstance = tmi.createTaskInstance(task, processInstance.getRootToken());
				taskInstance.setActorId(actorId);
				taskInstance.end();
				taskInstance.getTaskMgmtInstance().setProcessInstance(processInstance);
				jbpmCon.getSession().save(taskInstance);
				return null;
			}
			
		});
	}
	
	private void deleteJobsForInstance(final ProcessInstance processInstance) {
		jbpmTemplate.doInJbpm(new JbpmCallback() {

			public Object execute(JbpmContext jbpmCon) {
				logger.info("Deleting jobs assigned to process instance id: " + processInstance.getId());
				jbpmCon.getJobSession().deleteJobsForProcessInstance(processInstance);
				return null;
			}
		});
	}

	private TaskInstance getTaskInstance(final ProcessInstance processInstance, final Token token) {
		return (TaskInstance) jbpmTemplate.doInJbpm(new JbpmCallback() {
			
			public Object execute(JbpmContext jbpmCon) {
				Token nonFinalToken = token != null ? token : processInstance.getRootToken();
				Collection<TaskInstance> tasks = processInstance.getTaskMgmtInstance().getUnfinishedTasks(nonFinalToken);
				if (tasks.size() > 1) {
					throw new UnsupportedOperationException("Processes with more than one task instance per node are unsupported. ");
				}
				TaskInstance task = tasks.size() == 1 ? tasks.iterator().next() : null;
				return task;
			}
		});
	}
	
	private boolean canTaskBeContinuedByCurrentUser(String userName, TaskInstance task) {
		String actorId = task.getActorId();
		if (actorId == null) {
			return true;
		}
		return actorId.equalsIgnoreCase(userName);
	}	
	
	private boolean canTaskBeEndedByCurrentUser(String userName, TaskInstance taskInstance) {
		String actorId = taskInstance.getActorId();
		if (actorId == null) {
			return true;
		}
		
		return (actorId.equalsIgnoreCase(userName) || actorId.equals("DSCL") || actorId.equals("DMGCO"));
	}

	@Override
	public void endProcessInstance(final Long processInstanceId) {
		jbpmTemplate.doInJbpm(new JbpmCallback() {

			public Object execute(JbpmContext jbpmCon) {
				ProcessInstance processInstance = jbpmCon.getProcessInstance(processInstanceId);
				
				//Cancel tasks first
				Collection<TaskInstance> allTasks = (Collection<TaskInstance>) processInstance.getTaskMgmtInstance().getTaskInstances();
				for(TaskInstance taskInstance : allTasks) {
					if (!taskInstance.hasEnded()) {
						taskInstance.setSignalling(false);
						taskInstance.cancel();
						jbpmCon.save(taskInstance);
					}
				}
				
				processInstance.end();
				return null;
			}
		});
	}
}