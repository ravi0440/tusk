package com.roche.dss.qtt.query.web.model;

import java.io.InputStream;
import java.io.Serializable;

/**
 * @author pruchnil
 */
public class StreamResultBean implements Serializable {
	private static final long serialVersionUID = 7443219929367161978L;

	private transient InputStream inputStream;
	private String contentType;
	private String contentDisposition;

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentDisposition() {
		return contentDisposition;
	}

	public void setContentDisposition(String contentDisposition) {
		this.contentDisposition = contentDisposition;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
}
