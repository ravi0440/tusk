package com.roche.dss.qtt.query.comms.dao;

import com.roche.dss.qtt.query.web.model.RepositorySearchParams;
import com.roche.dss.qtt.service.SearchResults;

public interface RepositorySearchDAO {
	public SearchResults search(RepositorySearchParams searchParams);
}
