package com.roche.dss.qtt.service.impl;

import com.roche.dss.qtt.eao.*;
import com.roche.dss.qtt.model.FollowupAttachment;
import com.roche.dss.qtt.model.LdocType;
import com.roche.dss.qtt.model.PreQueryPreparation;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryAssessment;
import com.roche.dss.qtt.model.QueryVerification;
import com.roche.dss.qtt.query.comms.dao.QueryDAO;
import com.roche.dss.qtt.query.comms.dto.PreparationDTO;
import com.roche.dss.qtt.query.comms.dto.QueryAssessmentDTO;
import com.roche.dss.qtt.query.comms.dto.QueryListDTO;
import com.roche.dss.qtt.query.comms.dto.QueryVerificationDTO;
import com.roche.dss.qtt.service.QueryNoteService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.workflow.WorkflowGlobalConstants;
import com.roche.dss.util.ApplicationUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import static com.roche.dss.qtt.query.web.action.ActionConstants.*;
import static com.roche.dss.qtt.service.impl.QueryNoteServiceImpl.NoteType;
import static com.roche.dss.qtt.utility.Activities.DMG_DATA_SEARCH;
import static com.roche.dss.qtt.utility.Activities.MEDICAL_INTERPRETATION_I;
import static com.roche.dss.qtt.utility.Activities.MEDICAL_INTERPRETATION_II;

@Service("queryService")
@Transactional
public class QueryServiceImpl implements QueryService {
    private static final Logger logger = LoggerFactory.getLogger(QueryServiceImpl.class);

    
    private QueryDAO queryDAO;
    
    
    @Autowired
    private QueryEAO queryEAO;
    
    @Autowired
    private QueryAssessmentEAO queryAssessmentEAO;
    
    @Autowired
    private QueryVerificationEAO queryVerificationEAO;

    @Autowired
    private DrugEAO drugEAO;

    @Autowired
    private AttachmentEAO attachmentEAO;

    @Autowired
    private LdocTypeEAO ldocTypeEAO;

    @Autowired
    private QueryNoteService noteService;

    
    @Resource
    public void setQueryDAO(QueryDAO queryDAO) {
        this.queryDAO = queryDAO;
    }
    
    @Override
	public void persist(Query entity) {
        queryEAO.persist(entity);
    }

    @Override
	public Query merge(Query entity) {
        return queryEAO.merge(entity);
    }

    @Override
	public void remove(Query entity) {
        queryEAO.remove(entity);
    }

    @Override
	public void flush() {
        queryEAO.flush();
    }

    @Override
	public Query find(Long id) {
        return queryEAO.find(id);
    }

    @Override
    public boolean checkHaLegalNoDocs(long queryId) {
        return queryEAO.checkHaLegalNoDocs(queryId);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<QueryListDTO> getQueryList(String email, String column, String order) {
        List<Object[]> result = queryEAO.getQueryList(email, column, order);
        List<QueryListDTO> list = new ArrayList<QueryListDTO>();
        for (Object[] values : result) {
            QueryListDTO queryDTO = new QueryListDTO();
            queryDTO.setQuerySeq((Long) values[0]);
            queryDTO.setQueryNumber(String.valueOf(values[1]));
            queryDTO.setQueryType(String.valueOf(values[2]));
            queryDTO.setInitiationDate((DateTime) values[3]);
            queryDTO.setResponseDate((DateTime) values[4]);
            queryDTO.setDrugName(String.valueOf(values[5]));
            queryDTO.setUrgency(ApplicationUtils.convertToBoolean(String.valueOf(values[6])));
            queryDTO.setStatusCode(ApplicationUtils.getStatusCodeDescription(String.valueOf(values[7])));
            queryDTO.setQueryLabel(String.valueOf(values[8]));
            queryDTO.setRequesterEmail(String.valueOf(values[9]));
            list.add(queryDTO);
        }
        return list;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Query getPreviewQueryDetails(long querySeq) {
        Query query = queryEAO.find(querySeq);
        if (!query.isAllDrugs()) {
            query.setDrugForPreview(drugEAO.findDrugForQueryPreview(querySeq));
        }
        query.setPreparation(setPreQueryPreparations(query));
        return query;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public QueryAssessmentDTO getQueryAssessment(long querySeq, String followupNumber) {
        QueryAssessment queryAssessment = queryAssessmentEAO.findByQueryIdAndFollowupNumber(querySeq,followupNumber);
       
        return queryAssessment!=null?new QueryAssessmentDTO(queryAssessment):null;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public QueryVerificationDTO getQueryVerification(QueryVerification.RECORD_TYPE type, long querySeq, String followupNumber) {
    	 QueryVerification result= queryVerificationEAO.findByRecordTypeAndQueryId(type, querySeq, followupNumber);
        return result!=null?new QueryVerificationDTO(result):null;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public void removeQueryVerification(QueryVerification.RECORD_TYPE type, long querySeq,String followupNumber) {
    	 QueryVerification result= queryVerificationEAO.findByRecordTypeAndQueryId(type, querySeq,followupNumber);
    	 if(result!=null){
    		 queryVerificationEAO.remove(result);
    		 queryVerificationEAO.flush();
    	 }
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Query getClosedQueryDetails(long querySeq, boolean affiliate) {
        Query query = queryEAO.find(querySeq);
        query.filterAttachmentsForDisplay(affiliate);
        for (FollowupAttachment followupAttachment : query.getFollowupAttachments()) {
            followupAttachment.setAttachments(attachmentEAO.findForClosedQuery(query.getQuerySeq(), followupAttachment.getFollowupNumber()));
            followupAttachment.filterAttachmentsForDisplay(affiliate);
        }
        return query;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateQueryLabelRoute(long queryId, String queryLabel, String route) {
        Query query = find(queryId);
        query.setQueryLabel(queryLabel);
        if(StringUtils.isNotEmpty(route)){
            query.setWorkflowRouteType(route);
        }
        merge(query);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateQueryExpiryDate(long queryId, DateTime expiryDate, boolean affiliateAccess) {
        Query query = find(queryId);
        query.setExpiryDate(expiryDate);
        query.setAffiliateAccess(ApplicationUtils.booleanToChar(affiliateAccess));
        merge(query);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Set<LdocType> getLabellingDocTypes(long queryId) {
        Query query = find(queryId);
        return query.getLabelingDocTypes();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateSummaryResponseAppend(long queryId, DateTime expiryDate, String summary, boolean affiliateAccess, boolean ldocUpdate, String oldLabellingDocId, String newLabellingDocId) {
        Query query = find(queryId);
        query.setExpiryDate(expiryDate);
        query.setQuerySummary(summary);
        query.setQuerySummaryUpper(summary.toUpperCase());
        query.setAffiliateAccess(ApplicationUtils.booleanToChar(affiliateAccess));
        if (ldocUpdate) {
            LdocType labbellingDocType = null;
            if (oldLabellingDocId != null) {
                labbellingDocType = ldocTypeEAO.find(Long.valueOf(oldLabellingDocId));
                query.getLabelingDocTypes().remove(labbellingDocType);
            }
            labbellingDocType = ldocTypeEAO.find(Long.valueOf(newLabellingDocId));
            query.getLabelingDocTypes().add(labbellingDocType);
        }
        merge(query);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void setAccessKey(long queryId, String nextID) {
        Query query = find(queryId);
        query.setAccessKey(nextID);
        merge(query);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Query getAttachmentActivationList(long queryId, boolean dscl, String loggedUserName) {
        Query query = find(queryId);
        query.filterAttachmentsForActivation(dscl, loggedUserName);
        return query;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isFinalDocExists(long queryId, Long processInstanceId) {
        // in case of amending an unsubmitted query, when processInstanceId is null yet!
        if (processInstanceId==null) {
            return false;
        }
        DateTime latestQueryStartDate = queryEAO.getLatestQueryStartDate(processInstanceId);
        DateTime latestFinalDocDate = attachmentEAO.getLatestFinalDocCreationDate(queryId);
        if ((latestQueryStartDate == null) || (latestFinalDocDate == null)) {
            return false;
        } else if (latestFinalDocDate.isAfter(latestQueryStartDate)) {
            return true;
        }
        return false;
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isFinalDocAttachedInCurrentInteraction(long queryId, Long processInstanceId) {        
        if (processInstanceId==null) {
            return false;
        }
        DateTime latestQueryStartDate = queryEAO.getStartDateOfCurrentInteration(processInstanceId);
        DateTime latestFinalDocDate = attachmentEAO.getLatestFinalDocCreationDate(queryId);
        if ((latestQueryStartDate == null) || (latestFinalDocDate == null)) {
            return false;
        } else if (latestFinalDocDate.isAfter(latestQueryStartDate)) {
            return true;
        }
        return false;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean checkHasStatus(long queryId, String status) {
        return queryEAO.checkHasStatus(queryId, status);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Query changeUrgencyFlag(long queryId) {
        Query query = find(queryId);
        if (query.getUrgencyFlag() == 'Y') {
            query.setUrgencyFlag('N');
        } else {
            query.setUrgencyFlag('Y');
        }
        merge(query);
        return query;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateAlertFlag(long queryId, boolean b) {
        Query query = find(queryId);
        query.setAlertFlag(ApplicationUtils.booleanToChar(b));
        merge(query);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Query> findOpenQueryList() {
        List<Query> openQueryList = queryEAO.findOpenQueryList();
        return openQueryList;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateResponseDate(long queryId, DateTime date, String explanation, boolean affiliate,String author) {
        Query query = find(queryId);
        query.setCurrentDueDate(date);
        merge(query);
        noteService.saveQueryNote(0, queryId, new StringBuilder("").append(NoteType.RESPDATE.getMessage()).append(explanation).toString(), affiliate, author);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void updateDMGResponseDate(long queryId, DateTime date, String explanation, boolean affiliate, String author) {
        Query query = find(queryId);
        query.setDmgDueDate(date);
        merge(query);
        noteService.saveQueryNote(0, queryId, new StringBuilder("").append(NoteType.DMGRESPDATE.getMessage()).append(explanation).toString(), affiliate, author);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Query> findOpenQueryListByCountry(String countryCode) {
        return queryEAO.findOpenQueryListByCountry(countryCode);
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Query> getInitiateList() {
    	return queryEAO.getInitiateList();
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Query> findClosedQueryList() {
        return queryEAO.findClosedQueryList();
    }
    
    private PreparationDTO setPreQueryPreparations(Query query) {
        PreparationDTO preparation = new PreparationDTO();
        for (PreQueryPreparation prePreparation : query.getPreQueryPreparations()) {
            switch (prePreparation.getId().getPqpTypeId()) {
                case REPOSITORY:
                    preparation.setRepository(YES);
                    break;
                case PSURS:
                    preparation.setPsur(YES);
                    break;
                case CDS:
                    preparation.setCds(YES);
                    break;
                case ISSUE_WORKUPS:
                    preparation.setIssue(YES);
                    break;
                case LITERATURE:
                    preparation.setLiterature(YES);
                    break;
                case OTHERS:
                    preparation.setOthers(YES);
                    preparation.setOthersComment(prePreparation.getOtherComment());
                    break;
                default:
                    break;
            }
        }
        return preparation;
    }

	@Override
	public void addQueryAssessment(QueryAssessmentDTO assessment, long queryId, String followupNumber) {
		queryDAO.addQueryAssesment(assessment, queryId,followupNumber);
		
	}
	
	@Override
	public void updateQueryAssessment(QueryAssessmentDTO assessment, long queryId, String followupNumber) {
		queryDAO.updateQueryAssesment(assessment, queryId,followupNumber);
		
	}
	
	
	@Override
	public void addQueryVerification(QueryVerificationDTO queryVerification, long queryId,String followupNumber) {
		queryDAO.addQueryVerification(queryVerification, queryId,followupNumber);
		
	}
	
	@Override
	public void updateQueryVerification(QueryVerificationDTO queryVerification, long queryId,String followupNumber) {
		queryDAO.updateQueryVerification(queryVerification, queryId,followupNumber);
		
	}

	@Override
	public void assignVerifyPerson(String person, long queryId) {
		Query query = find(queryId);
        query.setVerifyPerson(person);
        merge(query);
	}
	
	@Override
	public void cleanVerifyPerson(long queryId) {
		Query query = find(queryId);
        query.setVerifyPerson(null);
        merge(query);
	}
	
	@Override
	public void setInformEUQPPV(boolean flag, long queryId) {
		Query query = find(queryId);
        query.setInformEuQPPV(flag ? 'Y' : 'N');
        merge(query);
	}


    @Override
    public String findActorIdFromDmgOrMiTask(Query query) {
    	if(query.getProcessInstance()==null)return null;
        Set<TaskInstance> taskInstances = query.getProcessInstance().getTaskInstances();

        for (TaskInstance taskInstance : taskInstances) {
            if (DMG_DATA_SEARCH.getTaskName().equals(taskInstance.getName()) || MEDICAL_INTERPRETATION_I.getTaskName().equals(taskInstance.getName())) {
                return taskInstance.getActorId();
            }
        }

        return "";
    }

    @Override
    public String findActorIdFromMiTask(Query query) {
        Set<TaskInstance> taskInstances = query.getProcessInstance().getTaskInstances();

        for (TaskInstance taskInstance : taskInstances) {
            if (MEDICAL_INTERPRETATION_I.getTaskName().equals(taskInstance.getName())) {
                return taskInstance.getActorId();
            }
        }

        return "";
    }

    @Override
    public boolean checkIfQueryRequestedDmgDataSearch(Query query) {
        Set<TaskInstance> taskInstances = query.getProcessInstance().getTaskInstances();

        for (TaskInstance taskInstance : taskInstances) {
            if (WorkflowGlobalConstants.WF_ROUTE_B.equals(query.getWorkflowRouteType()) &&
                    DMG_DATA_SEARCH.getTaskName().equals(taskInstance.getName())) {
                return true;
            }
            break;
        }

        return false;
    }

}
