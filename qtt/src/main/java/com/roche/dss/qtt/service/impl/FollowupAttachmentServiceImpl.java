package com.roche.dss.qtt.service.impl;

import com.roche.dss.qtt.eao.FollowupAttachmentEAO;
import com.roche.dss.qtt.model.FollowupAttachment;
import com.roche.dss.qtt.model.FollowupAttachmentId;
import com.roche.dss.qtt.service.FollowupAttachmentService;
import com.roche.dss.util.ApplicationUtils;
import java.util.Date;

import static com.roche.dss.qtt.utility.QueryStatusCodes.CLOSED;
import static com.roche.dss.qtt.utility.QueryStatusCodes.FOLLOWUP_ATT;

/**
 * User: pruchnil
 */
@Service("followupAttachmentService")
@Transactional
public class FollowupAttachmentServiceImpl implements FollowupAttachmentService {
    private static final Logger logger = LoggerFactory.getLogger(FollowupAttachmentServiceImpl.class);

    @Autowired
    private FollowupAttachmentEAO followupAttachmentEAO;

    @Override
    public void persist(FollowupAttachment entity) {
        followupAttachmentEAO.persist(entity);
    }

    @Override
    public FollowupAttachment merge(FollowupAttachment entity) {
        return followupAttachmentEAO.merge(entity);
    }

    @Override
    public void flush() {
        followupAttachmentEAO.flush();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void openFollowupAttachment(long queryId) {
        long followupSeq = followupAttachmentEAO.getMaxFollowupSeq(queryId);
        FollowupAttachmentId id = new FollowupAttachmentId(followupSeq + 1, queryId);
        FollowupAttachment attachment = new FollowupAttachment(id, new StringBuilder("ATT").append(ApplicationUtils.addZeros(id.getFollowupSeq())).toString(), FOLLOWUP_ATT.getCode(), new DateTime(new Date()));
        persist(attachment);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void closeFollowupAttachment(long queryId) {
        FollowupAttachment attachment = followupAttachmentEAO.findOpenByQuerySeq(queryId);
        attachment.setStatusCode(CLOSED.getCode());
        merge(attachment);
    }
}
