package com.roche.dss.qtt.query.comms.dao;

import com.roche.dss.qtt.query.comms.dto.ReportDTO;
import com.roche.dss.qtt.query.web.model.ReportCriteria;

import java.util.List;

/**
 * This DAO retrieves data for report.
 *
 * User: pruchnil
 */
public interface ReportDAO {

    List<ReportDTO> getReportData(ReportCriteria reportCriteria);

}
