package com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.PerformanceMetric;

public interface PerformanceMetricEAO extends QueryRelatedEAO<PerformanceMetric>{
	
}