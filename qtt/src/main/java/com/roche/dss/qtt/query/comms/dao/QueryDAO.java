package com.roche.dss.qtt.query.comms.dao;

import com.roche.dss.qtt.model.Audit;
import com.roche.dss.qtt.model.Case;
import com.roche.dss.qtt.model.CaseOformat;
import com.roche.dss.qtt.model.ClinicalTrial;
import com.roche.dss.qtt.model.DataSearch;
import com.roche.dss.qtt.model.Drug;
import com.roche.dss.qtt.model.Manufacturing;
import com.roche.dss.qtt.model.PerformanceMetric;
import com.roche.dss.qtt.model.Pregnancy;
import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.query.comms.dto.PartialQueryDTO;
import com.roche.dss.qtt.query.comms.dto.QueryAssessmentDTO;
import com.roche.dss.qtt.query.comms.dto.QueryDTO;
import com.roche.dss.qtt.query.comms.dto.QueryDescDTO;
import com.roche.dss.qtt.query.comms.dto.QueryVerificationDTO;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public interface QueryDAO {
    List<PartialQueryDTO> getPartialQueries(String email, String status);

    void deleteOldQueryType(int queryId, QueryType.Code oldQueryTypeCode);

    void updateQRF(QueryDTO query, int queryId, List<Integer> idsAttachmentsToRemove);

    int createQRF(QueryDTO queryDto);

    QueryDTO openQuery(int queryId, String emailAddress, boolean review);

    void addDrugEvent(QueryDescDTO queryDescDto, List caseDtoList, String aeTerm, int queryNumber, String addComment);

    void updateDrugEvent(QueryDescDTO queryDescDto, List caseDtoList, String aeTerm, int queryNumber, String addComment);

    void updateStatus(int queryNumber, String status);

    void createAttachments(List attachments, int queryId);

    void updateDrugInteraction(QueryDescDTO queryDescDto, List caseDtoList, int queryNumber, String addComment, List drugDtoList);

    void addDrugInteraction(QueryDescDTO queryDescDto, List caseDtoList, int queryNumber, String addComment, List drugDtoList);

    void updateManufacturing(QueryDescDTO queryDescDTO, Manufacturing manufacturing, ClinicalTrial clinical,
    		String aeTerm, String specialRequest, Integer queryNumber);

    void addManufacturing(QueryDescDTO queryDescDTO, Manufacturing manufacturing, ClinicalTrial clinical,
    		String aeTerm, String specialRequest, Integer queryNumber);

    void updateExternalAudit(QueryDescDTO queryDescDto, Audit audit, ClinicalTrial clinical, String specialRequest, int queryNumber);
//
    void updateCaseClarification(List caseDtoList, int queryNumber, String addComment, int outputId, String others);

    void addCaseClarification(List caseDtoList, int queryNumber, String addComment, int outputId, String others);

    void addINDUpdates(String indNumber, int queryNumber);

    void updateLiterature(QueryDescDTO queryDescDto, int queryNumber, String aeTerm);

    void addLiterature(QueryDescDTO queryDescDto, int queryNumber, String aeTerm);

    void addPerformanceMetrics(PerformanceMetric perf, int queryNumber);

    void updatePerformanceMetrics(PerformanceMetric perf, int queryNumber);

    void updateSignalDetection(QueryDescDTO queryDescDto, int queryNumber, String aeTerm);

    void addSignalDetection(QueryDescDTO queryDescDto, int queryNumber, String aeTerm);

    void addExternalAudit(QueryDescDTO queryDescDto, Audit audit, ClinicalTrial clinical, String specialRequest, int queryNumber);

    public void updateQueryDesc(QueryDescDTO queryDesc, int queryNumber);

    public void addQueryDesc(QueryDescDTO queryDesc, int queryNumber);
    public void addQueryAssesment(QueryAssessmentDTO queryAssessment, long queryNumber, String followupNumber);
    public void updateQueryAssesment(QueryAssessmentDTO queryAssessment, long queryNumber, String followupNumber);
    public void addQueryVerification(QueryVerificationDTO queryVerification, long queryNumber, String followupNumber);
    public void updateQueryVerification(QueryVerificationDTO queryVerification, long queryNumber, String followupNumber);

    void createFollowup(int queryId);

    public void updateCase(List caseDTO, int queryNumber);

	void updateDataSearch(QueryDescDTO queryDesc, DataSearch dataSearch,
			ClinicalTrial clinicalTrial, CaseOformat caseOformat, String other,
			String meddraTerm, String additionalComments, Integer queryNumber);

	void updateMedical(QueryDescDTO queryDescDTO, Case caze,
			Pregnancy pregnancy, List<Drug> drugs, String comments,
			Integer queryNumber);

    List<String> getPossibleEmailDomains();
}
