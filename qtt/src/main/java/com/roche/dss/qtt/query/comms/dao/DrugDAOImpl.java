package com.roche.dss.qtt.query.comms.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.roche.dss.qtt.query.comms.dao.impl.JDBCTemplateDAO;
import com.roche.dss.qtt.query.comms.dto.DrugDTO;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
@Repository
public class DrugDAOImpl extends JDBCTemplateDAO implements DrugDAO {
   
    @Value("${ext.table.drugs}")
    private String drugTab;

    @Override
    public List<DrugDTO> getDrugNames() {
        String queryStr = "SELECT distinct drug_retrieval_name FROM "+drugTab;
        return jdbcTemplate.query(queryStr, new DrugDTORowMapper());
    }

    @Override
    public List<DrugDTO> getGenDrugNames() {
        String queryStr = "SELECT distinct inn_generic_name FROM "+drugTab;
        return jdbcTemplate.query(queryStr, new GenDrugDTORowMapper());
    }

     class DrugDTORowMapper implements RowMapper<DrugDTO> {

        @Override
        public DrugDTO mapRow(ResultSet resultSet, int i) throws SQLException {
            DrugDTO drugDTO = new DrugDTO();
           // drugDTO.setGenericName(resultSet.getString("inn_generic_name"));
            drugDTO.setRetrName(resultSet.getString("drug_retrieval_name"));
            return drugDTO;
        }
    }

     class GenDrugDTORowMapper implements RowMapper<DrugDTO> {

        @Override
        public DrugDTO mapRow(ResultSet resultSet, int i) throws SQLException {
            DrugDTO drugDTO = new DrugDTO();
            drugDTO.setGenericName(resultSet.getString("inn_generic_name"));
           // drugDTO.setRetrName(resultSet.getString("drug_retrieval_name"));
            return drugDTO;
        }
    }

}
