package com.roche.dss.qtt.eao.impl;

import com.roche.dss.qtt.eao.AbstractQueryRelatedEAO;
import com.roche.dss.qtt.eao.NonAffiliateNoteEAO;
import com.roche.dss.qtt.model.NonAffiliateNote;

@Repository("nonAffiliateNoteEAO")
public class NonAffiliateNoteEAOImpl extends AbstractQueryRelatedEAO<NonAffiliateNote> implements NonAffiliateNoteEAO {
}
