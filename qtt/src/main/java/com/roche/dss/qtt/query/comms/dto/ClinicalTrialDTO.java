/* 
====================================================================
  $Header$
  @author $Author: komisarp $
  @version $Revision: 2453 $ $Date: 2012-06-27 16:07:46 +0200 (Śr, 27 cze 2012) $

====================================================================
*/
package com.roche.dss.qtt.query.comms.dto;


public class ClinicalTrialDTO  {
	
	private String protocolNumber;
	private String crtnNumber;
	private String patientNumber;
	private String caseSelection;
	private int outputFormatId;
	private String outputFormatDesc;
	

    /**
     * @return
     */
    public String getCaseSelection() {
        return caseSelection;
    }

    /**
     * @return
     */
    public String getCrtnNumber() {
        return crtnNumber;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((caseSelection == null) ? 0 : caseSelection.hashCode());
		result = prime * result
				+ ((crtnNumber == null) ? 0 : crtnNumber.hashCode());
		result = prime
				* result
				+ ((outputFormatDesc == null) ? 0 : outputFormatDesc.hashCode());
		result = prime * result + outputFormatId;
		result = prime * result
				+ ((patientNumber == null) ? 0 : patientNumber.hashCode());
		result = prime * result
				+ ((protocolNumber == null) ? 0 : protocolNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ClinicalTrialDTO))
			return false;
		ClinicalTrialDTO other = (ClinicalTrialDTO) obj;
		if (caseSelection == null) {
			if (other.caseSelection != null)
				return false;
		} else if (!caseSelection.equals(other.caseSelection))
			return false;
		if (crtnNumber == null) {
			if (other.crtnNumber != null)
				return false;
		} else if (!crtnNumber.equals(other.crtnNumber))
			return false;
		if (outputFormatDesc == null) {
			if (other.outputFormatDesc != null)
				return false;
		} else if (!outputFormatDesc.equals(other.outputFormatDesc))
			return false;
		if (outputFormatId != other.outputFormatId)
			return false;
		if (patientNumber == null) {
			if (other.patientNumber != null)
				return false;
		} else if (!patientNumber.equals(other.patientNumber))
			return false;
		if (protocolNumber == null) {
			if (other.protocolNumber != null)
				return false;
		} else if (!protocolNumber.equals(other.protocolNumber))
			return false;
		return true;
	}

	/**
     * @return
     */
    public int getOutputFormatId() {
        return outputFormatId;
    }

    /**
     * @return
     */
    public String getPatientNumber() {
        return patientNumber;
    }

    /**
     * @return
     */
    public String getProtocolNumber() {
        return protocolNumber;
    }

    /**
     * @param string
     */
    public void setCaseSelection(String string) {
        caseSelection = string;
    }

    /**
     * @param string
     */
    public void setCrtnNumber(String string) {
        crtnNumber = string;
    }

    /**
     * @param i
     */
    public void setOutputFormatId(int i) {
        outputFormatId = i;
    }

    /**
     * @param string
     */
    public void setPatientNumber(String string) {
        patientNumber = string;
    }

    /**
     * @param string
     */
    public void setProtocolNumber(String string) {
        protocolNumber = string;
    }

    /**
     * @return
     */
    public String getOutputFormatDesc() {
        return outputFormatDesc;
    }

    /**
     * @param string
     */
    public void setOutputFormatDesc(String string) {
        outputFormatDesc = string;
    }

}
