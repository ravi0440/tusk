package com.roche.dss.qtt.security.utils.storeprocedures;

/**
 * @author zerkowsm
 *
 */
public interface DSSStoreProceduresConstants {

	public static final String USER_SERVICE_GET_USER_OUT_PARAM = "l_getUser";
	public static final String USER_SERVICE_GET_USERS_OUT_PARAM = "l_getUsers";
	public static final String SECURITY_SERVICE_GET_USER_PERMISSIONS_OUT_PARAM = "l_getUserPerm";
    
	public static final String ROLE_PREFIX = "ROLE_";

}