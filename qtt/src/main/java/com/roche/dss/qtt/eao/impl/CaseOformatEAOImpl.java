package com.roche.dss.qtt.eao.impl;

import java.util.List;

import com.roche.dss.qtt.eao.AbstractBaseEAO;
import com.roche.dss.qtt.eao.CaseOformatEAO;
import com.roche.dss.qtt.model.CaseOformat;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
@Repository("caseOformatEAO")
public class CaseOformatEAOImpl  extends AbstractBaseEAO<CaseOformat> implements CaseOformatEAO {

	@Override
	public List<CaseOformat> getAllActive() {
		return entityManager.createNamedQuery("list.activeCaseOformats", CaseOformat.class).getResultList();
	}

}
