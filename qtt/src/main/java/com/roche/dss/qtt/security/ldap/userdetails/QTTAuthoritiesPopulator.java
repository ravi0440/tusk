package com.roche.dss.qtt.security.ldap.userdetails;

import com.roche.dss.qtt.security.model.DSSPermission;
import com.roche.dss.qtt.security.model.DSSUser;
import com.roche.dss.qtt.security.utils.storeprocedures.DSSStoreProceduresConstants;
import com.roche.dss.qtt.security.utils.storeprocedures.permission.DSSUserPermissionStoredProcedure;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * @author zerkowsm
 *
 */
public class QTTAuthoritiesPopulator implements AuthoritiesPopulator, DSSStoreProceduresConstants {
	
    private static final Log logger = LogFactory.getLog(QTTAuthoritiesPopulator.class);
	
	private String dssApplicationCode;
	
	private String dssPortalCode;
	
	private String QTTPortalPermissionCode;
	
    private DataSource securityDataSource;
	
    private boolean convertToUpperCase = true;

	@SuppressWarnings("unchecked")
	@Override
	public Collection<GrantedAuthority> getGrantedAuthorities(DSSUser userData, String username) {
		
        if (logger.isDebugEnabled()) {
            logger.debug("Getting authorities for user: " + username);
        }
		
		Map<String, Object> permissions = getUserPermissions(userData.getEmployeeId(), null);
		
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		String role = null;
		
		if(permissions.get(SECURITY_SERVICE_GET_USER_PERMISSIONS_OUT_PARAM) == null || ((ArrayList<DSSPermission>)permissions.get(SECURITY_SERVICE_GET_USER_PERMISSIONS_OUT_PARAM)).isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("Getting extra authorities for user: " + username);
			}
			
	        permissions = getUserPermissions(userData.getEmployeeId(), dssPortalCode);
	        
	        try{
				for(DSSPermission perm : (ArrayList<DSSPermission>)permissions.get(SECURITY_SERVICE_GET_USER_PERMISSIONS_OUT_PARAM)){
					if(QTTPortalPermissionCode.equals(perm.getPermissionCode())) {
						role = perm.getPermissionCode();
						if (convertToUpperCase) {
							role = role.toUpperCase();
						}
						authorities.add(new GrantedAuthorityImpl(ROLE_PREFIX + role));
					}
				}
				return authorities;
			}catch (Exception e) {
				throw new AuthenticationServiceException("Cannot retrieve extra permissions for user: " + username, e);
			}			
		} else{			
			try{
				for(DSSPermission perm : (ArrayList<DSSPermission>)permissions.get(SECURITY_SERVICE_GET_USER_PERMISSIONS_OUT_PARAM)){
					role = perm.getPermissionCode();
					if (convertToUpperCase) {
						role = role.toUpperCase();
					}
					authorities.add(new GrantedAuthorityImpl(ROLE_PREFIX + role));
				}
				return authorities;
			}catch (Exception e) {
				throw new AuthenticationServiceException("Cannot retrieve permissions for user: " + username, e);
			}
		}
	}
	
	public Map<String, Object> getUserPermissions(Long employeeId, String dssApplicationCode){
		DSSUserPermissionStoredProcedure sproc = new DSSUserPermissionStoredProcedure(securityDataSource);
        Map<String, Object> results = sproc.execute(employeeId, dssApplicationCode != null ? dssApplicationCode : this.dssApplicationCode);
        return results;
    }	
		
	public void setDssApplicationCode(String dssApplicationCode) {
		this.dssApplicationCode = dssApplicationCode;
	}
				
    public void setDssPortalCode(String dssPortalCode) {
		this.dssPortalCode = dssPortalCode;
	}

	public void setQTTPortalPermitionCode(String qTTPortalPermitionCode) {
		QTTPortalPermissionCode = qTTPortalPermitionCode;
	}

	public void setDataSource(DataSource securityDataSource) {
        this.securityDataSource = securityDataSource;
    }
	
}