package com.roche.dss.qtt.query.web.menu;

/**
 * @author zerkowsm
 * 
 */
public enum MenuItem {
	HOME("Home", "RetrieveQueryTaskList.action"), 
	QRF("QRF", "QrfAction.action"),
	QUERY_LIST("Query List", "OwnQueryList.action"),
	QUERY_LIST_UESERS("Query List Users", "QueryListMenu.action"),
	INITIATE("Initiate", "InitiateList.action"),
	SEARCH("Search", "RepositorySearch.action"),
	QUICK_SEARCH("Quick Search", "QuickSearch.action"),
	REPORTS("Reports", "ReportMenu.action"),
	HELP("Help", "http://rochewiki.roche.com/confluence/display/QTTUG/Home\" target=\"Help"),
	ABOUT("About", "About.action");

	private String value;
	private String link;

	private MenuItem(String value, String link) {
		this.value = value;
		this.link = link;
	}

	public String getValue() {
		return value;
	}

	public String getLink() {
		return link;
	}

}