package com.roche.dss.qtt.eao;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

/**
 * @author zerkowsm
 *
 */
public abstract class AbstractBaseEAO<T> implements BaseEAO<T> {

	protected EntityManager entityManager;
	
	protected Class<T> entityClass;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	@PersistenceContext
	@SuppressWarnings("unchecked")
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];		
	}

	@Override
	public void persist(T entity) {
		getEntityManager().persist(entity);
	}

	@Override
	public void persist(Collection<T> entities) {
		for (T entity : entities) {
			getEntityManager().persist(entity);
		}
	}
	
	@Override
	public T merge(T entity) {
		return getEntityManager().merge(entity);
	}
	
	@Override
	public void remove(T entity) {
		getEntityManager().remove(entity);
	}

	@Override
	public void flush() {
		getEntityManager().flush();
	}

	@Override
	public void clear() {
		getEntityManager().clear();
	}
	
	@Override
	public T find(Long id) {
		return getEntityManager().find(entityClass, id);
	}

	@Override
	public T getReference(Long id) {
		return getEntityManager().getReference(entityClass, id);
	}

	@Override
	public T get(Long id) {
		T entity = find(id);
		if (entity == null) {
			throw new EntityNotFoundException("'" + entityClass.getSimpleName()
					+ "' object with id '" + id + "' not found. ");
		}
		return entity;
	}

    @Override
    public List<T> getAll() {
        return getEntityManager().createQuery("from "+entityClass.getName(), entityClass).getResultList();
    }

    @Override
    public T getByIdObj(Object id) {
        return getEntityManager().find(entityClass, id);
    }
}
