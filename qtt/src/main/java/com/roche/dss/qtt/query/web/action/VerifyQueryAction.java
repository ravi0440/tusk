package com.roche.dss.qtt.query.web.action;

import java.util.Date;
import java.util.Hashtable;
import java.util.Set;

import javax.annotation.Resource;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import com.roche.dss.qtt.eao.QueryEAO;
import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.QueryVerification;
import com.roche.dss.qtt.query.comms.dto.QueryAssessmentDTO;
import com.roche.dss.qtt.query.comms.dto.QueryVerificationDTO;

import com.roche.dss.qtt.service.AttachmentService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.workflow.WorkflowService;
import com.roche.dss.qtt.utility.WorkflowRoute;
import com.roche.dss.qtt.security.web.SecurityConstants;

/**
 * @author zerkowsm
 */
@Conversion()
public class VerifyQueryAction extends TaskBaseAction {
	private static final Logger logger = LoggerFactory
			.getLogger(VerifyQueryAction.class);
	private static final long serialVersionUID = 519986614053742613L;

	private Query query;
	private TaskInstance taskInstance;
	private long queryId;
	private long taskId;
	private QueryService queryService;
	private WorkflowService workflowService;

	// form fields
	private boolean requirementsUnderstood;
	private boolean clarificationRequested;
	private boolean assumptionsDocumented;
	private String furtherRequirements;
	private String buildType;
	private boolean correctParametersPassed;
	private boolean codeReviewPassed;
	private boolean checkPassed;
	private boolean resultsVerified;
	private String furtherTestReference;
	private boolean resultsAppropriatelyLabeled;
	private boolean resultsStored;
	private boolean searchCriteriaStated;

	private Date date;
	
	private String password;
	
	private AttachmentService attachmentService;
	
	 @Resource
	    public void setAttachmentService(AttachmentService attachmentService) {
	        this.attachmentService = attachmentService;
	    }

	@Resource
	public void setQueryService(QueryService queryService) {
		this.queryService = queryService;
	}

	@Resource
	public void setWorkflowService(WorkflowService workflowService) {
		this.workflowService = workflowService;
	}

	@SkipValidation
	public String execute() {
		query = queryService.getClosedQueryDetails(queryId, isAffiliate());
		taskInstance = workflowService.getTaskInstance(taskId);
		
		// fetch verification record if exist
		
		QueryVerificationDTO dto;
		if(query.getVerifyPerson()!=null&&query.getVerifyPerson().equals(getLoggedUserName())){
			dto=queryService.getQueryVerification(QueryVerification.RECORD_TYPE.FILLED_BY_VERIFIER, queryId,query.getFollowupNumber());
		}else{
			dto=queryService.getQueryVerification(QueryVerification.RECORD_TYPE.FILLED_BY_OWNER, queryId,query.getFollowupNumber());
		}
		
		if(dto!=null){
			 requirementsUnderstood=dto.getRequirementsUnderstood().equals("Y");
			 clarificationRequested=dto.getClarificationRequested().equals("Y");
			 assumptionsDocumented=dto.getAssumptionsDocumented().equals("Y");
			 furtherRequirements=dto.getFurtherRequirements();
			 buildType=dto.getBuildType();
			 correctParametersPassed=dto.getCodeReviewPassed().equals("Y");
			 codeReviewPassed=dto.getCodeReviewPassed().equals("Y");
			 checkPassed=dto.getCheckPassed().equals("Y");
			 resultsVerified=dto.getResultsVerified().equals("Y");
			 furtherTestReference=dto.getFurtherTestReference();
			 resultsAppropriatelyLabeled=dto.getResultsAppropriatelyLabeled().equals("Y");
			 resultsStored=dto.getResultsStored().equals("Y");
			 searchCriteriaStated=dto.getSearchCriteriaStated().equals("Y");
		}
		date=new Date();
		
		
		return SUCCESS;
	}

	public String submitQueryVerification() {

		QueryVerificationDTO dto = new QueryVerificationDTO();
		dto.setRequirementsUnderstood(requirementsUnderstood ? "Y" : "N");
		dto.setClarificationRequested(clarificationRequested ? "Y" : "N");
		dto.setAssumptionsDocumented(assumptionsDocumented ? "Y" : "N");
		dto.setFurtherRequirements(furtherRequirements);
		dto.setBuildType(buildType);
		dto.setCorrectParametersPassed(correctParametersPassed ? "Y" : "N");
		dto.setCodeReviewPassed(codeReviewPassed ? "Y" : "N");
		dto.setCheckPassed(checkPassed ? "Y" : "N");
		dto.setResultsVerified(resultsVerified ? "Y" : "N");
		dto.setFurtherTestReference(furtherTestReference);
		dto.setResultsAppropriatelyLabeled(resultsAppropriatelyLabeled ? "Y"
				: "N");
		dto.setResultsStored(resultsStored ? "Y" : "N");
		dto.setSearchCriteriaStated(searchCriteriaStated ? "Y" : "N");
		dto.setFilledBy(securityService.getLoggedUserName());
		dto.setDateCompleted(date);

		
		// if owner than create record for owner

		if (securityService.getLoggedUserName().equals(
				taskInstance.getActorId())) {
			
			QueryVerificationDTO existingDTO=queryService.getQueryVerification(QueryVerification.RECORD_TYPE.FILLED_BY_OWNER, queryId,query.getFollowupNumber());

			dto.setRecordType(QueryVerification.RECORD_TYPE.FILLED_BY_OWNER);
			if(existingDTO!=null){
				queryService.updateQueryVerification(dto, queryId,query.getFollowupNumber());
			}else{
				queryService.addQueryVerification(dto, queryId,query.getFollowupNumber());
			}
			
			
		}

		// if verifier than create record for verifier
		if (securityService.getLoggedUserName().equals(query.getVerifyPerson())) {
			
			QueryVerificationDTO  existingDTO=queryService.getQueryVerification(QueryVerification.RECORD_TYPE.FILLED_BY_VERIFIER, queryId,query.getFollowupNumber());
			
			dto.setRecordType(QueryVerification.RECORD_TYPE.FILLED_BY_VERIFIER);
			
			if(existingDTO!=null){
				queryService.updateQueryVerification(dto, queryId,query.getFollowupNumber());
			}else{
				queryService.addQueryVerification(dto, queryId,query.getFollowupNumber());
			}
		}

		return SUCCESS;
	}

	@SkipValidation
	public String rejectQueryVerification() {
		query = queryService.getClosedQueryDetails(queryId, isAffiliate());
		taskInstance = workflowService.getTaskInstance(taskId);
		
		// remove final response or supporting document
		for(Attachment att : query.getAttachments()){
			if (("B").equals(query.getWorkflowRouteType())) {
				if(att.getAttachmentCategory().getAttachmentCategory().equals("Supporting Documentation")&&(att.getFollowupSeq()==query.getFollowupSeq())){
					 attachmentService.remove(att);
				}
			}else{
				if(att.getAttachmentCategory().getAttachmentCategory().equals("Final Response")&&(att.getFollowupSeq()==query.getFollowupSeq())){
					 attachmentService.remove(att);
				}
			}
			
		}
		
		//remove owner verification
		queryService.removeQueryVerification(QueryVerification.RECORD_TYPE.FILLED_BY_OWNER, queryId,query.getFollowupNumber());
		
		
		
		return SUCCESS;
	}
	
	@SkipValidation
	public String cancelQueryVerification() {
		return SUCCESS;
	}

	@Override
	public void validate() {
		query = queryService.getClosedQueryDetails(queryId, isAffiliate());
		taskInstance = workflowService.getTaskInstance(taskId);
		

		if (date==null) {
			addActionError("Please put correct Date completed");
		};
		
		
		if (password==null||password.isEmpty()||!canLogIn(getLoggedUser().getUsername(), password)) {
			addActionError("Please put correct password");
		};
		
		if (!requirementsUnderstood && !clarificationRequested
				&& !assumptionsDocumented) {
			addActionError("At least one option from Requirements analysis needs to be checked");
		};
		

		if (buildType == null) {
			addActionError("At least one option from Build needs to be checked");
		} else if (buildType.equals("MEDIUM")) {
			if (!codeReviewPassed && !checkPassed && !resultsVerified) {
				addActionError("At least one option from Test section needs to be checked");
			}
			;
		} else if (buildType.equals("HIGH")) {
			if (!correctParametersPassed) {
				addActionError("Correct parameters box needs to be checked");
			}
			;
		}

		if (!resultsAppropriatelyLabeled || !resultsStored
				|| !searchCriteriaStated) {
			addActionError("All options from release and use section needs to be checked");
		}
		;

		super.validate();
	}

	public boolean canLogIn (String login, String password){
		return passwordIsCorrect(login,password,"emea")||passwordIsCorrect(login,password,"asia")||passwordIsCorrect(login,password,"nala")||passwordIsCorrect(login,password,"exbp");
	}
	
	
	public boolean passwordIsCorrect(String login, String password, String domain){
		
	try{
	
		
			//Set up the environment for creating the initial context
			Hashtable env = new Hashtable();
			env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, SecurityConstants.LDAP_SERVER);
			env.put(Context.SECURITY_AUTHENTICATION, "simple");
		 		 
			String principal = login + "@" + domain + "." + SecurityConstants.ACTIVE_DIRECTORY + ".com";
		
			env.put(Context.SECURITY_PRINCIPAL, principal);
			env.put(Context.SECURITY_CREDENTIALS, password);
		
			//Create the initial context
			DirContext ctx = new InitialDirContext(env);
		
		return true;
	
	}catch(AuthenticationException ae){

		return false;
	}catch(NamingException e){
		return false;
	}
		
		
	}
	
	public long getQueryId() {
		return queryId;
	}

	public void setQueryId(long queryId) {
		this.queryId = queryId;
	}

	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	public Query getQuery() {
		return query;
	}

	public void setQuery(Query query) {
		this.query = query;
	}

	public TaskInstance getTaskInstance() {
		return taskInstance;
	}

	public void setTaskInstance(TaskInstance taskInstance) {
		this.taskInstance = taskInstance;
	}

	public boolean isRequirementsUnderstood() {
		return requirementsUnderstood;
	}

	public void setRequirementsUnderstood(boolean requirementsUnderstood) {
		this.requirementsUnderstood = requirementsUnderstood;
	}

	public boolean isClarificationRequested() {
		return clarificationRequested;
	}

	public void setClarificationRequested(boolean clarificationRequested) {
		this.clarificationRequested = clarificationRequested;
	}

	public boolean isAssumptionsDocumented() {
		return assumptionsDocumented;
	}

	public void setAssumptionsDocumented(boolean assumptionsDocumented) {
		this.assumptionsDocumented = assumptionsDocumented;
	}

	public String getFurtherRequirements() {
		return furtherRequirements;
	}

	public void setFurtherRequirements(String furtherRequirements) {
		this.furtherRequirements = furtherRequirements;
	}

	public String getBuildType() {
		return buildType;
	}

	public void setBuildType(String buildType) {
		this.buildType = buildType;
	}

	public boolean isCorrectParametersPassed() {
		return correctParametersPassed;
	}

	public void setCorrectParametersPassed(boolean correctParametersPassed) {
		this.correctParametersPassed = correctParametersPassed;
	}

	public boolean isCodeReviewPassed() {
		return codeReviewPassed;
	}

	public void setCodeReviewPassed(boolean codeReviewPassed) {
		this.codeReviewPassed = codeReviewPassed;
	}

	public boolean isCheckPassed() {
		return checkPassed;
	}

	public void setCheckPassed(boolean checkPassed) {
		this.checkPassed = checkPassed;
	}

	public boolean isResultsVerified() {
		return resultsVerified;
	}

	public void setResultsVerified(boolean resultsVerified) {
		this.resultsVerified = resultsVerified;
	}

	public String getFurtherTestReference() {
		return furtherTestReference;
	}

	public void setFurtherTestReference(String furtherTestReference) {
		this.furtherTestReference = furtherTestReference;
	}

	public boolean isResultsAppropriatelyLabeled() {
		return resultsAppropriatelyLabeled;
	}

	public void setResultsAppropriatelyLabeled(
			boolean resultsAppropriatelyLabeled) {
		this.resultsAppropriatelyLabeled = resultsAppropriatelyLabeled;
	}

	public boolean isResultsStored() {
		return resultsStored;
	}

	public void setResultsStored(boolean resultsStored) {
		this.resultsStored = resultsStored;
	}

	public boolean isSearchCriteriaStated() {
		return searchCriteriaStated;
	}

	public void setSearchCriteriaStated(boolean searchCriteriaStated) {
		this.searchCriteriaStated = searchCriteriaStated;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public QueryService getQueryService() {
		return queryService;
	}

	public WorkflowService getWorkflowService() {
		return workflowService;
	}

	public Date getDate() {
		return date;
	}

	@TypeConversion(converter = "com.roche.dss.util.converter.DateConverter")
	public void setDate(Date date) {
		this.date = date;
	}


}