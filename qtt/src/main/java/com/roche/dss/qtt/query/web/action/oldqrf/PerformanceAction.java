package com.roche.dss.qtt.query.web.action.oldqrf;

import java.util.List;

import com.roche.dss.qtt.model.CtrlOformat;
import com.roche.dss.qtt.model.PerformanceMetric;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.service.QrfService;

import javax.annotation.Resource;

public class PerformanceAction extends QRFAbstractAction {

	private static final long serialVersionUID = 6307593384547544897L;

	private QrfService qrfService;


    @Resource
    public void setQrfService(QrfService qrfService) {
        this.qrfService = qrfService;
    }

    PerformanceMetric performance = new PerformanceMetric();

    @Override
    public Object getModel() {
        return performance;
    }

    public List<CtrlOformat> getClotypes() {
        return qrfService.getCtrlOformats();
    }
    
    public String execute() {
        // the query is update
        if (getSession().get(ActionConstants.QRF_EDIT_QUERY_TYPE) != null) {
            qrfService.updatePerformanceMetrics(performance, (Integer) getSession().get(ActionConstants.QRF_QUERY_ID));
        } else {
            qrfService.addPerformanceMetrics(performance, (Integer) getSession().get(ActionConstants.QRF_QUERY_ID));
        }
        return SUCCESS;
    }

	@Override
	public void validate() {
		super.validate();
		if (StringUtils.isBlank(performance.getRequestDescription())) {
			addActionError(getText("metrics.displayname"));
		} else if (performance.getRequestDescription().length() > 1000) {
			addActionError(getText("metrics.maxlength.displayname"));
		}
		if (performance.getOutputFormat() == null || performance.getOutputFormat().getCtrlOformatId() == 0) {
			addActionError(getText("outputformat.displayname"));
		} else if (performance.getOutputFormat().getCtrlOformatId() == CtrlOformat.OUTPUT_FORMAT_OTHER
				&& StringUtils.isBlank(performance.getSpecialRequests())) {
			addActionError(getText("specialrequests.displayname"));
		}
		if (StringUtils.isNotBlank(performance.getSpecialRequests()) && performance.getSpecialRequests().length() > 300) {
			addActionError(getText("specialrequests.maxlength.displayname"));
		}
	}

}
