package com.roche.dss.qtt.service;

import java.util.List;

import com.roche.dss.qtt.model.Query;

//TODO to delete
public class RepositorySearchResults {
	protected List<Query> queries;
	
	public RepositorySearchResults(List<Query> queries) {
		this.queries = queries;
	}
	
	public boolean contains (Query query) {
		if (queries != null) {
			return queries.contains(query);
		} else {
			return false;
		}
	}

	public List<Query> getQueries() {
		return queries;
	}
}
