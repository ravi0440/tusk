package com.roche.dss.util.comparator;

import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;

import java.util.Comparator;

/**
 * User: pruchnil
 */
public class QuerySummaryTypeComparator implements Comparator<QuerySummaryDTO> {
    @Override
    public int compare(QuerySummaryDTO o1, QuerySummaryDTO o2) {
        String first = o1.getQuery().getQueryType().getQueryType();
		String second = o2.getQuery().getQueryType().getQueryType();

		if ( first == null )
			return -1;
		if ( second == null )
			return 1;

        return first.compareTo( second );
    }
}
