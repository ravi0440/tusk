package com.roche.dss.util;

import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import com.roche.dss.qtt.service.UtilService;

@Component("invalidatingJdbcTemplate")
public class InvalidatingJdbcTemplate{

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private UtilService utilService;
	
	@Resource(name = "qttDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

	public int update(String sql, Object... args) throws DataAccessException {
		int result =  jdbcTemplate.update(sql, args);
		utilService.cleanSecondLevelCache(true);
		return result;
	}
	
	public <T> List<T> query(String sql, RowMapper<T> rowMapper)
			throws DataAccessException {
		return jdbcTemplate.query(sql, rowMapper);
	}

	public <T> List<T> query(String sql, Object[] args,
			RowMapper<T> rowMapper) throws DataAccessException {
		return jdbcTemplate.query(sql, args, rowMapper);
	}

	public long queryForLong(String sql) throws DataAccessException {
		return jdbcTemplate.queryForLong(sql);
	}

	public int queryForInt(String sql, Object... args)
			throws DataAccessException {
		return jdbcTemplate.queryForInt(sql, args);
	}

	public SqlRowSet queryForRowSet(String sql, Object... args)
			throws DataAccessException {
		return jdbcTemplate.queryForRowSet(sql, args);
	}

	public long queryForLong(String sql, Object... args)
			throws DataAccessException {
		return jdbcTemplate.queryForLong(sql, args);
	}

	public <T> T queryForObject(String sql, Object[] args, Class<T> requiredType)
			throws DataAccessException {
		return jdbcTemplate.queryForObject(sql, args, requiredType);
	}

}
