package com.roche.dss.util.comparator;

import java.io.Serializable;
import java.util.Comparator;

import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;

/**
 * @author savovp
 *
 */
public class QuerySummaryWorkflowTypeComparator implements Comparator<QuerySummaryDTO>, Serializable {

	private static final long serialVersionUID = 5794808832172423110L;

	@Override
	public int compare(QuerySummaryDTO o1, QuerySummaryDTO o2) {
        
		String first = o1.getQuery().getWorkflowRouteTypeLowercase();
		String second = o2.getQuery().getWorkflowRouteTypeLowercase();
		
		if ( first == null )
			return -1;
		if ( second == null )
			return 1;
		
        return first.compareTo( second );
    }

}
