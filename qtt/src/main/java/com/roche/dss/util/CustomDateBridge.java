package com.roche.dss.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class CustomDateBridge implements TwoWayStringBridge {
	private final static TimeZone GMT = TimeZone.getTimeZone("GMT");
	private static final Calendar calInstance;
	private static final SimpleDateFormat DAY_FORMAT = new SimpleDateFormat(
			"yyyyMMdd", Locale.US);

	static {
		calInstance = Calendar.getInstance(GMT);
		DAY_FORMAT.setTimeZone(GMT);
	}

	public Object stringToObject(String stringValue) {
		if (StringHelper.isEmpty(stringValue))
			return null;
		try {
			synchronized (CustomDateBridge.class) {
				return DAY_FORMAT.parse(stringValue);	
			}
		} catch (ParseException e) {
			throw new SearchException("Unable to parse into date: "
					+ stringValue, e);
		}
	}

	public String objectToString(Object object) {
		return object != null ? dateToString((Date) object).toLowerCase()
				: null;
	}

	public static synchronized String dateToString(Date date) {
		return timeToString(date.getTime());
	}

	public static synchronized String timeToString(long time) {
		calInstance.setTimeInMillis(time);
		calInstance.set(Calendar.HOUR_OF_DAY, 0);
		calInstance.set(Calendar.MINUTE, 0);
		calInstance.set(Calendar.SECOND, 0);
		calInstance.set(Calendar.MILLISECOND, 0);
		Date date = calInstance.getTime();
		return DAY_FORMAT.format(date);
	}

}
