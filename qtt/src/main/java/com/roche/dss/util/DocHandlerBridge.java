package com.roche.dss.util;

import java.io.IOException;
import java.util.Properties;

import com.roche.dss.qtt.service.FullTextSearchService;

@Component
public class DocHandlerBridge implements StringBridge, ApplicationContextAware {
	protected static final Logger log = LoggerFactory.getLogger(DocHandlerBridge.class);
	
	protected static String fileRepositoryBase = "";
	protected static boolean testMode = false;
	
	protected static FullTextSearchService service;
	
	@Override
	public void setApplicationContext(ApplicationContext ctx)
			throws BeansException {
		try {
			Resource res = ctx.getResource("classpath:application.properties");
			if (res == null) {
				log.error("Cannot read application.properties");
				return;
			}
			Properties props = new Properties();
			props.load(res.getInputStream());
			if (props.getProperty("qtt.repository.baseDirectory") == null) {
				log.error("Cannot find the qtt.repository.baseDirectory parameter in application.properties");
				return;
			}
			fileRepositoryBase = props.getProperty("qtt.repository.baseDirectory");
			log.debug("fileRepositoryBase: " + fileRepositoryBase);
			if ("true".equals(props.getProperty("qtt.repository.testMode"))) {
				testMode = true;
				log.debug("indexing done in a test mode");
			}
		} catch (IOException e) {
			log.error("Error while reading application.properties", e);
		}
		
		service = ctx.getBean(FullTextSearchService.class);
	}
	
	@Override
	public String objectToString(Object arg) {
		String dummy = " ";
        if (arg == null || ! (arg instanceof String)) {
            return dummy;
        }
        String filename = (String) arg;
        if (testMode) {
        	filename = filename.substring(filename.indexOf("/") + 1);
        }
        filename = fileRepositoryBase + filename;
        
        return service.getFileTextContent(filename);
	}

	public static String getFileRepositoryBase() {
		return fileRepositoryBase;
	}

	public static boolean getTestMode() {
		return testMode;
	}
}
