package com.roche.dss.util.converter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class DateConverter extends StrutsTypeConverter {

	private static final String DATE_PATTERN = "^\\d{2}-[a-zA-Z]{3}-\\d{4}$";

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MMM-yyyy", Locale.UK);

	static {
		DATE_FORMAT.setLenient(false);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object convertFromString(Map context, String[] values, Class toClass) {
		if (values == null || values.length == 0 || StringUtils.isBlank(values[0])) {
			return null;
		}

		//Match the string against the date format, SimpleDateFormat is too lenient
		if (!values[0].matches(DATE_PATTERN)) {
			throw new TypeConversionException("Value '" + values[0] + "' does not match pattern: " + DATE_PATTERN);
		}

		try {
			return DATE_FORMAT.parse(values[0]);
		} catch (ParseException ex) {
			throw new TypeConversionException(ex);
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String convertToString(Map context, Object o) {
		try {
			return DATE_FORMAT.format((Date) o);
		} catch (Exception ex) {
			return null;
		}
	}

}
