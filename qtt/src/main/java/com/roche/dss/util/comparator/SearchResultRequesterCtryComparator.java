package com.roche.dss.util.comparator;

import java.util.Comparator;

import com.roche.dss.qtt.model.QttCountry;
import com.roche.dss.qtt.model.Requester;
import com.roche.dss.qtt.service.SearchResult;

public class SearchResultRequesterCtryComparator implements Comparator<SearchResult> {
    @Override
	public int compare(SearchResult o1, SearchResult o2) {
		Requester firstR = o1.getQuery().getRequester();
		Requester secondR = o2.getQuery().getRequester();
		
		if ( firstR == null )
			return -1;
		if ( secondR == null )
			return 1;
		
		QttCountry firstC = firstR.getCountry();
		QttCountry secondC = secondR.getCountry();

		if ( firstC == null )
			return -1;
		if ( secondC == null )
			return 1;
		
		String first = firstC.getDescription();
		String second = secondC.getDescription();
		
		if ( first == null )
			return -1;
		if ( second == null )
			return 1;
		
        return first.compareTo( second );
	}
}
