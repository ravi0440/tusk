/* 
 *====================================================================
 *  $Header$
 *  @author $Author: waltmana $
 *  @version $Revision: 2460 $ $Date: 2012-07-03 10:34:37 +0200 (Wt, 03 lip 2012) $
 *
 *====================================================================
 */
package com.roche.dss.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.SecureRandom;

/*
 * 
 * 
 * GUIDs are guaranteed to be globally unique by using ethernet MACs, IP
 * addresses, time elements, and sequential numbers. GUIDs can be used as
 * security devices to hide things such as files within a filesystem where
 * listings are unavailable (e.g. files that are served up from a Web server
 * with indexing turned off). This may be desireable in cases where standard
 * authentication is not appropriate. In this scenario, the RandomGUIDs are used
 * as directories. Another example is the use of GUIDs for primary keys in a
 * database where you want to ensure that the keys are secret. Random GUIDs can
 * then be used in a URL to prevent hackers (or users) from accessing records by
 * guessing or simply by incrementing sequential numbers.
 * 
 * There are many other possiblities of using GUIDs in the realm of security and
 * encryption where the element of randomness is important. This class was
 * written for these purposes but can also be used as a general purpose GUID
 * generator as well.
 * 
 * RandomGUID generates truly random GUIDs by using the system's IP address
 * (name/IP), system time in milliseconds (as an integer), and a very large
 * random number joined together in a single String that is passed through an
 * MD5 hash. The IP address and system time make the MD5 seed globally unique
 * and the random number guarantees that the generated GUIDs will have no
 * discernable pattern and cannot be guessed given any number of previously
 * generated GUIDs. It is generally not possible to access the seed information
 * (IP, time, random number) from the resulting GUIDs as the MD5 hash algorithm
 * provides one way encryption.
 * 
 * 
 * Seeding the basic generator in this way effectively decouples the random
 * numbers from the time component making it virtually impossible to predict the
 * random number component even if one had absolute knowledge of the System
 * time. Thanks to Ashutosh Narhari for the suggestion of using the static
 * method to prime the basic random generator.
 * 
 * Using the secure random option, this class compies with the statistical
 * random number generator tests specified in FIPS 140-2, Security Requirements
 * for Cryptographic Modules, secition 4.9.1.
 * 
 */
public class KeyGenerator {

	private static final Logger logger = LoggerFactory.getLogger(KeyGenerator.class);
	
    private static SecureRandom secureRand = null;

    private static String sid = null;

    private static class SingletonHolder { 
        public static final KeyGenerator instance = new KeyGenerator();
    }


    public static KeyGenerator getInstance() {

        if (secureRand == null) {
            secureRand = new SecureRandom();
        }
        if (sid == null) {
            try {
                sid = InetAddress.getLocalHost().toString();
            } catch (UnknownHostException e) {
            	logger.error(e.getMessage());
            }
        }
        
        return SingletonHolder.instance;
    }

    /**
     * Cannot instantiate. Must use instance() method to get a handle to the
     * object.
     */
    private KeyGenerator() {
    }

    /**
     * Returns a unique ID to the caller. This is synchronized to prevent
     * multiple callers from getting the same ID back if they called getNextID()
     * at the same time.
     *
     * @return String nextID
     * @throws Exception
     */
    public synchronized String getNextID() throws Exception {
        MessageDigest md5 = null;
        StringBuffer sbValueBeforeMD5 = new StringBuffer();
        String valueBeforeMD5 = "";
        String valueAfterMD5 = "";

        try {
            md5 = MessageDigest.getInstance("MD5");
            long time = System.currentTimeMillis();
            long rand = 0;

            rand = secureRand.nextLong();

            // This StringBuffer can be a long as you need; the MD5
            // hash will always return 128 bits. You can change
            // the seed to include anything you want here.
            // You could even stream a file through the MD5 making
            // the odds of guessing it at least as great as that
            // of guessing the contents of the file!
            sbValueBeforeMD5.append(sid);
            sbValueBeforeMD5.append(":");
            sbValueBeforeMD5.append(Long.toString(time));
            sbValueBeforeMD5.append(":");
            sbValueBeforeMD5.append(Long.toString(rand));

            valueBeforeMD5 = sbValueBeforeMD5.toString();
            md5.update(valueBeforeMD5.getBytes());

            byte[] array = md5.digest();
            StringBuffer sb = new StringBuffer();
            for (int j = 0; j < array.length; ++j) {
                int b = array[j] & 0xFF;
                if (b < 0x10)
                    sb.append('0');
                sb.append(Integer.toHexString(b));
            }
            valueAfterMD5 = sb.toString();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return valueAfterMD5;
    }

}
