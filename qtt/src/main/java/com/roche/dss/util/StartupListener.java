package com.roche.dss.util;

import com.roche.dss.qtt.service.UtilService;

@Component
public class StartupListener implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	protected UtilService utilService;
	
	protected static final Logger log = LoggerFactory.getLogger(DocHandlerBridge.class);
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		log.info("application refreshed, purging the whole cache");
		utilService.cleanSecondLevelCache(false);
	}

}
