package com.roche.dss.util.comparator;

import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;
import java.io.Serializable;
import java.util.Comparator;

/**
 * User: pruchnil
 */
public class QuerySummaryInitDateComparator implements Comparator<QuerySummaryDTO>, Serializable {

    @Override
    public int compare(QuerySummaryDTO o1, QuerySummaryDTO o2) {
        DateTime first = o1.getQuery().getInitiateDate();
		DateTime second = o2.getQuery().getInitiateDate();

		if ( first == null )
			return -1;
		if ( second == null )
			return 1;

        return first.compareTo( second );
    }

}
