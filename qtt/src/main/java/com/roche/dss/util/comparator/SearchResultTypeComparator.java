package com.roche.dss.util.comparator;

import java.util.Comparator;

import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.service.SearchResult;

public class SearchResultTypeComparator implements Comparator<SearchResult> {
    @Override
	public int compare(SearchResult o1, SearchResult o2) {
		QueryType firstT = o1.getQuery().getQueryType();
		QueryType secondT = o2.getQuery().getQueryType();

		if ( firstT == null )
			return -1;
		if ( secondT == null )
			return 1;
		
		String first = firstT.getQueryType();
		String second = secondT.getQueryType();
		
		if ( first == null )
			return -1;
		if ( second == null )
			return 1;
		
        return first.compareTo( second );
	}
}
