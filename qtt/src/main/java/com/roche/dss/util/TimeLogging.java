package com.roche.dss.util;

/**
 * User: pruchnil
 */
@Component
@Aspect
public class TimeLogging {
     private static final Logger logger = LoggerFactory.getLogger(TimeLogging.class);

    @Around("methodsToBeProfiled()")
    public Object invoke(ProceedingJoinPoint pjp) throws Throwable {
        StopWatch sw = new StopWatch();
        sw.start();
        Object val = pjp.proceed();
        sw.stop();
        logger.debug("Class " + pjp.getSourceLocation().getWithinType().getName() + " Method " + pjp.getSignature().getName() + ": " + sw.getTotalTimeSeconds());
        return val;
    }

    @Pointcut("within(com.roche.dss.qtt.service..*)")
    public void methodsToBeProfiled(){}

}
