package com.roche.dss.util.comparator;

import java.io.Serializable;
import java.util.Date;
import java.util.Comparator;

/**
 * @author zerkowsm
 *
 */
public class TaskEndDateComparator implements Comparator<TaskInstance>, Serializable {

	private static final long serialVersionUID = -8393215438835267374L;

	@Override
	public int compare(TaskInstance o1, TaskInstance o2) {
        
		Date taskEnd1 = (Date) o1.getEnd();
		Date taskEnd2 = (Date) o2.getEnd();
		
		if (taskEnd1 == null)
			return -1;
		if (taskEnd2 == null)
			return 1;
		
		return taskEnd1.compareTo(taskEnd2);
	}

}