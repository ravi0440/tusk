package com.roche.dss.util.comparator;

import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;
import java.util.Comparator;

/**
 * User: pruchnil
 */
public class QuerySummaryDMGRespDateComparator implements Comparator<QuerySummaryDTO> {
    @Override
    public int compare(QuerySummaryDTO o1, QuerySummaryDTO o2) {
        DateTime first = o1.getQuery().getDmgDueDate();
		DateTime second = o2.getQuery().getDmgDueDate();

		if ( first == null )
			return -1;
		if ( second == null )
			return 1;

        return first.compareTo( second );
    }
}
