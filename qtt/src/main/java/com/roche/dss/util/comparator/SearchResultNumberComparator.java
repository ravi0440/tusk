package com.roche.dss.util.comparator;

import java.util.Comparator;

import com.roche.dss.qtt.service.SearchResult;

public class SearchResultNumberComparator implements Comparator<SearchResult> {
	
    @Override
	public int compare(SearchResult o1, SearchResult o2) {
		String first = o1.getQuery().getQueryNumber();
		String second = o2.getQuery().getQueryNumber();
		
		if ( first == null )
			return -1;
		if ( second == null )
			return 1;
		
        return first.compareTo( second );
	}
}
