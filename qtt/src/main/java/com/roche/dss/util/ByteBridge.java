package com.roche.dss.util;

public class ByteBridge implements StringBridge {

	@Override
	public String objectToString(Object object) {
		if (object == null || ! (object instanceof Byte)) {
			return null;
		}
		return ((Byte) object).toString();
	}

}
