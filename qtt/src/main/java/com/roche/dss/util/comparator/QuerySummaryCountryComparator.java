package com.roche.dss.util.comparator;

import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;

import java.util.Comparator;

/**
 * User: pruchnil
 */
public class QuerySummaryCountryComparator implements Comparator<QuerySummaryDTO> {
    @Override
    public int compare(QuerySummaryDTO o1, QuerySummaryDTO o2) {
       	String first = o1.getQuery().getCountry().getDescription();
		String second = o2.getQuery().getCountry().getDescription();
		if ( first == null )
			return -1;
		if ( second == null )
			return 1;

        return first.compareTo( second );
    }
}
