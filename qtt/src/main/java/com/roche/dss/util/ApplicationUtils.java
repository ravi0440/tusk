package com.roche.dss.util;

import com.roche.dss.qtt.utility.QueryStatusCodes;
import com.roche.dss.qtt.service.SearchResult;
import com.roche.dss.util.comparator.SearchResultDrugNameComparator;
import com.roche.dss.util.comparator.SearchResultExpiryDateComparator;
import com.roche.dss.util.comparator.SearchResultNumberComparator;
import com.roche.dss.util.comparator.SearchResultRequesterCtryComparator;
import com.roche.dss.util.comparator.SearchResultRespDateComparator;
import com.roche.dss.util.comparator.SearchResultTypeComparator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.BreakIterator;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

/**
 * This class contains some common used static methods
 * <p/>
 * User: pruchnil
 */
public final class ApplicationUtils {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationUtils.class);
    public static final String WHITESPACE = " ";
    public static final String HYPHEN = "-";
    private final static String months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("dd-MMM-yyyy").withLocale(Locale.US);
    public static final DateTimeFormatter DATE_TIME_FORMATTER_ALL = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm aaa").withLocale(Locale.US);
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
    
    public static String DATE_INDEX_EXTREME_PAST = "19900101";
    
    public static String DATE_INDEX_EXTREME_FUTURE = "30010101";
    
	private final static String SORT_BY_QUERY_NUMBER = "queryNumber";
	private final static String SORT_BY_DRUG_NAME = "drugNameAbbrev";
	private final static String SORT_BY_QUERY_TYPE = "queryType.queryType";
	private final static String SORT_BY_REQUESTER_COUNTRY = "requester.country.description";
	private final static String SORT_BY_RESPONSE_DATE = "currentDueDate";
    private final static String SORT_BY_EXPIRY_DATE ="expiryDate";
    
    public static final String SESSION_QUICK_SEARCH_PARAMS = "quickSearchParams";
    public static final String SESSION_REPOSITORY_SEARCH_PARAMS = "repositorySearchParams";

    public static final String[] ACCEPTABLE_ATTACH_TYPES = new String[]{"pdf", "rtf", "doc"};
    
	private final static int MAX_LINE_LENGTH = 50;
	
	private final static String LINE_FEED = "\n";

    /**
     * This method calculates the difference expressed in days between two dates
     *
     * @param dateFrom
     * @param dateTo
     * @return difference in days between two dates
     */
    public static int calculateDaysBetween(Date dateFrom, Date dateTo) {
        int retVal = 0;
        if ((dateFrom == null) || (dateTo == null)) {
            return retVal;
        }
        retVal = (int) ((dateTo.getTime() - dateFrom.getTime()) / (1000 * 60 * 60 * 24));
        return retVal;
    }

    /**
     * This method converts string:'Y' or 'N' to boolean: true or false
     *
     * @param value
     * @return boolean value
     */
    public static boolean convertToBoolean(String value) {
        if (StringUtils.isNotEmpty(value) && "Y".equals(StringUtils.capitalize(value))) {
            return true;
        }
        return false;
    }

    /**
     * This method converts string:'Y' or 'N' to boolean: true or false
     *
     * @param value
     * @return boolean value
     */
    public static boolean convertToBoolean(char value) {
        return convertToBoolean(String.valueOf(value));
    }

    /**
     * This method converts boolean to char:'Y' or 'N'
     *
     * @param value
     * @return char value of boolean
     */
    public static char booleanToChar(boolean value) {
        return (value ? 'Y' : 'N');
    }

	public static String[] concatenateArrays(String[] firstPart, String[] secondPart){
		int fpl = firstPart.length;
		int spl = secondPart.length;
		
		String[] ret = new String[fpl + spl];
		
		System.arraycopy(firstPart, 0, ret, 0, fpl);
		System.arraycopy(secondPart, 0, ret, fpl, spl);
		
		return ret;
	}
    
    /**
     *  Add zeros to number to left site
     *
     * @param number
     * @return
     */
    public static String addZeros(long number){
        return String.format("%04d", number);
    }
    
    public static String getMonthCanonicalAbbrev(String abbrev) {
    	if (abbrev == null || abbrev.length() != 3) {
    		return null;
    	}
    	for (String m: months) {
    		if (m.equalsIgnoreCase(abbrev)) {
    			return m;
    		}
    	}
    	return null;
    }
    
    public static String getMonthNumber(String abbrev) {
    	if (abbrev == null || abbrev.length() != 3) {
    		return null;
    	}
    	for (int i = 0; i < months.length; ++ i) {
    		if (months[i].equalsIgnoreCase(abbrev)) {
    			return formatAsDay(new Integer(i + 1));
    		}
    	}
    	return null;
    }
    
    public static Integer getMonthInteger(String abbrev) {
    	if (abbrev == null || abbrev.length() != 3) {
    		return null;
    	}
    	for (int i = 0; i < months.length; ++ i) {
    		if (months[i].equalsIgnoreCase(abbrev)) {
    			return new Integer(i + 1);
    		}
    	}
    	return null;
    }
    
    public static boolean isEmptyString(String s) {
    	if (s == null || s.length() == 0 || s.trim().length() == 0) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    public static boolean isEmptyArray(Object[] arr) {
    	if (arr == null || arr.length == 0) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    public static String formatAsDay(Integer i) {
    	if (i == null) {
    		return null;
    	}
    	if (i >= 0 && i <= 9) {
    		return "0" + i.toString();
    	} else {
    		return i.toString();
    	}
    }
    
	public static boolean isThreeElemDateCorrect(Integer day, String month, Integer year) {
		boolean dateOk = true;
		if (day == null
				|| ApplicationUtils.isEmptyString(month)
				|| year == null) {
			dateOk = false;
		} else {
			if (year <= 1900 || year > 2099) {
				dateOk = false;
			} else {
				String monthCanonical = ApplicationUtils.getMonthCanonicalAbbrev(month);
				if (monthCanonical == null) {
					dateOk = false;
				} else {
					String dateFmt = day.toString()
						+ "-" + monthCanonical + "-" + year.toString();
					try {
						DATE_TIME_FORMATTER.parseDateTime(dateFmt);
					} catch (IllegalArgumentException e) {
						dateOk = false;
					}
				}
			}
		}
		return dateOk;
	}
	
	public static String searchDateRange(Date from, Date to, String searchTerm) {
		String r = null;
		
		if (from != null || to != null) {
			String d1 = null;
			String d2 = null;
			if (from != null) {
				d1 = DATE_FORMAT.format(from);
			} else {
				d1 = DATE_INDEX_EXTREME_PAST;
			}
			if (to != null) {
				d2 = DATE_FORMAT.format(to);
			} else {
				d2 = DATE_INDEX_EXTREME_FUTURE;
			}
			r = searchTerm + ":[" + d1 + " TO " + d2 + "]";
		}
		
		return r;
	}

	public static Comparator<SearchResult> getComparator(String orderBy) {
		Comparator<SearchResult> comparator = null;
		if (SORT_BY_QUERY_NUMBER.equals(orderBy)) {
			comparator = new SearchResultNumberComparator();
		} else if (SORT_BY_DRUG_NAME.equals(orderBy)) {
			comparator = new SearchResultDrugNameComparator();
		} else if (SORT_BY_QUERY_TYPE.equals(orderBy)) {
			comparator = new SearchResultTypeComparator();
		} else if (SORT_BY_REQUESTER_COUNTRY.equals(orderBy)) {
			comparator = new SearchResultRequesterCtryComparator();
		} else if (SORT_BY_RESPONSE_DATE.equals(orderBy)) {
			comparator = new SearchResultRespDateComparator();
		} else if (SORT_BY_EXPIRY_DATE.equals(orderBy)) {
			comparator = new SearchResultExpiryDateComparator();
		} else {
			comparator = new SearchResultNumberComparator();
		}
		return comparator;
	}
	
	public static String appendText (String text, String toAppend)
	{
		if (toAppend != null)
		{
			if (text != null) {
				if (text.length() > 0) {
					return text + ". " + toAppend;
				} else {
					return toAppend;
				}
			} else {
				return toAppend;
			}
		}
		else
		{
			return text;
		}
	}
	
    public static void copy(File s, File t) throws IOException {
        FileChannel in = (new FileInputStream(s)).getChannel();
        FileChannel out = (new FileOutputStream(t)).getChannel();
        try {
        	in.transferTo(0, s.length(), out);
        } finally {
        	in.close();
            out.close();	
        }
      }

    public static String getStatusCodeDescription(String code) {
        for(QueryStatusCodes qsc :  QueryStatusCodes.values()){
            if(qsc.getCode().equals(code)){
               return qsc.getValue();
            }
        }
        return "";
    }
    
    public static String formatStringAsHtml( String s )
    {
    	if ( s == null )  return s;
    	else return lineBreaker( s );
    }
    
	protected static String lineBreaker( String source )
	{
		final String crlf = "<br/>" + LINE_FEED; 
		StringBuffer retVal = new StringBuffer();
		
		// catch case of null data (return empty string)
		if ( source == null )  return "";
		
		// catch case of short data (don't need to do anything)
		if ( source.length() < MAX_LINE_LENGTH )  return source;
		
		BreakIterator wb = BreakIterator.getWordInstance();
		wb.setText( source );
		
		boolean finished = false;
		int position = 0,
			nextBreak = 0,
			nextPos = 0,
			nextEOL = 0;
		
		while ( !finished )
		{
			// update the pointers
			position = nextPos;
			nextBreak = position + MAX_LINE_LENGTH;
			nextEOL = source.indexOf( '\n', position + 1 );
			
			if ( ! ( nextBreak < source.length() ) )
			{	 
				nextPos = source.length();					// use remainder of string.
				finished = true;
			}
			else if ( ( nextEOL > -1 ) && ( nextEOL - position <= MAX_LINE_LENGTH ) )
			{
				nextPos = nextEOL;							// use up to next physical end-of-line.
			}
			else  nextPos = wb.preceding( nextBreak );		// use up to word break just before logical end-of-line.
			
			if ( nextPos <= position || ( nextPos == 0 ) )
			{
				// append a fixed-size chunk, by re-setting nextPos (!)
				nextPos = position + MAX_LINE_LENGTH;
				if ( ! ( nextPos < source.length() ) )
				{
					nextPos = source.length();
					finished = true;
				} 
				
				// chunk = max-length, insert crlf and hyphenate
				retVal.append( source.substring( position, nextPos ) + "-" + crlf );
			}
			else
			{
				// chunk = as recommended
				retVal.append( source.substring( position, nextPos ) + crlf );
			}
		}

		return retVal.toString();
	}
}
