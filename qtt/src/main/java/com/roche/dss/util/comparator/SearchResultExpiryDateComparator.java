package com.roche.dss.util.comparator;

import java.util.Comparator;

import com.roche.dss.qtt.service.SearchResult;

public class SearchResultExpiryDateComparator implements Comparator<SearchResult> {
    @Override
	public int compare(SearchResult o1, SearchResult o2) {
		DateTime firstD = o1.getQuery().getExpiryDate();
		DateTime secondD = o2.getQuery().getExpiryDate();

		if ( firstD == null )
			return -1;
		if ( secondD == null )
			return 1;
		
        return firstD.compareTo( secondD );
	}
}
