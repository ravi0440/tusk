package com.roche.dss.util.comparator;

import java.util.Comparator;

import com.roche.dss.qtt.service.SearchResult;

public class SearchResultDrugNameComparator implements Comparator<SearchResult> {

    @Override
	public int compare(SearchResult o1, SearchResult o2) {
		String first = o1.getQuery().getDrugNameAbbrev();
		String second = o2.getQuery().getDrugNameAbbrev();
		
		if ( first == null )
			return -1;
		if ( second == null )
			return 1;
		
        return first.compareTo( second );
	}
}
