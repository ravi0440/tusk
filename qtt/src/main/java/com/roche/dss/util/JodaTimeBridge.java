package com.roche.dss.util;

import java.util.Locale;

public class JodaTimeBridge implements TwoWayStringBridge{
	
	public static final DateTimeFormatter FMT = DateTimeFormat.forPattern("yyyyMMdd").withLocale(Locale.US);
	
	public String objectToString(Object object) {
		return object != null ? FMT.print(((DateTime) object)).toLowerCase()
				: null;
	}
	
	public Object stringToObject(String stringValue) {
		if (StringHelper.isEmpty(stringValue)) {
			return null;
		}
		return FMT.parseDateTime(stringValue);
	}
}
