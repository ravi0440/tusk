package com.roche.dss.util.comparator;

import java.io.Serializable;
import java.util.Comparator;

import com.roche.dss.qtt.query.comms.dto.QuerySummaryDTO;

/**
 * @author zerkowsm
 *
 */
public class QuerySummaryNumberComparator implements Comparator<QuerySummaryDTO>, Serializable {

	private static final long serialVersionUID = 5794808832172423110L;

	@Override
	public int compare(QuerySummaryDTO o1, QuerySummaryDTO o2) {
        
		String n1 = o1.getQuery().getQueryNumber();
        String n2 = o2.getQuery().getQueryNumber();

        try {
            int num1 = Integer.parseInt(n1.substring(3));
            int num2 = Integer.parseInt(n2.substring(3));
            
            return (num1 - num2);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

}
