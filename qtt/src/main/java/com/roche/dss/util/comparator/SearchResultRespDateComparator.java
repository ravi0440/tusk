package com.roche.dss.util.comparator;

import java.util.Comparator;

import com.roche.dss.qtt.service.SearchResult;

public class SearchResultRespDateComparator implements Comparator<SearchResult> {
    @Override
	public int compare(SearchResult o1, SearchResult o2) {
		DateTime first = o1.getQuery().getCurrentDueDate();
		DateTime second = o2.getQuery().getCurrentDueDate();

		if ( first == null )
			return -1;
		if ( second == null )
			return 1;
		
        return first.compareTo( second );
	}
}
