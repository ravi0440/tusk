package com.roche.dss.util;

import java.io.IOException;
import java.io.PrintWriter;

public class Msg2txtUnicode {
	protected static final Logger log = LoggerFactory.getLogger(Msg2txtUnicode.class);
	
	/**
	 * The stem used to create file names for the text file and the directory
	 * that contains the attachments.
	 */
	private String fileNameStem;
	
	private String fileNameOrig;
	
	/**
	 * The Outlook MSG file being processed.
	 */
	private MAPIMessage msg;
	
	public Msg2txtUnicode(String fileName) throws IOException {
		fileNameOrig = fileName;
		fileNameStem = fileName;
		if(fileNameStem.endsWith(".msg") || fileNameStem.endsWith(".MSG")) {
			fileNameStem = fileNameStem.substring(0, fileNameStem.length() - 4);
		}
		msg = new MAPIMessage(fileName);
	}
	
	/**
	 * Processes the message.
	 * 
	 * @throws IOException if an exception occurs while writing the message out
	 */
	public void processMessage() throws IOException {
		String txtFileName = fileNameStem + ".txt";
		PrintWriter txtOut = null;
		try {
			txtOut = new PrintWriter(txtFileName, "UTF8");
			try {
				String displayFrom = msg.getDisplayFrom();
				txtOut.println("From: "+displayFrom);
			} catch (ChunkNotFoundException e) {
				// ignore
			}
			try {
				String displayTo = msg.getDisplayTo();
				txtOut.println("To: "+displayTo);
			} catch (ChunkNotFoundException e) {
				// ignore
			}
			try {
				String displayCC = msg.getDisplayCC();
				txtOut.println("CC: "+displayCC);
			} catch (ChunkNotFoundException e) {
				// ignore
			}
			try {
				String displayBCC = msg.getDisplayBCC();
				txtOut.println("BCC: "+displayBCC);
			} catch (ChunkNotFoundException e) {
				// ignore
			}
			try {
				String subject = msg.getSubject();
				txtOut.println("Subject: "+subject);
			} catch (ChunkNotFoundException e) {
				// ignore
			}
			try {
				String body = msg.getTextBody();
				txtOut.println(body);
			} catch (ChunkNotFoundException e) {
				log.info("No message body in: " + fileNameOrig);
			}
		} finally {
			if(txtOut != null) {
				txtOut.close();
			}
		}
	}
}
