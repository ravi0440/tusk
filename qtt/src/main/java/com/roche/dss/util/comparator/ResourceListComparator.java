package com.roche.dss.util.comparator;

import java.io.Serializable;
import java.util.Comparator;


import com.roche.dss.qtt.security.model.DSSUser;

/**
 * @author zerkowsm
 *
 */
public class ResourceListComparator implements Comparator<DSSUser>, Serializable {

	private static final long serialVersionUID = -8393215438835267374L;

	@Override
	public int compare(DSSUser o1, DSSUser o2) {
        
		String userFullName1 = o1.getUserFullName();
		String userFullName2 = o2.getUserFullName();
		
		if (userFullName1 == null)
			return -1;
		if (userFullName2 == null)
			return 1;
		
		return userFullName1.compareTo(userFullName2);
	}

}