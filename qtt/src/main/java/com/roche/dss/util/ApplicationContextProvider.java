package com.roche.dss.util;

@Component
public class ApplicationContextProvider implements ApplicationContextAware {
	public static ApplicationContext ctx;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
        ctx = applicationContext;
	}

}
