package com.roche.dss.util;

import java.io.Serializable;

@Component("filenameKeyGenerator")
public class FirstStringParamKeyGenerator implements CacheKeyGenerator<Serializable>{
	@Override
	public Serializable generateKey(MethodInvocation methodInvocation) {
		String key = (String) methodInvocation.getArguments()[0];
		return key;
	}

	@Override
	public Serializable generateKey(Object... data) {
		return null;
	}
}
