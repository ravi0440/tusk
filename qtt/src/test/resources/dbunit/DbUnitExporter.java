package dbunit;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.dbunit.ext.oracle.Oracle10DataTypeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;

import static java.lang.Class.forName;

/**
 * Created by IntelliJ IDEA.
 * User: pruchnil
 * Date: 17.01.11
 * Time: 08:13
 * To change this template use File | Settings | File Templates.
 */
public class DbUnitExporter {

    public static void main(String[] args) throws Exception {
        Class driverClass = forName("oracle.jdbc.driver.OracleDriver");
        Connection jdbcConnection = DriverManager.getConnection(
                "jdbc:oracle:thin:@monet.kau.roche.com:16050:ADPJD", "qtt", "qtt_p1");
        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
        DatabaseConfig config = connection.getConfig();
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new Oracle10DataTypeFactory());

        QueryDataSet partialDataSet = new QueryDataSet(connection);

        //TABLES WITHOUT FOREIGN KEYS - SOME KIND OF DICTIONARIES
/*
        partialDataSet.addTable("COUNTRIES_LU", "SELECT '000',RQC.COUNTRY_CODE, RQC.DESCRIPTION FROM COUNTRIES_LU@ADVENT_DB RQC");
        partialDataSet.addTable("REPORTER_TYPES", "SELECT * FROM REPORTER_TYPES");
        partialDataSet.addTable("QUERY_TYPES", "SELECT * FROM QUERY_TYPES");
        partialDataSet.addTable("QUERY_STATUS_CODES", "SELECT * FROM QUERY_STATUS_CODES");
        partialDataSet.addTable("CASE_OFORMATS", "SELECT * FROM CASE_OFORMATS");
        partialDataSet.addTable("ORGANISATION_TYPES", "SELECT * FROM ORGANISATION_TYPES");
        partialDataSet.addTable("QTT_WORKFLOW_PROCESSES", "SELECT * FROM QTT_WORKFLOW_PROCESSES");
        partialDataSet.addTable("AUDIT_ACTIVITY", "SELECT * FROM AUDIT_ACTIVITY");
        partialDataSet.addTable("ATTACHMENT_CATEGORIES", "SELECT * FROM ATTACHMENT_CATEGORIES");
        partialDataSet.addTable("PQP_TYPES", "SELECT * FROM PQP_TYPES");*/


/*
        partialDataSet.addTable("ORGANISATIONS", "SELECT * FROM ORGANISATIONS where organisation_id < 127326 and organisation_id > 126886");
        partialDataSet.addTable("REQUESTERS", "select * from requesters where requester_id > 126089 and requester_id < 126469");
        partialDataSet.addTable("QUERIES", "SELECT * FROM QUERIES WHERE query_seq > 20261");
        partialDataSet.addTable("AE_TERMS", "SELECT * FROM AE_TERMS WHERE query_seq > 20261");
        partialDataSet.addTable("AUDIT_HISTORY", "SELECT * FROM AUDIT_HISTORY WHERE query_seq > 20261");
        partialDataSet.addTable("DRUGS", "SELECT * FROM drugs WHERE query_seq > 20261");
        partialDataSet.addTable("ATTACHMENTS", "SELECT * FROM ATTACHMENTS WHERE query_seq > 20261");
        partialDataSet.addTable("FOLLOWUP_ATTACHMENTS", "SELECT * FROM FOLLOWUP_ATTACHMENTS WHERE query_seq > 20261");
        partialDataSet.addTable("NON_AFFILIATE_NOTES", "SELECT * FROM NON_AFFILIATE_NOTES WHERE query_seq > 20261");
        partialDataSet.addTable("QUERY_DESCRIPTIONS", "SELECT * FROM QUERY_DESCRIPTIONS WHERE query_seq > 20261");
        partialDataSet.addTable("AE_TERMS", "SELECT * FROM AE_TERMS WHERE query_seq > 20261");
        partialDataSet.addTable("QUERY_DESCRIPTIONS", "SELECT * FROM QUERY_DESCRIPTIONS WHERE query_seq > 20261");
        partialDataSet.addTable("CASES", "SELECT * FROM CASES WHERE query_seq > 20261");
        partialDataSet.addTable("RETRIEVAL_PERIODS", "SELECT * FROM RETRIEVAL_PERIODS WHERE query_seq > 20261");
        partialDataSet.addTable("DRUGS", "SELECT * FROM DRUGS WHERE query_seq > 20261");
        partialDataSet.addTable("FOLLOWUP_ATTACHMENTS", "SELECT * FROM FOLLOWUP_ATTACHMENTS WHERE query_seq > 20261");
        partialDataSet.addTable("PRE_QUERY_PREPARATIONS", "SELECT * FROM PRE_QUERY_PREPARATIONS WHERE query_seq > 20261");*/
        partialDataSet.addTable("AFFILIATE_NOTES", "SELECT * FROM AFFILIATE_NOTES WHERE query_seq > 20261");

        XmlDataSet.write(partialDataSet, new FileOutputStream("target/export3.xml"), "UTF-8");


      // full database export  - not recommended:-)
/*      IDataSet fullDataSet = connection.createDataSet();
        FlatXmlDataSet.write(fullDataSet, new FileOutputStream("full.xml"));*/

        // dependent tables database export: export table X and all tables that
        // have a PK which is a FK on X, in the right order for insertion - not recommended
/*      String[] depTableNames =
                TablesDependencyHelper.getAllDependentTables(connection, "QUERIES");

        IDataSet depDataSet = connection.createDataSet(depTableNames);
        XmlDataSet.write(depDataSet, new FileOutputStream("dependents.xml"));*/

    }
}
