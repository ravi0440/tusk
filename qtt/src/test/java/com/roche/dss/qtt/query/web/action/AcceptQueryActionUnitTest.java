package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.QttGlobalConstants;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.Requester;
import com.roche.dss.qtt.security.service.SecurityService;
import com.roche.dss.qtt.service.InitiateService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.utility.EmailSender;
import com.roche.dss.qtt.utility.QueryStatusCodes;
import com.roche.dss.qtt.utility.config.Config;
import com.roche.dss.qtt.utility.pdf.QrfPdfInterface;
import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * User: tomasinp
 */
public class AcceptQueryActionUnitTest extends TestCase {

    private AcceptQueryAction action;
    private InitiateService initiateService = Mockito.mock(InitiateService.class);
    private QueryService queryService = Mockito.mock(QueryService.class);
    private SecurityService securityService = Mockito.mock(SecurityService.class);
    private QrfPdfInterface qrfPdf = Mockito.mock(QrfPdfInterface.class);
    private Config config = Mockito.mock(Config.class);
    protected EmailSender emailSender = Mockito.mock(EmailSender.class);
    private HashMap<String, Object> session;
    private Query query = new Query();


    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testExecuteAlreadyProcessed() throws Exception {
       action = new AcceptQueryAction();
        mockSecurity();
        action.setId(query.getQuerySeq());
        action.setQueryService(queryService);
        Mockito.when(queryService.checkHasStatus(query.getQuerySeq(), QueryStatusCodes.SUBMITTED.getCode())).thenReturn(Boolean.FALSE);
        Mockito.when(queryService.find(query.getQuerySeq())).thenReturn(query);
        assertEquals("already_processed", action.execute());
    }


    @Test
    public void testExecute() throws Exception {
        action = new AcceptQueryAction();
        mockSecurity();
        query.setQuerySeq(Long.MAX_VALUE);
        action.setId(query.getQuerySeq());
        mockServices();
        query.setRequester(new Requester());
        query.getRequester().setEmail("piotr.tomasinski@roche.com");
        assertEquals(Action.SUCCESS, action.execute());
    }



    private void mockSecurity() {
        Mockito.when(securityService.isAllowedCheck(QttGlobalConstants.DSCL_PERMISSION)).thenReturn(Boolean.TRUE);
        action.setSecurityService(securityService);
    }

    private void mockServices() {
        Mockito.when(queryService.checkHasStatus(query.getQuerySeq(), QueryStatusCodes.SUBMITTED.getCode())).thenReturn(Boolean.TRUE);
        Mockito.when(queryService.find(query.getQuerySeq())).thenReturn(query);
        action.setInitiateService(initiateService);
        action.setQueryService(queryService);
        action.setQrfPdfInterface(qrfPdf);
        action.setConfig(config);
        action.setEmailSender(emailSender);
    }
}
