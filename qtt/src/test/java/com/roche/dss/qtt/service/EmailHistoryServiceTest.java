package com.roche.dss.qtt.service;


import com.roche.dss.qtt.eao.EmailHistoryEAO;
import com.roche.dss.qtt.model.EmailHistory;
import com.roche.dss.qtt.service.impl.EmailHistoryServiceImpl;

/**
 *  Test for email history service
 *  @since 2016-06-06
 *  @author Krzysztof Palacz
 */
@RunWith(MockitoJUnitRunner.class)
public class EmailHistoryServiceTest {

    @InjectMocks
    private EmailHistoryService emailHistoryService = new EmailHistoryServiceImpl();

    @Mock
    private EmailHistoryEAO emailHistoryEAOMock;

    @Test
    public void testSaveEmailHistoryShouldSaveEmailHistory(){
        // given

        // when
        emailHistoryService.saveEmailHistory("subject", "contet", "sent to", "sent from");

        // then
        Mockito.verify(emailHistoryEAOMock, Mockito.times(1)).persist(Mockito.<EmailHistory>anyObject());
        Mockito.verify(emailHistoryEAOMock, Mockito.times(1)).flush();
    }
}
