package com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Query;

public class MedicalInterpretationActionUnitTest extends TestCase{
	private MedicalInterpretationAction action;
	private Query query;
	
	@Before
	public void setUp() throws Exception{
		super.setUp();
	}
	

	@Test
	public void test10DaysBefore() {
		query = new Query();
		query.setCurrentDueDate(new DateTime().minusDays(10));
		
		action = new MedicalInterpretationAction();
		action.setQuery(query);
		
		assertEquals(true, action.isDateExpiredWarning());
		
	}
	
	@Test
	public void test10DaysAfter() {
		query = new Query();
		query.setCurrentDueDate(new DateTime().plusDays(10));
		
		action = new MedicalInterpretationAction();
		action.setQuery(query);
		
		assertEquals(false, action.isDateExpiredWarning());
		
	}
	//this test will not pass if is executed exactly at midnight
	@Test
	public void test30SecoundsBeforeButTheSameDay() {
		query = new Query();
		query.setCurrentDueDate(new DateTime().minusSeconds(30));
		
		action = new MedicalInterpretationAction();
		action.setQuery(query);
		
		assertEquals(false, action.isDateExpiredWarning());
		
	}
}
