package com.roche.dss.qtt.model;

import junit.framework.Assert;
import junit.framework.TestCase;

public class AeTermTest extends TestCase {
	protected AeTerm o1, o1_1, o2, oNull, oNullOther;
	
	@Override
	public void setUp() {
		o1 = new AeTerm();
		o1_1 = new AeTerm();
		o2 = new AeTerm();
		oNull = new AeTerm();
		oNullOther = new AeTerm();
		
		Query q1 = new Query();
		Query q1_1 = new Query();
		Query q2 = new Query();
		
		q1.setQuerySeq(1);
		q1_1.setQuerySeq(1);
		q2.setQuerySeq(2);
		
		o1.setQuery(q1);
		o1_1.setQuery(q1_1);
		o2.setQuery(q2);
	}
	
	public void testEquals() {
		Assert.assertTrue(o1.equals(o1));
		Assert.assertTrue(o1.equals(o1_1));
		Assert.assertFalse(o1.equals(o2));
		Assert.assertFalse(o1.equals(oNull));
		Assert.assertFalse(oNull.equals(o1));
		Assert.assertFalse(o1.equals(null));
		Assert.assertFalse(oNull.equals(null));
		Assert.assertTrue(oNull.equals(oNull));
		Assert.assertFalse(oNull.equals(oNullOther));
	}
	
	public void testHashCode() {
		Assert.assertEquals(o1.hashCode(), o1_1.hashCode());
		Assert.assertNotSame(o1.hashCode(), o2.hashCode());
		Assert.assertNotSame(o1.hashCode(), oNull.hashCode());
		Assert.assertNotSame(o1.hashCode(), oNullOther.hashCode());
	}
}
