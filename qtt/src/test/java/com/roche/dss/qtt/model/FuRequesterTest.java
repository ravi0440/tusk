package com.roche.dss.qtt.model;

public class FuRequesterTest{

    @Test(expected = IllegalArgumentException.class)
	public void shouldFailOnEmptyRequester() {
        FuRequester tested = new FuRequester(null,1,null);
	}
	
    @Test(expected = IllegalArgumentException.class)
	public void shouldFailOnRequesterWithoutOrganisation() {
        Requester requester = new Requester();
        FuRequester tested = new FuRequester(requester,1,null);
	}

}
