package com.roche.dss.qtt.service;

import com.roche.dss.qtt.eao.EuRegulatoryHistoryEAO;
import com.roche.dss.qtt.model.EuRegulatoryHistory;
import com.roche.dss.qtt.service.impl.EuRegulatoryHistoryServiceImpl;

/**
 *  Test for eu regulatory history service
 *  @since 2016-06-09
 *  @author Krzysztof Palacz
 */
@RunWith(MockitoJUnitRunner.class)
public class EuRegulatoryHistoryServiceTest {

    @InjectMocks
    private EuRegulatoryHistoryService euRegulatoryHistoryService = new EuRegulatoryHistoryServiceImpl();

    @Mock
    private EuRegulatoryHistoryEAO euRegulatoryHistoryEAOMock;

    @Test
    public void testSaveEmailHistoryShouldSaveEmailHistory(){
        // given

        // when
        euRegulatoryHistoryService.saveEuRegulatoryHistory(new EuRegulatoryHistory());

        // then
        Mockito.verify(euRegulatoryHistoryEAOMock, Mockito.times(1)).persist(Mockito.<EuRegulatoryHistory>anyObject());
        Mockito.verify(euRegulatoryHistoryEAOMock, Mockito.times(1)).flush();
    }
}
