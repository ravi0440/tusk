package com.roche.dss.qtt.model;

public class DrugTest extends TestCase {
	protected Drug o1, o1_1, o2, oNull, oNullOther;
	
	public void setUp() {
		o1 = new Drug();
		o1_1 = new Drug();
		o2 = new Drug();
		oNull = new Drug();
		oNullOther = new Drug();
		o1.setDrugNumberSeq(1);
		o1_1.setDrugNumberSeq(1);
		o2.setDrugNumberSeq(2);
	}
	
	public void testEquals() {
		Assert.assertTrue(o1.equals(o1));
		Assert.assertTrue(o1.equals(o1_1));
		Assert.assertFalse(o1.equals(o2));
    //TODO PS due to using sequencer
//		Assert.assertFalse(o1.equals(oNull));
//		Assert.assertFalse(oNull.equals(o1));
//		Assert.assertFalse(o1.equals(null));
//		Assert.assertFalse(oNull.equals(null));
//		Assert.assertTrue(oNull.equals(oNull));
//		Assert.assertFalse(oNull.equals(oNullOther));
	}
	
	public void testHashCode() {
		Assert.assertEquals(o1.hashCode(), o1_1.hashCode());
		Assert.assertNotSame(o1.hashCode(), o2.hashCode());
		Assert.assertNotSame(o1.hashCode(), oNull.hashCode());
		Assert.assertNotSame(o1.hashCode(), oNullOther.hashCode());
	}
}
