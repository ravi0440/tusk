package com.roche.dss.qtt.model;

import com.roche.dss.qtt.query.comms.dto.*;
import com.roche.dss.qtt.query.web.action.AcceptQueryAction;

public class ComparisonTest extends TestCase{

	private static final Logger logger = LoggerFactory.getLogger(ComparisonTest.class);
	
	public void testEquals() {

		testClass(AttachmentDTO.class);
		testClass(CaseDTO.class);
		testClass(ClinicalTrialDTO.class);
		testClass(CountryDTO.class);
		testClass(CumulativePeriodDTO.class);
		testClass(DrugDTO.class);
		testClass(EmailDTO.class);
		testClass(OrganisationDTO.class);
		testClass(OrganisationTypeDTO.class);
		testClass(PartialQueryDTO.class);
		testClass(PregnancyDTO.class);
		testClass(PreparationDTO.class);
		testClass(PreparatoryDTO.class);
		testClass(QueryDescDTO.class);
		testClass(QueryDetailsDTO.class);
		testClass(QueryDTO.class);
		testClass(QueryListDTO.class);
		testClass(QueryTypeDTO.class);
		testClass(ReportDTO.class);
		testClass(ReporterTypeDTO.class);
		testClass(RequesterDTO.class);
		testClass(UserQueryDTO.class);
	}
	
	@SuppressWarnings("unchecked")
	private void testClass(@SuppressWarnings("rawtypes") Class clazz) {
		logger.info("Testing class: " + clazz.getCanonicalName());
		if(clazz.equals(QueryDescDTO.class) || clazz.equals(QueryDTO.class)) {
			QueryDescDTO obj1 = new QueryDescDTO();
			obj1.setNatureOfCase("A");
			QueryDescDTO obj2 = new QueryDescDTO();
			obj2.setNatureOfCase("B");
			Query q1 = new Query();
			q1.setQuerySeq(1L);
			Query q2 = new Query();
			q2.setQuerySeq(2L);
			EqualsVerifier.forClass(clazz).suppress(Warning.STRICT_INHERITANCE).suppress(Warning.NONFINAL_FIELDS).withPrefabValues(QueryDescDTO.class, obj1, obj2).withPrefabValues(Query.class, q1, q2).verify();
		} else {
			EqualsVerifier.forClass(clazz).suppress(Warning.STRICT_INHERITANCE).suppress(Warning.NONFINAL_FIELDS).verify();
		}
	}
}
