package com.roche.dss.qtt.model;

public class DrugIdTest extends TestCase {
	protected DrugId o1_1, o1_1_duplicate, o1_2, o2_1, o2_2;
	protected long ida1 = 1;
	protected long ida2 = 2;
	protected long idb1 = 10;
	protected long idb2 = 20;

	public void setUp() {
		o1_1 = new DrugId();
		o1_1_duplicate = new DrugId();
		o1_2 = new DrugId();
		o2_1 = new DrugId();
		o2_2 = new DrugId();
		
		o1_1.setDrugNumberSeq(ida1);
		o1_1.setQuerySeq(idb1);
		
		o1_1_duplicate.setDrugNumberSeq(ida1);
		o1_1_duplicate.setQuerySeq(idb1);
		
		o1_2.setDrugNumberSeq(ida1);
		o1_2.setQuerySeq(idb2);
		
		o2_1.setDrugNumberSeq(ida2);
		o2_1.setQuerySeq(idb1);
		
		o2_2.setDrugNumberSeq(ida2);
		o2_2.setQuerySeq(idb2);
	}
	
	public void testEquals() {
		Assert.assertTrue(o1_1.equals(o1_1));
		Assert.assertTrue(o1_1.equals(o1_1_duplicate));
		
		Assert.assertFalse(o1_1.equals(o1_2));
		Assert.assertFalse(o1_1.equals(o2_1));
		Assert.assertFalse(o1_1.equals(o2_2));
		
		Assert.assertEquals(o1_1.hashCode(), o1_1_duplicate.hashCode());
		Assert.assertNotSame(o1_1.hashCode(), o1_2.hashCode());
		Assert.assertNotSame(o1_1.hashCode(), o2_1.hashCode());
		Assert.assertNotSame(o1_1.hashCode(), o2_2.hashCode());
	}
}
