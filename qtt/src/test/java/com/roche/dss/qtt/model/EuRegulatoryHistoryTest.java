package com.roche.dss.qtt.model;

public class EuRegulatoryHistoryTest {

    private EuRegulatoryHistory objectUnderTest;
    private static final String MODIFIED_BY = "palaczk";
    private static final Long QUERY_SEQ = 3l;
    private static final Long FOLLOWUP_SEQ = 10l;
    private static final String VALUE = "Y";
    private static final DateTime MODIFICATION_DATE = new DateTime();

    @Before
    public void setup() throws Exception {
        objectUnderTest = new EuRegulatoryHistory();
    }

    @Test
    public void testGetters(){
        // given

        // when
        objectUnderTest = createEmailHsitory();

        // then
        assertEquals(MODIFIED_BY, objectUnderTest.getModifiedBy() );
        assertEquals(QUERY_SEQ, objectUnderTest.getQuerySeq() );
        assertEquals(FOLLOWUP_SEQ, objectUnderTest.getFollowupSeq() );
        assertEquals(VALUE, objectUnderTest.getValue() );
        assertEquals(MODIFICATION_DATE, objectUnderTest.getModificationDate() );
    }

    private EuRegulatoryHistory createEmailHsitory() {
        EuRegulatoryHistory euHistory = new EuRegulatoryHistory();
        euHistory.setModifiedBy(MODIFIED_BY);
        euHistory.setQuerySeq(QUERY_SEQ);
        euHistory.setFollowupSeq(FOLLOWUP_SEQ);
        euHistory.setValue(VALUE);
        euHistory.setModificationDate(MODIFICATION_DATE);
        return euHistory;
    }
}
