package com.roche.dss.qtt.model;

public class QueryTest extends TestCase {
	protected Query o1, o1_1, o2, oNull;
	protected long id1 = 1;
	protected long id2 = 2;
	
	public void setUp() {
		o1 = new Query();
		o1_1 = new Query();
		o2 = new Query();
		o1.setQuerySeq(id1);
		o1_1.setQuerySeq(id1);
		o2.setQuerySeq(id2);		
	}
	
	public void testEquals() {
		Assert.assertTrue(o1.equals(o1));
		Assert.assertTrue(o1.equals(o1_1));
		Assert.assertFalse(o1.equals(o2));
		Assert.assertFalse(o1.equals(oNull));
	}
	
	public void testHashCode() {
		Assert.assertEquals(o1.hashCode(), o1_1.hashCode());
		Assert.assertNotSame(o1.hashCode(), o2.hashCode());
	}
}
