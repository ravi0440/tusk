package com.roche.dss.qtt.model;

public class EmailHistoryTest {

    private EmailHistory objectUnderTest;

    private static final String TEST_CONTENT = "test content";
    private static final String TEST_SUBJCET = "test subjcet";
    private static final String SENT_TO = "kpalacz@contractors.roche.com.";
    private static final String SENT_FROM = "kpalacz@roche.com.";
    private static final DateTime SENT_DATE = new DateTime();


    @Before
    public void setup() throws Exception {
        objectUnderTest = new EmailHistory();
    }

    @Test
    public void testGetters(){
        // given

        // when
        objectUnderTest = createEmailHsitory();

        // then
        assertEquals(TEST_CONTENT, objectUnderTest.getContent() );
        assertEquals(TEST_SUBJCET, objectUnderTest.getSubject() );
        assertEquals(SENT_TO, objectUnderTest.getSentTo() );
        assertEquals(SENT_FROM, objectUnderTest.getSentFrom() );
        assertEquals(SENT_DATE, objectUnderTest.getSentDate() );
    }

    private EmailHistory createEmailHsitory() {
        EmailHistory emailHistory = new EmailHistory();
        emailHistory.setContent(TEST_CONTENT);
        emailHistory.setSubject(TEST_SUBJCET);
        emailHistory.setSentTo(SENT_TO);
        emailHistory.setSentFrom(SENT_FROM);
        emailHistory.setSentDate(SENT_DATE);

        return emailHistory;
    }

}
