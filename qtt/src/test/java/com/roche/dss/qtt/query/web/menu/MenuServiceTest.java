package com.roche.dss.qtt.query.web.menu;

import java.util.List;

import com.roche.dss.qtt.security.service.SecurityService;
import com.roche.dss.qtt.QttGlobalConstants;

public class MenuServiceTest implements QttGlobalConstants {
	SecurityService mockedSecurityService = mock(SecurityService.class);
	MenuServiceImpl menuService;
	Menu home = new Menu(MenuItem.HOME.getValue(), MenuItem.HOME.getLink());
	Menu qrf = new Menu(MenuItem.QRF.getValue(), MenuItem.QRF.getLink());
	Menu queryList = new Menu(MenuItem.QRF.getValue(), MenuItem.QRF.getLink());
	Menu queryListUsers = new Menu(MenuItem.QUERY_LIST_UESERS.getValue(), MenuItem.QUERY_LIST_UESERS.getLink());
	Menu initiate =  new Menu(MenuItem.INITIATE.getValue(), MenuItem.INITIATE.getLink());
	Menu search = new Menu(MenuItem.SEARCH.getValue(), MenuItem.SEARCH.getLink());
	Menu quickSearch = new Menu(MenuItem.QUICK_SEARCH.getValue(), MenuItem.QUICK_SEARCH.getLink());
	Menu reports = new Menu(MenuItem.REPORTS.getValue(), MenuItem.REPORTS.getLink());
	Menu help = new Menu(MenuItem.HELP.getValue(), MenuItem.HELP.getLink());
	Menu about = new Menu(MenuItem.ABOUT.getValue(), MenuItem.ABOUT.getLink());

	@Before
	public void cleanSecurityService() {
		stub(mockedSecurityService.isAllowedCheck(DSCL_PERMISSION)).toReturn(
				false);
		stub(mockedSecurityService.isAllowedCheck(DMG_COORD_PERMISSION))
				.toReturn(false);
		stub(mockedSecurityService.isAllowedCheck(DMG_SCI_PERMISSION))
				.toReturn(false);
		stub(mockedSecurityService.isAllowedCheck(PS_PERMISSION)).toReturn(
				false);
		stub(mockedSecurityService.isAllowedCheck(AFFILIATE_PERMISSION))
				.toReturn(false);
		stub(mockedSecurityService.isAllowedCheck(PDS_USER_PERMISSION))
				.toReturn(false);
		menuService = new MenuServiceImpl();
		menuService.setSecurityService(mockedSecurityService);
	}

	@Test
	public void testDsclAllowed(){
		stub(mockedSecurityService.isAllowedCheck(DSCL_PERMISSION)).toReturn(true);
		List<Menu> menuList = menuService.getMenuList();
		assertEquals(10, menuList.size() );
		assertTrue(menuList.contains(home));
		assertTrue(menuList.contains(qrf));
		assertTrue(menuList.contains(queryList));
		assertTrue(menuList.contains(queryListUsers));
		assertTrue(menuList.contains(initiate));
		assertTrue(menuList.contains(search));
		assertTrue(menuList.contains(quickSearch));
		assertTrue(menuList.contains(reports));
		assertTrue(menuList.contains(help));		
		assertTrue(menuList.contains(about));		
	}
	
	
	@Test
	public void testDmgCordinatorAllowed(){
		stub(mockedSecurityService.isAllowedCheck(DMG_COORD_PERMISSION)).toReturn(true);
		List<Menu> menuList = menuService.getMenuList();
		assertEquals(8, menuList.size() );
		assertTrue(menuList.contains(home));
		assertTrue(menuList.contains(qrf));
		assertTrue(menuList.contains(queryList));
		assertTrue(menuList.contains(search));
		assertTrue(menuList.contains(quickSearch));
		assertTrue(menuList.contains(reports));
		assertTrue(menuList.contains(help));
		assertTrue(menuList.contains(about));			
	}
	
	@Test
	public void testdmgSciAllowed(){
		stub(mockedSecurityService.isAllowedCheck(DMG_SCI_PERMISSION)).toReturn(true);
		List<Menu> menuList = menuService.getMenuList();
		assertEquals(7, menuList.size() );		
		assertTrue(menuList.contains(home));
		assertTrue(menuList.contains(qrf));
		assertTrue(menuList.contains(queryList));
		assertTrue(menuList.contains(search));
		assertTrue(menuList.contains(quickSearch));
		assertTrue(menuList.contains(help));
		assertTrue(menuList.contains(about));				
	}
	
	@Test
	public void testPsAllowed(){
		stub(mockedSecurityService.isAllowedCheck(PS_PERMISSION)).toReturn(true);
		List<Menu> menuList = menuService.getMenuList();
		assertEquals(7, menuList.size() );
		assertTrue(menuList.contains(home));
		assertTrue(menuList.contains(qrf));
		assertTrue(menuList.contains(queryList));
		assertTrue(menuList.contains(search));
		assertTrue(menuList.contains(quickSearch));
		assertTrue(menuList.contains(help));
		assertTrue(menuList.contains(about));		
	}
	
	@Test
	public void testAffiliateAllowed(){
		stub(mockedSecurityService.isAllowedCheck(AFFILIATE_PERMISSION)).toReturn(true);
		List<Menu> menuList = menuService.getMenuList();
		assertEquals(5, menuList.size() );
		assertTrue(menuList.contains(qrf));
		assertTrue(menuList.contains(search));
		assertTrue(menuList.contains(quickSearch));
		assertTrue(menuList.contains(help));
		assertTrue(menuList.contains(about));		
	}
	
	@Test
	public void testPdsUserAllowed(){
		stub(mockedSecurityService.isAllowedCheck(PDS_USER_PERMISSION)).toReturn(true);
		List<Menu> menuList = menuService.getMenuList();
		assertEquals(5, menuList.size() );
		assertTrue(menuList.contains(qrf));
		assertTrue(menuList.contains(queryList));
		assertTrue(menuList.contains(reports));
		assertTrue(menuList.contains(help));
		assertTrue(menuList.contains(about));		
	}
}
