package com.roche.dss.util;

import com.roche.dss.qtt.query.web.model.ReportCriteria;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Test class for ApplicationUtils
 * <p/>
 * User: pruchnil
 */
public class ApplicationUtilsTest {

    @Test
    public void testCalculateDaysBetween() throws Exception {
        ReportCriteria criteria = new ReportCriteria();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
            criteria.setDateFrom(sdf.parse("12/11/2010"));
            criteria.setDateTo(sdf.parse("19/11/2010"));
        } catch (Exception e) {
            fail("Date parsing error");
        }
        int diff = ApplicationUtils.calculateDaysBetween(criteria.getDateFrom(), criteria.getDateTo());
        assertEquals(7, diff);
    }

    @Test
    public void testConvertToBoolean() {
        assertTrue(ApplicationUtils.convertToBoolean("Y"));
        assertFalse(ApplicationUtils.convertToBoolean("N"));
    }

    @Test
    public void testBooleanToChar() {
        assertEquals('N', ApplicationUtils.booleanToChar(false));
        assertEquals('Y', ApplicationUtils.booleanToChar(true));
    }

}
