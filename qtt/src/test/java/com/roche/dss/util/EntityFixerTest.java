package com.roche.dss.util;

import com.roche.dss.qtt.filter.Requestfilter;

/**
 * Created by farynam on 01/05/17.
 */
public class EntityFixerTest {


    @Test
    public void testBeginningEnd() {
        String input = "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">ALA</P>\n" +
                "&NBSP;&NBSP;<P STYLE=\"MARGIN-BOTTOM: 0IN;\">aaa</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">MA</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">aaa</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">aaa</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">KOTA</P>&NBSP;";

        String expected = "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">ALA</P>\n" +
                "&#160;&#160;<P STYLE=\"MARGIN-BOTTOM: 0IN;\">aaa</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">MA</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">aaa</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">aaa</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">KOTA</P>&#160;";


        String [] values = Requestfilter.filter(new String [] {input});

        Assert.assertEquals(expected, values[0]);
    }

    @Test
    public void testNBSP() {
        String input = "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">ALA</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&NBSP;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">MA</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&NBSP;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&NBSP;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">KOTA</P>";

        String expected = "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">ALA</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&#160;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">MA</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&#160;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&#160;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">KOTA</P>";


        String [] values = Requestfilter.filter(new String [] {input});

        Assert.assertEquals(expected, values[0]);
    }

    @Test
    public void testNestedNBSP() {
        String input = "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">ALA</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&N&NBSP;BSP;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">MA</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&NBSP&NBSP;;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&&NBSPNBSP;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">KOTA</P>";

        String expected = "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">ALA</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&N&#160;BSP;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">MA</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&NBSP&#160;;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&&NBSPNBSP;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">KOTA</P>";

        String [] values = Requestfilter.filter(new String [] {input});

        Assert.assertEquals(expected, values[0]);
    }

    @Test
    public void testEmpty() {
        String input = "";
        String expected = "";

        String [] values = Requestfilter.filter(new String [] {input});

        Assert.assertEquals(expected, values[0]);
        values = Requestfilter.filter(null);

        Assert.assertNull(values);
    }

    @Test
    public void testAlreadySubstituted() {
        String input = "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">ALA</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&#160;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">MA</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&#160;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&#160;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">KOTA</P>";
        String expected = "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">ALA</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&#160;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">MA</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&#160;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">&#160;</P>\n" +
                "<P STYLE=\"MARGIN-BOTTOM: 0IN;\">KOTA</P>";

        String [] values = Requestfilter.filter(new String [] {input});

        Assert.assertEquals(expected, values[0]);
    }



}
