package itest.com.roche.dss.qtt.action;

import com.roche.dss.qtt.query.comms.dto.ReportDTO;
import com.roche.dss.qtt.query.web.action.ReportAction;
import com.roche.dss.qtt.query.web.model.ReportCriteria;
import com.roche.dss.qtt.service.ReportService;
import com.roche.dss.qtt.utility.WorkflowRoute;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static com.roche.dss.qtt.query.web.action.CommonActionSupport.*;

/**
 * User: pruchnil
 */
public class ReportActionTest extends BaseStrutsTestCase {
    private ReportAction action;

    public void testExecute() throws Exception {
        action = createAction(ReportAction.class, null, "ReportMenu");
        assertEquals(Action.SUCCESS, proxy.execute());

        assertNotNull(action.getRoutes());
        assertTrue(action.getRoutes().size() > 0);
    }

    public void testCreateReport() throws Exception {
        final ReportService reportServiceMock = Mockito.mock(ReportService.class);
        Mockito.when(reportServiceMock.retrieveReportData(Mockito.any(ReportCriteria.class))).thenReturn(new ArrayList<ReportDTO>());
        action = createAction(ReportAction.class, null, "TrackingReport");

        ReportCriteria reportCriteria = new ReportCriteria();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
        reportCriteria.setDateFrom(sdf.parse("12/02/2010"));
        action.setReportCriteria(reportCriteria);
        action.setReportService(reportServiceMock);

        assertEquals(SUCCESS, proxy.execute());
        assertEquals(0, action.getReportList().size());
    }

    public void testValidate() throws Exception {
        action = createAction(ReportAction.class, null, "TrackingReport");
        ReportCriteria reportCriteria = new ReportCriteria();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
        reportCriteria.setDateFrom(sdf.parse("12/02/2010"));
        reportCriteria.setDateTo(sdf.parse("01/02/2010"));
        reportCriteria.setRoute(WorkflowRoute.A1.getName());
        action.setReportCriteria(reportCriteria);

        assertEquals(INPUT, proxy.execute());

        assertTrue(action.hasActionErrors());
        assertEquals(2, action.getActionErrors().size());
    }
}
