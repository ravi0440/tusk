package itest.com.roche.dss.qtt.action;

import com.roche.dss.qtt.query.web.action.AttachmentAction;
import com.roche.dss.qtt.query.web.action.CommonActionSupport;
import com.roche.dss.qtt.query.web.model.FileUploadBean;
import com.roche.dss.qtt.security.service.SecurityService;
import com.roche.dss.qtt.service.AttachmentService;
import com.roche.dss.qtt.service.FileOperationsService;
import java.io.File;

/**
 * User: pruchnil
 */
public class AttachmentActionTest extends BaseStrutsTestCase {
    private static final Logger logger = LoggerFactory.getLogger(AttachmentActionTest.class);
    private AttachmentAction action;
    private AttachmentService attachmentServiceMock = Mockito.mock(AttachmentService.class);
    private SecurityService securityServiceMock = Mockito.mock(SecurityService.class);
    private static final long queryId = 20262l;
    private static final long attachId = 160897l;

    public void testExecute() throws Exception {
        action = createAction(AttachmentAction.class, null, "AddNewAttachment");
        action.setQueryId(queryId);
        assertEquals(SUCCESS, proxy.execute());
    }

    public void testView() throws Exception {
        action = createAction(AttachmentAction.class, null, "Attachment");
        action.setQueryId(queryId);
        action.setAttachId(attachId);
        assertEquals(SUCCESS, proxy.execute());
        assertNotNull(action.getInputStream());
        assertEquals("application/pdf", action.getContentType());
    }

    public void testUpload() throws Exception {
        action = createAction(AttachmentAction.class, null, "UploadAttachment");
        action.setQueryId(queryId);
        action.setClosed(true);
        action.setSecurityService(securityServiceMock);
        action.setAttachmentService(attachmentServiceMock);
        action.setAttachment(prepareFileUpload("GivenName", "src/test/resources/testAttachments/pl1.doc"));
        assertEquals(SUCCESS, proxy.execute());
        assertEquals("ClosedQueryDetails", action.getNextActionName());
    }

    public void testUploadEmptyFile() throws Exception {
        action = createAction(AttachmentAction.class, null, "UploadAttachment");
        action.setQueryId(queryId);
        action.setClosed(true);
        action.setSecurityService(securityServiceMock);
        action.setAttachmentService(attachmentServiceMock);
        action.setAttachment(prepareFileUpload("GivenName", "file.pdf"));
        assertEquals(INPUT, proxy.execute());
        assertEquals(1, action.getActionErrors().size());
        assertEquals(action.getText("attach.file.empty"), action.getActionErrors().iterator().next());
    }

    public void testUploadEmptyGivenName() throws Exception {
        action = createAction(AttachmentAction.class, null, "UploadAttachment");
        action.setQueryId(queryId);
        action.setClosed(true);
        action.setSecurityService(securityServiceMock);
        action.setAttachmentService(attachmentServiceMock);
        action.setAttachment(prepareFileUpload("", "src/test/resources/testAttachments/pl1.doc"));
        assertEquals(INPUT, proxy.execute());
        assertEquals(1, action.getActionErrors().size());
        assertEquals(action.getText("attach.name.required"), action.getActionErrors().iterator().next());
    }

    private FileUploadBean prepareFileUpload(String name, String filePath) {
        FileUploadBean file = new FileUploadBean();
        file.setGivenFileName(name);
        file.setType(AttachmentAction.AttachCategory.SUPPORT_DOC.getName());
        file.setUpload(new File(filePath));
        return file;
    }

}
