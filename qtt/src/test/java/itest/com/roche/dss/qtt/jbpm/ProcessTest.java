package itest.com.roche.dss.qtt.jbpm;


public class ProcessTest extends TestCase{
	
	public void testEasyProcess() {
		
		ProcessDefinition processDefinition = ProcessDefinition.parseXmlString(
		    "<process-definition>" +
		    "  <start-state>" +
		    "    <transition to='s' />" +
		    "  </start-state>" +
		    "  <state name='s'>" +
		    "    <transition to='end' />" +
		    "  </state>" +
		    "  <end-state name='end' />" +
		    "</process-definition>"
		);
		  
		ProcessInstance processInstance = new ProcessInstance(processDefinition);
		  
		Token token = processInstance.getRootToken();
		  
		assertSame(processDefinition.getStartState(), token.getNode());
		
		token.signal();

		assertSame(processDefinition.getNode("s"), token.getNode());

		token.signal();
		  
		assertSame(processDefinition.getNode("end"), token.getNode());
	}
	  
	
}