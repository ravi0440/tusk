package itest.com.roche.dss.qtt.action;

import com.roche.dss.qtt.query.web.action.OtherLoginAction;
import com.roche.dss.qtt.query.web.action.QueryListAction;
import com.roche.dss.qtt.security.service.SecurityService;
import static com.roche.dss.qtt.query.web.action.OtherLoginAction.*;
import static com.roche.dss.qtt.query.web.action.OtherLoginAction.Domain.*;

/**
 * User: pruchnil
 */
public class OtherLoginActionTest  extends BaseStrutsTestCase {
    private OtherLoginAction action;

    @Test
    public void testExecute() throws Exception {
        action = createAction(OtherLoginAction.class, null, "LoginQueryList");
        assertEquals(SUCCESS, proxy.execute());
        assertEquals(5, action.getDomainList().length);
    }

    @Test
    public void testLoginNonDomain() throws Exception {
        action = createAction(OtherLoginAction.class, null, "OtherLogin");
        action.setDomain(NONDOMAIN.getKey());
        action.setLogin("user@mail.com");
        action.setPassword("password");
        assertEquals(SUCCESS, proxy.execute());
    }

    @Test
    public void testLoginDomain() throws Exception {
        action = createAction(OtherLoginAction.class, null, "OtherLogin");
        action.setDomain(EMEA.getKey());
        action.setLogin("dscl1");
        action.setPassword("Orange12");
        assertEquals(SUCCESS, proxy.execute());
    }

    @Test
    public void testLoginNonDomainEmptyPassword() throws Exception {
        action = createAction(OtherLoginAction.class, null, "OtherLogin");
        action.setDomain(NONDOMAIN.getKey());
        action.setLogin("user@mail.com");
        assertEquals(INPUT, proxy.execute());
    }

    @Test
    public void testLoginDomainIncorrectPassword() throws Exception {
        action = createAction(OtherLoginAction.class, null, "OtherLogin");
        action.setDomain(ASIA.getKey());
        action.setLogin("dscl1");
        action.setPassword("aaa");
        assertEquals(INPUT, proxy.execute());
    }
}
