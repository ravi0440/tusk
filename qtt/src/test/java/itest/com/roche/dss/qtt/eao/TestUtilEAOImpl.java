package itest.com.roche.dss.qtt.eao;


import com.roche.dss.qtt.jbpm.entity.ProcessInstance;
import com.roche.dss.qtt.model.*;
import com.roche.dss.qtt.utility.Activities;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository("testUtilEAO")
public class TestUtilEAOImpl implements TestUtilEAO {
	@PersistenceContext
	protected EntityManager em;
	
	protected QueryStatusCode statusCodeClosed;
	
	protected QueryType queryType;
	
	protected ReporterType reporterType;
	
	protected OrganisationType organisationType;
	
	protected Organisation organisation;
	
	protected QttCountry country;
	
	protected Requester requester;
	
	protected QueryStatusCode statusCode2;
	
	protected QueryType queryType2;
	
	protected ReporterType reporterType2;
	
	protected OrganisationType organisationType2;
	
	protected Organisation organisation2;
	
	protected QttCountry country2;
	
	protected Requester requester2;
	
	protected QueryDescription queryDescription;
	
	protected PqpType pqpType;
	
	protected PreQueryPreparation pqp;
	
	protected PrgyExptype prgyType;
	
	protected AttachmentCategory attCat;
	
	protected CtrlOformat format;
	
	protected LdocType ldocType;
	
	@Override
	public void tearDown() {
		if (requester != null) {
			requester = em.merge(requester);
			em.remove(requester);
			requester = null;
		}
		
		if (country != null) {
			country = em.merge(country);
			em.remove(country);
			country = null;
		}
		
		if (organisation != null) {
			organisation = em.merge(organisation);
			em.remove(organisation);
			organisation = null;
		}
		
		if (organisationType != null) {
			organisationType = em.merge(organisationType);
			em.remove(organisationType);
			organisationType = null;
		}
		
		if (reporterType != null) {
			reporterType = em.merge(reporterType);
			em.remove(reporterType);
			reporterType = null;
		}
		
		if (queryType != null) {
			queryType = em.merge(queryType);
			em.remove(queryType);
			queryType = null;
		}
		
		if (ldocType != null) {
			ldocType = em.merge(ldocType);
			em.remove(ldocType);
			ldocType = null;
		}
		
		if (format != null) {
			format = em.merge(format);
			em.remove(format);
			format = null;
		}
		
		if (attCat != null) {
			attCat = em.merge(attCat);
			em.remove(attCat);
			attCat = null;
		}
		
		if (prgyType != null) {
			prgyType = em.merge(prgyType);
			em.remove(prgyType);
			prgyType = null;
		}
		
		/*if (pqp != null) {
			pqp = em.merge(pqp);
			em.remove(pqp);
			pqp = null;
		}*/
		
		if (pqpType != null) {
			pqpType = em.merge(pqpType);
			em.remove(pqpType);
			pqpType = null;
		}
		
		/*if (queryDescription != null) {
			queryDescription = em.merge(queryDescription);
			em.remove(queryDescription);
			queryDescription = null;
		}*/
		
		if (requester2 != null) {
			requester2 = em.merge(requester2);
			em.remove(requester2);
			requester2 = null;
		}
		
		if (country2 != null) {
			country2 = em.merge(country2);
			em.remove(country2);
			country2 = null;
		}
		
		if (organisation2 != null) {
			organisation2 = em.merge(organisation2);
			em.remove(organisation2);
			organisation2 = null;
		}
		
		if (organisationType2 != null) {
			organisationType2 = em.merge(organisationType2);
			em.remove(organisationType2);
			organisationType2 = null;
		}
		
		if (reporterType2 != null) {
			reporterType2 = em.merge(reporterType2);
			em.remove(reporterType2);
			reporterType2 = null;
		}
		
		if (queryType2 != null) {
			queryType2 = em.merge(queryType2);
			em.remove(queryType2);
			queryType2 = null;
		}
		
		if (statusCode2 != null) {
			statusCode2 = em.merge(statusCode2);
			em.remove(statusCode2);
			statusCode2 = null;
		}
	}
	
	@Override
	public Query createQueryWithMandatory() {
		Query query = new Query();
		
		statusCodeClosed = em.find(QueryStatusCode.class,  QueryStatusCode.CODE_CLOSED);
		if (statusCodeClosed == null) {
			statusCodeClosed = new QueryStatusCode(QueryStatusCode.CODE_CLOSED);
			statusCodeClosed.setStatus("Closed");
			em.persist(statusCodeClosed);
		}
		
		queryType = em.getReference(QueryType.class, QueryType.Code.DATA_SEARCH);
		
		reporterType = new ReporterType(" ");
		reporterType.setReporterTypeId((short) 15);
		em.persist(reporterType);
		
		organisationType = new OrganisationType(" ");
		organisationType.setTypeId(5);
		em.persist(organisationType);

		organisation = new Organisation(organisationType, " ");
		em.persist(organisation);
		
		country = new QttCountry(" ", "");
		em.persist(country);
		
		country2 = new QttCountry("TCOU1", "testCount1");
		em.persist(country2);
		
		requester = new Requester(organisation, " ", " ", " ", country, new DateTime(), " ");
		em.persist(requester);
		
		query.setQueryStatusCode(statusCodeClosed);
		query.setQueryType(queryType);
		query.setReporterType(reporterType);
		query.setRequester(requester);
		query.setAllDrugsFlag('N');
		query.setAlertFlag('N');
		query.setUrgencyFlag('N');
		query.setFollowupFlag ('N');
		query.setAffiliateAccess('N');
		query.setReopen('N');
		query.setSourceCountryCode(" ");
		
		return query;
	}

	@Override
	public void updateQuery2(Query query) {
		queryType2 = em.getReference(QueryType.class, QueryType.Code.DATA_SEARCH);
		
		reporterType2 = new ReporterType("testReporterType1");
		reporterType2.setReporterTypeId((short) 16);
		em.persist(reporterType2);
		
		organisationType2 = new OrganisationType(" ");
		organisationType2.setTypeId(6);
		em.persist(organisationType2);
		
		organisation2 = new Organisation(organisationType2, " ");
		em.persist(organisation2);

		requester2 = new Requester(organisation2, "testReqFirstName1", "testReqSurname1", "testReqPhone1", country2, new DateTime(), "testEmail@test.com1");
		requester2.setFaxNumber("testReqFax1");
		em.persist(requester2);
		
		queryDescription = new QueryDescription(query, "testNatureOfCase1", "testIndexCase1"
				, "testPrevMedicalHistory1", "testSignsAndSymptoms1", "testConmeds1", "testInvestigations1", "testDiagnoses1", "testOtherInfo1");
		em.persist(queryDescription);
		
		pqpType = new PqpType(TestUtil.ID_NOT_EXISTING2_SHORT, "testPqpType1");
		em.persist(pqpType);

		PreQueryPreparationId pqpId = new PreQueryPreparationId(query.getQuerySeq(), pqpType.getPqpTypeId());
		pqp = new PreQueryPreparation(pqpId, query, pqpType);
		pqp.setOtherComment("testPqpOtherComment1");
		em.persist(pqp);
		Set<PreQueryPreparation> pqps = new HashSet<PreQueryPreparation>();
		pqps.add(pqp);
		
		prgyType = new PrgyExptype(TestUtil.ID_NOT_EXISTING2_LONG, "tstPrgy1");
		em.persist(prgyType);

		Pregnancy pregnancy = new Pregnancy(query.getQuerySeq());
		pregnancy.setPrgyExptype(prgyType);
		pregnancy.setOutcome("testPregnancyOutcome1");
		em.persist(pregnancy);
		
		PerformanceMetric pm = new PerformanceMetric(query, "testPerformanceMetric1");
		em.persist(pm);
		
		AffiliateNote affNote = new AffiliateNote(query, "testUser1", new DateTime(), "testAffiliateNoteText1");
		em.persist(affNote);
		Set<AffiliateNote> affNotes = new HashSet<AffiliateNote> ();
		affNotes.add(affNote);
		
		NonAffiliateNote nonAffNote = new NonAffiliateNote(query, "testUser1", new DateTime(), "testNonAffiliateNoteText1");
		em.persist(nonAffNote);
		Set<NonAffiliateNote> nonAffNotes = new HashSet<NonAffiliateNote> ();
		nonAffNotes.add(nonAffNote);
		
		Drug drug = new Drug(query);
		drug.setDose("testDose1");
		drug.setDrugRetrievalName("testDrugRetrievalName1");
		drug.setFirstFlag('Y');
		drug.setFormulation("testFormulation1");
		drug.setIndication("testIndication1");
		drug.setInnGenericName("testInnGenericName1");
		drug.setRocheDrugFlag('N');
		drug.setRoute("testRoute1");
		em.persist(drug);
		List<Drug> drugs = new ArrayList<Drug>();
		drugs.add(drug);
		// the interacting drug
		drug = new Drug(query);
		drug.setDose("testDose2");
		drug.setDrugRetrievalName("testDrugRetrievalName2");
		// it denotes an interacting drug
		drug.setFirstFlag('N');
		drug.setFormulation("testFormulation2");
		drug.setIndication("testIndication2");
		drug.setInnGenericName("testInnGenericName2");
		drug.setRocheDrugFlag('N');
		drug.setRoute("testRoute2");
		em.persist(drug);
		drugs.add(drug);

		format = new CtrlOformat("testCtrlOformat1");
		em.persist(format);
		
		ClinicalTrial trial = new ClinicalTrial(query.getQuerySeq(), format,
				"testCaseSelection1", "testProtocolNumber1",
				"testPatientNumber1", "testCrtnNumber1");
		em.persist(trial);
		
		Case qcase = new Case(query, "testAerNumber1", "testLocalReferenceNumber1",
				"testExternalReferenceNumber1");
		em.persist(qcase);
		List<Case> cases = new ArrayList<Case>();
		cases.add(qcase);
		
		AeTerm aeTerm = new AeTerm(query, "testAeTermDescription1");
		em.persist(aeTerm);
		
		ldocType = new LdocType("testldocType1");
		ldocType.setLdocTypeId(3);
		em.persist(ldocType);
			
		Set<LdocType> ldocTypes = new HashSet<LdocType>();
		ldocTypes.add(ldocType);
		
		RetrievalPeriod period = new RetrievalPeriod(query.getQuerySeq(), "Y");
		period.setInterval("testInterval1");
		DateTime startTs = new DateTime(2002, 1, 1, 0, 0, 0, 0);
		period.setStartTs(startTs);
		DateTime endTs = new DateTime(2002, 1, 4, 0, 0, 0, 0);
		period.setEndTs(endTs);		
		em.persist(period);
		
		AttachmentCategory attCat = null;
		try {
			attCat = em.find(AttachmentCategory.class, AttachmentCategory.CATEGORY_FINAL_RESPONSE);
		} catch (PersistenceException e) {
		}
		if (attCat == null) {
			attCat = new AttachmentCategory(AttachmentCategory.CATEGORY_FINAL_RESPONSE, "Final Response", 'Y');
			em.persist(attCat);
		}
		
		// Attachment: Final Response
		Attachment att = new Attachment(query,
				attCat, new DateTime(),
				"testAttachmentTitle2", 'Y', "testFilename2",
				"testAuthor2", 'Y', "testAttFup2",
				TestUtil.ID_NOT_EXISTING2_LONG);
		em.persist(att);
		Set<Attachment> atts = new HashSet<Attachment>();
		atts.add(att);
		
		query.setQueryType(queryType2);
		query.setReporterType(reporterType2);
		query.setRequester(requester2);
		query.setQueryDescription(queryDescription);
		query.setPreQueryPreparations(pqps);
		query.setPregnancy(pregnancy);
		query.setPerformanceMetric(pm);
		query.setAffiliateNotes(affNotes);
		query.setNonAffiliateNotes(nonAffNotes);
		query.setDrugs(drugs);
		query.setTrial(trial);
		query.setCases(cases);
		query.setAeTerm(aeTerm);
		query.setLabelingDocTypes(ldocTypes);
		query.setRetrievalPeriod(period);
		query.setAttachments(atts);
		
	}
	
	@Override
	public void updateQuery3(Query query) {
		attCat = new AttachmentCategory(TestUtil.ID_NOT_EXISTING_BYTE, " ", 'Y');
		em.persist(attCat);
		
		// Attachment: other
		Attachment att = new Attachment(query,
				attCat, new DateTime(),
				"testAttachmentTitle1", 'Y', "testFilename1",
				"testAuthor1", 'Y', "testAttFup1",
				TestUtil.ID_NOT_EXISTING2_LONG);
		em.persist(att);
		
		query.getAttachments().add(att);
		
		org.jbpm.graph.exe.ProcessInstance processInternal = new org.jbpm.graph.exe.ProcessInstance();
		processInternal.setVersion(1);
		em.persist(processInternal);
		
		ProcessInstance process = new ProcessInstance(processInternal);
		Set<TaskInstance> taskInstances = new HashSet<TaskInstance>(0);
		process.setTaskInstances(taskInstances);
		
		DateTime startTs = new DateTime(2003, 1, 1, 0, 0, 0, 0);
		TaskInstance task = new TaskInstance();
		task.setName(Activities.JOB_START.getTaskName());
		task.setCreate(startTs.toDate());
		task.setActorId("testResponsible0");
		em.persist(task);
		
		process.getTaskInstances().add(task);
		task.setProcessInstance(processInternal);
		
		DateTime stopTs = new DateTime(2003, 1, 2, 0, 0, 0, 0);
		
		task = new TaskInstance();
		task.setName(Activities.SEND_TO_REQUESTER.getTaskName());
		task.setCreate(stopTs.toDate());
		em.persist(task);
		
		process.getTaskInstances().add(task);
		task.setProcessInstance(processInternal);
		
		// dsclMember
		task = new TaskInstance();
		task.setName(Activities.ASSIGN.getTaskName());
		task.setCreate(stopTs.toDate());
		task.setActorId("testResponsible11");
		em.persist(task);
		
		process.getTaskInstances().add(task);
		task.setProcessInstance(processInternal);
		
		// dmgMember
		stopTs = new DateTime(2003, 1, 3, 0, 0, 0, 0);
		task = new TaskInstance();
		task.setName(Activities.DMG_ASSESS.getTaskName());
		task.setCreate(stopTs.toDate());
		task.setActorId("testResponsible7");
		em.persist(task);
		
		process.getTaskInstances().add(task);
		task.setProcessInstance(processInternal);
		
		// pSMember
		stopTs = new DateTime(2003, 1, 4, 0, 0, 0, 0);
		task = new TaskInstance();
		task.setName(Activities.MEDICAL_INTERPRETATION_I.getTaskName());
		task.setCreate(stopTs.toDate());
		task.setActorId("testResponsible3");
		em.persist(task);
		
		process.getTaskInstances().add(task);
		task.setProcessInstance(processInternal);
		
		query.setProcessInstanceId(process.getId());
		query.setProcessInstance(process);
		
		em.flush();
	}
	
	@Override
	public void updateQuery4(Query query) {
		statusCode2 = new QueryStatusCode("STA1");
		statusCode2.setStatus("testStatusCode1");
		em.persist(statusCode2);
		
		query.setQueryStatusCode(statusCode2);
	}
	
	@Override
	public void updateQueryAttach(Query query, String filename) {
		AttachmentCategory attCat = em.find(AttachmentCategory.class, AttachmentCategory.CATEGORY_FINAL_RESPONSE);
		
		Attachment att = new Attachment(query,
				attCat, new DateTime(),
				"testAttachmentTitle1", 'Y', filename,
				"testAuthor1", 'Y', "testAttFup1",
				TestUtil.ID_NOT_EXISTING2_LONG);
		em.persist(att);
		
		Set<Attachment> atts = query.getAttachments();
		if (atts == null) {
			atts = new HashSet<Attachment>();
		}
		atts.add(att);
		
		query.setAttachments(atts);
	}
}
