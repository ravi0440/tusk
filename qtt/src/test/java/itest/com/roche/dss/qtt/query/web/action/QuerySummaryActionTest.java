package itest.com.roche.dss.qtt.query.web.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import itest.com.roche.dss.qtt.action.BaseStrutsTestCase;

import com.roche.dss.qtt.query.web.action.QuerySummaryAction;

public class QuerySummaryActionTest extends BaseStrutsTestCase {

	private QuerySummaryAction action = Mockito.mock(QuerySummaryAction.class);
	
    @Override
	@Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testValidate() throws Exception {
    	action = createAction(QuerySummaryAction.class, null, "UpdateQuerySummary");
    	action.setLabellingDoc("labellingDoc");
    	action.setSummary("summary");
    	action.setDate(new Date());
    	
    	action.validate();
    	Assert.assertFalse(action.hasActionErrors());
    }
	

    @Test
    public void testValidateWrongDate() throws Exception {
    	action = createAction(QuerySummaryAction.class, null, "UpdateQuerySummary");
    	action.setLabellingDoc("labellingDoc");
    	action.setSummary("summary");

    	Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
    	action.setDate(cal.getTime());
    	
    	action.validate();
    	Assert.assertTrue(action.hasActionErrors());
    	boolean hasMessage = false;
    	for (String err : action.getActionErrors()) {
    		if (action.getText("query.summary.expiryDate.later").equals(err)) {
    			hasMessage = true;
    		}
    	}
    	Assert.assertTrue(hasMessage);
    }
	
}
