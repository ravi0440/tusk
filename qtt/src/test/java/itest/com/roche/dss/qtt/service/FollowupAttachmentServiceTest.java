package itest.com.roche.dss.qtt.service;

import com.roche.dss.qtt.service.FollowupAttachmentService;
import javax.sql.DataSource;

/**
 * User: pruchnil
 */
@ContextConfiguration(locations = {"classpath:applicationContext-service-test.xml"})
public class FollowupAttachmentServiceTest extends AbstractTransactionalJUnit4SpringContextTests {
    private static final long queryIdToOpen = 20262l;
    private static final long queryIdToClose = 20263l;

    @Autowired
    FollowupAttachmentService service;

    @Override
    @Autowired
    @Qualifier("qttDataSource")
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    @Test
    public void testOpenFollowupAttachment() {
        int oldCount = simpleJdbcTemplate.queryForInt("select count(*) from followup_attachments");
        service.openFollowupAttachment(queryIdToOpen);
        service.flush();
        int newCount = simpleJdbcTemplate.queryForInt("select count(*) from followup_attachments");
        Assert.assertEquals(oldCount + 1, newCount);
    }

    @Test
    public void testCloseFollowupAttachment() {
        int oldCount = simpleJdbcTemplate.queryForInt("select count(*) from followup_attachments where status_code='FUAT'");
        service.closeFollowupAttachment(queryIdToClose);
        service.flush();
        int newCount = simpleJdbcTemplate.queryForInt("select count(*) from followup_attachments where status_code='FUAT'");
        Assert.assertEquals(oldCount - 1, newCount);
    }


}
