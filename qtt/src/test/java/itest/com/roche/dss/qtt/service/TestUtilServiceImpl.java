package itest.com.roche.dss.qtt.service;

import com.roche.dss.qtt.jbpm.entity.ProcessInstance;
import com.roche.dss.qtt.model.*;
import com.roche.dss.qtt.service.UtilService;
import itest.com.roche.dss.qtt.eao.TestUtilEAO;
import java.util.List;
import java.util.Set;

@Service("testUtilService")
@Transactional
public class TestUtilServiceImpl implements TestUtilService {
	@PersistenceContext
	protected EntityManager em;
	
	@Autowired
	protected TestUtilEAO testUtilEAO;
	
	@Autowired
	protected UtilService utilService;
	
	@Override
	public Query createQueryWithMandatory() {
		return testUtilEAO.createQueryWithMandatory();
	}

	@Override
	public void updateQuery2(Query query) {
		testUtilEAO.updateQuery2(query);
	}
	
	@Override
	public void updateQuery3(Query query) {
		testUtilEAO.updateQuery3(query);
	}
	
	@Override
	public void updateQuery4(Query query) {
		testUtilEAO.updateQuery4(query);
	}
	
	@Override
	public void updateQueryAttach(Query query, String filename) {
		testUtilEAO.updateQueryAttach(query, filename);
	}

	@Override
	public void removeQuery(Query query) {
		query = em.merge(query);
		QueryDescription queryDescription = query.getQueryDescription();
		if (queryDescription != null) {
			queryDescription = em.merge(queryDescription);
			em.remove(queryDescription);
		}
		Set<PreQueryPreparation> pqps = query.getPreQueryPreparations();
		if (pqps != null) {
			for (PreQueryPreparation pqp: pqps) {
				pqp = em.merge(pqp);
				em.remove(pqp);
			}
		}
		Pregnancy pregnancy = query.getPregnancy();
		if (pregnancy != null) {
			pregnancy = em.merge(pregnancy);
			em.remove(pregnancy);
		}
		PerformanceMetric pm = query.getPerformanceMetric();
		if (pm != null) {
			pm = em.merge(pm);
			em.remove(pm);
		}
		
		Set<AffiliateNote> affNotes = query.getAffiliateNotes();
		if (affNotes != null) {
			for (AffiliateNote affNote: affNotes) {
				affNote = em.merge(affNote);
				em.remove(affNote);
			}
		}
		Set<NonAffiliateNote> nonAffNotes = query.getNonAffiliateNotes();
		if (nonAffNotes != null) {
			for (NonAffiliateNote nonAffNote: nonAffNotes) {
				nonAffNote = em.merge(nonAffNote);
				em.remove(nonAffNote);
			}
		}
		
		List<Drug> drugs = query.getDrugs();
		if (drugs != null) {
			for (Drug drug: drugs) {
				drug = em.merge(drug);
				em.remove(drug);
			}
		}
		ClinicalTrial trial = query.getTrial();
		if (trial != null) {
			trial = em.merge(trial);
			em.remove(trial);
		}
		List<Case> cases = query.getCases();
		if (cases != null) {
			for (Case qcase: cases) {
				qcase = em.merge(qcase);
				em.remove(qcase);
			}
		}
		AeTerm term = query.getAeTerm();
		if (term != null) {
			term = em.merge(term);
			em.remove(term);
		}
		RetrievalPeriod period = query.getRetrievalPeriod();
		if (period != null) {
			period = em.merge(period);
			em.remove(period);
		}
		Set<Attachment> atts = query.getAttachments();
		if (atts != null) {
			for (Attachment att: atts) {
				att = em.merge(att);
				em.remove(att);
			}
		}
		Set<AuditHistory> hists = query.getAuditHistorySet();
		if (hists != null) {
			for (AuditHistory hist: hists) {
				hist = em.merge(hist);
				em.remove(hist);
			}
		}
		
		Requester req = query.getRequester();
		
		em.flush();
		em.refresh(query);
		
		ProcessInstance pi = query.getProcessInstance();
		if (pi != null) {
			pi = em.merge(pi);
			Set<TaskInstance> tasks = pi.getTaskInstances();
			if (tasks != null) {
				for (TaskInstance task: tasks) {
					task = em.merge(task);
					for (Object jobO: em.createQuery("from org.jbpm.job.Job job where job.taskInstance.id = " + task.getId()).getResultList()) {
						Job job = (Job) jobO;
						em.remove(job);
					}
					em.remove(task);
				}
			}
		}
		
		em.remove(query);
		
		if (pi != null) {
			Token token = pi.getRootToken();
			if (token != null) {
				token = em.merge(token);
				em.remove(token);
			}
			em.remove(pi);
		}
		
		if (req != null) {
			req = em.merge(req);
			em.remove(req);
		}
	}
	
	@Override
	public void removeAeTerm(AeTerm term) {
		Query query = term.getQuery();
		term = em.merge(term);
		em.remove(term);
		if (query != null) {
			query = em.merge(query);
			em.remove(query);
		}
	}
	
	@Override
	public void tearDown() {
		testUtilEAO.tearDown();
	}
	
	@Override
	public void indexAll() throws InterruptedException {
		FullTextEntityManager emFT = Search.getFullTextEntityManager(em);
		
		MassIndexer indexer = emFT.createIndexer(Query.class);
		
		indexer.startAndWait();
	}

	@Override
	public void reindexAll(Class entityClass) throws InterruptedException {
		utilService.reindexAll(entityClass, UtilService.STAGE_INIT, 0, 0);
		
		utilService.reindexAll(entityClass, UtilService.STAGE_INDEX, 0, 1000000);
		
		utilService.reindexAll(entityClass, UtilService.STAGE_FINAL, 0, 0);
	}
}
