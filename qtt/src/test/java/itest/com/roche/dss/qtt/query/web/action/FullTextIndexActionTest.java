package itest.com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.query.web.action.FullTextIndexAction;
import com.roche.dss.qtt.service.UtilService;

import itest.com.roche.dss.qtt.action.BaseStrutsTestCase;

public class FullTextIndexActionTest extends  BaseStrutsTestCase {
	public void testRebuildIndexStageInit() throws Exception {
        FullTextIndexAction action = createAction(FullTextIndexAction.class, null, "rebuildIndex");
        
        action.setStage(UtilService.STAGE_INIT);
        String result = action.rebuildIndex();
        Assert.assertEquals(ActionSupport.SUCCESS, result);
	}
	
	public void testRebuildIndexStageIndex() throws Exception {
        FullTextIndexAction action = createAction(FullTextIndexAction.class, null, "rebuildIndex");
        
        action.setStage(UtilService.STAGE_INDEX);
        action.setStartIndex(1);
        action.setCount(10);
        String result = action.rebuildIndex();
        Assert.assertEquals(ActionSupport.SUCCESS, result);
	}
	
	public void testRebuildIndexStageFinal() throws Exception {
        FullTextIndexAction action = createAction(FullTextIndexAction.class, null, "rebuildIndex");
        
        action.setStage(UtilService.STAGE_FINAL);
        String result = action.rebuildIndex();
        Assert.assertEquals(ActionSupport.SUCCESS, result);
	}
	
	public void testRebuildIndexStageOptimize() throws Exception {
        FullTextIndexAction action = createAction(FullTextIndexAction.class, null, "rebuildIndex");
        
        action.setStage(UtilService.STAGE_OPTIMIZE);
        String result = action.rebuildIndex();
        Assert.assertEquals(ActionSupport.SUCCESS, result);
	}
	
	public void testRebuildIndexStagePurgeIndex() throws Exception {
        FullTextIndexAction action = createAction(FullTextIndexAction.class, null, "rebuildIndex");
        
        action.setStage(UtilService.STAGE_PURGE_INDEX);
        String result = action.rebuildIndex();
        Assert.assertEquals(ActionSupport.SUCCESS, result);
	}
}
