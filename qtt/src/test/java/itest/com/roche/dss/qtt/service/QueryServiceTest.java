package itest.com.roche.dss.qtt.service;

import com.roche.dss.qtt.model.Attachment;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.comms.dto.AttachmentDTO;
import com.roche.dss.qtt.query.comms.dto.QueryListDTO;
import com.roche.dss.qtt.query.web.action.QueryListAction.Sort;
import com.roche.dss.qtt.service.QrfService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.util.ApplicationUtils;
import com.roche.dss.util.KeyGenerator;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * User: pruchnil
 */
@ContextConfiguration(locations = {"classpath:applicationContext-service-test.xml"})
public class QueryServiceTest extends AbstractTransactionalJUnit4SpringContextTests {
    private static final long closedQueryId = 20262l;
    private static final long closedQueryIdWithFollowupAttachment = 20263l;
    private static final long openQueryId = 20289l;
    private static final long processInstanceId = 222l;

    @Autowired
    private QueryService queryService;
    @Autowired
    private QrfService qrfService;

    @Override
    @Autowired
    @Qualifier("qttDataSource")
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    @Test
    public void testGetQueryList() throws Exception {
        List<QueryListDTO> result = queryService.getQueryList("MIRIAM.ADAMCOVA@ROCHE.COM", Sort.QUERYSEQ.toString(), "asc");
        assertEquals(1, result.size());
        assertEquals(20263, result.get(0).getQuerySeq());
        assertEquals("QTT020263", result.get(0).getQueryNumber());
        assertEquals("Case Data Request", result.get(0).getQueryType());
        assertEquals("19-Feb-2010", result.get(0).getInitiationDateFormatted());
        assertEquals("24-Feb-2010", result.get(0).getResponseDateFormatted());
        assertEquals("ROCEPHIN", result.get(0).getDrugName());
        assertTrue(result.get(0).isUrgency());
    }

    @Test
    public void testGetPreviewQueryDetails() {
        Query q = queryService.getPreviewQueryDetails(openQueryId);
        assertNotNull(q);
        assertEquals("QTT020289", q.getQueryNumber());
        assertTrue(q.isSignalDetection());
    }

    @Test
    public void testGetClosedQueryDetails() {
        Query q = queryService.getClosedQueryDetails(closedQueryIdWithFollowupAttachment, false);
        assertNotNull(q);
        assertEquals("QTT020263", q.getQueryNumber());
        assertTrue(q.getQueryStatusCode().isStatusClosed());
        assertTrue(CollectionUtils.isNotEmpty(q.getFollowupAttachments()));
        assertTrue(q.isOpenFollowupAttachExist());
        assertTrue(CollectionUtils.isNotEmpty(q.getFollowupAttachments()));
    }

    @Test
    public void testUpdateQueryLabel() {
        final String newLabel = "New Label";
        queryService.updateQueryLabelRoute(closedQueryId, newLabel, null);
        queryService.flush();
        Query q = queryService.find(closedQueryId);
        assertEquals(newLabel, q.getQueryLabel());
    }

    @Test
    public void testUpdateQueryLabelRoute() {
        final String newLabel = "New Label";
        final String newRoute = "C";
        queryService.updateQueryLabelRoute(closedQueryId, newLabel, newRoute);
        queryService.flush();
        Query q = queryService.find(closedQueryId);
        assertEquals(newLabel, q.getQueryLabel());
        assertEquals(newRoute, "C");
    }

    @Test
    public void testUpdateQueryExpiryDate() {
        DateTime newDate = new DateTime();
        queryService.updateQueryExpiryDate(closedQueryId, newDate, false);
        queryService.flush();
        Query q = queryService.find(closedQueryId);
        assertEquals(newDate, q.getExpiryDate());
    }

    @Test
    public void testSetAccessKey() throws Exception {
        Query q = queryService.find(closedQueryId);
        String before = q.getAccessKey();
        queryService.setAccessKey(closedQueryId, KeyGenerator.getInstance().getNextID());
        assertNotSame(q.getAccessKey(), before);
    }

    @Test
    public void testGetAttachmentActivationList() throws Exception {
        Query q = queryService.getAttachmentActivationList(closedQueryId, true, null);
        assertNotNull(q.getAttachmentsForDisplay());
        assertTrue(q.getAttachmentsForDisplay().size() > 0);
        for (Attachment attachment : q.getAttachmentsForDisplay()) {
            assertTrue(StringUtils.isEmpty(attachment.getFollowupNumber()));
        }
    }

    @Test
    public void testIsFinalDocExists() {
        assertFalse(queryService.isFinalDocExists(closedQueryId, processInstanceId));
    }

    @Test
    public void testUpdateAlertFlag() {
        queryService.updateAlertFlag(closedQueryId, true);
        queryService.flush();
        assertEquals(new Character('Y'), queryService.find(closedQueryId).getAlertFlag());
    }

    @Test
    public void testUrgencyFlag() {
        Query before = queryService.find(closedQueryId);
        queryService.changeUrgencyFlag(closedQueryId);
        queryService.flush();
        Query after = queryService.find(openQueryId);
        assertEquals(ApplicationUtils.convertToBoolean(after.getUrgencyFlag()), !ApplicationUtils.convertToBoolean(before.getUrgencyFlag()));
    }

    @Test
    public void testUpdateResponseDate() {
        DateTime newDate = new DateTime();
        Character alertBefore = queryService.find(openQueryId).getAlertFlag();
        queryService.updateResponseDate(openQueryId, newDate, "content", false, "SYSTEM");
        queryService.flush();
        Query after = queryService.find(openQueryId);
        assertEquals(newDate, after.getCurrentDueDate());
        assertEquals(ApplicationUtils.convertToBoolean(alertBefore), !ApplicationUtils.convertToBoolean(after.getAlertFlag()));
    }

    @Test
    public void testUpdateDMGResponseDate() {
        DateTime newDate = new DateTime();
        Character alertBefore = queryService.find(openQueryId).getAlertFlag();
        queryService.updateDMGResponseDate(openQueryId, newDate, "content", true,"SYSTEM");
        queryService.flush();
        Query after = queryService.find(openQueryId);
        assertEquals(newDate, after.getDmgDueDate());
        assertEquals(ApplicationUtils.convertToBoolean(alertBefore), ApplicationUtils.convertToBoolean(after.getAlertFlag()));
    }

    @Test
    public void testCreateAttachment() {
        int before = simpleJdbcTemplate.queryForInt("select count(*) from attachments");
        List attachmentList = new ArrayList();
        AttachmentDTO dto1 = new AttachmentDTO();
        dto1.setAuthor("author");
        dto1.setTitle("title");
        dto1.setFileName("filename");
        attachmentList.add(dto1);

        qrfService.createAttachments(attachmentList , 20262);
        queryService.flush();

        int after = simpleJdbcTemplate.queryForInt("select count(*) from attachments");
        assertEquals(before + 1, after);
    }

}
