package itest.com.roche.dss.qtt.service;

import com.roche.dss.qtt.query.web.model.FileUploadBean;
import com.roche.dss.qtt.service.AttachmentService;
import com.roche.dss.qtt.service.QueryNoteService;
import com.roche.dss.util.ApplicationUtils;
import javax.sql.DataSource;
import java.io.File;

/**
 * User: pruchnil
 */
@ContextConfiguration(locations = {"classpath:applicationContext-service-test.xml"})
public class AttachmentServiceTest extends AbstractTransactionalJUnit4SpringContextTests {
    private static final long queryId = 20263l;
    private static final long attachId = 155763l;

    @Autowired
    AttachmentService attachmentService;

    @Autowired
    QueryNoteService noteService;


    @Override
    @Autowired
    @Qualifier("qttDataSource")
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    @Test
    public void testChangeActivationFlag() {
        boolean before = ApplicationUtils.convertToBoolean(attachmentService.find(attachId).getActivationFlag());
        assertTrue(before);
        attachmentService.changeActivationFlag(attachId, queryId, false);
        attachmentService.flush();
        boolean after = ApplicationUtils.convertToBoolean(attachmentService.find(attachId).getActivationFlag());
        assertFalse(after);
    }

    @Test
    public void testAddFileToRepo() throws Exception {
        final String fileName = "name.pdf";
        int notesBefore = simpleJdbcTemplate.queryForInt("select count(*) from NON_AFFILIATE_NOTES");
        int attachsBefore = simpleJdbcTemplate.queryForInt("select count(*) from ATTACHMENTS");

        FileUploadBean upload = new FileUploadBean();
        upload.setType("1");
        upload.setUploadContentType("application/pdf");
        upload.setUploadFileName("pl1.pdf");
        upload.setGivenFileName(fileName);
        upload.setUpload(new File("src/test/resources/testAttachments/pl1.pdf"));
        attachmentService.addFileToRepo(queryId, upload, "SYSTEM", false);
        attachmentService.flush();

        int notesAfter = simpleJdbcTemplate.queryForInt("select count(*) from NON_AFFILIATE_NOTES");
        int attachsAfter = simpleJdbcTemplate.queryForInt("select count(*) from ATTACHMENTS");
        int a = simpleJdbcTemplate.queryForInt("select count(*) from ATTACHMENTS where filename=?", "name.pdf");
        
        assertEquals(notesBefore + 1, notesAfter);
        assertEquals(attachsBefore + 1, attachsAfter);
//      Attachment attachment = attachmentService.getFileFromRepo("" ,queryId);
    }
}
