package itest.com.roche.dss.qtt.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import itest.com.roche.dss.qtt.eao.TestUtil;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.WorkflowRouteType;
import com.roche.dss.qtt.query.comms.dao.impl.RepositorySearchDAOImpl;
import com.roche.dss.qtt.query.web.model.RepositorySearchParams;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.RepositorySearchService;
import com.roche.dss.qtt.service.SearchResults;

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@ContextConfiguration(locations = "classpath:applicationContext-service-test.xml")
@Transactional
public class RepositorySearchServiceTest {
	@Autowired
	protected RepositorySearchService service;
	
	@Autowired
	protected QueryService queryService;
	
	@Autowired
	protected TestUtilService testUtilService;
	
	protected Query query;
	
	protected static final Logger log = LoggerFactory.getLogger(RepositorySearchServiceTest.class);
	
	@Before
	public void setUp () {
		query = testUtilService.createQueryWithMandatory();
		queryService.persist(query);
	}
	
	@After
	public void tearDown() {
		testUtilService.removeQuery(query);
	}
	
	@Test
	public void testSearchByDrugRetrievalName() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setDrugRetrievalName(new String[]{"testDrugRetrievalName1"});
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setDrugRetrievalName(new String[]{"testDrugRetrievalName"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	public void testSearchByInnGenericName() {	
		RepositorySearchParams p = new RepositorySearchParams();
		p.setInnGenericName(new String[]{"testInnGenericName1"});
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setInnGenericName(new String[]{"testInnGenericName"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	public void testSearchByIndication() {	
		RepositorySearchParams p = new RepositorySearchParams();
		p.setIndication(new String[]{"testIndication1"});
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setIndication(new String[]{"testIndication"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));		
	}
	
	@Test
	public void testSearchByDrugRetrievalNameAndByInnGenericName() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setDrugRetrievalName(new String[]{"testDrugRetrievalName1"});
		p.setInnGenericName(new String[]{"testInnGenericName1"});
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setDrugRetrievalName(new String[]{"testDrugRetrievalName"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		p.setDrugRetrievalName(null);
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
	}
	
	@Test
	public void testSearchByQueryType() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setQueryType("Random nonexistent type");
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQueryType("Random nonexistent type");
		results = service.search(p);
		Assert.assertFalse(results.contains(query));		
	}
	
	@Test
	public void testSearchByDrugRetrievalNameAndByQueryType() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setDrugRetrievalName(new String[]{"testDrugRetrievalName1"});
		p.setQueryType("Random nonexistent type");
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQueryType("Random nonexistent type");
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		p.setQueryType(null);
		results = service.search(p);
		Assert.assertTrue(results.contains(query));		
	}
	
	
	@Test
	public void testSearchByAeTermDescription() {	
		RepositorySearchParams p = new RepositorySearchParams();
		p.setAeTermDescription("testAeTermDescription1");
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setAeTermDescription("testAeTermDescriptionX");
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		p.setAeTermDescription("testAeTermDescriptioN");
		results = service.search(p);
		Assert.assertTrue(results.contains(query));	
	}
	
	@Test
	public void testSearchByDrugRetrievalNameAndByAeTermDescription() {	
		RepositorySearchParams p = new RepositorySearchParams();
		p.setDrugRetrievalName(new String[]{"testDrugRetrievalName1"});
		p.setAeTermDescription("testAeTermDescription1");
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setAeTermDescription("testAeTermDescriptionX");
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		p.setAeTermDescription(null);
		results = service.search(p);
		Assert.assertTrue(results.contains(query));	
	}
	
	@Test
	public void testSearchByQueryDescriptionPrevMedicalHistory() {	
		RepositorySearchParams p = new RepositorySearchParams();
		p.setQueryDescriptionPrevMedicalHistory("testPrevMedicalHistory1");
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQueryDescriptionPrevMedicalHistory("testPrevMedicalHistoryX");
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		p.setQueryDescriptionPrevMedicalHistory("testPrevMedicalHistorY");
		results = service.search(p);
		Assert.assertTrue(results.contains(query));	
	}
	
	@Test
	public void testSearchByDrugRetrievalNameAndByQueryDescriptionPrevMedicalHistory() {	
		RepositorySearchParams p = new RepositorySearchParams();
		p.setDrugRetrievalName(new String[]{"testDrugRetrievalName1"});
		p.setQueryDescriptionPrevMedicalHistory("testPrevMedicalHistory1");
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQueryDescriptionPrevMedicalHistory("testPrevMedicalHistoryX");
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		p.setQueryDescriptionPrevMedicalHistory(null);
		results = service.search(p);
		Assert.assertTrue(results.contains(query));	
	}
	
	@Test
	public void testSearchByInteractingDrugName() {	
		RepositorySearchParams p = new RepositorySearchParams();
		p.setInteractingDrugName("testInnGenericName2");
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setInteractingDrugName("testInnGenericNameX");
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		p.setInteractingDrugName("testInnGenericNamE");
		results = service.search(p);
		Assert.assertTrue(results.contains(query));	
	}
	
	@Test
	public void testSearchByDrugRetrievalNameAndByInteractingDrugName() {	
		RepositorySearchParams p = new RepositorySearchParams();
		p.setDrugRetrievalName(new String[]{"testDrugRetrievalName1"});
		p.setInteractingDrugName("testInnGenericName2");
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setInteractingDrugName("testInnGenericNameX");
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		p.setInteractingDrugName(null);
		results = service.search(p);
		Assert.assertTrue(results.contains(query));	
	}
	
	@Test
	public void testSearchByDrugRetrievalNameAndByByInnGenericNameAndByInteractingDrugName() {	
		RepositorySearchParams p = new RepositorySearchParams();
		p.setDrugRetrievalName(new String[]{"testDrugRetrievalName1"});
		p.setInnGenericName(new String[]{"testInnGenericName1"});
		p.setInteractingDrugName("testInnGenericName2");
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setInteractingDrugName("testInnGenericNameX");
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		p.setInteractingDrugName(null);
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setInnGenericName(new String[]{"testInnGenericName"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByAuthor() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setAuthor(new String[]{"testAuthor2","testAuthorNotExisting2"});
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setAuthor(new String[]{"testAuthor2"});
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setAuthor(new String[]{"testAuthorNotExisting2"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByPeriodDateFrom() throws Exception {
		RepositorySearchParams p = new RepositorySearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setPeriodDateFrom(sdf.parse("01/12/2001"));
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		//RetrievalPeriod.endTs = 04-Jan-2002
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setPeriodDateFrom(sdf.parse("04/01/2002"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setPeriodDateFrom(sdf.parse("05/01/2002"));
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByPeriodDateTo() throws Exception {
		RepositorySearchParams p = new RepositorySearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setPeriodDateTo(sdf.parse("02/01/2002"));
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		//RetrievalPeriod.startTs = 01-Jan-2002
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setPeriodDateTo(sdf.parse("01/01/2002"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setPeriodDateTo(sdf.parse("31/12/2001"));
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByPeriodDateFromAndTo() throws Exception {
		RepositorySearchParams p = new RepositorySearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setPeriodDateFrom(sdf.parse("01/12/2001"));
		
		p.setPeriodDateTo(sdf.parse("02/01/2002"));
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		//RetrievalPeriod.endTs = 04-Jan-2002
		//RetrievalPeriod.startTs = 01-Jan-2002
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setPeriodDateFrom(sdf.parse("04/01/2002"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setPeriodDateFrom(sdf.parse("05/01/2002"));
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByLabelingDocTypeIds() {	
		RepositorySearchParams p = new RepositorySearchParams();
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		p.setLabelingDocTypeIds(new Long[]{query.getLabelingDocTypes().iterator().next().getLdocTypeId()});
		SearchResults results = service.search(p);
		Assert.assertTrue(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByQuerySummary() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setQuerySummary("testQuerySummary1");
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		query.setQuerySummary("testQuerySummary1");
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQuerySummary("testQuerySummarY");
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQuerySummary("testQuerySummaryX");
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByWorkflowRouteTypeIds() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setWorkflowRouteTypeIds(new String[]{WorkflowRouteType.TYPE_A1});
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		query.setWorkflowRouteType(WorkflowRouteType.TYPE_A1);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByDmgMembers() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setDmgMembers(new String[]{"testResponsible7"});
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setDmgMembers(new String[]{"testResponsible"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	// FIXME this test fails on PostgreSQL now, we must only be sure that it succeeds on Oracle
	//@Test
	public void testSearchByDmgMembersWithLimit() {
		int prevMaxResults = RepositorySearchDAOImpl.MAX_RESULTS;
		RepositorySearchDAOImpl.MAX_RESULTS = 10;
		
		RepositorySearchParams p = new RepositorySearchParams();
		p.setDmgMembers(new String[]{"STIRNIH1"});
		
		SearchResults results = service.search(p);
		RepositorySearchDAOImpl.MAX_RESULTS = prevMaxResults;
		Assert.assertEquals(10, results.getResultSize());
		Assert.assertEquals(10, results.getActualResultSize());
	}

	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByPsMembers() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setPsMembers(new String[]{"testResponsible3"});
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setPsMembers(new String[]{"testResponsible"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}

	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByDsclMembers() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setDsclMembers(new String[]{"testResponsible11"});
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setDsclMembers(new String[]{"testResponsible"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}

	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByDmgMembersAndByPsMembers() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setDmgMembers(new String[]{"testResponsible7"});
		p.setPsMembers(new String[]{"testResponsible3"});
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setDmgMembers(new String[]{"testResponsible"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByDmgMembersAndByDsclMembers() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setDmgMembers(new String[]{"testResponsible7"});
		p.setDsclMembers(new String[]{"testResponsible11"});
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setDmgMembers(new String[]{"testResponsible"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByDsclMembersAndByPsMembers() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setDsclMembers(new String[]{"testResponsible11"});
		p.setPsMembers(new String[]{"testResponsible3"});
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setPsMembers(new String[]{"testResponsible"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByAllMemberTypes() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setDmgMembers(new String[]{"testResponsible7"});
		p.setPsMembers(new String[]{"testResponsible3"});
		p.setDsclMembers(new String[]{"testResponsible11"});
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setDmgMembers(new String[]{"testResponsible"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}

	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByNonExpiredResponsesOnly () {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setNonExpiredResponsesOnly("true");
		
		SearchResults results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		DateTime now = new DateTime().dayOfMonth().roundFloorCopy();
		
		query.setExpiryDate(now);
		query = queryService.merge(query);
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		now = now.plusMillis(1000);
		query.setExpiryDate(now);
		query = queryService.merge(query);
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		now = now.minusMillis(2000);
		query.setExpiryDate(now);
		query = queryService.merge(query);
		results = service.search(p);
		Assert.assertFalse(results.contains(query));		
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByRequesterSurname() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setRequesterSurname("testReqSurname1");
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setRequesterSurname("testReqSurname");
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByRequesterCountryCode() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setRequesterCountryCode(new String[]{"TCOU1"});
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setRequesterCountryCode(new String[]{"TCOU"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByReporterTypeId() {
		RepositorySearchParams p = new RepositorySearchParams();
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		p.setReporterTypeId(new Short[]{query.getReporterType().getReporterTypeId()});
		SearchResults results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setReporterTypeId(new Short[]{TestUtil.ID_NOT_EXISTING_SHORT});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchBySourceCountryCode() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setSourceCountryCode(new String[]{"TCOU1"});
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		query.setSourceCountryCode("TCOU1");
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setSourceCountryCode(new String[]{"TCOU"});
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByDatetimeSubmittedFrom() throws Exception {
		RepositorySearchParams p = new RepositorySearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setQryResponseDateFrom(sdf.parse("01/12/2000"));
		SearchResults results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		DateTime datetimeSubmitted = new DateTime(2001, 1, 6, 0, 0, 0, 0);
		query.setDatetimeSubmitted(datetimeSubmitted);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryResponseDateFrom(sdf.parse("06/01/2001"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryResponseDateFrom(sdf.parse("07/01/2001"));
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...	
	public void testSearchByDatetimeSubmittedTo() throws Exception {
		RepositorySearchParams p = new RepositorySearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setQryResponseDateTo(sdf.parse("01/02/2001"));
		SearchResults results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		DateTime datetimeSubmitted = new DateTime(2001, 1, 6, 0, 0, 0, 0);
		query.setDatetimeSubmitted(datetimeSubmitted);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryResponseDateTo(sdf.parse("06/01/2001"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryResponseDateTo(sdf.parse("05/01/2001"));
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByDatetimeSubmittedFromAndTo() throws Exception {
		RepositorySearchParams p = new RepositorySearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setQryResponseDateFrom(sdf.parse("01/12/2000"));
		
		p.setQryResponseDateTo(sdf.parse("01/02/2001"));
		SearchResults results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		DateTime datetimeSubmitted = new DateTime(2001, 1, 6, 0, 0, 0, 0);
		query.setDatetimeSubmitted(datetimeSubmitted);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryResponseDateFrom(sdf.parse("06/01/2001"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryResponseDateTo(sdf.parse("06/01/2001"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));		
		
		p.setQryResponseDateFrom(sdf.parse("07/01/2001"));
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}   

	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByQryInitiateDateFrom() throws Exception {
		RepositorySearchParams p = new RepositorySearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setQryInitiateDateFrom(sdf.parse("01/12/2002"));
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		// AuditHistory.startTs = 1-Jan-2003
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryInitiateDateFrom(sdf.parse("01/01/2003"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryInitiateDateFrom(sdf.parse("02/01/2003"));
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}

	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...	
	public void testSearchByQryInitiateTo() throws Exception {
		RepositorySearchParams p = new RepositorySearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setQryInitiateDateTo(sdf.parse("01/02/2003"));
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		// AuditHistory.startTs = 1-Jan-2003
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryInitiateDateTo(sdf.parse("01/01/2003"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryInitiateDateTo(sdf.parse("31/12/2002"));
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByQryInitiateDateFromAndTo() throws Exception {
		RepositorySearchParams p = new RepositorySearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setQryInitiateDateFrom(sdf.parse("01/12/2002"));
		
		p.setQryInitiateDateTo(sdf.parse("01/02/2003"));
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		// AuditHistory.startTs = 1-Jan-2003
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryInitiateDateFrom(sdf.parse("01/01/2003"));
		
		p.setQryInitiateDateTo(sdf.parse("01/01/2003"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryInitiateDateFrom(sdf.parse("02/01/2003"));
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByQryInitiateDateFromAndByDmgMembers() throws Exception {
		RepositorySearchParams p = new RepositorySearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setQryInitiateDateFrom(sdf.parse("01/12/2002"));
		p.setDmgMembers(new String[]{"testResponsible7"});
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		// AuditHistory.startTs = 1-Jan-2003
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryInitiateDateFrom(sdf.parse("01/01/2003"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryInitiateDateFrom(sdf.parse("02/01/2003"));
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByQryFinalDeliveryDateFrom() throws Exception {
		RepositorySearchParams p = new RepositorySearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setQryFinalDeliveryDateFrom(sdf.parse("01/12/2002"));
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		// AuditHistory.startTs = 2-Jan-2003
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryFinalDeliveryDateFrom(sdf.parse("02/01/2003"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryFinalDeliveryDateFrom(sdf.parse("03/01/2003"));
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}

	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...	
	public void testSearchByQryFinalDeliveryTo() throws Exception {
		RepositorySearchParams p = new RepositorySearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setQryFinalDeliveryDateTo(sdf.parse("01/02/2003"));
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		// AuditHistory.startTs = 2-Jan-2003
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryFinalDeliveryDateTo(sdf.parse("02/01/2003"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryFinalDeliveryDateTo(sdf.parse("01/01/2003"));
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByQryFinalDeliveryDateFromAndTo() throws Exception {
		RepositorySearchParams p = new RepositorySearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setQryFinalDeliveryDateFrom(sdf.parse("01/12/2002"));
		
		p.setQryFinalDeliveryDateTo(sdf.parse("01/02/2003"));
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		// AuditHistory.startTs = 2-Jan-2003
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryFinalDeliveryDateFrom(sdf.parse("02/01/2003"));
		
		p.setQryFinalDeliveryDateTo(sdf.parse("02/01/2003"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryFinalDeliveryDateFrom(sdf.parse("03/01/2003"));
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByQryFinalDeliveryDateFromAndByDmgMembers() throws Exception {
		RepositorySearchParams p = new RepositorySearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setQryFinalDeliveryDateFrom(sdf.parse("01/12/2002"));
		p.setDmgMembers(new String[]{"testResponsible7"});
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		// AuditHistory.startTs = 2-Jan-2003
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryFinalDeliveryDateFrom(sdf.parse("02/01/2003"));
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQryFinalDeliveryDateFrom(sdf.parse("03/01/2003"));
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByRequesterQueryLabel() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setRequesterQueryLabel("testRequesterQueryLabel1");
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		query.setRequesterQueryLabel("testRequesterQueryLabel1");
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setRequesterQueryLabel("testRequesterQueryLabeL");
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setRequesterQueryLabel("testRequesterQueryLabelX");
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	// TODO add a test case: testSearchByDrugRetrievalNameAndBy...
	public void testSearchByQueryNumber() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setQueryNumber("testQueryNumber1");
		
		SearchResults results = service.search(p);
		Assert.assertFalse(results.contains(query));
		
		query.setQueryNumber("testQueryNumber1");
		query = queryService.merge(query);
		
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQueryNumber("testQueryNumbeR");
		results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		p.setQueryNumber("QueryNumberX");
		results = service.search(p);
		Assert.assertFalse(results.contains(query));
	}
	
	@Test
	public void testSearchNonClosed() {
		RepositorySearchParams p = new RepositorySearchParams();
		p.setQueryNumber("testQueryNumber1");
				
		query.setQueryNumber("testQueryNumber1");
		query = queryService.merge(query);
		
		SearchResults results = service.search(p);
		Assert.assertTrue(results.contains(query));
		
		testUtilService.updateQuery4(query);
		
		results = service.search(p);
		Assert.assertFalse(results.contains(query));		
	}
}
