package itest.com.roche.dss.qtt.action;

import com.roche.dss.qtt.query.web.action.QueryListAction;
import com.roche.dss.qtt.security.service.SecurityService;
import com.roche.dss.qtt.security.user.RocheUserDetails;

/**
 * User: pruchnil
 */
public class QueryListActionTest extends BaseStrutsTestCase {
    private static final String email = "pawel.savov@contractors.roche.com";
    private QueryListAction action;
    private SecurityService securityService = Mockito.mock(SecurityService.class);

    @Test
    public void testExecute() throws Exception {
        action = createAction(QueryListAction.class, null, "QueryListMenu");
        assertEquals(Action.SUCCESS, proxy.execute());
    }

    @Test
    public void testQueryList() throws Exception {
        action = createAction(QueryListAction.class, null, "QueryList");
        assertEquals(Action.INPUT, proxy.execute());
    }

    @Test
    public void testOwnQueryList() throws Exception {
        action = createAction(QueryListAction.class, null, "OwnQueryList");
        mockSecurityInAction();

        assertEquals(Action.SUCCESS, proxy.execute());
        assertNotNull(action.getQueryList());
        assertEquals(1, action.getQueryList().size());
    }

   @Test
    public void testSortQueryList() throws Exception {
        action = createAction(QueryListAction.class, null, "SortQueryList");

        assertEquals(0, action.getSort());
        action.setEmail(email);
        action.setSort(1);
        action.setLast(1);

        assertEquals(Action.SUCCESS, proxy.execute());
        assertNotNull(action.getQueryList());
        assertEquals(1, action.getQueryList().size());
    }

    private void mockSecurityInAction(){
        RocheUserDetails user = Mockito.mock(RocheUserDetails.class);
        Mockito.when(user.getMail()).thenReturn(email);
        Mockito.when(securityService.getLoggedUser()).thenReturn(user);
        action.setSecurityService(securityService);
    }
}
