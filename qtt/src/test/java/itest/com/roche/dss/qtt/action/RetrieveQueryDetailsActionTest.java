package itest.com.roche.dss.qtt.action;

import com.roche.dss.qtt.query.web.action.RetrieveQueryDetailsAction;

/**
 * User: pruchnil
 */
public class RetrieveQueryDetailsActionTest extends BaseStrutsTestCase {
    private RetrieveQueryDetailsAction action;
    private static final long queryId = 20262l;

    public void testExecuteRetrieveClosedQuery() throws Exception {
        action = createAction(RetrieveQueryDetailsAction.class, null, "ClosedQueryDetails");
        action.setQueryId(queryId);
        assertEquals(SUCCESS, proxy.execute());
    }
}
