package itest.com.roche.dss.qtt.query.web.action;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.web.action.AcceptQueryAction;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.action.MedicalInterpretationAction;
import com.roche.dss.qtt.query.web.action.QrfAction;
import com.roche.dss.qtt.query.web.action.QueryReviewAction;
import com.roche.dss.qtt.query.web.action.WorkflowProcessAction;
import com.roche.dss.qtt.query.web.action.oldqrf.*;
import com.roche.dss.qtt.query.web.qrf.model.ProcessQRFActionModel;
import com.roche.dss.qtt.query.web.qrf.model.QrfFormModel;
import com.roche.dss.qtt.query.web.qrf.model.QueryDescFormModel;
import com.roche.dss.qtt.query.web.utils.UserDataInterceptor;
import com.roche.dss.qtt.security.model.DSSUser;
import com.roche.dss.qtt.security.service.SecurityService;
import com.roche.dss.qtt.security.user.RocheUserDetails;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.util.ApplicationContextProvider;
import com.roche.dss.util.OpenEntityManagerInTests;
import itest.com.roche.dss.qtt.action.BaseStrutsTestCase;
import itest.com.roche.dss.qtt.service.TestUtilService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.roche.dss.qtt.QttGlobalConstants.DSCL_PERMISSION;
import static com.roche.dss.qtt.QttGlobalConstants.PS_PERMISSION;

public class QrfActionTest extends BaseStrutsTestCase {
	
	protected static final Logger log = LoggerFactory.getLogger(QrfActionTest.class);
	
	protected Integer queryIdI;
		
	public void tearDown() {
		QueryService queryService = ApplicationContextProvider.ctx.getBean(QueryService.class);
		TestUtilService testUtilService = ApplicationContextProvider.ctx.getBean(TestUtilService.class);
		
		Query query = queryService.find(queryIdI.longValue());
		testUtilService.removeQuery(query);
		testUtilService.tearDown();
	}
	
	public void testExecute() throws Exception {
		// opening the first page of QRF
		QrfAction action = createAction(QrfAction.class, null, "QrfAction");
		Map<String, Object> session = new HashMap<String, Object>();
        action.setSession(session);
        
        RocheUserDetails user = Mockito.mock(RocheUserDetails.class);
        Mockito.when(user.getMail()).thenReturn(UserDataInterceptor.testMail);
		
        action.setUserDetails(user);
        
        String result = action.execute();
        Assert.assertEquals(ActionSupport.SUCCESS, result);
        
        QrfFormModel model = (QrfFormModel) action.getModel();
        Assert.assertEquals(UserDataInterceptor.testMail, model.getEmail());
        
        // submitting the first page of QRF
        StdQrfAction action2 = createAction(StdQrfAction.class, null, "StdQrfAction");
        action2.setSession(session);
        
        QrfFormModel model2 = (QrfFormModel) action2.getModel();

        model2.setRequesterQueryLabel("THIS IS A TEST QUERY USED BY BAMBOO BUILD PLAN!");
        model2.setFirstname("testFirstName1");
        model2.setLastname("testLastName1");
        model2.setTelephone("testTele1");
        model2.setEmail(UserDataInterceptor.testMail);
        String reqCountryCode = action2.getOrderedCountries().get(0).getCountryCode();
        model2.setReqCountry(reqCountryCode);
        model2.setOrgName("testOrgName1");
        String orgTypeS = action2.getOrganisationTypes().get(0).getTypeId();
        model2.setOrgType(Integer.parseInt(orgTypeS));
        
        String srcCountryCode = action2.getOrderedCountries().get(1).getCountryCode();
        model2.setSourceCountry(srcCountryCode);
        String repTypeS = action2.getReporterTypes().get(0).getReporterTypeId();
        model2.setReporterType(Integer.parseInt(repTypeS));
        
        model2.setAllDrugs(0);
        String retrDrug = action2.getDrugRetrievalNames().get(0);
        model2.setRetrDrug(retrDrug);
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
        model2.setDateRequested(sdf.parse("01/06/2011"));

        model2.setCumulative(1);        
                
        model2.setQueryType(action2.getDictionary().getQueryTypes().get(0).getQueryTypeCode());
        
        result = action2.execute();
        Assert.assertEquals("drugEvent", result);
        
        queryIdI = (Integer) session.get(ActionConstants.QRF_QUERY_ID);
        Assert.assertNotNull(queryIdI);
        
        
        // opening the fourth page of QRF
        ProcessQRFAction action7 = createAction(ProcessQRFAction.class, null, "ProcessQRFAction");
        
        ProcessQRFActionModel model7 = (ProcessQRFActionModel) action7.getModel();
        model7.setActivity("sub");
        model7.setId(queryIdI.toString());
        
        result = action7.execute();
        Assert.assertEquals("submitSuccess", result);
		
		OpenEntityManagerInTests openEMT = ApplicationContextProvider.ctx.getBean(OpenEntityManagerInTests.class);
        openEMT.loadEntityManager();
		
        // Initiate Query: "Open query"
        QueryReviewAction action8 = createAction(QueryReviewAction.class, null, "QueryReview");
        action8.setSession(session);
        
        result = action8.execute();
        Assert.assertEquals(ActionSupport.SUCCESS, result);
        
        // Initiate Query: "Accept Query"
        AcceptQueryAction action9 = createAction(AcceptQueryAction.class, null, "AcceptQuery");
        action9.setId(queryIdI.longValue());
        
        SecurityService securityServiceMock = Mockito.mock(SecurityService.class);
		Mockito.when(securityServiceMock.isAllowedCheck(PS_PERMISSION)).thenReturn(true);
		Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
		Mockito.when(securityServiceMock.getLoggedUserName()).thenReturn(UserDataInterceptor.testUserId);
		action9.setSecurityService(securityServiceMock);
        
        result = action9.execute();
        
        Assert.assertEquals(ActionSupport.SUCCESS, result);
        
        // Select Workflow Route: opening the parameter page
        WorkflowProcessAction action10 = createAction(WorkflowProcessAction.class, null, "SelectWorkflowProcess");
        action10.setSecurityService(securityServiceMock);
        
        QueryService queryService = ApplicationContextProvider.ctx.getBean(QueryService.class);
        Query query = queryService.find(queryIdI.longValue());
        long taskId = 0;
        for (TaskInstance task: query.getProcessInstance().getTaskInstances()) {
        	taskId = task.getId();
        	if ("Unassigned".equals(task.getName())) {
        		break;
        	}
        }
        
        action10.setQueryId(queryIdI.longValue());
        action10.setTaskId(taskId);
        action10.setTaskOwner(UserDataInterceptor.testUserId);
        
        result = action10.select();
        Assert.assertEquals(ActionSupport.SUCCESS, result);
        
        // Select Workflow Route: submitting the page
        WorkflowProcessAction action11 = createAction(WorkflowProcessAction.class, null, "UpdateProcessRoute");
        action11.setSecurityService(securityServiceMock);
        
        action11.setQueryId(queryIdI.longValue());
        action11.setSelectedRoute("B");
        
        result = action11.update();
        Assert.assertEquals(ActionSupport.SUCCESS, result);
        
        // Medical Interpretation: opening the parameter page
		MedicalInterpretationAction action12 = createAction(MedicalInterpretationAction.class, null, "MedicalInterpretation");
		action12.setSecurityService(securityServiceMock);
		
		action12.setTaskName("Assing PS");
		action12.setQueryId(queryIdI.longValue());
		action12.setTaskId(taskId);
		
        result = action12.execute();
        Assert.assertEquals(ActionSupport.SUCCESS, result); 
        // NOTE we do it in order to refresh data changes done by JBPM from the database
        openEMT.unloadEntityManager();        
        openEMT.loadEntityManager();
        
        // Medical Interpretation: submitting the page
		MedicalInterpretationAction action13 = createAction(MedicalInterpretationAction.class, null, "MIResourceAssignment");
		action13.setSecurityService(securityServiceMock);
		
		query = queryService.find(queryIdI.longValue());
		
		long taskAssignPS = 0;
        for (TaskInstance task: query.getProcessInstance().getTaskInstances()) {
        	if ("Assign PS".equals(task.getName())) {
        		taskAssignPS = task.getId();
        	}
        }
		
		action13.setQuery(query);
		action13.setTaskId(taskAssignPS);
		action13.setUserForMI(UserDataInterceptor.testUserId);
		
    	DSSUser userD = new DSSUser();
    	userD.setEmployeeId(UserDataInterceptor.testEmployeeId);
       	userD.setFirstName(UserDataInterceptor.testFirstName);
    	userD.setLastName(UserDataInterceptor.testSurname);
    	userD.setPhoneNumber(UserDataInterceptor.testPhone);
    	userD.setFax(UserDataInterceptor.testFax);
    	userD.setEmail(UserDataInterceptor.testMail);
    	userD.setCountryCode(UserDataInterceptor.testCountryCode);
    	userD.setSite(UserDataInterceptor.testSite);
    	userD.setUserName(UserDataInterceptor.testUserId);
    	List<DSSUser> resourceList = new ArrayList<DSSUser>();
    	resourceList.add(userD);
    	
		action13.setResourceList(resourceList);
        
        result = action13.assign();        
        Assert.assertEquals(ActionSupport.SUCCESS, result);  
        //jbpm sends e-mail notification!!!
		
        openEMT.unloadEntityManager();
	}
	
	/*public void testLeaveData() throws Exception {
		for (int i = 0; i < 2; ++ i) {
			testExecute();
		}
	}*/
}
