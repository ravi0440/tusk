package itest.com.roche.dss.qtt.action;

import com.roche.dss.qtt.query.web.action.CommonActionSupport;
import com.roche.dss.qtt.query.web.action.WorkflowProcessAction;
import com.roche.dss.qtt.security.service.SecurityService;
import com.roche.dss.qtt.service.workflow.WorkflowService;
import com.roche.dss.qtt.service.workflow.exception.TaskAlreadyBegunException;
import com.roche.dss.qtt.service.workflow.exception.TaskAlreadyCompletedException;
import static com.roche.dss.qtt.QttGlobalConstants.DSCL_PERMISSION;

/**
 * User: pruchnil
 */

public class WorkflowProcessActionTest extends BaseStrutsTestCase {
    private static final Logger logger = LoggerFactory.getLogger(WorkflowProcessActionTest.class);
    private WorkflowProcessAction action;
    private SecurityService securityServiceMock = Mockito.mock(SecurityService.class);
    private WorkflowService workflowServiceMock = Mockito.mock(WorkflowService.class);
    private static final long queryId = 20262l;
    private static final long processId = 1l;
    private static final String taskOwner = "TaskOwner";

    public void testExecute() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(WorkflowProcessAction.class, null, "RestartWorkflowProcess");
        action.setSecurityService(securityServiceMock);
        action.setQueryId(queryId);
        assertEquals(CommonActionSupport.SUCCESS, proxy.execute());
    }

    public void testExecuteDenied() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(false);
        action = createAction(WorkflowProcessAction.class, null, "RestartWorkflowProcess");
        action.setSecurityService(securityServiceMock);
        assertEquals(CommonActionSupport.DENIED, proxy.execute());
    }

    public void testRestart() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(WorkflowProcessAction.class, null, "ConfirmProcessRestart");
        action.setSecurityService(securityServiceMock);
        action.setWorkflowService(workflowServiceMock);
        action.setProcessId(processId);
        action.setTaskOwner(taskOwner);
        assertEquals(CommonActionSupport.SUCCESS, proxy.execute());
        logger.debug("proxy.getActionName()" + proxy.getActionName());
        assertEquals(taskOwner, action.getTaskOwner());

    }

    public void testRestartDenied() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(false);
        action = createAction(WorkflowProcessAction.class, null, "ConfirmProcessRestart");
        action.setSecurityService(securityServiceMock);
        action.setWorkflowService(workflowServiceMock);
        assertEquals(CommonActionSupport.DENIED, proxy.execute());
    }

    public void testSelectAlreadyBegun() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(WorkflowProcessAction.class, null, "SelectWorkflowProcess");
        action.setSecurityService(securityServiceMock);
        Mockito.doThrow(new TaskAlreadyBegunException()).when(workflowServiceMock).lockTask(Mockito.anyString(), Mockito.anyLong());
        action.setWorkflowService(workflowServiceMock);
        assertEquals(CommonActionSupport.TASK_ALREADY_BEGUN, proxy.execute());
    }

    public void testUpdateAlreadyCompleted() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(WorkflowProcessAction.class, null, "UpdateProcessRoute");
        action.setSecurityService(securityServiceMock);
        Mockito.doThrow(new TaskAlreadyCompletedException()).when(workflowServiceMock).selectWorkflowRoute(Mockito.anyLong(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
        action.setWorkflowService(workflowServiceMock);
        action.setQueryLabel("Label");
        action.setSelectedRoute("A");
        assertEquals(CommonActionSupport.TASK_ALREADY_COMPLETED, proxy.execute());
    }

}
