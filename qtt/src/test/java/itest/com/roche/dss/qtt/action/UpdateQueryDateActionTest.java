package itest.com.roche.dss.qtt.action;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.roche.dss.qtt.query.web.action.UpdateQueryDateAction;
import com.roche.dss.qtt.security.service.SecurityService;
import static com.roche.dss.qtt.QttGlobalConstants.DSCL_PERMISSION;
import static com.roche.dss.qtt.query.web.action.CommonActionSupport.*;

/**
 * User: pruchnil
 */
public class UpdateQueryDateActionTest extends BaseStrutsTestCase {
    private UpdateQueryDateAction action;
    private SecurityService securityServiceMock = Mockito.mock(SecurityService.class);
    private static final long closedQueryId = 20262l;
    private static final String dateMsg = "New date";

    public void testExpiryDateExecute() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(UpdateQueryDateAction.class, null, "EnterExpiryDate");
        action.setSecurityService(securityServiceMock);
        action.setExpiry(true);
        action.setQueryId(closedQueryId);
        assertEquals(SUCCESS, proxy.execute());
    }

    public void testExpiryDateExecuteDenied() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(false);
        action = createAction(UpdateQueryDateAction.class, null, "EnterExpiryDate");
        action.setExpiry(true);
        action.setSecurityService(securityServiceMock);
        assertEquals(DENIED, proxy.execute());
    }

    public void testExpiryDateUpdate() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(UpdateQueryDateAction.class, null, "UpdateExpiryDate");
        action.setExpiry(true);
        action.setSecurityService(securityServiceMock);
        action.setAffiliateAccess(true);
        action.setQueryId(closedQueryId);
        action.setDate(getExampleDate());
        assertEquals(UPDATE, proxy.execute());
    }

    public void testExpiryDateUpdateValidationFailed() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(UpdateQueryDateAction.class, null, "UpdateExpiryDate");
        action.setExpiry(true);
        action.setSecurityService(securityServiceMock);
        assertEquals(INPUT, proxy.execute());
        assertEquals(1, action.getActionErrors().size());
        assertEquals(action.getText("field.required", new String[]{dateMsg}), action.getActionErrors().iterator().next());
    }

    public void testResponseDateExecute() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(UpdateQueryDateAction.class, null, "EnterResponseDate");
        action.setSecurityService(securityServiceMock);
        action.setExpiry(false);
        action.setQueryId(closedQueryId);
        assertEquals(SUCCESS, proxy.execute());
    }

    public void testResponseDateExecuteDenied() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(false);
        action = createAction(UpdateQueryDateAction.class, null, "EnterResponseDate");
        action.setExpiry(false);
        action.setSecurityService(securityServiceMock);
        assertEquals(DENIED, proxy.execute());
    }

    public void testResponseDateUpdateValidationFailed() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(UpdateQueryDateAction.class, null, "UpdateResponseDate");
        action.setExpiry(false);
        action.setSecurityService(securityServiceMock);
        assertEquals(INPUT, proxy.execute());
        assertEquals(2, action.getActionErrors().size());
        assertEquals(action.getText("field.required", new String[]{dateMsg}), action.getActionErrors().iterator().next());
    }

    public void testResponseDateUpdate() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(UpdateQueryDateAction.class, null, "UpdateResponseDate");
        action.setExpiry(false);
        action.setQueryId(closedQueryId);
        action.setDate(getExampleDate());
        action.setExplanation("explanation");
        action.setSecurityService(securityServiceMock);
        assertEquals(UPDATE, proxy.execute());
    }

    public void testDMGResponseDateExecute() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(UpdateQueryDateAction.class, null, "EnterDMGResponseDate");
        action.setSecurityService(securityServiceMock);
        action.setExpiry(false);
        action.setQueryId(closedQueryId);
        assertEquals(SUCCESS, proxy.execute());
    }

    public void testDMGResponseDateExecuteDenied() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(false);
        action = createAction(UpdateQueryDateAction.class, null, "EnterDMGResponseDate");
        action.setExpiry(false);
        action.setSecurityService(securityServiceMock);
        assertEquals(DENIED, proxy.execute());
    }

    public void testDMGResponseDateUpdateValidationFailed() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(UpdateQueryDateAction.class, null, "UpdateDMGResponseDate");
        action.setExpiry(false);
        action.setSecurityService(securityServiceMock);
        assertEquals(INPUT, proxy.execute());
        assertEquals(2, action.getActionErrors().size());
        assertEquals(action.getText("field.required", new String[]{dateMsg}), action.getActionErrors().iterator().next());
    }

    public void testDMGResponseDateUpdate() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(UpdateQueryDateAction.class, null, "UpdateDMGResponseDate");
        action.setExpiry(false);
        action.setQueryId(closedQueryId);
        action.setDate(getExampleDate());
        action.setExplanation("explanation");
        action.setSecurityService(securityServiceMock);
        assertEquals(UPDATE, proxy.execute());
    }

    private static Date getExampleDate() throws ParseException {
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.parse("31/12/2021");
    }

}
