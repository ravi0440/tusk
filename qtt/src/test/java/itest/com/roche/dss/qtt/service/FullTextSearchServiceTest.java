package itest.com.roche.dss.qtt.service;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.model.WorkflowRouteType;
import com.roche.dss.qtt.query.web.model.QuickSearchParams;
import com.roche.dss.qtt.service.FullTextSearchService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.SearchResults;
import com.roche.dss.qtt.utility.config.Config;

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class})
@ContextConfiguration(locations = "classpath:applicationContext-service-test.xml")
public class FullTextSearchServiceTest {
	@Autowired
	protected FullTextSearchService searchService;
	
	@Autowired
	protected QueryService queryService;
	
	@Autowired
	protected TestUtilService testUtilService;
	
	@Autowired
	protected Config config;
	
	protected Query query;
	
	@Before
	public void setUp () {
		query = testUtilService.createQueryWithMandatory();
		queryService.persist(query);
		createPdfTempDir();
	}
	
	private void createPdfTempDir() {
		File tempPfdDir = new File(config.getPdfTemp());
		if(!tempPfdDir.exists()){
			tempPfdDir.mkdir();
		}
	}

	@After
	public void tearDown() {
		testUtilService.removeQuery(query);
		testUtilService.tearDown();
	}
	
	@Test
	public void testSearchDatabase () {
		// testing Query.querySeq
		QuickSearchParams p1 = new QuickSearchParams(String.valueOf(query.getQuerySeq()));
		SearchResults queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.queryNumber
		p1 = new QuickSearchParams("testquerynumber*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setQueryNumber("testQueryNumber1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("testQueryNumber*"));
		Assert.assertTrue(queries.contains(query));
		
		p1 = new QuickSearchParams("querynumber*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setQueryNumber("test QueryNumber1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("QueryNumber*"));
		Assert.assertFalse(queries.contains(query));
		
		// testing Query.followupNumber
		p1 = new QuickSearchParams("testfunumber*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setFollowupNumber("testFuNumber1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("testFuNumber*"));
		Assert.assertTrue(queries.contains(query));
		
		query.setFollowupNumber("test FuNumber1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(new QuickSearchParams("funumber*"));
		Assert.assertFalse(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("FuNumber*"));
		Assert.assertFalse(queries.contains(query));
		
		// testing Query.sourceCountry
		p1 = new QuickSearchParams("tcou*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setSourceCountryCode("TCOU1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.queryType.queryType
		// testing Query.reporterType.reporterType
		// testing Query.requester.firstName
		// testing Query.queryDescription.natureOfCase
		QuickSearchParams p2 = new QuickSearchParams("testquerytype*");
		queries = searchService.searchDatabase(p2);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p3 = new QuickSearchParams("testreportertype*");
		queries = searchService.searchDatabase(p3);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p5 = new QuickSearchParams("testReqFirstName*");
		queries = searchService.searchDatabase(p5);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p6 = new QuickSearchParams("testReqSurname*");
		queries = searchService.searchDatabase(p6);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p7 = new QuickSearchParams("testReqPhone*");
		queries = searchService.searchDatabase(p7);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p8 = new QuickSearchParams("testReqFax*");
		queries = searchService.searchDatabase(p8);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p9 = new QuickSearchParams("testCount*");
		queries = searchService.searchDatabase(p9);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p10 = new QuickSearchParams("testEmail@test.com*");
		queries = searchService.searchDatabase(p10);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p11 = new QuickSearchParams("testNatureOfCase*");
		queries = searchService.searchDatabase(p11);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p12 = new QuickSearchParams("testIndexCase*");
		queries = searchService.searchDatabase(p12);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p13 = new QuickSearchParams("testPrevMedicalHistory*");
		queries = searchService.searchDatabase(p13);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p14 = new QuickSearchParams("testSignsAndSymptoms*");
		queries = searchService.searchDatabase(p14);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p15 = new QuickSearchParams("testConmeds*");
		queries = searchService.searchDatabase(p15);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p16 = new QuickSearchParams("testInvestigations*");
		queries = searchService.searchDatabase(p16);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p17 = new QuickSearchParams("testDiagnoses*");
		queries = searchService.searchDatabase(p17);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p18 = new QuickSearchParams("testOtherInfo*");
		queries = searchService.searchDatabase(p18);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p19 = new QuickSearchParams("testPqpOtherComment*");
		queries = searchService.searchDatabase(p19);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p20 = new QuickSearchParams("testPqpType*");
		queries = searchService.searchDatabase(p20);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p21 = new QuickSearchParams("tstPrgy*");
		queries = searchService.searchDatabase(p21);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p22 = new QuickSearchParams("testPregnancyOutcome*");
		queries = searchService.searchDatabase(p22);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p23 = new QuickSearchParams("testPerformanceMetric*");
		queries = searchService.searchDatabase(p23);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p24 = new QuickSearchParams("testAffiliateNoteText*");
		queries = searchService.searchDatabase(p24);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p25 = new QuickSearchParams("testNonAffiliateNoteText*");
		queries = searchService.searchDatabase(p25);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p26 = new QuickSearchParams("testDose*");
		queries = searchService.searchDatabase(p26);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p27 = new QuickSearchParams("testDrugRetrievalName*");
		queries = searchService.searchDatabase(p27);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p28 = new QuickSearchParams("testFormulation*");
		queries = searchService.searchDatabase(p28);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p29 = new QuickSearchParams("testIndication*");
		queries = searchService.searchDatabase(p29);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p30 = new QuickSearchParams("testInnGenericName*");
		queries = searchService.searchDatabase(p30);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p31 = new QuickSearchParams("testRoute*");
		queries = searchService.searchDatabase(p31);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p32 = new QuickSearchParams("testCtrlOformat*");
		queries = searchService.searchDatabase(p32);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p33 = new QuickSearchParams("testCaseSelection*");
		queries = searchService.searchDatabase(p33);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p34 = new QuickSearchParams("testProtocolNumber*");
		queries = searchService.searchDatabase(p34);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p35 = new QuickSearchParams("testPatientNumber*");
		queries = searchService.searchDatabase(p35);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p36 = new QuickSearchParams("testCrtnNumber*");
		queries = searchService.searchDatabase(p36);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p37 = new QuickSearchParams("testAerNumber*");
		queries = searchService.searchDatabase(p37);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p38 = new QuickSearchParams("testLocalReferenceNumber*");
		queries = searchService.searchDatabase(p38);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p39 = new QuickSearchParams("testExternalReferenceNumber*");
		queries = searchService.searchDatabase(p39);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p40 = new QuickSearchParams("testAeTermDescription*");
		queries = searchService.searchDatabase(p40);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p41 = new QuickSearchParams("testldocType*");
		queries = searchService.searchDatabase(p41);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p42 = new QuickSearchParams("testInterval*");
		queries = searchService.searchDatabase(p42);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p43 = new QuickSearchParams("20020101");
		queries = searchService.searchDatabase(p43);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p44 = new QuickSearchParams("20020104");
		queries = searchService.searchDatabase(p44);
		Assert.assertFalse(queries.contains(query));
		QuickSearchParams p45 = new QuickSearchParams("testAttachmentTitle2");
		queries = searchService.searchDatabase(p45);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p45);
		Assert.assertFalse(queries.contains(query));
		
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p2);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p3);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p5);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p6);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p7);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p8);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p9);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p10);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p11);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p12);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p13);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p14);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p15);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p16);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p17);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p18);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p19);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p20);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p21);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p22);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p23);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p24);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p25);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p26);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p27);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p28);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p29);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p30);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p31);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p32);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p33);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p34);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p35);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p36);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p37);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p38);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p39);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p40);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p41);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p42);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p43);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p44);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p45);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p45);
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.currentDueDate
		p1 = new QuickSearchParams("20010101");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		DateTime currentDueDate = new DateTime(2001, 1, 1, 0, 0, 0, 0);
		query.setCurrentDueDate(currentDueDate);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.originalDueDate
		p1 = new QuickSearchParams("20010102");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		DateTime originalDueDate = new DateTime(2001, 1, 2, 0, 0, 0, 0);
		query.setOriginalDueDate(originalDueDate);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.urgencyReason
		p1 = new QuickSearchParams("testurgencyreason*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setUrgencyReason("testUrgencyReason1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("testUrgencyReason*"));
		Assert.assertTrue(queries.contains(query));
		
		p1 = new QuickSearchParams("urgencyreason*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setUrgencyReason("test UrgencyReason1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("UrgencyReason*"));
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.workflowRouteType
		p1 = new QuickSearchParams("a*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setWorkflowRouteType(WorkflowRouteType.TYPE_A1);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("A*"));
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.originalDueDate
		p1 = new QuickSearchParams("20010103");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		DateTime dateRequested = new DateTime(2001, 1, 3, 0, 0, 0, 0);
		query.setDateRequested(dateRequested);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.dmgDueDate
		p1 = new QuickSearchParams("20010104");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		DateTime dmgDueDate = new DateTime(2001, 1, 4, 0, 0, 0, 0);
		query.setDmgDueDate(dmgDueDate);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.expiry
		p1 = new QuickSearchParams("20010105");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		DateTime expiryDate = new DateTime(2001, 1, 5, 0, 0, 0, 0);
		query.setExpiryDate(expiryDate);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.querySummary
		p1 = new QuickSearchParams("testquerysummary*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setUrgencyReason("testQuerySummary1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("testQuerySummary*"));
		Assert.assertTrue(queries.contains(query));
		
		p1 = new QuickSearchParams("querysummary*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setUrgencyReason("test QuerySummary1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("QuerySummary*"));
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.preQueryPrepComment
		p1 = new QuickSearchParams("testprequeryprepcomment*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setUrgencyReason("testPreQueryPrepComment1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("testPreQueryPrepComment*"));
		Assert.assertTrue(queries.contains(query));
		
		p1 = new QuickSearchParams("prequeryprepcomment*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setUrgencyReason("test PreQueryPrepComment1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("PreQueryPrepComment*"));
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.caseComment
		p1 = new QuickSearchParams("testcasecomment*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setCaseComment("testCaseComment1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("testCaseComment*"));
		Assert.assertTrue(queries.contains(query));
		
		p1 = new QuickSearchParams("casecomment*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setCaseComment("test CaseComment1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("CaseComment*"));
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.oformatSpecialRequest
		p1 = new QuickSearchParams("testoformatspecialrequest*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setCaseComment("testOformatSpecialRequest1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("testOformatSpecialRequest*"));
		Assert.assertTrue(queries.contains(query));
		
		p1 = new QuickSearchParams("oformatspecialreques*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setCaseComment("test OformatSpecialRequest1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("OformatSpecialRequest*"));
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.mnftBatchNumber
		p1 = new QuickSearchParams("testmnftbatchnumber*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setMnftBatchNumber("testMnftBatchNumber1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("testMnftBatchNumber*"));
		Assert.assertTrue(queries.contains(query));
		
		p1 = new QuickSearchParams("mnftbatchnumber*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setMnftBatchNumber("test MnftBatchNumber1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("MnftBatchNumber*"));
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.indNumber
		p1 = new QuickSearchParams("testindnumber*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setIndNumber("testIndNumber1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("testIndNumber*"));
		Assert.assertTrue(queries.contains(query));
		
		p1 = new QuickSearchParams("indnumber*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setCaseComment("test IndNumber1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("IndNumber*"));
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.datetimeSubmitted
		p1 = new QuickSearchParams("20010106");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		DateTime datetimeSubmitted = new DateTime(2001, 1, 6, 0, 0, 0, 0);
		query.setDatetimeSubmitted(datetimeSubmitted);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.queryLabel
		p1 = new QuickSearchParams("testquerylabel*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setQueryLabel("testQueryLabel1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("testQueryLabel*"));
		Assert.assertTrue(queries.contains(query));
		
		p1 = new QuickSearchParams("querylabel*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setQueryLabel("test QueryLabel1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("QueryLabel*"));
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.requesterQueryLabel
		p1 = new QuickSearchParams("testrequesterquerylabel*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setRequesterQueryLabel("testRequesterQueryLabel1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("testRequesterQueryLabel*"));
		Assert.assertTrue(queries.contains(query));
		
		p1 = new QuickSearchParams("requesterquerylabel*");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		
		query.setRequesterQueryLabel("test RequesterQueryLabel1");
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(new QuickSearchParams("RequesterQueryLabel*"));
		Assert.assertTrue(queries.contains(query));
		
		p1 = new QuickSearchParams("testAttachmentTitle1");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		p2 = new QuickSearchParams("testFilename1");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		p3 = new QuickSearchParams("testAuthor1");
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		// initiateDate
		QuickSearchParams p4 = new QuickSearchParams("20030101");
		queries = searchService.searchDatabase(p4);
		Assert.assertFalse(queries.contains(query));
		// finalDeliveryDate
		p5 = new QuickSearchParams("20030102");
		queries = searchService.searchDatabase(p5);
		Assert.assertFalse(queries.contains(query));
		//dmgMember
		p6 = new QuickSearchParams("testResponsible7");
		queries = searchService.searchDatabase(p6);
		Assert.assertFalse(queries.contains(query));
		//pSMember
		p7 = new QuickSearchParams("testResponsible3");
		queries = searchService.searchDatabase(p7);
		Assert.assertFalse(queries.contains(query));
		// dSCLMember
		p8 = new QuickSearchParams("testResponsible11");
		queries = searchService.searchDatabase(p8);
		Assert.assertFalse(queries.contains(query));
		
		testUtilService.updateQuery3(query);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p2);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p2);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p3);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p3);
		Assert.assertTrue(queries.contains(query));
		// initiateDate
		queries = searchService.searchDatabase(p4);
		Assert.assertTrue(queries.contains(query));
		// finalDeliveryDate
		queries = searchService.searchDatabase(p5);
		Assert.assertTrue(queries.contains(query));
		//dmgMember
		queries = searchService.searchDatabase(p6);
		Assert.assertTrue(queries.contains(query));
		//pSMember
		queries = searchService.searchDatabase(p7);
		Assert.assertTrue(queries.contains(query));
		// dSCLMember
		queries = searchService.searchDatabase(p8);
		Assert.assertTrue(queries.contains(query));
		
		// testing Query.queryStatusCode.statusCode
		p1 = new QuickSearchParams("testResponsible7");
		p2 = new QuickSearchParams("sta*");
		
		queries = searchService.searchDatabase(p1);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchDatabase(p2);
		Assert.assertFalse(queries.contains(query));
		
		testUtilService.updateQuery4(query);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchDatabase(p2);
		Assert.assertFalse(queries.contains(query));
	}
	
	@Test
	public void testSearchDatabaseByDateRanges () throws Exception {
		QuickSearchParams p = new QuickSearchParams();
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		p.setQryResponseDateFrom(sdf.parse("01/02/2001"));
		
		SearchResults results = searchService.searchDatabase(p);
		Assert.assertFalse(results.contains(query));
		
		DateTime dateRequested = new DateTime(2001, 3, 1, 0, 0, 0, 0);
		query.setDateRequested(dateRequested);
		query = queryService.merge(query);
		
		results = searchService.searchDatabase(p);
		Assert.assertTrue(results.contains(query));
		
		p.setPeriodDateFrom(sdf.parse("01/12/2001"));
		results = searchService.searchDatabase(p);
		Assert.assertFalse(results.contains(query));
		
		//RetrievalPeriod.startTs = 01-Jan-2002
		testUtilService.updateQuery2(query);
		query = queryService.merge(query);
		
		results = searchService.searchDatabase(p);
		Assert.assertTrue(results.contains(query));
	}
	
	@Test
	public void testSearchAttachmentsPDF() {
		QuickSearchParams p1 = new QuickSearchParams("gęślą jaźń");
		SearchResults queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertFalse(queries.contains(query));
		
		String filename = "pl1.pdf";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		//Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth(p1);
		//Assert.assertTrue(queries.contains(query));
	}

	@Test
	public void testSearchAttachmentsPDFandRtfEnglish() {
		QuickSearchParams p1 = new QuickSearchParams("gęślą jaźń.");
		QuickSearchParams p2 = new QuickSearchParams("testRtfEnghish1");
		SearchResults queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertFalse(queries.contains(query));
		
		queries = searchService.searchDatabase(p2);
		queries = searchService.searchDatabase(p2);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p2);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p2);
		Assert.assertFalse(queries.contains(query));
		
		String filename = "pl1.pdf";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		//Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth(p1);
		//Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(p2);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p2);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p2);
		Assert.assertFalse(queries.contains(query));
		
		filename = "en1.rtf";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
//		queries = searchService.searchDatabase(p1);
//		Assert.assertFalse(queries.contains(query));
//		queries = searchService.searchAttachments(p1);
//		Assert.assertTrue(queries.contains(query));
//		queries = searchService.searchBoth(p1);
//		Assert.assertTrue(queries.contains(query));
		
		queries = searchService.searchDatabase(p2);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p2);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth(p2);
		Assert.assertTrue(queries.contains(query));
	}

	
	@Test
	public void testSearchAttachmentsPlain() {
		QuickSearchParams p1 = new QuickSearchParams("gęślą1 jaźń1");
		SearchResults queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertFalse(queries.contains(query));
		
		String filename = "pl1.txt";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertTrue(queries.contains(query));
	}
	
	@Test
	public void testSearchAttachmentsDoc() {
		QuickSearchParams p1 = new QuickSearchParams("gęślą2 jaźń2");
		SearchResults queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertFalse(queries.contains(query));
		
		String filename = "pl1.doc";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertTrue(queries.contains(query));
	}
	
	@Test
	public void testSearchAttachmentsDocx() {
		QuickSearchParams p1 = new QuickSearchParams("gęślą3 jaźń3");
		SearchResults queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertFalse(queries.contains(query));
		
		String filename = "pl1.docx";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertTrue(queries.contains(query));
	}
	
	//@Test
	public void testSearchAttachmentsRtf() {
		// FIXME does not work
		/*QuickSearchParams p1 = new QuickSearchParams("gęślą6 jaźń6");
		SearchResults queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertFalse(queries.contains(query));
		
		String filename = "pl1.rtf";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertTrue(queries.contains(query));*/
	}
	
	@Test
	public void testSearchAttachmentsRtfEnglish() {
		QuickSearchParams p1 = new QuickSearchParams("testRtfEnghish1");
		SearchResults queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertFalse(queries.contains(query));
		
		String filename = "en1.rtf";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertTrue(queries.contains(query));
	}
	
	//@Test
	public void testSearchAttachmentsXls() {
		/*QuickSearchParams p1 = new QuickSearchParams("gęślą7 jaźń7");
		SearchResults queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertFalse(queries.contains(query));
		
		String filename = "pl1.xls";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertTrue(queries.contains(query));*/
	}
	
	//@Test
	public void testSearchAttachmentsXlsx() {
		/*QuickSearchParams p1 = new QuickSearchParams("gęślą8 jaźń8");
		SearchResults queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertFalse(queries.contains(query));
		
		String filename = "pl1.xlsx";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertTrue(queries.contains(query));*/
	}

	//@Test
	public void testSearchAttachmentsPpt() {
		/*QuickSearchParams p1 = new QuickSearchParams("gęślą9 jaźń9");
		SearchResults queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertFalse(queries.contains(query));
		
		String filename = "pl2.ppt";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertTrue(queries.contains(query));*/
	}
	
	//@Test
	public void testSearchAttachmentsPptx() {
		/*QuickSearchParams p1 = new QuickSearchParams("gęślą10 jaźń10");
		SearchResults queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertFalse(queries.contains(query));
		
		String filename = "pl2.pptx";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertTrue(queries.contains(query));*/
	}
	
	//@Test
	public void testSearchAttachmentsHtm() {
		/*QuickSearchParams p1 = new QuickSearchParams("gęślą11 jaźń11");
		SearchResults queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertFalse(queries.contains(query));
		
		String filename = "pl1.htm";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertTrue(queries.contains(query));*/
	}
	
	//@Test
	public void testSearchAttachmentsMsgHtmlUnicode() {
		/*QuickSearchParams p1 = new QuickSearchParams("gęślą4 jaźń4");
		SearchResults queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertFalse(queries.contains(query));
		
		String filename = "pl1_html_unicode.msg";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase(p1);
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments(p1);
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth(p1);
		Assert.assertTrue(queries.contains(query));*/
	}
	
	//@Test
	public void testSearchAttachmentsMsgHtml() {
		// FIXME does not work
		/*List<Query> queries = searchService.searchDatabase("gęślą5 jaźń5");
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments("gęślą5 jaźń5");
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchBoth("gęślą5 jaźń5");
		Assert.assertFalse(queries.contains(query));
		
		String filename = "src/test/resources/testAttachments/pl1_html.msg";
		testUtilService.updateQueryAttach(query, filename);
		query = queryService.merge(query);
		
		queries = searchService.searchDatabase("gęślą5 jaźń5");
		Assert.assertFalse(queries.contains(query));
		queries = searchService.searchAttachments("gęślą5 jaźń5");
		Assert.assertTrue(queries.contains(query));
		queries = searchService.searchBoth("gęślą5 jaźń5");
		Assert.assertTrue(queries.contains(query));*/
	}
}
