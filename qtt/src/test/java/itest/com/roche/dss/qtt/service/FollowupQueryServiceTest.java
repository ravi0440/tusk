package itest.com.roche.dss.qtt.service;

import com.roche.dss.qtt.eao.FollowupQueryEAO;
import com.roche.dss.qtt.model.FuQuery;
import com.roche.dss.qtt.model.FuQueryId;
import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.service.FollowupQueryService;
import com.roche.dss.qtt.service.QueryService;
import javax.sql.DataSource;

/**
 * User: pruchnil
 */
@ContextConfiguration(locations = {"classpath:applicationContext-service-test.xml"})
public class FollowupQueryServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private FollowupQueryService followupQueryService;
    @Autowired
    private FollowupQueryEAO followupQueryEAO;
    @Autowired
    private QueryService queryService;

    @Override
    @Autowired
    @Qualifier("qttDataSource")
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    @Test
    public void testCreateFollowup(){
        int beforeFuQuery = simpleJdbcTemplate.queryForInt("select count(*) from fu_queries");
        int beforeFuDrug = simpleJdbcTemplate.queryForInt("select count(*) from fu_drugs");
        int beforeFuLabellingDocs = simpleJdbcTemplate.queryForInt("select count(*) from fu_labelling_documents");
        int beforeFuRetrievalPeriod = simpleJdbcTemplate.queryForInt("select count(*) from fu_retrieval_periods");
        int beforeFuRequester = simpleJdbcTemplate.queryForInt("select count(*) from fu_requesters");
        int beforeFuOrganization = simpleJdbcTemplate.queryForInt("select count(*) from fu_organisations");
        followupQueryService.createFollowup(20262);
        queryService.flush();

        int afterFuQuery = simpleJdbcTemplate.queryForInt("select count(*) from fu_queries");
        int afterFuDrug = simpleJdbcTemplate.queryForInt("select count(*) from fu_drugs");
        int afterFuLabellingDocs = simpleJdbcTemplate.queryForInt("select count(*) from fu_labelling_documents");
        int afterFuRetrievalPeriod = simpleJdbcTemplate.queryForInt("select count(*) from fu_retrieval_periods");
        int afterFuRequester = simpleJdbcTemplate.queryForInt("select count(*) from fu_requesters");
        int afterFuOrganization = simpleJdbcTemplate.queryForInt("select count(*) from fu_organisations");
        FuQuery fuQuery = followupQueryEAO.getByIdObj(new FuQueryId(0, 20262l));
        Query query = queryService.find(20262l);

        assertEquals(beforeFuQuery + 1, afterFuQuery);
        assertEquals(beforeFuDrug + 1, afterFuDrug);
        assertEquals(beforeFuRetrievalPeriod + 1, afterFuRetrievalPeriod);
        assertEquals(beforeFuLabellingDocs + 1, afterFuLabellingDocs);
        assertEquals(beforeFuRequester + 1, afterFuRequester);
        assertEquals(beforeFuOrganization + 1, afterFuOrganization);
        assertNotNull(fuQuery);
        assertEquals("FU0000", fuQuery.getFollowupNumber());
        assertNotNull(query);
        assertEquals("FU0001", query.getFollowupNumber());
        assertEquals(Long.valueOf(1), query.getFollowupSeq());
    }
}
