package itest.com.roche.dss.qtt.service;

import com.roche.dss.qtt.service.QueryNoteService;
import javax.sql.DataSource;

/**
 * User: pruchnil
 */
@ContextConfiguration(locations = {"classpath:applicationContext-service-test.xml"})
public class QueryNoteServiceTest extends AbstractTransactionalJUnit4SpringContextTests {
    private static final long queryIdToNonAffNote = 20262l;
    private static final long existingNonAffNoteId = 152221l;
    private static final long queryIdToAffNote = 20263l;
    private static final long existingAffNoteId = 152222l;
    private static final String text = "content";
    private static final String user = "username";

    @Autowired
    private QueryNoteService noteService;

    @Override
    @Autowired
    @Qualifier("qttDataSource")
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    @Test
    public void testSaveNewNonAffiliateNote() {
        int oldCount = simpleJdbcTemplate.queryForInt("select count(*) from non_affiliate_notes");
        noteService.saveQueryNote(0, queryIdToNonAffNote, text, false, user);
        noteService.flush();
        int newCount = simpleJdbcTemplate.queryForInt("select count(*) from non_affiliate_notes");
        assertEquals(oldCount + 1, newCount);
    }

    @Test
    public void testSaveExistingNonAffiliateNote() {
        noteService.saveQueryNote(existingNonAffNoteId, queryIdToNonAffNote, text, false, user);
        noteService.flush();
        String content = noteService.findContent(existingNonAffNoteId, false);
        assertEquals(StringUtils.upperCase(text), content);
    }


    @Test
    public void testSaveNewAffiliateNote() {
        int oldCount = simpleJdbcTemplate.queryForInt("select count(*) from affiliate_notes");
        noteService.saveQueryNote(0, queryIdToAffNote, text, true, "username");
        noteService.flush();
        int newCount = simpleJdbcTemplate.queryForInt("select count(*) from affiliate_notes");
        assertEquals(oldCount + 1, newCount);
    }

    @Test
    public void testSaveExistingAffiliateNote() {
        noteService.saveQueryNote(existingAffNoteId, queryIdToAffNote, text, true, user);
        noteService.flush();
        String content = noteService.findContent(existingAffNoteId, true);
        assertEquals(StringUtils.upperCase(text), content);
    }

    @Test
    public void testRemoveQueryNote(){
        int oldCount = simpleJdbcTemplate.queryForInt("select count(*) from non_affiliate_notes");
        noteService.removeQueryNote(existingNonAffNoteId, false);
        noteService.flush();
        int newCount = simpleJdbcTemplate.queryForInt("select count(*) from non_affiliate_notes");
        assertEquals(oldCount - 1, newCount);
    }


}
