package itest.com.roche.dss.qtt.eao;

public class TestUtil {
	public static long ID_NOT_EXISTING_LONG = -1;
	public static long ID_NOT_EXISTING2_LONG = -2;
	public static long ID_NOT_EXISTING3_LONG = -3;
	public static long ID_NOT_EXISTING4_LONG = -4;
	public static short ID_NOT_EXISTING_SHORT = -1;
	public static short ID_NOT_EXISTING2_SHORT = -2;
	public static byte ID_NOT_EXISTING_BYTE = -1;
	public static byte ID_NOT_EXISTING2_BYTE = -2;
}
