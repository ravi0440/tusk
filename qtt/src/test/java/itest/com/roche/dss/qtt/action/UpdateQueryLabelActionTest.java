package itest.com.roche.dss.qtt.action;

import com.roche.dss.qtt.query.web.action.CommonActionSupport;
import com.roche.dss.qtt.query.web.action.UpdateQueryLabelAction;
import com.roche.dss.qtt.security.service.SecurityService;
import static com.roche.dss.qtt.QttGlobalConstants.DSCL_PERMISSION;

/**
 * User: pruchnil
 */
public class UpdateQueryLabelActionTest extends BaseStrutsTestCase {
    private UpdateQueryLabelAction action;
    private SecurityService securityServiceMock = Mockito.mock(SecurityService.class);
    private static final long closedQueryId = 20262l;

    public void testExecute() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(UpdateQueryLabelAction.class, null, "EnterQueryLabel");
        action.setSecurityService(securityServiceMock);
        action.setQueryId(closedQueryId);
        assertEquals(CommonActionSupport.SUCCESS, proxy.execute());
    }

    public void testExecuteDenied() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(false);
        action = createAction(UpdateQueryLabelAction.class, null, "EnterQueryLabel");
        action.setSecurityService(securityServiceMock);
        assertEquals(CommonActionSupport.DENIED, proxy.execute());
    }

    public void testUpdate() throws Exception {
        Mockito.when(securityServiceMock.isAllowedCheck(DSCL_PERMISSION)).thenReturn(true);
        action = createAction(UpdateQueryLabelAction.class, null, "UpdateQueryLabel");
        action.setSecurityService(securityServiceMock);
        action.setQueryLabel("");
        assertEquals(CommonActionSupport.INPUT, proxy.execute());
    }

}
