package itest.com.roche.dss.qtt.service;

import com.roche.dss.qtt.model.AttachmentCategory;
import com.roche.dss.qtt.model.LdocType;
import com.roche.dss.qtt.model.QttCountry;
import com.roche.dss.qtt.model.QueryType;
import com.roche.dss.qtt.model.ReporterType;
import com.roche.dss.qtt.model.WorkflowRouteType;
import com.roche.dss.qtt.service.DictionaryService;
import javax.sql.DataSource;
import java.util.List;

/**
 * User: pruchnil
 */
@ContextConfiguration(locations = {"classpath:applicationContext-service-test.xml"})
public class DictionaryServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    DictionaryService service;

    @Override
    @Autowired
    @Qualifier("qttDataSource")
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    @Test
    public void testGetAllAttachmentCategories(){
        List<AttachmentCategory> result = service.getAllAttachmentCategories();
        Assert.assertTrue(result.size()==3);
    }
    
    @Test
    public void testGetUsedRetrievalNames() {
    	List<String> values = service.getUsedRetrievalNames();
    	Assert.assertEquals(16, values.size());
    }
    
    @Test
    public void testGetUsedGenericNames() {
    	List<String> values = service.getUsedGenericNames();
    	Assert.assertEquals(13, values.size());
    }
    
    @Test
    public void testGetUsedIndications() {
    	List<String> values = service.getUsedIndications();
    	Assert.assertEquals(8, values.size());
    }
    
    @Test
    public void testGetQueryTypes() {
    	List<QueryType> values = service.getQueryTypes();
    	Assert.assertEquals(14, values.size());
    }
    
    @Test
    public void testGetUsedResponseAuthors() {
    	List<String> values = service.getUsedResponseAuthors();
    	Assert.assertEquals(3, values.size());
    }
    
    @Test
    public void testGetUsedLabellingDocs() {
    	List<LdocType> values = service.getUsedLabellingDocs();
    	Assert.assertEquals(1, values.size());
    }
    
    @Test
    public void getProcessTypes() {
    	List<WorkflowRouteType> values = service.getProcessTypes();
    	Assert.assertEquals(1, values.size());
    }

    @Test
    public void testGetDMGUserInvolvedNames() {
    	List<String> values = service.getDMGUserInvolvedNames();
        //TODO some jbpm sample data needed for tests
    	Assert.assertEquals(0, values.size());
    }
    
    @Test
    public void testGetPSUserInvolvedNames() {
    	List<String> values = service.getPSUserInvolvedNames();
         //TODO some jbpm sample data needed for tests
    	Assert.assertEquals(0, values.size());
    }
    
    @Test
    public void testGetDSCLUserInvolvedNames() {
    	List<String> values = service.getDSCLUserInvolvedNames();
         //TODO some jbpm sample data needed for tests
    	Assert.assertEquals(0, values.size());
    }

    @Test
    public void testGetUsedRequestorLastNames() {
    	List<String> values = service.getUsedRequestorLastNames();
    	Assert.assertEquals(22, values.size());
    }

    @Test
    public void testGetUsedRequestorCountries() {
    	List<QttCountry> values = service.getUsedRequestorCountries();
    	Assert.assertEquals(9, values.size());
    }

    @Test
    public void testGetUsedReporterTypes() {
    	List<ReporterType> values = service.getUsedReporterTypes();
    	Assert.assertEquals(6, values.size());
    }

    @Test
    public void testGetUsedSourceCountries() {
    	List<QttCountry> values = service.getUsedSourceCountries();
    	Assert.assertEquals(9, values.size());
    }
}
