package itest.com.roche.dss.qtt.service;

import javax.annotation.Resource;
import javax.sql.DataSource;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.web.model.QuickSearchParams;
import com.roche.dss.qtt.query.web.model.RepositorySearchParams;
import com.roche.dss.qtt.service.FullTextSearchService;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.qtt.service.RepositorySearchService;
import com.roche.dss.qtt.service.SearchResults;
import com.roche.dss.qtt.service.UtilService;
import com.roche.dss.util.InvalidatingJdbcTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class})
@ContextConfiguration(locations = "classpath:applicationContext-service-test.xml")
public class CacheTest {
	
	protected JdbcTemplate jdbcTemplate;
	
	protected InvalidatingJdbcTemplate invalidatingJdbcTemplate;
		
    @Autowired
    @Qualifier("qttDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
	@Autowired
	protected QueryService queryService;
    
	@Autowired
	protected TestUtilService testUtilService;
	
	@Autowired
	protected UtilService utilService;
	
	@Autowired
	protected RepositorySearchService repositorySearchService;
	
	@Autowired
	protected FullTextSearchService quickSearchService;
	
	@Resource(name = "invalidatingJdbcTemplate")
	public void setJdbcTemplate(InvalidatingJdbcTemplate invalidatingJdbcTemplate) {
		this.invalidatingJdbcTemplate = invalidatingJdbcTemplate;
	}
	
	protected Query query;
    
	@Before
	public void setUp () {
		query = testUtilService.createQueryWithMandatory();
		queryService.persist(query);
	}
	
	@After
	public void tearDown() {
		testUtilService.removeQuery(query);
		testUtilService.tearDown();
	}
	
    @Test
    public void testSecondLevelCache() {
    	String summary = "testQuerySummary2";
    	query.setQuerySummary(summary);
    	query = queryService.merge(query);
    	
    	jdbcTemplate.update("update queries set query_summary='testQuerySummaryUpdated2' where query_seq = ?", query.getQuerySeq());
    	
    	query = queryService.find(query.getQuerySeq());
    	
    	Assert.assertEquals(summary, query.getQuerySummary());
    	
    	utilService.cleanSecondLevelCache(false);
    	
    	query = queryService.find(query.getQuerySeq());
    	
    	Assert.assertEquals("testQuerySummaryUpdated2", query.getQuerySummary());
    }
    
    @Test
    public void testInvalidatingJdbcTemplateCache() {
    	String summary = "testQuerySummary2";
    	query.setQuerySummary(summary);
    	query = queryService.merge(query);
    	invalidatingJdbcTemplate.update("update queries set query_summary='testQuerySummaryUpdated2' where query_seq = ?", query.getQuerySeq());
    	query = queryService.find(query.getQuerySeq());
    	Assert.assertEquals("testQuerySummaryUpdated2", query.getQuerySummary());
    }
    
    @Test
    public void testQueryCache() {
    	String summary = "testQuerySummary2";
    	query.setQuerySummary(summary);
    	query = queryService.merge(query);
    	
		RepositorySearchParams p = new RepositorySearchParams();
		p.setQuerySummary(summary);
		
		SearchResults results = repositorySearchService.search(p);
		Assert.assertTrue(results.contains(query));
		
		jdbcTemplate.update("update queries set query_summary='testQuerySummaryUpdated2' where query_seq = ?", query.getQuerySeq());
		
		results = repositorySearchService.search(p);
		Assert.assertTrue(results.contains(query));
		
		utilService.cleanSecondLevelCache(false);
		
		results = repositorySearchService.search(p);
		Assert.assertFalse(results.contains(query));
    }
    
    @Test
    public void testQuickSearchQueryCache() throws InterruptedException {
    	String summary = "testQuerySummary2";
    	query.setQuerySummary(summary);
    	query = queryService.merge(query);
    	
		QuickSearchParams p = new QuickSearchParams(summary);
		SearchResults results = quickSearchService.searchDatabase(p);
		Assert.assertTrue(results.contains(query));
		
		jdbcTemplate.update("update queries set query_summary='testQuerySummaryUpdated2' where query_seq = ?", query.getQuerySeq());
		
		utilService.cleanSecondLevelCache(false);
		
		results = quickSearchService.searchDatabase(p);
		Assert.assertTrue(results.contains(query));
		
		testUtilService.reindexAll(Query.class);
		
		results = quickSearchService.searchDatabase(p);
		Assert.assertFalse(results.contains(query));
    }
}
