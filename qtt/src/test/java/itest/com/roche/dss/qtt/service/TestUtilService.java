package itest.com.roche.dss.qtt.service;

import com.roche.dss.qtt.model.AeTerm;
import com.roche.dss.qtt.model.Query;

public interface TestUtilService {
	public Query createQueryWithMandatory ();
	
	public void updateQuery2(Query query);
	
	public void updateQuery3(Query query);
	
	public void updateQuery4(Query query);
	
	public void removeQuery(Query query);
	
	public void updateQueryAttach(Query query, String filename);
	
	public void removeAeTerm(AeTerm term);
	
	public void tearDown();
	
	public void indexAll() throws InterruptedException;
	
	public void reindexAll(Class entityClass) throws InterruptedException;
}
