package itest.com.roche.dss.qtt.action;

import java.util.HashMap;
import java.util.Map;

/**
 * User: pruchnil
 */
public abstract class BaseStrutsTestCase extends TestCase {
    private static final String CONFIG_LOCATIONS = "classpath:applicationContext-web-test.xml";
    private static ApplicationContext applicationContext;
    private Dispatcher dispatcher;
    protected ActionProxy proxy;
    protected static MockServletContext servletContext;
    protected MockHttpServletRequest request;
    protected MockHttpServletResponse response;

    /**
     * Created action class based on namespace and name
     *
     * @param clazz     Class for which to create Action
     * @param namespace Namespace of action
     * @param name      Action name
     * @return Action class
     * @throws Exception Catch-all exception
     */
    @SuppressWarnings("unchecked")
    protected <T> T createAction(Class<T> clazz, String namespace, String name, Map<String, Object> session) throws Exception {
        // create a proxy class which is just a wrapper around the action call.
        // The proxy is created by checking the namespace and name against the
        // struts.xml configuration
        proxy = dispatcher.getContainer().getInstance(ActionProxyFactory.class).createActionProxy(namespace, name, null, null, true, false);
        // by default, don't pass in any request parameters
        proxy.getInvocation().getInvocationContext().setParameters(new HashMap());
        if (session != null) {
        	proxy.getInvocation().getInvocationContext().setSession(session);
        }
        // do not execute the result after executing the action
        proxy.setExecuteResult(true);

        // set the actions context to the one which the proxy is using
        ServletActionContext.setContext(proxy.getInvocation().getInvocationContext());
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        ServletActionContext.setRequest(request);
        ServletActionContext.setResponse(response);
        ServletActionContext.setServletContext(servletContext);
        ServletActionContext.getContext().setSession(new HashMap());
        return (T) proxy.getAction();
    }
    
    protected <T> T createAction(Class<T> clazz, String namespace, String name) throws Exception {
    	return createAction(clazz, namespace, name, null);
    }

    protected void setUp() throws Exception {
        if (applicationContext == null) {
            // this is the first time so initialize Spring context
            servletContext = new MockServletContext();
            servletContext.addInitParameter(ContextLoader.CONFIG_LOCATION_PARAM, CONFIG_LOCATIONS);
            applicationContext = (new ContextLoader()).initWebApplicationContext(servletContext);
//            servletContext.addInitParameter(BasicTilesContainer.DEFINITIONS_CONFIG, "WEB-INF/tiles.xml");
            // Struts JSP support servlet (for Freemarker)
            new JspSupportServlet().init(new MockServletConfig(servletContext));
            final StrutsTilesListener tilesListener = new StrutsTilesListener();
//            final ServletContextEvent event = new ServletContextEvent(servletContext);
//            tilesListener.contextInitialized(event);
        }
        // Dispatcher is the guy that actually handles all requests.  Pass in
        // an empty. Map as the parameters but if you want to change stuff like
        // what config files to read, you need to specify them here.  Here's how to
        // scan packages for actions (thanks to Hardy Ferentschik - Comment 66)
        // (see Dispatcher's source code)
        HashMap params = new HashMap();
        params.put("actionPackages", "com.roche.dss.query.web.action");
        dispatcher = new Dispatcher(servletContext, params);
        dispatcher.init();
        Dispatcher.setInstance(dispatcher);
    }
}
