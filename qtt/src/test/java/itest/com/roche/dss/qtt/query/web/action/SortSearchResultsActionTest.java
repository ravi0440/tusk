package itest.com.roche.dss.qtt.query.web.action;

import java.util.HashMap;
import java.util.Map;

import itest.com.roche.dss.qtt.action.BaseStrutsTestCase;
import com.roche.dss.qtt.query.web.action.SortSearchResultsAction;

public class SortSearchResultsActionTest extends  BaseStrutsTestCase {
	
    public void testQuickSearchResultsNoParams() throws Exception {
        Map<String, Object> session = new HashMap<String, Object>();
    	SortSearchResultsAction action = createAction(SortSearchResultsAction.class, null, "QuickSearchResults", session);
        
        String result = proxy.execute();
        Assert.assertEquals(ActionSupport.SUCCESS, result);
        
        Assert.assertNull(action.getResults());
    }
}
