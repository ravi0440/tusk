package itest.com.roche.dss.qtt.query.web.action.oldqrf;

import com.roche.dss.qtt.model.Query;
import com.roche.dss.qtt.query.web.action.ActionConstants;
import com.roche.dss.qtt.query.web.action.oldqrf.StdQrfAction;
import com.roche.dss.qtt.query.web.qrf.model.QrfFormModel;
import com.roche.dss.qtt.query.web.utils.UserDataInterceptor;
import com.roche.dss.qtt.service.QueryService;
import com.roche.dss.util.ApplicationContextProvider;
import itest.com.roche.dss.qtt.action.BaseStrutsTestCase;
import itest.com.roche.dss.qtt.service.TestUtilService;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public class StdQrfActionTest extends  BaseStrutsTestCase {

	protected static final Logger log = LoggerFactory.getLogger(StdQrfActionTest.class);
	
	protected Integer queryIdI;
	
	public void tearDown() {
		QueryService queryService = ApplicationContextProvider.ctx.getBean(QueryService.class);
		TestUtilService testUtilService = ApplicationContextProvider.ctx.getBean(TestUtilService.class);
		
		Query query = queryService.find(queryIdI.longValue());
		testUtilService.removeQuery(query);
		testUtilService.tearDown();
	}
	
	public void testExecute() throws Exception {
        StdQrfAction action2 = createAction(StdQrfAction.class, null, "StdQrfAction");
		Map<String, Object> session = new HashMap<String, Object>();
        action2.setSession(session);
        
        QrfFormModel model2 = (QrfFormModel) action2.getModel();
        
        model2.setFirstname("testFirstName1");
        model2.setLastname("testLastName1");
        model2.setTelephone("testTele1");
        model2.setEmail(UserDataInterceptor.testMail);
        String reqCountryCode = action2.getOrderedCountries().get(0).getCountryCode();
        model2.setReqCountry(reqCountryCode);
        model2.setOrgName("testOrgName1");
        String orgTypeS = action2.getOrganisationTypes().get(0).getTypeId();
        model2.setOrgType(Integer.parseInt(orgTypeS));
        
        String srcCountryCode = action2.getOrderedCountries().get(1).getCountryCode();
        model2.setSourceCountry(srcCountryCode);
        String repTypeS = action2.getReporterTypes().get(0).getReporterTypeId();
        model2.setReporterType(Integer.parseInt(repTypeS));
        
        model2.setAllDrugs(0);
        String retrDrug = action2.getDrugRetrievalNames().get(0);
        model2.setRetrDrug(retrDrug);
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
        model2.setDateRequested(sdf.parse("01/06/2011"));

        model2.setCumulative(1);        
                
        model2.setQueryType(action2.getDictionary().getQueryTypes().get(0).getQueryTypeCode());
        
        String result = action2.execute();
        Assert.assertEquals("drugEvent", result);
        
        queryIdI = (Integer) session.get(ActionConstants.QRF_QUERY_ID);
        Assert.assertNotNull(queryIdI);
	}
}
