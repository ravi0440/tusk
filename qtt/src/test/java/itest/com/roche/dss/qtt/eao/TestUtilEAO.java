package itest.com.roche.dss.qtt.eao;

import com.roche.dss.qtt.model.Query;

public interface TestUtilEAO {
	public Query createQueryWithMandatory ();
	
	public void updateQuery2(Query query);
	
	public void updateQuery3(Query query);
	
	public void updateQuery4(Query query);
	
	public void updateQueryAttach(Query query, String filename);
	
	public void tearDown();
}
