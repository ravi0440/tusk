package itest.com.roche.dss.qtt.service;

import com.roche.dss.qtt.query.comms.dto.ReportDTO;
import com.roche.dss.qtt.query.web.model.ReportCriteria;
import com.roche.dss.qtt.service.ReportService;
import javax.annotation.Resource;
import javax.sql.DataSource;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * This is test class for ReportService. It should be run with env-dev profile
 * User: pruchnil
 */
@ContextConfiguration(locations = {"classpath:applicationContext-service-test.xml"})
public class ReportServiceTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private ReportService reportService;

    @Override
    @Resource(name = "qttDataSource")
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    @Test
    public void testRetrieveReportDataByDateCriteria() {
        ReportCriteria criteria = new ReportCriteria();
        try {
        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
            criteria.setDateFrom(sdf.parse("23/02/2004"));
            criteria.setDateTo(sdf.parse("28/02/2010"));
        } catch (Exception e) {
            fail("Date parsing error");
        }
        List<ReportDTO> resultList = reportService.retrieveReportData(criteria);
        assertNotNull(resultList);
 // TODO commented after removing AuditHistory
//        assertTrue(resultList.size() > 0);
    }

    @Test
    public void testRetrieveReportDataByRouteCriteria() {
        ReportCriteria criteria = new ReportCriteria();
        criteria.setRoute("A1");
        List<ReportDTO> resultList = reportService.retrieveReportData(criteria);
        assertNotNull(resultList);
  // TODO commented after removing AuditHistory
//        assertTrue(resultList.size() == 16);
    }

//    @Test
//    public void test() throws Exception {
//        ReportCriteria criteria = new ReportCriteria();
//        reportService.exportToXLS(criteria);
//    }

}
