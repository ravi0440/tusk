#!/bin/bash
VERSIONS=$(ls -1 $DB_SCRIPTS_PATH)

. ./util.sh

function executeVersion {
    path=$1
    scripts=$(ls -1 $path|grep -v rollback)

    for script in $scripts
    do
        finalPath="$path/$script"
        runsql $finalPath $QTT_DB_USER $QTT_DB_PASS
    done
}

for version in $VERSIONS
do
    executeVersion $DB_SCRIPTS_PATH/$version
done
