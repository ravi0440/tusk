#!/bin/bash

function runsql {
    scrFileName=$1
    user=$2
    password=$3
    filter=$4
    replace="$5"

    echo "executing:$scrFileName"

    if [ -z "$filter" ]; then
       script=$(cat $scrFileName | sed 's/\r$//' | grep -v '\-\-')
    else
       replace=$(echo $replace | sed -r 's/\//\\\//g')
       echo "replacing $filter with $replace"
       script=$(sed "s/$filter/$replace/g" $scrFileName | sed 's/\r$//' | grep -v '\-\-')
    fi

    script="SET AUTOCOMMIT ON \n $script"

    echo $user
    echo $password
    echo -e "$script"

    echo -e "$script\n exit\n" | sqlplus -S $user/$password
}

