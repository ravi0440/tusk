DROP USER qtt_jbpm CASCADE;
DROP TABLESPACE tbs_temp_qtt_jbpm INCLUDING CONTENTS AND DATAFILES;
DROP TABLESPACE qtt_jbpm INCLUDING CONTENTS AND DATAFILES;
DROP TABLESPACE APP_IDX INCLUDING CONTENTS AND DATAFILES;
CREATE TABLESPACE qtt_jbpm
  DATAFILE 'tbs_qtt_jbpm.dat'
    SIZE 200M
    REUSE
    AUTOEXTEND ON NEXT 10M MAXSIZE 600M;
CREATE TEMPORARY TABLESPACE tbs_temp_qtt_jbpm
  TEMPFILE 'tbs_temp_qtt_jbpm.dbf'
    SIZE 50M
    AUTOEXTEND ON;
CREATE USER qtt_jbpm
  IDENTIFIED BY qtt_jbpm
  DEFAULT TABLESPACE qtt_jbpm
  TEMPORARY TABLESPACE tbs_temp_qtt_jbpm;
CREATE TABLESPACE APP_IDX DATAFILE 'tbs_qtt_jbpm_idx.dat' SIZE 50M
    EXTENT MANAGEMENT LOCAL AUTOALLOCATE;
GRANT create session TO qtt_jbpm;
GRANT create table TO qtt_jbpm;
GRANT create view TO qtt_jbpm;
GRANT create any trigger TO qtt_jbpm;
GRANT create any procedure TO qtt_jbpm;
GRANT create sequence TO qtt_jbpm;
GRANT create synonym TO qtt_jbpm;
grant ALL PRIVILEGES to qtt_jbpm;

ALTER USER qtt_jbpm IDENTIFIED BY "qwerty";
ALTER USER qtt_jbpm identified BY "qwerty" account unlock;