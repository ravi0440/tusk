#!/bin/bash

last_backup_file=""

export last_backup_file=$(ssh oracle@rkalv264629.kau.roche.com "ls -t $BACKUP_PATH/AGPRD_expdp_865737441_QTT* | head -2| grep -v '\.log'")
export LAST_BACKUP_BASE_NAME=$(basename $last_backup_file)


if [ ! -f "$LOCAL_DB_BACKUP_PATH/$LAST_BACKUP_BASE_NAME" ]
then
  echo "$last_backup_file not exists in $LOCAL_DB_BACKUP_PATH"
  scp oracle@rkalv264629.kau.roche.com:$last_backup_file $LOCAL_DB_BACKUP_PATH/
fi

export LAST_BACKUP_BASE_NAME=$(basename $last_backup_file)

