#!/bin/bash

#to configure
export LOCAL_DB_PASS=qwerty
export LOCAL_DB_USER=system
export LOCAL_DB_BACKUP_PATH=/home/marcin/Desktop/qtt/sf_qttdb
export QTT_DB_USER="qtt_jbpm"
export QTT_DB_PASS="qwerty"
export BACKUPS_PATH=/home/marcin/Desktop/qtt/copy
export DB_SCRIPTS_PATH="../sf_qttdbscripts"
#do not change following
export BACKUP_PATH=/exp01/TRACCP1
export LAST_BACKUP_BASE_NAME=""

. ./util.sh

if [ $# -eq 0 ]
then
    . ./importDBBackup.sh
else
    export LAST_BACKUP_BASE_NAME=$1
    if [ "${LAST_BACKUP_BASE_NAME: -3}" != "dmp" ]
    then
        export LAST_BACKUP_BASE_NAME="$LAST_BACKUP_BASE_NAME.dmp"
    fi
fi

echo "creating copy of $LAST_BACKUP_BASE_NAME to new env"
cp -n $LOCAL_DB_BACKUP_PATH/$LAST_BACKUP_BASE_NAME $BACKUPS_PATH
chmod 666 $BACKUPS_PATH/$LAST_BACKUP_BASE_NAME

#echo "removing and creating localdb"
runsql removeOldDB.sql $LOCAL_DB_USER $LOCAL_DB_PASS
runsql createDumpDir.sql $LOCAL_DB_USER $LOCAL_DB_PASS '<path>' "$BACKUPS_PATH"

. ./importStructure.sh

echo "converting columns"
runsql convertColumns.sql $LOCAL_DB_USER $LOCAL_DB_PASS

. ./importData.sh

. ./importConstraints.sh

#. ./upgradeSchema.sh

runsql importViews.sql $LOCAL_DB_USER $LOCAL_DB_PASS

echo "finished at $(date)"

