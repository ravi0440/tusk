#!/bin/bash
REMOTE_REPO=rbaus0g1.bas.roche.com
REMOTE_REPO_PATH=sml35_qtt_deployment_dir/
REMOTE_REPO_RESTART_DIR=/restart_sml35
LOG_FILE="deployDev.log"
USER=farynam


OPTS="-k -v -u $USER --progress-bar"
warName=""

savedPAth=$(pwd)
cd ../..
echo $(pwd)



function buildWar {
  echo "building war"
  mvn -P dev -DskipTests clean package
}

function copyWarToRemote {
  echo "copying war:$warName to remote"
  curl $OPTS -T "target/$warName" "https://$REMOTE_REPO/$REMOTE_REPO_PATH" | tee -a "${LOG_FILE}" ; test ${PIPESTATUS[0]} -eq 0
}

function restart {
   echo "Restarting the jboss"
   touch "restart"
   curl $OPTS -T "restart" "https://$REMOTE_REPO/$REMOTE_REPO_RESTART_DIR" | tee -a "${LOG_FILE}" ; test ${PIPESTATUS[0]} -eq 0
   rm "restart"
}

buildWar

warName=$(ls -1 target|grep "\.war")

if [ -f "target/$warName" ]
  then
    copyWarToRemote
    restart
  else
    echo "war not available nothing to copy"
fi

cd $savedPAth

echo "finished"


